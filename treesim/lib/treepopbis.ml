(* ident 0 is reserved for the abstract root gathering all trees *)
(* mais quel trait ? *)
module G = Tree.Genealogy

(* FIXME might have to add the prune proba in it ? *)
type ('a, 'b) t = {
  traitable : ('a, 'b) Pop.Traitable.t ;
  genealogy : G.t ;
}

module Make (T : Pop.Sig.TRAIT) :
  (Pop.Sig.POP with type trait = T.t
                and type group = T.x
                and type t = (T.t, T.x) t) =
  struct
    module H = Pop.Traitable.Make (T)
   
    type trait = T.t
    type group = T.x
    (* probably path for ident ? *)
    (* FIXME might put ident in ancestry instead of trait *)
    type ident = int
    type nonrec t = (trait, group) t

    let create_empty () =
      let z = {
        traitable = H.create_empty () ;
        genealogy = G.Intbl.create 997 ;
      } in
      G.Intbl.add z.genealogy 0
      { G.birth = 0. ; G.death = infinity ; G.parent = 0 ; G.children = [] } ;
      z

    let zero = create_empty ()

    let group_of = T.group_of

    let identified = T.identified

    let copy = T.copy

    let count_all z = H.count_all z.traitable

    let count z y = H.count z.traitable y
    let idents z y = H.idents z.traitable y

    let traitmax z = H.traitmax z.traitable
    let traitmin z = H.traitmin z.traitable
    let traitval z i = H.traitval z.traitable i
    let choose_any rng z = H.choose_any rng z.traitable
    let choose rng z y = H.choose rng z.traitable y
    let new_ident z = H.new_ident z.traitable

    (* FIXME we'd like to prune here, but we have no params *)
    (* pass it in z ? *)
    let add_descent t z i i' x' =
      (* do the Traitable thing *)
      ignore (H.add_descent t z.traitable i i' x') ;
      begin
      match i, i' with
      | None, None -> ()
      | None, Some _ | Some _, None ->
        failwith "Treepopbis.add_descent : only one of i i' identified"
      | Some i, Some i' ->
        let pind = G.Intbl.find z.genealogy i in
        if t > pind.G.death then
          invalid_arg "Treepopbis.add_descent : dead individual"
        else begin
          G.Intbl.replace z.genealogy i
          { pind with G.children = i' :: pind.G.children } ;
          G.Intbl.add z.genealogy i'
          { G.birth = t ; G.death = infinity ; G.parent = i ; G.children = [] }
        end ;
      end ;
      z

    let remove t z i x =
      (* do the Traitable thing *)
      ignore (H.remove t z.traitable i x) ;
      begin
      match i with
      | None -> ()
      | Some i ->
        let ind = G.Intbl.find z.genealogy i in
        if t > ind.G.death then
          invalid_arg "Treepopbis.remove : dead individual" ;
        G.Intbl.replace z.genealogy i { ind with G.death = t } ;
        (* Note that we don't remove from z.traitidents :
         * we want to have all the refs at the end *)
      end ;
      z
  end


(* FIXME need some kind of output *)
(* might move descent to tree and have a function over there *)

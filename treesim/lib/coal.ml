
(*
 * we simulate a system,
 * sample that system however we want,
 * then want to reconstruct a genalogy 
 * we consider that this doesn't bother itself with simulating
 * the initial system, but will ask for a function giving coalescent rate
 * (eval float cadlag for instance)
 * can we integrate using Poisson if we require that it be a cadlag ?
 * or is it cheating (if it comes from a deterministic run ?
 *)

module type SYSTEM =
  sig
    (* for now, no trait -> no migration to simplify *)
    type hidden
    type param
    val coal_rate : param -> hidden -> float
    (** random continuous sampling of individuals *)
    val sample_rate : param -> hidden -> float
  end


(* or not SYSTEM ? *)
module Make (S : SYSTEM) =
  struct
    (* events are coalescences and mutations *)
    (* need to reread paper *)
    module T = Tree.Timed.Make (Tree.Label.WithInt (Tree.Timed.State))
    module Tset = Set.Make(struct
      type t = T.t
      let compare = compare
    end)
    type param = S.param
    type t = {
      next : int ;
      size : int ;
      pop : Tset.t ;
    }
    type aux = int
    type hidden = S.hidden

    let tset_choose n tset =
      let i = Random.int n in
      let f_choose x (j, xo) =
        match xo with
        | Some _ -> (j, xo)
        | None ->
          match j with
          | 0 -> (j, Some x)
          | _ -> (j - 1, xo)
      in
      match Tset.fold f_choose tset (i, None) with
      | _, None -> failwith "bad random int ?"
      | _, Some x -> x

    let tset_choose_pair n tset =
      let x = tset_choose n tset in
      let tset' = Tset.remove x tset in
      let x' = tset_choose (n - 1) tset' in
      (x, x')

    let coal_rate par t y z =
      float z.size *. float (z.size - 1) *. (S.coal_rate par y)

    let coal_modif par rng (t : float) y (z : t) =
      (* choose a pair in z.pop how ? *)
      let x, x' = tset_choose_pair z.size z.pop in
      (* should be tf -. t *)
      let coal = T.binode (~-. t, ~-1) x x' in
      let nset = (Tset.add coal) @@ (Tset.remove x') @@ (Tset.remove x) @@ (z.pop) in
      { z with size = z.size - 1 ;
               pop = nset }


    let sample_rate par t y z =
      (* I'm pretty sure this is not enough for deterministic sample times ? *)
      S.sample_rate par y

    let sample_modif par rng t y z =
      (* should be tf -. t *)
      let lf = T.leaf (~-. t, z.next) in
      let npop = Tset.add lf z.pop in
      { next = z.next + 1 ;
        size = z.size + 1 ;
        pop = npop }

    let evl = [ (coal_rate, coal_modif) ; (sample_rate, sample_modif) ]
               
    let auxf ?(auxi=0) par t z = z.size 
  end

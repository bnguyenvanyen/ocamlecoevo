
type path = Tree.Path.t

module D = Tree.Descent

module Make (T : Pop.Sig.TRAIT) :
  (Pop.Sig.POP with type trait = T.t
                and type countable = T.x
                and type ident = int
                and type t = (T.t, T.x) Pop.Traitable.t * T.t D.t) =
  struct
    module H = Pop.Traitable.Make (T)
   
    type trait = T.t
    type countable = T.x
    (* probably path for ident ? *)
    (* FIXME might put ident in ancestry instead of trait *)
    type ident = int
    type t = H.t * trait D.t

    let create_empty () =
      H.create_empty (), {
        D.alive = D.Intbl.create 997 ;
        D.ancestry = D.Pathtbl.create 997 ;
        D.maxtree = 0 ;
      }

    let zero = create_empty ()

    let countable_of = T.countable_of

    let copy = T.copy

    let count_all (z, _) = H.count_all z

    let count (z, _) y = H.count z y
    let group (z, _) y = H.group z y

    let traitmax (z, _) = H.traitmax z
    let traitmin (z, _) = H.traitmin z
    let traitval (z, _) i = H.traitval z i
    let choose_any rng (z, _) = H.choose_any rng z
    let choose rng (z, _) y = H.choose rng z y
    let new_ident (z, _) = H.new_ident z
    
    let add_root t (z, desc) i' x' =
      let z = H.add_root t z i' x' in
      let n = desc.D.maxtree + 1 in
      let p' = Tree.Path.End n in
      (* record time of planting *)
      D.Pathtbl.add desc.D.ancestry p' (t, x') ;
      (* then the alive path is *)
      let pf' = Tree.Path.Forward p' in
      D.Intbl.add desc.D.alive i' pf' ;
      desc.D.maxtree <- n ;
      (z, desc)


    let add_descent t (z, desc) i i' x' =
      let z = H.add_descent t z i i' x' in
      let x = H.traitval z i in
      let p = D.Intbl.find desc.D.alive i in
      (* parent on the left (but right going up) *)
      let pl = Tree.Path.Right p in
      (* child on the right (but left going up) *)
      let pr = Tree.Path.Left p in
      (* replace parent path *)
      D.Intbl.replace desc.D.alive i pl ;
      (* add child path *)
      D.Intbl.add desc.D.alive i' pr ;
      (* add now discarded path p to descent *)
      D.Pathtbl.add desc.D.ancestry p (t, x) ;
      (z, desc)

    let remove t (z, desc) i =
      let z = H.remove t z i in
      let x = H.traitval z i in
      let p = D.Intbl.find desc.D.alive i in
      (* kill path *)
      D.Intbl.remove desc.D.alive i ;
      (* add path to history *)
      D.Pathtbl.add desc.D.ancestry p (t, x) ;
      (z, desc)
  end


(* FIXME need some kind of output *)
(* might move descent to tree and have a function over there *)

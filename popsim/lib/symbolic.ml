open Pop.Sig
open Popsim__Sig

module U = Util

(* Basically we want something a bit less specific than we have in Zen so far :
 * - no hypothesis about what trait is
 * - on the side that relates to trait, we can keep it non-symbolic
 *   (user-supplied functions) 
 *   so we get rid of dist and of some annoying things in trait.
 * - we still need a way to symbolize functions on the pop :
 *   rates, probas, and modifs,
 *   with also indiv choosing put into the mix. *)

module Make (I : IMPL) :
  (SYMBOLIC with type 'a value = 'a I.value
             and type 'a group = 'a I.group
             and type 'a trait = 'a I.trait
             and module Pop = I.Pop) =
  struct
    include I


    (* So before modif, we need a way to formulate
     * proba,
     * classical "transformations"
     * also proba depending on typically indivs (some subpart of the state).
     * Do we have room to do that ?
     * Doesn't seem authorized in Sim.Symbolic so far *)

    (* we need a way to handle dists in Prm...
     * As it stands to draw a normal variable for instance,
     * you can reuse u yet again to take its inverse by the cdf
     * So it's ok if the user specifies a function that uses the pt,
     * but it won't be the same as for Rng, so how to do that at this point ? *)

    module Rate =
      struct
        (* FIXME need a way to look at stored maxima and stuff *)
        type _ t =
          | Eval :
              ('a -> 'b) t * 'a t ->
                'b t
          | Lift_left :
              'a t ->
                ('b -> 'a) t
          | Lift_right :
              ('a -> 'b) t ->
                ('a -> 'c -> 'b) t
          | Lift_fun :
              ('a -> 'b) t ->
                (('c -> 'a) -> ('c -> 'b)) t
          | Lift_double :
              ('a -> 'b -> 'c) t ->
                (('d -> 'a) -> ('d -> 'b) -> ('d -> 'c)) t
          | Constant :
              'a value ->
                'a t
          | Count :
              [`Id of id group | `Nonid of nonid group] ->
                (Pop.t -> 'b U.posint) t
          | Max_score :
              'a U.posint value ->
                (Pop.t -> 'b U.pos) t
          | Fun :
              ('a U.anypos -> 'b U.pos) ->
                ('a U.anypos -> 'b U.pos) t
          | Time :
                ('a U.anypos -> 'b U.pos) t
          | Float_of_int :
                (U.anyposint -> 'b U.pos) t
          | Add_int :
                (U.anyposint -> U.anyposint -> 'c U.posint) t
          | Mul_int :
                (U.anyposint -> U.anyposint -> 'c U.posint) t
          | Add :
                ('a U.anypos -> 'b U.anypos -> 'c U.pos) t
          | Mul :
                ('a U.anypos -> 'b U.anypos -> 'c U.pos) t
          | Div :
                ('a U.anypos -> 'b U.anypos -> 'c U.pos) t
          | Pow :
              U.anyposint value ->
                ('b U.anypos -> 'c U.pos) t

      end

    module Dist =
      struct
        type _ t =
          | Eval :
              ('a -> 'b) t * 'a t ->
                'b t
          (* this might be enough for more complex things,
           * as from ('a -> 'd) you can always take 'b is 'a * 'd *)
          | Chain :
              ('a -> 'b) t * ('b -> 'c) t ->
                ('a -> 'c) t
          (* can 'a be like a function here ? *)
          | Constant :
              'a value ->
                'a t
          | Rng_auto :
              ('a -> 'b) Sim.Ctmjp.Rng_unsymb.Auto.S.Modif.symb ->
                ('a -> 'b) t
          | Rng_timedep :
              ('a -> 'b) Sim.Ctmjp.Rng_unsymb.Timedep.S.Modif.symb ->
                (Sim.Sig.time -> 'a -> 'b) t
          | Unichoice :
                ('a list -> 'a) t
          | Exp :
                ('a U.anypos -> 'b U.pos) t
          | Uniform :
              'a U.anyfloat value * 'b U.anyfloat value ->
                U._float t
          | Poisson :
                ('a U.anypos -> 'b U.posint) t
          | Negbin :
              'a U.anypos value ->
                ('a U.anypos -> 'b U.posint) t
          | Normal :
              'a U.anypos value ->
                ('b U.anyfloat -> U._float) t
          | Lognormal :
              'a U.anypos value ->
                ('b U.anyfloat -> U._float) t
          | Multinormal :
              mat value ->
                (vec -> vec) t
          | Multinormal_std :
              U.anyposint value ->
                vec t
      end


    module Transfo =
      struct
        module Choice =
          struct
            type (_, _) t =
              | And :
                  ('a, 'b) t * ('a, 'c) t ->
                    ('a, 'b * 'c) t
              | Choose :
                  id group ->
                    (Pop.t, id Pop.indiv) t
          end

        (* Forget that it's id / nonid in the return type *)
        module Action =
          struct
            type (_, _) t =
              | And :
                  ('d, 'a -> 'b) t * ('e, 'b -> 'c) t ->
                    ('d * 'e, 'a -> 'c) t
              | Remove :
                    ('a Pop.indiv, Pop.t -> Pop.t) t
              | Remove_from :
                  nonid group ->
                    (Pop.t, Pop.t) t
              | Copy :
                    ('a Pop.indiv, Pop.t -> Pop.t) t
              (* will we be able to provide an implementation for this ? *)
              | Copy_mutate :
                  (id Pop.trait -> id Pop.trait) Dist.t ->
                    (id Pop.indiv, Pop.t -> Pop.t) t
              | New_id :
                  id Pop.trait Dist.t ->
                    (Pop.t, Pop.t) t
              | New_from :
                  nonid group ->
                    (Pop.t, Pop.t) t
          end

        type ('a, 'b) t =
          | Choice of ('a, 'b) Choice.t
          | Action of ('a, 'b) Action.t

        module C = Choice
        module A = Action
      end

    module T = Transfo


    module Proba =
      struct
        (* FIXME we need a way to depend on indivs (a tuple of them) *)
        type _ t =
          | Eval :
              ('a -> 'b) t * 'a t ->
                'b t
          | Lift_left :
              'a t ->
                ('b -> 'a) t
          | Lift_right :
              ('a -> 'b) t ->
                ('a -> 'c -> 'b) t
          | Lift_fun :
              ('a -> 'b) t ->
                (('c -> 'a) -> ('c -> 'b)) t
          | Lift_double :
              ('a -> 'b -> 'c) t ->
                (('d -> 'a) -> ('d -> 'b) -> ('d -> 'c)) t
          | Constant :
              U.anyproba value ->
                'a U.proba t
          | Count :
              [`Id of id group | `Nonid of nonid group] ->
                (Pop.t -> 'b U.posint) t
          | Rel_max_score :
              'a U.posint value ->
                (id Pop.indiv -> Pop.t -> 'c U.proba) t
          | Time :
                ('a U.anypos -> 'b U.pos) t
          | Float_of_int :
                (U.anyposint -> 'b U.pos) t
          | Add_int :
                (U.anyposint -> U.anyposint -> 'c U.posint) t
          | Mul_int :
                (U.anyposint -> U.anyposint -> 'c U.posint) t
          | Add :
                ('a U.anypos -> 'b U.anypos -> 'c U.pos) t
          | Mul :
                (U.anyproba -> U.anyproba -> 'c U.proba) t
          | Prop :
                ('a U.anypos -> 'b U.anypos -> 'c U.proba) t
      end


    (* For now I go the ugly and easy way :
     * two modules Auto and Timedep,
     * and two corresponding Impl and Sim.Symbolic instances *)
    module Auto =
      struct
        module Impl =
          struct
            type ('a, 'b) rate = ('a -> 'b U.pos) Rate.t
            type ('a, 'b) transformation = ('a, 'b) Transfo.t
            type 'a proba = ('a -> U.anyproba) Proba.t
            type ('a, 'b) lift_proba = ('a -> 'b -> U.anyproba) Proba.t
          end

        module S = Sim.Symbolic.Make (Impl)
        
        let rec simplify_action :
           type a b c d. (d, a) T.C.t -> (a, b -> c) T.A.t ->
            (b, d) eq * (b, c) eq * (b -> c) S.Modif.symb =
          (* do we need the eq in here ? *)
          fun choice action ->
            match choice, action with
            | T.C.And (c, c'), T.A.And (a, a') ->
                (match simplify_action c a, simplify_action c' a' with
                 (Eq, Eq, smpa), (Eq, Eq, smpa') ->
                   Eq, Eq, S.Modif.Chain (smpa, smpa'))
            | T.C.Choose g, T.A.Remove ->
                Eq, Eq, S.Modif.Transfo (T.Action (T.A.Remove_from (to_nonid g)))
            | T.C.Choose g, T.A.Copy ->
                Eq, Eq, S.Modif.Transfo (T.Action (T.A.New_from (to_nonid g)))
            | T.C.Choose g, T.A.Copy_mutate _ ->
                invalid_arg "Not_allowed"
            (* other branches are dead *)

        let rec simplify_lift :
          type a b c d. (d, a) T.C.t -> (a -> b -> c) S.Modif.symb ->
            (b, d) eq * (b, c) eq * (b -> c) S.Modif.symb = S.Modif.(
          (* do we need the eq in here ? *)
          let fail () = invalid_arg "Not_allowed" in
          fun choice ->
          function
          (* Descend the Ands *)
          | Transfo (T.Action action) ->
              simplify_action choice action
          | Chain_lifted (e, e') ->
              (* 'choice' doesn't necessarily have the right type
               * for both expressions ?
               * Even with equality type witness ? 
               * yes because after the fact
               * (at exit from simplify_lift, not at entrance) *)
              (match simplify_lift choice e, simplify_lift choice e' with
                 (Eq, Eq, smpl), (Eq, Eq, smpl') ->
                   Eq, Eq, S.Modif.Chain (smpl, smpl))
          (* we want to kill those cases *)
          | Erase_lifted _ ->
              (* Erase_lifted should be illegal because [p] can depend on
               * the drawn indivs etc ('b in the Map),
               * and we don't know what to do with it to get rid of it *)
              fail ()
          (* probably dead ? *)
          | Chain _ ->
              (* this looks dead as in
               * Chain : ('a -> 'b) symb * ('b -> 'c) symb -> ('a -> 'c) symb,
               * we know that
               * 'a should be (indiv * (... * indiv)),
               * 'c should be (pop -> pop),
               * and through Transfo we only know how to build (indiv * ...) -> pop,
               * but not (pop -> (pop -> pop)) *)
              assert false
          (* dead branches (everything must be at least ('b -> 'a -> 'a) t *)
          | Identity ->
              assert false
          | Bernoulli_lifted _ ->
              assert false
          | Erase _ ->
              assert false
          | Map _ ->
              assert false
          | Transfo (T.Choice _) ->
              (* nice surprise : refutable *)
              .
        )

        (* from a 'nonid' modif, rewrite some common constructs simpler *)
        let rec simplify :
          type a b. (a -> b) S.Modif.symb -> (a, b) eq * (a -> b) S.Modif.symb = S.Modif.(
          let fail () = failwith "Not_implemented" in
          function
          (* no choose can remain *)
          | Map (Transfo (T.Choice choice), e) ->
              (* do we get the eq from simplify_lift or do we put it in here ? *)
              let _, eq, smpl = simplify_lift choice e in
              eq, smpl
          (* good cases *)
          | (Transfo (T.Action (T.A.Remove_from _)) as e) ->
              Eq, e
          | (Transfo (T.Action (T.A.New_from _)) as e) ->
              Eq, e
          (* ok (but weird) case *)
          | (Map (Identity, _) as e) ->
              Eq, e
          (* bad (but possible) cases *)
          | Map (Transfo (T.Action _), _) ->
              fail ()
          | Map (Bernoulli _, _) ->
              fail ()
          | Map (_, _) ->
              fail ()
          | Transfo (T.Action (T.A.New_id _)) ->
              fail ()
          (* branches we want to kill
           * (and will actually already be dead from the outside,
           * since it will be typed as (pop -> pop) *)
          | Transfo (T.Choice _) ->
              assert false
          | Transfo (T.Action T.A.And _) ->
              assert false
          | Transfo (T.Action T.A.Remove) ->
              assert false
          | Transfo (T.Action T.A.Copy) ->
              assert false
          | Transfo (T.Action (T.A.Copy_mutate _)) ->
              assert false
          | Bernoulli_lifted _ ->
              assert false
          | Erase_lifted _ ->
              assert false
          | Chain_lifted _ ->
              assert false
          (* (pop -> pop) != ('a -> bool) *)
          | Bernoulli _ ->
              assert false
          (* anything else stays the same *)
          | (Identity as e) ->
              Eq, e
          | Erase (e, e') ->
              let _, smp' = simplify e' in
              Eq, Erase (e, smp')
          | Chain (e, e') ->
              (* Here we need a proof that both e and e' are (pop -> pop)
               * Could we just change Chain's type to not compose ?
               * Do we ever need anything else ? *)
              (match simplify e, simplify e' with (Eq, smp), (Eq, smp') ->
                 Eq, Chain (smp, smp'))
          | Lift e ->
              let _, smp = simplify e in
              Eq, Lift smp
        )

        
        let simplify_modif e =
          let _, e' = simplify e in
          e'
      end


    module Timedep =
      struct
        module Impl =
          struct
            open Sim.Sig

            type ('a, 'b) rate = (time -> 'a -> 'b U.pos) Rate.t
            type 'a proba = (time -> 'a -> U.anyproba) Proba.t
            type ('a, 'b) lift_proba = (time -> 'a -> 'b -> U.anyproba) Proba.t
            type ('a, 'b) transformation = ('a, 'b) Transfo.t
          end

        module S = Sim.Symbolic.Make (Impl)

        let rec simplify_action :
           type a b c d. (d, a) T.C.t -> (a, b -> c) T.A.t ->
            (b, d) eq * (b, c) eq * (b -> c) S.Modif.symb =
          (* do we need the eq in here ? *)
          fun choice action ->
            match choice, action with
            | T.C.And (c, c'), T.A.And (a, a') ->
                (match simplify_action c a, simplify_action c' a' with
                 (Eq, Eq, smpa), (Eq, Eq, smpa') ->
                   Eq, Eq, S.Modif.Chain (smpa, smpa'))
            | T.C.Choose g, T.A.Remove ->
                Eq, Eq, S.Modif.Transfo (T.Action (T.A.Remove_from (to_nonid g)))
            | T.C.Choose g, T.A.Copy ->
                Eq, Eq, S.Modif.Transfo (T.Action (T.A.New_from (to_nonid g)))
            | T.C.Choose g, T.A.Copy_mutate _ ->
                invalid_arg "Not_allowed"
            (* other branches are dead *)

        let rec simplify_lift :
          type a b c d. (d, a) T.C.t -> (a -> b -> c) S.Modif.symb ->
            (b, d) eq * (b, c) eq * (b -> c) S.Modif.symb = S.Modif.(
          (* do we need the eq in here ? *)
          let fail () = invalid_arg "Not_allowed" in
          fun choice ->
          function
          (* Descend the Ands *)
          | Transfo (T.Action action) ->
              simplify_action choice action
          | Chain_lifted (e, e') ->
              (* 'choice' doesn't necessarily have the right type
               * for both expressions ?
               * Even with equality type witness ? 
               * yes because after the fact
               * (at exit from simplify_lift, not at entrance) *)
              (match simplify_lift choice e, simplify_lift choice e' with
                 (Eq, Eq, smpl), (Eq, Eq, smpl') ->
                   Eq, Eq, S.Modif.Chain (smpl, smpl))
          (* we want to kill those cases *)
          | Erase_lifted _ ->
              (* Erase_lifted should be illegal because [p] can depend on
               * the drawn indivs etc ('b in the Map),
               * and we don't know what to do with it to get rid of it *)
              fail ()
          (* probably dead ? *)
          | Chain _ ->
              (* this looks dead as in
               * Chain : ('a -> 'b) symb * ('b -> 'c) symb -> ('a -> 'c) symb,
               * we know that
               * 'a should be (indiv * (... * indiv)),
               * 'c should be (pop -> pop),
               * and through Transfo we only know how to build (indiv * ...) -> pop,
               * but not (pop -> (pop -> pop)) *)
              assert false
          (* dead branches (everything must be at least ('b -> 'a -> 'a) t *)
          | Identity ->
              assert false
          | Bernoulli_lifted _ ->
              assert false
          | Erase _ ->
              assert false
          | Map _ ->
              assert false
          | Transfo (T.Choice _) ->
              (* nice surprise : refutable *)
              .
        )

        (* from a 'nonid' modif, rewrite some common constructs simpler *)
        let rec simplify :
          type a b. (a -> b) S.Modif.symb -> (a, b) eq * (a -> b) S.Modif.symb = S.Modif.(
          let fail () = failwith "Not_implemented" in
          function
          (* no choose can remain *)
          | Map (Transfo (T.Choice choice), e) ->
              (* do we get the eq from simplify_lift or do we put it in here ? *)
              let _, eq, smpl = simplify_lift choice e in
              eq, smpl
          (* good cases *)
          | (Transfo (T.Action (T.A.Remove_from _)) as e) ->
              Eq, e
          | (Transfo (T.Action (T.A.New_from _)) as e) ->
              Eq, e
          (* ok (but weird) case *)
          | (Map (Identity, _) as e) ->
              Eq, e
          (* bad (but possible) cases *)
          | Map (Transfo (T.Action _), _) ->
              fail ()
          | Map (Bernoulli _, _) ->
              fail ()
          | Map (_, _) ->
              fail ()
          | Transfo (T.Action (T.A.New_id _)) ->
              fail ()
          (* branches we want to kill
           * (and will actually already be dead from the outside,
           * since it will be typed as (pop -> pop) *)
          | Transfo (T.Choice _) ->
              assert false
          | Transfo (T.Action T.A.And _) ->
              assert false
          | Transfo (T.Action T.A.Remove) ->
              assert false
          | Transfo (T.Action T.A.Copy) ->
              assert false
          | Transfo (T.Action (T.A.Copy_mutate _)) ->
              assert false
          | Bernoulli_lifted _ ->
              assert false
          | Erase_lifted _ ->
              assert false
          | Chain_lifted _ ->
              assert false
          (* (pop -> pop) != ('a -> bool) *)
          | Bernoulli _ ->
              assert false
          (* anything else stays the same *)
          | (Identity as e) ->
              Eq, e
          | Erase (e, e') ->
              let _, smp' = simplify e' in
              Eq, Erase (e, smp')
          | Chain (e, e') ->
              (* Here we need a proof that both e and e' are (pop -> pop)
               * Could we just change Chain's type to not compose ?
               * Do we ever need anything else ? *)
              (match simplify e, simplify e' with (Eq, smp), (Eq, smp') ->
                 Eq, Chain (smp, smp'))
          | Lift e ->
              let _, smp = simplify e in
              Eq, Lift smp
        )

        
        let simplify_modif e =
          let _, e' = simplify e in
          e'
      end


  end

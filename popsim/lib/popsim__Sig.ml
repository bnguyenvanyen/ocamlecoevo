module U = Util


type (_, _) eq = Eq : ('a, 'a) eq

type vec = Lacaml.D.Vec.t
type mat = Lacaml.D.Mat.t


module type POPSIG =
  sig
    type 'a trait
    type 'a group

    module I : (Pop.Sig.INDIV with type 'a trait = 'a trait)

    type 'a indiv = 'a I.t

    type t
  end


module type IMPL =
  sig
    open Pop.Sig (* id nonid ... *)

    (* Can we put variance annotations ?
     * (especially I'm pretty sure that value should be covariant) *)
    type 'a value

    (* do we also need positive / count ? *)

    type 'a trait
    type 'a group

    (* place-holder for now *)
    val name_value : string -> 'a value
    val register_value : 'a -> 'a value

    val eval_value : 'a value -> 'a
    val prove_nonid : 'a group -> ('a, nonid) eq
    val to_nonid : 'a group -> nonid group
    (* there should not really be a trait for nonid groups *)
    val group_to_trait : nonid group -> nonid trait

    (* it's in FLATPOP too *)
    val group_to_int : nonid group -> int
    val int_to_group : int -> nonid group
    val nb_nonid_groups : int

    module Pop : (POPSIG with type 'a group = 'a group
                          and type 'a trait = 'a trait)
  end


module type SYMBOLIC =
  sig
    open Pop.Sig

    include IMPL

    (* How to allow for time dependency ? *)
    module Rate :
      sig
        type _ t =
          | Eval :
              ('a -> 'b) t * 'a t ->
                'b t
          | Lift_left :
              'a t ->
                ('b -> 'a) t
          | Lift_right :
              ('a -> 'b) t ->
                ('a -> 'c -> 'b) t
          | Lift_fun :
              ('a -> 'b) t ->
                (('c -> 'a) -> ('c -> 'b)) t
          | Lift_double :
              ('a -> 'b -> 'c) t ->
                (('d -> 'a) -> ('d -> 'b) -> ('d -> 'c)) t
          (* this might be too general -- only allow pos values ? *)
          | Constant :
              'a value ->
                'a t
          | Count :
              [`Id of id group | `Nonid of nonid group] ->
                (Pop.t -> 'b U.posint) t
          | Max_score :
              'a U.posint value ->
                (Pop.t -> 'b U.pos) t
          | Fun :
              ('a U.anypos -> 'b U.pos) ->
                ('a U.anypos -> 'b U.pos) t
          | Time :
                ('a U.anypos -> 'b U.pos) t
          | Float_of_int :
                (U.anyposint -> 'b U.pos) t
          | Add_int :
                (U.anyposint -> U.anyposint -> 'c U.posint) t
          | Mul_int :
                (U.anyposint -> U.anyposint -> 'c U.posint) t
          | Add :
                ('a U.anypos -> 'b U.anypos -> 'c U.pos) t
          | Mul :
                ('a U.anypos -> 'b U.anypos -> 'c U.pos) t
          | Div :
                ('a U.anypos -> 'b U.anypos -> 'c U.pos) t
          | Pow :
              U.anyposint value ->
                ('b U.anypos -> 'c U.pos) t
      end

    (* FIXME Allow for supplying dists through Ctmjp symbolic exprs *)
    module Dist :
      sig
        type _ t =
          | Eval :
              ('a -> 'b) t * 'a t ->
                'b t
          (* this might be enough for more complex things,
           * as from ('a -> 'd) you can always take 'b is 'a * 'd *)
          | Chain :
              ('a -> 'b) t * ('b -> 'c) t ->
                ('a -> 'c) t
          (* can 'a be like a function here ? *)
          | Constant :
              'a value ->
                'a t
          (* When we have an implementation for Rng
           * (can't be used with Prm) *)
          | Rng_auto :
              ('a -> 'b) Sim.Ctmjp.Rng_unsymb.Auto.S.Modif.symb ->
                ('a -> 'b) t
          | Rng_timedep :
              ('a -> 'b) Sim.Ctmjp.Rng_unsymb.Timedep.S.Modif.symb ->
                (Sim.Sig.time -> 'a -> 'b) t
          | Unichoice :
                ('a list -> 'a) t
          | Exp :
                ('a U.anypos -> 'b U.pos) t
          | Uniform :
              'a U.anyfloat value * 'b U.anyfloat value ->
                U._float t
          | Poisson :
                ('a U.anypos -> 'b U.posint) t
          | Negbin :
              'a U.anypos value ->
                ('a U.anypos -> 'b U.posint) t
          | Normal :
              'a U.anypos value ->
                ('b U.anyfloat -> U._float) t
          | Lognormal :
              'a U.anypos value ->
                ('b U.anyfloat -> U._float) t
          | Multinormal :
              mat value ->
                (vec -> vec) t
          | Multinormal_std :
              U.anyposint value ->
                vec t
      end

    module Transfo :
      sig
        module Choice :
          sig
            type (_, _) t =
              | And :
                  ('a, 'b) t * ('a, 'c) t ->
                    ('a, 'b * 'c) t
              | Choose :
                  id group ->
                    (Pop.t, id Pop.indiv) t
          end

        module Action :
          sig
            type (_, _) t =
              | And :
                  ('d, 'a -> 'b) t * ('e, 'b -> 'c) t ->
                    ('d * 'e, 'a -> 'c) t
              | Remove :
                    ('a Pop.indiv, Pop.t -> Pop.t) t
              | Remove_from :
                  nonid group ->
                    (Pop.t, Pop.t) t
              | Copy :
                    ('a Pop.indiv, Pop.t -> Pop.t) t
              (* will we be able to provide an implementation for this ? *)
              | Copy_mutate :
                  (id Pop.trait -> id Pop.trait) Dist.t ->
                    (id Pop.indiv, Pop.t -> Pop.t) t
              | New_id :
                  id Pop.trait Dist.t ->
                    (Pop.t, Pop.t) t
              | New_from :
                  nonid group ->
                    (Pop.t, Pop.t) t
          end

        type ('a, 'b) t =
          | Choice of ('a, 'b) Choice.t
          | Action of ('a, 'b) Action.t

        module C = Choice
        module A = Action
      end

    module T = Transfo

    module Proba :
      sig
        type _ t =
          | Eval :
              ('a -> 'b) t * 'a t ->
                'b t
          | Lift_left :
              'a t ->
                ('b -> 'a) t
          | Lift_right :
              ('a -> 'b) t ->
                ('a -> 'c -> 'b) t
          | Lift_fun :
              ('a -> 'b) t ->
                (('c -> 'a) -> ('c -> 'b)) t
          | Lift_double :
              ('a -> 'b -> 'c) t ->
                (('d -> 'a) -> ('d -> 'b) -> ('d -> 'c)) t
          | Constant :
              U.anyproba value ->
                'a U.proba t
          | Count :
              [`Id of id group | `Nonid of nonid group] ->
                (Pop.t -> 'b U.posint) t
          (* we'd like to name components *)
          | Rel_max_score :
              'a U.posint value ->
                (id Pop.indiv -> Pop.t -> 'c U.proba) t
          (* maybe for now we forbid this
          | Fun :
              ('a -> Pop.t -> 'b U.proba) ->
                ('a -> Pop.t -> 'b U.proba) t
          *)
          | Time :
                ('a U.anypos -> 'b U.pos) t
          | Float_of_int :
                (U.anyposint -> 'b U.pos) t
          | Add_int :
                (U.anyposint -> U.anyposint -> 'c U.posint) t
          | Mul_int :
                (U.anyposint -> U.anyposint -> 'c U.posint) t
          | Add :
                ('a U.anypos -> 'b U.anypos -> 'c U.pos) t
          | Mul :
                (U.anyproba -> U.anyproba -> 'c U.proba) t
          | Prop :
                ('a U.anypos -> 'b U.anypos -> 'c U.proba) t
      end


    module Auto :
      sig
        module S : (Sim.Sig.SYMBOLIC
          with type ('a, 'b) rate = ('a -> 'b U.pos) Rate.t
           and type 'a proba = ('a -> U.anyproba) Proba.t
           and type ('a, 'b) lift_proba = ('a -> 'b -> U.anyproba) Proba.t
           and type ('a, 'b) transformation = ('a, 'b) Transfo.t
        )

        (* can raise *)
        val simplify_modif : Pop.t S.Modif.t -> Pop.t S.Modif.t
      end

    module Timedep :
      sig
        open Sim.Sig (* time *)

        module S : (Sim.Sig.SYMBOLIC
          with type ('a, 'b) rate = (time -> 'a -> 'b U.pos) Rate.t
           and type 'a proba = (time -> 'a -> U.anyproba) Proba.t
           and type ('a, 'b) lift_proba = (time -> 'a -> 'b -> U.anyproba) Proba.t
           and type ('a, 'b) transformation = ('a, 'b) Transfo.t
        )

        (* can raise *)
        val simplify_modif : Pop.t S.Modif.t -> Pop.t S.Modif.t
      end
  end



module type SYMBOLIC_FLAT =
  sig
    module Pop :
     sig
       include POPSIG

       include (Pop.Sig.POP_FLAT with type 'a group := 'a group
                                  and type t := t)

     end

    include (SYMBOLIC with type 'a group = 'a Pop.group
                       and type 'a trait = 'a Pop.trait
                       and module Pop := Pop)
  end


module type SYMBOLIC_CONT =
  sig
    module Pop : (POPSIG with type t = vec)

    include (SYMBOLIC with type 'a group = 'a Pop.group
                       and type 'a trait = 'a Pop.trait
                       and module Pop := Pop)
  end


(* FIXME add a signature that has scores / compares but doesn't have add / remove *)

module type SYMBOLIC_AUTO =
  sig
    module Pop : Pop.Sig.POP_WITH_MAX

    include (SYMBOLIC with type 'a group = 'a Pop.group
                       and type 'a trait = 'a Pop.trait
                       and module Pop := Pop)
  end


module type SYMBOLIC_TIMEDEP =
  sig
    module Pop : Pop.Sig.POP_COMPLEX

    include (SYMBOLIC with type 'a group = 'a Pop.group
                       and type 'a trait = 'a Pop.trait
                       and module Pop := Pop)
  end

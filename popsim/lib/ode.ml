open Popsim__Sig

module Vec = Lacaml.D.Vec

module U = Util


module Make (Sym : SYMBOLIC_CONT) =
  struct
    open Sym

    (* Push 'Eval (Lift_fun Float_of_int, e)' inside e,
     * until we can get rid of it (in front of 'Count') *)
    let rec floatify :
      type a. a Rate.t -> a Rate.t  = Rate.(
      function
      (* Auto cases (the only ones) *)
      (* base cases *)
      | (Eval (Lift_fun Float_of_int,
          Count (`Nonid _))) as e ->
          e
      | (Eval (Lift_fun Float_of_int,
          Constant _)) as e ->
          e
      | (Eval (Lift_fun Float_of_int,
          (Lift_fun (Constant _)))) as e ->
          e
      (* commute op and float_of_int *)
      | Eval (Lift_fun Float_of_int,
          Eval (Eval (Lift_double Add_int, e), e')
        ) ->
          Eval (
            Eval (
              Lift_double Add,
              floatify (Eval (Lift_fun Float_of_int, e))
            ),
            floatify (Eval (Lift_fun Float_of_int, e'))
          )
      | Eval (Lift_fun Float_of_int,
          Eval (Eval (Lift_double Mul_int, e), e')
        ) ->
          Eval (
            Eval (
              Lift_double Mul,
              floatify (Eval (Lift_fun Float_of_int, e))
            ),
            floatify (Eval (Lift_fun Float_of_int, e'))
          )
      (* And all other cases do nothing *)
      | e ->
          e
    )

    let rec eval_rate :
      type a. a Rate.t -> a = Rate.(
      let fail () = invalid_arg "Not_allowed" in
      function
      | Eval (Lift_fun Float_of_int, Count (`Nonid group)) ->
          let i = group_to_int group in
          (* change type of count ? *)
          (* we need transparent operations on vec with U types *)
          (fun z -> U.Float.of_float_unsafe (z.{i}))
      | Eval (ef, ex) ->
          let f = eval_rate ef in
          let x = eval_rate ex in
          f x
      | Lift_left ex ->
          let x = eval_rate ex in
          (fun _ -> x)
      | Lift_right ef ->
          let f = eval_rate ef in
          (fun x _ -> f x)
      | Lift_fun ef ->
          let f = eval_rate ef in
          (fun g c -> f (g c))
      | Lift_double ef ->
          let f = eval_rate ef in
          (fun g g' d -> f (g d) (g' d))
      | Constant ex ->
          eval_value ex
      | Fun f ->
          f
      | Time ->
          U.Float.Pos.narrow
      | Float_of_int ->
          U.Int.Pos.to_float
      | Add_int ->
          U.Int.Pos.add
      | Mul_int ->
          U.Int.Pos.mul
      | Add ->
          U.Float.Pos.add
      | Mul ->
          U.Float.Pos.mul
      | Div ->
          U.Float.Pos.div
      | Pow ei ->
          let i = U.Int.to_int (eval_value ei) in
          (fun x -> U.Float.Pos.pow x i)
      | Count _ ->
          fail ()
      | Max_score _ ->
          fail ()
    )



    let eval_action :
      type a b. (a, b) T.Action.t -> (a, vec) eq * (b, vec) eq * vec = T.A.(
      let fail () = invalid_arg "Not_allowed" in
      function
      | Remove_from g ->
          let i = group_to_int g in
          let v = Vec.make0 nb_nonid_groups in
          v.{i} <- ~-. 1. ;
          Eq, Eq, v
      | New_from g ->
          let i = group_to_int g in
          let v = Vec.make0 nb_nonid_groups in
          v.{i} <- 1. ;
          Eq, Eq, v
      | And _ ->
          fail ()
      | Remove ->
          fail ()
      | Copy ->
          fail ()
      | Copy_mutate _ ->
          fail ()
      | New_id _ ->
          fail ()
    )


    let rec eval_proba :
      type a. a Proba.t -> a = Proba.(
      let fail () = invalid_arg "Not_allowed" in
      function
      (* To allow combinations of Count,
       * we can go down another function when we hit Float_of_int *)
      | Eval (Lift_fun Float_of_int, Count (`Nonid group)) ->
          let i = group_to_int group in
          (* change type of count ? *)
          (fun z -> U.Float.of_float_unsafe (z.{i}))
      | Eval (ef, ex) ->
          let f = eval_proba ef in
          let x = eval_proba ex in
          f x
      | Lift_left ep ->
          let p = eval_proba ep in
          (fun _ -> p)
      | Lift_right ef ->
          let f = eval_proba ef in
          (fun x _ -> f x)
      | Lift_fun ef ->
          let f = eval_proba ef in
          (fun g c -> f (g c))
      | Lift_double ef ->
          let f = eval_proba ef in
          (fun g g' d -> f (g d) (g' d))
      | Constant pv ->
          U.Float.Proba.narrow (eval_value pv)
      | Float_of_int ->
          U.Int.Pos.to_float
      | Time ->
          U.Float.Pos.narrow
      | Add_int ->
          U.Int.Pos.add
      | Mul_int ->
          U.Int.Pos.mul
      | Add ->
          U.Float.Pos.add
      | Mul ->
          U.Float.Proba.mul
      | Prop ->
          (fun a b -> let _, p, _ = U.Float.Pos.proportions a b in p)
      | Count _ ->
          fail ()
      | Rel_max_score _ ->
          fail ()
    )


    module Auto =
      struct
        module S = Auto.S
        module S' = Sim.Ode.Unsymb.Auto.S

        let rec eval :
          type a. a S.Modif.symb -> a S'.Modif.symb =
          let fail () = invalid_arg "Not_allowed" in
          function 
          | S.Modif.Identity ->
              S'.Modif.Identity
          | S.Modif.Transfo (Transfo.Action a) ->
              (match eval_action a with (Eq, Eq, v) ->
                 S'.Modif.Transfo (Sim.Ode.Impl.Auto.Vec v)
              )
          | S.Modif.Bernoulli p ->
              S'.Modif.Bernoulli (eval_proba p)
          | S.Modif.Erase (pred, e) ->
              S'.Modif.Erase (eval pred, eval e)
          | S.Modif.Chain (e, e') ->
              S'.Modif.Chain (eval e, eval e')
          | S.Modif.Lift e ->
              S'.Modif.Lift (eval e)
          | S.Modif.Transfo (Transfo.Choice _) ->
              fail ()
          | S.Modif.Map _ ->
              fail ()
          | S.Modif.Bernoulli_lifted _ ->
              fail ()
          | S.Modif.Erase_lifted _ ->
              fail ()
          | S.Modif.Chain_lifted _ ->
              fail ()

        let field (S.Colors l) =
          let colors = S'.Colors (List.map (fun (r, sm) ->
            (eval_rate (floatify r)), eval sm) l)
          in Sim.Ode.Unsymb.Auto.field colors
      end


    module Timedep =
      struct
        module S = Timedep.S
        module S' = Sim.Ode.Unsymb.Timedep.S

        let rec eval :
          type a. a S.Modif.symb -> a S'.Modif.symb =
          let fail () = invalid_arg "Not_allowed" in
          function 
          | S.Modif.Identity ->
              S'.Modif.Identity
          | S.Modif.Transfo (Transfo.Action a) ->
              (match eval_action a with (Eq, Eq, v) ->
                 S'.Modif.Transfo (Sim.Ode.Impl.Timedep.Vec v)
              )
          (* The constructor S.Modif.Bernoulli is too restrictive *)
          | S.Modif.Bernoulli p ->
              (* it looks like a pretty dumb type bug... *)
              S'.Modif.Bernoulli (eval_proba p)
          | S.Modif.Erase (pred, e) ->
              S'.Modif.Erase (eval pred, eval e)
          | S.Modif.Chain (e, e') ->
              S'.Modif.Chain (eval e, eval e')
          | S.Modif.Lift e ->
              S'.Modif.Lift (eval e)
          | S.Modif.Transfo (Transfo.Choice _) ->
              fail ()
          | S.Modif.Map _ ->
              fail ()
          | S.Modif.Bernoulli_lifted _ ->
              fail ()
          | S.Modif.Erase_lifted _ ->
              fail ()
          | S.Modif.Chain_lifted _ ->
              fail ()

        let field (S.Colors l) =
          let colors = S'.Colors (List.map (fun (r, sm) ->
            (eval_rate (floatify r)), eval sm) l)
          in Sim.Ode.Unsymb.Timedep.field colors
        end
  end

open Sig


let%expect_test _ =
  let rng = U.rng None in
  let cov = U.mat_to_cov (Lac.Mat.make0 2 2) in
  let u = Lac.Vec.make0 2 in
  let x = Lac.Vec.make0 2 in
  U.rand_multi_normal_center ~rng ~u ~cov x ;
  Printf.printf "%f %f" (abs_float x.{1}) (abs_float x.{2}) ;
  [%expect{| 0.000000 0.000000 |}]


let%expect_test _ =
  let rng = U.rng (Some 0) in
  let u = Lac.Vec.make0 2 in
  let x = Lac.Vec.make0 2 in
  let cov = U.mat_to_cov (
    Lac.Mat.of_array [| [| 1. ; 0. |] ; [| 1. ; 0. |] |]
  ) in
  U.rand_multi_normal_center ~rng ~u ~cov x ;
  Printf.printf "%f %f" x.{1} x.{2} ;
  [%expect{| 1.857254 1.857254 |}]


(* test the d / logd against numpy *)

(* centered *)
let%expect_test _ =
  (* with scipy.stats :
   * d = multivariate_normal(cov=np.array([[0.7, 0.34], [0.34, 0.6]])) *)
  let cov = ref (U.Cov (
    Lac.Mat.of_array [| [| 0.7 ; 0.34 |] ; [| 0.34 ; 0.6 |] |]
  ))
  in
  let v = Lac.Vec.of_array [| 12. ; 18. |] in
  Printf.printf "%.8f" (F.to_float (U.logd_multi_normal_center cov v)) ;
  (* according to scipy.stats :
   * d.logpdf([12, 18]) = -274.43633762558886
   *)
  [%expect{| -274.43633763 |}] ;
  let v' = Lac.Vec.of_array [| 0.1 ; 0.1 |] in
  Printf.printf "%.8f" (F.to_float (U.d_multi_normal_center cov v')) ;
  (* according to scipy.stats :
   * d.pdf([0.1, 0.1]) = 0.2855452671897972
   *)
  [%expect{| 0.28554527 |}]


let%expect_test _ =
  (* with scipy.stats :
   * d = multivariate_normal(
   *   mean=np.array([10., 10.]),
   *   cov=np.array([[0.7, 0.34], [0.34, 0.6]]),
   * )
   *)
  let cov = ref (U.Cov (
    Lac.Mat.of_array [| [| 0.7 ; 0.34 |] ; [| 0.34 ; 0.6 |] |]
  ))
  in
  let mean = Lac.Vec.of_array [| 10. ; 10. |] in
  let v = Lac.Vec.of_array [| 12. ; 18. |] in
  Printf.printf "%.8f" (F.to_float (U.logd_multi_normal mean cov v)) ;
  (* according to scipy.stats : 
   * d.logpdf([12, 18]) = -60.90151502374911
   *)
  [%expect{| -60.90151502 |}]


(* test condition_on *)

(* condition_on on the same value of x we just drew :
 * the value of u should not change *)
let%expect_test _ =
  let m = Lac.Vec.make0 3 in
  let cov = ref (U.Cov (
    Lac.Mat.of_array [|
      [| 0.7 ; 0.34 ; 0.2 |] ;
      [| 0.34 ; 0.6 ; 0.15 |] ;
      [| 0.2 ; 0.15 ; 0.4 |] ;
    |]
  ))
  in
  let rng = U.rng (Some 0) in
  let u = Lac.Vec.make0 3 in
  let x = Lac.Vec.make0 3 in
  let x' = U.rand_multi_normal ~rng ~stop:2 ~m ~cov ~u x in
  Lacaml.Io.pp_fvec Format.std_formatter u ;
  [%expect{|
     1.85725
    0.771469
           0 |}] ;
  U.condition_on ~stop:2 ~cov ~x:x' u ;
  Lacaml.Io.pp_fvec Format.std_formatter u ;
  [%expect{|
     1.85725
    0.771469
           0 |}]


(* test rand_multi_normal_conditional against another implementation *)


let%expect_test _ =
  let m = Lac.Vec.make0 2 in
  let cov = ref (U.Cov (
    Lac.Mat.of_array [| [| 0.7 ; 0.34 |] ; [| 0.34 ; 0.6 |] |]
  ))
  in
  (* each with its own rng, and the results are magically the same *)
  let rng = U.rng (Some 0) in
  let u = Lac.Vec.make0 2 in
  let x = Lac.Vec.make0 2 in
  let x = U.rand_multi_normal_conditional ~rng ~start:2 ~m ~cov ~u x in
  let rng = U.rng (Some 0) in
  let u' = Lac.Vec.make0 2 in
  let x' = Lac.Vec.make0 2 in
  let x' = U.rand_multi_normal_cond_notrick ~rng ~start:2 ~m ~cov ~u:u' x' in
  Lacaml.Io.pp_fvec Format.std_formatter x ;
  [%expect{|
          0
    1.22474 |}] ;
  Lacaml.Io.pp_fvec Format.std_formatter x' ;
  [%expect{|
          0
    1.22474 |}]

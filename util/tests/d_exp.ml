open Sig


let%expect_test _ =
  P.printf "%f\n" (F.to_float (U.d_exp F.zero 1.)) ;
  [%expect{| 0.000000 |}]


let%expect_test _ =
  P.printf "%f\n" (F.to_float (U.d_exp F.one 0.)) ;
  [%expect{| 1.000000 |}]


let%expect_test _ =
  P.printf "%f\n" (F.to_float (U.d_exp F.one (~-. 1.))) ;
  [%expect{| 0.000000 |}]


let%expect_test _ =
  P.printf "%f\n" (F.to_float (U.d_exp F.one 1.)) ;
  [%expect{| 0.367879 |}]



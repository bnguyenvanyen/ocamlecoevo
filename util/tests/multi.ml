open Sig

(* test sym_compute_root *)

(* test syev to understand sym_compute_root *)
let%expect_test _ =
  let m = Lac.Mat.of_array [|
    [| 1. ; 0.23 ; 0.37 |] ;
    [| 0. ; 0.7 ; 0.18 |] ;
    [| 0. ; 0. ; 0.55 |] ;
  |]
  in
  let p = Lac.lacpy m in
  let lbd = Lac.syev ~up:true ~vectors:true p in
  Printf.printf "lbd =\n" ;
  Lacaml.Io.pp_fvec Format.std_formatter lbd ;
  [%expect{|
    lbd =
    0.334727
    0.576374
      1.3389 |}] ;
  Printf.printf "P =\n" ;
  Lacaml.Io.pp_fmat Format.std_formatter p ;
  [%expect{|
    P =
    -0.436499  0.439922 0.784816
    -0.161324 -0.896437 0.412765
     0.885123 0.0535616 0.462264 |}] ;
  (* P is orthonormal
   * its inverse equals its transpose *)
  let i = Lac.lacpy p in
  Lac.getri i ;
  Printf.printf "P^-1\n" ;
  Lacaml.Io.pp_fmat Format.std_formatter i ;
  [%expect{|
    P^-1
    -0.436499 -0.161324  0.885123
     0.439922 -0.896437 0.0535616
     0.784816  0.412765  0.462264 |}] ;
  let p_d = Lac.lacpy p in
  Lac.Mat.scal_cols p_d lbd ;
  Printf.printf "P * D\n" ;
  Lacaml.Io.pp_fmat Format.std_formatter p_d ;
  [%expect{|
    P * D
     -0.146108   0.25356  1.05079
    -0.0539996 -0.516683 0.552651
      0.296275 0.0308715 0.618925 |}] ;
  (* p_d now contains P * D *)
  Printf.printf "M =\n" ;
  Lacaml.Io.pp_fmat Format.std_formatter m ;
  [%expect{|
    M =
    1 0.23 0.37
    0  0.7 0.18
    0    0 0.55 |}] ;
  let m' = Lac.gemm ~transb:`T p_d p in
  Printf.printf "P * D * P^T =\n" ;
  Lacaml.Io.pp_fmat Format.std_formatter m' ;
  [%expect{|
    P * D * P^T =
       1 0.23 0.37
    0.23  0.7 0.18
    0.37 0.18 0.55 |}]


let%expect_test _ =
  let m = Lac.Mat.of_array [|
    [| 1. ; 0.23 ; 0.37 |] ;
    [| 0. ; 0.7 ; 0.18 |] ;
    [| 0. ; 0. ; 0.55 |] ;
  |]
  in
  let rt = U.Mat.sym_compute_root ~up:true m in
  (* rt now holds the sqrt *)
  Lacaml.Io.pp_fmat Format.std_formatter rt ;
  [%expect{|
    0.969867 0.116183 0.214151
    0.116183 0.822286 0.101718
    0.214151 0.101718 0.702704 |}] ;
  (* check that rt * rt looks like m *)
  let m' = Lac.gemm rt rt in
  Lacaml.Io.pp_fmat Format.std_formatter m ;
  [%expect{|
    1 0.23 0.37
    0  0.7 0.18
    0    0 0.55 |}] ;
  Lacaml.Io.pp_fmat Format.std_formatter m' ;
  [%expect{|
       1 0.23 0.37
    0.23  0.7 0.18
    0.37 0.18 0.55 |}]


(* test geev to understand gen_linear_flow *)
let%expect_test _ =
 let m = Lac.Mat.of_array [|
   [| -0.5 ; 0.2 ; 0.3 |] ;
   [| 0.2 ; -0.5 ; 0.3 |] ;
   [| 0.3 ; 0.3 ; -0.6 |] ;
 |]
 in
 let _, re, im, p = Lac.geev ~vl:None (Lac.lacpy m) in
 Lac.Vec.iter (fun x -> assert (U.nearly_equal 0. x)) im ;
 let lbd = re in
 let work = Lac.Mat.make0 (Lac.Mat.dim1 p) (Lac.Mat.dim2 p) in
 let m' = U.Mat.dediag ~return:`Fresh ~work p lbd in
 Printf.printf "P D P^-1 = \n" ;
 Lacaml.Io.pp_fmat Format.std_formatter m' ;
 [%expect{|
   P D P^-1 =
   -0.5  0.2  0.3
    0.2 -0.5  0.3
    0.3  0.3 -0.6 |}]

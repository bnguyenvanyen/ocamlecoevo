open Sig


let%expect_test _ =
  P.printf "%f\n" (F.to_float (U.d_unif 0. 1. 2.)) ;
  [%expect{| 0.000000 |}]


let%expect_test _ =
  P.printf "%f\n" (F.to_float (U.d_unif 0. 1. 0.5)) ;
  [%expect{| 1.000000 |}]

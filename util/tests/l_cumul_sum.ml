open Sig


let print xl =
  P.printf "[\n" ;
  L.iter (P.printf "%f ;\n") xl ;
  P.printf "]\n"


let%expect_test _ =
  print (U.l_cumul_sum [0. ; 1. ; 2.]) ;
  [%expect{|
    [
    0.000000 ;
    1.000000 ;
    3.000000 ;
    ] |}]

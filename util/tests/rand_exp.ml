open Sig


let%expect_test _ =
  (* works with any random number *)
  let rng = Random.State.make_self_init () in
  let t = U.rand_exp ~rng F.zero in
  print_float (F.to_float t) ;
  [%expect{| inf |}]


let%expect_test _ =
  (* works with any random number *)
  let rng = Random.State.make_self_init () in
  let t = U.rand_exp ~rng F.infinity in
  print_float (F.to_float t) ;
  [%expect{| 0. |}]

module U = Util
module F = U.Float


let%expect_test _ =
  let logd = U.logd_gamma F.one F.one 0. in
  Printf.printf "%f" (F.to_float logd) ;
  [%expect{| -inf |}]


let%expect_test _ =
  let logd = U.logd_gamma F.one F.one 1. in
  Printf.printf "%f" (F.to_float logd) ;
  [%expect{| -1.000000 |}]


let%expect_test _ =
  let logd = U.logd_gamma (F.Pos.of_float 1.5) F.one 3. in
  Printf.printf "%f" (F.to_float logd) ;
  [%expect{| -2.329912 |}]
  (* With R:
     > dgamma(3, 1.5, 1, log=T)
     [1] -2.329912
   *)


let%expect_test _ =
  let logd = U.logd_gamma (F.Pos.of_float 1.5) (F.Pos.of_float 0.1) 3. in
  Printf.printf "%f" (F.to_float logd) ;
  [%expect{| -3.083789 |}]
  (* With R:
     > dgamma(3, 1.5, rate=0.1, log=T)
     [1] -3.083789
   *)

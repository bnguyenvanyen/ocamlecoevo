open Sig


let print sll =
  P.printf "[\n" ;
  L.iter (fun sl ->
    P.printf "[%s] ;\n" (L.reduce (P.sprintf "%s ; %s") sl)
  ) sll ;
  P.printf "]\n"


let%expect_test _ =
  print (U.transpose [[]]) ;
  [%expect{|
    [
    ] |}]


let%expect_test _ =
  print (U.transpose [["a" ; "b"] ; ["c" ; "d"]]) ;
  [%expect{|
    [
    [a ; c] ;
    [b ; d] ;
    ] |}]



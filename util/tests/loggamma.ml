open Sig


let%test _ =
  (abs_float (U.loggamma 1.) < 1e-9)


(* confirmed with R *)
let%expect_test _ =
  P.printf "%f" (U.loggamma 2.5) ;
  [%expect{| 0.284683 |}]

open Sig


let%expect_test _ =
  let rng = Random.State.make_self_init () in
  let n = U.rand_poisson ~rng F.zero in
  print_int (U.Int.to_int n) ;
  [%expect{| 0 |}]

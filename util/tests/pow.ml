(* Test Util.pow *)

open Sig


let%expect_test _ =
  P.printf "%f\n" (U.pow 0. 0) ;
  [%expect{| 1.000000 |}]


let%expect_test _ =
  P.printf "%f\n" (U.pow 0. 1) ;
  [%expect{| 0.000000 |}]


let%expect_test _ =
  P.printf "%f\n" (U.pow 1. 0) ;
  [%expect{| 1.000000 |}]


let%expect_test _ =
  P.printf "%f\n" (U.pow 1. 2) ;
  [%expect{| 1.000000 |}]


let%expect_test _ =
  P.printf "%f\n" (U.pow 2. (~-1)) ;
  [%expect{| 0.500000 |}]


let%expect_test _ =
  P.printf "%f\n" (U.pow 2. 2) ;
  [%expect{| 4.000000 |}]


let%expect_test _ =
  P.printf "%f\n" (U.pow (~-. 2.) 2) ;
  [%expect{| 4.000000 |}]



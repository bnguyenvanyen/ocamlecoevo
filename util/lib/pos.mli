(** Positive values *)

type t

(** [of_float x] raises [Invalid_argument] if [x] is not positive. *)
val of_float : float -> t

(** [of_float_unsafe x] is actually the identity function,
 *  and as such, UNSAFE ! *)
val of_float_unsafe : float -> t

val to_float : t -> float

val zero : t
val one : t
val two : t
val pi : t

val add : t -> t -> t

(** [sub x y] is [x -. y].
 *  Raises [Assertion_error] if the result is not positive. *)
val sub : t -> t -> t

val mul : t -> t -> t

val div : t -> t -> t

val exp : float -> t
val log : t -> float
val sq : float -> t
val sqrt : t -> t
val neg : t -> float
val pow : t -> int -> t
val gamma : t -> t

val (+) : t -> t -> t
val (-) : t -> t -> t
val ( * ) : t -> t -> t
val (/) : t -> t -> t

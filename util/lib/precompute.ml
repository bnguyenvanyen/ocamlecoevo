open Base

module P = Proba

module F = Float
module I = Int


module type PRE =
  sig
    (** [logfact k] is the log of [factorial k] *)
    val logfact : int -> float

    val logbin_coeff : int -> int -> float

    (** [logp_poisson lbd k] is a faster {!logp_poisson}
     *  with pre-computed log-factorial values. *)
    val logp_poisson :
      _ F.anypos F.t ->
      int ->
        _ F.neg F.t

    (** faster {!logsump_poisson} *)
    val logsump_poisson :
      _ F.anypos F.t list ->
      int ->
        F._float F.t

    (** faster {!logp_negbin} *)
    val logp_negbin :
      _ F.anypos F.t ->
      F.anyproba F.t ->
      int ->
        _ F.neg F.t

    (** [logp_negbin_over] is {!logp_negbin} with the 'over-dispersion'
     *  parameterization. *)
    val logp_negbin_over :
      _ F.anypos F.t ->
      _ F.anypos F.t ->
      int ->
        _ F.neg F.t

    (** [logp_n_choice_k] is a faster {!log_n_choice_k}
     *  with pre-computed log-factorial values. *)
    val logp_n_choice_k :
      choose:int ->
      among:int ->
        _ F.neg F.t

    (** [logd_poisson_process] is a faster {!logd_poisson_process}
     *  with pre-computed log-factorial values. *)
    val logd_poisson_process :
      _ F.anypos F.t ->
      I.anypos I.t ->
        _ F.neg F.t

    val rand_binomial :
      rng:Random.State.t ->
      I.anypos I.t ->
      F.anyproba F.t ->
        int

    val p_binomial :
      I.anypos I.t ->
      F.anyproba F.t ->
      int ->
        _ F.proba F.t

    val logp_binomial :
      I.anypos I.t ->
      F.anyproba F.t ->
      int ->
        _ F.neg F.t

    val logp_multinomial :
      I.anypos I.t ->
      F.anyproba F.t list ->
      int list ->
        _ F.neg F.t

    module Int :
      sig
        module Pos :
          sig
            (** [logfact k] is the log of [factorial k] *)
            val logfact : I.anypos I.t -> _ F.pos F.t
          end
      end
  end


module Make ( ) =
  struct
    let max_pre = 128
    let size = 257

    let logfact_values = direct_logfact_array ~size max_pre

    let logfact k =
      assert (k >= 0) ;
      if k <= max_pre then
        logfact_values.(k)
      else begin
        if k < size then
          if logfact_values.(k) > 0. then
            logfact_values.(k)
          else
            let res = logfact k in
            logfact_values.(k) <- res ;
            res
        else begin
          logfact k
        end
      end

    let logbin_coeff k n =
      logfact n -. logfact k -. logfact (n - k)

    let logp_poisson lbd k = P.logp_poisson_with logfact lbd k

    let logsump_poisson lbd ks = P.logsump_poisson_with logfact lbd ks

    let logp_negbin r p k = P.logp_negbin_with logfact r p k

    let logp_negbin_over ovd lbd k =
      let r, p = P.over_disp_deparam ovd lbd in
      logp_negbin r p k

    let logp_n_choice_k ~choose ~among =
      P.logp_n_choice_k_with logfact ~choose ~among

    let logd_poisson_process volume k =
      let logfact k = Float.of_float_unsafe (logfact (Int.to_int k)) in
      P.logd_poisson_process_with logfact volume k

    let rand_binomial ~rng n p =
      Rand.rand_binomial_with logbin_coeff ~rng n p

    let logp_binomial n p k =
      P.logp_binomial_with logbin_coeff n p k

    let p_binomial n p k =
      P.p_binomial_with logbin_coeff n p k

    let logp_multinomial n p x =
      P.logp_multinomial_with logfact n p x

    module Int =
      struct
        module Pos =
          struct
            let two_five_six = Int.Pos.of_int 256
            let two_five_seven = Int.Pos.of_int 257

            let logfact_values =
              Int.Pos.array_init
              ~f:Int.Pos.logfact
              ~n:two_five_seven

            let logfact k =
              let x =
                if Int.Op.(k <= two_five_six) then
                  logfact_values.(Int.to_int k)
                else
                  Int.Pos.logfact k
              in Float.Pos.narrow x
          end
      end
  end

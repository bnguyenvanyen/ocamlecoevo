

let tune () =
  Gc.set { (Gc.get ()) with
    minor_heap_size = (262_144 * 2) ;
    major_heap_increment = (1_000_448 * 4) ;
  }

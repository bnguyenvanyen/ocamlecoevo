open Interfaces

module Product =
  struct
    (* This product is not symmetric, the order matters *)
    module Comparable (A : COMPARABLE) (B : COMPARABLE) :
      (COMPARABLE with type t = A.t * B.t) =
      struct
        type t = A.t * B.t

        let compare (a, b) (a', b') =
          let c = A.compare a a' in
          if c = 0 then
            B.compare b b'
          else
            c
      end

    (* this one is symmetric *)
    module Equalable (A : EQUALABLE) (B : EQUALABLE) :
      (EQUALABLE with type t = A.t * B.t) =
      struct
        type t = A.t * B.t

        let equal (a, b) (a', b') =
          (A.equal a a') && (B.equal b b')
      end
  end

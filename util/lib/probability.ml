type t = float

let of_float x : t =
  if x >= 0. && x <= 1. then
    x
  else
    invalid_arg "Negative or > 1"

let of_pos x =
  let x' = Pos.to_float x in
  if x' <= 1. then
    x'
  else
    invalid_arg "Greater than 1"

let of_float_unsafe x : t = x

let of_pos_unsafe x = x |> Pos.to_float |> of_float_unsafe

let to_float x = x

let to_pos = Pos.of_float

let zero = 0.

let one = 1.

let compl x = 1. -. x

let mul = ( *. )

let sq = Base.sq

let sqrt = sqrt

let pow p k = Base.pow p (Posint.to_int k)

let neglog p = Pos.of_float (~-. (log p))

let ( * ) = mul


let ( ** ) p r = ( ** ) p (Pos.to_float r)

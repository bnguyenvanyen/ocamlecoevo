
module F = Float
module I = Int

module type PRE =
  sig
    (** [logfact k] is the log of [factorial k] *)
    val logfact : int -> float

    val logbin_coeff : int -> int -> float

    (** [logp_poisson lbd k] is a faster {!logp_poisson}
     *  with pre-computed log-factorial values. *)
    val logp_poisson :
      _ F.anypos F.t ->
      int ->
        _ F.neg F.t

    (** faster {!logsump_poisson} *)
    val logsump_poisson :
      _ F.anypos F.t list ->
      int ->
        F._float F.t

    (** faster {!logp_negbin} *)
    val logp_negbin :
      _ F.anypos F.t ->
      F.anyproba F.t ->
      int ->
        _ F.neg F.t

    (** [logp_negbin_over] is {!logp_negbin} with the 'over-dispersion'
     *  parameterization. *)
    val logp_negbin_over :
      _ F.anypos F.t ->
      _ F.anypos F.t ->
      int ->
        _ F.neg F.t

    (** [logp_n_choice_k] is a faster {!log_n_choice_k}
     *  with pre-computed log-factorial values. *)
    val logp_n_choice_k :
      choose:int ->
      among:int ->
        _ F.neg F.t

    (** [logd_poisson_process] is a faster {!logd_poisson_process}
     *  with pre-computed log-factorial values. *)
    val logd_poisson_process :
      _ F.anypos F.t ->
      I.anypos I.t ->
        _ F.neg F.t

    val rand_binomial :
      rng:Random.State.t ->
      I.anypos I.t ->
      F.anyproba F.t ->
        int

    val p_binomial :
      I.anypos I.t ->
      F.anyproba F.t ->
      int ->
        _ F.proba F.t

    val logp_binomial :
      I.anypos I.t ->
      F.anyproba F.t ->
      int ->
        _ F.neg F.t

    val logp_multinomial :
      I.anypos I.t ->
      F.anyproba F.t list ->
      int list ->
        _ F.neg F.t

    module Int :
      sig
        module Pos :
          sig
            (** [logfact k] is the log of [factorial k] *)
            val logfact : I.anypos I.t -> _ F.pos F.t
          end
      end
  end


module Make : functor ( ) -> PRE

module Lac = Lacaml.D

type return = [
  | `Fresh
  | `Reuse
  | `Into of Lac.Mat.t
]


let return_copy_mat m =
  function
  | `Reuse ->
      (* directly in m *)
      m
  | `Fresh ->
      Lac.lacpy m
  | `Into mat ->
      Lac.lacpy ~b:mat m


(* product of a diagonal matrix times any matrix *)
let dot_diag_mat ~return d m =
  let m' = return_copy_mat m return in
  Lac.Mat.scal_rows d m' ;
  m'


let dot_mat_diag ~return m d =
  let m' = return_copy_mat m return in
  Lac.Mat.scal_cols m' d ;
  m'


(* FIXME do less allocations *)
let dediag ~return ~work p d =
  (* P^-1 M P for general matrices *)
  let inv_p = Lac.lacpy ~b:work p in
  Lac.getri inv_p ;
  let p_d = dot_mat_diag ~return p d in
  Lac.gemm p_d inv_p


let sym_dediag ~return p d =
  let p_d = dot_mat_diag ~return p d in
  (* FIXME do less allocations *)
  Lac.gemm (* ~c:p_d *) ~transb:`T p_d p


(* root of a symmetric matrix,
 * directly in the matrix *)
let sym_compute_root ~up m =
  (* name p to make it clear that it will hold eigenvectors,
   * but actually shares memory with m *)
  let p = Lac.lacpy m in
  let work = Lac.Mat.make0 (Lac.Mat.dim1 p) (Lac.Mat.dim2 p) in
  let lbd = Lac.syev ~up ~vectors:true p in
  (* p now contains eigenvectors *)
  (* p is orthonormal, so P^-1 = P^T *)
  (* lbd is the vector of eigenvalues *)
  let sqrt_lbd = Lac.Vec.sqrt lbd in
  (* the square root of m = p * sqrt_d * p^t *)
  (* FIXME do less allocations *)
  dediag ~return:`Fresh ~work p sqrt_lbd


let sym_linear_flow ~up a =
  (* p will contain the eigenvectors *)
  let p = Lac.lacpy a in
  let lbd = Lac.syev ~vectors:true ~up p in
  let work = Lac.Vec.make0 (Lac.Vec.dim lbd) in
  let work' = Lac.Mat.make0 (Lac.Mat.dim1 a) (Lac.Mat.dim2 a) in
  (* compute e^(dt A) *)
  let f (dt : _ Float.anypos Float.t) =
    (* copy lbd *)
    let lbd = Lac.copy ~y:work lbd in
    (* scale by dt *)
    Lac.scal (Float.to_float dt) lbd ;
    (* diagonal exponents *)
    let e_lbd = Lac.Vec.exp ~y:lbd lbd in
    (* e^(dt r) = p * e^(dt lbd) * p^t *)
    (* first compute p * e^(dt lbd) *)
    let p_e_lbd = dot_mat_diag ~return:(`Into work') p e_lbd in
    (* then compute p_e_lbd * p^t *)
    Lac.gemm ~transb:`T p_e_lbd p
  in f


(* for a real diagonalizable matrix with real eigenvalues *)
let gen_linear_flow a =
  let _, lbd, im, p = Lac.geev ~vl:None (Lac.lacpy a) in
  (* check im is nearly zero *)
  Lac.Vec.iter (fun x -> assert (Base.nearly_equal x 0.)) im ;
  let work = Lac.Vec.make0 (Lac.Vec.dim lbd) in
  let work' = Lac.Mat.make0 (Lac.Mat.dim1 a) (Lac.Mat.dim2 a) in
  let work'' = Lac.Mat.make0 (Lac.Mat.dim1 p) (Lac.Mat.dim2 p) in
  (* compute e^(dt A) *)
  let f (dt : _ Float.anypos Float.t) =
    (* copy lbd *)
    let lbd = Lac.copy ~y:work lbd in
    (* scale by dt *)
    Lac.scal (Float.to_float dt) lbd ;
    (* diagonal exponents *)
    let e_lbd = Lac.Vec.exp ~y:lbd lbd in
    (* e^(dt r) = p * e^(dt lbd) * p^-1 *)
    dediag ~return:(`Into work') ~work:work'' p e_lbd
  in f

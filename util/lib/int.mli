(** Integer with typed sign
  
    Distinction between positive and negative values.
  
    (See also {!Float} for floats on the same principle)
  
    Dedicated modules conserve the properties.
  
    Unification of types is sometimes capricious,
    but you can try to use the {!narrow} function and variants.
  
  
 *)


(** (phantom) type for integers *)
type +'a t

(** unknown integer type *)
type _int = [ `Int ]

(** any kind of integer *)
type 'a anyint = [< `Int | `Pos | `Neg > `Int] as 'a

(** a positive integer *)
type 'a pos = [< `Int | `Pos > `Int ] as 'a

(** any kind of positive integer *)
type anypos = [ `Int | `Pos ]

(** a negative integer *)
type 'a neg = [< `Int | `Neg > `Int ] as 'a

(** any kind of negative integer *)
type anyneg = [ `Int | `Neg ]


val of_int : int -> _int t

val of_int_unsafe : int -> _ anyint t

val to_int : _ anyint t -> int

val to_float : _ anyint t -> Float._float Float.t

val of_float : _ Float.anyfloat Float.t -> _int t

val ceil : _ Float.anyfloat Float.t -> _int t

val narrow : _ anyint t -> _int t

val promote_unsafe : _ anyint t -> _ anyint t


val zero : _ pos t

val one : _ pos t

val two : _ pos t

val neg_one : _ neg t


val succ : _ anyint t -> _int t

val pred : _ anyint t -> _int t

val neg : _ anyint t -> _int t


val add : _ anyint t -> _ anyint t -> _int t

val sub : _ anyint t -> _ anyint t -> _int t

val mul : _ anyint t -> _ anyint t -> _int t

val div : _ anyint t -> _ anyint t -> _int t

val to_string : _ anyint t -> string

val of_string : string -> _int t option

val compare : _ anyint t -> _ anyint t -> int


module Op :
  sig
    val (~+) : int -> _int t
    val (~-) : _ anyint t -> _int t

    val (+) : _ anyint t -> _ anyint t -> _int t
    val (-) : _ anyint t -> _ anyint t -> _int t
    val ( * ) : _ anyint t -> _ anyint t -> _int t
    val (/) : _ anyint t -> _ anyint t -> _int t

    val (mod) : _ anyint t -> _ anyint t -> _int t

    val (=) : _ anyint t -> _ anyint t -> bool
    val (<>) : _ anyint t -> _ anyint t -> bool
    val (<) : _ anyint t -> _ anyint t -> bool
    val (<=) : _ anyint t -> _ anyint t -> bool
    val (>) : _ anyint t -> _ anyint t -> bool
    val (>=) : _ anyint t -> _ anyint t -> bool
  end


module Pos :
  sig
    val of_int : int -> _ pos t

    val of_anyint : _ anyint t -> _ pos t

    val to_float : anypos t -> _ Float.pos Float.t

    val of_float : _ Float.anypos Float.t -> _ pos t
    
    val of_string : string -> _ pos t option

    val ceil : _ Float.anypos Float.t -> _ pos t

    val narrow : anypos t -> _ pos t


    val succ : anypos t -> _ pos t

    (* not free : checks *)
    val pred : anypos t -> _ pos t

    val neg : anypos t -> _ neg t


    val add : anypos t -> anypos t -> _ pos t

    val mul : anypos t -> anypos t -> _ pos t


    val factorial : anypos t -> _ pos t

    val logfact : anypos t -> _ Float.pos Float.t

    val fold :
      f:('a -> anypos t -> 'a) ->
      x:'a ->
      n:anypos t ->
        'a

    val list_init :
      f:(anypos t -> 'a) ->
      n:anypos t ->
        'a list

    val array_init :
      f:(anypos t -> 'a) ->
      n:anypos t ->
        'a array

    module Op :
      sig
        val (~-) : anypos t -> _ neg t

        val (+) : anypos t -> anypos t -> _ pos t
        val ( * ) : anypos t -> anypos t -> _ pos t
        val (/) : anypos t -> anypos t -> _ pos t

        val (mod) : anypos t -> anypos t -> _ pos t
      end
  end


module Neg :
  sig
    val of_int : int -> _ neg t

    val to_float : anyneg t -> _ Float.neg Float.t

    val narrow : anyneg t -> _ neg t

    
    val one : anyneg t


    val pred : anyneg t -> _ neg t

    val neg : anyneg t -> _ pos t


    val add : anyneg t -> anyneg t -> _ neg t

    val mul : anyneg t -> anyneg t -> _ pos t

    module Op :
      sig
        val (~-) : anyneg t -> _ pos t

        val (+) : anyneg t -> anyneg t -> _ neg t
        val ( * ) : anyneg t -> anyneg t -> _ neg t
      end
  end

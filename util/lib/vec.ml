module Lac = Lacaml


let complex ~re ~im =
  let n = Lac.D.Vec.dim re in
  let n' = Lac.D.Vec.dim im in
  assert (n = n') ;
  let c = Lacaml.Z.Vec.make0 n in
  for i = 1 to n do
    c.{i} <- { Complex.re = re.{i} ; im = im.{i} }
  done
  ;
  c

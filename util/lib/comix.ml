open Sig

module F = Float

(* maybe too complex for Util ? *)

module Iset = Set.Make (struct type t = int let compare = compare end)

(* for now, columns should sum to 1. *)
let rand_comixture ~rng (comix : F.closed_pos F.t array array) =
  (* special array product that modifies the first array *)
  let update_probas choices probas = A.modify (fun (i, x) ->
    (i, F.Pos.mul probas.(i) x)) choices
  in
  let diag_choices a = A.mapi (fun i line -> (i, line.(i))) a in
  let choices = diag_choices comix in
  let rec and_choose chosen =
    let i = Rand.rand_choose ~rng (A.to_list choices) in
    (* not yet chosen *)
    if not (Iset.mem i chosen) then
      let chosen' = Iset.add i chosen in
      update_probas choices comix.(i) ;
      and_choose chosen'
    else
      chosen
  in Iset.elements (and_choose Iset.empty)

    

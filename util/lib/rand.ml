(******* Functions using the random generator *******)
open Sig
open Base

module F = Float
module I = Int

type rng = Random.State.t

let rng = function
  | None -> Random.State.make_self_init ()
  | Some n -> Random.State.make (Array.init 1 (fun _ -> n))


(* doesn't guarantee that the PRNG has been initialized *)
(*
let some_rng = function
  | None -> Random.get_state ()
  | Some rng -> rng
*)


let rand_rng rng =
  Random.State.make [| Random.State.bits rng |]


(* exclusive *)
let rand_int ~rng top : _ I.pos I.t =
  I.of_int_unsafe (Random.State.int rng (I.to_int top))


let rand_int_parts ~rng top : (_ I.pos I.t * _ I.pos I.t) =
  let top' = I.to_int top in
  let i = Random.State.int rng top' in
  I.of_int_unsafe i, I.of_int_unsafe (top' - i)


(* > 0. *)
let rand_float ~rng (top : _ F.anypos F.t) : _ F.pos F.t =
  let top' = F.to_float top in
  let rec f () =
    match Random.State.float rng top' with
    | 0. ->
        f ()
    | u  ->
        u
  in
  if top' = 0. then
    F.zero
  else
    F.of_float_unsafe (f ())


let rand_bool ~rng =
  Random.State.bool rng


let rand_proba ~rng : _ F.proba F.t =
  F.promote_unsafe (rand_float ~rng F.one)


let thousand : I.anypos I.t = I.Pos.of_int 1_000

let insist ?(ntries=thousand) ~rng g =
  let rec f =
    function
    | 0 ->
        failwith "exhausted payload"
    | n when n < 0 ->
        assert false (* protected by the type *)
    | n ->
        match g ~rng with
        | None ->
            f (n - 1)
        | Some x ->
            x
  in f (I.to_int ntries)


let bound ~rng a b f =
  (* cadlag : left included, right excluded *)
  let p x = (x >= a) && (x < b) in

  let g ~rng =
    let x = f ~rng in
    if p x then Some x else None
  in
  insist ~rng g


let rand_exp ~rng lambda : _ F.pos F.t =
  (* log1p (- u) where u ~ U[0, 1]:
     normally equivalent to log(u), but gives better precision *)
  let u = rand_proba ~rng in
  F.promote_unsafe F.(Op.(~- (log1p (~- u)) / lambda))


(* Box-Muller method *)
(* FIXME Should we implement Ziggurat ? *)
let rand_normal ~rng m v =
  let v' = F.to_float v in
  let u = F.to_float (rand_proba ~rng) in
  let w = F.to_float (rand_proba ~rng) in
  (* N (0, 1) *)
  let x = (sqrt (~-. 2. *. (log u) )) *. (cos (2. *. pi *. w)) in
  (* rescale *)
  m +. (sqrt v') *. x


(* I think *)
let rand_lognormal ~rng ?(b=10.) m v =
  let x = rand_normal ~rng m v in
  b ** x


let four : I.anypos I.t = I.Pos.of_int 4

let rand_square ~rng a v =
  let x = F.to_float (rand_float ~rng a) in
  let y = rand_normal ~rng 0. v in
  let a' = F.to_float a in
  match I.to_int (rand_int ~rng four) with
  | 0 -> (x, y)
  | 1 -> (y, x)
  | 2 -> (x, y +. a')
  | 3 -> (y +. a', x)
  | _ -> failwith "Impossible case"


let rand_bernoulli ~rng p =
  let u = rand_proba ~rng in
  (u <= p)


(* a bit annoying because we basically rewrite
   l_cumul_sum and rand_choose *)
let rand_binomial_with logbin_coeff ~rng n p =
  (* n trials with proba p of success *)
  let n' = Int.to_int n in
  let rec cumulbin k tot acc =
    let pb = Proba.p_binomial_with logbin_coeff n p k in
    match k with
    | 0 -> 
      let tot' = F.Pos.add tot pb in
      (tot', tot' :: acc)
    | _ ->
      let tot' = F.Pos.add tot pb in
      cumulbin (k - 1) tot' (tot' :: acc)
  in
  let probtot, pl = cumulbin n' F.zero [] in
  let u = rand_float ~rng probtot in
  let rec choose k pl =
    match pl with
    | p :: ptl ->
      if u <= p then
        k
      else
        choose (k - 1) ptl
    | [] ->
      (* should only be reached on entry with n = 0 *)
      0
  in choose n' (L.rev pl)


let rand_binomial ~rng n p =
  rand_binomial_with Base.logbin_coeff ~rng n p


(* FIXME where does this implementation come from ? *)
let rand_poisson_acrej_from (lbd : _ F.anypos F.t) =
  let lbd' = F.to_float lbd in
  let sqlbd = sqrt lbd' in
  let loglbd = log lbd' in
  let b = 0.931 +. 2.53 *. sqlbd in
  let a = ~-. 0.059 +. 0.02483 *. b in
  let invalpha = 1.1239 +. 1.1328 /. (b -. 3.4) in
  let vr = 0.9277 -. 3.6244 /. (b -. 2.) in
  let f u v =
    let u = F.to_float u -. 0.5 in
    let v = F.to_float v in
    let us = 0.5 -. abs_float u in
    let k = floor (u *. (2. *. a /. us +. b) +. lbd' +. 0.43) in
    if (us >= 0.07) && (v <= vr) then
      Some (int_of_float k)
    else if (k < 0.) || ((us < 0.013) && (v > us)) then
      None
    else if (log v +. log invalpha -. log (a /. (us *. us) +. b))
         <= (k *. loglbd -. lbd' -. loggamma (k +. 1.)) then
      Some (int_of_float k)
    else
      None
  in f


let rand_poisson_acrej ~rng (lbd : _ F.anypos F.t) : _ Int.pos Int.t =
  let rpaf = rand_poisson_acrej_from lbd in
  let f ~rng =
    let u = rand_proba ~rng in
    let v = rand_proba ~rng in
    rpaf u v
  in Int.of_int_unsafe (insist ~rng f)


let rand_poisson_exp ~rng (lbd : _ F.anypos F.t) : _ Int.pos Int.t =
  let rec f k t =
    let e = F.to_float (rand_exp ~rng F.one) in
    let nt = t -. e in
    if nt >= 0. then
      f (Int.Pos.succ k) nt
    else 
      k
  in Int.Pos.narrow (f Int.zero (F.to_float lbd))


let rand_poisson ~rng (lbd : _ F.anypos F.t) : _ Int.pos Int.t =
  let ten = F.of_float 10. in 
  if F.Op.(lbd > ten) then
    match rand_poisson_acrej ~rng lbd with
    | n ->
        n
    | exception Failure _ ->
        rand_poisson_exp ~rng lbd
  else if F.Op.(lbd = F.zero) then
    Int.zero
  else
    rand_poisson_exp ~rng lbd


let rand_poisson_process ~rng (lbd : _ F.anypos F.t) (tf : _ F.anypos F.t) =
  let rec f acc t =
    let e = rand_exp ~rng lbd in
    let nt = F.Pos.add t e in
    if F.Op.(nt < tf) then
      f (nt :: acc) nt
    else
      acc
  in L.rev (f [] F.zero)


(* logarithmic variable *)
let rand_log ~rng p =
  (* Can't manage to pass a proba x because of recursive calls *)
  let incr_series k (x : _ F.anypos F.t) =
    let posk = Int.Pos.to_float k in
    let pk = F.Pos.pow p (Int.to_int k) in
    F.Pos.add x (F.Pos.div pk posk)
    (* F.Pos.Op.(x + pk / posk) *)
  in
  let u = rand_proba ~rng in
  (* ~ log (1 - p) *)
  let r = p |> F.Proba.compl |> F.Proba.log |> F.Neg.neg in
  let ru = F.Pos.mul r u in
  let rec stop_at k (x : _ F.anypos F.t) =
    if F.Op.(ru <= x) then
      k
    else
      let k' = Int.Pos.succ k in
      let x' = incr_series k' x in
      stop_at k' x'
  in
  Int.Pos.narrow (stop_at Int.zero F.zero)


(* quite simple but not very efficient if lbd is big...
 * but gamma doesn't really seem better *)
let rand_negbin ~rng r p =
  let rec add_logs k =
    function
    | 0 ->
        k
    | n when n <= 0 ->
        invalid_arg "negative"
    | n ->
        let k' = Int.Pos.add k (rand_log ~rng p) in
        add_logs k' (n - 1)
  in
  let nlcp = p |> F.Proba.compl |> F.Proba.log |> F.Neg.neg in
  let lbd = F.Pos.mul r nlcp in
  let n = rand_poisson ~rng lbd in
  Int.Pos.narrow (add_logs Int.zero (Int.to_int n))


let over_disp_deparam ovd lbd =
  (* r = 1 / ovd *)
  let r = F.Pos.div F.one ovd in
  (* p = lbd / (lbd + r) *)
  let _, p, _ = F.Pos.proportions lbd r in
  (r, p)


let rand_negbin_over ~rng ovd lbd =
  let r, p = over_disp_deparam ovd lbd in
  rand_negbin ~rng r p


(* FIXME replace by integer part of rand_exp for p < 1. /. 10. *)
let rand_geom ~rng p =
  let rec f n =
    if rand_bernoulli ~rng p then
      n
    else
      f (Int.Pos.succ n)
  in Int.Pos.narrow (f Int.zero)


let rand_cumul_choose ?tot ~rng csp_l =
  (* In order of increasing cumulative weights *)
  let tot =
    match tot with
    | None ->
        (* assume sum to 1. *)
        F.one
    | Some x ->
        x
  in
  let u = rand_float ~rng tot in
  let rec f =
    function
    | [] -> 
        invalid_arg "Util.rand_cumul_choose: total too small"
    | (c, sp) :: tl ->
        begin match F.Op.(u <= sp) with
        | true -> c 
        | false -> f tl
        end
  in
  match csp_l with
  | [] ->
      invalid_arg "Empty"
  | _ ->
      f csp_l


let rand_cumul_rev_choose ?tot ~rng csp_l =
  (* In order of decreasing cumulative weights *)
  let tot =
    match tot with
    | None ->
        (* assume sum to 1. *)
        F.one
    | Some x ->
        x
  in
  let u = rand_float ~rng tot in
  let rec f c = function
    | [] ->
        c
    | (c', sp) :: tl ->
        (match F.Op.(u >= sp) with
         | true -> c (* and not c' *)
         | false -> f c' tl)
  in
  match csp_l with
  | [] -> invalid_arg "Nothing to choose from"
  | (c, _) :: tl -> f c tl


(* Another more efficient possibility for rand_choose, to test
let rand_choose ~rng cp_l =
  (* c (for p) has already been seen *)
  let cumul (_, p) (c', p') =
    (c', F.Pos.add p p')
  in
  (* then we need a dummy choice (never chosen) for init *)
  let dummy_choice, _ = L.hd cp_l in
  let tot, revscp_l =
    l_cumul_fold_left cumul (dummy_choice, F.zero) cp_l
  in
  rand_cumul_rev_choose ~rng ~tot revscp_l
*)


(* would be faster on arrays *)
let rand_unif_choose ~rng c_l =
  c_l
  |> L.length
  |> I.of_int_unsafe
  |> rand_int ~rng
  |> I.to_int
  |> L.nth c_l


let rand_choose ~rng cp_l =
  let c_l, p_l = L.split cp_l in
  let tot, revsp_l = cumul_fold_left F.Pos.add F.zero p_l in
  let sp_l = L.rev revsp_l in
  let csp_l = L.combine c_l sp_l in
  rand_cumul_choose ~rng ~tot:tot csp_l


let rand_unif_choose_set
  (type a set)
  (module Set : (BatSet.S with type elt = a and type t = set))
  ~rng (set : set) : a =
  let k =
    set
    |> Set.cardinal
    |> I.of_int_unsafe
    |> rand_int ~rng
    |> I.to_int
  in Set.at_rank_exn k set


let rand_subset
  (type a set)
  (module Set : (BatSet.S with type elt = a and type t = set))
  ~rng (set : set) k : set =
  let n = Set.cardinal set in
  if k > n then
    invalid_arg (Printf.sprintf "subset too big: %i > %i" k n)
  else
    (* TODO optimization possible if n > k / 2 -> choose elements to remove *)
    Base.int_fold ~f:(fun subset _ ->
      let x = insist ~rng (fun ~rng ->
        let x =
          rand_unif_choose_set
            (module Set : (BatSet.S with type elt = a and type t = set))
            ~rng
            set
        in
        if Set.mem x subset then
          None
        else
          Some x
      )
      in
      Set.add x subset
    ) ~x:Set.empty ~n:k


let rand_cumul_multinomial ~rng n csp_l =
  let module M = BatMap.Int in
  let ip_l = L.mapi (fun i (_, p) -> (i, p)) csp_l in
  let is = I.Pos.fold ~f:(fun is _ ->
      let i = rand_cumul_choose ~rng ip_l in
      M.modify_def I.zero i I.Pos.succ is
    ) ~x:M.empty ~n
  in
  L.map2 (fun (x, _) (i, _) ->
    let k =
      match M.find i is with
      | k ->
          k
      | exception Not_found ->
          I.zero
    in
    (x, I.Pos.narrow k)
  ) csp_l ip_l
  

let rand_multinomial ~rng n cp_l =
  (* FIXME we compute the cumul sum list many times *)
  let c_l, p_l = L.split cp_l in
  let _, revsp_l = cumul_fold_left F.Pos.add F.zero p_l in
  let sp_l = L.rev revsp_l in
  let csp_l = L.combine c_l sp_l in
  rand_cumul_multinomial ~rng n csp_l


let rand_standard_gamma ~rng (alpha : _ F.anypos F.t) =
  (* drop signs to be more readable *)
  let alpha = F.to_float alpha in
  if alpha = 1. then
    rand_exp ~rng F.one
  else if alpha > 1. then begin
    (* Marsaglia and Tsang's method, see
       G. Marsaglia and W. Tsang.
       A simple method for generating gamma variables.
       ACM Transactions on Mathematical Software, 26(3):363-372, 2000.
     *)
    let b = alpha -. 1./.3. in
    let c = 1. /. (sqrt (9. *. b)) in
    let f ~rng =
      let u = F.to_float (rand_proba ~rng) in
      let z = rand_normal ~rng 0. F.one in
      let v = cub (1. +. c *. z) in
      let d = 1./.2. *. sq z +. b -. b *. v +. b *. log v in
      if (z > ~-. 1. /. c) && (log u < d) then
        Some (F.Pos.of_float (b *. v))
      else
        None
    in
    insist ~rng f
  end else begin
    (* Kundu and Gupta's method, see
       Kundu D, Gupta RD.
       A convenient way of generating gamma random variables
       using generalized exponential distribution.
       Computational Statistics & Data Analysis. 2007
     *)
    let d = 1.0334 -. 0.0766 *. exp (2.2942 *. alpha) in
    let a = (2. *. (1. -. exp (~-. d /. 2.))) ** alpha in
    let b = alpha *. d ** (alpha -. 1.) *. exp (~-. d) in
    let c = a +. b in
    let f ~rng =
      let u = F.to_float (rand_proba ~rng) in
      let x =
        if u <= a /. c then
          ~-. 2. *. log (1. -. (c *. u) ** (1. /. alpha) /. 2.)
        else
          ~-. (log ((c *. (1. -. u)) /. (alpha *. d ** (alpha -. 1.))))
      in
      let v = F.to_float (rand_proba ~rng) in
      if x <= d then
        let y = (x ** (alpha -. 1.) *. exp (~-. x /. 2.))
             /. ((2. *. (1. -. exp (~-. x /. 2.))) ** (alpha -. 1.))
        in
        if v <= y then
          Some (F.Pos.of_float x)
        else
          None
      else
        let y = (d /. x) ** (1. -. alpha) in
        if v <= y then
          Some (F.Pos.of_float x)
        else None
    in
    insist ~rng f
  end


let rand_gamma ~rng alpha beta =
  F.Pos.div (rand_standard_gamma ~rng alpha) beta


let rand_dirichlet ~rng alphas =
  let gs = L.map (rand_standard_gamma ~rng) alphas in
  let tot = L.fold_left F.Pos.add F.zero gs in
  L.map (fun g -> F.Pos.div g tot) gs


let subsample ~rng p l =
  let rec f acc l =
    match l with
    | x :: tl ->
      if rand_bernoulli ~rng p then
        f (x :: acc) tl
      else
        f acc tl
    | [] ->
      acc
  in L.rev (f [] l)


let p_monte_carlo p rand_f n : _ F.proba F.t =
  let rec f k i =
    match i with
    | 0 ->
      k
    | _ ->
      let x = rand_f () in
      if p x then
        f (k + 1) (i - 1)
      else
        f k (i - 1)
  in
  let n' = Int.to_int n in
  let k = f 0 n' in
  F.of_float_unsafe ((float k) /. (float n'))

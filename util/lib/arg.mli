(** Sugar on top of Cmdliner for CSV config file optional input.
  
    - conversion of util float and int types
    - terms with CSV config file optional input
    - updater terms with associated CSV optional input and output
 *)


type 'a term = 'a Cmdliner.Term.t
type 'a conv = 'a Cmdliner.Arg.conv


(** {1:conv Custom converters} *)

val positive : unit -> _ Float.pos Float.t conv

val proba : unit -> _ Float.proba Float.t conv

val posint : unit -> _ Int.pos Int.t conv

val read_csv :
  string ->
  (string -> ('a, [ `Msg of string ]) result) ->
  string ->
    'a

val read_csv_opt :
  string ->
  (string -> ('a, [ `Msg of string ]) result) ->
  string ->
    'a option


(** {1:config Argument terms with optional input from CSV config file} *)

type 'a configurable =
  (config:string option -> 'a)


(** [opt conv ~doc name] is a term for an optional argument
    converted with [conv], specified by '--name',
    and with documentation [doc]. *)
val opt :
  'a conv ->
  doc:string ->
  string ->
    'a option term


(** [config conv name] reads a value from the CSV file at [fname] if given. *)
val config :
  'a conv ->
  string ->
    'a option configurable term


(** [config_opt conv name] reads a value from the CSV file at [fname],
    if given and the column is present. *)
val config_opt :
  'a conv ->
  string ->
    'a option configurable term


(** [opt_config conv ~doc name] is like {!opt},
    but the argument's value is read from the CSV file at [fname] if given,
    and if no value is given through the command line. *)
val opt_config :
  'a conv ->
  doc:string ->
  string ->
    'a option configurable term


(** [opt_config_opt conv ~doc name] combines {!opt} and {!config_opt}.
    The return value is [Some v] if [--name v] is passed on the command line,
    or if a file is passed and a column [name] is present in it,
    and otherwise [None]. *)
val opt_config_opt :
  'a conv ->
  doc:string ->
  string ->
    'a option configurable term


(** [flag_config ~doc name] is a flag argument that returns the value
    [true] if the flag is present on the command line,
    or is present with value [true] in the file,
    and returns the value [false] otherwise,
    if the flag is absent, and the file is absent,
    or the flag is absent from the file, or has value [false] in the file. *)
val flag_config :
  doc:string ->
  string ->
    bool configurable term


(** {1:capture Argument terms with capture} *)

module Capture :
  sig
    (** Read values from the command line or a configuration file,
        while capturing the input for saving to another file.
        This improves reproducibility.

        The interface is functional and slightly similar to the one offered
        by Cmdliner itself for terms.
     *)

    (** A value ['a] with additional captured data *)
    type 'a t

    (** [empty x] is the value [x] with no captured data *)
    val empty : 'a -> 'a t

    (** [output fname x] removes the captured data from [x]
        and optionally outputs it to the file [fname]. *)
    val output : string option -> 'a t -> 'a

    (** [apply f x] is the captured function [f]
        applied to the captured value [x].
        The resulting captured data is the union of both.
        @raise Failure if the data overlaps. *)
    val apply : ('a -> 'b) t -> 'a t -> 'b t

    (** [apply_keep_left f x] is like {!apply} but if the data overlaps,
        keeps the value coming from [f]. *)
    val apply_keep_left : ('a -> 'b) t -> 'a t -> 'b t

    (** [apply_keep_right f x] is like {!apply} but if the data overlaps,
        keeps the value coming from [x]. *)
    val apply_keep_right : ('a -> 'b) t -> 'a t -> 'b t

    (** Operator for {!apply} *)
    val ($) : ('a -> 'b) t -> 'a t -> 'b t

    (** The arrow indicates what is kept *)

    (** Operator for {!apply_keep_left} *)
    val ($<) : ('a -> 'b) t -> 'a t -> 'b t

    (** Operator for {!apply_keep_right} *)
    val ($>) : ('a -> 'b) t -> 'a t -> 'b t

    (** [opt] is like {!opt} with capture. *)
    val opt :
      'a conv ->
      doc:string ->
      string ->
        'a t option term

    (** [config] is like {!config} with capture. *)
    val config :
      'a conv ->
      string ->
        'a t option configurable term

    (** [config_opt] is like {!config_opt} with capture. *)
    val config_opt :
      'a conv ->
      string ->
        'a t option configurable term

    (** [opt_config] is like {!opt_config} with capture. *)
    val opt_config :
      'a conv ->
      doc:string ->
      string ->
        'a t option configurable term

    (** [opt_config_opt] is like {!opt_config_opt} with capture. *)
    val opt_config_opt :
      'a conv ->
      doc:string ->
      string ->
        'a t option configurable term

    (** [flag_config] is like {!flag_config} with capture. *)
    val flag_config :
      doc:string ->
      string ->
        bool t configurable term
  end


(** {1:update Argument terms for updating parameters} *)


module Update :
  sig
    (** Update a value from multiple arguments *)

    type 'a update = ('a -> 'a)

    (** [opt set conv ~doc name] is like {!opt},
        but to update values with [set]. *)
    val opt :
      ('b -> 'a -> 'a) ->
      'b conv ->
      doc:string ->
      string ->
        'a update term

    (** [config set conv name] is like {!config},
        but to update values with [set]. *)
    val config :
      ('b -> 'a -> 'a) ->
      'b conv ->
      string ->
        'a update configurable term

    (** [config_opt set conv name] is like {!config_opt},
        but to update values with [set]. *)
    val config_opt :
      ('b -> 'a -> 'a) ->
      'b conv ->
      string ->
        'a update configurable term

    (** [opt_config set conv ~doc name] is like {!opt_config},
        but to update values with [set]. *)
    val opt_config :
      ('b -> 'a -> 'a) ->
      'b conv ->
      doc:string ->
      string ->
        'a update configurable term

    (** [opt_config_opt set conv ~doc name] is like {!opt_config_opt},
        but to update values with [set]. *)
    val opt_config_opt :
      ('b -> 'a -> 'a) ->
      'b conv ->
      doc:string ->
      string ->
        'a update configurable term

    (** [flag_config set ~doc name] is like {!flag_config},
        but to update values with [set]. *)
    val flag_config :
      (bool -> 'a -> 'a) ->
      doc:string ->
      string ->
        'a update configurable term

    (** {2:updcapt Simultaneous updates and captures} *)
    module Capture :
      sig
        (** [constant get conv name] captures the value for [get],
            and does not update anything *)
        val constant :
          ('a -> 'b) ->
          'b conv ->
          string ->
            'a Capture.t update

        (** [ignore f] applies [f] with no capture. *)
        val ignore :
          ('a -> 'b) ->
            ('a Capture.t -> 'b Capture.t)

        (** map from capture update to capture update *)
        val map :
          ('a -> 'b) ->
          ('b -> 'a -> 'a) ->
          'b Capture.t update ->
            'a Capture.t update

        (** [update set capt] is an ['a] update with the value from [capt].
            Data is captured only if [capt] is [Some x]. *)
        val update :
          ('b -> 'a -> 'a) ->
          'b Capture.t option ->
            'a Capture.t update

        (** [update_always set capt] is an ['a] update
            with the value from [capt].
            The update always happens, and data is always captured. *)
        val update_always :
          ('b -> 'a -> 'a) ->
          'b Capture.t ->
            'a Capture.t update

        (** [update_capture_default get set conv name capt] is an ['a] update
            with the value from [capt].
            If [capt] is [None] then data is captured with [get]
            from the ['a] input. *)
        val update_capture_default :
          ('a -> 'b) ->
          ('b -> 'a -> 'a) ->
          'b conv ->
          string ->
          'b Capture.t option ->
            'a Capture.t update

        (** [opt get set ?fname conv ~doc name] is like {!opt_config}
            if a [fname] is given, and like {!opt} otherwise,
            with additional preparation of CSV output of the values read. *)
        val opt :
          ('a -> 'b) ->
          ('b -> 'a -> 'a) ->
          'b conv ->
          doc:string ->
          string ->
            'a Capture.t update term

        (** [config get set conv name] is like {!config} but with capture. *)
        val config :
          ('a -> 'b) ->
          ('b -> 'a -> 'a) ->
          'b conv ->
          string ->
            'a Capture.t update configurable term

        (** [config_opt] is like {!config_opt} with capture. *)
        val config_opt :
          ('a -> 'b) ->
          ('b -> 'a -> 'a) ->
          'b conv ->
          string ->
            'a Capture.t update configurable term

        (** [opt_config get set conv ~doc name] is like {!opt_config},
            but for updating, with capture. *)
        val opt_config :
          ('a -> 'b) ->
          ('b -> 'a -> 'a) ->
          'b conv ->
          doc:string ->
          string ->
            'a Capture.t update configurable term

        (** [opt_config_opt] is like {!opt_config_opt} with capture. *)
        val opt_config_opt :
          ('a -> 'b) ->
          ('b -> 'a -> 'a) ->
          'b conv ->
          doc:string ->
          string ->
            'a Capture.t update configurable term

        (** [flag_config get set ~doc name] is like {!flag_config},
            but for updating, with capture. *)
        val flag_config :
          ('a -> bool) ->
          (bool -> 'a -> 'a) ->
          doc:string ->
          string ->
            'a Capture.t update configurable term
      end
  end

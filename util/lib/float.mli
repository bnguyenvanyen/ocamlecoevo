(** Float with typed sign
  
    Distinction between positive, negative and [0, 1] values.
  
    Dedicated modules conserve the properties.
    The module {!Pos} for instance, contains functions that conserve
    typedness,
    so that {!Pos.add} requires two positive values,
    and returns a positive value,
    while {!add} can take a float of any type, and returns a float of
    undetermined sign.
  
    Unification of types is sometimes capricious. 
    The [narrow] functions ({!narrow}, {!Pos.narrow}, ...) can help.
  
    Safe input conversion functions are provided :
    - {!Pos.of_float} checks that the input is [>= 0.]
    - {!Proba.of_float} checks that the input is in [[0., 1.]]
    - {!Proba.of_pos} checks that the input is [<= 1.]
    - {!Neg.of_float} checks that the input is [< 0.]
  
    A few unsafe functions are also exposed (they have [_unsafe] in their name).
    - {!of_float_unsafe} to obtain a [t] type that can be unified with any other,
      from a [float]
    - {!promote_unsafe} to obtain a [t] type that can be unified with any other,
      from a less general [t]
  
    Operators (unary and infix) are provided in Op modules,
    {!Op}, {!Pos.Op}, {!Proba.Op} and {!Neg.Op}.
 *)


(** (phantom) type for float *)
type +'a t

(** unknown float type *)
type _float = [ `Float ]

(** any kind of float : most permissive type and so unsafe to give to values 
    (the only value really of this type is 0.) *)
type 'a anyfloat = [< `Float | `Neg | `Pos | `Proba > `Float ] as 'a

(** unknown positive type *)
type 'a pos = [< `Float | `Pos > `Float ] as 'a

(** any kind of positive value and so unsafe to give to values
    (the values of this type are the values in [[0., 1.]]) *)
type 'a anypos = [< `Float | `Pos | `Proba > `Float `Pos ] as 'a

type 'a proba = 'a anypos

(** any proba (actually a closed type) *)
type anyproba = [ `Float | `Pos | `Proba ]

(** unknown negative type *)
type 'a neg = [< `Float | `Neg > `Float ] as 'a

(** any kind of negative value *)
type anyneg = [ `Float | `Neg ]


(** weirder types that we need sometimes... *)
type 'a anynoneg = [< `Float | `Pos | `Proba > `Float] as 'a
type closed_pos = [ `Float | `Pos ]


(** an existential type to identify in which class we fall into *)
type identity =
  | Zero : _ anyfloat t -> identity
  | Proba : anyproba t -> identity
  | Pos : _ anypos t -> identity
  | Neg : anyneg t -> identity


(** [of_float x] is 'only' a [_float t] *)
val of_float : float -> _float t

(** [of_float_unsafe x] is 'any kind of' [t]. UNSAFE ! *)
val of_float_unsafe : float -> _ anyfloat t

val to_float : _ anyfloat t -> float

(** [narrow x] is 'only' a [_float t] *)
val narrow : _ anyfloat t -> _float t

(** [promote_unsafe x] is 'any kind of' float. UNSAFE ! *)
val promote_unsafe : _ anyfloat t -> _ anyfloat t


(** [identify x] is the {!identity} of [x].
    @raise Invalid_argument if [x] is [nan]. *)
val identify : _ anyfloat t -> identity


(** [zero] is [0.]. It can be used as any kind of float. *)
val zero : _ anyfloat t

(** [one] is [1.]. It can be used as any kind of positive value. *)
val one : _ proba t

(** [half] is [0.5]. *)
val half : _ proba t

(** [two] is [2.]. *)
val two : _ pos t

(** [three] is [3.]. *)
val three : _ pos t

(** [pi] is [4. *. atan 1.] *)
val pi : _ pos t

(** [neg_one] is [~-. 1.] *)
val neg_one : _ neg t

val infinity : _ pos t

val neg_infinity : _ neg t

val nan : _float t


(** [neg x] is [~-. x] *)
val neg : _ anyfloat t -> _float t

(** [invert x] is [1. /. x] *)
val invert : _ anyfloat t -> _float t

(** [add x y] is [x +. y] *)
val add : _ anyfloat t -> _ anyfloat t -> _float t

(** [sub x y] is [x -. y] *)
val sub : _ anyfloat t -> _ anyfloat t -> _float t

(** [mul x y] is [x *. y] *)
val mul : _ anyfloat t -> _ anyfloat t -> _float t

(** [div x y] is [x /. y] *)
val div : _ anyfloat t -> _ anyfloat t -> _float t

(** [abs x] is [abs_float x] *)
val abs : _ anyfloat t -> _ pos t

(** [sq x] is [x ** 2.], the result is positive. *)
val sq : _ anyfloat t -> _ pos t

(** [exp x] is the exponential of [x], the result is positive *)
val exp : _ anyfloat t -> _ pos t

(** [cos x] is the cosine of [x], see {!cos}. *)
val cos : _ anyfloat t -> _float t

(** [sin x] is the sine of [x], see {!sin}. *)
val sin : _ anyfloat t -> _float t

(** [tan x] is the tangent of [x], see {!tan}. *)
val tan : _ anyfloat t -> _float t

(** [pow x k] is [x] to the power [k]. *)
val pow : _ anyfloat t -> int -> _float t

(** [log x], see {!log}. *)
val log : _ anyfloat t -> _float t

(** [log1p x], see {!log1p}. *)
val log1p : _ anyfloat t -> _float t

(** [ceil x], see {!ceil}. *)
val ceil : _ anyfloat t -> _float t

(** [floor x], see {!floor}. *)
val floor : _ anyfloat t -> _float t


(** [classify x] is [classify_float (to_float x)] *)
val classify : _ anyfloat t -> fpclass

(** [is_nan x] is [true] iff [x] is not a number. *)
val is_nan : _ anyfloat t -> bool

(** [to_string x] is {!string_of_float} [x] *)
val to_string : _ anyfloat t -> string

(** [of_string s] is [Some {!float_of_string} s] if it returns,
    and [None] if it raises [Failure]. *)
val of_string : string -> _float t option

(** print any float BatIO style *)
val print : (_ anyfloat t, 'a) BatIO.printer

(** [compare] is the comparison function on floats. *)
val compare : _ anyfloat t -> _ anyfloat t -> int

(** [compare_approx ~tol] is a comparison function
    where [x] and [x'] compare equal if they are apart by less than [tol]. *)
val compare_approx :
  ?tol : _ anypos t ->
  _ anyfloat t ->
  _ anyfloat t ->
    int

(** [min x x'] is the smallest of [x] and [x']. *)
val min : _ anyfloat t -> _ anyfloat t -> _float t

(** [max x x'] is the greater of [x] and [x']. *)
val max : _ anyfloat t -> _ anyfloat t -> _float t

(** [positive_part x] is [x] if it is positive, or else [zero]. *)
val positive_part : _ anyfloat t -> _ pos t

(** [nearly_equal ?tol x x'] is [true] iff [|x - x'| < tol]. *)
val nearly_equal :
  ?tol : _ anypos t ->
  _ anyfloat t ->
  _ anyfloat t ->
    bool

(** [nearly_inferior ?tol x x'] is [true] iff [x <= x' + tol]. *)
val nearly_inferior :
  ?tol : _ anypos t ->
  _ anyfloat t ->
  _ anyfloat t ->
    bool


module Op :
  sig
    (** {1:unary Unary operators} *)

    (** [(~+)] is {!of_float} *)
    val (~+) : float -> _float t

    (** [(~-)] is {!neg} *)
    val (~-) : _ anyfloat t -> _float t

    (** {2:infix Infix operators} *)

    (** [(+)] is {!add} *)
    val (+) : _ anyfloat t -> _ anyfloat t -> _float t

    (** [(-)] is {!sub} *)
    val (-) : _ anyfloat t -> _ anyfloat t -> _float t

    (** [( * )] is {!mul} *)
    val ( * ) : _ anyfloat t -> _ anyfloat t -> _float t

    (** [(/)] is {!div} *)
    val (/) : _ anyfloat t -> _ anyfloat t -> _float t

    (** Comparisons without type unification *)

    (** [(=)] is the equality operator *)
    val (=) : _ anyfloat t -> _ anyfloat t -> bool

    (** [(<>)] is the non-equality operator *)
    val (<>) : _ anyfloat t -> _ anyfloat t -> bool

    (** [(=~)] is the approximate equality operator *)
    val (=~) : _ anyfloat t -> _ anyfloat t -> bool


    (** [(<)] is the strictly-smaller operator *)
    val (<) : _ anyfloat t -> _ anyfloat t -> bool

    (** [(<=)] is the smaller-or-equal operator *)
    val (<=) : _ anyfloat t -> _ anyfloat t -> bool

    (** [(>)] is the strictly-greater operator *)
    val (>) : _ anyfloat t -> _ anyfloat t -> bool

    (** [(>=)] is the greater-or-equal operator *)
    val (>=) : _ anyfloat t -> _ anyfloat t -> bool

    (** [(<=~)] is the approximately-smaller operator *)
    val (<=~) : _ anyfloat t -> _ anyfloat t -> bool
  end


module Pos :
  sig
    (** [of_float x] is [x] as a [pos t] if [x] is positive ([>= 0.]).
        @raise Invalid_argument if [x] is negative. *)
    val of_float : float -> _ pos t

    (** [of_string s] is {!of_string} of [s] if the result is positive.
        @raise Invalid_argument if the result is negative. *)
    val of_string : string -> _ pos t option

    (** [of_anyfloat x] is [x] as a [pos t] if [x] is positive ([>= 0.]).
        @raise Invalid_argument if [x] is negative. *)
    val of_anyfloat : _ anyfloat t -> _ pos t

    (** [narrow x] is [x] as "only" a [minpos t] *)
    val narrow : _ anypos t -> _ pos t

    val close : _ anypos t -> closed_pos t


    (** [neg] is {!neg} with a negative result *)
    val neg : _ anypos t -> _ neg t

    (** [invert] is {!invert} on positive numbers *)
    val invert : _ anypos t -> _ pos t

    (** [add] is {!add} with a positive result *)
    val add : _ anypos t -> _ anypos t -> _ pos t

    (** [mul] is {!mul} with a positive result *)
    val mul : _ anypos t -> _ anypos t -> _ pos t

    (** [div] is {!div} with a positive result *)
    val div : _ anypos t -> _ anypos t -> _ pos t

    (** [sqrt x] is the square root of [x] *)
    val sqrt : _ anypos t -> _ pos t

    (** [log x] is the logarithm of [x] *)
    val log : _ anypos t -> _float t

    (** [log1p x] is [log (1 + x)] *)
    val log1p : _ anypos t -> _ pos t

    (** [pow] is {!pow} with a positive result *)
    val pow : _ anypos t -> int -> _ pos t

    (** [expow x y] is [x ** y = exp (y log x)] *)
    val expow : _ anypos t -> _ anyfloat t -> _ pos t

    (** [gamma] is {!Base.gamma} with a positive argument and result *)
    val gamma : _ anypos t -> _ pos t

    (** [loggamma] is a typed {!Base.loggamma} *)
    val loggamma : _ anypos t -> _float t

    (** [bcos b x] is [b + b cos x] *)
    val bcos : _ anypos t -> _ anyfloat t -> _ pos t

    (** [bsin b x] is [b + b sin x] *)
    val bsin : _ anypos t -> _ anyfloat t -> _ pos t

    (** [proportions a b] is [(a + b, a / (a + b), b / (a + b))] *)
    val proportions : _ anypos t -> _ anypos t ->
      _ pos t * _ proba t * _ proba t

    (** [min] is {!min} for positive values. *)
    val min : _ anypos t -> _ anypos t -> _ pos t

    (** [max] is {!max} for positive values. *)
    val max : _ anypos t -> _ anyfloat t -> _ pos t

    module Op :
      sig
        val (~-) : _ anypos t -> _ neg t

        val (+) : _ anypos t -> _ anypos t -> _ pos t
        val ( * ) : _ anypos t -> _ anypos t -> _ pos t
        val (/) : _ anypos t -> _ anypos t -> _ pos t

        val ( ** ) : _ anypos t -> _ anyfloat t -> _ pos t
      end
  end


module Proba :
  sig
    (** [of_float x] is [x] as a [proba t] if [x] is in [[zero, one]].
        @raise Invalid_argument otherwise. *)
    val of_float : float -> _ proba t

    (** [of_anyfloat x] is [x] as a [proba t]
        if [x] is positive ([>= zero]) and inferior to {!one},
        @raise Invalid_argument otherwise. *)
    val of_anyfloat : _ anyfloat t -> _ proba t

    (** [of_pos x] is [x] as a [proba t] if [x] is inferior to [one].
        @raise Invalid_argument if [x > one]. *)
    val of_pos : _ anypos t -> _ proba t

    (** [of_string s] is {!of_string} of [s] if the result is positive,
        else [Invalid_argument] is raised. *)
    val of_string : string -> _ proba t option

    val narrow : anyproba t -> _ proba t

    (** [compl p] is [1. -. p] *)
    val compl : anyproba t -> _ proba t


    (** [mul] is {!mul} *)
    val mul : anyproba t -> anyproba t -> _ proba t

    (** [log p] is the logarithm of p, and is negative *)
    val log : anyproba t -> _ neg t


    module Op :
      sig
        val ( * ) : anyproba t -> anyproba t -> _ proba t
      end
  end


module Neg :
  sig
    (** [of_float x] is [x] if [x] is strictly negative,
        @raise Invalid_argument if [x >= zero]. *)
    val of_float : float -> _ neg t

    (** [of_anyfloat x] is [x] as a [neg t]
        if [x] is negative ([x < zero]),
        @raise Invalid_argument if [x >= zero]. *)
    val of_anyfloat : _ anyfloat t -> _ proba t

    val narrow : anyneg t -> _ neg t

    (** [one] is {!neg_one} *)
    val one : anyneg t

    (** [infinity] is {!neg_infinity} *)
    val infinity : anyneg t


    val neg : anyneg t -> _ pos t


    val add : anyneg t -> anyneg t -> _ neg t

    val mul : anyneg t -> anyneg t -> _ pos t

    val div : anyneg t -> anyneg t -> _ pos t

    (** [exp x] is the exponential of [x] and is between [0.] and [1.] *)
    val exp : anyneg t -> 'a proba t

    (** [min] is {!min} for negative values. *)
    val min : anyneg t -> _ anyfloat t -> _ neg t

    (** [max] is {!max} for negative values. *)
    val max : anyneg t -> anyneg t -> _ neg t

    module Op :
      sig
        val (+) : anyneg t -> anyneg t -> _ neg t
        val ( * ) : anyneg t -> anyneg t -> _ pos t
        val (/) : anyneg t -> anyneg t -> _ pos t
      end
  end

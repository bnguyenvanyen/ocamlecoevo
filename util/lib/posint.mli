(** Positive int values *)

type t

(** [of_int x] raises [Invalid_argument] if [x] is not positive. *)
val of_int : int -> t

val of_int_unsafe : int -> t

val to_int : t -> int

val to_pos : t -> Pos.t

val zero : t
val one : t

val succ : t -> t
val add : t -> t -> t
val mul : t -> t -> t

val factorial : t -> t

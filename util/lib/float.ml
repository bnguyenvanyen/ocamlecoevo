type 'a t = float

type _float = [ `Float ]

type 'a anyfloat = [< `Float | `Neg | `Pos | `Proba > `Float ] as 'a

type 'a pos = [< `Float | `Pos > `Float ] as 'a

type 'a anypos = [< `Float | `Pos | `Proba > `Float `Pos ] as 'a

type 'a proba = 'a anypos

type anyproba = [ `Float | `Pos | `Proba ]

type 'a neg = [< `Float | `Neg > `Float ] as 'a

type anyneg = [ `Float | `Neg ]

type 'a anynoneg = [< `Float | `Pos | `Proba > `Float] as 'a

type closed_pos = [ `Float | `Pos ]

type identity =
  | Zero : _ anyfloat t -> identity
  | Proba : anyproba t -> identity
  | Pos : _ anypos t -> identity
  | Neg : anyneg t -> identity


let of_float x = x [@@ inline]

let of_float_unsafe x = x [@@ inline]

let to_float x = x [@@ inline]

let narrow x = x [@@ inline]

let promote_unsafe x = x [@@ inline]

let identify x =
  if x = 0. then
    Zero x
  else if x < 0. then
    Neg x
  else if x < 1. then
    Proba x
  else if x > 1. then
    Pos x
  else (* nan *)
    (* I don't know what to do for nans *)
    invalid_arg "nan"


let zero = 0.

let one = 1.

let half = 0.5

let two = 2.

let three = 3.

let pi = Base.pi

let neg_one = ~-. 1.

let infinity = infinity

let neg_infinity = neg_infinity

let nan = nan


let neg = (~-.)

let invert x = 1. /. x

let add x y = x +. y  [@@ inline]

let sub x y = x -. y  [@@ inline]

let mul x y = x *. y  [@@ inline]

let div x y = x /. y  [@@ inline]

let abs x = abs_float x [@@ inline]

let sq = Base.sq

let exp = exp

let cos = cos

let sin  = sin
 
let tan = tan

let pow = Base.pow

let log = log

let log1p = log1p

let ceil = ceil

let floor = floor


let classify = classify_float

let is_nan = Stdlib.Float.is_nan

let to_string = string_of_float

let of_string = Option.no_fail_opt float_of_string

let print = BatFloat.print

let compare (x : float) (x' : float) =
  compare x x'
  [@@ inline]

let min (x : float) (x' : float) =
  min x x'
  [@@ inline]

let max (x : float) (x' : float) =
  max x x'
  [@@ inline]

let positive_part (x : float) =
  max 0. x
  [@@ inline]

let nearly_equal ?tol x y =
  Base.nearly_equal ?tol x y
  [@@ inline]

let compare_approx ?tol x y =
  if nearly_equal ?tol x y then
    0
  else
    compare x y
  [@@ inline]

let nearly_inferior ?tol x y =
  Base.nearly_inferior ?tol x y
  [@@ inline]


module Op =
  struct
    (* might be confusing, but convenient *)
    let (~+) x = of_float x  [@@ inline]
    let (~-) x = neg x  [@@ inline]

    let (+) x y = add x y  [@@ inline]
    let (-) x y = sub x y  [@@ inline]
    let ( * ) x y = mul x y  [@@ inline]
    let (/) x y = div x y  [@@ inline]

    let (=) (x : float) (x' : float) = (x = x')  [@@ inline]
    let (=~) (x : float) (x' : float) = (nearly_equal x x')  [@@ inline]
    let (<>) (x : float) (x' : float) = (x <> x')  [@@ inline]
    let (<) (x : float) (x' : float) = (x < x')  [@@ inline]
    let (<=) (x : float) (x' : float) = (x <= x')  [@@ inline]
    let (>) (x : float) (x' : float) = (x > x')  [@@ inline]
    let (>=) (x : float) (x' : float) = (x >= x')  [@@ inline]
    let (<=~) (x : float) (x' : float) = (nearly_inferior x x')  [@@ inline]
  end


module Pos =
  struct
    let of_float x =
      if x >= 0. then
        x
      else
        invalid_arg "Strictly negative"
      [@@ inline]

    let of_string s = Option.map of_float (of_string s)

    let of_anyfloat x = of_float x  [@@ inline]

    let narrow x = x  [@@ inline]

    let close x = x  [@@ inline]

    let neg x = neg x  [@@ inline]

    let invert x = invert x  [@@ inline]

    let add x y = add x y  [@@ inline]

    let mul x y = mul x y  [@@ inline]

    let div x y = div x y  [@@ inline]

    let sqrt = sqrt

    let log = log

    let log1p = log1p

    let pow = pow

    let expow x y = x ** y

    let gamma = Base.gamma

    let loggamma = Base.loggamma

    let bcos b x = b +. b *. cos x

    let bsin b x = b +. b *. sin x

    let proportions a b =
      let tot = a +. b in
      (tot, a /. tot, b /. tot)


    let min x y = min x y  [@@ inline]

    let max x y = max x y  [@@ inline]

    module Op =
      struct
        let (~-) x = neg x  [@@ inline]

        let (+) x y = add x y  [@@ inline]

        let ( * ) x y = mul x y  [@@ inline]

        let (/) x y = div x y  [@@ inline]

        let ( ** ) x y = expow x y  [@@ inline]
      end
  end


module Proba =
  struct
    let of_float x =
      if x <= 1. then
        Pos.of_float x
      else
        invalid_arg "Bigger than 1"
      [@@ inline]
    
    let of_anyfloat x = of_float x  [@@ inline]

    let of_pos x =
      if x <= 1. then
        x
      else
        invalid_arg "Bigger than 1"
      [@@ inline]

    let of_string s = Option.map of_float (of_string s)

    let narrow x = x [@@ inline]

    let compl p = (1. -. p)  [@@ inline]

    let mul x y = mul x y  [@@ inline]

    let log = log

    module Op =
      struct
        let ( * ) x y = mul x y  [@@ inline]
      end
  end


module Neg =
  struct
    let of_float x =
      if x <= 0. then
        x
      else
        invalid_arg "Positive"
      [@@ inline]

    let of_anyfloat x = of_float x  [@@ inline]

    let narrow x = x [@@ inline]

    let one = neg_one

    let infinity = neg_infinity

    let neg x = neg x  [@@ inline]

    let add x y = add x y  [@@ inline]

    let mul x y = mul x y  [@@ inline]

    let div x y = div x y  [@@ inline]

    let exp = exp

    let min x y = min x y  [@@ inline]

    let max x y = max x y [@@ inline]

    module Op =
      struct
        let (+) x y = add x y  [@@ inline]
        let ( * ) x y = mul x y  [@@ inline]
        let (/) x y = div x y  [@@ inline]
      end
  end

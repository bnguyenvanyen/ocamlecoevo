open Sig

module B = Base


type pos_def_mat =
  | Cov of Lac.Mat.t
  | Chol of Lac.Mat.t
  | Cov_chol of {
      cov : Lac.Mat.t ;
      chol : Lac.Mat.t ;
    }
  | Ccdi of {
      cov : Lac.Mat.t ;
      chol : Lac.Mat.t ;
      det : float ;
      inv : Lac.Mat.t ;
    }
  | Ccldi of {
      cov : Lac.Mat.t ;
      chol : Lac.Mat.t ;
      logdet : float ;
      inv : Lac.Mat.t ;
    }


type covariance = pos_def_mat ref


let covchol c =
  Lac.syrk ~up:false c

let cov_to_mat cov =
  match !cov with
  | Chol c ->
      covchol c
  | Cov cov
  | Cov_chol { cov ; _ }
  | Ccdi { cov ; _ }
  | Ccldi { cov ; _ } ->
      cov


let mat_to_cov c = ref (Chol c)


let dim1 cov =
  match !cov with
  | Chol c ->
      Lac.Mat.dim1 c
  | Cov cov
  | Cov_chol { cov ; _ }
  | Ccdi { cov ; _ }
  | Ccldi { cov ; _ } ->
      Lac.Mat.dim1 cov


let cholesky ?(triang=false) mat =
  let chol = Lac.lacpy mat in
  Lac.potrf ~up:false chol ;
  (* set the upper triangle to 0s *)
  if triang then Lac.lacpy ~uplo:`L chol else chol


(* note that this is the inverse of the covariance matrix,
 * computed from its Cholesky factorization.
 * (and not the inverse of the Cholesky factorization) *)
let chol_inv chol =
  let inv = Lac.lacpy chol in
  Lac.potri ~up:false inv ;
  Lac.Mat.detri ~up:false inv ;
  inv


(* Code transcribed from ramcmc *)
let chol_update ~(up:[`Up | `Down]) a u =
  (* a should be lower triangular *)
  let fr =
    match up with
    | `Up ->
        (fun x y -> sqrt (B.sq x +. B.sq y))
    | `Down ->
        (fun x y -> sqrt (B.sq x -. B.sq y))
  in
  let plus_minus =
    match up with
    | `Up ->
        (~+.)
    | `Down ->
        (~-.)
  in
  assert ((Lac.Vec.dim u = Lac.Mat.dim1 a)
       && (Lac.Vec.dim u = Lac.Mat.dim2 a)) ;
  let n = Lac.Vec.dim u in
  for i = 1 to n - 1 do
    let r = fr a.{i,i} u.{i} in
    let c = r /. a.{i,i} in
    let s = u.{i} /. a.{i,i} in
    (* plus or minus s *)
    let pms = plus_minus s in
    (* a update *)
    (* a.{i+1:n,i} <- (a.{i+1:n,i} +. s *. u.{i+1:n}) /. c ; *)
    a.{i,i} <- r ;
    let ai = Lac.Mat.col a i in
    (* starting element *)
    let ip1 = i + 1 in
    (* number of elements to affect *)
    let k = n - i in
    Lac.axpy ~alpha:pms ~n:k ~ofsx:ip1 u ~ofsy:ip1 ai ;
    Lac.scal ~n:k ~ofsx:ip1 (1. /. c) ai ;
    (* u update *)
    (* u.{i+1:n} <- c *. u.{i+1:n} -. s *. a.{i+1:n,i} *)
    Lac.scal c ~n:k ~ofsx:ip1 u ;
    Lac.axpy ~alpha:(~-. s) ~n:k ~ofsx:ip1 ai ~ofsy:ip1 u 
  done ;
  a.{n, n} <- fr a.{n, n} u.{n}


let detinv c =
  let diag = Lac.Mat.copy_diag c in
  let det = B.sq (Lac.Vec.prod diag) in
  if det = 0. then begin
    for i = 1 to Lac.Vec.dim diag do
      Printf.eprintf "%f " diag.{i}
    done ;
    Printf.eprintf "\n" ;
  end ;
  let inv = chol_inv c in
  det, inv


let logdet_inv c =
  let diag = Lac.Mat.copy_diag c in
  let log_det = 2. *. (Lac.Vec.sum (Lac.Vec.log diag)) in
  let inv = chol_inv c in
  log_det, inv


let to_cov cov =
  match !cov with
  | Chol lchol ->
      covchol lchol
  | Cov c
  | Cov_chol { cov = c ; _ }
  | Ccdi { cov = c ; _ }
  | Ccldi { cov = c ; _ } ->
      c


let up_to_ccdi cov =
  match !cov with
  | Cov c ->
      let lchol = cholesky c in
      let det, inv = detinv lchol in
      cov := Ccdi { cov = c ; chol = lchol ; det ; inv } ;
      c, lchol, det, inv
  | Chol lchol ->
      let c = covchol lchol in
      let det, inv = detinv lchol in
      cov := Ccdi { cov = c ; chol = lchol ; det ; inv } ;
      c, lchol, det, inv
  | Cov_chol { cov = c ; chol = lchol } ->
      let det, inv = detinv lchol in
      cov := Ccdi { cov = c ; chol = lchol ; det ; inv } ;
      c, lchol, det, inv
  | Ccdi { cov = c ; chol = lchol ; det ; inv ; _ } ->
      c, lchol, det, inv
  | Ccldi { cov = c ; chol = lchol ; logdet ; inv ; _ } ->
      c, lchol, exp logdet, inv


let up_to_ccldi cov =
  match !cov with
  | Cov c ->
      let lchol = cholesky c in
      let logdet, inv = logdet_inv lchol in
      cov := Ccldi { cov = c ; chol = lchol ; logdet ; inv } ;
      c, lchol, logdet, inv
  | Chol lchol ->
      let c = covchol lchol in
      let logdet, inv = logdet_inv lchol in
      cov := Ccldi { cov = c ; chol = lchol ; logdet ; inv } ;
      c, lchol, logdet, inv
  | Cov_chol { cov = c ; chol = lchol } ->
      let logdet, inv = logdet_inv lchol in
      cov := Ccldi { cov = c ; chol = lchol ; logdet ; inv } ;
      c, lchol, logdet, inv
  | Ccdi { cov = c ; chol ; det ; inv ; _ } ->
      c, chol, log det, inv
  | Ccldi { cov = c ; chol ; logdet ; inv ; _ } ->
      c, chol, logdet, inv


let get_chol cov =
  match !cov with
  | Cov c ->
      let chol = cholesky c in
      cov := Cov_chol { cov = c ; chol } ;
      chol
  | Chol chol
  | Cov_chol { chol ; _ }
  | Ccdi { chol ; _ }
  | Ccldi { chol ; _ } ->
      chol


let common_multi_normal mean cov v =
  (* if lchol, lower triangular matrix obtained by Cholesky decomposition
   * is given, it is used, otherwise it is computed. *)
  let n = Lac.Vec.dim v in
  let _, _, det, inv = up_to_ccdi cov in
  (* FIXME inv, the precision matrix,
   * probably has features that we could use *)
  let a = 1. /. (sqrt ((B.pow (2. *. B.pi) n) *. det)) in
  (* FIXME could reuse storage vector *)
  let centered =
    match mean with
    | `Centered ->
        v
    | `Mean m ->
        Lac.Vec.sub v m
  in
  let dotted = Lac.gemv inv centered in
  (* FIXME I think *)
  let sqmaha = Lac.dot dotted centered in
  let e = ~-. sqmaha /. 2. in
  (a, e)


let common_multi_normal_std x =
  let n = Lac.Vec.dim x in
  let a = 1. /. (sqrt (B.pow (2. *. B.pi) n)) in
  let sqmaha = Lac.dot x x in
  let e = ~-. sqmaha /. 2. in
  (a, e)


let d_multi_normal m cov v : 'a Float.pos Float.t =
  let a, e = common_multi_normal (`Mean m) cov v in
  Float.of_float_unsafe (a *. exp e)


let d_multi_normal_center cov v : 'a Float.pos Float.t =
  let a, e = common_multi_normal `Centered cov v in
  Float.of_float_unsafe (a *. exp e)


let d_multi_normal_std v : 'a Float.pos Float.t =
  let a, e = common_multi_normal_std v in
  Float.of_float_unsafe (a *. exp e)


let logd_multi_normal_general mean cov v =
  let n = Lac.Vec.dim v in
  let _, _, log_det, inv = up_to_ccldi cov in
  let loga = ~-. 1. /. 2. *. (float n *. (log 2. +. log B.pi) +. log_det) in
  let centered =
    match mean with
    | `Centered ->
        v
    | `Mean m ->
        Lac.Vec.sub v m
  in
  let dotted = Lac.gemv inv centered in
  let sqmaha = Lac.dot dotted centered in
  let e = ~-. sqmaha /. 2. in
  Float.of_float (loga +. e)


let logd_multi_normal_center cov v =
  logd_multi_normal_general `Centered cov v


let logd_multi_normal m cov v =
  logd_multi_normal_general (`Mean m) cov v


(*
let logd_multi_normal m cov v =
  let a, e = common_multi_normal m cov v in
  Float.of_float (log a +. e)
*)


let logd_multi_normal_std v =
  let a, e = common_multi_normal_std v in
  Float.of_float (log a +. e)


let update_mean_estimate m k v =
  (* m <- ((k - 1) m + v) / k *)
  Lac.scal (float (k - 1)) m ;
  Lac.axpy v m ;
  Lac.scal (1. /. float k) m


let single_wishart_mle ?y m v =
  let n = Lac.Vec.dim m in
  (* FIXME find way to reuse centered memory *)
  let centered = Lac.Vec.sub v m in
  let mat =
    match y with
    | None -> Lac.Mat.make0 n n
    | Some y -> y
  in
  Lac.syr ~up:false centered mat ;
  mat


let update_cov_mle cov m k v =
  (* assume real mean (or mean estimate already updated) *)
  (* cov <- ((k - 1) cov + w) / k *)
  let c = to_cov cov in
  Lac.Mat.scal (float (k - 1)) c ;
  let c = single_wishart_mle ~y:c m v in
  Lac.Mat.scal (1. /. float k) c ;
  (* reinitialize cov (forget chol etc) *)
  cov := Cov c


(* condition on the values of [x] on [1..stop]
 * by changing the values in [u] *)
(* TODO there should be a way to do less work here,
 * when only *some* values of [x] change *)
let condition_on ?stop ~cov ~x u =
  (* x = a * u *)
  (* u = a^-1 x *)
  (* -> solve the system a u = x *)
  let u = Lac.copy ~y:u ?n:stop x in
  let chol = get_chol cov in
  Lac.trsv ~up:false ?n:stop chol u


(* (re)draw a start->stop block in u *)
let rand_standard_block ~rng ?(start=1) ?stop u =
  let stop =
    match stop with
    | None ->
        Lac.Vec.dim u
    | Some n ->
        assert (n <= Lac.Vec.dim u) ;
        n
  in
  for i = start to stop do
    u.{i} <- Rand.rand_normal ~rng 0. Float.one
  done


(* maybe create, and return the vector with the draw *)
let rand_multi_normal_std ~rng ?store ?start n =
  let x =
    match store with
    | None -> Lac.Vec.make0 n
    | Some v -> v
  in
  rand_standard_block ~rng ?start x ;
  x


let len_block start stop =
  match start, stop with
  | None, None
  | Some _, None ->
      None
  | None, Some stop ->
      Some stop
  | Some start, Some stop ->
      Some (stop - start + 1)


let rand_multi_normal_center ~rng ?start ?stop ~cov ~u x =
  let a = get_chol cov in
  (* get a N(0, 1) random vector *)
  rand_standard_block ~rng ?start ?stop u ;
  let n = len_block start stop in
  let x = Lac.copy ~y:x ?n ?ofsx:start ?ofsy:start u in
  (* (lower) triangular matrix vector product *)
  Lac.trmv ~up:false ?n ?ar:start ?ac:start ?ofsx:start a x


(* what do we do in the positive semi-definite case ? I'm not sure *)
let rand_multi_normal ~rng ?start ?stop ~m ~cov ~u x =
  rand_multi_normal_center ~rng ?start ?stop ~cov ~u x ;
  let n = len_block start stop in
  Lac.Vec.add ~z:x ?n ?ofsx:start ?ofsy:start ?ofsz:start m x


(* draw marginal for the first [stop] variables *)
let rand_multi_normal_marginal ~rng ?stop ~m ~cov ~u x =
  rand_multi_normal ~rng ?stop ~m ~cov ~u x


(* draw for the last n variables, conditional on the previous ones.
 * the previous draw is in u *)
let rand_multi_normal_conditional ~rng ?start ~m ~cov ~u x =
  (* redraw the last n block in u *)
  rand_standard_block ~rng ?start u ;
  (* do the full multiplication and sum *)
  let a = get_chol cov in
  let x = Lac.copy ~y:x u in
  (* x now holds the contents of u *)
  Lac.trmv ~up:false a x ;
  Lac.Vec.add ~z:x m x


let conditional_mean_cov ?start ~m ~cov x =
  (* center the conditioned upon variables *)
  let c = cov_to_mat cov in
  (* block size of the conditioned upon (left) block *)
  let n1 = Option.map (fun k -> k - 1) start in
  (* block size of the conditional (right) block *)
  let n2 = Option.map (fun k -> Lac.Vec.dim x - k + 1) start in
  let centered = Lac.Vec.sub ?n:n1 x m in
  (* m_|start = m[start:] + cov * cov_cross[start:]^-1 * centered *)
  (* I use gemv, gemm because simple *)
  (* a = cov[start:,:start] * cov[:start,:start]^-1 *)
  let a = Lac.gemm ?m:n2 ?n:n1 ?k:n1 ?ac:start c c in
  let cond_mean = Lac.copy ?n:n2 ?ofsx:start m in
  (* m + a * centered *)
  let cond_mean = Lac.gemv ~beta:1. ~y:cond_mean ?m:n2 ?n:n1 a centered in
  let cond_cov = Lac.lacpy ?n:n2 ?m:n2 ?ar:start ?ac:start c in
  (* compute inverse of the conditioned upon block *)
  let inv = Lac.lacpy ?n:n1 c in
  Lac.trtri ~up:false ?n:n1 inv ;
  let a = Lac.gemm ?m:n1 ?n:n2 ?k:n1 inv ?bc:start c in
  let cond_cov = Lac.gemm
    ~alpha:(~-.1.) ?m:n2 ?n:n2 ?k:n1 ?ar:start c a
    ~beta:1. ~c:cond_cov
  in
  cond_mean, ref (Cov cond_cov)  


(* conventional way to draw from the conditional distribution of
 * x[start:] given x[:start] *)
let rand_multi_normal_cond_notrick ~rng ?start ~m ~cov ~u x =
  let cond_mean, cond_cov = conditional_mean_cov ?start ~m ~cov x in
  let n =
    match start with
    | None ->
        Lac.Vec.dim m
    | Some k ->
        Lac.Vec.dim m - k + 1
  in
  (* can't use u x and start stop because
   * the dims of u x and m cov are incompatible *)
  let u' = Lac.Vec.make0 n in
  let x' = Lac.Vec.make0 n in
  let x' =  rand_multi_normal ~rng ~m:cond_mean ~cov:cond_cov ~u:u' x' in
  (* put the values in u and x *)
  let u = Lac.copy ~y:u ?ofsy:start u' in
  ignore u ;
  let x = Lac.copy ~y:x ?ofsy:start x' in
  x

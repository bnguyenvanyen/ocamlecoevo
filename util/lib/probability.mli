(** Probability values *)

type t

(** [of_float x] raises [Invalid_argument] if [x] is not in [[0, 1]]. *)
val of_float : float -> t

val of_pos : Pos.t -> t

(** [of_float_unsafe x] is actually the identity function,
 *  and as such, UNSAFE ! *)
val of_float_unsafe : float -> t

val of_pos_unsafe : Pos.t -> t

val to_float : t -> float

val to_pos : t -> Pos.t

val zero : t
val one : t

(* [compl p] is [1 - p] *)
val compl : t -> t

(* [mul p p'] is [p * p'] *)
val mul : t -> t -> t

val sq : t -> t

val sqrt : t -> t

val pow : t -> Posint.t -> t

val neglog : t -> Pos.t

(* [p * p'] is {!mul} [p p']. *)
val ( * ) : t -> t -> t

val ( ** ) : t -> Pos.t -> t

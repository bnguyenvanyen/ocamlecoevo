type t = int

let of_int x : t =
  if x >= 0 then
    x
  else
    invalid_arg "Negative"

let of_int_unsafe x = x

let to_int x = x

let to_pos x = Pos.of_float_unsafe (float x)

let zero = 0

let one = 1


let succ = succ

let add = (+)

let mul = ( * )

let factorial = Base.factorial

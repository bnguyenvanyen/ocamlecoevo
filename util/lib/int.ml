

type 'a t = int


type _int = [ `Int ]

type 'a anyint = [< `Int | `Pos | `Neg > `Int] as 'a

type 'a pos = [< `Int | `Pos > `Int ] as 'a

type anypos = [ `Int | `Pos ]

type 'a neg = [< `Int | `Neg > `Int ] as 'a

type anyneg = [ `Int | `Neg ]



let of_int n = n [@@ inline]

let of_int_unsafe n = n [@@ inline]

let to_int n = n [@@ inline]

let to_float n = Float.of_float (float n)

let of_float x = int_of_float (Float.to_float x)

let ceil x = int_of_float (ceil (Float.to_float x))

let narrow x = x [@@ inline]

let promote_unsafe x = x [@@ inline]


let zero = 0

let one = 1

let two = 2

let neg_one = ~- 1


let succ n = succ n  [@@ inline]

let pred n = pred n  [@@ inline]

let neg n = (~-) n  [@@ inline]

let add n m = (+) n m [@@ inline]

let sub n m = (-) n m [@@ inline]

let mul n m = ( * ) n m [@@ inline]

let div n m = (/) n m [@@ inline]

let to_string = string_of_int

let of_string = Option.no_fail_opt int_of_string

let compare (i : int) (i' : int) = compare i i'  [@@ inline]


module Op =
  struct
    let (~+) n = of_int n  [@@ inline]
    let (~-) n = neg n  [@@ inline]

    let (+) n m = add n m  [@@ inline]
    let (-) n m = sub n m  [@@ inline]
    let ( * ) n m = mul n m  [@@ inline]
    let (/) n m = div n m  [@@ inline]

    let (mod) n m = (mod) n m  [@@ inline]

    let (=) (i : int) (i' : int) = (i = i')  [@@ inline]
    let (<>) (i : int) (i' : int) = (i <> i')  [@@ inline]
    let (<) (i : int) (i' : int) = (i < i')  [@@ inline]
    let (<=) (i : int) (i' : int) = (i <= i')  [@@ inline]
    let (>) (i : int) (i' : int) = (i > i')  [@@ inline]
    let (>=) (i : int) (i' : int) = (i >= i')  [@@ inline]
  end


module Pos =
  struct
    let of_int n =
      if n >= 0 then
        n
      else
        invalid_arg "Strictly negative"
      [@@ inline]

    let of_anyint n = of_int n  [@@ inline]

    let to_float n = Float.of_float_unsafe (float n)  [@@ inline]

    let of_float x = int_of_float (Float.to_float x)  [@@ inline]

    let of_string s = Option.map of_int (of_string s)  [@@ inline]

    let ceil x = int_of_float (Stdlib.ceil (Float.to_float x))  [@@ inline]

    let narrow n = n [@@ inline]

    let succ n = succ n  [@@ inline]

    let pred n =
      if n > 0 then
        pred n
      else
        invalid_arg "Int.Pos.pred : 0"
      [@@ inline]

    let neg n = neg n  [@@ inline]

    let add n m = add n m  [@@ inline]

    let mul n m = mul n m  [@@ inline]

    let div n m = div n m  [@@ inline]

    let factorial = Base.factorial

    let logfact k : 'a Float.pos Float.t =
      Float.of_float_unsafe (Base.logfact k)

    let fold = Base.int_fold

    let list_init ~f ~n =
      BatList.init n f

    let array_init ~f ~n =
      BatArray.init n f

    module Op =
      struct
        let (~-) n = neg n  [@@ inline]

        let (+) n m = add n m  [@@ inline]
        let ( * ) n m = mul n m  [@@ inline]
        let (/) n m = div n m  [@@ inline]

        let (mod) n m = (mod) n m  [@@ inline]
      end
  end


module Neg =
  struct
    let of_int n =
      if n < 0 then
        n
      else
        invalid_arg "Positive"
      [@@ inline]

    let to_float n = Float.of_float_unsafe (float n)  [@@ inline]

    let narrow n = n  [@@ inline]


    let one = (~- 1)

    let pred n = pred n  [@@ inline]

    let neg n = neg n  [@@ inline]

    let add n m = add n m  [@@ inline]

    let mul n m = mul n m  [@@ inline]


    module Op =
      struct
        let (~-) n = neg n  [@@ inline]

        let (+) n m = add n m  [@@ inline]
        let ( * ) n m = mul n m  [@@ inline]
      end
  end

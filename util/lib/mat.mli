open Sig

type return = [
  | `Fresh
  | `Reuse
  | `Into of Lac.Mat.t
]


val dot_diag_mat :
  return: return ->
  Lac.Vec.t ->
  Lac.Mat.t ->
    Lac.Mat.t


val dot_mat_diag :
  return: return ->
  Lac.Mat.t ->
  Lac.Vec.t ->
    Lac.Mat.t


val dediag :
  return: return ->
  work: Lac.Mat.t ->
  Lac.Mat.t ->
  Lac.Vec.t ->
    Lac.Mat.t


val sym_dediag :
  return: return ->
  Lac.Mat.t ->
  Lac.Vec.t ->
    Lac.Mat.t


(** [sym_compute_root ~up sy] computes the principal square root of 
    the symmetric matrix [sy] (in the same memory space). *)
val sym_compute_root :
  up : bool ->
  Lac.Mat.t ->
    Lac.Mat.t


(** [sym_linear_flow ~up a] is the linear flow operator [t -> e^(t a)]
    for the symmetric matrix [a]. *)
val sym_linear_flow :
  up : bool ->
  Lac.Mat.t ->
    (_ Float.anypos Float.t -> Lac.Mat.t)


(** [gen_linear_flow a] is the linear flow operator [t -> e^(t a)]
    for the real matrix [a].
    [a] should be diagonalizable with real eigenvalues. *)
val gen_linear_flow :
  Lac.Mat.t ->
    (_ Float.anypos Float.t -> Lac.Mat.t)

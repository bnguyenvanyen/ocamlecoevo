(** Sugar on top of Cmdliner for CSV config file optional input.
 *
 *  - conversion of util float and int types
 *  - terms with CSV config file optional input
 *  - updater terms with associated CSV optional input and output
 *)

open Sig

module M = BatMap
module C = Cmdliner


type 'a term = 'a C.Term.t
type 'a conv = 'a C.Arg.conv

type 'a configurable =
  (config:string option -> 'a)


let positive () =
  let parse_float = C.Arg.(conv_parser float) in
  let print_float = C.Arg.(conv_printer float) in
  let parse s =
    match parse_float s with
    | Ok x ->
        begin match Float.Pos.of_float x with
        | y ->
            Ok y
        | exception Invalid_argument s ->
            Error (`Msg s)
        end
    | (Error _) as err ->
        err
  in
  let print fmt x =
    print_float fmt (Float.to_float x)
  in
  C.Arg.conv ~docv:"POS" (parse, print)


let proba () =
  let parse_float = C.Arg.(conv_parser float) in
  let print_float = C.Arg.(conv_printer float) in
  let parse s =
    match parse_float s with
    | Ok x ->
        begin match Float.Proba.of_float x with
        | y ->
            Ok y
        | exception Invalid_argument s ->
            Error (`Msg s)
        end
    | (Error _) as err ->
        err
  in
  let print fmt x =
    print_float fmt (Float.to_float x)
  in
  C.Arg.conv ~docv:"PROBA" (parse, print)


let posint () =
  let parse_int = C.Arg.(conv_parser int) in
  let print_int = C.Arg.(conv_printer int) in
  let parse s =
    match parse_int s with
    | Ok n ->
        begin match Int.Pos.of_int n with
        | m ->
            Ok m
        | exception Invalid_argument s ->
            Error (`Msg s)
        end
    | (Error _) as err ->
        err
  in
  let print fmt x =
    print_int fmt (Int.to_int x)
  in
  C.Arg.conv ~docv:"POSINT" (parse, print)


let combine_opt read fname =
  function
  | Some x ->
      Some x
  | None ->
      begin match fname with
      | None ->
          None
      | Some path ->
          Some (read path)
      end


let combine_opt_opt read fname =
  function
  | Some x ->
      Some x
  | None ->
      Option.bind fname read


let parse_csv p row name =
  match Util__Csv.Row.find_exn row name with
  | s ->
      p s
  | exception Invalid_argument msg ->
      Error (`Msg msg)


let parse_csv_opt p row name =
  match Util__Csv.Row.find row name with
  | Some s ->
      Result.map (fun x -> Some x) (p s)
  | None ->
      Ok None


let read_csv name parse path =
  let c = Util__Csv.open_in path in
  let res = parse_csv parse (Csv.Rows.next c) name in
  match res with
  | Ok x ->
      x
  | Error (`Msg msg) ->
      failwith msg


let read_csv_opt name parse path =
  let c = Util__Csv.open_in path in
  let res = parse_csv_opt parse (Csv.Rows.next c) name in
  match res with
  | Ok x ->
      x
  | Error (`Msg msg) ->
      failwith msg


let opt conv ~doc name =
  let docv = String.uppercase_ascii name in
  let info = C.Arg.info ~docv ~doc [name] in
  let arg = C.Arg.opt (C.Arg.some conv) None info in
  C.Arg.value arg


(* get from file if filename given *)
let config conv name =
  let p = C.Arg.conv_parser conv in
  let read config =
    read_csv name p config
  in
  C.Term.(const (fun ~config -> Option.map read config))


(* get from file if filename given and column present *)
let config_opt conv name =
  let p = C.Arg.conv_parser conv in
  let read path =
    read_csv_opt name p path
  in
  C.Term.(const (fun ~config -> Option.bind config read))


(* get from command line if given, or from file if given *)
let opt_config conv ~doc name =
  let term = opt conv ~doc name in
  let p = C.Arg.conv_parser conv in
  let read path =
    read_csv name p path
  in
  C.Term.(const (fun x ~config -> combine_opt read config x) $ term)


let opt_config_opt conv ~doc name =
  let term = opt conv ~doc name in
  let p = C.Arg.conv_parser conv in
  let read path =
    read_csv_opt name p path
  in
  C.Term.(const (fun x ~config -> combine_opt_opt read config x) $ term)


let flag_config ~doc name =
  let docv = String.uppercase_ascii name in
  let term = C.Arg.(
    value
    & flag
    & info [name] ~doc ~docv
  )
  in
  let p = C.Arg.conv_parser C.Arg.bool in
  let read path =
    read_csv name p path
  in
  let combine_bool fname flag =
    if flag then
      flag
    else
      begin match fname with
      | None ->
          false
      | Some path ->
          read path
      end
  in
  C.Term.(const (fun x ~config -> combine_bool config x) $ term)


module Capture =
  struct
    (* captured values as a map name->value *)
    type captured = string M.String.t
    type 'a t = 'a * captured

    let lift_conv name conv =
      let parse = C.Arg.conv_parser conv in
      let print = C.Arg.conv_printer conv in
      let docv = C.Arg.conv_docv conv in
      let parse s =
        match parse s with
        | Ok x ->
            Ok (x, M.String.singleton name s)
        | (Error _) as error ->
            error
      in
      let print fmt (x, _) =
        print fmt x
      in
      C.Arg.conv ~docv (parse, print)

    let empty x =
      (x, M.String.empty)

    let output =
      function
        | None ->
            (fun (y, _) -> y)
        | Some fname ->
            (fun (y, m) ->
              let cols, vals = L.split (M.String.bindings m) in
              let c = Util__Csv.open_out fname in
              Csv.output_record c cols ;
              Csv.output_record c vals ;
              Util__Csv.close_out c ;
              y
            )

    let merge_distinct =
      M.String.merge (fun _ x x' ->
        match x, x' with
        | Some _, Some _ ->
            failwith "capture on both sides"
        | None, None ->
            None
        | Some y, None
        | None, Some y ->
            Some y
      )

    let merge_keep_left =
      M.String.merge (fun _ x x' ->
        match x, x' with
        | Some y, Some _ ->
            Some y
        | None, None ->
            None
        | Some y, None
        | None, Some y ->
            Some y
      )

    let merge_keep_right =
      M.String.merge (fun _ x x' ->
        match x, x' with
        | Some _, Some y ->
            Some y
        | None, None ->
            None
        | Some y, None
        | None, Some y ->
            Some y
      )

    (* apply and merge, refuse conflicts *)
    let apply (f, mf) (x, mx) =
      (f x, merge_distinct mf mx)

    let apply_keep_left (f, mf) (x, mx) =
      (f x, merge_keep_left mf mx)

    let apply_keep_right (f, mf) (x, mx) =
      (f x, merge_keep_right mf mx)

    let ($) = apply
    let ($<) = apply_keep_left
    let ($>) = apply_keep_right

    (* arg terms *)
    (* don't even need to go through special stuff *)
    let opt conv ~doc name =
      opt (lift_conv name conv) ~doc name

    let config conv name =
      config (lift_conv name conv) name

    let config_opt conv name =
      config_opt (lift_conv name conv) name

    let opt_config conv ~doc name =
      opt_config (lift_conv name conv) ~doc name

    let opt_config_opt conv ~doc name =
      opt_config_opt (lift_conv name conv) ~doc name

    (* more annoying, needs a rewrite *)
    let flag_config ~doc name =
      let docv = String.uppercase_ascii name in
      let term = C.Arg.(
        value
        & flag
        & info [name] ~doc ~docv
      )
      in
      let cv = lift_conv name C.Arg.bool in
      let parse = C.Arg.conv_parser cv in
      let read path =
        read_csv name parse path
      in
      (* Not very clean code *)
      let combine_bool fname flag =
        if flag then
          (flag, M.String.singleton name "true")
        else
          begin match fname with
          | None ->
              (flag, M.String.empty)
          | Some path ->
              read path
          end
      in
      C.Term.(const (fun x ~config -> combine_bool config x) $ term)
  end


module Update =
  struct
    type 'a update = ('a -> 'a)

    let print_string cv_print s =
      cv_print Format.str_formatter s ;
      Format.flush_str_formatter ()

    let update_with set =
      function
      | None ->
          (fun y -> y)
      | Some x ->
          set x

    let config set conv name =
      let term = config conv name in
      let update get ~config =
        update_with set (get ~config)
      in
      C.Term.(const update $ term)

    let config_opt set conv name =
      let term = config_opt conv name in
      let update get ~config =
        update_with set (get ~config)
      in
      C.Term.(const update $ term)

    module Capture =
      struct
        let constant get conv name =
          let print = print_string (C.Arg.conv_printer conv) in
          (fun (y, m) ->
            (y, M.String.add name (print (get y)) m)
          )

        let ignore upd =
          let update x =
            Capture.(empty upd $ x)
          in
          update

        let map get set f (y, data) =
          let x = get y in
          let (x, data) = f (x, data) in
          (set x y, data)

        let update set =
          function
          | None ->
              (fun y -> y)
          | Some x ->
              (fun y -> Capture.(empty set $ x $ y))

        let update_always set x y =
          Capture.(empty set $ x $ y)

        let update_capture_default get set conv name =
          function
          | None ->
              constant get conv name
          | Some x ->
              (fun y -> Capture.(empty set $ x $ y))
    
        let opt get set conv ~doc name =
          let sub = Capture.opt conv ~doc name in
          let f = update_capture_default get set conv name in
          C.Term.(const f $ sub)

        let config get set conv name =
          let sub = Capture.config conv name in
          let f read ~config =
            update_capture_default get set conv name (read ~config)
          in
          C.Term.(const f $ sub)

        let config_opt get set conv name =
          let sub = Capture.config_opt conv name in
          let f read ~config =
            update_capture_default get set conv name (read ~config)
          in
          C.Term.(const f $ sub)

        let opt_config get set conv ~doc name =
          let sub = Capture.opt_config conv ~doc name in
          let f read ~config =
            update_capture_default get set conv name (read ~config)
          in
          C.Term.(const f $ sub)

        let opt_config_opt get set conv ~doc name =
          let sub = Capture.opt_config_opt conv ~doc name in
          let f read ~config =
            update_capture_default get set conv name (read ~config)
          in
          C.Term.(const f $ sub)

        let flag_config _get set ~doc name =
          let sub = Capture.flag_config ~doc name in
          let f read ~config y =
            (* avoid weird open+pun *)
            let from = config in
            Capture.(empty set $ read ~config:from $ y)
          in
          C.Term.(const f $ sub)
      end

    (* don't shadow base opt in Capture *)

    let opt set conv ~doc name =
      let term = opt conv ~doc name in
      C.Term.(const (update_with set) $ term)

    let opt_config set conv ~doc name =
      let term = opt_config conv ~doc name in
      let update get ~config =
        update_with set (get ~config)
      in
      C.Term.(const update $ term)

    let opt_config_opt set conv ~doc name =
      let term = opt_config_opt conv ~doc name in
      let update get ~config =
        update_with set (get ~config)
      in
      C.Term.(const update $ term)

    let flag_config set ~doc name =
      let term = flag_config ~doc name in
      let update get ~config =
        set (get ~config)
      in
      C.Term.(const update $ term)
  end

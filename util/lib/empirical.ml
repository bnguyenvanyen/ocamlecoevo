(* might use BatArray instead ? *)

open Sig
open Base

module F = Float

let every k =
  L.filteri (fun i _ -> (i mod k = 0))

let mean xs =
  let fn = float (List.length xs) in
  L.fsum xs /. fn

let variance xs =
  let m = mean xs in
  mean @@ (L.map (fun x -> sq (x -. m))) @@ xs

let make_cdf compare xs =
  let n = L.length xs in
  let k = max 1 (n / 100) in
  let sorted_xs = L.sort compare xs in
  let less_xs = every k sorted_xs in
  let cdf y =
    (* find first x >= y *)
    let i, _ = L.findi (fun _ x -> compare x y >= 0) less_xs in
    float i /. 100.
  in cdf


(* rough from estimate, not smoothed *)
let quantiles ?(n=100) xs =
  let len = L.length xs in
  int_fold (fun q k -> (L.at xs (len * (k - 1) / n)) :: q) [] n




let make_density ?(floor=0.) compare d xs =
  let n = L.length xs in
  let k = max 1 (n / 100) in
  let fn = float n in
  let fk = float k in
  let sorted_xs = L.sort compare xs in
  let less_xs = every k sorted_xs in
  let density y =
    match L.findi (fun _ x -> compare x y >= 0) less_xs with
    | i, next_x ->
      (match L.at less_xs (i - 1) with
       | prev_x ->
           fk /. (fn *. (d prev_x next_x))
       | exception Not_found ->
           floor)
    | exception Not_found ->
        floor
  in density


let make_log_density ?(floor=neg_infinity) compare d xs =
  let n = L.length xs in
  let k = max 1 (n / 100) in
  let fn = float n in
  let fk = float k in
  let lfn = log fn in
  let lfk = log fk in
  let sorted_xs = L.sort compare xs in
  let less_xs = every k sorted_xs in
  let log_density y =
    match L.findi (fun _ x -> compare x y >= 0) less_xs with
    | i, next_x ->
      (match L.at less_xs (i - 1) with
       | prev_x ->
           lfk -. lfn -. log (d prev_x next_x)
       | exception Not_found ->
           floor
       | exception (Invalid_argument _) ->
           floor)
    | exception Not_found ->
        floor
    | exception (Invalid_argument _) ->
        floor
  in log_density


let make_normal_density xs =
  let n = L.length xs in
  let fn = float n in
  let posn = F.of_float_unsafe fn in
  let m = (L.fold_left (+.) 0. xs) /. fn in
  let sqds = L.map (fun x -> F.sq (F.of_float (x -. m))) xs in
  let v = F.Pos.div (L.fold_left F.Pos.add (F.Pos.narrow F.zero) sqds) posn in
  let nd y = Proba.d_normal m v y in
  nd


let make_normal_log_density xs =
  let n = L.length xs in
  let fn = float n in
  let posn = F.of_float_unsafe fn in
  let m = (L.fold_left (+.) 0. xs) /. fn in
  let sqds = L.map (fun x -> F.sq (F.of_float (x -. m))) xs in
  let v = F.Pos.div (L.fold_left F.Pos.add (F.Pos.narrow F.zero) sqds) posn in
  let nd y = Proba.logd_normal m v y in
  nd


let make_gamma_density ?(n_cv=1) xs =
  let n = L.length xs in
  let fn = float n in
  let lxs = L.map log xs in
  let s = log (L.fsum xs /. fn) -. (L.fsum lxs) /. fn in
  let bad_k = (3. -. s +. sqrt (sq (s -. 3.) +. 24. *. s)) /. (12. *. s) in
  (* FIXME we don't have digamma trigamma *)
  let nr_update k =
    k -. (log k -. digamma k -. s) /. (1. /. k -. trigamma k)
  in
  let rec f =
    function
    | 0 ->
        (fun k -> k)
    | n when n < 0 ->
        invalid_arg "negative n"
    | n ->
        (fun k -> f (n - 1) (nr_update k))
  in f n_cv bad_k


module FM = BatMap.Make (struct
  type t = float
  let compare = compare
end)


let lin_local_intp f m xl xr =
  let yl = f xl in
  let yr = f xr in
  (* maybe bad ? *)
  let a =
    (yr -. yl) /. (xr -. xl)
  in
  (* assert (classify_float a <> FP_nan) ; *)
  let g x =
    let y = yl +. a *. (x -. xl) in
    if classify_float y = FP_nan then
      Printf.eprintf "Evaluated to nan in %f, where xl = %f\n" x xl ;
    y
  in FM.add xl g m


let lin_local_intp_opt f m xl xr =
  let bad y =
    let cl = classify_float y in
    (cl = FP_nan) || (cl = FP_infinite)
  in
  let yl = f xl in
  let yr = f xr in
  (* maybe bad ? *)
  let a =
    (yr -. yl) /. (xr -. xl)
  in
  (* assert (classify_float a <> FP_nan) ; *)
  let g x =
    let y = yl +. a *. (x -. xl) in
    if bad y then
      Printf.eprintf
      "Evaluated to %f in %f, where xl = %f and yl = %f and a = %f\n"
      y x xl yl a ;
    y
  in
  let go =
    if (bad yl) || (bad yr) then
      None
    else
      Some g
  in FM.add xl go m


let lin_intp ~at f =
  l_fold_succ (lin_local_intp f) FM.empty at


let lin_behaved_intp ~at f =
  (* we reuse the nearest well-behaved estimation *)
  (* only get "good" interpolations *)
  let intp_opts = l_fold_succ (lin_local_intp_opt f) FM.empty at in
  (* replace None by nearest Some, and drop option *)
  let good_intps = FM.filter_map (fun _ yo -> yo) intp_opts in
  FM.mapi (fun x ->
    function
    | Some g ->
        g
    | None ->
        let lower, xo, greater = FM.split x good_intps in
        (* xo should necessarily be None *)
        assert (xo = None) ;
        (* check which is closer of lower and greater *)
        (* FIXME might be Not_found *)
        match FM.max_binding lower with
        | (xl, gl) ->
            (match FM.min_binding greater with
             | (xr, gr) ->
                 if (x -. xl) < (xr -. x) then
                   gl
                 else
                   gr
             | exception (Not_found | Invalid_argument _) ->
                 gl)
        | exception (Not_found | Invalid_argument _) ->
            (match FM.min_binding greater with
             | (_, gr) ->
                 gr
             | exception (Not_found | Invalid_argument _) ->
                 failwith "Empty")
  ) intp_opts


let intp_of_tag =
  function
  | `Linear ->
      lin_intp
  | `Behave ->
      lin_behaved_intp

(* basic linear interpolation for now *)
let interpolate ?(mode=`Linear) ~at f =
  let ref_vals = (intp_of_tag mode) ~at f in
  let intp x =
    let lower, xo, greater = FM.split x ref_vals in
    let g =
      match xo with
      | Some g ->
          g
      | None ->
          (match FM.max_binding lower with
           | _, g ->
               g
           | exception (Not_found | Invalid_argument _) ->
               (match FM.min_binding greater with
                | _, g ->
                    g
                | exception (Not_found | Invalid_argument _) ->
                    failwith "Empty"))

    in g x
  in intp

  

(* don't necessarily need this type *)
(* functions for kde taken from wikipedia *)
type kernel_tag = [
  | `Uniform
  | `Triangular
  | `Epanechnikov
  | `Quartic
  | `Triweight
  | `Tricube
  | `Gaussian
  | `Cosine
  | `Logistic
  | `Sigmoid
  | `Silverman
]


let in_support f u =
  if (u >= ~-. 1.) && (u <= 1.) then
    f u
  else
    0.


let kernel_of_tag =
  function
  | `Uniform ->
      in_support (fun _ -> 1. /. 2.)
  | `Triangular ->
      in_support (fun u -> 1. -. abs_float u)
  | `Epanechnikov ->
      in_support (fun u -> 3. /. 4. *. (1. -. sq u))
  | `Quartic ->
      in_support (fun u -> 15. /. 16. *. sq (1. -. sq u))
  | `Triweight ->
      in_support (fun u -> 35. /. 32. *. cub (1. -. sq u))
  | `Tricube ->
      in_support (fun u -> 70. /. 81. *. cub (1. -. cub (abs_float u)))
  | `Gaussian ->
      (fun u -> 1. /. sqrt (2. *. pi) *. exp (~-. 1. /. 2. *. sq u))
  | `Cosine ->
      in_support (fun u -> pi /. 4. *. cos (pi /. 2. *. u))
  | `Logistic ->
      (fun u -> 1. /. (exp u +. 2. +. exp (~-. u)))
  | `Sigmoid ->
      (fun u -> 2. /. (pi *. (exp u +. exp (~-. u))))
  | `Silverman ->
      let sqrt2 = sqrt 2. in
      (fun u ->
        let normed_u = abs_float u /. sqrt2 in
        1. /. 2. *. exp (~-. normed_u) *. sin (normed_u +. pi /. 4.))


let estimate_density
  ~h ?(interpol=true) ?(kernel=`Epanechnikov) xs =
  (* gives negative values ? how ? problem with kernel ? *)
  let k = kernel_of_tag kernel in
  let h = F.to_float h in
  let invnh = 1. /. (float (List.length xs) *. h) in
  let f x =
    invnh *. L.fsum (L.map (fun xi ->
      k ((x -. xi) /. h)
    ) xs)
  in
  let f' =
    if interpol then
      let xmin = L.min xs in
      let xmax = L.max xs in
      let sd = sqrt @@ variance @@ xs in
      let at = L.frange (xmin -. sd) `To (xmax +. sd) 100 in
      (* let at = quantiles xs in *)
      interpolate ~at f
    else
      f
  (* FIXME a bit annoying *)
  in (fun x -> x |> F.to_float |> f' |> F.of_float)


let estimate_log_density
  ~h ?(interpol=true) ?(kernel=`Gaussian) xs =
  let k = kernel_of_tag kernel in
  let h = F.to_float h in
  let linvnh =
    ((~-.) @@ log @@ float @@ L.length @@ xs) -. log h
  in
  let f x =
    linvnh +. log (L.fsum (L.map (fun xi ->
      k ((x -. xi) /. h)
    ) xs))
  in
  let f' =
    if interpol then
      let xmin = L.min xs in
      let xmax = L.max xs in
      let sd = sqrt @@ variance @@ xs in
      let at = L.frange (xmin -. sd) `To (xmax +. sd) 100 in
      (* let at = quantiles xs in *)
      (* FIXME need a guarantee that approx on the left and right go down *)
      interpolate (* ~mode:`Behave *) ~at f
    else
      f
  in (fun x -> x |> F.to_float |> f' |> F.of_float)


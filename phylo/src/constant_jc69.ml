open Cmdliner
module PC = Phylo.Generic.Make (Phylo.Constant) (Phylo.Jc69)

let () =
  Printexc.record_backtrace true ;
  Util.Gc.tune () ;
  Term.exit @@ Term.eval (PC.Term.run, PC.Term.info)

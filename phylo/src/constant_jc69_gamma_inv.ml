open Cmdliner
module X = Phylo.Generic.Make (Phylo.Constant) (Phylo.Jc69_gamma_inv)

let () =
  Printexc.record_backtrace true ;
  Util.Gc.tune () ;
  Term.exit @@ Term.eval (X.Term.run, X.Term.info)

open Cmdliner
module PS = Phylo.Generic.Make (Phylo.Sirs) (Phylo.Jc69)

let () =
  Printexc.record_backtrace true ;
  Util.Gc.tune () ;
  Term.exit @@ Term.eval (PS.Term.run, PS.Term.info)

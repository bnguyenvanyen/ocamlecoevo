open Cmdliner
module PC = Phylo.Constant_jc69

let () =
  Printexc.record_backtrace true ;
  Util.Gc.tune () ;
  Term.exit @@ Term.eval (
    PC.Term.evaluate_proposal,
    PC.Term.info_eval_prop
  )

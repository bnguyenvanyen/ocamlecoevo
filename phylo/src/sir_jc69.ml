open Cmdliner
module PS = Phylo.Gen_sir_jc69

let () =
  Printexc.record_backtrace true ;
  Util.Gc.tune () ;
  Term.exit @@ Term.eval (PS.Term.run, PS.Term.info)

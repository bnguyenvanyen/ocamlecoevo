let read_one_timed_tree chan =
  let first = input_line chan in
  let k = Scanf.sscanf first ">%i" (fun k -> k) in
  let newick = input_line chan in
  let tree = Tree.Lt.of_newick newick in
  assert (input_line chan = "") ;
  (k, tree)


let read_tree_on_line ?(line=2) chan =
  for _ = 1 to line - 1 do
    ignore (input_line chan)
  done
  ;
  let newick = input_line chan in
  Tree.Lt.of_newick newick


let read_timed_trees chan =
  let rec f () =
    match read_one_timed_tree chan with
    | (k, Some tree) ->
        (k, tree) :: (f ())
    | (_, None) ->
        failwith "invalid newick string"
    | exception End_of_file ->
        []
  in f ()


let relabel tree =
  (* not good enough ? *)
  let lv = List.map (fun (_, tk) -> tk) (Tree.Lt.leaves tree) in
  let sorted = List.sort (fun i i' -> compare i' i) lv in
  Tree.Lt.map_case
    ~leaf:(fun (t, k) ->
      let i, _ = BatList.findi (fun _ (_, k') -> k = k') sorted in
      (t, i + 1)
    )
    ~node:(fun x -> x)
    ~binode:(fun x -> x)
    tree




let output_trace path kds =
  let columns = ["k" ; "distance" ; "root_time" ; "branch_length"] in
  let extract (k, d, rt, bl) =
    function
    | "k" ->
        string_of_int k
    | "distance" ->
        string_of_float d
    | "root_time" ->
        string_of_float rt
    | "branch_length" ->
        string_of_float bl
    | c ->
        invalid_arg (Printf.sprintf "unrecognized column : %s" c)
  in
  Util.Csv.output
    ~columns
    ~extract
    ~path
    kds


let output_trace_no_distance path kds =
  let columns = ["k" ; "root_time" ; "branch_length"] in
  let extract (k, rt, bl) =
    function
    | "k" ->
        string_of_int k
    | "root_time" ->
        string_of_float rt
    | "branch_length" ->
        string_of_float bl
    | c ->
        invalid_arg (Printf.sprintf "unrecognized column : %s" c)
  in
  Util.Csv.output
    ~columns
    ~extract
    ~path
    kds


module Kc = Tree.Kendall_colijn.Make (Tree.Lt)


let root_time tree =
  Tree.Lt.case
    ~binode:(fun _ _ _ -> failwith "unrooted tree")
    ~leaf:(fun _ -> failwith "unrooted tree")
    ~node:(fun _ stree -> Tree.Lt.State.to_time (Tree.Lt.root_value stree))
    tree


let run path_target target_line path_samples no_distance out =
  let ktrees =
    let c = open_in path_samples in
    let ktrees = read_timed_trees c in
    close_in c ;
    ktrees
  in
  if no_distance then
    let kds = List.map (fun (k, tr) ->
      (k,
       root_time tr,
       Tree.Lt.branch_length tr)
    ) ktrees
    in
    output_trace_no_distance out kds
  else
    let tree =
      let c = open_in path_target in
      match read_tree_on_line ?line:target_line c with
      | Some tree ->
          close_in c ;
          relabel tree
      | None ->
          failwith "could not read newick input"
    in
    let kds = List.map (fun (k, tr) ->
      (k,
       Kc.distance 0.5 tr tree,
       root_time tr,
       Tree.Lt.branch_length tr)
    ) ktrees
    in
    output_trace out kds


module Term =
  struct
    module C = Cmdliner

    let path_target =
      let doc =
        "Path to 'tnewick' file containing target tree."
      in
      C.Arg.(
        required
        & opt (some string) None
        & info ["path-target"] ~docv:"PATH-TARGET" ~doc
      )

    let path_samples =
      let doc =
        "Path to 'tnewick' file containing samples to score."
      in
      C.Arg.(
        required
        & opt (some string) None
        & info ["path-samples"] ~docv:"PATH-SAMPLES" ~doc
      )

    let target_line =
      let doc =
        "Line to read target tree from. By default 2."
      in
      C.Arg.(
        value
        & opt (some int) None
        & info ["target-line"] ~docv:"TARGET-LINE" ~doc
      )

    let no_distance =
      let doc =
        "Flag to not compute the Kendall-Colijn distance."
      in
      C.Arg.(
        value
        & flag
        & info ["no-distance"] ~docv:"NO-DISTANCE" ~doc
      )

    let out =
      let doc =
        "Path to CSV file to output to."
      in
      C.Arg.(
        required
        & pos 0 (some string) None
        & info [] ~docv:"OUT" ~doc
      )

    let run =
      C.Term.(
        const run
        $ path_target
        $ target_line
        $ path_samples
        $ no_distance
        $ out
      )

    let info =
      let doc =
        "Plot the trace of the Kendall-Colijn distance to some target tree."
      in
      C.Term.info
        "phylo-trace-kc"
        ~version:"%%VERSION%%"
        ~doc
        ~exits:C.Term.default_exits
        ~man:[]
  end


let () =
  Printexc.record_backtrace true ;
  Cmdliner.Term.exit @@ Cmdliner.Term.eval (Term.run, Term.info)

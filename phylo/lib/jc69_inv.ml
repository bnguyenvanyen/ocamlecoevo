open Abbrevs


module S = Seqsim

module Q = S.With_invariant.Make (S.Jc69)


let name = "JC69+Inv"


module Hyper =
  struct
    class t = object
      inherit Jc69.Hyper.mu_able
      inherit With_inv.Hyper.t
    end

    let default = new t

    let ncat _ =
      (* + inv *)
      2

    let open_term () =
      let f jc69_stuff pi_j ~config hy =
        hy
        |> jc69_stuff ~config
        |> pi_j ~config
      in
      Cmdliner.Term.(
        const f
        $ Jc69.Hyper.open_term ()
        $ With_inv.Hyper.Term.p_inv_jump ()
      )

    type 'a term =
      'a
      U.Arg.Capture.t
      U.Arg.configurable
      Cmdliner.Term.t

    let term : t term =
      let f update ~config =
        new t
        |> U.Arg.Capture.empty
        |> update ~config
      in
      Cmdliner.Term.(
        const f
        $ open_term ()
      )
  end


module Theta =
  struct
    type t = Q.t

    module Infer =
      struct
        module I = Fit.Infer

        class t = object
          inherit Gtr.Theta.Infer.mu_able
          inherit With_inv.Theta.Infer.t
        end

        type tag = [
          | Jc69.Theta.Infer.tag
          | With_inv.Theta.Infer.tag
        ]

        let tags =
            Jc69.Theta.Infer.tags
          @ With_inv.Theta.Infer.tags

        let tag_to_string =
          function
          | #Jc69.Theta.Infer.tag as tag ->
              Jc69.Theta.Infer.tag_to_string tag
          | #With_inv.Theta.Infer.tag as tag ->
              With_inv.Theta.Infer.tag_to_string tag

        let tag_to_columns =
          function
          | #Jc69.Theta.Infer.tag as tag ->
              Jc69.Theta.Infer.tag_to_columns tag
          | #With_inv.Theta.Infer.tag as tag ->
              With_inv.Theta.Infer.tag_to_columns tag

        let default =
          new t

        let get =
          function
          | `Mu ->
              (fun infr -> infr#mu)
          | #With_inv.Theta.Infer.tag as tag ->
              With_inv.Theta.Infer.get tag

        let fix =
          function
          | `Mu ->
              (fun infr -> infr#with_mu I.fixed)
          | #With_inv.Theta.Infer.tag as tag ->
              With_inv.Theta.Infer.fix tag

        let infer =
          function
          | `Mu ->
              (fun infr -> infr#with_mu I.adapt)
          | #With_inv.Theta.Infer.tag as tag ->
              With_inv.Theta.Infer.infer tag
      end

    let columns (infr : Infer.t) =
      let f tag =
        if Fit.Infer.is_free (Infer.get tag infr) then
          Infer.tag_to_columns tag
        else
          []
      in
      L.fold_left (fun cs tag ->
        cs @ (f tag)
      ) [] Infer.tags

    let extract = Q.extract

    let mu = Q.mu

    let prior hy =
      let mu = Gtr.Theta.Prior.mu hy in
      let p_inv = With_inv.Theta.Prior.p_inv in
      let to_tuple (th : t) =
        th.sub.mu, F.to_float th.p_inv
      in
      let of_tuple (mu, p_inv) : t =
        let sub : S.Jc69.t = { mu } in
        let p_inv = F.Proba.of_float p_inv in
        { sub ; p_inv }
      in
      Fit.Dist.Map (
        to_tuple,
        of_tuple,
        Fit.Dist.pair mu p_inv
      )

    module Draws =
      struct
       let mu _ dx (th : t) : t =
         let mu = th.sub.mu +. dx in
         let sub : S.Jc69.t = { mu } in
         { th with sub }

        let p_inv _ dx (th : t) : t =
          let p_inv = F.to_float th.p_inv +. dx in
          match F.Proba.of_float p_inv with
          | p_inv ->
              { th with p_inv }
          | exception (Invalid_argument _ as e) ->
              Fit.raise_draw_error e

        let of_tag =
          function
          | `Mu ->
              [mu]
          | `P_inv ->
              [p_inv]
      end

    let draws infer =
      let f tag =
        if Fit.Infer.is_free_adapt (Infer.get tag infer) then
          Draws.of_tag tag
        else
          []
      in
      L.fold_left (fun ds tag ->
        ds @ (f tag)
      ) [] Infer.tags

    module Jumps =
      struct
        let of_tag =
          function
          | `Mu ->
              Gtr.Theta.Jumps.mu
          | `P_inv ->
              Gtr_gamma_inv.Theta.Jumps.p_inv
      end

    let jumps infer (hy : Hyper.t) =
      let f tag =
        if Fit.Infer.is_free_adapt (Infer.get tag infer) then
          Jumps.of_tag tag hy
        else
          []
      in
      L.fold_left (fun js tag ->
        js @ (f tag)
      ) [] Infer.tags

    let term =
      Q.capture_term
  end


type vec = Q.vec
type mat = Q.mat


let settings = Q.settings


let equilibrium_probas (th : Theta.t) =
  S.Jc69.equilibrium_probas th.sub


let transition_probas ?mat th =
  Q.transition_probas ?mat th


let flow ?mat th =
  let tp = transition_probas ?mat th in
  let trans dt =
    tp (F.Pos.of_float dt)
  in
  S.Probas.sym_transition settings trans

open Abbrevs


module Theta =
  struct
    module Param =
      struct
        class t = object
          val lambda_v = 1.
          val mu_v = 1.

          method lambda = lambda_v
          method mu = mu_v

          method with_lambda x = {< lambda_v = x >}
          method with_mu x = {< mu_v = x >}
        end

        let lambda = Coalfit.Param.lambda
        let mu = Coalfit.Param.mu

        let to_jc69 th =
          { Seqsim.Jc69.mu = th#mu }

        module Term =
          struct
            let lambda () =
              let doc =
                "Initial pairwise coalescence rate."
              in
              let get th = th#lambda in
              let set x th = th#with_lambda x in
              Epifit.Term.float_arg ~get ~set ~doc "lambda"

            let mu () =
              let doc =
                "Initial mutation rate."
              in
              let get th = th#mu in
              let set x th = th#with_mu x in
              Epifit.Term.float_arg ~get ~set ~doc "mu"
          end

        let term =
          let f lambda mu =
            (new t)
            |> U.Arg.Capture.empty
            |> lambda ~config:None
            |> mu ~config:None
            |> U.Arg.Capture.output None
          in
          Cmdliner.Term.(
            const f
            $ Term.lambda ()
            $ Term.mu ()
          )
      end

    type t = Param.t * T.t * Lac.Mat.t M.Int.t
  end


module Hyper =
  struct
    class t = object
      inherit Coalfit.Param.prior
      inherit Hyper.proposal_weights_able
      inherit Hyper.scalable
      inherit Hyper.slide_root_able
      inherit Hyper.spr_div_scalable
      inherit Hyper.spr_guided_scalable
      inherit Hyper.ignore_sequences_able
    end

    let lambda_mean = Coalfit.Param.lambda_mean
    let lambda_var = Coalfit.Param.lambda_var

    let mu_mean = Coalfit.Param.mu_mean
    let mu_var = Coalfit.Param.mu_var

    let spr_div_scale hy =
      F.Pos.of_float hy#spr_div_scale

    let spr_guided_scale hy =
      F.Pos.of_float hy#spr_guided_scale

    let term =
      let f lbd_m lbd_v mu_m mu_v sc_j sr_j sd sg ig_seq pw config capture =
        (new t)
        |> U.Arg.Capture.empty
        |> lbd_m ~config
        |> lbd_v ~config
        |> mu_m ~config
        |> mu_v ~config
        |> sc_j ~config
        |> sr_j ~config
        |> sd ~config
        |> sg ~config
        |> ig_seq ~config
        |> U.Arg.Capture.output capture
        |> pw
      in
      Cmdliner.Term.(const f
        $ Coalfit.Term.Hyper.lambda_mean ()
        $ Coalfit.Term.Hyper.lambda_var ()
        $ Coalfit.Term.Hyper.mu_mean ()
        $ Coalfit.Term.Hyper.mu_var ()
        $ Hyper.Term.scale_jump ()
        $ Hyper.Term.slide_root_jump ()
        $ Hyper.Term.spr_div_scale ()
        $ Hyper.Term.spr_guided_scale ()
        $ Hyper.Term.ignore_sequences ()
        $ Hyper.Term.proposal_weights ()
        $ Coalfit.Term.Hyper.load_from
      )
end


module Data = Seq_data


module Sim =
  struct
    module S = Seqsim

    let stgs =
      Seqsim.Probas.Homogen

    let coal_tree_to_tree =
      T.of_coal_tree

    let draw_tree data rng lbd =
      T.draw data rng lbd

    let init_probas data =
      Probas.init stgs data 1

    let transition ?mat par =
      match S.Jc69.transition_probas ?mat par with
      | tp ->
          let trans_mat dt =
            tp (F.Pos.of_float dt)
          in
          S.Probas.sym_transition stgs trans_mat
      | exception (Invalid_argument _ as e) ->
          Fit.raise_draw_error e

    let compute_probas data =
      let mat = Probas.zero_transition stgs 1 in
      let pmem_4n = Probas.zero_proba stgs data 1 in
      let f par tree probas =
        let trans = transition ~mat par in
        Probas.compute
          stgs
          ~pmem_4n
          data
          trans
          tree
          probas
      in f

    let update_probas data hy =
      if hy#ignore_sequences then
        (fun _ _ probas -> probas)
      else
        let comp = compute_probas data in
        (fun th tree probas ->
          let par = Theta.Param.to_jc69 th in
          comp par tree probas
        )
  end


module Prior =
  struct
    module D = Fit.Dist

    (* FIXME Allow a flat prior ?
       This would correspond to 1/X on popsize in BEAST ? *)
    let lambda hy =
      let m = log (Hyper.lambda_mean hy) /. log 10. in
      D.Lognormal (m, Hyper.lambda_var hy)

    let mu hy =
      let m = log (Hyper.mu_mean hy) /. log 10. in
      D.Lognormal (m, Hyper.mu_var hy)

    let tree_log_dens_proba lambda tree =
      (* Work on nlineages intervals *)
      (* number of lineages on each interval from root to tip *)
      let nlineages = T.nlineages_backwards tree in
      (* look backwards, from the tip to the root *)
      let rev_nlineages = Jump.reverse nlineages in
      Jump.fold_succ_left (fun logd t n t' n' ->
          (* going back in time *)
          assert (t' < t) ;
          (* merge event rate *)
          let fn = F.Pos.of_float (float n) in
          let fnm1 = F.Pos.of_float (float (n - 1)) in
          (* total coal rate *)
          let lbd = F.Pos.Op.(lambda * fn * fnm1 / F.two) in
          let dlogd =
            if n' = n + 1 then
              (* sampling event *)
              (* proba of no merge event between t and t' *)
              (* e (- (lbd * (t - t'))) *)
              (* log proba: lbd * (t' - t) *)
              F.Op.(lbd * F.Neg.of_float (t' -. t))
            else if n' = n - 1 then
              (* merge event *)
              (* density of merge event at t *)
              U.logd_exp lbd (t -. t')
            else begin
              (* unexpected event *)
              (* typically several coalescences happen at the same time,
               * which is forbidden *)
              (* F.neg_infinity *)
              failwith "Unexpected event"
            end
          in
          F.Op.(logd + dlogd)
      ) F.zero rev_nlineages

    (* Constant coalescence rate prior, for now implemented here *)
    let tree lambda =
      (* alternative Gillespie simulation ?
       * I think it does not generalize *)
      let draw _ =
        failwith "Not_implemented"
      in
      let log_dens_proba tree =
        [F.to_float (tree_log_dens_proba lambda tree)]
      in
      Fit.Dist.Base { draw ; log_dens_proba }

    let probas data =
      let probas = Sim.init_probas data in
      Fit.Dist.Constant_flat probas
  end


module Likelihood =
  struct
    let loglik data =
      (* we never need to hold onto transition matrices,
       * so we can put them all in the same memory *)
      let stgs = Seqsim.Probas.Homogen in
      let mat = Probas.zero_transition stgs 1 in
      let eq = Seqsim.Jc69.equilibrium_probas () in
      let sstore_4 = Probas.zero_vectors stgs data 1 in
      let pmem_4n = Probas.zero_proba stgs data 1 in
      let smem_4 = Probas.zero_vector stgs 1 in
      let pmem_1n = Probas.zero_proba_1n stgs data 1 in
      let mmem_1n = Probas.zero_mat_1n data in
      let loglik = Probas.log_likelihood
        stgs
        ~sstore_4
        ~pmem_4n
        ~smem_4
        ~pmem_1n
        ~mmem_1n
        data
      in
      let comp par probas tree =
        let trans = Sim.transition ~mat par in
        loglik eq trans probas tree
      in
      comp

    let dist data hy =
      let draw _ =
        failwith "Not_implemented"
      in
      let log_dens_proba (th, tree, probas) =
        if hy#ignore_sequences then
          [0.]
        else begin
          let par = Theta.Param.to_jc69 th in
          loglik data par probas tree
        end
      in Fit.Dist.Base { draw ; log_dens_proba }
  end


module Propose =
  struct
    module P = Treefit.Propose.Make (T)
    module P' = Treefit.Propose_guided.Make (T)

    let scale _ dx (th, tree, probas) =
      let alpha = F.exp (F.of_float dx) in
      match P.scale alpha tree with
      | tree' ->
          (* no need to update probas
           * as it will get done in vector_draw *)
          (th, tree', probas)
      | exception (Invalid_argument _ as e) ->
          Fit.raise_draw_error e

    let scale_node data rng dx (th, tree, probas) =
      let nint = Data.ninterior data in
      let k = I.to_int (U.rand_int ~rng nint) in
      let alpha = F.exp (F.of_float dx) in
      match P.scale_node k alpha tree with
      | tree' ->
          (* no need to update probas
           * as it will get done in vector_draw *)
          (th, tree', probas)
      | exception (Invalid_argument _ as e) ->
          Fit.raise_draw_error e

    let slide_root _ dx (th, tree, probas) =
      match P.slide_node 0 dx tree with
      | _, tree' ->
          (* no need to update probas
           * as it will get done in vector_draw *)
          (th, tree', probas)
      | exception (Invalid_argument _ as e) ->
          Fit.raise_draw_error e


    let div_var data hy th =
      let m = F.of_float (float (Data.nsites data)) in
      let mu = Theta.Param.mu th in
      let sgm = Hyper.spr_div_scale hy in
      (* dimension of a time *)
      let d = F.mul sgm (F.invert (F.mul mu m)) in
      F.sq d

    let spr_div_log_pd_ratio (_, tree, _) (_, move, _) =
      [F.to_float (P.Spr.Divergence.log_pd_ratio tree move)]

    let spr_div_move_to_sample (th, move, probas) =
      (th, P.Spr.Divergence.move_to_sample move, probas)

    let spr_div data hy =
      let nint = Data.ninterior data in
      (* I copy the definitions from P.Spr.Divergence 
       * to get a varc that depends on theta through div_var *)
      let draw rng (th, tree, probas) =
        let v = div_var data hy th in
        match P.Spr.Divergence.draw nint v rng tree with
        | move ->
            let tree' = P.Spr.Divergence.move_to_sample move in
            let probas' = Sim.update_probas
              data
              hy
              th
              tree'
              probas
            in
            (th, move, probas')
        | exception (Failure _ as e) ->
            Fit.raise_draw_error e
      in
      let log_pd_ratio x x' =
        spr_div_log_pd_ratio x x'
      in
      let move_to_sample x =
        spr_div_move_to_sample x
      in
      Fit.Propose.Base { draw ; log_pd_ratio ; move_to_sample }

    let vdraw_spr_div data hy =
      let nint = Data.ninterior data in
      (fun rng v (th, tree, probas) ->
        let varc = F.abs (F.of_float v.{1}) in
        match P.Spr.Divergence.draw nint varc rng tree with
        | move ->
            let tree' = P.Spr.Divergence.move_to_sample move in
            let probas' = Sim.update_probas
              data
              hy
              th
              tree'
              probas
            in
            (th, move, probas')
        | exception (Failure _ as e) ->
            Fit.raise_draw_error e
      )

    let spr_unif data hy =
      let nint = Data.ninterior data in
      let draw rng (th, tree, probas) =
        let move = P.Spr.Uniform.draw nint rng tree in
        let tree' = P.Spr.Uniform.move_to_sample move in
        let probas' = Sim.update_probas
          data
          hy
          th
          tree'
          probas
        in
        (th, move, probas')
      in
      let log_pd_ratio (_, tree, _) (_, move, _) =
        [F.to_float (P.Spr.Uniform.log_pd_ratio tree move)]
      in
      let move_to_sample (th, move, probas) =
        (th, P.Spr.Uniform.move_to_sample move, probas)
      in
      Fit.Propose.Base { draw ; log_pd_ratio ; move_to_sample }
 
    let guided_sigma data hy th =
      let m = F.Pos.of_float (float (Data.nsites data)) in
      let mu = Theta.Param.mu th in
      let sgm = Hyper.spr_guided_scale hy in
      F.Pos.Op.(sgm * mu * m)

    let spr_guided data hy =
      let draw rng (th, tree, probas) =
        let upd = Sim.update_probas
          data
          hy
          th
        in
        let update tree _ probas =
          upd tree probas
        in
        let sgm = guided_sigma data hy th in
        let tree', probas', context = P'.Spr.draw
          Homogen
          update
          sgm
          rng
          (tree, probas)
        in
        (th, tree', probas', context)
      in
      let log_pd_ratio (th, tree, probas) (_, tree', probas', context) =
        let sgm = guided_sigma data hy th in
        let logr = P'.Spr.log_pd_ratio
          Homogen
          sgm
          (tree, probas)
          (tree', probas', context)
        in
        [F.to_float logr]
      in
      let move_to_sample (th, tree, probas, _) =
        (th, tree, probas)
      in
      Fit.Propose.Base { draw ; log_pd_ratio ; move_to_sample }

    let tree_prior data hy =
      let draw rng (th, _, probas) =
        let tree' = Sim.draw_tree data rng (Theta.Param.lambda th) in
        let probas' = Sim.update_probas
          data
          hy
          th
          tree'
          probas
        in
        (th, tree', probas')
      in
      let log_pd_ratio (th, tree, _) (th', tree', _) =
        (* obtain tree *)
        let logd = Prior.tree_log_dens_proba (Theta.Param.lambda th') tree
        in
        (* obtain tree' *)
        let logd' = Prior.tree_log_dens_proba (Theta.Param.lambda th) tree' in
        [F.to_float F.Op.(logd - logd')]
      in
      Fit.Propose.Base { draw ; log_pd_ratio ; move_to_sample = (fun x -> x) }

    let dummy_spr_div_context =
      let x_dummy = (0., 0) in
      P.Spr.Divergence.{
        x_prune = x_dummy ;
        x_prune_parent = x_dummy ;
        x_graft_parent = x_dummy ;
        t_graft = 0. ;
        distance = 0. ;
        moves = [] ; 
      }

    let dummy_spr_unif_context =
      let x_dummy = (0., 0) in
      P.Spr.Uniform.{
        x_prune = x_dummy ;
        x_prune_parent = x_dummy ;
        x_graft_parent = x_dummy ;
        length = 0. ;
      }

    let dummy_spr_div_move tree =
      (tree, dummy_spr_div_context)

    let dummy_spr_unif_move tree =
      (tree, dummy_spr_unif_context)

    let dummy_spr_guided_move (th, tree, probas) =
      (* is that ok ? *)
      let x_dummy = (0., 0) in
      let dummy_context = P'.Spr.{
        x_prune = x_dummy ;
        x_prune_sub = x_dummy ;
        x_prune_parent = x_dummy ;
        x_prune_sibling = x_dummy ;
        x_graft_parent = x_dummy ;
        x_graft_sibling = x_dummy ;
      }
      in
      (th, tree, probas, dummy_context)
  end


module Infer =
  struct
    type t = {
      lambda : Fit.Infer.t ;
      mu : Fit.Infer.t ;
      tree : Fit.Infer.t ;
    }

    let default = {
      lambda = Free `Adapt ;
      mu = Free `Adapt ;
      tree = Free `Adapt ;
    }

    let tag_to_string =
      function
      | `Lambda ->
          "lambda"
      | `Mu ->
          "mu"
      | `Tree ->
          "tree"

    let fix =
      function
      | `Lambda ->
          (fun infer -> { infer with lambda = Fixed })
      | `Mu ->
          (fun infer -> { infer with mu = Fixed })
      | `Tree ->
          (fun infer -> { infer with tree = Fixed })

    let lambda hy infer =
      Fit.Infer.maybe_adaptive_float
        ~tag:`Lambda
        ~get:(fun th -> th#lambda)
        ~set:(fun th x -> th#with_lambda x)
        ~prior:(Prior.lambda hy)
        (* adaptive or nothing *)
        ~proposal:Fit.Propose.Constant_flat
        ~jump:1e-2
        infer

    let mu hy infer =
      Fit.Infer.maybe_adaptive_float
        ~tag:`Mu
        ~get:(fun th -> th#mu)
        ~set:(fun th x -> th#with_mu x)
        ~prior:(Prior.mu hy)
        (* adaptive or nothing *)
        ~proposal:Fit.Propose.Constant_flat
        ~jump:1e-4
        infer

    let pars hy infer =
      Fit.Param.List.[
        mu hy infer.mu ;
        lambda hy infer.lambda ;
      ]

    module Term =
      struct
        let infer_tags =
          [`Lambda ; `Mu ; `Tree]
          |> L.map (fun tag -> (tag_to_string tag, tag))
          |> Cmdliner.Arg.enum

        let fix_tags =
          let doc =
            "Keep this parameter fixed."
          in
          Cmdliner.Arg.(
            value
            & opt_all infer_tags []
            & info ["fix"] ~docv:"FIX" ~doc
          )
      end

    let term =
      let f fix_tags =
        L.fold_right fix fix_tags default
      in
      Cmdliner.Term.(
        const f
        $ Term.fix_tags
      )
  end


module Estimate =
  struct
    let columns =
      Fit.Csv.columns_sample_move @ [
        "mu" ;
        "lambda" ;
        "move_mu" ;
        "move_lambda" ;
      ]

    let extract k res =
      let sof = string_of_float in
      Some (Fit.Csv.extract_sample_move
        ~move_to_sample:(fun th -> th)
        ~extract:(fun th ->
          function
          | "mu" ->
              sof th#mu
          | "lambda" ->
              sof th#lambda
          | _ ->
              failwith "unrecognized column"
        )
        k res
      )

    let theta_output ?theta_every ?chol_every chol path =
      Simfit.Out.convert_csv_ram
        ?n_thin:theta_every
        ?chol_every
        ~columns
        ~extract
        ~chol
        path

    let tree_output ?tree_every path =
      T.output_sample ?every:tree_every path

    let tree_move_output ?tree_every path =
      T.output_move ?every:tree_every path
     
    let tree_sample ret =
      let _, tree, _ = Fit.Mh.Return.sample ret in
      tree

    let tree_div_move ret =
       match Fit.Mh.Return.move ret with
       | _, (tree, _), _ ->
           Some tree
       | exception Invalid_argument _ ->
           None

    let tree_unif_move ret =
      match Fit.Mh.Return.move ret with
      | _, (tree, _), _ ->
          Some tree
      | exception Invalid_argument _ ->
          None

    let tree_guided_move ret =
      match Fit.Mh.Return.move ret with
      | _, tree, _, _ ->
          Some tree
      | exception Invalid_argument _ ->
          None

    let par_result res =
      Fit.Mh.Return.map
        (fun (th, _, _) -> th)
        (fun (th, _, _) -> th)
        res

    let par_34_result res =
      Fit.Mh.Return.map
        (fun (th, _, _) -> th)
        (fun (th, _, _, _) -> th)
        res

    let id_output th_out tree_out =
      U.Out.combine
        (fun ret _ -> ret)
        (U.Out.map par_result th_out)
        (U.Out.map tree_sample tree_out)

    let spr_div_output th_out tree_out tree_mv_out =
      U.Out.combine
        (fun ret _ -> ret)
        (U.Out.map par_result th_out)
        (U.Out.combine
          (fun ret _ -> ret)
          (U.Out.map tree_sample tree_out)
          (U.Out.map tree_div_move tree_mv_out)
        )

    let spr_unif_output th_out tree_out tree_mv_out =
      U.Out.combine
        (fun ret _ -> ret)
        (U.Out.map par_result th_out)
        (U.Out.combine
          (fun ret _ -> ret)
          (U.Out.map tree_sample tree_out)
          (U.Out.map tree_unif_move tree_mv_out)
        )

    let spr_guided_output th_out tree_out tree_mv_out =
      U.Out.combine
        (fun ret _ -> ret)
        (U.Out.map par_34_result th_out)
        (U.Out.combine
          (fun ret _ -> ret)
          (U.Out.map tree_sample tree_out)
          (U.Out.map tree_guided_move tree_mv_out)
        )

    let prior data hy =
      (* the order of parameters matters *)
      Fit.Dist.Joint {
        first = Fit.Param.List.(prior (new Theta.Param.t) [
          Infer.mu hy (Free `Adapt) ;
          Infer.lambda hy (Free `Adapt) ;
        ]) ;
        second = (fun th ->
          let first =
            match Theta.Param.lambda th with
            | lbd ->
                Prior.tree lbd
            | exception Invalid_argument _ ->
                (* negative lambda - already rejected *)
                Fit.Dist.Flat
          in
          Fit.Dist.Joint_indep {
            first ;
            second = Prior.probas data ;
            get = (fun tree_probas -> tree_probas) ;
            combine = (fun tree probas -> (tree, probas)) ;
          }
        ) ;
        get = (fun (th, tree, probas) -> (th, (tree, probas))) ;
        combine = (fun th (tree, probas) -> (th, tree, probas)) ;
      }

    let theta_draws hy infer =
      let pars = Infer.pars hy infer in
      let trio_draw_left f rng dx (th, tree, probas) =
        (f rng dx th, tree, probas)
      in
      pars
      |> Fit.Param.List.adapt_draws
      |> L.map trio_draw_left

    let to_chol js =
      js
      |> A.of_list
      |> Lac.Vec.of_array
      |> Lac.Mat.of_diag

    let theta_chol hy infer =
      let pars = Infer.pars hy infer in
      pars
      |> Fit.Param.List.adapt_jumps
      |> to_chol

    let tree_scale_draws (infer : Infer.t) =
      if Fit.Infer.is_free_adapt infer.tree then
        [Propose.scale]
      else
        []

    let tree_scale_chol hy (infer : Infer.t) =
      let js =
        if Fit.Infer.is_free_adapt infer.tree then
          [hy#scale_jump]
        else
          []
      in
      to_chol js

    let tree_slide_root_draws (infer : Infer.t) =
      if Fit.Infer.is_free_adapt infer.tree then
        [Propose.slide_root]
      else
        []

    let tree_slide_root_chol hy (infer : Infer.t) =
      let js =
        if Fit.Infer.is_free_adapt infer.tree then
          [hy#slide_root_jump]
        else
          []
      in
      to_chol js

    let vector_draw data hy draws =
      let vd = Fit.Propose.vector_draw (A.of_list draws) in
      (fun rng v (th, tree, probas) ->
        let th', tree', _ = vd rng v (th, tree, probas) in
        let probas' = Sim.update_probas
          data
          hy
          th'
          tree'
          probas
        in
        (th', tree', probas')
      )

    let proposal
      ~path ?theta_every ?tree_every ?chol_every
      data (hy : Hyper.t) infer =
      let th_draws = theta_draws hy infer in
      let th_chol = theta_chol hy infer in
      let tsc_draws = tree_scale_draws infer in
      let tsc_chol = tree_scale_chol hy infer in
      let tsr_draws = tree_slide_root_draws infer in
      let tsr_chol = tree_slide_root_chol hy infer in
      let prior = prior data hy in
      let likelihood = Likelihood.dist data hy in
      let th_out = theta_output ?theta_every ?chol_every th_chol path in
      let tree_out, tree_close = tree_output ?tree_every path in
      let tree_mv_out, tree_mv_close = tree_move_output ?tree_every path in
      let ram_output = id_output th_out tree_out in
      let sprd_output = spr_div_output th_out tree_out tree_mv_out in
      let spru_output = spr_unif_output th_out tree_out tree_mv_out in
      let sprg_output = spr_guided_output th_out tree_out tree_mv_out in
      let tprior_output = id_output th_out tree_out in
      let as_sprd_move (th, tree, probas) =
        (th, Propose.dummy_spr_div_move tree, probas)
      in
      let as_spru_move (th, tree, probas) =
        (th, Propose.dummy_spr_unif_move tree, probas)
      in
      let as_sprg_move =
        Propose.dummy_spr_guided_move
      in
      let weight th =
        Fit.Mh.Sample.weight prior likelihood th
      in
      let start k ((_, tree, _) as th_tree_p) =
        let smpl = weight th_tree_p in
        let th_res = par_result
          (Fit.Mh.Return.accepted (fun x -> x) smpl)
        in
        th_out.start k th_res ;
        tree_out.start k tree ;
        smpl
      in
      let return k smplf =
        let ram_res = Fit.Mh.Return.accepted (fun x -> x) smplf in
        let sprd_res = Fit.Mh.Return.accepted as_sprd_move smplf in
        let spru_res = Fit.Mh.Return.accepted as_spru_move smplf in
        let sprg_res = Fit.Mh.Return.accepted as_sprg_move smplf in
        let _ = ram_output.return k ram_res in
        let _ = sprd_output.return k sprd_res in
        let _ = spru_output.return k spru_res in
        let resf = sprg_output.return k sprg_res in
        tree_close () ; tree_mv_close () ;
        resf
      in
      let th_ram_prop = Fit.Ram.Loop.proposal 
        ~output:ram_output
        (fun x -> x)
        (fun x -> x)
        (L.length th_draws)
        th_chol
        prior
        (vector_draw data hy th_draws)
        likelihood
      in
      let tsc_ram_prop = Fit.Ram.Loop.proposal
        ~output:ram_output
        (fun x -> x)
        (fun x -> x)
        (L.length tsc_draws)
        tsc_chol
        prior
        (vector_draw data hy tsc_draws)
        likelihood
      in
      let tsr_ram_prop = Fit.Ram.Loop.proposal
        ~output:ram_output
        ~adapt:false
        (fun x -> x)
        (fun x -> x)
        (L.length tsr_draws)
        tsr_chol
        prior
        (vector_draw data hy tsr_draws)
        likelihood
      in
      let sprd_mh_prop = Fit.Mh.Loop.proposal
        ~output:sprd_output
        as_sprd_move
        prior
        (Propose.spr_div data hy)
        likelihood
      in
      let spru_mh_prop = Fit.Mh.Loop.proposal
        ~output:spru_output
        as_spru_move
        prior
        (Propose.spr_unif data hy)
        likelihood
      in
      let sprg_mh_prop = Fit.Mh.Loop.proposal
        ~output:sprg_output
        as_sprg_move
        prior
        (Propose.spr_guided data hy)
        likelihood
      in
      let tprior_mh_prop = Fit.Mh.Loop.proposal
        ~output:tprior_output
        (fun x -> x)
        prior
        (Propose.tree_prior data hy)
        likelihood
      in
      let choices =
          (if not (Fit.Infer.is_fixed infer.lambda
                && Fit.Infer.is_fixed infer.mu) then [
             (th_ram_prop, hy#proposal_weights.th_ram)
           ]
           else
             []
          )
        @ (if not (Fit.Infer.is_fixed infer.tree) then [
             (tsc_ram_prop, hy#proposal_weights.tsc_ram) ;
             (tsr_ram_prop, hy#proposal_weights.tsr_ram) ;
             (sprd_mh_prop, hy#proposal_weights.sprd_mh) ;
             (spru_mh_prop, hy#proposal_weights.spru_mh) ;
             (sprg_mh_prop, hy#proposal_weights.sprg_mh) ;
             (tprior_mh_prop, hy#proposal_weights.tprior_mh) ;
           ]
           else
             []
          )
      in
      let proposal = Fit.Propose.Choice choices in
      proposal, weight, start, return

    let posterior
      ?seed ?theta_every ?tree_every ?chol_every
      ~path hypar infer tseqs tree0 par niter =
      let data = Data.read tseqs in
      let proposal, _, start, return = proposal
        ?theta_every
        ?tree_every
        ?chol_every
        ~path
        data
        hypar
        infer
      in
      let tree =
        match tree0 with
        | None ->
            Sim.draw_tree
              data
              (U.rng seed)
              (Theta.Param.lambda par)
        | Some tr ->
            tr
      in
      let probas = Sim.update_probas
        data
        hypar
        par
        tree
        (Sim.init_probas data)
      in
      let smpl0 = start 0 (par, tree, probas) in
      let smplf =
        Fit.Markov_chain.direct_simulate ?seed proposal smpl0 niter
      in
      return niter smplf

    let evaluate_proposal ?seed ~path hypar infer tseqs tree par niter =
      let data = Data.read tseqs in
      let proposal, weight, start, return = proposal
        ~theta_every:1
        ~tree_every:1
        ~chol_every:1
        ~path
        data
        hypar
        infer
      in
      let smpl0 =
        let tree =
          match tree with
          | None ->
              Sim.draw_tree
                data
                (U.rng seed)
                (Theta.Param.lambda par)
          | Some tr ->
              tr
        in
        let probas = Sim.update_probas
          data
          hypar
          par
          tree
          (Sim.init_probas data)
        in
        start 0 (par, tree, probas)
      in
      let rng = U.rng seed in
      let loop rng k =
        let tree =
          match tree with
          | None ->
              Sim.draw_tree
                data
                rng
                (Theta.Param.lambda par)
          | Some tr ->
              tr
        in
        let probas = Sim.update_probas
          data
          hypar
          par
          tree
          (Sim.init_probas data)
        in
        let smpl = weight (par, tree ,probas) in
        ignore (Fit.Propose.draw_from rng (k, smpl) proposal)
      in
      for k = 0 to niter - 1 do
        loop rng k
      done ;
      ignore (return niter smpl0)
  end


module Term =
  struct
    module C = Cmdliner

    let tree_every =
      let doc =
        "Output phylogenetic tree every $(docv) iterations.
         By default, do not output."
      in
      U.Arg.opt C.Arg.int ~doc "tree-every"

    let tree =
      let doc =
        "Optional Newick file for the starting tree."
      in
      let fname = C.Arg.(
          value
        & opt (some string) None
        & info ["tree"] ~docv:"TREE" ~doc
      )
      in
      C.Term.(const (Option.map T.read) $ fname)

    let run =
      let flat seed theta_every tree_every chol_every
               path read_hypar infer tseqs tree par niter =
        let capt_hy = Some (Printf.sprintf "%s.par.csv" path) in
        let hypar = read_hypar capt_hy in
        let t = Sys.time () in
        let res = Estimate.posterior
          ?seed ?theta_every ?tree_every ?chol_every
          ~path hypar infer tseqs tree par niter
        in
        Printf.printf "Execution CPU time: %f\n" (Sys.time () -. t) ;
        res
      in
      C.Term.(const flat
          $ Coalfit.Term.seed
          $ Coalfit.Term.theta_every
          $ tree_every
          $ Coalfit.Term.chol_every
          $ Coalfit.Term.path
          $ Hyper.term
          $ Infer.term
          $ Coalfit.Term.sequences
          $ tree
          $ Theta.Param.term
          $ Coalfit.Term.n_iter
      )

    let evaluate_proposal =
      let flat seed path read_hypar infer tseqs tree par niter =
        let capt_hy = Some (Printf.sprintf "%s.par.csv" path) in
        let hypar = read_hypar capt_hy in
        Estimate.evaluate_proposal
          ?seed ~path hypar infer tseqs tree par niter
      in
      C.Term.(const flat
        $ Coalfit.Term.seed
        $ Coalfit.Term.path
        $ Hyper.term
        $ Infer.term
        $ Coalfit.Term.sequences
        $ tree
        $ Theta.Param.term
        $ Coalfit.Term.n_iter
      )

    let info =
      let doc =
        "Estimate a phylogeny under Kingman's coalescent and JC69
         substitutions from timed DNA sequences."
      in C.Term.info
        "phylo-constant"
        ~version:"%%VERSION%%"
        ~doc
        ~exits:C.Term.default_exits
        ~man:[]

    let info_eval_prop =
      let doc =
        "Evaluate a mixed proposal under Kingman's coalescent
         and JC69 substitutions from timed DNA sequences."
      in C.Term.info
        "phylo-eval-prop"
        ~version:"%%VERSION%%"
        ~doc
        ~exits:C.Term.default_exits
        ~man:[]
  end

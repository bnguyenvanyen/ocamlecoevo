open Abbrevs

module S = Seqsim


let name = "GTR"


module Hyper =
  struct
    class rel_rates_able = object
      val rel_rates_jump_v = 1e-2

      method rel_rates_jump = rel_rates_jump_v

      method with_rel_rates_jump x = {< rel_rates_jump_v = x >}
    end

    class pis_able = object
      val pis_jump_v = 1e-3

      method pis_jump = pis_jump_v
      method with_pis_jump x = {< pis_jump_v = x >}
    end

    class t = object
      inherit Jc69.Hyper.mu_able
      inherit rel_rates_able
      inherit pis_able
    end

    let default = new t

    let mu_prior hy =
      hy#mu_prior
    
    let mu_mean hy =
      F.to_float (F.Pos.of_float hy#mu_mean)

    let mu_var hy =
      F.Pos.of_float hy#mu_var

    let mu_max hy =
      F.to_float (F.Pos.of_float hy#mu_max)

    let mu_jump hy =
      F.Pos.of_float hy#mu_jump

    let rel_rates_prior hy =
      hy#rel_rates_prior

    let rel_rates_mean hy =
      F.to_float (F.Pos.of_float hy#rel_rates_mean)

    let rel_rates_var hy =
      F.Pos.of_float hy#rel_rates_var

    let rel_rates_max hy =
      F.to_float (F.Pos.of_float hy#rel_rates_max)

    let rel_rates_jump hy =
      F.Pos.of_float hy#rel_rates_jump

    let ncat _ =
      1

    module Term =
      struct
        module T = Jc69.Hyper.Term

        let rel_rates_jump () =
          let doc =
            "Initial standard deviation of relative rate jumps."
          in
          let get hy = hy#rel_rates_jump in
          let set x hy = hy#with_rel_rates_jump x in
          T.float_arg ~get ~set ~doc "rel-rates-jump"
      end

    (* eta-expanded version *)
    let open_term () =
      let f mu_stuff rr_j ~config hy =
        hy
        |> mu_stuff ~config
        |> rr_j ~config
      in
      Cmdliner.Term.(
        const f
        $ Jc69.Hyper.open_term ()
        $ Term.rel_rates_jump ()
      )

    type 'a term =
      'a
      U.Arg.Capture.t
      U.Arg.configurable
      Cmdliner.Term.t

    let term : t term =
      let f update ~config =
        new t
        |> U.Arg.Capture.empty
        |> update ~config
      in
      Cmdliner.Term.(
        const f
        $ open_term ()
      )
  end


module Theta =
  struct
    type t = S.Gtr.t

    module Infer =
      struct
        module I = Fit.Infer

        class mu_able = object
          val mu_v = I.adapt

          method mu = mu_v
          method with_mu x = {< mu_v = x >}
        end

        (* for now only all at once or nothing (simpler) *)
        class rel_rates_able = object
          val rel_rates_v = I.adapt

          method rel_rates = rel_rates_v
          method with_rel_rates x = {< rel_rates_v = x >}
        end

        (* for now only all at once or nothing (simpler prior) *)
        class pis_able = object
          val pis_v = I.adapt

          method pis = pis_v

          method with_pis x = {< pis_v = x >}
        end

        class t = object
          inherit mu_able
          inherit rel_rates_able
          inherit pis_able
        end

        type tag = [`Mu | `Rel_rates | `Pis ]

        let tags = [`Mu ; `Rel_rates ; `Pis ]

        let tag_to_string =
          function
          | `Mu ->
              "mu"
          | `Rel_rates ->
              "rel-rates"
          | `Pis ->
              "pis"

        let tag_to_columns =
          function
          | `Mu ->
              ["mu"]
          | `Rel_rates ->
              ["rct" ; "rat" ; "rgt" ; "rac" ; "rcg" ; "rag"]
          | `Pis ->
              ["pia" ; "pit" ; "pic" ; "pig"]

        let default =
          new t

        let get =
          function
          | `Mu ->
              (fun infr -> infr#mu)
          | `Rel_rates ->
              (fun infr -> infr#rel_rates)
          | `Pis ->
              (fun infr -> infr#pis)

        let fix =
          function
          | `Mu ->
              (fun infr -> infr#with_mu I.fixed)
          | `Rel_rates ->
              (fun infr -> infr#with_rel_rates I.fixed)
          | `Pis ->
              (fun infr -> infr#with_pis I.fixed)

        let infer =
          function
          | `Mu ->
              (fun infr -> infr#with_mu I.adapt)
          | `Rel_rates ->
              (fun infr -> infr#with_rel_rates I.adapt)
          | `Pis ->
              (fun infr -> infr#with_pis I.adapt)
      end

    let columns (infr : Infer.t) =
      let f tag =
        if Fit.Infer.is_free (Infer.get tag infr) then
          Infer.tag_to_columns tag
        else
          []
      in
      L.fold_left (fun cs tag ->
        cs @ (f tag)
      ) [] Infer.tags

    let extract = S.Gtr.extract

    let mu = S.Gtr.mu

    module Prior =
      struct
        module D = Fit.Dist

        let mu hy =
          match Hyper.mu_prior hy with
          | `Lognormal ->
              let m = log (Hyper.mu_mean hy) /. log 10. in
              D.Lognormal (m, Hyper.mu_var hy)
          | `Uniform ->
              D.Uniform (0., Hyper.mu_max hy)

        (* any one of the relative rates *)
        let rel_rates =
          let to_list (rct, rat, rgt, rac, rcg) =
            let rag = 1. -. rct -. rat -. rgt -. rac -. rcg in
            [ rct ; rat ; rgt ; rac ; rcg ; rag ]
          in
          let to_tup =
            function
            | [ rct ; rat ; rgt ; rac ; rcg ; _ ] ->
                (rct, rat, rgt, rac, rcg)
            | _ ->
                assert false
          in
          let dir =
            D.Dirichlet [ F.one ; F.one ; F.one ; F.one ; F.one ; F.one ]
          in
          D.Map (to_list, to_tup, dir)

        let pis =
          let to_list (pia, pit, pic) =
            let pig = 1. -. pia -. pit -. pic in
            [ pia ; pit ; pic ; pig ]
          in
          let to_tup =
            function
            | [ pia ; pit ; pic ; _ ] ->
                (pia, pit, pic)
            | _ ->
                assert false
          in
          let dir =
            D.Dirichlet [ F.one ; F.one ; F.one ; F.one ]
          in
          D.Map (to_list, to_tup, dir)
      end

    let prior hy =
      let to_tuple (th : t) =
        th.mu,
        ((th.rct, th.rat, th.rgt, th.rac, th.rcg),
         (th.pia, th.pit, th.pic))
      in
      let of_tuple (mu, ((rct, rat, rgt, rac, rcg), (pia, pit, pic))) : t =
        { mu ; rct ; rat ; rgt ; rac ; rcg ; pia ; pit ; pic }
      in
      Fit.Dist.Map (
        to_tuple,
        of_tuple,
        Fit.Dist.trio (Prior.mu hy) Prior.rel_rates Prior.pis
      )

    module Draws =
      struct
        let mu _ dx (p : t) : t =
          { p with mu = p.mu +. dx }

        let rct _ dx (p : t) : t =
          { p with rct = p.rct +. dx }

        let rat _ dx (p : t) : t =
          { p with rat = p.rat +. dx }

        let rgt _ dx (p : t) : t =
          { p with rgt = p.rgt +. dx }

        let rac _ dx (p : t) : t =
          { p with rac = p.rac +. dx }

        let rcg _ dx (p : t) : t =
          { p with rcg = p.rcg +. dx }

        let pia _ dx (p : t) : t =
          { p with pia = p.pia +. dx }

        let pit _ dx (p : t) : t =
          { p with pit = p.pit +. dx }

        let pic _ dx (p : t) : t =
          { p with pic = p.pic +. dx }

        let rel_rates = [
          rct ;
          rat ;
          rgt ;
          rac ;
          rcg ;
        ]

        let pis = [
          pia ;
          pit ;
          pic ;
        ]

        let of_tag =
          function
          | `Mu ->
              [mu]
          | `Rel_rates ->
              rel_rates
          | `Pis ->
              pis
      end

    let draws infer =
      let f tag =
        if Fit.Infer.is_free_adapt (Infer.get tag infer) then
          Draws.of_tag tag
        else
          []
      in
      L.fold_left (fun ds tag ->
        ds @ (f tag)
      ) [] Infer.tags

    module Jumps =
      struct
        let mu hy =
          [hy#mu_jump]

        let rel_rates hy =
          L.init 5 (fun _ -> hy#rel_rates_jump)

        let pis hy =
          L.init 3 (fun _ -> hy#pis_jump)

        let of_tag =
          function
          | `Mu ->
              mu
          | `Rel_rates ->
              rel_rates
          | `Pis ->
              pis
      end

    let jumps infer (hy : Hyper.t) =
      let f tag =
        if Fit.Infer.is_free_adapt (Infer.get tag infer) then
          Jumps.of_tag tag hy
        else
          []
      in
      L.fold_left (fun js tag ->
        js @ (f tag)
      ) [] Infer.tags

    let term =
      S.Gtr.capture_term
  end


type vec = S.Gtr.vec
type mat = S.Gtr.mat


let settings = S.Gtr.settings

let equilibrium_probas par =
  S.Gtr.equilibrium_probas par

let transition_probas ?mat par =
  S.Gtr.transition_probas ?mat par

let flow ?mat par =
  let tp = transition_probas ?mat par in
  let trans dt =
    tp (F.Pos.of_float dt)
  in
  S.Probas.gen_transition settings trans

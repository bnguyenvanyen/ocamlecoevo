module L = BatList


module type INPUT =
  sig
    type t
    type tag

    val tags : tag list
    val tag_to_string : tag -> string

    val default : t

    (** [infer tag inf] is [inf] with the parameter [tag]
        set to be inferred. *)
    val infer : tag -> t -> t

    (** [fix tag inf] is [inf] with the parameter [tag] set to be fixed. *)
    val fix : tag -> t -> t
  end


module type S =
  sig
    type t

    (** [term] is a term with arguments 'fix' and 'infer'. *)
    val term : t Cmdliner.Term.t
  end


module Make (Infer : INPUT) : (S with type t = Infer.t) =
  struct
    module C = Cmdliner

    type t = Infer.t

    let tags_enum =
      let kvs = L.map (fun tag ->
        Infer.tag_to_string tag, tag
      ) Infer.tags
      in
      C.Arg.enum kvs

    let fix_tags =
      let doc =
        "Fix the specified parameter."
      in
      C.Arg.(
        value
        & opt_all tags_enum []
        & info ["fix"] ~docv:"FIX" ~doc
      )

    let infer_tags =
      let doc =
        "Infer the specified parameter.$(docv) overrides 'FIX'."
      in
      C.Arg.(
        value
        & opt_all tags_enum []
        & info ["infer"] ~docv:"INFER" ~doc
      )

    let term =
      let f fix_tags infer_tags =
       Infer.default
       |> L.fold_right Infer.fix fix_tags
       |> L.fold_right Infer.infer infer_tags
      in
      C.Term.(
        const f
        $ fix_tags
        $ infer_tags
      )
  end

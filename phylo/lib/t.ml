open Abbrevs


include Tree.Labeltimed.Make (State)


let of_coal_tree n ctree =
  let module C = Coal.Constant_lazy in
  (* reimplement map across tree types *)
  let rec f m coal_tree =
    let leaf (t, ks, _) =
      (* ks should be a singleton *)
      assert (S.Int.cardinal ks = 1) ;
      let k = S.Int.any ks in
      leaf (t, k), m
    in
    let node (t, _, _) stree =
      let stree', m' = f m stree in
      node (t, m' + 1) stree', (m' + 1)
    in
    let binode (t, _, _) ltree rtree =
      let ltree', m' = f m ltree in
      let rtree', m'' = f m' rtree in
      binode (t, m'' + 1) ltree' rtree', (m'' + 1)
    in
    C.Pop.T.case ~leaf ~node ~binode coal_tree
  in
  f n ctree


let cut_to_forest t0 tree =
  let t0' = State.to_time (root_value tree) in
  if t0 <= t0' then
    [tree]
  else
    cut_before t0 tree


let draw data rng lambda =
  let module D = Seq_data in
  let module C = Coal.Constant_lazy in
  let samples = D.sorted data in
  let nsamples = D.nsamples data in
  let _, tf, _ = L.hd samples in
  let nu = C.Simulate.Backward.rand_prm
    ~rng
    ~samples
    ~tf
  in
  let zf, _ = C.Simulate.Backward.until_merged
    ~tf
    ~lambda
    ~samples
    nu
  in
  (* we want only one tree
   * (for now) raise if there is more than one *)
  match C.Pop.to_forest zf with
  | (_, _, tree) :: [] ->
      let tree', n = of_coal_tree nsamples tree in
      assert (n = 2 * nsamples) ;
      tree'
  | _ ->
      failwith "not exactly one tree"


let compatible data tree =
  let module D = Seq_data in
  let module S = BatSet.Make (struct
    type t = float
    let compare x y =
      (* tol:1e-6 because of stamp precision in newick *)
      if U.nearly_equal ~tol:1e-6 x y then
        0
      else
        compare x y
  end)
  in
  let ks_seq =
    data
    |> D.sorted
    |> L.map (fun (_, t, _) -> F.to_float t)
    |> S.of_list
  in
  let ks_tree =
    tree
    |> leaves
    |> L.map snd
    |> L.map State.to_time
    |> S.of_list
  in
  let excl = S.sym_diff ks_seq ks_tree in
  if excl = S.empty then
    true
  else begin
    Printf.eprintf "incompatible:\n" ;
    S.print BatFloat.print BatIO.stderr excl ;
    false
  end 


(* slightly expensive *)
let relabel
  (type a b)
  (module T : Tree.Labeltimed.S with type state = a
                                 and type t = b)
  (tree : b) : t =
  let comp (t, lab) (t', lab') =
    let c = compare t t' in
    if c = 0 then
      T.State.Label.compare lab lab'
    else
      c
  in
  let module Map = M.Make (struct
    type t = float * T.State.label
    let compare = comp
  end)
  in
  let leaf_labels, n =
    tree
    (* slightly expensive *)
    |> T.leaves
    |> L.map snd
    |> L.map (fun x -> (T.State.to_time x, T.State.to_label x))
    |> L.sort (fun tx tx' -> comp tx' tx)
    (* associate original labels to time and order *)
    |> L.fold_lefti (fun (m, _) i tlab ->
        let j = i + 1 in
        let m = Map.modify_def
          S.Int.empty
          tlab
          (S.Int.add j)
          m
        in
        (m, j)
    ) (Map.empty, 0)
  in
  (* see also Constant.Sim.coal_tree_to_tree *)
  (* nearly map_case, but need to pass m around *)
  let rec f lv m tree =
    let leaf x =
      let t = T.State.to_time x in
      let lab = T.State.to_label x in
      let js, lv = Map.extract (t, lab) lv in
      let j, js = S.Int.pop_min js in
      let lv =
        if S.Int.is_empty js then
          (* no more leaves with this state *)
          lv
        else
          (* restore for remaining leaves *)
          Map.add (t, lab) js lv
      in
      leaf (t, j), m, lv
    in
    let node x stree =
      let t = T.State.to_time x in
      let stree, m, lv = f lv m stree in
      node (t, m + 1) stree, (m + 1), lv
    in
    let binode x ltree rtree =
      let t = T.State.to_time x in
      let ltree, m, lv = f lv m ltree in
      let rtree, m, lv = f lv m rtree in
      binode (t, m + 1) ltree rtree, (m + 1), lv
    in
    T.case ~leaf ~node ~binode tree
  in
  let tree', _, lv = f leaf_labels n tree in
  (* all leaf labels have been consumed *)
  assert (Map.is_empty lv) ;
  tree'


let read ?fmt ?(line=2) path =
  (* assume the tree is on the second line *)
  let module St = struct
    type t = float * string
    type label = string

    module Label = Tree.Label.String

    let to_time (t, _) = t

    let at_time t (_, lab) = (t, lab)

    let to_label (_, lab) = lab

    let to_string (t, lab) =
      Printf.sprintf "%s:%f" lab t

    let scan =
      match fmt with
      | None ->
          (fun s -> Scanf.sscanf s "%s@:%f" (fun s t -> (t, s)))
      | Some fmt ->
          (fun s -> Scanf.sscanf s fmt (fun s t -> (t, s)))

    let of_string s =
      match scan s with
      | tx ->
          Some tx
      | exception Scanf.Scan_failure _ ->
          None
  end
  in
  let module T = Tree.Labeltimed.Make (St) in
  let chan = open_in path in
  for _ = 1 to line - 1 do
    ignore (input_line chan)
  done ;
  (* assume timed input for now *)
  let tree = T.read_newick chan in
  (* relabel the tree *)
  relabel
    (module T : Tree.Labeltimed.S with type state = float * string
                                   and type t = T.t)
    tree


let output_every ?n_thin ~lengthed_chan ~timed_chan k tree =
  let out_one ~timed chan k tree =
    Printf.fprintf chan ">%i\n" k ;
    if timed then
      output_newick chan tree
    else
      output_lengthed_newick chan tree
    ;
    Printf.fprintf chan "\n\n"
  in
  let out k tree =
    out_one ~timed:false lengthed_chan k tree ;
    out_one ~timed:true timed_chan k tree
  in
  match n_thin with
  | None ->
      ()
  | Some n_thin ->
      if (k mod n_thin) = 0 then
        out k tree
      else
        ()


let output_sample ?every path =
  let lengthed_chan =
    open_out (Printf.sprintf "%s.tree.newick" path)
  in
  let timed_chan =
    open_out (Printf.sprintf "%s.tree.tnewick" path)
  in
  let output k tree =
    output_every
      ?n_thin:every
      ~lengthed_chan
      ~timed_chan
      k
      tree
  in
  let close () =
    close_out lengthed_chan ;
    close_out timed_chan
  in
  let start k res =
    output k res
  in
  let out k res =
    output k res
  in
  let return k res =
    output k res ;
    (k, res)
  in
  U.Out.{ start ; out ; return }, close


let output_move ?every path =
  let lengthed_chan =
    open_out (Printf.sprintf "%s.tree.move.newick" path)
  in
  let timed_chan =
    open_out (Printf.sprintf "%s.tree.move.tnewick" path)
  in
  let output k move =
    Option.iter (output_every
      ?n_thin:every
      ~lengthed_chan
      ~timed_chan
      k
    ) move
  in
  let close () =
    close_out lengthed_chan ;
    close_out timed_chan
  in
  let start k res =
    output k res
  in
  let out k res =
    output k res
  in
  let return k res =
    output k res ;
    (k, res)
  in
  U.Out.{ start ; out ; return }, close

module L = BatList

module U = Util
module F = U.Float


type t = {
  th_ram : U.closed_pos ;
  (** RAM proposal for theta *)
  th_more_ram : U.closed_pos ;
  (** Additional RAM proposal for theta *)
  tsc_ram : U.closed_pos ;
  (** RAM proposal for tree scale *)
  tsr_ram : U.closed_pos ;
  (** RAM (non-adaptive) proposal for tree slide root *)
  tsn_ram : U.closed_pos ;
  (** RAM proposal for tree slide node *)
  sprd_mh : U.closed_pos ;
  (** MH proposal for divergence SPR *)
  spru_mh : U.closed_pos ;
  (** MH proposal for uniform SPR *)
  sprg_mh : U.closed_pos ;
  (** MH proposal for guided SPR *)
  tprior_mh : U.closed_pos ;
  (** MH proposal for tree prior draws *)
}


let zero = {
  th_ram = F.zero ;
  th_more_ram = F.zero ;
  tsc_ram = F.zero ;
  tsr_ram = F.zero ;
  tsn_ram = F.zero ;
  spru_mh = F.zero ;
  sprd_mh = F.zero ;
  sprg_mh = F.zero ;
  tprior_mh = F.zero ;
}


let default = {
  th_ram = F.Pos.of_float (1. /. 10.) ;
  th_more_ram = F.zero ;
  tsc_ram = F.Pos.of_float (1. /. 10.) ;
  tsr_ram = F.Pos.of_float (1. /. 4.) ;
  tsn_ram = F.one ;
  spru_mh = F.Pos.of_float (1. /. 10.) ;
  sprd_mh = F.one ;
  sprg_mh = F.one ;
  tprior_mh = F.zero ;
}


let modify =
  function
  | `Theta ->
      (fun x y -> { y with th_ram = x })
  | `Theta_more ->
      (fun x y -> { y with th_more_ram = x })
  | `Tree_scale ->
      (fun x y -> { y with tsc_ram = x })
  | `Tree_slide_root ->
      (fun x y -> { y with tsr_ram = x })
  | `Tree_slide_node ->
      (fun x y -> { y with tsn_ram = x })
  | `Spr_div ->
      (fun x y -> { y with sprd_mh = x })
  | `Spr_unif ->
      (fun x y -> { y with spru_mh = x })
  | `Spr_guided ->
      (fun x y -> { y with sprg_mh = x })
  | `Tree_prior ->
      (fun x y -> { y with tprior_mh = x })


let to_string =
  function
  | `Theta ->
      "theta"
  | `Theta_more ->
      "theta-more"
  | `Tree_scale ->
      "tree-scale"
  | `Tree_slide_root ->
      "tree-slide-root"
  | `Tree_slide_node ->
      "tree-slide-node"
  | `Spr_div ->
      "spr-div"
  | `Spr_unif ->
      "spr-unif"
  | `Spr_guided ->
      "spr-guided"
  | `Tree_prior ->
      "tree-prior"


module Term =
  struct
    let tags =
      [
        `Theta ;
        `Theta_more ;
        `Tree_scale ;
        `Tree_slide_root ;
        `Tree_slide_node ;
        `Spr_div ;
        `Spr_unif ;
        `Spr_guided ;
        `Tree_prior
      ]
      |> L.map (fun tag -> (to_string tag, tag))
      |> Cmdliner.Arg.enum

    let modify =
      let doc =
        "Modify the propensity to choose this proposal"
      in
      Cmdliner.Arg.(
        value
        & opt_all (pair tags (U.Arg.positive ())) []
        & info
          ["proposal-weight"]
          ~docv:"PROPOSAL-WEIGHT"
          ~doc
      )
  end


let term =
  let f modifiers =
    L.fold_right
      (fun (tag, w) -> modify tag w)
      modifiers
      default
  in
  Cmdliner.Term.(
    const f
    $ Term.modify
  )

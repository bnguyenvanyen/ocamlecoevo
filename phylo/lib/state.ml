type t = float * int
(* put integers only on leaves *)

type label = int


let to_time (t, _) = t


let at_time t (_, k) = (t, k)


let to_label (_, k) = k


module Label = Tree.Label.Int


let to_string (t, k) =
  Printf.sprintf "%i:%f" k t


let of_string s =
  match Scanf.sscanf s "%i:%f" (fun k t -> (t, k)) with
  | tk ->
      Some tk
  | exception Scanf.Scan_failure _ ->
      None

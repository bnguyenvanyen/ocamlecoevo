open Abbrevs

module S = Seqsim


let name = "JC69"


module Hyper =
  struct
    class mu_able = object
      val mu_prior_v : [`Uniform | `Lognormal] = `Lognormal
      val mu_mean_v = 1e-4
      val mu_var_v = 1.
      val mu_max_v = 1e-2
      val mu_jump_v = 1e-6

      method mu_prior = mu_prior_v
      method mu_mean = mu_mean_v
      method mu_var = mu_var_v
      method mu_max = mu_max_v
      method mu_jump = mu_jump_v

      method with_mu_prior x = {< mu_prior_v = x >}
      method with_mu_mean x = {< mu_mean_v = x >}
      method with_mu_var x = {< mu_var_v = x >}
      method with_mu_max x = {< mu_max_v = x >}
      method with_mu_jump x = {< mu_jump_v = x >}
    end

    class t = object
      inherit mu_able
    end

    let default = new t
    
    let mu_prior hy =
      hy#mu_prior

    let mu_mean hy =
      F.to_float (F.Pos.of_float hy#mu_mean)

    let mu_var hy =
      F.Pos.of_float hy#mu_var

    let mu_max hy =
      F.to_float (F.Pos.of_float hy#mu_max)

    let mu_jump hy =
      F.Pos.of_float hy#mu_jump

    let ncat _ =
      1

    module Term =
      struct
        let float_arg ~doc ~get ~set name =
          U.Arg.Update.Capture.opt_config
            get
            set
            Cmdliner.Arg.float
            ~doc
            name

        let prior_choice_arg ~doc ~get ~set name =
          let conv = Cmdliner.Arg.enum [
            ("uniform", `Uniform) ;
            ("lognormal", `Lognormal) ;
          ]
          in
          U.Arg.Update.Capture.opt_config
            get
            set
            conv
            ~doc
            name

        let mu_prior () =
          let doc =
            "Choose between lognormal and uniform prior for mu."
          in
          let get hy = hy#mu_prior in
          let set x hy = hy#with_mu_prior x in
          prior_choice_arg ~get ~set ~doc "mu-prior"

        let mu_mean () =
          let doc =
            "Mean of the mu prior if lognormal."
          in
          let get hy = hy#mu_mean in
          let set x hy = hy#with_mu_mean x in
          float_arg ~get ~set ~doc "mu-mean"

        let mu_var () =
          let doc =
            "10-log-variance of the mu prior if lognormal."
          in
          let get hy = hy#mu_var in
          let set x hy = hy#with_mu_var x in
          float_arg ~get ~set ~doc "mu-var"

        let mu_max () =
          let doc =
            "Upper bound of the beta prior if uniform."
          in
          let get hy = hy#mu_max in
          let set x hy = hy#with_mu_max x in
          float_arg ~get ~set ~doc "mu-max"

        let mu_jump () =
          let doc =
            "Initial standard deviation of mu jumps."
          in
          let get hy = hy#mu_jump in
          let set x hy = hy#with_mu_jump x in
          float_arg ~get ~set ~doc "mu-jump"
      end

    (* eta-expanded version *)
    let open_term () =
      let f mu_p mu_m mu_v mu_x mu_j ~config hy =
        hy
        |> mu_p ~config
        |> mu_m ~config
        |> mu_v ~config
        |> mu_x ~config
        |> mu_j ~config
      in
      Cmdliner.Term.(
        const f
        $ Term.mu_prior ()
        $ Term.mu_mean ()
        $ Term.mu_var ()
        $ Term.mu_max ()
        $ Term.mu_jump ()
      )

    type 'a term =
      'a
      U.Arg.Capture.t
      U.Arg.configurable
      Cmdliner.Term.t

    let term : t term =
      let f update ~config =
        U.Arg.Capture.empty (new t)
        |> update ~config
      in
      Cmdliner.Term.(
        const f
        $ open_term ()
      )
  end


module Theta =
  struct
    type t = S.Jc69.t

    module Infer =
      struct
        type t = Fit.Infer.t

        type tag = [`Mu]

        let tags = [`Mu]

        let tag_to_string `Mu =
          "mu"

        let tag_to_columns (`Mu as tag) =
          [tag_to_string tag]

        let default = Fit.Infer.adapt

        let fix `Mu _ =
          Fit.Infer.fixed

        let infer `Mu _ =
          Fit.Infer.adapt
      end

    let columns infer =
      if Fit.Infer.is_fixed infer then
        []
      else
        S.Jc69.columns

    let extract = S.Jc69.extract

    let mu = S.Jc69.mu

    let prior hy =
      let d =
        match Hyper.mu_prior hy with
        | `Lognormal ->
            let m = log (Hyper.mu_mean hy) /. log 10. in
            Fit.Dist.Lognormal (m, Hyper.mu_var hy)
        | `Uniform ->
            Fit.Dist.Uniform (0., Hyper.mu_max hy)
      in
      Fit.Dist.Map (
        (fun ({ mu } : t) -> mu),
        (fun mu : t -> { mu }),
        d
      )

    let draws infer =
      if Fit.Infer.is_fixed infer then
        []
      else
        [(fun _ dx (p : t) : t -> { mu = p.mu +. dx })]

    let jumps infer hy =
      if Fit.Infer.is_fixed infer then
        []
      else
        [hy#mu_jump]

    let term =
      S.Jc69.capture_term
  end


type vec = S.Jc69.vec
type mat = S.Jc69.mat

let settings = S.Jc69.settings

let equilibrium_probas par =
  S.Jc69.equilibrium_probas par

let transition_probas ?mat par =
  S.Jc69.transition_probas ?mat par

let flow ?mat par =
  let tp = transition_probas ?mat par in
  let trans dt =
    tp (F.Pos.of_float dt)
  in
  S.Probas.sym_transition settings trans

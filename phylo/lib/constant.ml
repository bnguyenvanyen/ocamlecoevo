open Abbrevs


let name = "Kingman"


module Hyper =
  struct
    class t0_able = object
      val t0_v : float option = None

      method t0 = t0_v
      method with_t0 x = {< t0_v = x >}
    end

    class lambda_able = object
      val lambda_prior_v : [`Lognormal | `Exponential | `Uniform] =
        `Lognormal
      val lambda_mean_v = 1.
      val lambda_max_v = 10.
      val lambda_var_v = 1.
      val lambda_jump_v = 1e-3

      method lambda_prior = lambda_prior_v
      method lambda_mean = lambda_mean_v
      method lambda_max = lambda_max_v
      method lambda_var = lambda_var_v
      method lambda_jump = lambda_jump_v

      method with_lambda_prior x = {< lambda_prior_v = x >}
      method with_lambda_mean x = {< lambda_mean_v = x >}
      method with_lambda_max x = {< lambda_max_v = x >}
      method with_lambda_var x = {< lambda_var_v = x >}
      method with_lambda_jump x = {< lambda_jump_v = x >}
    end

    class t = object
      inherit t0_able
      inherit lambda_able
    end

    let default = new t

    let t0 hy =
      hy#t0

    let tree_t0 hy =
      t0 hy

    let lambda_prior hy =
      hy#lambda_prior

    let lambda_mean hy = 
      F.Pos.of_float hy#lambda_mean

    let lambda_var hy =
      F.Pos.of_float hy#lambda_var

    let lambda_max hy =
      F.to_float (F.Pos.of_float hy#lambda_max)

    let lambda_jump hy =
      F.Pos.of_float hy#lambda_jump

    module Term =
      struct
        let float_arg ~doc ~get ~set name =
          U.Arg.Update.Capture.opt_config
            get
            set
            Cmdliner.Arg.float
            ~doc
            name

        let prior_choice_arg ~doc ~get ~set name =
          let conv = Cmdliner.Arg.enum [
            ("lognormal", `Lognormal) ;
            ("exponential", `Exponential) ;
            ("uniform", `Uniform) ;
          ]
          in
          U.Arg.Update.Capture.opt_config
            get
            set
            conv
            ~doc
            name

        let lambda_prior () =
          let doc =
            "Choose between prior shapes for lambda."
          in
          let get hy = hy#lambda_prior in
          let set x hy = hy#with_lambda_prior x in
          prior_choice_arg ~get ~set ~doc "lambda-prior"

        let lambda_mean () =
          let doc =
            "Mean of the lambda prior if lognormal or exponential."
          in
          let get hy = hy#lambda_mean in
          let set x hy = hy#with_lambda_mean x in
          float_arg ~get ~set ~doc "lambda-mean"

        let lambda_var () =
          let doc =
            "10-log-variance of the lambda prior if lognormal."
          in
          let get hy = hy#lambda_var in
          let set x hy = hy#with_lambda_var x in
          float_arg ~get ~set ~doc "lambda-var"

        let lambda_max () =
          let doc =
            "Upper bound for the lambda prior if uniform."
          in
          let get hy = hy#lambda_max in
          let set x hy = hy#with_lambda_max x in
          float_arg ~get ~set ~doc "lambda-max"

        let lambda_jump () =
          let doc =
            "Initial standard deviation of lambda jumps."
          in
          let get hy = hy#lambda_jump in
          let set x hy = hy#with_lambda_jump x in
          float_arg ~get ~set ~doc "lambda-jump"

        let t0 () =
          let doc =
            "Start from this time for the sequence and tree likelihoods. " ^
            "By default compute until the root."
          in
          let get hy = hy#t0 in
          let set x hy = hy#with_t0 x in
          U.Arg.Update.Capture.opt_config
            get
            set
            Cmdliner.Arg.(some float)
            ~doc
            "t0"
      end

    let open_term () =
      let f lbd_p lbd_m lbd_v lbd_x lbd_j t0 ~config hy =
        hy
        |> lbd_p ~config
        |> lbd_m ~config
        |> lbd_v ~config
        |> lbd_x ~config
        |> lbd_j ~config
        |> t0 ~config
      in
      Cmdliner.Term.(
        const f
        $ Term.lambda_prior ()
        $ Term.lambda_mean ()
        $ Term.lambda_var ()
        $ Term.lambda_max ()
        $ Term.lambda_jump ()
        $ Term.t0 ()
      )

    type 'a term =
      'a
      U.Arg.Capture.t
      U.Arg.configurable
      Cmdliner.Term.t

    let term : t term =
      let f update ~config =
        U.Arg.Capture.empty (new t)
        |> update ~config
      in
      Cmdliner.Term.(
        const f
        $ open_term ()
      )
  end


module Data =
  struct
    module Settings =
      struct
        type t = unit

        let term =
          Cmdliner.Term.const ()
      end

    type t = unit

    let load _ =
      ()
  end


module Theta =
  struct
    type t = {
      lambda : float ;
      (** constant pair coalescence rate *)
    }

    let default =
      { lambda = 1. }

    module Infer =
      struct
        type t = Fit.Infer.t

        type tag = [`Lambda]

        let tags = [`Lambda]

        let tag_to_string `Lambda =
          "lambda"

        let tag_to_columns (`Lambda as tag) =
          [tag_to_string tag]

        let default = Fit.Infer.adapt

        let fix `Lambda _ =
          Fit.Infer.fixed

        let infer `Lambda _ =
          Fit.Infer.adapt
      end

    let columns infer =
      if Fit.Infer.is_fixed infer then
        []
      else
        Infer.tag_to_columns `Lambda

    let extract th =
      function
      | "lambda" ->
          Some (string_of_float th.lambda)
      | _ ->
          None

    let lambda th =
      F.Pos.of_float th.lambda

    let prior hy =
      let module D = Fit.Dist in
      let d =
        match Hyper.lambda_prior hy with
        | `Lognormal ->
            let lbd_m = F.to_float (Hyper.lambda_mean hy) in
            let m = log lbd_m /. log 10. in
            D.Lognormal (m, Hyper.lambda_var hy)
        | `Exponential ->
            let lbd = F.Pos.invert (Hyper.lambda_mean hy) in
            D.Exponential lbd
        | `Uniform ->
            D.Uniform (0., Hyper.lambda_max hy)
      in
      D.Map (
        (fun ({ lambda } : t) -> lambda),
        (fun lambda : t -> { lambda }),
        d
      )

    let draws infer _ =
      if Fit.Infer.is_fixed infer then
        []
      else
       [(fun _ dx (p : t) : t -> { lambda = p.lambda +. dx })]

    let jumps infer hy =
      if Fit.Infer.is_fixed infer then
        []
      else
        [hy#lambda_jump]

    let more_draws _ _ =
      []

    let more_jumps _ _ =
      []

    let more_draws_ratio _ _ =
      []

    let term =
      let doc =
        "Pair coalescence rate (Kingman)."
      in
      let lambda = U.Arg.Capture.opt_config
        Cmdliner.Arg.float
        ~doc
        "lambda"
      in
      let get { lambda } = lambda in
      let set lambda _ = { lambda } in
      let f read ~config =
        let upd = U.Arg.Update.Capture.update_capture_default
          get
          set
          Cmdliner.Arg.float
          "lambda"
          (read ~config)
        in
        upd (U.Arg.Capture.empty default)
      in
      Cmdliner.Term.(const f $ lambda)
  end


module Initial =
  struct
    type t = unit

    module Infer =
      struct
        type t = unit

        type tag = |
        
        let tags = []

        let tag_to_string : tag -> string =
          function
          | _ ->
              .

        let default = ()

        let fix : tag -> _ =
          function
          | _ ->
              .

        let infer : tag -> _ =
          function
          | _ ->
              .
      end
    
    let columns _ =
      []

    let extract _ =
      function
      | _ ->
          None

    let prior _ =
      Fit.Dist.Constant_flat ()

    let draws _ _ =
      []

    let jumps _ _ =
      []

    let more_draws _ _ =
      []

    let more_jumps _ _ =
      []

    let more_draws_ratio _ _ =
      []

    type 'a term =
      'a
      U.Arg.Capture.t
      U.Arg.configurable
      Cmdliner.Term.t

    let term : t term =
      Cmdliner.Term.const (fun ~config:_ -> U.Arg.Capture.empty ())
  end


module Traj =
  struct
    type t = Theta.t

    let output ?every:_ _ =
      Util.Out.convert_null 

    let simulate _ th _ =
      th

    let log_likelihood _ _ _ _ =
      []

    let maybe_cut_left t0 c =
      match t0 with
      | None ->
          c
      | Some t0 ->
          begin match Jump.cut_left t0 c with
          | c ->
              c
          | exception Invalid_argument _ ->
              c
          end

    let tree_log_likelihood hy tree th =
      let lambda = Theta.lambda th in
      let t0 = Hyper.tree_t0 hy in
      let rev_nlineages =
        tree
        (* get the coalescent event intervals *)
        |> T.nlineages_backwards
        (* only take starting from t0 *)
        |> maybe_cut_left t0
        (* look backwards, from the tip to the root *)
        |> Jump.reverse
      in
      let logd = Jump.fold_succ_left (fun logd t n t' n' ->
          (* going back in time *)
          assert (t' < t) ;
          (* merge event rate *)
          let fn = F.Pos.of_float (float n) in
          let fnm1 = F.Pos.of_float (float (n - 1)) in
          (* total coal rate *)
          (* lambda * number of ordered pairs *)
          let lbd = F.Pos.Op.(lambda * fn * fnm1 / F.two) in
          let dlogd =
            if n' > n then
              (* sampling event - leaves can have equal dates *)
              (* proba of no merge event between t and t' *)
              (* e (- (lbd * (t - t'))) *)
              (* log proba: lbd * (t' - t) *)
              F.Op.(lbd * F.Neg.of_float (t' -. t))
            else if n' = n - 1 then
              (* merge event *)
              (* density of merge event at t *)
              U.logd_exp lbd (t -. t')
            else begin
              (* unexpected event *)
              (* typically several coalescences happen at the same time,
               * which is forbidden *)
              (* F.neg_infinity *)
              failwith "Unexpected event"
            end
          in
          F.Op.(logd + dlogd)
      ) F.zero rev_nlineages
      in
      [F.to_float logd]
  end

open Abbrevs


(* This is for SIR *)
module Model = Epifit.Sir


module Sim =
  struct
    let draw_tree data_seq rng lbd =
      T.draw data_seq rng lbd

    let init_probas data_seq =
      Probas.init Homogen data_seq 1

    let update_probas data_seq lik_seq =
      if lik_seq then
        Constant_jc69.Sim.compute_probas data_seq
      else
        (fun _ _ probas -> probas)
  end


module Hyper =
  struct
    class sequences_weight_able = object
      val sequences_weight_v = 1.

      method sequences_weight = sequences_weight_v
      method with_sequences_weight x = {< sequences_weight_v = x >}
    end

    (* FIXME this pulls in many things I don't need *)
    class t = object
      inherit Epifit.Param.prior_base
      inherit Epifit.Param.prop_base
      inherit Epifit.Param.lik
      inherit Epifit.Param.odeable
      inherit Coalfit.Param.prior
      inherit Coalfit.Param.prop
      inherit Hyper.proposal_weights_able
      inherit Hyper.scalable
      inherit Hyper.slide_root_able
      inherit Hyper.spr_div_scalable
      inherit Hyper.spr_guided_scalable
      inherit sequences_weight_able
    end

    let spr_div_scale hy =
      F.Pos.of_float hy#spr_div_scale

    let spr_guided_scale hy =
      F.Pos.of_float hy#spr_guided_scale

    module Term =
      struct
        let sequences_weight () =
          let doc =
            "Weight of sequences likelihood"
          in
          let get hy = hy#sequences_weight in
          let set x hy = hy#with_sequences_weight x in
          Epifit.Term.float_arg ~get ~set ~doc "sequences-weight"

        let plus () =
          let f sc_j sr_j sd sg seq_w ~config hy =
            hy
            |> sc_j ~config
            |> sr_j ~config
            |> sd ~config
            |> sg ~config
            |> seq_w ~config
          in
          Cmdliner.Term.(
            const f
            $ Hyper.Term.scale_jump ()
            $ Hyper.Term.slide_root_jump ()
            $ Hyper.Term.spr_div_scale ()
            $ Hyper.Term.spr_guided_scale ()
            $ sequences_weight ()
          )
      end

    let config_term =
      let f base lik plus pw load_theta ~config capture =
        (new t)
        |> U.Arg.Capture.empty
        |> base load_theta ~config
        |> lik ~config
        |> plus ~config
        |> U.Arg.Capture.output capture
        |> pw  (* FIXME not captured *)
      in Cmdliner.Term.(
          const f
        $ Epifit.Term.Hyper.base ()
        $ Epifit.Term.Hyper.lik ()
        $ Term.plus ()
        $ Hyper.Term.proposal_weights ()
      )

    let term =
      let f consume load_theta config =
        consume load_theta ~config
      in
      Cmdliner.Term.(
        const f
        $ config_term
        $ Epifit.Term.Theta.load_from
        $ Epifit.Term.Hyper.load_from
      )
  end


(* For data we also have two types of data, one for the seq side,
 * and one for the epi side. *)
module Data =
  struct
    module Epi = Epifit.Data

    module Seq = Seq_data

    type t = {
      seq : Seq.t ;
      epi : Epi.t ;
    }

    let load path_root =
      let epi = Epifit.Cli.load_data path_root in
      let seq = Seq_data.load path_root in
      { epi ; seq }

    let t0 { epi ; _ } =
      Option.map (fun cases ->
        Jump.t0 cases
      ) epi.cases
  end


(* what is inferred by the MCMC *)
(* I consider that the traj is also a part of it *)
module Theta =
  struct
    module Traj =
      struct
        type t = Epi_traj.t

        let simulate hy epi z0 =
          Jump.Cadlag_map.of_list (
          Model.S.simulate
           ~out:`Cadlag
           Model.Fit.state_settings
           (Ode Simple)
           hy
           epi
           z0
           ()
          )
      end
        
    module Epi =
      struct
        class t = object
          inherit Epi.Param.t_unit
        end

        let term = Model.Fit.term
      end

    module Seq =
      struct
       type t = Seqsim.Jc69.t

       module Term =
         struct
           let mu () =
             let doc =
               "Initial mutation rate."
             in
             let get (th_seq : t) = th_seq.mu in
             let set x (_ : t) = { Seqsim.Jc69.mu = x } in
             Epifit.Term.float_arg ~get ~set ~doc "mu"
         end

       let term =
         let f mu =
           { Seqsim.Jc69.mu = 1. }
           |> U.Arg.Capture.empty
           |> mu ~config:None
           |> U.Arg.Capture.output None
         in
         Cmdliner.Term.(
           const f
           $ Term.mu ()
         )
      end

    module Probas =
      struct
        type t = (Lac.Mat.t * S.Int.t) M.Int.t

        let copy p =
          M.Int.map (fun (ps, is) -> (Lac.lacpy ps, is)) p
      end

    class ['tree] poly
      ((seq, epi, tree, z0, traj, probas) :
       (Seq.t * Epi.t * 'tree * Epifit.Sig.sir * Traj.t * Probas.t)) =
    object
      val seq_v = seq
      val epi_v = epi
      val tree_v = tree
      val z0_v = z0
      val traj_v = traj
      val probas_v = probas

      method seq = seq_v
      method epi = epi_v
      method tree = tree_v
      method z0 = z0_v
      method traj = traj_v
      method probas = probas_v

      method with_seq seq = {< seq_v = seq >}
      method with_epi epi = {< epi_v = epi >}
      method with_z0 z0 = {< z0_v = z0 >}
      (* no with_tree because it can't be at a different type *)
      method with_traj traj = {< traj_v = traj >}
      method with_probas probas = {< probas_v = probas >}
    end

    (* mentally a <
        seq : Seq.t ;
        epi : Epi.t ;
        tree : T.t ;
        z0 : Lac.Vec.t ;
        traj : Traj.t ;
        probas : Probas.t ;
      >
     *)
    type t = T.t poly

    type param = <
      seq : Seq.t ;
      epi : Epi.t ;
      z0 : Epifit.Sig.sir ;
    >

    let make seq epi tree z0 traj probas =
      new poly (seq, epi, tree, z0, traj, probas)

    let with_seq seq th =
      th#with_seq seq

    let with_epi epi th =
      th#with_epi epi

    let with_tree tree th =
      make th#seq th#epi tree th#z0 th#traj th#probas

    let with_z0 z0 th =
      th#with_z0 z0

    let with_traj traj th =
      th#with_traj traj

    (* FIXME should we copy here ? *)
    let with_probas probas th =
      th#with_probas probas

    let copy th =
      (* FIXME premature ? *)
      let probas = Probas.copy th#probas in
      th
      |> with_probas probas

    (*
    let t0 hy data tree =
      let cas_t0 = Data.t0 data in
      let tree_t0 = Tree.t0 tree in
      match cas_t0 with
      | None ->
          tree_t0
      | Some cas_t0 ->
          let dt = F.to_float (Epifit.Param.dt hy) in
          min (cas_t0 -. dt) tree_t0
    *)

    let simulate_traj hy epi z0 =
      Traj.simulate hy epi z0

    let refresh_traj hy th =
      let traj = simulate_traj hy th#epi th#z0 in
      with_traj traj th

    let update_probas (data : Data.t) lik th =
      let probas = Sim.update_probas data.seq lik th#seq th#tree th#probas in
      with_probas probas th
  end


module Prior =
  struct
    module Seq =
      struct
        let dist hy =
          Fit.Dist.Map (
            (fun Seqsim.Jc69.{ mu } -> mu),
            (fun mu -> ({ mu } : Seqsim.Jc69.t)),
            (Constant_jc69.Prior.mu hy)
          )
      end

    module Epi =
      struct
        (* small difficulty : z0 is apart from theta in epifit,
         * so what should it look like in the end ?
         * I think I keep them apart *)
        let theta hy =
          (* for the prior, always use free *)
          let infer = new Epifit.Infer.base in
          let pars = Model.Fit.params infer hy in
          (* need a starting point for the prior,
           * but if everything is inferred, it matters little *)
          Fit.Param.List.prior (new Theta.Epi.t) pars

        (* FIXME Can this work correctly with z0 state ? *)
        let z0 hy =
          (* for the prior, always use free *)
          let infer = new Epifit.Infer.base in
          let pars = Model.Fit.params_state infer hy in
          (* prior base does not matter when free *)
          let z0 = Model.Fit.to_state (new Epifit.Episim.theta) in
          Fit.Param.List.prior z0 pars
      end

    let dist hy =
      (* go through many constructors : ugly *)
      Fit.Dist.joint6
        ~get:(fun (th : Theta.t) ->
           (th#seq, th#epi, th#tree, th#z0, th#traj, th#probas)
         )
        ~combine:Theta.make
        (Seq.dist hy)
        (Epi.theta hy)
        Fit.Dist.Flat
        (Epi.z0 hy)
        Fit.Dist.Flat
        Fit.Dist.Flat
  end


module Likelihood =
  struct
    module Settings =
      struct
        type t = {
          seq : bool ;
          epi : [`None | `Negbin | `Poisson] ;
        }

        module Term =
          struct
            let ignore_seq =
              let doc =
                "Flag to ignore the sequence data."
              in
              Cmdliner.Arg.(
                value
                & flag
                & info ["ignore-sequences"] ~docv:"IGNORE-SEQUENCES" ~doc
              )

            let seq =
              Cmdliner.Term.(const not $ ignore_seq)

            let epi =
              let doc =
                "Control the cases count likelihood."
              in
              let en = Cmdliner.Arg.enum [
                ("none", `None) ;
                ("poisson", `Poisson) ;
                ("negbin", `Negbin) ;
              ]
              in
              Cmdliner.Arg.(
                value
                & opt en `Negbin
                & info ["likelihood-cases"] ~docv:"LIKELIHOOD-CASES" ~doc
              )
          end

        let term =
          let f seq epi =
            { seq ; epi }
          in
          Cmdliner.Term.(
            const f
            $ Term.seq
            $ Term.epi
          )
      end
 
    type settings = Settings.t = {
      seq : bool ;
      epi : [`None | `Negbin | `Poisson] ;
    }

    (* three different likelihoods *)
    module Seq =
      struct
        let log_dens_proba data_seq =
          (* we never need to hold onto transition matrices,
           * so we can put them all in the same memory *)
          let mat = Seqsim.Snp.zero_mat None in
          let comp th_seq probas tree =
            let eq = Seqsim.Jc69.equilibrium_probas th_seq in
            let trans = Constant_jc69.Sim.transition ~mat th_seq in
            Probas.log_likelihood
              Homogen
              data_seq
              eq
              trans
              probas
              tree
          in comp
      end

    module Tree =
      struct
        (* SIR is easy because there is only one lineage color, I.
         * So the likelihood is only that of the coalescences happening
         * when they do.
         * The formula given in Volz 2012 is slightly suspicious,
         * but I still use it here, to stay consensual
         * and avoid losing more time *)

        let rate n (pt : Epi.Traj.point) =
          let fn = F.Pos.of_float (float n) in
          let fnm1 = F.Pos.of_float (float (n - 1)) in
          (* check that n <= i *)
          if F.Op.(fn > pt.i) then begin
            failwith "n > i"
          end ;
          F.Pos.Op.(fn * fnm1 * pt.coal)

        (* n the number of bundles at t *)
        let merge_rate traj t n =
          rate n (Jump.Cadlag_map.eval traj t )

        (* n is the (constant) number of bundles between t and t' *)
        let merge_pressure traj t t' n =
          assert (t <= t') ;
          Jump.Cadlag_map.integrate ~from_t:t ~to_t:t' (rate n) traj

        let maybe_cut_left t0 c =
          match Jump.cut_left t0 c with
          | c ->
              c
          | exception Invalid_argument _ ->
              c

        let log_dens_proba hy traj tree =
          let dlogd t n t' n' =
            (* integrate the merge event rate between t and t' *)
            let sum_lbd =
              merge_pressure traj (F.of_float t') (F.of_float t) n
            in
            if n' = n + 1 then
              (* sampling event *)
              F.Op.(~- sum_lbd)
            else if n' = n - 1 then
              (* merge event *)
              (* merge rate at merge point *)
              let lbd = merge_rate traj (F.of_float t') n in
              F.Op.(F.log lbd - sum_lbd)
            else
                failwith "Unexpected event"
          in
          (* t0 is required *)
          let t0 = U.Option.some hy#t0 in
          let rev_nlineages =
            tree
            (* get the coalescent event intervals *)
            |> T.nlineages_backwards
            (* only take starting from t0 *)
            |> maybe_cut_left t0
            (* look backwards, from the tip to the root *)
            |> Jump.reverse
          in
          let logd = Jump.fold_succ_left (fun logd t n t' n' ->
            let dlogd =
              match dlogd t n t' n' with
              | dlogd ->
                  dlogd
              | exception Failure _ ->
                  F.of_float (~-. 1000.)
            in
            F.Op.(logd + dlogd)
          ) F.zero rev_nlineages
          in
          [F.to_float logd]
      end

    module Epi =
      struct
        (* FIXME grab from Epi_traj ? *)
        let log_dens_proba hy obs data_epi =
          let loglikfs = [
            (fun hy Epifit.Data.{ cases ; _ } traj ->
              Epifit.Observe.reported_cases
                ~obs
                hy
                (U.Option.some cases)
                (Epi.Traj.reporting_rate (Jump.Cadlag_map.to_list traj))
            ) ;
          ]
          in
          Epifit.Likelihood.traj_log_dens_proba loglikfs hy data_epi
      end

    let dist hy settings (data : Data.t) =
      let draw _ =
        failwith "Not_implemented"
      in
      let log_dens_proba (th : Theta.t) =
        let seq_lik =
          if settings.seq then
            Seq.log_dens_proba data.seq th#seq th#probas th#tree
          else
            []
        in
        let epi_lik =
          match settings.epi with
          | `None ->
              []
          | (`Poisson | `Negbin) as obs ->
              Epi.log_dens_proba hy obs data.epi th#traj
        in
        let tree_lik = Tree.log_dens_proba hy th#traj th#tree in
        let w_seq_lik = L.map (fun x -> hy#sequences_weight *. x) seq_lik in
          w_seq_lik
        @ epi_lik
        @ tree_lik
      in
      Fit.Dist.Base { draw ; log_dens_proba }
  end


module Propose =
  struct
    module Tree =
      struct
        module P = Treefit.Propose.Make (T)
        module P' = Treefit.Propose_guided.Make (T)

        let scale dx tree =
          let alpha = F.exp (F.of_float dx) in
          match P.scale alpha tree with
          | tree' ->
              tree'
          | exception (Invalid_argument _ as e) ->
              (* scaling hit a leaf *)
              Fit.raise_draw_error e

        let slide_root dx tree =
          match P.slide_node 0 dx tree with
          | _, tree' ->
              tree'
          | exception (Invalid_argument _ as e) ->
              Fit.raise_draw_error e

        let div_var data_seq hy th_seq =
          let mu = Seqsim.Jc69.mu th_seq in
          let m = F.of_float (float (Data.Seq.nsites data_seq)) in
          let sgm = Hyper.spr_div_scale hy in
          let d = F.mul sgm (F.invert (F.mul mu m)) in
          F.sq d

        let guided_sigma data_seq hy th_seq =
          let mu = Seqsim.Jc69.mu th_seq in
          let m = F.Pos.of_float (float (Data.Seq.nsites data_seq)) in
          let sgm = Hyper.spr_guided_scale hy in
          F.Pos.Op.(sgm * mu * m)

        (* Can I have th_seq ? *)
        let spr_div data_seq hy th_seq =
          let nint = Data.Seq.ninterior data_seq in
          let v = div_var data_seq hy th_seq in
          P.Spr.Divergence.move nint v

        let spr_unif data_seq =
          let nint = Data.Seq.ninterior data_seq in
          P.Spr.Uniform.move nint

        let spr_guided data_seq lik_seq hy th_seq =
          let upd = Sim.update_probas
            data_seq
            lik_seq
            th_seq
          in
          let update tree _ probas =
            upd tree probas
          in
          let sgm = guided_sigma data_seq hy th_seq in
          P'.Spr.move Homogen update sgm

        let dummy_spr_div_context =
          let x_dummy = (0., 0) in
          P.Spr.Divergence.{
            x_prune = x_dummy ;
            x_prune_parent = x_dummy ;
            x_graft_parent = x_dummy ;
            t_graft = 0. ;
            distance = 0. ;
            moves = [] ; 
          }

        let dummy_spr_unif_context =
          let x_dummy = (0., 0) in
          P.Spr.Uniform.{
            x_prune = x_dummy ;
            x_prune_parent = x_dummy ;
            x_graft_parent = x_dummy ;
            length = 0. ;
          }

        let dummy_spr_div_move tree =
          (tree, dummy_spr_div_context)

        let dummy_spr_unif_move tree =
          (tree, dummy_spr_unif_context)

        let dummy_spr_guided_context =
          let x_dummy = (0., 0) in
          P'.Spr.{
            x_prune = x_dummy ;
            x_prune_sub = x_dummy ;
            x_prune_parent = x_dummy ;
            x_prune_sibling = x_dummy ;
            x_graft_parent = x_dummy ;
            x_graft_sibling = x_dummy ;
          }
      end

    let prop_tree_in_theta data_seq lik_seq prop =
      let get th =
        th#tree,
        (th#seq, (th#epi, (th#z0, (th#traj, th#probas))))
      in
      let combine move (seq, (epi, (z0, (traj, probas)))) =
        (* update probas *)
        let tree = Fit.Propose.move_to_sample prop move in
        let probas = Sim.update_probas
          data_seq
          lik_seq
          seq
          tree
          probas
        in
        Theta.make seq epi move z0 traj probas
      in
      Fit.Propose.Both {
        first = prop ;  (* tree *)
        second = Fit.Propose.constant_flat_5 () ;
        (* seq epi z0 traj probas *)
        get_sample = get ;
        get_move = get ;
        combine_sample = combine ;
        combine_move = combine ;
      }

    let prop_seqtree_in_theta data_seq lik_seq pair =
      let get th =
        (th#seq, th#tree),
        (th#epi, (th#z0, (th#traj, th#probas)))
      in
      let combine_sample (seq, tree) (epi, (z0, (traj, probas))) =
        Theta.make seq epi tree z0 traj probas
      in
      let combine_move (seq, move) (epi, (z0, (traj, probas))) =
        (* update probas *)
        let _, tree = Fit.Propose.move_to_sample pair (seq, move) in
        let probas = Sim.update_probas
          data_seq
          lik_seq
          seq
          tree
          probas
        in
        Theta.make seq epi move z0 traj probas
      in
      Fit.Propose.Both {
        first = pair ;  (* seq+tree *)
        second = Fit.Propose.constant_flat_4 () ;
        (* epi z0 traj probas *)
        get_sample = get ;
        get_move = get ;
        combine_sample ;
        combine_move ;
      }

    let prop_seq_tree_probas_in_theta trio =
      let get_sample th =
        (th#seq, (th#tree, th#probas)),
        (th#epi, (th#z0, th#traj))
      in
      let combine_sample (seq, (tree, probas)) (epi, (z0, traj)) =
        Theta.make seq epi tree z0 traj probas
      in
      let get_move (th, context) =
        (th#seq, (th#tree, th#probas, context)),
        (th#epi, (th#z0, th#traj))
      in
      let combine_move (seq, (tree, probas, context)) (epi, (z0, traj)) =
        (Theta.make seq epi tree z0 traj probas, context)
      in
      Fit.Propose.Both {
        first = trio ;  (* seq+probas+tree (+context) *)
        second = Fit.Propose.constant_flat_3 () ;
        (* epi z0 traj *)
        get_sample ;
        get_move ;
        combine_sample ;
        combine_move ;
      }

    let spr_div (data : Data.t) (lik : Likelihood.settings) hy =
      prop_seqtree_in_theta
        data.seq
        lik.seq
        (Fit.Propose.pair_dep
          Fit.Propose.Constant_flat
          (Tree.spr_div data.seq hy)
        )

    let spr_unif (data : Data.t) (lik : Likelihood.settings) =
      prop_seqtree_in_theta
        data.seq
        lik.seq
        (Fit.Propose.pair
          Fit.Propose.Constant_flat
          (Tree.spr_unif data.seq)
        )

    let spr_guided (data : Data.t) (lik : Likelihood.settings) hy =
      prop_seq_tree_probas_in_theta
        (Fit.Propose.pair_dep
          Fit.Propose.Constant_flat  (* th#seq *)
          (Tree.spr_guided data.seq lik.seq hy)
        )
  end


module Output =
  struct
    module Tree =
      struct
        let sample = T.output_sample

        let move = T.output_move
      end

    (* this is the part for the param part *)
    module Param =
      struct
        let columns =
          (* hard-code it *)
          let tags = [ `Beta ; `Nu ; `Rho ; `Sir ] in
            Fit.Csv.columns_sample_move
          @ [ "mu" ]
          @ (L.fold_left (fun cs tag ->
              cs @ (Model.Fit.tag_to_columns tag)
             ) [] tags
          )
          @ [ "move_mu" ]
          @ (L.fold_left (fun cs tag ->
              cs @ (L.map (fun s -> "move_" ^ s) (Model.Fit.tag_to_columns tag))
            ) [] tags
          )

        let extract k ret =
          let ext th =
            function
            | "mu" ->
                string_of_float th#seq.Seqsim.Jc69.mu
            | ("s0" | "i0" | "r0" | "ps0" | "pi0" | "pr0") as s ->
                Model.Fit.extract_state th#z0 s
            | s ->
                Model.Fit.extract th#epi s
          in
          Some (Fit.Csv.extract_sample_move
            ~move_to_sample:(fun x -> x)
            ~extract:ext
            k ret
          )

        let return ?theta_every ?chol_every chol path =
          Simfit.Out.convert_csv_ram
            ?n_thin:theta_every
            ?chol_every
            ~columns
            ~extract
            ~chol
            path
      end

    module Traj =
      struct
        let return ?traj_every path =
          Epi_traj.output ?every:traj_every path
      end

    (* Then need the outputs for the different proposals *)

    let param_return ret =
      Fit.Mh.Return.map
        (fun th -> (th :> Theta.param))
        (fun th -> (th :> Theta.param))
        ret

    let param_left_return ret =
      Fit.Mh.Return.map
        (fun th -> (th :> Theta.param))
        (fun (th, _) -> (th :> Theta.param))
        ret

    let traj_sample ret =
      let th = Fit.Mh.Return.sample ret in
      th#traj

    let tree_sample ret =
      let th = Fit.Mh.Return.sample ret in
      th#tree

    let tree_div_move ret =
       match Fit.Mh.Return.move ret with
       | th ->
           let tree, _ = th#tree in
           Some tree
       | exception Invalid_argument _ ->
           None

    let tree_unif_move ret =
      match Fit.Mh.Return.move ret with
      | th ->
          let tree, _ = th#tree in
          Some tree
      | exception Invalid_argument _ ->
          None

    let tree_guided_move ret =
      match Fit.Mh.Return.move ret with
      | th, _ ->
          Some th#tree
      | exception Invalid_argument _ ->
          None

    let ram par_out traj_out tree_out =
      U.Out.combine
        (fun ret _ -> ret)
        (U.Out.combine
           (fun ret _ -> ret)
           (U.Out.map param_return par_out)
           (U.Out.map traj_sample traj_out)
        )
        (U.Out.map tree_sample tree_out)

    let spr_div par_out tree_out tree_mv_out =
      U.Out.combine
        (fun ret _ -> ret)
        (U.Out.map param_return par_out)
        (U.Out.combine
          (fun ret _ -> ret)
          (U.Out.map tree_sample tree_out)
          (U.Out.map tree_div_move tree_mv_out)
        )

    let spr_unif par_out tree_out tree_mv_out =
      U.Out.combine
        (fun ret _ -> ret)
        (U.Out.map param_return par_out)
        (U.Out.combine
          (fun ret _ -> ret)
          (U.Out.map tree_sample tree_out)
          (U.Out.map tree_unif_move tree_mv_out)
        )

    let spr_guided par_out tree_out tree_mv_out =
      U.Out.combine
        (fun ret _ -> ret)
        (U.Out.map param_left_return par_out)
        (U.Out.combine
          (fun ret _ -> ret)
          (U.Out.map tree_sample tree_out)
          (U.Out.map tree_guided_move tree_mv_out)
        )
  end


module Infer =
  struct
    module Epi =
      struct
        class t = object
          inherit Epifit.Infer.base
        end
      end

    type t = {
      seq : Fit.Infer.t ;
      epi : Epi.t ;
      tree : Fit.Infer.t ;
    }

    let default = {
      seq = Free `Adapt ;
      tree = Free `Adapt ;
      epi = new Epi.t ;
    }

    let tag_to_string =
      function
      | `Seq ->
          "seq"
      | `Tree ->
          "tree"
      | #Epifit.Tag.base as tag ->
          Model.Fit.tag_to_string tag

    let fix =
      function
      | `Seq ->
          (fun infer -> { infer with seq = Fixed })
      | `Tree ->
          (fun infer -> { infer with tree = Fixed })
      | #Epifit.Tag.base as tag ->
          (fun infer ->
            { infer with epi = Epifit.Infer.fix_base tag infer.epi }
          )

    module Term =
      struct
        let infer_tags =
          [`Seq ; `Tree] @ Epifit.Symbols.base
          |> L.map (fun tag -> (tag_to_string tag, tag))
          |> Cmdliner.Arg.enum

        let fix_tags =
          let doc =
            "Keep this parameter fixed."
          in
          Cmdliner.Arg.(
            value
            & opt_all infer_tags []
            & info ["fix"] ~docv:"FIX" ~doc
          )
      end

    let term =
      let f fix_tags =
        L.fold_right fix fix_tags default
      in
      Cmdliner.Term.(
        const f
        $ Term.fix_tags
      )
  end


module Estimate =
  struct
    let to_chol js =
      js
      |> A.of_list
      |> Lac.Vec.of_array
      |> Lac.Mat.of_diag

    let tree_scale_draws (infer : Infer.t) =
      if Fit.Infer.is_free_adapt infer.tree then
        [
          (fun _ dx th ->
            let move = Propose.Tree.scale dx th#tree in
            Theta.with_tree move th
          ) ;
        ]
      else
        []

    let tree_scale_jumps (infer : Infer.t) hy =
      if Fit.Infer.is_free_adapt infer.tree then
        [hy#scale_jump]
      else
        []

    let tree_slide_root_draws (infer : Infer.t) =
      if not (Fit.Infer.is_fixed infer.tree) then
        [
          (fun _ dx th ->
            let move = Propose.Tree.slide_root dx th#tree in
            Theta.with_tree move th
          ) ;
        ]
      else
        []

    let tree_slide_root_jumps (infer : Infer.t) hy =
      if not (Fit.Infer.is_fixed infer.tree) then
        [hy#slide_root_jump]
      else
        []

    let seq_draws infer =
      if Fit.Infer.is_free_adapt infer then
        [
          (fun _ dx (th : Theta.t) ->
            let mu = th#seq.mu +. dx in
            Theta.with_seq { Seqsim.Jc69.mu } th
          ) ;
        ]
      else
        []

    let z0_draws =
      function
      | `All (Fit.Infer.Free `Adapt) -> [
          (* z0 ~~ unsafe, needs copy *)
          (fun _ dx th ->
            let (sir : Epifit.Sig.sir) = th#z0 in
            let sir = { sir with s = sir.s +. dx } in
            th#with_z0 sir
          ) ;
          (fun _ dx th ->
            let (sir : Epifit.Sig.sir) = th#z0 in
            let sir = { sir with i = sir.i +. dx } in
            th#with_z0 sir
          ) ;
          (fun _ dx th ->
            let (sir : Epifit.Sig.sir) = th#z0 in
            let sir = { sir with r = sir.r +. dx } in
            th#with_z0 sir
          ) ;
        ]
      | `Only_i (Fit.Infer.Free `Adapt) -> [
          (fun _ dx th ->
            let (sir : Epifit.Sig.sir) = th#z0 in
            let sir = { sir with i = sir.i +. dx } in
            th#with_z0 sir
          ) ;
        ]
      | `All (Fixed | Free `Custom | Free `Prior)
      | `Only_i (Fixed | Free `Custom | Free `Prior) ->
          []

    let epi_draws (infer : Infer.Epi.t) =
        (z0_draws infer#sir)
      @ (if Fit.Infer.is_free_adapt infer#beta then
           [
             (fun _ dx th ->
               let beta = th#epi#beta +. dx in
               let epi = th#epi#with_beta beta in
               Theta.with_epi epi th
             ) ;
           ]
         else
           []
      )
      @ (if Fit.Infer.is_free_adapt infer#nu then
           [
             (fun _ dx th ->
               let nu = th#epi#nu +. dx in
               let epi = th#epi#with_nu nu in
               Theta.with_epi epi th
             ) ;
           ]
         else
           []
      )
      @ (if Fit.Infer.is_free_adapt infer#rho then
           [
             (fun _ dx th ->
               let rho = th#epi#rho +. dx in
               let epi = th#epi#with_rho rho in
               Theta.with_epi epi th
             ) ;
           ]
         else
           []
      )

    let par_draws (infer : Infer.t) =
        (seq_draws infer.seq)
      @ (epi_draws infer.epi)

    (* also by hand *)
    let par_jumps (infer : Infer.t) hy =
        (if Fit.Infer.is_free_adapt infer.seq then
           [hy#mu_jump]
         else
           []
        )
      @ (match infer.epi#sir with
         | `All (Fit.Infer.Free `Adapt) -> [
             hy#sr_jump *. hy#popsize ;
             hy#ei_jump *. hy#popsize ;
             hy#sr_jump *. hy#popsize ;
           ]
         | `Only_i (Fit.Infer.Free `Adapt) -> [
             hy#ei_jump *. hy#popsize ;
           ]
         | `All (Fixed | Free `Custom | Free `Prior)
         | `Only_i (Fixed | Free `Custom | Free `Prior) ->
             []
      )
      @ (if Fit.Infer.is_free_adapt infer.epi#beta then
           [hy#beta_jump]
         else
           []
      )
      @ (if Fit.Infer.is_free_adapt infer.epi#nu then
           [hy#nu_jump]
         else
           []
      )
      @ (if Fit.Infer.is_free_adapt infer.epi#rho then
           [hy#rho_jump]
         else
           []
      )

    let vector_draw ~before ~after draws =
      let vd =
        Fit.Propose.vector_draw (A.of_list draws)
      in
      (fun rng v (th : Theta.t) ->
         th
         |> before
         |> vd rng v
         |> after
      )

    let as_sprd_move th =
      let move = Propose.Tree.dummy_spr_div_move th#tree in
      Theta.with_tree move th

    let as_spru_move th =
      let move = Propose.Tree.dummy_spr_unif_move th#tree in
      Theta.with_tree move th

    let as_sprg_move th =
      let ctxt = Propose.Tree.dummy_spr_guided_context in
      (th, ctxt)

    let proposal
      ?theta_every ?traj_every ?tree_every ?chol_every ?adapt_until 
      ~path lik_settings infer data (hy : Hyper.t) =
      let par_chol = to_chol (par_jumps infer hy) in
      let tsc_draws = tree_scale_draws infer in
      let tsc_chol = to_chol (tree_scale_jumps infer hy) in
      let tsr_draws = tree_slide_root_draws infer in
      let tsr_chol = to_chol (tree_slide_root_jumps infer hy) in
      let prior = Prior.dist hy in
      let likelihood = Likelihood.dist hy lik_settings data in
      let par_out = Output.Param.return
        ?theta_every
        ?chol_every
        par_chol
        path
      in
      let traj_out = Output.Traj.return
        ?traj_every
        path
      in
      let tree_out, tree_close = Output.Tree.sample
        ?every:tree_every
        path
      in
      let tree_mv_out, tree_mv_close = Output.Tree.move
        ?every:tree_every
        path
      in
      let ram_output = Output.ram
        par_out
        traj_out
        tree_out
      in
      let sprd_output = Output.spr_div par_out tree_out tree_mv_out in
      let spru_output = Output.spr_unif par_out tree_out tree_mv_out in
      let sprg_output = Output.spr_guided par_out tree_out tree_mv_out in
      let start k th =
        let smpl = Fit.Mh.Sample.weight prior likelihood th in
        let par_res = Output.param_return
          (Fit.Mh.Return.accepted (fun x -> x) smpl)
        in
        par_out.start k par_res ;
        tree_out.start k th#tree ;
        smpl
      in
      let return k smplf =
        let ram_res = Fit.Mh.Return.accepted (fun x -> x) smplf in
        let sprd_res = Fit.Mh.Return.accepted as_sprd_move smplf in
        let spru_res = Fit.Mh.Return.accepted as_spru_move smplf in
        let _ = ram_output.return k ram_res in
        let _ = sprd_output.return k sprd_res in
        let resf = spru_output.return k spru_res in
        tree_close () ; tree_mv_close () ;
        resf
      in
      let par_ram_prop = Fit.Ram.Loop.proposal
        ~output:ram_output
        ?kf:adapt_until
        (fun x -> x)
        (fun x -> x)
        (L.length (par_draws infer))
        par_chol
        prior
        (vector_draw
          ~before:Fun.id
          ~after:(fun th ->
            th
            (* we update epi pars *)
            |> Theta.refresh_traj hy
            (* we also update seq pars *)
            |> Theta.update_probas data lik_settings.seq
          )
          (par_draws infer)
        )
        likelihood
      in
      let tsc_ram_prop = Fit.Ram.Loop.proposal
        ~output:ram_output
        ?kf:adapt_until
        (fun x -> x)
        (fun x -> x)
        (L.length tsc_draws)  (* (tree_draws infer.tree)) *)
        tsc_chol
        prior
        (vector_draw
          ~before:Fun.id
          ~after:(Theta.update_probas data lik_settings.seq)
          tsc_draws
        )
        likelihood
      in
      let tsr_ram_prop = Fit.Ram.Loop.proposal
        ~output:ram_output
        ~adapt:false
        ?kf:adapt_until
        (fun x -> x)
        (fun x -> x)
        (L.length tsr_draws)
        tsr_chol
        prior
        (vector_draw
          ~before:Fun.id
          ~after:(Theta.update_probas data lik_settings.seq)
          tsr_draws
        )
        likelihood
      in
      let sprd_mh_prop = Fit.Mh.Loop.proposal
        ~output:sprd_output
        as_sprd_move
        prior
        (Propose.spr_div data lik_settings hy)
        likelihood
      in
      let spru_mh_prop = Fit.Mh.Loop.proposal
        ~output:spru_output
        as_spru_move
        prior
        (Propose.spr_unif data lik_settings)
        likelihood
      in
      let sprg_mh_prop = Fit.Mh.Loop.proposal
        ~output:sprg_output
        as_sprg_move
        prior
        (Propose.spr_guided data lik_settings hy)
        likelihood
      in
      let choices =
          [(par_ram_prop, hy#proposal_weights.th_ram)]
        @ (if not (Fit.Infer.is_fixed infer.tree) then [
             (tsc_ram_prop, hy#proposal_weights.tsc_ram) ;
             (tsr_ram_prop, hy#proposal_weights.tsr_ram) ;
             (sprd_mh_prop, hy#proposal_weights.sprd_mh) ;
             (spru_mh_prop, hy#proposal_weights.spru_mh) ;
             (sprg_mh_prop, hy#proposal_weights.sprg_mh) ;
           ]
           else
             []
        )
      in
      let proposal = Fit.Propose.Choice choices in
      proposal, start, return

    let posterior
      ?seed ?theta_every ?traj_every ?tree_every ?chol_every ?adapt_until
      ~path hypar lik_settings infer data th_epi0 th_seq0 tree0 niter =
      let proposal, start, return = proposal
        ?theta_every
        ?traj_every
        ?tree_every
        ?chol_every
        ?adapt_until
        ~path
        lik_settings
        infer
        data
        hypar
      in
      (* make the starting state *)
      let tree0 =
        match tree0 with
        | None ->
            (* just start from a dumb constant tree *)
            (* right order of magnitude of coalescence rate *)
            let lbd = F.Pos.of_float th_epi0#beta in
            Sim.draw_tree data.seq (U.rng seed) lbd
        | Some tr ->
            tr
      in
      (* make z0 from th_epi *)
      let z0 = Model.Fit.to_state th_epi0 in
      (* simulate traj *)
      let traj0 = Theta.simulate_traj hypar (th_epi0 :> Theta.Epi.t) z0 in
      (* init probas *)
      let probas0 = Sim.update_probas
        data.seq
        lik_settings.seq
        th_seq0
        tree0
        (Sim.init_probas data.seq)
      in
      (* make starting theta *)
      let th0 = Theta.make
        th_seq0
        (th_epi0 :> Theta.Epi.t)
        tree0
        z0
        traj0
        probas0
      in
      let smpl0 = start 0 th0 in
      let smplf =
        Fit.Markov_chain.direct_simulate ?seed proposal smpl0 niter
      in
      return niter smplf
  end


module Term =
  struct
    module C = Cmdliner

    let adapt_until =
      let doc =
        "Number of iterations during which to adapt the Ram proposal."
      in
      Coalfit.Term.int_opt ~doc ~arg:"adapt-until" ~docv:"ADAPT-UNTIL"

    let data =
      let doc =
          "Path prefix to input dataset as CSV files. "
        ^ "Case data is loaded from '$(docv).data.cases.csv', "
        ^ "and should have columns 't' and 'cases'. "
        ^ "Sequence data is loaded from '$(docv).data.sequences.fasta'."
      in
      let fname = C.Arg.(
          required
        & opt (some string) None
        & info ["data"] ~docv:"DATA" ~doc
      )
      in
      C.Term.(const Data.load $ fname)

    let tree =
      let doc =
        "Optional Newick file for the starting tree."
      in
      let fname = C.Arg.(
          value
        & opt (some string) None
        & info ["tree"] ~docv:"TREE" ~doc
      )
      in
      C.Term.(const (Option.map T.read) $ fname)

    let run =
      let flat seed theta_every traj_every tree_every chol_every 
          adapt_until path
          read_hypar lik_settings infer data read_th_epi th_seq tree niter =
        let capt_hy = Some (Printf.sprintf "%s.par.csv" path) in
        let capt_th_epi = Some (Printf.sprintf "%s.theta0.epi.csv" path) in
        let hypar = read_hypar capt_hy in
        let th_epi = read_th_epi capt_th_epi in
        begin match hypar#t0 with
        | None ->
            failwith "t0 must be given explicitly"
        | Some _ ->
            ()
        end
        ;
        Estimate.posterior
          ?seed
          ?theta_every ?traj_every ?tree_every ?chol_every
          ?adapt_until ~path
          hypar lik_settings infer data th_epi th_seq tree niter
      in
      C.Term.(const flat
        $ Coalfit.Term.seed
        $ Coalfit.Term.theta_every
        $ Coalfit.Term.traj_every
        $ Constant_jc69.Term.tree_every
        $ Coalfit.Term.chol_every
        $ adapt_until
        $ Coalfit.Term.path
        $ Hyper.term
        $ Likelihood.Settings.term
        $ Infer.term
        $ data
        $ Theta.Epi.term
        $ Theta.Seq.term
        $ tree
        $ Coalfit.Term.n_iter
      )

    let info =
      let doc =
        "Estimate SIR model parameters and phylogeny
         from case count data and timed DNA sequences,
         with JC69 substitutions."
      in C.Term.info
        "phylo-sir-jc69"
        ~version:"%%VERSION%%"
        ~doc
        ~exits:C.Term.default_exits
        ~man:[]
  end

open Abbrevs

module E = Epifit


let name = "SIR"


module Hyper =
  struct
    class t = object
      inherit E.Sir.Hyper.t

      inherit Hyper.tree_t0_able
    end

    let default = new t

    let t0 hy = hy#t0

    let tree_t0 hy =
      (* tree_t0 must be supplied separately,
         because it's "dangerous" to use it *)
      hy#tree_t0

    let uterm () =
      let f tree_t0 read load_theta ~config hy =
        hy
        |> read load_theta ~config
        |> tree_t0 ~config
      in
      Cmdliner.Term.(
        const f
        $ Hyper.Term.tree_t0 ()
        $ E.Sir.Hyper.uterm ()
      )
       
    let capture_term =
      let f read load_theta ~config =
        (new t)
        |> U.Arg.Capture.empty
        |> read load_theta ~config
      in Cmdliner.Term.(
          const f
        $ uterm ()
      )

    let term =
      let f read load_theta =
        read load_theta
      in
      Cmdliner.Term.(
        const f
        $ capture_term
        $ Generic.Term.load_theta_from
      )
  end


module Data =
  struct
    module Settings =
      struct
        type t = E.Likelihood.settings
       
        let term = E.Term.Mcmc.likelihood
      end
      
    type t = E.Data.t

    let load = E.Cli.load_data
  end


module Theta =
  struct
    type t = Epi.Param.t_unit

    module Infer =
      struct
        type t = E.Infer.base_pars
        
        type tag = E.Tag.base_pars

        let tags = E.Symbols.base_pars

        let tag_to_string = E.Sir.Fit.tag_to_string

        let default = new E.Infer.base_pars

        let fix tag infer =
          E.Infer.fix_base_pars tag infer

        let infer tag infer =
          E.Infer.infer_base_pars `Adapt tag infer
      end

    let lambda =
      Epi.Param.beta

    let columns infer =
      let tags =
        E.Infer.gather_base_pars Fit.Infer.is_free infer []
      in
      L.fold_left (fun cs tag ->
        cs @ (E.Sir.Fit.tag_to_columns tag)
      ) [] tags

    let extract th col =
      match E.Sir.Fit.extract th col with
      | s ->
          Some s
      | exception Failure _ ->
          None

    let prior hy =
      let pars = E.Sir.Fit.params Infer.default hy in
      Fit.Param.List.prior (new Epi.Param.t_unit) pars

    let draws infer hy =
      let pars = E.Sir.Fit.params infer hy in
      Fit.Param.List.adapt_draws pars

    let jumps infer hy =
      let pars = E.Sir.Fit.params infer hy in
      Fit.Param.List.adapt_jumps pars

    let more_draws infer _ =
        (E.Draw.if_adapt E.Draw.beta infer#beta)
      @ (E.Draw.if_adapt E.Draw.nu infer#nu)
      @ (E.Draw.if_adapt E.Draw.rho infer#rho)

    let more_jumps infer hy =
      let d = more_draws infer hy in
      L.map (fun _ -> 0.01) d

    let more_draws_ratio infer _ =
        (E.Draw.if_adapt (E.Draw.Ratio.beta ()) infer#beta)
      @ (E.Draw.if_adapt (E.Draw.Ratio.nu ()) infer#nu)
      @ (E.Draw.if_adapt (E.Draw.Ratio.rho ()) infer#rho)

    let term =
      let f base ~config =
        (new Epi.Param.t_unit)
        |> U.Arg.Capture.empty
        |> base ~config
      in
      Cmdliner.Term.(
        const f
        $ Epi.Term.Par.base ()
      )
  end


module Initial =
  struct
    (* keep it immutable *)
    type t = Epifit.Sig.sir

    module Infer =
      struct
        type t = E.Infer.state

        type tag = E.Tag.state

        let tags = E.Symbols.state

        let tag_to_string = E.Symbols.Write.any

        let default = new E.Infer.state

        let fix tag infer =
          E.Infer.fix_state tag infer

        let infer tag infer =
          E.Infer.infer_state `Adapt tag infer
      end

    let columns infer =
      let tags =
        E.Infer.gather_state Fit.Infer.is_free infer []
      in
      L.fold_left (fun cs tag ->
        cs @ (E.Symbols.Write.columns tag)
      ) [] tags

    let extract x col =
      match E.Sir.Fit.extract_state x col with
      | s ->
          Some s
      | exception Failure _ ->
          None

    let prior hy =
      E.Prior.sir hy

    let draws infer hy =
      let comb = E.Fitsim.sir Fun.id Fun.id hy infer#sir in
      Fit.Param.List.(adapt_draws [comb])

    let jumps infer hy =
      let comb = E.Fitsim.sir Fun.id Fun.id hy infer#sir in
      Fit.Param.List.(adapt_jumps [comb])

    let more_draws infer hy =
      E.Draw.sir infer#sir hy

    let more_jumps infer hy =
      let d = more_draws infer hy in
      L.map (fun _ -> 0.01) d

    let more_draws_ratio infer hy =
      E.Draw.Ratio.sir infer#sir hy

    let term =
      let latency = Epi.Sir.Model.spec.latency in
      let to_state th =
        E.Sir.Fit.to_state th
      in
      let f sir ~config =
        new E.Episim.theta
        |> U.Arg.Capture.empty
        |> sir ~config
        |> U.Arg.Update.Capture.ignore to_state
      in
      Cmdliner.Term.(
        const f
        $ E.Term.Theta.init_sir ~latency
      )
  end


module Traj =
  struct
    type t = Epi_traj.t

    let output ?every path =
      Epi_traj.output ?every path
   
    let simulate hy th x0 =
      Jump.Cadlag_map.of_list (E.Sir.S.simulate
        ~out:`Cadlag
        E.Sir.Fit.state_settings
        (Ode Simple)
        hy
        th
        x0
        ()
      )

    let log_likelihood =
      Epi_traj.log_likelihood

    module Tree =
      struct
        (* FIXME copied from Sir_jc69 *)
        (* SIR is easy because there is only one lineage color, I.
         * So the likelihood is only that of the coalescences happening
         * when they do.
         * The formula given in Volz 2012 is slightly suspicious,
         * but I still use it here, to stay consensual
         * and avoid losing more time *)

        let rate n (pt : Epi.Traj.point) =
          let fn = F.Pos.of_float (float n) in
          let fnm1 = F.Pos.of_float (float (n - 1)) in
          (* check that n <= i *)
          if F.Op.(fn > pt.i) then begin
            failwith "n > i"
          end ;
          F.Pos.Op.(fn * fnm1 * pt.coal)

        (* n the number of bundles at t *)
        let merge_rate traj t n =
          rate n (Jump.Cadlag_map.eval traj t )

        (* n is the (constant) number of bundles between t and t' *)
        let merge_pressure traj t t' n =
          assert (t <= t') ;
          Jump.Cadlag_map.integrate ~from_t:t ~to_t:t' (rate n) traj

        let maybe_cut_left t0 c =
          match Jump.cut_left t0 c with
          | c ->
              c
          | exception Invalid_argument _ ->
              c

        let log_likelihood hy tree traj =
          let dlogd t n t' n' =
            (* integrate the merge event rate between t and t' *)
            let sum_lbd =
              merge_pressure traj (F.of_float t') (F.of_float t) n
            in
            if n' > n then
              (* sampling event - leaves can have equal dates *)
              F.Op.(~- sum_lbd)
            else if n' = n - 1 then
              (* merge event *)
              (* merge rate at merge point *)
              let lbd = merge_rate traj (F.of_float t') n in
              F.Op.(F.log lbd - sum_lbd)
            else begin
              failwith "Unexpected event"
            end
          in
          (* Start at the latest between t0 and tree_t0 *)
          let t0 =
            match Hyper.t0 hy, Hyper.tree_t0 hy with
            | Some t0, None ->
                t0
            | Some t0, Some tt0 ->
                max t0 tt0
            | None, (Some _ | None) ->
                (* Is that right ? *)
                invalid_arg "t0 is required"
          in
          let rev_nlineages =
            tree
            (* get the coalescent event intervals *)
            |> T.nlineages_backwards
            (* only take starting from t0 *)
            |> maybe_cut_left t0
            (* look backwards, from the tip to the root *)
            |> Jump.reverse
          in
          let logd = Jump.fold_succ_left (fun logd t n t' n' ->
            let dlogd =
              match dlogd t n t' n' with
              | dlogd ->
                  dlogd
              | exception Failure _ ->
                  F.of_float (~-. 1000.)
            in
            F.Op.(logd + dlogd)
          ) F.zero rev_nlineages
          in
          [F.to_float logd]
      end

    let tree_log_likelihood hy tree traj =
      Tree.log_likelihood hy tree traj
  end

open Abbrevs


type t = {
  nsamples : int ;  (* number of samples *)
  samples : (int * float * unit) list ;  (* sorted *)
  sequences : Seqs.t M.Int.t ;
  seq_probas : (Lac.mat * S.Int.t) M.Int.t ;
  invariants : int Seqsim.Tree.Ntm.t ;
  nsites : int ; (* total number of sites *)
  nvsites : int ; (* number of variant sites *)
}

module Settings =
  struct
    type t = {
      use_seqs : bool ;
      use_tree : bool ;
    }

    module C = Cmdliner

    let use_seqs =
      let doc =
        "Flag to ignore the sequence data"
      in
      let ignore_seq =
        C.Arg.(
          value
          & flag
          & info ["ignore-sequences"] ~docv:"IGNORE-SEQUENCES" ~doc
        )
      in
      C.Term.(const not $ ignore_seq)

    let use_tree =
      let doc =
        "Flag to ignore the tree likelihood."
      in
      let ignore_tree =
        C.Arg.(
          value
          & flag
          & info ["ignore-tree"] ~docv:"IGNORE-TREE" ~doc
        )
      in
      C.Term.(const not $ ignore_tree)

    let term =
      let f use_seqs use_tree =
        { use_seqs ; use_tree }
      in
      C.Term.(const f $ use_seqs $ use_tree)
  end


let read txseqs =
  (* This is a bit fragile, but this needs to be the same
     comparison function as in Phylo.T.relabel *)
  let comp ((t : float), (lab : string)) (t', lab') =
    let c = compare t t' in
    if c = 0 then
      compare lab lab'
    else
      c
  in
  let nsamples = L.length txseqs in
  let sorted = L.sort (fun (tx, _) (tx', _) -> comp tx' tx) txseqs in
  let samples = L.mapi (fun k ((t, _), _) -> (k + 1, t, ())) sorted in
  let sequences = L.fold_lefti (fun seqs i (_, seq) ->
    (* start from 1 *)
    let k = i + 1 in
    M.Int.add k seq seqs
  ) M.Int.empty sorted
  in
  (* nsites counted on full sequences *)
  let nsites = Fel.nsites sequences in
  let invariants, sequences =
    Seqsim.Base.split_invariant sequences
  in
  let nsites = I.to_int nsites in
  let nvsites = I.to_int (Fel.nsites sequences) in
  let seq_probas = Seqsim.Probas.sequences sequences in
  {
    nsamples ;
    samples ;
    sequences ;
    seq_probas ;
    invariants ;
    nsites ;
    nvsites
  }


let load ?fmt path_root =
  let chan = open_in (path_root ^ ".data.sequences.fasta") in
  let scan =
    match fmt with
    | None ->
        (fun s -> Scanf.sscanf s "%s@:%f" (fun s t -> (t, s)))
    | Some fmt ->
        (fun s -> Scanf.sscanf s fmt (fun s t -> (t, s)))
  in
  let read_stamp s =
    match scan s with
    | tx ->
        Some tx
    | exception Scanf.Scan_failure _ ->
        None
  in
  let txseqs =
    match Seqs.Io.assoc_of_any_fasta read_stamp chan with
    | None ->
        failwith
        "FASTA stamps do not respect the format ('%s@:%f' or supplied)"
    | Some txseqs ->
        txseqs
  in
  close_in chan ;
  let txseqs = L.map (fun ((t, x), seq) ->
      (* check that time is positive *)
      ((F.to_float (F.Pos.of_float t), x), seq)
    ) txseqs
  in
  read txseqs


let nsamples { nsamples ; _ } =
  nsamples


let sorted { samples ; _ } =
  L.map (fun (k, t, x) -> (k, F.Pos.of_float t, x)) samples


let sequences { sequences ; _ } =
  sequences


let sequence_probas { seq_probas ; _ } =
  seq_probas


let invariants { invariants ; _ } =
  invariants


let nsites { nsites ; _ } =
  nsites


let variant_sites { nvsites ; _ } =
  nvsites


let ninterior data =
  I.Pos.of_int (nsamples data - 1)

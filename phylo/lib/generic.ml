open Abbrevs


type 'a term =
  'a Cmdliner.Term.t

type 'a config_term =
  'a U.Arg.configurable Cmdliner.Term.t

type 'a capture_term =
  'a U.Arg.Capture.t U.Arg.configurable Cmdliner.Term.t


module type ECO =
  sig
    val name : string

    module Hyper :
      sig
        type t
        val default : t
        val tree_t0 : t -> float option
        val term : t capture_term
      end

    module Data :
      sig
        module Settings :
          sig
            type t

            val term : t term
          end

        type t

        val load : string -> t
      end

    module Theta :
      sig
        type t

        module Infer : Infer_term.INPUT

        val lambda : t -> _ U.pos

        val columns : Infer.t -> string list

        val extract : t -> string -> string option

        val prior : Hyper.t -> (t, float list) Fit.Dist.t

        val draws :
          Infer.t ->
          Hyper.t ->
            (U.rng -> float -> t -> t) list

        val jumps :
          Infer.t ->
          Hyper.t ->
            float list

        val more_draws :
          Infer.t ->
          Hyper.t ->
            (U.rng -> float -> t -> t) list

        val more_jumps :
          Infer.t ->
          Hyper.t ->
            float list

        val more_draws_ratio :
          Infer.t ->
          Hyper.t ->
            (t -> t -> float) list

        val term : t capture_term
      end

    (* Is keeping this separated from Theta too complicated ?
     * What is the use ?
     * Because it naturally comes like that ? *)
    module Initial :
      sig
        type t
        
        module Infer : Infer_term.INPUT

        val columns : Infer.t -> string list

        val extract : t -> string -> string option

        val prior : Hyper.t -> (t, float list) Fit.Dist.t

        val draws :
          Infer.t ->
          Hyper.t ->
            (U.rng -> float -> t -> t) list

        val jumps :
          Infer.t ->
          Hyper.t ->
            float list

        val more_draws :
          Infer.t ->
          Hyper.t ->
            (U.rng -> float -> t -> t) list

        val more_jumps :
          Infer.t ->
          Hyper.t ->
            float list

        val more_draws_ratio :
          Infer.t ->
          Hyper.t ->
            (t -> t -> float) list

        val term : t capture_term
      end

    module Traj :
      sig
        type t

        val output :
          ?every:int ->
          string ->
          (int, t, int * t) Util.Out.t

        val simulate :
          Hyper.t ->
          Theta.t ->
          Initial.t ->
            t

        val log_likelihood :
          Hyper.t ->
          Data.Settings.t ->
          Data.t ->
          t ->
            float list

        val tree_log_likelihood :
          Hyper.t ->
          T.t ->
          t ->
            float list
      end
  end


module type EVO =
  sig
    val name : string

    module Hyper :
      sig
        type t

        val default : t

        val ncat : t -> int

        val term : t capture_term
      end

    module Theta :
      sig
        type t

        module Infer : Infer_term.INPUT

        val columns : Infer.t -> string list

        val extract : t -> string -> string option

        val mu : t -> _ U.pos

        val prior : Hyper.t -> (t, float list) Fit.Dist.t
        
        val draws :
          Infer.t ->
            (U.rng -> float -> t -> t) list

        val jumps :
          Infer.t ->
          Hyper.t ->
            float list

        val term : t capture_term
      end

      type vec
      type mat

      val settings : (vec, mat) Seqsim.Probas.settings

      (* I will need more for + Gamma + I *)
      val equilibrium_probas :
        Theta.t ->
          Lac.Vec.t

      val flow :
        ?mat:mat ->
        Theta.t ->
          (vec, mat) Seqsim.Probas.transition
  end


(* put terms used in Make here, so that they can be used
 * from ECO and EVO implementations *)
module Term =
  struct
    module C = Cmdliner

    let tree_every =
      let doc =
        "Output phylogenetic tree every $(docv) iterations.
         By default, do not output."
      in
      U.Arg.opt C.Arg.int ~doc "tree-every"

    let adapt_until =
      let doc =
        "Number of iterations during which to adapt the RAM proposals."
      in
      U.Arg.opt C.Arg.int ~doc "adapt-until"

    let load_theta_from =
      let doc =
        "Load theta parameter values from $(docv)."
      in
      U.Arg.opt C.Arg.string ~doc "load-theta"

    let parse_fasta_headers () =
      let doc =
        "Read the timestamp from FASTA headers with format string $(docv)."
      in
      let arg = U.Arg.opt C.Arg.string ~doc "parse-fasta-headers" in
      let f s =
        Scanf.format_from_string s "%s@:%f"
      in
      C.Term.(const (Option.map f) $ arg)

    let parse_newick_labels () =
      let doc =
        "Read the times from Newick labels with format string $(docv)."
      in
      let arg = U.Arg.opt C.Arg.string ~doc "parse-newick-labels" in
      let f s =
        Scanf.format_from_string s "%s@:%f"
      in
      C.Term.(const (Option.map f) $ arg)
  end


module Make (Eco : ECO) (Evo : EVO) =
  struct
    module Hyper =
      struct
        module Tree =
          struct
            class t = object
              (* move this stuff to somewhere else *)
              inherit Hyper.scalable
              inherit Hyper.slide_root_able
              inherit Hyper.spr_div_scalable
              inherit Hyper.spr_guided_scalable
            end

            let spr_div_scale hy =
              F.Pos.of_float hy#spr_div_scale

            let spr_guided_scale hy =
              F.Pos.of_float hy#spr_guided_scale

            let term =
              let f sc_j sr_j sd sg ~config =
                U.Arg.Capture.empty (new t)
                |> sc_j ~config
                |> sr_j ~config
                |> sd ~config
                |> sg ~config
              in
              Cmdliner.Term.(
                const f
                $ Hyper.Term.scale_jump ()
                $ Hyper.Term.slide_root_jump ()
                $ Hyper.Term.spr_div_scale ()
                $ Hyper.Term.spr_guided_scale ()
              )
          end

        module Lik =
          struct
            class t = object
              inherit Hyper.seq_loglik_weight_able
            end

            let term =
              let f slw ~config =
                new t
                |> U.Arg.Capture.empty
                |> slw ~config
              in
              Cmdliner.Term.(
                const f
                $ Hyper.Term.seq_loglik_weight ()
              )
          end

        type t = {
          eco : Eco.Hyper.t ;
          evo : Evo.Hyper.t ;
          tree : Tree.t ;
          prop : Proposal_weights.t ;
          lik : Lik.t ;
        }

        let default = {
          eco = Eco.Hyper.default ;
          evo = Evo.Hyper.default ;
          tree = new Tree.t ;
          prop = Proposal_weights.default ;
          lik = new Lik.t ;
        }

        (* FIXME add sequences_weight *)

        let load_from = Epifit.Term.Hyper.load_from

        module UA = U.Arg
        module UAC = U.Arg.Capture
        module UAUC = U.Arg.Update.Capture

        let term =
          let f eco evo tree prop lik from capture =
            let g eco evo tree prop lik =
              { eco ; evo ; tree ; prop ; lik }
            in
            UAC.(
              empty g
              $ eco ~config:from
              $ evo ~config:from
              $ tree ~config:from
              $ empty prop
              $ lik ~config:from
              |> output capture
            )
          in Cmdliner.Term.(
            const f
            $ Eco.Hyper.term
            $ Evo.Hyper.term
            $ Tree.term
            $ Proposal_weights.term
            $ Lik.term
            $ load_from
          )
      end

    module Data =
      struct
        module Evo = Seq_data

        module Eco = Eco.Data

        type t = {
          eco : Eco.t ;
          evo : Evo.t ;
        }

        module Settings =
          struct
            type t = {
              eco : Eco.Settings.t ;
              evo : Evo.Settings.t ;
            }

            let term =
              let f eco evo =
                { eco ; evo }
              in
              Cmdliner.Term.(
                const f
                $ Eco.Settings.term
                $ Evo.Settings.term
              )
          end

        let load ?fmt path_root =
          let eco = Eco.load path_root in
          let evo = Evo.load ?fmt path_root in
          { eco ; evo }
      end

    module Tree = T

    module Probas =
      struct
        let init data_seq hy_evo =
          Probas.init
            Evo.settings
            data_seq
            (Evo.Hyper.ncat hy_evo)

        let zero_transition hy_evo =
          Probas.zero_transition
            Evo.settings
            (Evo.Hyper.ncat hy_evo)

        let zero_proba data_seq hy_evo =
          Probas.zero_proba
            Evo.settings
            data_seq
            (Evo.Hyper.ncat hy_evo)

        let compute data_seq hy_evo =
          let mat = zero_transition hy_evo in
          let pmem_4n = zero_proba data_seq hy_evo in
          let f th_evo tree probas =
            (* closure *)
            match Evo.flow ~mat th_evo with
            | trans ->
                Probas.compute
                  Evo.settings
                  ~pmem_4n
                  data_seq
                  trans
                  tree
                  probas
            | exception (Invalid_argument _ as e) ->
                Fit.raise_draw_error e
          in f

        let recompute use_seq data_seq hy_evo =
          if use_seq then
            compute data_seq hy_evo
          else
            (fun _ _ probas -> probas)

        let update use_seq data_seq hy_evo =
          if use_seq then
            let mat = zero_transition hy_evo in
            let pmem_4n = zero_proba data_seq hy_evo in
            let f th_evo tree changed_nodes probas =
              (* closure *)
              match Evo.flow ~mat th_evo with
              | trans ->
                  Probas.update
                    Evo.settings
                    ~pmem_4n
                    data_seq
                    trans
                    tree
                    changed_nodes
                    probas
              | exception (Invalid_argument _ as e) ->
                  Fit.raise_draw_error e
            in f
          else
            (fun _ _ _ probas -> probas)

        let log_likelihood use_seq cut_tree hy_evo data_seq =
          let stgs = Evo.settings in
          let ncat = Evo.Hyper.ncat hy_evo in
          let mat = Probas.zero_transition stgs ncat in
          let sstore_4 = Probas.zero_vectors stgs data_seq ncat in
          let pmem_4n = Probas.zero_proba stgs data_seq ncat in
          let smem_4 = Probas.zero_vector stgs ncat in
          let pmem_1n = Probas.zero_proba_1n stgs data_seq ncat in
          let mmem_1n = Probas.zero_mat_1n data_seq in
          let loglik = Probas.log_likelihood
            Evo.settings
            ~sstore_4
            ~pmem_4n
            ~smem_4
            ~pmem_1n
            ~mmem_1n
            ?cut:cut_tree
            data_seq
          in
          let comp th_seq probas tree =
            let eq = Evo.equilibrium_probas th_seq in
            (* closure *)
            match Evo.flow ~mat th_seq with
            | trans ->
                loglik
                  eq
                  trans
                  probas
                  tree
            | exception (Invalid_argument _ as e) ->
                Fit.raise_draw_error e
          in
          if use_seq then
            comp
          else
            (fun _ _ _ -> [])
      end

    module Theta =
      struct
        module Probas =
          struct
            type t = (Evo.mat * S.Int.t) M.Int.t

            include Probas
          end

        class ['tree] poly
          ((eco_par, evo_par, eco_initial, eco_traj, tree, probas) :
           (Eco.Theta.t *
            Evo.Theta.t *
            Eco.Initial.t *
            Eco.Traj.t *
            'tree *
            Probas.t)) =
        object
          val eco_par_v = eco_par
          val evo_par_v = evo_par
          val eco_initial_v = eco_initial
          val eco_traj_v = eco_traj
          val tree_v = tree
          val probas_v = probas

          method eco_par = eco_par_v
          method evo_par = evo_par_v
          method eco_initial = eco_initial_v
          method eco_traj = eco_traj_v
          method tree = tree_v
          method probas = probas_v

          method with_eco_par eco = {< eco_par_v = eco >}
          method with_evo_par evo = {< evo_par_v = evo >}
          method with_eco_initial z0 = {< eco_initial_v = z0 >}
          method with_eco_traj traj = {< eco_traj_v = traj >}
          (* no with_tree because it can't be at a different type *)
          method with_probas probas = {< probas_v = probas >}
        end

        (* mentally a <
            eco_par : Epi.Theta.t ;
            evo_par : Evo.Theta.t ;
            eco_initial : Eco.Initial.t ;
            eco_traj : Eco.Traj.t ;
            tree : T.t ;
            probas : Probas.t ;
          >
         *)
        type t = T.t poly

        type param = <
          eco_par : Eco.Theta.t ;
          evo_par : Evo.Theta.t ;
          eco_initial : Eco.Initial.t ;
        >

        let make eco_par evo_par eco_initial eco_traj tree probas =
          new poly (eco_par, evo_par, eco_initial, eco_traj, tree, probas)

        let with_eco_traj traj th =
          th#with_eco_traj traj

        let with_tree tree th =
          make
            th#eco_par
            th#evo_par
            th#eco_initial
            th#eco_traj
            tree
            th#probas

        let with_probas probas th =
          th#with_probas probas

        let refresh_traj (hy : Hyper.t) (th : t) =
          let traj = Eco.Traj.simulate hy.eco th#eco_par th#eco_initial in
          with_eco_traj traj th

        let recompute_probas use_seq (data : Data.t) (hy : Hyper.t) =
          let recompute = Probas.recompute
            use_seq
            data.evo
            hy.evo
          in
          let f th =
            let probas = recompute
              th#evo_par
              th#tree
              th#probas
            in
            with_probas probas th
          in f

        let update_probas use_seq (data : Data.t) (hy : Hyper.t) =
          let update = Probas.update
            use_seq
            data.evo
            hy.evo
          in
          let f changed th =
            let probas = update
              th#evo_par
              th#tree
              changed
              th#probas
            in
            with_probas probas th
          in f
      end

    module Prior =
      struct
        let dist (hy : Hyper.t) =
          Fit.Dist.joint6
            ~get:(fun (th : Theta.t) ->
              (th#eco_par,
               th#evo_par,
               th#eco_initial,
               th#eco_traj,
               th#tree,
               th#probas)
            )
            ~combine:Theta.make
            (Eco.Theta.prior hy.eco)
            (Evo.Theta.prior hy.evo)
            (Eco.Initial.prior hy.eco)
            Fit.Dist.Flat
            Fit.Dist.Flat
            Fit.Dist.Flat
      end

    module Likelihood =
      struct
        let dist
          (hy : Hyper.t)
          (stgs : Data.Settings.t)
          (data : Data.t) =
          let draw _ =
            failwith "Not_implemented"
          in
          let evo_log_likelihood =
            Probas.log_likelihood
              stgs.evo.use_seqs
              (Eco.Hyper.tree_t0 hy.eco)
              hy.evo
              data.evo
          in
          let eco_log_likelihood =
            Eco.Traj.log_likelihood
              hy.eco
              stgs.eco
              data.eco
          in
          let tree_log_likelihood =
            if stgs.evo.use_tree then
              Eco.Traj.tree_log_likelihood
                hy.eco
            else
              (fun _ _ -> [])
          in
          let log_dens_proba (th : Theta.t) =
            let eco = eco_log_likelihood th#eco_traj in
            let tree = tree_log_likelihood th#tree th#eco_traj in
            let evo = evo_log_likelihood
              th#evo_par
              th#probas
              th#tree
            in
            let weighted =
              L.map (fun x -> x *. hy.lik#seq_loglik_weight)
            in
            (eco, tree, weighted evo)
          in
          (* TODO change this for richer output *)
          let sum (eco, tree, evo) =
               Fit.Dist.fsum eco
            +. Fit.Dist.fsum tree
            +. Fit.Dist.fsum evo
          in
          let exp (eco, tree, evo) =
            (L.map exp eco,
             L.map exp tree,
             L.map exp evo)
          in
          let output _ _ =
            ()
          in
          Fit.Dist.Base_poly {
            draw ;
            log_dens_proba ;
            sum ;
            exp ;
            output ;
          }

        let columns = [
          "loglik-eco" ;
          "loglik-tree" ;
          "loglik-evo" ;
        ]

        let extract (eco, tree, evo) =
          let sof = string_of_float in
          function
          | "loglik-eco" ->
              Some (sof (Fit.Dist.fsum eco))
          | "loglik-tree" ->
              Some (sof (Fit.Dist.fsum tree))
          | "loglik-evo" ->
              Some (sof (Fit.Dist.fsum evo))
          | _ ->
              None

      end

    module Propose =
      struct
        module Tree =
          struct
            module P = Treefit.Propose.Make (T)
            module P' = Treefit.Propose_guided.Make (T)

            let scale dx tree =
              let alpha = F.exp (F.of_float dx) in
              match P.scale alpha tree with
              | tree' ->
                  tree'
              | exception (Invalid_argument _ as e) ->
                  (* scaling hit a leaf *)
                  Fit.raise_draw_error e

            let slide_root dx tree =
              match P.slide_node 0 dx tree with
              | _, tree' ->
                  tree'
              | exception (Invalid_argument _ as e) ->
                  Fit.raise_draw_error e

            let slide_node data_seq rng dx tree =
              (* choose a random node but the root 0 *)
              let nint = Data.Evo.ninterior data_seq in
              let nintm1 = I.Pos.of_anyint (I.pred nint) in
              let k = 1 + I.to_int (U.rand_int ~rng nintm1) in
              match P.slide_node k dx tree with
              | x, tree' ->
                  let j = T.State.to_label x in
                  j, tree'
              | exception (Invalid_argument _ as e) ->
                  Fit.raise_draw_error e

            let div_var data_seq hy_tree th_seq =
              let mu = Evo.Theta.mu th_seq in
              let m = F.of_float (float (Data.Evo.nsites data_seq)) in
              let sgm = Hyper.Tree.spr_div_scale hy_tree in
              let d = F.mul sgm (F.invert (F.mul mu m)) in
              F.sq d

            let guided_sigma data_seq hy_tree th_seq =
             let mu = Evo.Theta.mu th_seq in
             let m = F.Pos.of_float (float (Data.Evo.nsites data_seq)) in
             let sgm = Hyper.Tree.spr_guided_scale hy_tree in
             F.Pos.Op.(sgm * mu * m)

            let spr_div_changed (ctxt : P.Spr.Divergence.context) =
              [ ctxt.x_prune ; ctxt.x_prune_parent ]
              |> L.map T.State.to_label
              |> S.Int.of_list

            let spr_unif_changed (ctxt : P.Spr.Uniform.context) =
              [ ctxt.x_prune ; ctxt.x_prune_parent ]
              |> L.map T.State.to_label
              |> S.Int.of_list

            let spr_guided_changed (ctxt : P'.Spr.context) =
              [ ctxt.x_prune ; ctxt.x_prune_parent ]
              |> L.map T.State.to_label
              |> S.Int.of_list

            (* Can I have th_seq ? *)
            let spr_div data_seq hy_tree th_seq =
              let nint = Data.Evo.ninterior data_seq in
              let v = div_var data_seq hy_tree th_seq in
              P.Spr.Divergence.move nint v

            let spr_unif data_seq =
              let nint = Data.Evo.ninterior data_seq in
              P.Spr.Uniform.move nint

            let spr_guided use_seq data_seq hy_evo hy_tree =
              let upd = Probas.update
                use_seq
                data_seq
                hy_evo
              in
              let update th_seq tree context probas =
                let nodes = spr_guided_changed context in
                upd th_seq tree nodes probas
              in
              let f th_seq =
                let sgm = guided_sigma data_seq hy_tree th_seq in
                P'.Spr.move Evo.settings (update th_seq) sgm
              in f

            let dummy_spr_div_context =
              let x_dummy = (0., 0) in
              P.Spr.Divergence.{
                x_prune = x_dummy ;
                x_prune_parent = x_dummy ;
                x_graft_parent = x_dummy ;
                t_graft = 0. ;
                distance = 0. ;
                moves = [] ; 
              }

            let dummy_spr_unif_context =
              let x_dummy = (0., 0) in
              P.Spr.Uniform.{
                x_prune = x_dummy ;
                x_prune_parent = x_dummy ;
                x_graft_parent = x_dummy ;
                length = 0. ;
              }

            let dummy_spr_guided_context =
              let x_dummy = (0., 0) in
              P'.Spr.{
                x_prune = x_dummy ;
                x_prune_sub = x_dummy ;
                x_prune_parent = x_dummy ;
                x_prune_sibling = x_dummy ;
                x_graft_parent = x_dummy ;
                x_graft_sibling = x_dummy ;
              }
          end

        let prop_evo_tree update pair =
          let get th =
            (th#evo_par, th#tree),
            (th#eco_par, (th#eco_initial, (th#eco_traj, th#probas)))
          in
          let combine_sample (evo, tree) (eco, (z0, (traj, probas))) =
            Theta.make eco evo z0 traj tree probas
          in
          let combine_move (evo, move) (eco, (z0, (traj, probas))) =
            let probas = update evo move probas in
            Theta.make eco evo z0 traj move probas
          in
          Fit.Propose.Both {
            first = pair ;
            second = Fit.Propose.constant_flat_4 () ;
            get_sample = get ;
            get_move = get ;
            combine_sample ;
            combine_move ;
          }

        (* used for guided, so no need to update probas *)
        let prop_evo_tree_probas trio =
          let get_sample th =
            (th#evo_par, (th#tree, th#probas)),
            (th#eco_par, (th#eco_initial, th#eco_traj))
          in
          let combine_sample (evo, (tree, probas)) (eco, (z0, traj)) =
            Theta.make eco evo z0 traj tree probas
          in
          let get_move (th, context) =
            (th#evo_par, (th#tree, th#probas, context)),
            (th#eco_par, (th#eco_initial, th#eco_traj))
          in
          let combine_move (evo, (tree, probas, context)) (eco, (z0, traj)) =
            (Theta.make eco evo z0 traj tree probas, context)
          in
          Fit.Propose.Both {
            first = trio ;
            second = Fit.Propose.constant_flat_3 () ;
            get_sample ;
            get_move ;
            combine_sample ;
            combine_move ;
          }

        let spr_div
          (data : Data.t)
          (lik : Data.Settings.t)
          (hy : Hyper.t) =
          let upd = Probas.update
            lik.evo.use_seqs
            data.evo
            hy.evo
          in
          let update evo (tree, ctxt) probas =
            let nodes = Tree.spr_div_changed ctxt in
            upd evo tree nodes probas
          in
          prop_evo_tree
            update
            (Fit.Propose.pair_dep
              Fit.Propose.Constant_flat
              (Tree.spr_div data.evo hy.tree)
            )

        let spr_unif (data : Data.t) (lik : Data.Settings.t) (hy : Hyper.t) =
          let upd = Probas.update
            lik.evo.use_seqs
            data.evo
            hy.evo
          in
          let update evo (tree, ctxt) probas =
            let nodes = Tree.spr_unif_changed ctxt in
            upd evo tree nodes probas
          in
          prop_evo_tree
            update
            (Fit.Propose.pair
              Fit.Propose.Constant_flat
              (Tree.spr_unif data.evo)
            )

        let spr_guided
          (data : Data.t)
          (lik : Data.Settings.t)
          (hy : Hyper.t) =
          prop_evo_tree_probas
            (Fit.Propose.pair_dep
              Fit.Propose.Constant_flat  (* th#seq *)
              (Tree.spr_guided lik.evo.use_seqs data.evo hy.evo hy.tree)
            )
      end

    module Infer =
      struct
        module Eco_par = Eco.Theta.Infer
        module Eco_initial = Eco.Initial.Infer
        module Evo_par = Evo.Theta.Infer

        module Tree =
          struct
            type t = Fit.Infer.t

            type tag = [`Tree]

            let tags = [`Tree]

            let tag_to_string `Tree = "tree"

            let default = Fit.Infer.adapt

            let fix `Tree _ =
              Fit.Infer.fixed

            let infer `Tree _ =
              Fit.Infer.adapt
          end

        type t = {
          eco_par : Eco_par.t ;
          eco_initial : Eco_initial.t ;
          evo_par : Evo_par.t ;
          tree : Tree.t ;
        }

        module Term_input =
          struct
            type nonrec t = t

            type tag =
              | Eco_par of Eco_par.tag
              | Eco_initial of Eco_initial.tag
              | Evo_par of Evo_par.tag
              | Tree of Tree.tag

            let tags =
                (L.map (fun tag -> Eco_par tag) Eco_par.tags)
              @ (L.map (fun tag -> Eco_initial tag) Eco_initial.tags)
              @ (L.map (fun tag -> Evo_par tag) Evo_par.tags)
              @ (L.map (fun tag -> Tree tag) Tree.tags)

            (* Here they need to not overlap *)
            let tag_to_string =
              function
              | Eco_par tag ->
                  Eco_par.tag_to_string tag
              | Eco_initial tag ->
                  Eco_initial.tag_to_string tag
              | Evo_par tag ->
                  Evo_par.tag_to_string tag
              | Tree tag ->
                  Tree.tag_to_string tag

            let default = {
              eco_par = Eco_par.default ;
              eco_initial = Eco_initial.default ;
              evo_par = Evo_par.default ;
              tree = Tree.default ;
            }

            let fix =
              function
              | Eco_par tag ->
                  (fun inf ->
                     let eco_par = Eco_par.fix tag inf.eco_par in
                     { inf with eco_par }
                  )
              | Eco_initial tag ->
                  (fun inf ->
                     let eco_initial =
                       Eco_initial.fix tag inf.eco_initial
                     in
                     { inf with eco_initial }
                  )
              | Evo_par tag ->
                  (fun inf ->
                     let evo_par = Evo_par.fix tag inf.evo_par in
                     { inf with evo_par }
                  )
              | Tree tag ->
                  (fun inf ->
                     let tree = Tree.fix tag inf.tree in
                     { inf with tree }
                  )

            let infer =
              function
              | Eco_par tag ->
                  (fun inf ->
                     let eco_par = Eco_par.infer tag inf.eco_par in
                     { inf with eco_par }
                  )
              | Eco_initial tag ->
                  (fun inf ->
                     let eco_initial =
                       Eco_initial.infer tag inf.eco_initial
                     in
                     { inf with eco_initial }
                  )
              | Evo_par tag ->
                  (fun inf ->
                     let evo_par = Evo_par.infer tag inf.evo_par in
                     { inf with evo_par }
                  )
              | Tree tag ->
                  (fun inf ->
                     let tree = Tree.infer tag inf.tree in
                     { inf with tree }
                  )
          end

        module T =  Infer_term.Make (Term_input)

        let term = T.term
      end

    module Output =
      struct
        module Tree =
          struct
            let sample = T.output_sample

            let move = T.output_move
          end

        module Param =
          struct
            let columns (infer : Infer.t) =
              let cs =
                  (Eco.Theta.columns infer.eco_par)
                @ (Eco.Initial.columns infer.eco_initial)
                @ (Evo.Theta.columns infer.evo_par)
              in
                Fit.Csv.columns_sample_move
              @ Likelihood.columns
              @ cs
              @ (L.map (fun s -> "move_" ^ s) cs)

            let extract likelihood k ret =
              let extract_theta th s =
                match Eco.Theta.extract th#eco_par s with
                | Some v ->
                    v
                | None ->
                    begin match Eco.Initial.extract th#eco_initial s with
                    | Some v ->
                        v
                    | None ->
                        begin match Evo.Theta.extract th#evo_par s with
                        | Some v ->
                            v
                        | None ->
                            failwith "column not recognized"
                        end
                    end
              in
              Some (Fit.Csv.extract_sample_move_poly
                ~move_to_sample:(fun x -> x)
                ~extract:extract_theta
                ~likelihood
                ~extract_loglik:Likelihood.extract
                k
                ret
              )

            let return ?theta_every ?chol_every likelihood infer chol path =
              let columns = columns infer in
              let extract = extract likelihood in
              Simfit.Out.convert_csv_ram
                ?n_thin:theta_every
                ?chol_every
                ~columns
                ~extract
                ~chol
                path
          end

        let param_return ret =
          Fit.Mh.Return.map
            (fun th -> (th :> Theta.param))
            (fun th -> (th :> Theta.param))
            ret

        let param_left_return ret =
          Fit.Mh.Return.map
            (fun th -> (th :> Theta.param))
            (fun (th, _) -> (th :> Theta.param))
            ret

        let traj_sample ret =
          let th = Fit.Mh.Return.sample ret in
          th#eco_traj

        let tree_sample ret =
          let th = Fit.Mh.Return.sample ret in
          th#tree

        let tree_div_move ret =
           match Fit.Mh.Return.move ret with
           | th ->
               let tree, _ = th#tree in
               Some tree
           | exception Invalid_argument _ ->
               None

        let tree_unif_move ret =
          match Fit.Mh.Return.move ret with
          | th ->
              let tree, _ = th#tree in
              Some tree
          | exception Invalid_argument _ ->
              None

        let tree_guided_move ret =
          match Fit.Mh.Return.move ret with
          | th, _ ->
              Some th#tree
          | exception Invalid_argument _ ->
              None

        let ram par_out traj_out tree_out =
          U.Out.combine
            (fun ret _ -> ret)
            (U.Out.combine
               (fun ret _ -> ret)
               (U.Out.map param_return par_out)
               (U.Out.map traj_sample traj_out)
            )
            (U.Out.map tree_sample tree_out)

        let spr_div par_out tree_out tree_mv_out =
          U.Out.combine
            (fun ret _ -> ret)
            (U.Out.map param_return par_out)
            (U.Out.combine
              (fun ret _ -> ret)
              (U.Out.map tree_sample tree_out)
              (U.Out.map tree_div_move tree_mv_out)
            )

        let spr_unif par_out tree_out tree_mv_out =
          U.Out.combine
            (fun ret _ -> ret)
            (U.Out.map param_return par_out)
            (U.Out.combine
              (fun ret _ -> ret)
              (U.Out.map tree_sample tree_out)
              (U.Out.map tree_unif_move tree_mv_out)
            )

        let spr_guided par_out tree_out tree_mv_out =
          U.Out.combine
            (fun ret _ -> ret)
            (U.Out.map param_left_return par_out)
            (U.Out.combine
              (fun ret _ -> ret)
              (U.Out.map tree_sample tree_out)
              (U.Out.map tree_guided_move tree_mv_out)
            )
      end

    module Estimate =
      struct
        let to_chol js =
          js
          |> A.of_list
          |> Lac.Vec.of_array
          |> Lac.Mat.of_diag

        let tree_scale_draws (infer : Infer.t) =
          if Fit.Infer.is_free_adapt infer.tree then
            [
              (fun _ dx th ->
                let move = Propose.Tree.scale dx th#tree in
                Theta.with_tree move th
              ) ;
            ]
          else
            []

        (* lognormal proposal on length to tip *)
        let tree_scale_ratio (infer : Infer.t) =
          if Fit.Infer.is_free_adapt infer.tree then
            let first_time tree =
              T.case
                ~binode:(fun _ _ _ -> invalid_arg "unrooted tree")
                ~leaf:(fun _ -> invalid_arg "unrooted tree")
                ~node:(fun _ stree ->
                  T.State.to_time (T.root_value stree)
                )
                tree
            in
            Some (fun th th' ->
              (* assume tree' comes from a scale *)
              let t_first = first_time th#tree in
              let t_first' = first_time th'#tree in
              let t_tip = T.State.to_time (T.tip_value th#tree) in
              let len = t_tip -. t_first in
              let len' = t_tip -. t_first' in
              [log len' -. log len]
            )
          else
            None

        let tree_scale_jumps (infer : Infer.t) (hy : Hyper.t) =
          if Fit.Infer.is_free_adapt infer.tree then
            [hy.tree#scale_jump]
          else
            []

        let tree_slide_root_draws
          (stgs : Data.Settings.t)
          (infer : Infer.t)
          data
          hy =
          if not (Fit.Infer.is_fixed infer.tree) then
            let theta_update_probas = Theta.update_probas
              stgs.evo.use_seqs
              data
              hy
            in
            [
              (fun _ dx th ->
                let move = Propose.Tree.slide_root dx th#tree in
                th
                |> Theta.with_tree move
                |> theta_update_probas S.Int.empty
              ) ;
            ]
          else
            []

        let tree_slide_root_jumps (infer : Infer.t) (hy : Hyper.t) =
          if not (Fit.Infer.is_fixed infer.tree) then
            [hy.tree#slide_root_jump]
          else
            []

        let tree_slide_node_draws
          (stgs : Data.Settings.t)
          (infer : Infer.t)
          (data : Data.t)
          hy =
          if not (Fit.Infer.is_fixed infer.tree) then
            let slide = Propose.Tree.slide_node data.evo in
            let theta_update_probas = Theta.update_probas
              stgs.evo.use_seqs
              data
              hy
            in
            [
              (fun rng dx th ->
                let k, tree = slide rng dx th#tree in
                let changed = S.Int.singleton k in
                th
                |> Theta.with_tree tree
                |> theta_update_probas changed
              ) ;
            ]
          else
            []

        let tree_slide_node_jumps (infer : Infer.t) (hy : Hyper.t) =
          (* same as tree_slide_root_jumps should be reasonable *)
          tree_slide_root_jumps infer hy

        let draw_eco_par f rng dx (th : Theta.t) =
          let eco = f rng dx th#eco_par in
          th#with_eco_par eco

        let draw_eco_initial f rng dx (th : Theta.t) =
          let z0 = f rng dx th#eco_initial in
          th#with_eco_initial z0

        let eco_par_draws (infer : Infer.t) (hy : Hyper.t) =
          L.map
            draw_eco_par
            (Eco.Theta.draws infer.eco_par hy.eco)

        let eco_initial_draws (infer : Infer.t) (hy : Hyper.t) =
          L.map
            draw_eco_initial
            (Eco.Initial.draws infer.eco_initial hy.eco)

        let eco_draws infer hy =
            (eco_par_draws infer hy)
          @ (eco_initial_draws infer hy)

        let eco_jumps (infer : Infer.t) (hy : Hyper.t) =
            (Eco.Theta.jumps infer.eco_par hy.eco)
          @ (Eco.Initial.jumps infer.eco_initial hy.eco)

        let eco_more_par_draws (infer : Infer.t) (hy : Hyper.t) =
            L.map
              draw_eco_par
              (Eco.Theta.more_draws infer.eco_par hy.eco)

        let eco_more_initial_draws (infer : Infer.t) (hy : Hyper.t) =
            L.map
              draw_eco_initial
              (Eco.Initial.more_draws infer.eco_initial hy.eco)

        let eco_more_draws infer hy =
            (eco_more_par_draws infer hy)
          @ (eco_more_initial_draws infer hy)

        let eco_more_jumps (infer : Infer.t) (hy : Hyper.t) =
            (Eco.Theta.more_jumps infer.eco_par hy.eco)
          @ (Eco.Initial.more_jumps infer.eco_initial hy.eco)

        let eco_more_par_draws_ratio (infer : Infer.t) (hy : Hyper.t) =
          L.map
            (fun r th th' -> r th#eco_par th'#eco_par)
            (Eco.Theta.more_draws_ratio infer.eco_par hy.eco)

        let eco_more_initial_draws_ratio (infer : Infer.t) (hy : Hyper.t) =
          L.map
            (fun r th th' -> r th#eco_initial th'#eco_initial)
            (Eco.Initial.more_draws_ratio infer.eco_initial hy.eco)
         
        let eco_more_draws_ratio infer hy =
          let fold rs =
            match rs with
            | [] ->
                None
            | rs ->
                Some (fun th th' -> L.fold_right (fun r logrs ->
                  r th th' :: logrs
                ) rs [])
          in
          fold (
               eco_more_par_draws_ratio infer hy
             @ eco_more_initial_draws_ratio infer hy
          )

        let evo_draws (infer : Infer.t) =
          L.map (fun f rng dx (th : Theta.t) ->
            let evo = f rng dx th#evo_par in
            th#with_evo_par evo
          ) (Evo.Theta.draws infer.evo_par)

        let evo_jumps (infer : Infer.t) (hy : Hyper.t) =
          Evo.Theta.jumps infer.evo_par hy.evo

        let vector_draw ~before ~after draws =
          let vd =
            Fit.Propose.vector_draw (A.of_list draws)
          in
          (fun rng v (th : Theta.t) ->
             th
             |> before
             |> vd rng v
             |> after
          )

        let as_sprd_move th =
          let ctxt = Propose.Tree.dummy_spr_div_context in
          Theta.with_tree (th#tree, ctxt) th

        let as_spru_move th =
          let ctxt = Propose.Tree.dummy_spr_unif_context in
          Theta.with_tree (th#tree, ctxt) th

        let as_sprg_move th =
          let ctxt = Propose.Tree.dummy_spr_guided_context in
          (th, ctxt)

        let proposal
          ?theta_every ?traj_every ?tree_every ?chol_every ?adapt_until 
          ~path lik_stgs infer data (hy : Hyper.t) =
          let tsc_draws = tree_scale_draws infer in
          let tsc_chol = to_chol (tree_scale_jumps infer hy) in
          let tsc_ratio = tree_scale_ratio infer in
          let tsr_draws =
            tree_slide_root_draws lik_stgs infer data hy
          in
          let tsr_chol = to_chol (tree_slide_root_jumps infer hy) in
          let tsn_draws =
            tree_slide_node_draws lik_stgs infer data hy
          in
          let tsn_chol = to_chol (tree_slide_node_jumps infer hy) in
          let eco_draws = eco_draws infer hy in
          let eco_chol = to_chol (eco_jumps infer hy) in
          let eco_more_draws = eco_more_draws infer hy in
          let eco_more_chol = to_chol (eco_more_jumps infer hy) in
          let eco_more_ratio = eco_more_draws_ratio infer hy in
          let evo_draws = evo_draws infer in
          let evo_chol = to_chol (evo_jumps infer hy) in
          let prior = Prior.dist hy in
          let likelihood = Likelihood.dist
            hy
            lik_stgs
            data
          in
          let par_out = Output.Param.return
            ?theta_every
            ?chol_every
            likelihood
            infer
            eco_chol  (* FIXME *)
            path
          in
          let traj_out = Eco.Traj.output
            ?every:traj_every
            path
          in
          let tree_out, tree_close = Output.Tree.sample
            ?every:tree_every
            path
          in
          let tree_mv_out, tree_mv_close = Output.Tree.move
            ?every:tree_every
            path
          in
          let ram_output = Output.ram
            par_out
            traj_out
            tree_out
          in
          let sprd_output = Output.spr_div par_out tree_out tree_mv_out in
          let spru_output = Output.spr_unif par_out tree_out tree_mv_out in
          let sprg_output = Output.spr_guided par_out tree_out tree_mv_out in
          let start k th =
            let smpl = Fit.Mh.Sample.weight prior likelihood th in
            let par_res = Output.param_return
              (Fit.Mh.Return.accepted (fun x -> x) smpl)
            in
            par_out.start k par_res ;
            tree_out.start k th#tree ;
            smpl
          in
          let return k smplf =
            let ram_res = Fit.Mh.Return.accepted (fun x -> x) smplf in
            let sprd_res = Fit.Mh.Return.accepted as_sprd_move smplf in
            let spru_res = Fit.Mh.Return.accepted as_spru_move smplf in
            let sprg_res = Fit.Mh.Return.accepted as_sprg_move smplf in
            let _ = ram_output.return k ram_res in
            let _ = sprd_output.return k sprd_res in
            let _ = spru_output.return k spru_res in
            let resf = sprg_output.return k sprg_res in
            tree_close () ; tree_mv_close () ;
            resf
          in
          let wtot =
            if Fit.Infer.is_free infer.tree then
              F.Op.(
                  hy.prop.th_ram
                + hy.prop.th_ram
                + hy.prop.th_more_ram
                + hy.prop.tsc_ram
                + hy.prop.tsr_ram
                + hy.prop.tsn_ram
                + hy.prop.sprd_mh
                + hy.prop.spru_mh
                + hy.prop.sprg_mh
              )
            else
             F.Op.(
                hy.prop.th_ram
              + hy.prop.th_ram
            )
          in
          let eco_ram_prop = Fit.Ram.Loop.proposal
            ~output:ram_output
            ~alpha:(F.to_float F.Op.(hy.prop.th_ram / wtot))
            ?kf:adapt_until
            (fun x -> x)
            (fun x -> x)
            (L.length eco_draws)
            eco_chol
            prior
            (vector_draw
              ~before:Fun.id
              ~after:(Theta.refresh_traj hy)
              eco_draws
            )
            likelihood
          in
          let eco_more_ram_prop = Fit.Ram.Loop.proposal
              ~output:ram_output
              ~alpha:(F.to_float F.Op.(hy.prop.th_more_ram / wtot))
              ?kf:adapt_until
              ?draws_ratio:eco_more_ratio
              (fun x -> x)
              (fun x -> x)
              (L.length eco_more_draws)
              eco_more_chol
              prior
              (vector_draw
                ~before:Fun.id
                ~after:(Theta.refresh_traj hy)
                eco_more_draws
              )
              likelihood
          in
          let evo_ram_prop = Fit.Ram.Loop.proposal
            ~output:ram_output
            ~alpha:(F.to_float F.Op.(hy.prop.th_ram / wtot))
            ?kf:adapt_until
            (fun x -> x)
            (fun x -> x)
            (L.length evo_draws)
            evo_chol
            prior
            (vector_draw
              ~before:Fun.id
               (* fully recompute probas *)
              ~after:(Theta.recompute_probas
                lik_stgs.evo.use_seqs
                data
                hy
              )
              evo_draws
            )
            likelihood
          in
          let tsc_ram_prop = Fit.Ram.Loop.proposal
            ~output:ram_output
            ~alpha:(F.to_float F.Op.(hy.prop.tsc_ram / wtot))
            ?draws_ratio:tsc_ratio
            ?kf:adapt_until
            (fun x -> x)
            (fun x -> x)
            (L.length tsc_draws)
            tsc_chol
            prior
            (vector_draw
              ~before:Fun.id
               (* fully recompute probas *)
              ~after:(Theta.recompute_probas
                lik_stgs.evo.use_seqs
                data
                hy
              )
              tsc_draws
            )
            likelihood
          in
          let tsr_ram_prop = Fit.Ram.Loop.proposal
            ~output:ram_output
            ~adapt:false
            ~alpha:(F.to_float F.Op.(hy.prop.tsr_ram / wtot))
            ?kf:adapt_until
            (fun x -> x)
            (fun x -> x)
            (L.length tsr_draws)
            tsr_chol
            prior
            (vector_draw
              ~before:Fun.id
               (* partial probas update already done in draws *)
              ~after:Fun.id
              tsr_draws
            )
            likelihood
          in
          let tsn_ram_prop = Fit.Ram.Loop.proposal
            ~output:ram_output
            ~alpha:(F.to_float F.Op.(hy.prop.tsn_ram / wtot))
            ?kf:adapt_until
            (fun x -> x)
            (fun x -> x)
            (L.length tsn_draws)
            tsn_chol
            prior
            (vector_draw
              ~before:Fun.id
               (* partial probas update already done *)
              ~after:Fun.id
              tsn_draws
            )
            likelihood
          in
          let sprd_mh_prop = Fit.Mh.Loop.proposal
            ~output:sprd_output
            as_sprd_move
            prior
            (Propose.spr_div data lik_stgs hy)
            likelihood
          in
          let spru_mh_prop = Fit.Mh.Loop.proposal
            ~output:spru_output
            as_spru_move
            prior
            (Propose.spr_unif data lik_stgs hy)
            likelihood
          in
          let sprg_mh_prop = Fit.Mh.Loop.proposal
            ~output:sprg_output
            as_sprg_move
            prior
            (Propose.spr_guided data lik_stgs hy)
            likelihood
          in
          (* We don't want to use the proposals if they contain
             no draws. This way of doing it is ugly, though... *)
          let choices =
              (if L.length eco_draws > 0 then
                 [(eco_ram_prop, hy.prop.th_ram)]
               else
                 []
              )
            @ (if L.length eco_more_draws > 0 then
                 [(eco_more_ram_prop, hy.prop.th_more_ram)]
               else
                 []
              )
            @ (if L.length evo_draws > 0 then
                 [(evo_ram_prop, hy.prop.th_ram)]
               else
                 []
               )
            @ (if not (Fit.Infer.is_fixed infer.tree) then [
                 (tsc_ram_prop, hy.prop.tsc_ram) ;
                 (tsr_ram_prop, hy.prop.tsr_ram) ;
                 (tsn_ram_prop, hy.prop.tsn_ram) ;
                 (sprd_mh_prop, hy.prop.sprd_mh) ;
                 (spru_mh_prop, hy.prop.spru_mh) ;
                 (sprg_mh_prop, hy.prop.sprg_mh) ;
               ]
               else
                 []
            )
          in
          let proposal = Fit.Propose.Choice choices in
          proposal, start, return

        let posterior
          ?seed ?theta_every ?traj_every ?tree_every ?chol_every ?adapt_until
          ~path hypar lik_stgs infer data eco0 evo0 z0 tree0 niter =
          let proposal, start, return = proposal
            ?theta_every
            ?traj_every
            ?tree_every
            ?chol_every
            ?adapt_until
            ~path
            lik_stgs
            infer
            data
            hypar
          in
          (* make the starting state *)
          let tree0 =
            match tree0 with
            | None ->
                (* just start from a dumb constant tree *)
                (* right order of magnitude of coalescence rate *)
                let lbd = Eco.Theta.lambda eco0 in
                Tree.draw data.evo (U.rng seed) lbd
            | Some tree ->
                if Tree.compatible data.evo tree then
                  tree
                else
                  failwith "Supplied tree and sequences are incompatible"
          in
          (* simulate traj *)
          let traj0 = Eco.Traj.simulate hypar.eco eco0 z0 in
          (* init probas *)
          let probas0 = Probas.recompute
            lik_stgs.evo.use_seqs
            data.evo
            hypar.evo
            evo0
            tree0
            (Probas.init data.evo hypar.evo)
          in
          (* make starting theta *)
          let th0 = Theta.make
            eco0
            evo0
            z0
            traj0
            tree0
            probas0
          in
          let smpl0 = start 0 th0 in
          let smplf =
            Fit.Markov_chain.direct_simulate ?seed proposal smpl0 niter
          in
          return niter smplf
      end

    module Term =
      struct
        module C = Cmdliner

        let data_path =
          let doc =
              "Path prefix to input dataset as CSV files. "
            ^ "Case data is loaded from '$(docv).data.cases.csv', "
            ^ "and should have columns 't' and 'cases'. "
            ^ "Sequence data is loaded from '$(docv).data.sequences.fasta'."
          in
          C.Arg.(
              required
            & opt (some string) None
            & info ["data"] ~docv:"DATA" ~doc
          )

        let data =
          let f fmt data_path =
            Data.load ?fmt data_path
          in
          C.Term.(
            const f
            $ Term.parse_fasta_headers ()
            $ data_path
          )

        let tree_line =
          let doc =
            "Optional line number to read starting tree from."
          in
          C.Arg.(
              value
            & opt (some int) None
            & info ["tree-line"] ~docv:"TREE-LINE" ~doc
          )

        let tree_path =
          let doc =
            "Path to optional Newick file for the starting tree."
          in
          C.Arg.(
              value
            & opt (some string) None
            & info ["tree"] ~docv:"TREE" ~doc
          )

        let tree =
          let f fmt line tree_path =
            Option.map (Tree.read ?fmt ?line) tree_path
          in
          C.Term.(
            const f
            $ Term.parse_newick_labels ()
            $ tree_line
            $ tree_path
          )

        let run =
          let capture_theta eco evo z tree =
            (eco, evo, z, tree)
          in
          let flat
              seed
              theta_every traj_every tree_every chol_every 
              adapt_until path
              hypar lik_stgs infer data
              eco evo z tree niter load_theta =
            (* output niter *)
            U.Csv.output
              ~columns:["niter"]
              ~extract:(fun niter ->
                function
                | "niter" ->
                    string_of_int niter
                | _ ->
                    invalid_arg "unrecognized column"
              )
              ~path:(Printf.sprintf "%s.niter.csv" path)
              [niter]
            ;
            let capt_hy_path =
              Some (Printf.sprintf "%s.par.csv" path)
            in
            let capt_theta_path =
              Some (Printf.sprintf "%s.theta0.csv" path)
            in
            let hypar = hypar capt_hy_path in
            let eco, evo, z, tree =
              U.Arg.Capture.(
                empty capture_theta
                $ eco ~config:load_theta
                $ evo ~config:load_theta
                $ z ~config:load_theta
                $ empty tree
                |> output capt_theta_path
              )
            in
            U.time_it_to
              (Printf.sprintf "%s.duration.csv" path)
              (Estimate.posterior
                ?seed
                ?theta_every ?traj_every ?tree_every ?chol_every
                ?adapt_until ~path
                hypar lik_stgs infer data eco evo z tree)
              niter
          in
          C.Term.(const flat
            $ Coalfit.Term.seed
            $ Coalfit.Term.theta_every
            $ Coalfit.Term.traj_every
            $ Term.tree_every
            $ Coalfit.Term.chol_every
            $ Term.adapt_until
            $ Coalfit.Term.path
            $ Hyper.term
            $ Data.Settings.term
            $ Infer.term
            $ data
            $ Eco.Theta.term
            $ Evo.Theta.term
            $ Eco.Initial.term
            $ tree
            $ Coalfit.Term.n_iter
            $ Term.load_theta_from
          )

        let info =
          let doc = Printf.sprintf (
              "Estimate %s model parameters and phylogeny " ^^
              "from data and timed DNA sequences, " ^^
              "under the %s evolution model."
            )
            (String.uppercase_ascii Eco.name)
            (String.uppercase_ascii Evo.name)
          in
          let name = Printf.sprintf "phylo-%s-%s"
            (String.lowercase_ascii Eco.name)
            (String.lowercase_ascii Evo.name)
          in
          C.Term.info
            name
            ~version:"%%VERSION%%"
            ~doc
            ~exits:C.Term.default_exits
            ~man:[]
          end
  end

open Abbrevs

module S = Seqsim

module Q = S.With_invariant.Make (
  S.With_gamma.Make (
    S.Gtr
  )
)


let name = "GTR+Gamma+Inv"


module Hyper =
  struct
    class t = object
      inherit Jc69.Hyper.mu_able
      inherit Gtr.Hyper.rel_rates_able
      inherit Gtr.Hyper.pis_able
      inherit With_gamma.Hyper.t
      inherit With_inv.Hyper.t
    end

    let default = new t

    module H = With_gamma.Hyper

    let gamma_shape_prior =
      H.gamma_shape_prior

    let gamma_shape_mean =
      H.gamma_shape_mean

    let gamma_shape_max =
      H.gamma_shape_max

    let gamma_n_cat =
      H.gamma_n_cat

    let ncat hy =
      (* + inv *)
      1 + gamma_n_cat hy


    let open_term () =
      let f gtr_stuff gamma_stuff pi_j load_theta ~config hy =
        hy
        |> gtr_stuff ~config
        |> gamma_stuff load_theta ~config
        |> pi_j ~config
      in
      Cmdliner.Term.(
        const f
        $ Gtr.Hyper.open_term ()
        $ With_gamma.Hyper.open_term ()
        $ With_inv.Hyper.Term.p_inv_jump ()
        $ Generic.Term.load_theta_from
      )

    type 'a term =
      'a
      U.Arg.Capture.t
      U.Arg.configurable
      Cmdliner.Term.t

    let term : t term =
      let f update ~config =
        new t
        |> U.Arg.Capture.empty
        |> update ~config
      in
      Cmdliner.Term.(
        const f
        $ open_term ()
      )
  end


module Theta =
  struct
    type t = Q.t

    module Infer =
      struct
        module I = Fit.Infer

        class t = object
          inherit Gtr.Theta.Infer.t
          inherit With_gamma.Theta.Infer.t
          inherit With_inv.Theta.Infer.t
        end

        type tag = [
          | Gtr.Theta.Infer.tag
          | With_gamma.Theta.Infer.tag
          | With_inv.Theta.Infer.tag
        ]

        let tags =
            Gtr.Theta.Infer.tags
          @ With_gamma.Theta.Infer.tags
          @ With_inv.Theta.Infer.tags

        let tag_to_string =
          function
          | #Gtr.Theta.Infer.tag as tag ->
              Gtr.Theta.Infer.tag_to_string tag
          | #With_gamma.Theta.Infer.tag as tag ->
              With_gamma.Theta.Infer.tag_to_string tag
          | #With_inv.Theta.Infer.tag as tag ->
              With_inv.Theta.Infer.tag_to_string tag

        let tag_to_columns = 
          function
          | #Gtr.Theta.Infer.tag as tag ->
              Gtr.Theta.Infer.tag_to_columns tag
          | #With_gamma.Theta.Infer.tag as tag ->
              With_gamma.Theta.Infer.tag_to_columns tag
          | #With_inv.Theta.Infer.tag as tag ->
              With_inv.Theta.Infer.tag_to_columns tag

        let default =
          new t

        let get =
          function
          | #Gtr.Theta.Infer.tag as tag ->
              Gtr.Theta.Infer.get tag
          | #With_gamma.Theta.Infer.tag as tag ->
              With_gamma.Theta.Infer.get tag
          | #With_inv.Theta.Infer.tag as tag ->
              With_inv.Theta.Infer.get tag

        let fix =
          function
          | #Gtr.Theta.Infer.tag as tag ->
              Gtr.Theta.Infer.fix tag
          | #With_gamma.Theta.Infer.tag as tag ->
              With_gamma.Theta.Infer.fix tag
          | #With_inv.Theta.Infer.tag as tag ->
              With_inv.Theta.Infer.fix tag

        let infer =
          function
          | #Gtr.Theta.Infer.tag as tag ->
              Gtr.Theta.Infer.infer tag
          | #With_gamma.Theta.Infer.tag as tag ->
              With_gamma.Theta.Infer.infer tag
          | #With_inv.Theta.Infer.tag as tag ->
              With_inv.Theta.Infer.infer tag
      end

    let columns (infr : Infer.t) =
      let f tag =
        if Fit.Infer.is_free (Infer.get tag infr) then
          Infer.tag_to_columns tag
        else
          []
      in
      L.fold_left (fun cs tag ->
        cs @ (f tag)
      ) [] Infer.tags

    let extract = Q.extract

    let mu = Q.mu

    module Prior =
      struct
        let gamma_shape = With_gamma.Theta.Prior.gamma_shape

        let p_inv = With_inv.Theta.Prior.p_inv
      end

    let prior hy =
      let gtr = Gtr.Theta.prior hy in
      let shape = Prior.gamma_shape hy in
      let p_inv = Prior.p_inv in
      (* only for Gamma *)
      let n_cat = Hyper.gamma_n_cat hy in
      let to_tuple (th : t) =
        th.sub.sub, (th.sub.shape, F.to_float th.p_inv)
      in
      let of_tuple (sub, (shape, p_inv)) : t =
        let sub : Q.sub = { sub ; shape ; n_cat } in
        let p_inv = F.Proba.of_float p_inv in
        { sub ; p_inv }
      in
      Fit.Dist.Map (
        to_tuple,
        of_tuple,
        Fit.Dist.trio gtr shape p_inv
      )

    module Draws =
      struct
        let mu rng dx (th : t) =
          let sub = { th.sub with sub = Gtr.Theta.Draws.mu rng dx th.sub.sub } in
          { th with sub }

        let rel_rates =
          L.map (fun f (rng : U.rng) dx (th : t) : t ->
            let sub = { th.sub with sub = f rng dx th.sub.sub } in
            { th with sub }
          ) Gtr.Theta.Draws.rel_rates

        let pis =
          L.map (fun f rng dx (th : t) : t ->
            let sub = { th.sub with sub = f rng dx th.sub.sub } in
            { th with sub }
          ) Gtr.Theta.Draws.pis

        let gamma_shape _ dx (th : t) : t =
          let sub = { th.sub with shape = th.sub.shape +. dx } in
          { th with sub }

        let p_inv _ dx (th : t) : t =
          let p_inv = F.to_float th.p_inv +. dx in
          match F.Proba.of_float p_inv with
          | p_inv ->
              { th with p_inv }
          | exception (Invalid_argument _ as e) ->
              Fit.raise_draw_error e

        let of_tag =
          function
          | `Mu ->
              [mu]
          | `Rel_rates ->
              rel_rates
          | `Pis ->
              pis
          | `Gamma_shape ->
              [gamma_shape]
          | `P_inv ->
              [p_inv]
      end

    let draws infer =
      let f tag =
        if Fit.Infer.is_free_adapt (Infer.get tag infer) then
          Draws.of_tag tag
        else
          []
      in
      L.fold_left (fun ds tag ->
        ds @ (f tag)
      ) [] Infer.tags

    module Jumps =
      struct
        let gamma_shape hy =
          [hy#gamma_shape_jump]

        let p_inv hy =
          [hy#p_inv_jump]

        let of_tag =
          function
          | `Gamma_shape ->
              gamma_shape
          | `P_inv ->
              p_inv
          | #Gtr.Theta.Infer.tag as tag ->
              Gtr.Theta.Jumps.of_tag tag
      end

    let jumps infer (hy : Hyper.t) =
      let f tag =
        if Fit.Infer.is_free_adapt (Infer.get tag infer) then
          Jumps.of_tag tag hy
        else
          []
      in
      L.fold_left (fun js tag ->
        js @ (f tag)
      ) [] Infer.tags

    let term =
      Q.capture_term
  end


type vec = Q.vec
type mat = Q.mat


let settings = Q.settings


let equilibrium_probas (th : Theta.t) =
  S.Gtr.equilibrium_probas th.sub.sub


let transition_probas ?mat th =
  Q.transition_probas ?mat th


let flow ?mat par =
  let tp = transition_probas ?mat par in
  let trans dt =
    tp (F.Pos.of_float dt)
  in
  S.Probas.gen_transition settings trans

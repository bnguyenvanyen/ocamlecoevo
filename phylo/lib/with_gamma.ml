open Abbrevs


module Hyper =
  struct
    class t = object
      val gamma_shape_prior_v : [`Uniform | `Exponential] = `Exponential
      val gamma_shape_mean_v = 1.
      val gamma_shape_max_v = 2.
      val gamma_shape_jump_v = 1e-3
      val gamma_n_cat_v = 4

      method gamma_shape_prior = gamma_shape_prior_v
      method gamma_shape_mean = gamma_shape_mean_v
      method gamma_shape_max = gamma_shape_max_v
      method gamma_shape_jump = gamma_shape_jump_v
      method gamma_n_cat = gamma_n_cat_v

      method with_gamma_shape_prior x = {< gamma_shape_prior_v = x >}
      method with_gamma_shape_mean x = {< gamma_shape_mean_v = x >}
      method with_gamma_shape_max x = {< gamma_shape_max_v = x >}
      method with_gamma_shape_jump x = {< gamma_shape_jump_v = x >}
      method with_gamma_n_cat x = {< gamma_n_cat_v = x >}
    end

    let gamma_shape_prior hy =
      hy#gamma_shape_prior

    let gamma_shape_mean hy =
      F.Pos.of_float hy#gamma_shape_mean

    let gamma_shape_max hy =
      F.to_float (F.Pos.of_float hy#gamma_shape_max)

    let gamma_n_cat hy =
      hy#gamma_n_cat

    module Term =
      struct
        let float_arg = Jc69.Hyper.Term.float_arg

        let prior_choice_arg ~doc ~get ~set name =
          let conv = Cmdliner.Arg.enum [
            ("uniform", `Uniform) ;
            ("exponential", `Exponential) ;
          ]
          in
          U.Arg.Update.Capture.opt_config
            get
            set
            conv
            ~doc
            name

        let gamma_shape_prior () =
          let doc =
            "Choose between exponential and uniform prior for gamma shape."
          in
          let get hy = hy#gamma_shape_prior in
          let set x hy = hy#with_gamma_shape_prior x in
          prior_choice_arg ~get ~set ~doc "gamma-shape-prior"

        let gamma_shape_mean () =
          let doc =
            "Mean of the gamma shape prior if exponential."
          in
          let get hy = hy#gamma_shape_mean in
          let set x hy = hy#with_gamma_shape_mean x in
          float_arg ~get ~set ~doc "gamma-shape-mean"

        let gamma_shape_max () =
          let doc =
            "Upper bounds of the gamma shape prior if uniform."
          in
          let get hy = hy#gamma_shape_max in
          let set x hy = hy#with_gamma_shape_max x in
          float_arg ~get ~set ~doc "gamma-shape-max"

        let gamma_shape_jump () =
          let doc =
            "Initial standard deviation of gamma shape jumps."
          in
          let get hy = hy#gamma_shape_jump in
          let set x hy = hy#with_gamma_shape_jump x in
          float_arg ~get ~set ~doc "gamma-shape-jump"

        let gamma_n_cat () =
          let set x hy = hy#with_gamma_n_cat x in
          let f n_cat ~config =
            U.Arg.Update.Capture.update set (n_cat ~config)
          in
          Cmdliner.Term.(
            const f
            $ Seqsim.With_gamma.Term.n_cat
          )
      end

    let open_term () =
      let f gs_p gs_m gs_x gs_j gnc load_theta ~config hy =
        hy
        |> gs_p ~config
        |> gs_m ~config
        |> gs_x ~config
        |> gs_j ~config
        |> gnc ~config:load_theta
      in
      Cmdliner.Term.(
        const f
        $ Term.gamma_shape_prior ()
        $ Term.gamma_shape_mean ()
        $ Term.gamma_shape_max ()
        $ Term.gamma_shape_jump ()
        $ Term.gamma_n_cat ()
      )
  end


module Theta =
  struct
    module Infer =
      struct
        module I = Fit.Infer

        class t = object
          val shape_v = I.adapt
          method shape = shape_v
          method with_shape x = {< shape_v = x >}
        end

        type tag = [ `Gamma_shape ]

        let tags = [ `Gamma_shape ]

        let tag_to_string =
          function
          | `Gamma_shape ->
              "gamma-shape"

        let tag_to_columns col =
          [tag_to_string col]

        let get =
          function
          | `Gamma_shape ->
              (fun infr -> infr#shape)

        let fix =
          function
          | `Gamma_shape ->
              (fun infr -> infr#with_shape I.fixed)

        let infer =
          function
          | `Gamma_shape ->
              (fun infr -> infr#with_shape I.adapt)
      end

    module Prior =
      struct
        module D = Fit.Dist

        let gamma_shape hy =
          match Hyper.gamma_shape_prior hy with
          | `Exponential ->
              let lbd = F.Pos.invert (Hyper.gamma_shape_mean hy) in
              D.Exponential lbd
          | `Uniform ->
              D.Uniform (0., Hyper.gamma_shape_max hy)
      end
  end

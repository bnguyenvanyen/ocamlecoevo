open Abbrevs

module E = Epifit


let name = "SIRS"


module Hyper =
  struct
    class t = object
      inherit E.Sirs.Hyper.t

      inherit Hyper.tree_t0_able
    end

    let default = new t

    let t0 hy = hy#t0

    let tree_t0 hy =
      (* tree_t0 must be supplied separately,
         because it's "dangerous" to use it *)
      hy#tree_t0

    let uterm () =
      let f tree_t0 read load_theta ~config hy =
        hy
        |> read load_theta ~config
        |> tree_t0 ~config
      in
      Cmdliner.Term.(
        const f
        $ Hyper.Term.tree_t0 ()
        $ E.Sirs.Hyper.uterm ()
      )
       
    let capture_term =
      let f read load_theta ~config =
        (new t)
        |> U.Arg.Capture.empty
        |> read load_theta ~config
      in Cmdliner.Term.(
          const f
        $ uterm ()
      )

    let term =
      let f read load_theta =
        read load_theta
      in
      Cmdliner.Term.(
        const f
        $ capture_term
        $ Generic.Term.load_theta_from
      )
  end


module Data = Sir.Data


module Theta =
  struct
    type t = Epi.Param.t_unit

    module Infer =
      struct
        class t = object
          inherit E.Infer.base_pars
          inherit E.Infer.circular
        end

        type tag = [ E.Tag.base_pars | E.Tag.circular ]

        let tags = E.Symbols.(base_pars @ circular)

        let tag_to_string = E.Sirs.Fit.tag_to_string

        let default = new t

        let fix tag infer =
          match tag with
          | #E.Tag.base_pars as tag ->
              E.Infer.fix_base_pars tag infer
          | #E.Tag.circular as tag ->
              E.Infer.fix_circular tag infer

        let infer tag infer =
          match tag with
          | #E.Tag.base_pars as tag ->
              E.Infer.infer_base_pars `Adapt tag infer
          | #E.Tag.circular as tag ->
              E.Infer.infer_circular `Adapt tag infer

        let gather is_infer infer tags =
             (E.Infer.gather_base_pars is_infer infer)
          @@ (E.Infer.gather_circular is_infer infer)
          @@ tags
      end

    let lambda =
      Epi.Param.beta

    let columns infer =
      let tags = Infer.gather Fit.Infer.is_free infer [] in
      L.fold_left (fun cs tag ->
        cs @ (E.Sirs.Fit.tag_to_columns tag)
      ) [] tags

    let extract th col =
      match E.Sirs.Fit.extract th col with
      | s ->
          Some s
      | exception Failure _ ->
          None

    let prior hy =
      let pars = E.Sirs.Fit.params Infer.default hy in
      Fit.Param.List.prior (new Epi.Param.t_unit) pars

    let draws infer hy =
      let pars = E.Sirs.Fit.params infer hy in
      Fit.Param.List.adapt_draws pars

    let jumps infer hy =
      let pars = E.Sirs.Fit.params infer hy in
      Fit.Param.List.adapt_jumps pars

    let more_draws infer _ =
        (E.Draw.if_adapt E.Draw.beta infer#beta)
      @ (E.Draw.if_adapt E.Draw.nu infer#nu)
      @ (E.Draw.if_adapt E.Draw.rho infer#rho)
      
    let more_jumps infer hy =
      let d = more_draws infer hy in
      L.map (fun _ -> 0.01) d

    let more_draws_ratio infer _ =
        (E.Draw.if_adapt (E.Draw.Ratio.beta ()) infer#beta)
      @ (E.Draw.if_adapt (E.Draw.Ratio.nu ()) infer#nu)
      @ (E.Draw.if_adapt (E.Draw.Ratio.rho ()) infer#rho)

    let term =
      let f base circular ~config =
        (new Epi.Param.t_unit)
        |> U.Arg.Capture.empty
        |> base ~config
        |> circular ~config
      in
      Cmdliner.Term.(
        const f
        $ Epi.Term.Par.base ()
        $ Epi.Term.Par.circular ()
      )
  end


module Initial = Sir.Initial


module Traj =
  struct
    type t = Epi_traj.t

    let output ?every path =
      Epi_traj.output ?every path

    let simulate hy th z0 =
      Jump.Cadlag_map.of_list (E.Sirs.S.simulate
        ~out:`Cadlag
        E.Sirs.Fit.state_settings
        (Ode Simple)
        hy
        th
        z0
        ()
      )

    let log_likelihood =
      Epi_traj.log_likelihood

    (* SIRS, like SIR, only has the color I.
     * Like SIR, except that the trajectory is different,
     * and the coal rate in pt.coal is different.
     *)
     let tree_log_likelihood hy tree traj =
       Sir.Traj.tree_log_likelihood hy tree traj
  end

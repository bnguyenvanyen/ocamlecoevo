module Hyper =
  struct
    class t = object
      val p_inv_jump_v = 1e-3

      method p_inv_jump = p_inv_jump_v

      method with_p_inv_jump x = {< p_inv_jump_v = x >}
    end

    module Term =
      struct
        let float_arg = Jc69.Hyper.Term.float_arg

        let p_inv_jump () =
          let doc =
            "Initial standard deviation of invariant probability jumps."
          in
          let get hy = hy#p_inv_jump in
          let set x hy = hy#with_p_inv_jump x in
          float_arg ~get ~set ~doc "p-inv-jump"
      end
  end


module Theta =
  struct
    module Infer =
      struct
        module I = Fit.Infer

        class t = object
          val p_inv_v = I.adapt
          method p_inv = p_inv_v
          method with_p_inv x = {< p_inv_v = x >}
        end

        type tag = [ `P_inv ]

        let tags = [ `P_inv ]

        let tag_to_string =
          function
          | `P_inv ->
              "p-inv"

        let tag_to_columns col =
          [tag_to_string col]

        let get =
          function
          | `P_inv ->
              (fun infr -> infr#p_inv)

        let fix =
          function
          | `P_inv ->
              (fun infr -> infr#with_p_inv I.fixed)

        let infer =
          function
          | `P_inv ->
              (fun infr -> infr#with_p_inv I.adapt)
      end

    module Prior =
      struct
        module D = Fit.Dist

        let p_inv =
          D.Uniform (0., 1.)
      end
  end

module U = Util


class proposal_weights_able = object
  val proposal_weights_v = Proposal_weights.default

  method proposal_weights = proposal_weights_v
  method with_proposal_weights x = {< proposal_weights_v = x >}
end


class scalable = object
  val scale_jump_v = 0.01
  method scale_jump = scale_jump_v
  method with_scale_jump x = {< scale_jump_v = x >}
end


class slide_root_able = object
  val slide_root_jump_v = 0.1
  method slide_root_jump = slide_root_jump_v
  method with_slide_root_jump x = {< slide_root_jump_v = x >}
end


class spr_div_scalable = object
  val spr_div_scale_v = 1.
  method spr_div_scale = spr_div_scale_v
  method with_spr_div_scale x = {< spr_div_scale_v = x >}
end


class spr_guided_scalable = object
  val spr_guided_scale_v = 1.
  method spr_guided_scale = spr_guided_scale_v
  method with_spr_guided_scale x = {< spr_guided_scale_v = x >}
end


class ignore_sequences_able = object
  val ignore_sequences_v = false
  method ignore_sequences = ignore_sequences_v
  method with_ignore_sequences x = {< ignore_sequences_v = x >}
end


class seq_loglik_weight_able = object
  val seq_loglik_weight_v = 1.
  method seq_loglik_weight = seq_loglik_weight_v
  method with_seq_loglik_weight x = {< seq_loglik_weight_v = x >}
end


class tree_t0_able =
  object
    val tree_t0_v : float option = None
    method tree_t0 = tree_t0_v
    method with_tree_t0 x = {< tree_t0_v = x >}
  end


module Term =
  struct
    let float_arg ~get ~set ~doc name =
      U.Arg.Update.Capture.opt_config
        get
        set
        Cmdliner.Arg.float
        ~doc
        name

    let float_opt_arg ~get ~set ~doc name =
      U.Arg.Update.Capture.opt_config
        get
        set
        Cmdliner.Arg.(some float)
        ~doc
        name

    let scale_jump () =
      let doc =
        "Initial standard deviation of tree scaling moves."
      in
      let get hy = hy#scale_jump in
      let set x hy = hy#with_scale_jump x in
      float_arg ~get ~set ~doc "scale-jump"

    let slide_root_jump () =
      let doc =
        "Initial standard deviation of tree node sliding moves."
      in
      let get hy = hy#slide_root_jump in
      let set x hy = hy#with_slide_root_jump x in
      float_arg ~get ~set ~doc "slide-root-jump"

    let spr_div_scale () =
      let doc =
        "Standard deviation of divSPR moves is '$(docv) * mu * nsites'."
      in
      let get hy = hy#spr_div_scale in
      let set x hy = hy#with_spr_div_scale x in
      float_arg ~get ~set ~doc "spr-div-scale"

    let spr_guided_scale () =
      let doc =
        "Standard deviation for grafting in guidedSPR by $(docv)."
      in
      let get hy = hy#spr_guided_scale in
      let set x hy = hy#with_spr_guided_scale x in
      float_arg ~get ~set ~doc "spr-guided-scale"
      
    (* FIXME not captured *)
    let proposal_weights () =
      Cmdliner.Term.(
        const (fun y hy -> hy#with_proposal_weights y)
        $ Proposal_weights.term
      )

    (* deprecated *)
    let ignore_sequences () =
      let doc =
        "Ignore the sequencs (flat likelihood)."
      in
      let get hy = hy#ignore_sequences in
      let set x hy = hy#with_ignore_sequences x in
      U.Arg.Update.Capture.flag_config get set ~doc "ignore-sequences"

    let seq_loglik_weight () =
      let doc =
        "Scale the sequence log-likelihood by $(docv)."
      in
      let get hy = hy#seq_loglik_weight in
      let set x hy = hy#with_seq_loglik_weight x in
      float_arg ~get ~set ~doc "seq-loglik-weight"

    let tree_t0 () =
      let doc =
        "Where to start the tree likelihoods from, "
      ^ "if different from t0."
      in
      let get hy = hy#tree_t0 in
      let set x hy = hy#with_tree_t0 x in
      float_opt_arg ~get ~set ~doc "tree-t0"
  end

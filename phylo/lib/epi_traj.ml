module L = BatList

module U = Util
module E = Epifit


type t = Epi.Traj.point Jump.Cadlag_map.t


let output ?every path =
  match every with
  | None ->
      Util.Out.convert_null
  | Some traj_every ->
  let columns = "k" :: Epi.Traj.columns in
  let chan = U.Csv.open_out (Printf.sprintf "%s.traj.csv" path) in
  Csv.output_record chan columns ;
  let output k traj =
    let ext tpt =
      function
      | "k" ->
          string_of_int k
      | s ->
          Epi.Traj.extract tpt s
    in
    U.Csv.write
      ~f:(U.Csv.Row.unfold ~columns ext)
      ~chan
      (Jump.Cadlag_map.to_list traj)
  in
  let start k0 traj0 =
    output k0 traj0
  in
  let out k traj =
    if k mod traj_every = 0 then
      output k traj
  in
  let return k traj =
    U.Csv.close_out chan ;
    (k, traj)
  in
  U.Out.{ start ; out ; return }


let cases_loglik =
  function
  | `None ->
      None
  | (`Prevalence | `Prevalence_negbin) ->
      failwith "Not_implemented"
  | (`Poisson | `Negbin) as obs ->
      Some (fun hy E.Data.{ cases ; _ } traj ->
        let rr =
          traj
          |> Jump.Cadlag_map.to_list
          |> Epi.Traj.reporting_rate
        in
        E.Observe.reported_cases
          ~obs
          hy
          (U.Option.some cases)
          rr
      )


let sero_loglik =
  function
  | `None ->
      None
  | `Stratified ->
      failwith "Not_implemented"
  | `Aggregate ->
      Some (fun _ E.Data.({ sero_agg ; _ }) traj ->
        let rp =
          traj
          |> Jump.Cadlag_map.to_list
          |> Epi.Traj.removed_proportion
        in
        E.Observe.aggregate_sero
          (U.Option.some sero_agg)
          rp
      )


let log_likelihood hy (stgs : E.Likelihood.settings) data =
  begin match stgs.alpha with
  | Some _ ->
      failwith "Not_implemented"
  | None ->
      ()
  end ;
  let loglikfs = L.filter_map (fun x -> x) [
    cases_loglik stgs.cases ;
    sero_loglik stgs.sero ;
  ]
  in
  E.Likelihood.traj_log_dens_proba loglikfs hy data

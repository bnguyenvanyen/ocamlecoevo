open Abbrevs

module SI = S.Int
module S = Seqsim
module D = Seq_data


let init
  (type u v) (stgs : (u, v) S.Probas.settings)
  data ncat : (v * SI.t) M.Int.t =
  let nsites = D.variant_sites data in
  let nsamples = D.nsamples data in
  match stgs with
  | Homogen ->
      S.Probas.init_nodes_homo ~nsamples ~nsites
  | Heterogen ->
      S.Probas.init_nodes_hetero ~nsamples ~nsites ~ncat


let zero_transition
  (type u v) (stgs : (u, v) S.Probas.settings)
  ncat : v =
  match stgs with
  | Homogen ->
      Lac.Mat.make0 4 4
  | Heterogen ->
      L.init ncat (fun _ -> (Lac.Mat.make0 4 4, 0.))


let zero_proba
  (type u v) (stgs : (u, v) S.Probas.settings)
  data ncat : v =
  let nsites = D.variant_sites data in
  match stgs with
  | Homogen ->
      Lac.Mat.make0 4 nsites
  | Heterogen ->
      L.init ncat (fun _ -> (Lac.Mat.make0 4 nsites, 0.))


let zero_vector
  (type u v) (stgs : (u, v) S.Probas.settings)
  ncat : u =
  match stgs with
  | Homogen ->
      Lac.Vec.make0 4
  | Heterogen ->
      L.init ncat (fun _ -> (Lac.Vec.make0 4, 0.))

  
let zero_vectors
  (type u v) (stgs : (u, v) S.Probas.settings)
  data ncat : u M.Int.t =
  let nsamples = D.nsamples data in
  match stgs with
  | Homogen ->
      S.Probas.zero_vectors_homo ~nsamples
  | Heterogen ->
      S.Probas.zero_vectors_hetero ~nsamples ~ncat


let zero_proba_1n
  (type u v) (stgs : (u, v) S.Probas.settings)
  data ncat : v =
  let nsites = D.variant_sites data in
  match stgs with
  | Homogen ->
      Lac.Mat.make0 1 nsites
  | Heterogen ->
      L.init ncat (fun _ -> (Lac.Mat.make0 1 nsites, 0.))


let zero_mat_1n data =
  let nsites = D.variant_sites data in
  Lac.Mat.make0 1 nsites


let compute
  (type u v)
  (stgs : (u, v) S.Probas.settings)
  ?pmem_4n
  data =
  let seq_probas = D.sequence_probas data in
  let f trans_mat tree probas =
    (* Return fresh matrices with copy *)
    (* FIXME alloc -- I need at most two copies *)
    let probas = M.Int.map (fun (ps, is) ->
      (S.Probas.lacpy stgs ps, is)
    ) probas
    in
    let _, probas = Fel.alignment_likelihood
      stgs
      ~pstore_4n:probas
      ?pmem_4n
      seq_probas
      trans_mat
      tree
    in
    U.Option.some probas
  in f


let update
  (type u v)
  (stgs : (u, v) S.Probas.settings)
  ?pmem_4n
  data =
  let seq_probas = D.sequence_probas data in
  let f trans_mat tree changed_nodes probas =
    let _, probas = Fel.alignment_likelihood_least_work
      stgs
      ?pmem_4n
      ~pstore_4n:probas
      ~changed_nodes
      seq_probas
      trans_mat
      tree
    in
    probas
  in f



let log_likelihood
  stgs ?cut
  ?sstore_4 ?pmem_4n ?smem_4 ?pmem_1n ?mmem_1n
  data =
  let nvsites = D.variant_sites data in
  (* for other operations where we don't need to hold onto memory *)
  let invs = D.invariants data in
  let f eq_probas trans_mat probas tree =
    let logd =
      if nvsites = 0 then
        (* if no variant sites, this component doesn't count *)
        0.
      else begin
        let rprobas = Fel.root_probas stgs ?pmem_4n probas tree in
        Fel.log_likelihood_from_root
          stgs
          ?pmem_1n
          ?mmem_1n
          eq_probas
          rprobas
      end
    in
    let logd' = Fel.log_likelihood_invariant
      stgs
      ?sstore_4
      ?smem_4
      invs
      eq_probas
      trans_mat
      tree
    in
    [logd +. logd']
  in
  match cut with
  | None ->
      f
  | Some t0 ->
      (* when cut is Some t0, we cut the tree under t0,
       * and compute the loglik for the different branches *)
      (fun eq_probas trans_mat probas tree ->
        tree
        |> T.cut_to_forest t0
        |> L.map (f eq_probas trans_mat probas)
        |> L.reduce (@)
      )

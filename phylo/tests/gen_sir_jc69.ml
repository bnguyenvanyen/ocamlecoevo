module F = Util.Float

module PS = Phylo.Gen_sir_jc69


(* call Estimate.posterior *)
let run pws path =
  let seed = 1234 in
  let theta_every = 1 in
  let hypar =
    PS.Hyper.default
    |> (fun hy -> { hy with eco = hy.eco#with_t0 (Some 0.) })
    |> (fun hy -> { hy with prop = pws })
  in
  let stgs = PS.Data.Settings.{
    eco = Epifit.Likelihood.none ;
    evo = { use_seqs = true ; use_tree = true } ;
  }
  in
  let infer = PS.Infer.Term_input.default in
  let seq_data = Phylo.Seq_data.read [
    ((1., "1"), Seqs.Io.of_string "ATCGAACG") ;
    ((1.5, "2"), Seqs.Io.of_string "ATGGAACG") ;
    ((2., "3"), Seqs.Io.of_string "ATCGATCG") ;
    ((2.5, "4"), Seqs.Io.of_string "ATGGAACG") ;
  ]
  in
  let data = PS.Data.{
    eco = Epifit.Data.empty ;
    evo = seq_data ;
  }
  in
  let eco0 = new Epi.Param.t_unit in
  let evo0 = { Seqsim.Jc69.mu = 1e-1 } in
  let z0 = Epifit.Sir.Fit.to_state (new Epifit.Episim.theta) in
  let tree0 = None in
  ignore (PS.Estimate.posterior
    ~seed
    ~theta_every
    ~path
    hypar
    stgs
    infer
    data
    eco0
    evo0
    z0
    tree0
    4
  )


(* Test default proposal weights *)
let%expect_test _ =
  let path = "/tmp/ocamlecoevo.phylo.sir.test" in
  run Phylo.Proposal_weights.default path ;
  [%expect] ;
  let s = BatFile.with_file_in
    ~perm:BatFile.user_read
    (path ^ ".theta.csv")
    BatIO.read_all
  in
  Printf.printf "%s" s ;
  [%expect{|
    k,outcome,logprior,loglik,logu,p'p,qq',l'l,logv,loglik-eco,loglik-tree,loglik-evo,beta,nu,rho,s0,i0,r0,mu,move_beta,move_nu,move_rho,move_s0,move_i0,move_r0,move_mu
    0,accept,-20.9227086714,-43.240038139,0.,inf,inf,inf,inf,0.,-12.7195999658,-30.5204381732,70.,52.,0.1,899980.,10.,100000.,0.1,70.,52.,0.1,899980.,10.,100000.,0.1
    0,accept,-20.9227086714,-33.3626952374,0.,0.,-0.401512274705,9.87734290158,9.47583062687,0.,-11.6313749673,-21.7313202701,70.,52.,0.1,899980.,10.,100000.,0.1,70.,52.,0.1,899980.,10.,100000.,0.1
    1,accept,-20.9227086714,-32.7758456307,0.,0.,0.00559531755773,0.586849606759,0.592444924317,0.,-11.6331956281,-21.1426500026,70.,52.,0.1,899980.,10.,100000.,0.1,70.,52.,0.1,899980.,10.,100000.,0.1
    2,failed_proposal,-20.9227086714,-32.7758456307,0.,nan,-inf,nan,-inf,0.,-11.6331956281,-21.1426500026,70.,52.,0.1,899980.,10.,100000.,0.1,,,,,,,
    3,accept,-20.9227086714,-32.7614810894,0.,0.,0.00549492855337,0.0143645412365,0.0198594697899,0.,-11.633059233,-21.1284218564,70.,52.,0.1,899980.,10.,100000.,0.1,70.,52.,0.1,899980.,10.,100000.,0.1 |}]


(* Test unif SPR proposal *)
let%expect_test _ =
  let pws = Phylo.Proposal_weights.{ zero with spru_mh = F.one } in
  let path = "/tmp/ocamlecoevo.phylo.sir.unif.test" in
  run pws path ;
  [%expect] ;
  let s = BatFile.with_file_in
    ~perm:BatFile.user_read
    (path ^ ".theta.csv")
    BatIO.read_all
  in
  Printf.printf "%s" s ;
  [%expect{|
    k,outcome,logprior,loglik,logu,p'p,qq',l'l,logv,loglik-eco,loglik-tree,loglik-evo,beta,nu,rho,s0,i0,r0,mu,move_beta,move_nu,move_rho,move_s0,move_i0,move_r0,move_mu
    0,accept,-20.9227086714,-43.240038139,0.,inf,inf,inf,inf,0.,-12.7195999658,-30.5204381732,70.,52.,0.1,899980.,10.,100000.,0.1,70.,52.,0.1,899980.,10.,100000.,0.1
    0,accept,-20.9227086714,-43.2370415559,0.,0.,0.,0.00299658309716,0.00299658309716,0.,-12.7196634285,-30.5173781275,70.,52.,0.1,899980.,10.,100000.,0.1,70.,52.,0.1,899980.,10.,100000.,0.1
    1,accept,-20.9227086714,-43.2453000331,-1.61924456752,0.,0.,-0.00825847722094,-0.00825847722094,0.,-12.7194935529,-30.5258064803,70.,52.,0.1,899980.,10.,100000.,0.1,70.,52.,0.1,899980.,10.,100000.,0.1
    2,accept,-20.9227086714,-43.2506789494,-0.479276235269,0.,0.,-0.00537891630071,-0.00537891630071,0.,-12.7194907959,-30.5311881535,70.,52.,0.1,899980.,10.,100000.,0.1,70.,52.,0.1,899980.,10.,100000.,0.1
    3,accept,-20.9227086714,-40.9541352238,0.,0.,0.,2.2965437256,2.2965437256,0.,-12.7240262858,-28.2301089381,70.,52.,0.1,899980.,10.,100000.,0.1,70.,52.,0.1,899980.,10.,100000.,0.1 |}]


(* Test div SPR proposal *)
let%expect_test _ =
  let pws = Phylo.Proposal_weights.{ zero with sprd_mh = F.one } in
  let path = "/tmp/ocamlecoevo.phylo.sir.div.test" in
  run pws path ;
  [%expect] ;
  let s = BatFile.with_file_in
    ~perm:BatFile.user_read
    (path ^ ".theta.csv")
    BatIO.read_all
  in
  Printf.printf "%s" s ;
  [%expect{|
    k,outcome,logprior,loglik,logu,p'p,qq',l'l,logv,loglik-eco,loglik-tree,loglik-evo,beta,nu,rho,s0,i0,r0,mu,move_beta,move_nu,move_rho,move_s0,move_i0,move_r0,move_mu
    0,accept,-20.9227086714,-43.240038139,0.,inf,inf,inf,inf,0.,-12.7195999658,-30.5204381732,70.,52.,0.1,899980.,10.,100000.,0.1,70.,52.,0.1,899980.,10.,100000.,0.1
    0,failed_proposal,-20.9227086714,-43.240038139,0.,nan,-inf,nan,-inf,0.,-12.7195999658,-30.5204381732,70.,52.,0.1,899980.,10.,100000.,0.1,,,,,,,
    1,failed_proposal,-20.9227086714,-43.240038139,0.,nan,-inf,nan,-inf,0.,-12.7195999658,-30.5204381732,70.,52.,0.1,899980.,10.,100000.,0.1,,,,,,,
    2,accept,-20.9227086714,-40.9825986979,0.,0.,0.,2.25743944107,2.25743944107,0.,-12.7240409149,-28.258557783,70.,52.,0.1,899980.,10.,100000.,0.1,70.,52.,0.1,899980.,10.,100000.,0.1
    3,failed_proposal,-20.9227086714,-40.9825986979,0.,nan,-inf,nan,-inf,0.,-12.7240409149,-28.258557783,70.,52.,0.1,899980.,10.,100000.,0.1,,,,,,, |}]


(* Test guided SPR proposal *)
let%expect_test _ =
  let pws = Phylo.Proposal_weights.{ zero with sprg_mh = F.one } in
  let path = "/tmp/ocamlecoevo.phylo.sir.guided.test" in
  run pws path ;
  [%expect] ;
  let s = BatFile.with_file_in
    ~perm:BatFile.user_read
    (path ^ ".theta.csv")
    BatIO.read_all
  in
  Printf.printf "%s" s ;
  [%expect{|
    k,outcome,logprior,loglik,logu,p'p,qq',l'l,logv,loglik-eco,loglik-tree,loglik-evo,beta,nu,rho,s0,i0,r0,mu,move_beta,move_nu,move_rho,move_s0,move_i0,move_r0,move_mu
    0,accept,-20.9227086714,-43.240038139,0.,inf,inf,inf,inf,0.,-12.7195999658,-30.5204381732,70.,52.,0.1,899980.,10.,100000.,0.1,70.,52.,0.1,899980.,10.,100000.,0.1
    0,accept,-20.9227086714,-33.3626952374,0.,0.,-0.401512274705,9.87734290158,9.47583062687,0.,-11.6313749673,-21.7313202701,70.,52.,0.1,899980.,10.,100000.,0.1,70.,52.,0.1,899980.,10.,100000.,0.1
    1,accept,-20.9227086714,-32.7758456307,0.,0.,0.00559531755773,0.586849606759,0.592444924317,0.,-11.6331956281,-21.1426500026,70.,52.,0.1,899980.,10.,100000.,0.1,70.,52.,0.1,899980.,10.,100000.,0.1
    2,accept,-20.9227086714,-31.2049590584,-0.623787646177,0.,-1.98866942029,1.57088657223,-0.41778284806,0.,-10.6747176445,-20.5302414139,70.,52.,0.1,899980.,10.,100000.,0.1,70.,52.,0.1,899980.,10.,100000.,0.1
    3,accept,-20.9227086714,-31.3209857898,-1.55731000127,0.,-0.00128571255924,-0.116026731349,-0.117312443909,0.,-10.801386924,-20.5195988658,70.,52.,0.1,899980.,10.,100000.,0.1,70.,52.,0.1,899980.,10.,100000.,0.1 |}]

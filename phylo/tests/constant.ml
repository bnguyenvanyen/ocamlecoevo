module F = Util.Float

module PC = Phylo.Constant_jc69


let run pws path =
  let seed = 1234 in
  let theta_every = 1 in
  let hypar =
    (new PC.Hyper.t)#with_proposal_weights pws
  in
  let infer = PC.Infer.default in
  let tseqs = [
    ((1., "1"), Seqs.Io.of_string "ATCGAACG") ;
    ((1.5, "2"), Seqs.Io.of_string "ATGGAACG") ;
    ((2., "3"), Seqs.Io.of_string "ATCGATCG") ;
    ((2.5, "4"), Seqs.Io.of_string "ATGGAACG") ;
  ]
  in
  let theta = (new PC.Theta.Param.t) in
  ignore (PC.Estimate.posterior
    ~seed
    ~theta_every
    ~path
    hypar
    infer
    tseqs
    None
    theta
    4
  )


(* Test default proposal weights *)
let%expect_test _ =
  let path = "/tmp/ocamlecoevo.phylo.constant.test" in
  run Phylo.Proposal_weights.default path ;
  [%expect] ;
  let s = BatFile.with_file_in
    ~perm:BatFile.user_read
    (path ^ ".theta.csv")
    BatIO.read_all
  in
  Printf.printf "%s" s ;
  [%expect{|
    k,outcome,logprior,loglik,logu,p'p,qq',l'l,logv,mu,lambda,move_mu,move_lambda
    0,accept,-10.586140762,-40.1729734116,0.,inf,inf,inf,inf,1.,1.,1.,1.
    0,accept,-10.4297338554,-36.2923868765,0.,0.156406906606,-1.20741974884,3.88058653511,2.82957369288,1.,1.,1.,1.
    1,accept,-10.6764696052,-36.8325365525,-1.34536786052,-0.246735749855,-0.000788492438573,-0.54014967607,-0.787673918363,1.,1.,1.,1.
    2,reject,-10.6764696052,-36.8325365525,-0.895374825244,-0.582189353453,0.538174772383,-1.93601632726,-1.98003090833,1.,1.,1.,1.
    3,accept,-9.68048611644,-31.7793214817,0.,0.995983488799,0.0111849401796,5.05321507081,6.06038349979,1.,1.,1.,1. |}]


(* Test unif SPR proposal *)
let%expect_test _ =
  let pws = Phylo.Proposal_weights.{ zero with spru_mh = F.one } in
  let path = "/tmp/ocamlecoevo.phylo.constant.unif.test" in
  run pws path ;
  [%expect] ;
  let s = BatFile.with_file_in
    ~perm:BatFile.user_read
    (path ^ ".theta.csv")
    BatIO.read_all
  in
  Printf.printf "%s" s ;
  [%expect{|
    k,outcome,logprior,loglik,logu,p'p,qq',l'l,logv,mu,lambda,move_mu,move_lambda
    0,accept,-10.586140762,-40.1729734116,0.,inf,inf,inf,inf,1.,1.,1.,1.
    0,reject,-10.586140762,-40.1729734116,-1.0621124108,-0.40022838428,0.,-1.0838426584,-1.48407104268,1.,1.,1.,1.
    1,reject,-10.586140762,-40.1729734116,-1.76689772012,-1.98918870573,0.,-1.45067175855,-3.43986046428,1.,1.,1.,1.
    2,accept,-10.1221687549,-37.2017114217,0.,0.463972007139,0.,2.97126198985,3.43523399698,1.,1.,1.,1.
    3,reject,-10.1221687549,-37.2017114217,-1.12197271706,-0.526496506548,0.,-2.09136052316,-2.61785702971,1.,1.,1.,1. |}]


(* Test div SPR proposal *)
let%expect_test _ =
  let pws = Phylo.Proposal_weights.{ zero with sprd_mh = F.one } in
  let path = "/tmp/ocamlecoevo.phylo.constant.div.test" in
  run pws path ;
  [%expect] ;
  let s = BatFile.with_file_in
    ~perm:BatFile.user_read
    (path ^ ".theta.csv")
    BatIO.read_all
  in
  Printf.printf "%s" s ;
  [%expect{|
    k,outcome,logprior,loglik,logu,p'p,qq',l'l,logv,mu,lambda,move_mu,move_lambda
    0,accept,-10.586140762,-40.1729734116,0.,inf,inf,inf,inf,1.,1.,1.,1.
    0,accept,-10.6896527905,-40.5515182737,-0.677573937695,-0.10351202853,0.,-0.378544862079,-0.482056890609,1.,1.,1.,1.
    1,reject,-10.6896527905,-40.5515182737,-0.0216274116059,-0.015408517671,0.,-0.0494898380784,-0.0648983557495,1.,1.,1.,1.
    2,accept,-10.8388342379,-40.700429343,-1.48238738411,-0.149181447342,0.,-0.148911069368,-0.29809251671,1.,1.,1.,1.
    3,accept,-10.8312775079,-40.6754289105,0.,0.0075567299519,0.,0.025000432485,0.0325571624369,1.,1.,1.,1. |}]


(* Test guided SPR proposal *)
let%expect_test _ =
  let pws = Phylo.Proposal_weights.{ zero with sprg_mh = F.one } in
  let path = "/tmp/ocamlecoevo.phylo.constant.guided.test" in
  run pws path ;
  [%expect] ;
  let s = BatFile.with_file_in
    ~perm:BatFile.user_read
    (path ^ ".theta.csv")
    BatIO.read_all
  in
  Printf.printf "%s" s ;
  [%expect{|
    k,outcome,logprior,loglik,logu,p'p,qq',l'l,logv,mu,lambda,move_mu,move_lambda
    0,accept,-10.586140762,-40.1729734116,0.,inf,inf,inf,inf,1.,1.,1.,1.
    0,accept,-10.4297338554,-36.2923868765,0.,0.156406906606,-1.20741974884,3.88058653511,2.82957369288,1.,1.,1.,1.
    1,accept,-10.6764696052,-36.8325365525,-1.34536786052,-0.246735749855,-0.000788492438573,-0.54014967607,-0.787673918363,1.,1.,1.,1.
    2,reject,-10.6764696052,-36.8325365525,-0.895374825244,-0.582189353453,0.538174772383,-1.93601632726,-1.98003090833,1.,1.,1.,1.
    3,accept,-9.68048611644,-31.7793214817,0.,0.995983488799,0.0111849401796,5.05321507081,6.06038349979,1.,1.,1.,1. |}]

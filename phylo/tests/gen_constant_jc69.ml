module F = Util.Float

module PC = Phylo.Generic.Make (Phylo.Constant) (Phylo.Jc69)


let run pws path =
  let seed = 1234 in
  let theta_every = 1 in
  let hypar =
    PC.Hyper.default
    |> (fun hy -> { hy with prop = pws })
  in
  let stgs = PC.Data.Settings.{
    eco = () ;
    evo = { use_seqs = true ; use_tree = true } ;
  }
  in
  let infer = PC.Infer.Term_input.default in
  let seq_data = Phylo.Seq_data.read [
    ((1., "1"), Seqs.Io.of_string "ATCGAACG") ;
    ((1.5, "2"), Seqs.Io.of_string "ATGGAACG") ;
    ((2., "3"), Seqs.Io.of_string "ATCGATCG") ;
    ((2.5, "4"), Seqs.Io.of_string "ATGGAACG") ;
  ]
  in
  let data = PC.Data.{
    eco = () ;
    evo = seq_data ;
  }
  in
  let eco0 = Phylo.Constant.Theta.default in
  let evo0 = { Seqsim.Jc69.mu = 1e-1 } in
  let z0 = () in
  let tree0 = None in
  ignore (PC.Estimate.posterior
    ~seed
    ~theta_every
    ~path
    hypar
    stgs
    infer
    data
    eco0
    evo0
    z0
    tree0
    4
  )


(* Test default proposal weights *)
let%expect_test _ =
  let path = "/tmp/ocamlecoevo.phylo.constant.test" in
  run Phylo.Proposal_weights.default path ;
  [%expect] ;
  let s = BatFile.with_file_in
    ~perm:BatFile.user_read
    (path ^ ".theta.csv")
    BatIO.read_all
  in
  Printf.printf "%s" s ;
  [%expect{|
    k,outcome,logprior,loglik,logu,p'p,qq',l'l,logv,loglik-eco,loglik-tree,loglik-evo,lambda,mu,move_lambda,move_mu
    0,accept,-4.03529197342,-25.269794832,0.,inf,inf,inf,inf,0.,-0.748263695585,-24.5215311364,1.,0.1,1.,0.1
    0,accept,-4.03529197342,-23.0785939023,0.,0.,-1.89867021788,2.19120092971,0.292530711827,0.,-1.48858280872,-21.5900110936,1.,0.1,1.,0.1
    1,accept,-4.03529197342,-23.5034933805,-1.34536786052,0.,-0.128149640904,-0.424899478184,-0.553049119088,0.,-1.87984032473,-21.6236530557,1.,0.1,1.,0.1
    2,accept,-4.03529197342,-23.632665066,-0.895374825244,0.,-0.0299268682689,-0.12917168553,-0.159098553799,0.,-1.97153443372,-21.6611306323,1.,0.1,1.,0.1
    3,accept,-4.03529197342,-22.8982366493,0.,0.,0.0156187915739,0.734428416704,0.750047208278,0.,-1.31291609338,-21.5853205559,1.,0.1,1.,0.1 |}]


(* Test unif SPR proposal *)
let%expect_test _ =
  let pws = Phylo.Proposal_weights.{ zero with spru_mh = F.one } in
  let path = "/tmp/ocamlecoevo.phylo.constant.unif.test" in
  run pws path ;
  [%expect] ;
  let s = BatFile.with_file_in
    ~perm:BatFile.user_read
    (path ^ ".theta.csv")
    BatIO.read_all
  in
  Printf.printf "%s" s ;
  [%expect{|
    k,outcome,logprior,loglik,logu,p'p,qq',l'l,logv,loglik-eco,loglik-tree,loglik-evo,lambda,mu,move_lambda,move_mu
    0,accept,-4.03529197342,-25.269794832,0.,inf,inf,inf,inf,0.,-0.748263695585,-24.5215311364,1.,0.1,1.,0.1
    0,accept,-4.03529197342,-25.9978201716,-1.0621124108,0.,0.,-0.728025339634,-0.728025339634,0.,-1.14849207987,-24.8493280918,1.,0.1,1.,0.1
    1,accept,-4.03529197342,-27.6057570336,-1.76689772012,0.,0.,-1.60793686201,-1.60793686201,0.,-3.04512655091,-24.5606304827,1.,0.1,1.,0.1
    2,accept,-4.03529197342,-26.923321751,0.,0.,0.,0.682435282629,0.682435282629,0.,-2.65893785516,-24.2643838958,1.,0.1,1.,0.1
    3,accept,-4.03529197342,-24.8079200047,0.,0.,0.,2.11540174635,2.11540174635,0.,-0.674932750388,-24.1329872543,1.,0.1,1.,0.1 |}]


(* Test div SPR proposal *)
let%expect_test _ =
  let pws = Phylo.Proposal_weights.{ zero with sprd_mh = F.one } in
  let path = "/tmp/ocamlecoevo.phylo.constant.div.test" in
  run pws path ;
  [%expect] ;
  let s = BatFile.with_file_in
    ~perm:BatFile.user_read
    (path ^ ".theta.csv")
    BatIO.read_all
  in
  Printf.printf "%s" s ;
  [%expect{|
    k,outcome,logprior,loglik,logu,p'p,qq',l'l,logv,loglik-eco,loglik-tree,loglik-evo,lambda,mu,move_lambda,move_mu
    0,accept,-4.03529197342,-25.269794832,0.,inf,inf,inf,inf,0.,-0.748263695585,-24.5215311364,1.,0.1,1.,0.1
    0,reject,-4.03529197342,-25.269794832,-0.0826192139387,0.,0.,-1.9387319511,-1.9387319511,0.,-0.748263695585,-24.5215311364,1.,0.1,1.,0.1
    1,accept,-4.03529197342,-24.6191709071,0.,0.,0.,0.650623924879,0.650623924879,0.,-0.357077504261,-24.2620934029,1.,0.1,1.,0.1
    2,reject,-4.03529197342,-24.6191709071,-1.04683080431,0.,0.,-1.51352567159,-1.51352567159,0.,-0.357077504261,-24.2620934029,1.,0.1,1.,0.1
    3,reject,-4.03529197342,-24.6191709071,-0.404045086581,0.,-0.69314718056,-0.283511856779,-0.976659037339,0.,-0.357077504261,-24.2620934029,1.,0.1,1.,0.1 |}]


(* Test guided SPR proposal *)
let%expect_test _ =
  let pws = Phylo.Proposal_weights.{ zero with sprg_mh = F.one } in
  let path = "/tmp/ocamlecoevo.phylo.constant.guided.test" in
  run pws path ;
  [%expect] ;
  let s = BatFile.with_file_in
    ~perm:BatFile.user_read
    (path ^ ".theta.csv")
    BatIO.read_all
  in
  Printf.printf "%s" s ;
  [%expect{|
    k,outcome,logprior,loglik,logu,p'p,qq',l'l,logv,loglik-eco,loglik-tree,loglik-evo,lambda,mu,move_lambda,move_mu
    0,accept,-4.03529197342,-25.269794832,0.,inf,inf,inf,inf,0.,-0.748263695585,-24.5215311364,1.,0.1,1.,0.1
    0,accept,-4.03529197342,-23.0785939023,0.,0.,-1.89867021788,2.19120092971,0.292530711827,0.,-1.48858280872,-21.5900110936,1.,0.1,1.,0.1
    1,accept,-4.03529197342,-23.5034933805,-1.34536786052,0.,-0.128149640904,-0.424899478184,-0.553049119088,0.,-1.87984032473,-21.6236530557,1.,0.1,1.,0.1
    2,accept,-4.03529197342,-23.632665066,-0.895374825244,0.,-0.0299268682689,-0.12917168553,-0.159098553799,0.,-1.97153443372,-21.6611306323,1.,0.1,1.,0.1
    3,accept,-4.03529197342,-22.8982366493,0.,0.,0.0156187915739,0.734428416704,0.750047208278,0.,-1.31291609338,-21.5853205559,1.,0.1,1.,0.1 |}]

module F = Util.Float

module PC = Phylo.Constant_jc69
module PS = Phylo.Sir_jc69


(* call Estimate.posterior *)
let run pws path =
  let seed = 1234 in
  let theta_every = 1 in
  let hypar =
    (new PS.Hyper.t)
    |> (fun hy -> hy#with_t0 (Some 0.))
    |> (fun hy -> hy#with_proposal_weights pws)
  in
  let lik_settings = PS.Likelihood.Settings.{ seq = true ; epi = `None } in
  let infer = PS.Infer.{
    seq = Free `Adapt ;
    tree = Free `Adapt ;
    epi = new PS.Infer.Epi.t ;
  }
  in
  let seq_data = PC.Data.read [
    ((1., "1"), Seqs.Io.of_string "ATCGAACG") ;
    ((1.5, "2"), Seqs.Io.of_string "ATGGAACG") ;
    ((2., "3"), Seqs.Io.of_string "ATCGATCG") ;
    ((2.5, "4"), Seqs.Io.of_string "ATGGAACG") ;
  ]
  in
  let data = PS.Data.{
    seq = seq_data ;
    epi = Epifit.Data.empty ;
  }
  in
  let tree0 = None in
  let th_seq0 = { Seqsim.Jc69.mu = 1e-1 } in
  let th_epi0 = new Epifit.Episim.theta in
  ignore (PS.Estimate.posterior
    ~seed
    ~theta_every
    ~path
    hypar
    lik_settings
    infer
    data
    th_epi0
    th_seq0
    tree0
    4
  )


(* Test default proposal weights *)
let%expect_test _ =
  let path = "/tmp/ocamlecoevo.phylo.sir.test" in
  run Phylo.Proposal_weights.default path ;
  [%expect] ;
  let s = BatFile.with_file_in
    ~perm:BatFile.user_read
    (path ^ ".theta.csv")
    BatIO.read_all
  in
  Printf.printf "%s" s ;
  [%expect{|
    k,outcome,logprior,loglik,logu,p'p,qq',l'l,logv,mu,beta,nu,rho,s0,i0,r0,ps0,pi0,pr0,move_mu,move_beta,move_nu,move_rho,move_s0,move_i0,move_r0,move_ps0,move_pi0,move_pr0
    0,accept,-20.9227086714,-43.240038139,0.,inf,inf,inf,inf,0.1,70.,52.,0.1,899980.,10.,100000.,0.89998899989,1.0000100001e-05,0.10000100001,0.1,70.,52.,0.1,899980.,10.,100000.,0.89998899989,1.0000100001e-05,0.10000100001
    0,accept,-20.9227086714,-33.3626952374,0.,0.,-0.401512274705,9.87734290158,9.47583062687,0.1,70.,52.,0.1,899980.,10.,100000.,0.89998899989,1.0000100001e-05,0.10000100001,0.1,70.,52.,0.1,899980.,10.,100000.,0.89998899989,1.0000100001e-05,0.10000100001
    1,accept,-20.9227086714,-32.7758456307,0.,0.,0.00559531755773,0.586849606759,0.592444924317,0.1,70.,52.,0.1,899980.,10.,100000.,0.89998899989,1.0000100001e-05,0.10000100001,0.1,70.,52.,0.1,899980.,10.,100000.,0.89998899989,1.0000100001e-05,0.10000100001
    2,failed_proposal,-20.9227086714,-32.7758456307,0.,nan,-inf,nan,-inf,0.1,70.,52.,0.1,899980.,10.,100000.,0.89998899989,1.0000100001e-05,0.10000100001,,,,,,,,,,
    3,failed_proposal,-20.9227086714,-32.7758456307,0.,nan,-inf,nan,-inf,0.1,70.,52.,0.1,899980.,10.,100000.,0.89998899989,1.0000100001e-05,0.10000100001,,,,,,,,,, |}]


(* Test unif SPR proposal *)
let%expect_test _ =
  let pws = Phylo.Proposal_weights.{ zero with spru_mh = F.one } in
  let path = "/tmp/ocamlecoevo.phylo.sir.unif.test" in
  run pws path ;
  [%expect] ;
  let s = BatFile.with_file_in
    ~perm:BatFile.user_read
    (path ^ ".theta.csv")
    BatIO.read_all
  in
  Printf.printf "%s" s ;
  [%expect{|
    k,outcome,logprior,loglik,logu,p'p,qq',l'l,logv,mu,beta,nu,rho,s0,i0,r0,ps0,pi0,pr0,move_mu,move_beta,move_nu,move_rho,move_s0,move_i0,move_r0,move_ps0,move_pi0,move_pr0
    0,accept,-20.9227086714,-43.240038139,0.,inf,inf,inf,inf,0.1,70.,52.,0.1,899980.,10.,100000.,0.89998899989,1.0000100001e-05,0.10000100001,0.1,70.,52.,0.1,899980.,10.,100000.,0.89998899989,1.0000100001e-05,0.10000100001
    0,accept,-20.9227086714,-43.2370415559,0.,0.,0.,0.00299658309716,0.00299658309716,0.1,70.,52.,0.1,899980.,10.,100000.,0.89998899989,1.0000100001e-05,0.10000100001,0.1,70.,52.,0.1,899980.,10.,100000.,0.89998899989,1.0000100001e-05,0.10000100001
    1,accept,-20.9227086714,-43.2453000331,-1.61924456752,0.,0.,-0.00825847722094,-0.00825847722094,0.1,70.,52.,0.1,899980.,10.,100000.,0.89998899989,1.0000100001e-05,0.10000100001,0.1,70.,52.,0.1,899980.,10.,100000.,0.89998899989,1.0000100001e-05,0.10000100001
    2,accept,-20.9227086714,-43.2506789494,-0.479276235269,0.,0.,-0.00537891630071,-0.00537891630071,0.1,70.,52.,0.1,899980.,10.,100000.,0.89998899989,1.0000100001e-05,0.10000100001,0.1,70.,52.,0.1,899980.,10.,100000.,0.89998899989,1.0000100001e-05,0.10000100001
    3,accept,-20.9227086714,-40.9541352238,0.,0.,0.,2.2965437256,2.2965437256,0.1,70.,52.,0.1,899980.,10.,100000.,0.89998899989,1.0000100001e-05,0.10000100001,0.1,70.,52.,0.1,899980.,10.,100000.,0.89998899989,1.0000100001e-05,0.10000100001 |}]


(* Test div SPR proposal *)
let%expect_test _ =
  let pws = Phylo.Proposal_weights.{ zero with sprd_mh = F.one } in
  let path = "/tmp/ocamlecoevo.phylo.sir.div.test" in
  run pws path ;
  [%expect] ;
  let s = BatFile.with_file_in
    ~perm:BatFile.user_read
    (path ^ ".theta.csv")
    BatIO.read_all
  in
  Printf.printf "%s" s ;
  [%expect{|
    k,outcome,logprior,loglik,logu,p'p,qq',l'l,logv,mu,beta,nu,rho,s0,i0,r0,ps0,pi0,pr0,move_mu,move_beta,move_nu,move_rho,move_s0,move_i0,move_r0,move_ps0,move_pi0,move_pr0
    0,accept,-20.9227086714,-43.240038139,0.,inf,inf,inf,inf,0.1,70.,52.,0.1,899980.,10.,100000.,0.89998899989,1.0000100001e-05,0.10000100001,0.1,70.,52.,0.1,899980.,10.,100000.,0.89998899989,1.0000100001e-05,0.10000100001
    0,failed_proposal,-20.9227086714,-43.240038139,0.,nan,-inf,nan,-inf,0.1,70.,52.,0.1,899980.,10.,100000.,0.89998899989,1.0000100001e-05,0.10000100001,,,,,,,,,,
    1,failed_proposal,-20.9227086714,-43.240038139,0.,nan,-inf,nan,-inf,0.1,70.,52.,0.1,899980.,10.,100000.,0.89998899989,1.0000100001e-05,0.10000100001,,,,,,,,,,
    2,accept,-20.9227086714,-40.9825986979,0.,0.,0.,2.25743944107,2.25743944107,0.1,70.,52.,0.1,899980.,10.,100000.,0.89998899989,1.0000100001e-05,0.10000100001,0.1,70.,52.,0.1,899980.,10.,100000.,0.89998899989,1.0000100001e-05,0.10000100001
    3,failed_proposal,-20.9227086714,-40.9825986979,0.,nan,-inf,nan,-inf,0.1,70.,52.,0.1,899980.,10.,100000.,0.89998899989,1.0000100001e-05,0.10000100001,,,,,,,,,, |}]


(* Test guided SPR proposal *)
let%expect_test _ =
  let pws = Phylo.Proposal_weights.{ zero with sprg_mh = F.one  } in
  let path = "/tmp/ocamlecoevo.phylo.sir.guided.test" in
  run pws path ;
  [%expect] ;
  let s = BatFile.with_file_in
    ~perm:BatFile.user_read
    (path ^ ".theta.csv")
    BatIO.read_all
  in
  Printf.printf "%s" s ;
  [%expect{|
    k,outcome,logprior,loglik,logu,p'p,qq',l'l,logv,mu,beta,nu,rho,s0,i0,r0,ps0,pi0,pr0,move_mu,move_beta,move_nu,move_rho,move_s0,move_i0,move_r0,move_ps0,move_pi0,move_pr0
    0,accept,-20.9227086714,-43.240038139,0.,inf,inf,inf,inf,0.1,70.,52.,0.1,899980.,10.,100000.,0.89998899989,1.0000100001e-05,0.10000100001,0.1,70.,52.,0.1,899980.,10.,100000.,0.89998899989,1.0000100001e-05,0.10000100001
    0,accept,-20.9227086714,-33.3626952374,0.,0.,-0.401512274705,9.87734290158,9.47583062687,0.1,70.,52.,0.1,899980.,10.,100000.,0.89998899989,1.0000100001e-05,0.10000100001,0.1,70.,52.,0.1,899980.,10.,100000.,0.89998899989,1.0000100001e-05,0.10000100001
    1,accept,-20.9227086714,-32.7758456307,0.,0.,0.00559531755773,0.586849606759,0.592444924317,0.1,70.,52.,0.1,899980.,10.,100000.,0.89998899989,1.0000100001e-05,0.10000100001,0.1,70.,52.,0.1,899980.,10.,100000.,0.89998899989,1.0000100001e-05,0.10000100001
    2,accept,-20.9227086714,-31.2049590584,-0.623787646177,0.,-1.98866942029,1.57088657223,-0.41778284806,0.1,70.,52.,0.1,899980.,10.,100000.,0.89998899989,1.0000100001e-05,0.10000100001,0.1,70.,52.,0.1,899980.,10.,100000.,0.89998899989,1.0000100001e-05,0.10000100001
    3,accept,-20.9227086714,-31.3209857898,-1.55731000127,0.,-0.00128571255924,-0.116026731349,-0.117312443909,0.1,70.,52.,0.1,899980.,10.,100000.,0.89998899989,1.0000100001e-05,0.10000100001,0.1,70.,52.,0.1,899980.,10.,100000.,0.89998899989,1.0000100001e-05,0.10000100001 |}]

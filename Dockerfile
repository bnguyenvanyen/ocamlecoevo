FROM ocurrent/opam:debian-10-ocaml-4.08-flambda

# Install external dependencies
# libcairo-dev pulls a lot of heavy dependencies
RUN sudo apt-get update\
 && sudo apt-get install -y\
    gfortran\
    m4\
    libblas-dev\
    liblapack-dev\
    libcairo2-dev\
    python3-pip


# Install OCaml dependencies

# Copy the opam file (with dependencies) to /tmp
COPY ecoevo.opam /tmp/

# This fails on curl timeouts, so split up the installs
RUN eval $(opam env) && opam install /tmp/ecoevo.opam --deps-only -v

# Install Python dependencies

RUN pip3 install numpy scipy pandas matplotlib seaborn


# Prepare

COPY --chown=opam . $HOME/ocamlecoevo
WORKDIR $HOME/ocamlecoevo

RUN eval $(opam env)\
 && dune build\
 && dune runtest\
 && dune install

ENV PYTHONPATH "${PYTHONPATH}:${HOME}/.local/bin"

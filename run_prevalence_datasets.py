#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os
import shutil
import json
from subprocess import run

seed = 12345


def dirac(val) :
  return {
    "name" : "dirac",
    "distributionParameter" : [{ "value" : val }]
  }


# def discretized_normal(lower, upper, mean, sd) :
def normal(lower, upper, mean, sd) :
  return {
    # "name" : "discretized_normal",
    "name" : "normal",
    "distributionParameter" : [
        { "name" : "lower", "value" : lower },
        { "name" : "upper", "value" : upper },
        { "name" : "mean", "value" : mean },
        { "name" : "sd", "value" : sd },
    ]
  }


command = (
  "dune exec epi-gill-sir -- "
  "-birth 0. "
  "-death 0. "
  "-popsize {size} "
  "-beta {beta} "
  "-nu {nu} "
  "-rho 0.9 "
  "-seed {seed} "
  "-dt {dt} "
  "-tf {tf} "
  "-s0 {s0} "
  "-i0 {i0} "
  "-r0 {r0} "
  "-out _data/sir/run.{seed}.{exp}"
)


command_data = (
  "dune exec epi-gen-data -- "
  "--seed {seed} "
  "--from cases "
  "--prev "
  "--rho 0.9 "
  "_data/sir/run.{seed}.{exp}.traj.csv"
)


tf = 1. / 12.
dt = 1. / 365.25

expts = list(range(5)) + [7]

for exp in expts :
  scale = 2 ** exp
  beta = 240
  nu = 80 
  size = int(500 * scale)
  s0 = int(0.9 * size)
  i0 = int(scale * 10)
  r0 = size - s0 - i0
  cmd = command.format(
    size=size,
    beta=beta,
    nu=nu,
    seed=seed,
    tf=tf,
    dt=dt,
    s0=s0,
    i0=i0,
    r0=r0,
    exp=exp
  )
  cmd_data = command_data.format(
    seed=seed,
    exp=exp,
  )
  run(cmd, shell=True)
  run(cmd_data, shell=True)

  # setup the SSM model
  # the model is already set up with the right fixed parameter values :
  # init beta, nu prior, rho prior, starting date, etc
  # what we need to change are the N prior and the data

  # create the new directory
  old_dir = "/home/bnguyen/Documents/these/SSM/ssm/SIR_prevalence"
  new_dir = "/home/bnguyen/Documents/these/SSM/ssm/SIR_prevalence_{seed}_{exp}".format(
    seed=seed,
    exp=exp
  )
  try :
    shutil.copytree(old_dir, new_dir)
  except FileExistsError :
    # assume new_dir already exists
    pass

  data_in_fmt = "_data/sir/run.{seed}.{exp}.data.{source}.csv"

  data_out_fmt = "{new_dir}/data/data.{source}.csv"

  # I'm lazy so I reuse the scripts
  command_conv_data = (
    "cat {data_in} "
    "| python3 scripts/translate_years.py --add 2000 "
    "| python3 scripts/years_to_date.py "
    "> {data_out}"
  )

  for source in ["cases", "prevalence"] :
    data_in = data_in_fmt.format(
      seed=seed,
      exp=exp,
      source=source
    )

    data_out = data_out_fmt.format(
      new_dir=new_dir,
      source=source
    )

    cmd_conv_data = command_conv_data.format(
      data_in=data_in,
      data_out=data_out,
    )

    run(cmd_conv_data, shell=True)

  # Write theta.json file
  theta = {
    "resources" : [
      {"name" : "values",
       "description" : "initial values",
       "data": {
         "beta" : beta,
         "s" : s0,
         "i" : i0,
       }
      },
       {"name" : "covariance",
        "description" : "covariance matrix",
        "data" : {
          "beta" : {"beta" : 0.1},
          "s" : {"s" : 0.1},
          "i" : {"i" : 0.1}
        }
       }
    ]
  }

  theta_path = "{new_dir}/theta.json".format(new_dir=new_dir)
  with open(theta_path, "w") as f :
    json.dump(theta, f)
  
  # Write priors json files

  priors = [
    { "path" : "s",
      "name" : "normal",
      "value" : s0,
      "lower" : 0,
      "upper" : size,
      "sd" : s0 / 10 },
    { "path" : "i",
      "name" : "normal",
      "value" : i0,
      "lower" : 0,
      "upper" : size,
      "sd" : i0 / 10 },
    { "path" : "n",
      "name" : "dirac",
      "value" : size },
  ]
  # replace the prior files
  for d in priors :
    if d["name"] == "dirac" :
      prior = dirac(d["value"])
    elif d["name"] == "normal" :
      prior = normal(
        lower=d["lower"],
        upper=d["upper"],
        mean=d["value"],
        sd=d["sd"],
      )
    prior_path = "{new_dir}/priors/{name}.json".format(
      new_dir=new_dir,
      name=d["path"]
    )
    with open(prior_path, "w") as f :
      # overwrite prior file
      json.dump(prior, f)

#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os
import shutil
import json
from subprocess import run

seed = 1234


def dirac(val) :
  return {
    "name" : "dirac",
    "distributionParameter" : [{ "value" : val }]
  }


# def discretized_normal(lower, upper, mean, sd) :
def normal(lower, upper, mean, sd) :
  return {
    # "name" : "discretized_normal",
    "name" : "normal",
    "distributionParameter" : [
        { "name" : "lower", "value" : lower },
        { "name" : "upper", "value" : upper },
        { "name" : "mean", "value" : mean },
        { "name" : "sd", "value" : sd },
    ]
  }


command = (
  "dune exec epi-gill-sirs -- "
  "-birth 0.014286 "
  "-death 0.014286 "
  "-popsize {size} "
  "-beta {beta} {beta_ampl} "
  "-nu {nu} "
  "-gamma 0.1 "
  "-eta 1. "
  "-freq 1. "
  "-phase 0. "
  "-rho 0.9 "
  "-seed {seed} "
  "-dt {dt} "
  "-tf {tf} "
  "-s0 {s0} "
  "-i0 {i0} "
  "-r0 {r0} "
  "-out _data/sirs/run.{seed}.{tf}years"
)


command_data = (
  "dune exec epi-gen-data -- "
  "--seed {seed} "
  "--from cases "
  "--rho 0.9 "
  "_data/sirs/run.{seed}.{tf}years.traj.csv"
)


dt = 0.02


for tf in ["1", "3", "10"] :
  beta = 78
  beta_ampl = 0.3
  nu = 52
  size = 10000
  s0 = int(0.6 * size)
  i0 = 10
  r0 = size - s0 - i0
  cmd = command.format(
    size=size,
    beta=beta,
    beta_ampl=beta_ampl,
    nu=nu,
    seed=seed,
    tf=tf,
    dt=dt,
    s0=s0,
    i0=i0,
    r0=r0,
  )
  cmd_data = command_data.format(
    seed=seed,
    tf=tf,
  )
  run(cmd, shell=True)
  run(cmd_data, shell=True)

  # setup the SSM model
  # the model is already set up with the right fixed parameter values :
  # init beta, nu prior, rho prior, starting date, etc
  # what we need to change are the N prior and the data

  # create the new directory
  old_dir = "/home/qe/Documents/these/SSM/ssm/SIRS_seasonal_prevalence"
  new_dir = "/home/qe/Documents/these/SSM/ssm/SIRS_seasonal_prevalence_{seed}_{tf}years".format(
    seed=seed,
    tf=tf
  )
  try :
    shutil.copytree(old_dir, new_dir)
  except FileExistsError :
    # assume new_dir already exists
    pass

  data_in_fmt = "_data/sirs/run.{seed}.{tf}.data.{source}.csv"

  data_out_fmt = "{new_dir}/data/data.{source}.csv"

  # I'm lazy so I reuse the scripts instead of importing
  command_conv_data = (
    "cat {data_in} "
    "| python3 scripts/translate_years.py --add 2000 "
    "| python3 scripts/years_to_date.py "
    "> {data_out}"
  )

  for source in ["cases", "prevalence"] :
    data_in = data_in_fmt.format(
      seed=seed,
      tf=tf,
      source=source
    )

    data_out = data_out_fmt.format(
      new_dir=new_dir,
      source=source
    )

    cmd_conv_data = command_conv_data.format(
      data_in=data_in,
      data_out=data_out,
    )

    run(cmd_conv_data, shell=True)

  # Write theta.json file
  theta = {
    "resources" : [
      {"name" : "values",
       "description" : "initial values",
       "data": {
         "beta" : beta,
         "s" : s0,
         "i" : i0,
       }
      },
       {"name" : "covariance",
        "description" : "covariance matrix",
        "data" : {
          "beta" : {"beta" : 0.1},
          "s" : {"s" : 0.1},
          "i" : {"i" : 0.1}
        }
       }
    ]
  }

  theta_path = "{new_dir}/theta.json".format(new_dir=new_dir)
  with open(theta_path, "w") as f :
    json.dump(theta, f)
  
  # Write priors json files

  priors = [
    { "path" : "s",
      "name" : "normal",
      "value" : s0,
      "lower" : 0,
      "upper" : size,
      "sd" : s0 / 10 },
    { "path" : "i",
      "name" : "normal",
      "value" : i0,
      "lower" : 0,
      "upper" : size,
      "sd" : i0 / 10 },
    { "path" : "n",
      "name" : "dirac",
      "value" : size },
  ]
  # replace the prior files
  for d in priors :
    if d["name"] == "dirac" :
      prior = dirac(d["value"])
    elif d["name"] == "normal" :
      prior = normal(
        lower=d["lower"],
        upper=d["upper"],
        mean=d["value"],
        sd=d["sd"],
      )
    prior_path = "{new_dir}/priors/{name}.json".format(
      new_dir=new_dir,
      name=d["path"]
    )
    with open(prior_path, "w") as f :
      # overwrite prior file
      json.dump(prior, f)

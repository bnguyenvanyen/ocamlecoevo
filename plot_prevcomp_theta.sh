#!/usr/bin/bash


BASETARGET="_data/sir/run.1234"
BASEDEST="_data/plot/mcmc/sir/run.1234"

CTXT="talk_cramped"


# Plot the data
# for EXPT in "0" "1" "2" "3" "4" "7"; do
#   python3 plot/trajs.py\
#     --focus $BASETARGET"."$EXPT".traj.csv"\
#     --compartments s i\
#     --dpi 200 --width 320 --context $CTXT\
#     --out "_data/plot/sir/run.1234."$EXPT".trajs."$CTXT".png"

#   python3 plot/cases.py\
#     --focus $BASETARGET"."$EXPT".data.prevalence.csv"\
#     --dpi 200 --width 320 --context $CTXT\
#     --cases-column prevalence\
#     --rows\
#     --out "_data/plot/sir/run.1234."$EXPT".prevalence."$CTXT".png"
# done

# python3 plot/cases.py\
#   --focus $BASETARGET."3.data.prevalence.csv"\
#   --cases-column prevalence\
#   --range 0. 900.\
#   --rows\
#   --dpi 200 --width 320 --context $CTXT\
#   --out "_data/plot/sir/run.1234.3.prevalence."$CTXT".png"

# python3 plot/cases.py\
#   --focus $BASETARGET".3.data.prevalence.csv"\
#   --glob-label "Augmentation"\
#   --glob "" "_data/mcmc/sir/run.1234.3.mcmc.444.custom.exact/run.iter.*.data.prevalence.csv"\
#   --cases-column "prevalence"\
#   --range 0. 900.\
#   --rows\
#   --alpha 0.1 --dpi 200 --width 320 --context $CTXT\
#   --out $BASEDEST".3.mcmc.short.prm_exact.prevalence."$CTXT".png"

# python3 plot/trajs.py\
#   --focus $BASETARGET".0.traj.csv"\
#   --focus $BASETARGET".1.traj.csv"\
#   --focus $BASETARGET".2.traj.csv"\
#   --focus $BASETARGET".3.traj.csv"\
#   --focus $BASETARGET".4.traj.csv"\
#   --compartments s i\
#   --dpi 200 --width 320 --context $CTXT\
#   --out "_data/plot/sir/run.1234."$CTXT."trajs.png"


# Plot particles comparison
# for EXPT in "0" "1" "2" "3" "4"; do
#   python3 plot/params.py\
#     --target $BASETARGET"."$EXPT".par.csv"\
#     --read-template "_data/pmcmc/sir/run.1234."$EXPT".pmcmc.negbin.{}.20201210.theta.csv"\
#     --read-label "Particles"\
#     --read "N = 200" "200"\
#     --read "N = 400" "400"\
#     --read "N = 600" "600"\
#     --parameters logprior loglik beta\
#     --burn 10000\
#     --alpha 0.05 --dpi 200 --width 320 --context $CTXT\
#     --out "_data/plot/pmcmc/sir/run.1234."$EXPT".negbin.20201210.compare_particles.theta.{}."$CTXT".png"
# done


# Compare Exact / Fintzi
# for EXPT in "0" "1" "2" "3" "4"; do
#   # Thin version for params
#   python3 scripts/thin.py\
#     --burn 100000\
#     --multiples 400 0\
#     --in "_data/mcmc/sir/run.1234."$EXPT".mcmc.333.custom.exact/run.iter.theta.csv"\
#     --out "_data/mcmc/sir/run.1234."$EXPT".mcmc.333.custom.exact/run.thin.theta.csv"

#   python3 scripts/thin.py\
#     --burn 100000\
#     --multiples 1000 0\
#     --in "_data/mcmc/sir/run.1234."$EXPT".mcmc.444.custom.exact/run.iter.theta.csv"\
#     --out "_data/mcmc/sir/run.1234."$EXPT".mcmc.444.custom.exact/run.thin.theta.csv"

#   python3 scripts/thin.py\
#     --burn 1000\
#     --multiples 4 0\
#     --in "_data/fintzi/run.1234."$EXPT".fintzi.20201210.theta.csv"\
#     --out "_data/fintzi/run.1234."$EXPT".fintzi.20201210.thin.theta.csv"

#   python3 plot/params.py\
#     --target $BASETARGET"."$EXPT".par.csv"\
#     --read-template "_data/{}.thin.theta.csv"\
#     --read-label "Augmentation"\
#     --read "PRM" "mcmc/sir/run.1234."$EXPT".mcmc.444.custom.exact/run"\
#     --parameters beta ps0 pi0\
#     --rename beta "\$\beta\$"\
#     --rename ps0 "\$pS_0\$"\
#     --rename pi0 "\$pI_0\$"\
#     --no-share-k\
#     --alpha 0.05 --dpi 200 --width 320 --latex --context $CTXT\
#     --out $BASEDEST"."$EXPT".mcmc.short.prm_exact.theta.{}."$CTXT".png"

#   python3 plot/params.py\
#     --target $BASETARGET"."$EXPT".par.csv"\
#     --read-template "_data/{}.thin.theta.csv"\
#     --read-label "Augmentation"\
#     --read "PRM" "mcmc/sir/run.1234."$EXPT".mcmc.444.custom.exact/run"\
#     --parameters beta\
#     --rename beta "\$\beta\$"\
#     --rename ps0 "\$pS_0\$"\
#     --rename pi0 "\$pI_0\$"\
#     --split-params\
#     --no-share-k\
#     --no-trace --no-violin\
#     --alpha 0.05 --dpi 200 --width 320 --latex --context $CTXT\
#     --out $BASEDEST"."$EXPT".mcmc.short.prm_exact.beta.{}."$CTXT".png"

#   # Plot the comparison
#   python3 plot/params.py\
#     --target $BASETARGET"."$EXPT".par.csv"\
#     --read-template "_data/{}.thin.theta.csv"\
#     --read-label "Augmentation"\
#     --read "PRM" "mcmc/sir/run.1234."$EXPT".mcmc.333.custom.exact/run"\
#     --read "Subject-level" "fintzi/run.1234."$EXPT".fintzi.20201210"\
#     --parameters beta ps0 pi0\
#     --rename beta "\$\beta\$"\
#     --rename ps0 "\$pS_0\$"\
#     --rename pi0 "\$pI_0\$"\
#     --no-share-k\
#     --alpha 0.05 --dpi 200 --width 320 --latex --context $CTXT\
#     --out $BASEDEST"."$EXPT".mcmc.short.compare_prm_fintzi.theta.{}."$CTXT".png"

#   # Plot the trajs
#   python3 plot/trajs.py\
#     --focus $BASETARGET"."$EXPT".traj.csv"\
#     --glob-label "Augmentation"\
#     --glob "Subject-level" "_data/fintzi/run.1234."$EXPT".fintzi.20201210.trajs.csv"\
#     --concat\
#     --compartments s i\
#     --alpha 0.1 --dpi 200 --width 320 --latex --context $CTXT\
#     --out $BASEDEST"."$EXPT".mcmc.short.fintzi.trajs."$CTXT".png"

#   python3 plot/trajs.py\
#     --focus $BASETARGET"."$EXPT".traj.csv"\
#     --glob-label "Augmentation"\
#     --glob "PRM" "_data/mcmc/sir/run.1234."$EXPT".mcmc.444.custom.exact/run.iter.*.traj.csv"\
#     --compartments s i\
#     --alpha 0.1 --dpi 200 --width 320 --context $CTXT\
#     --out $BASEDEST"."$EXPT".mcmc.short.prm_exact.trajs."$CTXT".png"

#   for F in _data/mcmc/sir/run.1234."$EXPT".mcmc.444.custom.exact/run.iter.*.traj.csv; do
#     dune exec epi-gen-data --\
#       --no-cases --no-random --prev --rho 0.9\
#       $F
#   done

#   python3 plot/cases.py\
#     --focus $BASETARGET"."$EXPT".data.prevalence.csv"\
#     --glob-label "Augmentation"\
#     --glob "PRM" "_data/mcmc/sir/run.1234."$EXPT".mcmc.444.custom.exact/run.iter.*.data.prevalence.csv"\
#     --cases-column "prevalence"\
#     --rows\
#     --alpha 0.1 --dpi 200 --width 320 --context $CTXT\
#     --out $BASEDEST"."$EXPT".mcmc.short.prm_exact.prevalence."$CTXT".png"

#   # Compute and output the ESS
#   python3 plot/ess.py\
#     --read "PRM" "_data/mcmc/sir/run.1234."$EXPT".mcmc.333.custom.exact/run.iter.theta.csv"\
#     --parameters beta ps0 pi0 pr0\
#     --out-template "_data/mcmc/sir/run.1234."$EXPT".mcmc.333.custom.exact/run.ess.csv"

#   python3 plot/ess.py\
#     --read-template "_data/fintzi/run.1234."$EXPT".fintzi.20201210.theta.csv"\
#     --read "Subject-level" ""\
#     --burn 1000\
#     --parameters beta ps0 pi0 pr0
# done


## Plot benchmark
python3 plot/bench.py\
  --read-template "_data/{label}"\
  --staged-template\
  --read-legend-title "Augmentation"\
  --objective-column min\
  --xaxis-label "Population size"\
  --yaxis-label "\$s/{mESS}\$"\
  --xaxis-values 0 1 2 3 4\
  --map-x 0 500\
  --map-x 1 1000\
  --map-x 2 2000\
  --map-x 3 4000\
  --map-x 4 8000\
  --xaxis-log-base 2 --xaxis-plain-format\
  --power-factor 10\
  --read "PRM" "mcmc/sir/run.1234.{x}.mcmc.333.custom.exact/run.{measure}.csv"\
  --read "Subject-level" "fintzi/run.1234.{x}.fintzi.20201210.{measure}.csv"\
  --dpi 200 --width 320 --latex --context paper_cramped\
  --out $BASEDEST".expts.mcmc.compare_prm_fintzi.bench.duration_mess."$CTXT".png"

python3 plot/bench.py\
  --read-template "_data/{label}"\
  --staged-template\
  --read-legend-title "Augmentation"\
  --objective niter --objective-column niter\
  --xaxis-label "Population size"\
  --yaxis-label "\$s/{iter}\$"\
  --xaxis-values 0 1 2 3 4\
  --map-x 0 500\
  --map-x 1 1000\
  --map-x 2 2000\
  --map-x 3 4000\
  --map-x 4 8000\
  --power-factor 2e-2\
  --xaxis-log-base 2 --xaxis-plain-format\
  --read "PRM" "mcmc/sir/run.1234.{x}.mcmc.333.custom.exact/run.{measure}.csv"\
  --read "Subject-level" "fintzi/run.1234.{x}.fintzi.20201210.{measure}.csv"\
  --dpi 200 --width 320 --latex --context paper_cramped\
  --out $BASEDEST".expts.mcmc.compare_prm_fintzi.bench.duration_niter."$CTXT".png"

python3 plot/bench.py\
  --read-template "_data/{label}"\
  --staged-template\
  --read-legend-title "Augmentation"\
  --cost niter --cost-column niter\
  --objective-column min\
  --xaxis-label "Population size"\
  --yaxis-label "\${iter}/{mESS}\$"\
  --xaxis-values 0 1 2 3 4\
  --map-x 0 500\
  --map-x 1 1000\
  --map-x 2 2000\
  --map-x 3 4000\
  --map-x 4 8000\
  --xaxis-log-base 2 --xaxis-plain-format\
  --power-exponents "0" "1"\
  --power-factor 1e3\
  --read "PRM" "mcmc/sir/run.1234.{x}.mcmc.333.custom.exact/run.{measure}.csv"\
  --read "Subject-level" "fintzi/run.1234.{x}.fintzi.20201210.{measure}.csv"\
  --dpi 200 --width 320 --latex --context paper_cramped\
  --out $BASEDEST".expts.mcmc.compare_prm_fintzi.bench.niter_mess."$CTXT".png"


# # Theta Approx / PMCMC
# for EXPT in "0" "1" "2" "3" "4"; do
#   # With negbin
#   J="200"

#   # Plot short one
#   python3 plot/params.py\
#     --target $BASETARGET"."$EXPT".par.csv"\
#     --read-template "_data/{}.theta.csv"\
#     --read-label "Method"\
#     --read "PRM-augmentation" "mcmc/sir/run.1234."$EXPT".mcmc.333.custom.approx/run.iter"\
#     --read "PMCMC" "pmcmc/sir/run.1234."$EXPT".pmcmc.negbin."$J".20201210"\
#     --parameters beta\
#     --rename beta "\$\beta\$"\
#     --burn 10000\
#     --alpha 0.05 --dpi 200 --width 320 --context $CTXT\
#     --out $BASEDEST"."$EXPT".mcmc.short.compare_prm_pmcmc.theta.{}."$CTXT".png"

#   # Trajs for approx
#   python3 plot/trajs.py\
#     --focus $BASETARGET"."$EXPT".traj.csv"\
#     --glob-label "Method"\
#     --glob "PRM-augmentation" "_data/mcmc/sir/run.1234."$EXPT".mcmc.333.custom.approx/run.iter.*.traj.csv"\
#     --compartments s i\
#     --alpha 0.1 --dpi 200 --width 320 --latex --context $CTXT\
#     --out $BASEDEST"."$EXPT".mcmc.short.prm_approx.trajs."$CTXT".png"
# done


# # ESS Approx / PMCMC
# for EXPT in "0" "1" "2" "3" "4"; do
#   # Compute ESS and output
#   python3 plot/ess.py\
#     --read "PRM-augmentation" "_data/mcmc/sir/run.1234."$EXPT".mcmc.333.custom.approx/run.iter.theta.csv"\
#     --parameters beta\
#     --out-template "_data/mcmc/sir/run.1234."$EXPT".mcmc.333.custom.approx/run.ess.csv"

#   J="200"
#   python3 plot/ess.py\
#     --read-template "_data/pmcmc/sir/run.1234."$EXPT".pmcmc.negbin."$J".20201210.theta.csv"\
#     --read "PMCMC" ""\
#     --burn 10000\
#     --parameters beta
# done


# Benchmark Approx / PMCMC

J="200"
python3 plot/bench.py\
  --read-template "_data/{label}"\
  --staged-template\
  --read-legend-title "Method"\
  --objective-column multi\
  --xaxis-label "Population size"\
  --yaxis-label "\$s/{mESS}\$"\
  --xaxis-values 0 1 2 3 4\
  --map-x 0 500\
  --map-x 1 1000\
  --map-x 2 2000\
  --map-x 3 4000\
  --map-x 4 8000\
  --xaxis-log-base 2 --xaxis-plain-format\
  --power-exponents "0" "1"\
  --power-factor 5\
  --read "PRM-augmentation" "mcmc/sir/run.1234.{x}.mcmc.333.custom.approx/run.{measure}.csv"\
  --read "PMCMC" "pmcmc/sir/run.1234.{x}.pmcmc.negbin."$J".20201210.{measure}.csv"\
  --dpi 200 --width 320 --latex --context paper_cramped\
  --out $BASEDEST".expts.mcmc.compare_prm_pmcmc."$J".bench.duration_mess."$CTXT".png"

python3 plot/bench.py\
  --read-template "_data/{label}"\
  --staged-template\
  --read-legend-title "Method"\
  --objective niter --objective-column niter\
  --xaxis-label "Population size"\
  --yaxis-label "\$s/{iter}\$"\
  --xaxis-values 0 1 2 3 4\
  --map-x 0 500\
  --map-x 1 1000\
  --map-x 2 2000\
  --map-x 3 4000\
  --map-x 4 8000\
  --xaxis-log-base 2 --xaxis-plain-format\
  --power-exponents "0" "1"\
  --power-factor 1e-2\
  --read "PRM-augmentation" "mcmc/sir/run.1234.{x}.mcmc.333.custom.approx/run.{measure}.csv"\
  --read "PMCMC" "pmcmc/sir/run.1234.{x}.pmcmc.negbin."$J".20201210.{measure}.csv"\
  --dpi 200 --width 320 --latex --context paper_cramped\
  --out $BASEDEST".expts.mcmc.compare_prm_pmcmc."$J".bench.duration_niter."$CTXT".png"

python3 plot/bench.py\
  --read-template "_data/{label}"\
  --staged-template\
  --read-legend-title "Method"\
  --cost niter --cost-column niter\
  --objective-column multi\
  --xaxis-label "Population size"\
  --yaxis-label "\${iter}/{mESS}\$"\
  --xaxis-values 0 1 2 3 4\
  --map-x 0 500\
  --map-x 1 1000\
  --map-x 2 2000\
  --map-x 3 4000\
  --map-x 4 8000\
  --xaxis-log-base 2 --xaxis-plain-format\
  --power-exponents "0" "1"\
  --power-factor 5e2\
  --read "PRM-augmentation" "mcmc/sir/run.1234.{x}.mcmc.333.custom.approx/run.{measure}.csv"\
  --read "PMCMC" "pmcmc/sir/run.1234.{x}.pmcmc.negbin."$J".20201210.{measure}.csv"\
  --dpi 200 --width 320 --latex --context paper_cramped\
  --out $BASEDEST".expts.mcmc.compare_prm_pmcmc."$J".bench.niter_mess."$CTXT".png"


# Compare Exact / Approx
python3 plot/bench.py\
  --read-template "_data/mcmc/sir/run.1234.{x}.mcmc.333.custom.{label}/run.{measure}.csv"\
  --read-legend-title "Simulations"\
  --objective niter --objective-column niter\
  --xaxis-label "Population size"\
  --yaxis-label "\$s/{iter}\$"\
  --xaxis-values 0 1 2 3 4\
  --map-x 0 500\
  --map-x 1 1000\
  --map-x 2 2000\
  --map-x 3 4000\
  --map-x 4 8000\
  --xaxis-log-base 2 --xaxis-plain-format\
  --power-exponents "0" "1"\
  --power-factor 5e-3\
  --read "Exact" "exact"\
  --read "Approximate" "approx"\
  --dpi 200 --width 320 --latex --context paper_cramped\
  --out $BASEDEST".expts.mcmc.compare_exact_approx.bench.duration_niter."$CTXT".png"


# # Evaluate inference with expt 7

# # Theta 7
# python3 plot/params.py\
#   --target "_data/sir/run.12345.7.par.csv"\
#   --read-template "_data/{}.theta.csv"\
#   --read-label "Method"\
#   --read "PRM-augmentation" "mcmc/sir/run.12345.7.mcmc.333.custom.approx/run.iter"\
#   --parameters beta\
#   --alpha 0.05 --dpi 200 --width 320 --context paper_cramped\
#   --out "_data/plot/mcmc/sir/run.12345.7.mcmc.333.custom.approx.theta.{}.paper_cramped.png"

# # ESS 7
# python3 plot/ess.py\
#   --read "PRM-augmentation" "_data/mcmc/sir/run.12345.7.mcmc.333.custom.approx/run.iter.theta.csv"\
#   --parameters beta\
#   --out-template "_data/mcmc/sir/run.12345.7.mcmc.333.custom.approx/run.ess.csv"

# # "benchmark" 7
# python3 plot/bench.py\
#   --read-template "_data/{label}"\
#   --staged-template\
#   --read-legend-title "Method"\
#   --objective-column multi\
#   --xaxis-label "Population size"\
#   --yaxis-label "\$s/{mESS}\$"\
#   --xaxis-values 7\
#   --map-x 7 64000\
#   --xaxis-log-base 2 --xaxis-plain-format\
#   --power-exponents "1"\
#   --power-factor 1e-3\
#   --read "PRM-augmentation" "mcmc/sir/run.12345.{x}.mcmc.333.custom.approx/run.{measure}.csv"\
#   --dpi 200 --width 320 --latex --context paper_cramped\
#   --out "_data/plot/mcmc/sir/run.12345.7.mcmc.prm.bench.duration_mess."$CTXT".png"

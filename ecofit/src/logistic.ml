module T = Ecofit.Logistic.Term
open Cmdliner

let () = Term.exit @@ Term.eval (T.posterior, T.info)

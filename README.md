
OCAML-ECOEVO
============

Simulations and inference for ecology and evolution.

Includes libraries:
- sim : Simulation of ODEs (odepack) and CTMJPs
- fit : Bayesian inference
- pop : Simulate population of individuals with evolving trait
- tree : Binary timed trees
- seqs : DNA / aa sequences
- seqsim : Simulate nucleotide evolution
- eco : Simulate some ecological systems
- epi : Simulate S(E)IR(S) systems
- epifit : Inference for S(E)IR(S) systems

Disclaimer : The interface is unstable.


Installation
------------

The system dependencies that must be satisfied are a Fortran compiler,
BLAS, LAPACK, Cairo, an OCaml compiler, and opam (the OCaml package manager).
For ubuntu/debian, this corresponds to the packages
`gfortran`, `m4`, `libblas-dev`, `liblapack-dev` and `libcairo2-dev`,
`ocaml`, and `opam`, and you could install them with
```
> sudo apt-get install\
    gfortran\
    m4\
    libblas-dev\
    liblapack-dev\
    libcairo2-dev\
    ocaml\
    opam
```

The simplest way to install the ecoevo package is then
```
> opam switch create 4.08.1+flambda
> opam pin add ecoevo git+https://gitlab.com/bnguyenvanyen/ocamlecoevo.git
```

The project is built with dune, so instead you can also build with
```
> opam switch create 4.08.1+flambda
> git clone https://gitlab.com/bnguyenvanyen/ocamlecoevo.git
> cd ocamlecoevo
> opam install . --deps-only
> dune build
```

To run tests
```
> dune runtest
```

To install
```
> dune install
```

A Docker image with the package installed is available on the Docker Hub,
as `bnguyenvanyen/ocamlecoevo`.


Documentation
-------------

The API can be built locally with
```
> dune build @doc
```

You can also take a look at the .mli interface files directly.


%start <(int * Tree.t option) option> nexus

%%

nexus:
  | EOF
      { None }
  | line ; n = nexus
      { n }
  | WTREE ; WSTATE ; UNDER ; k = NUMBER ; EQUAL ; t = tree
      { Some (int_of_string k, t) }
  ;

line:
  | any ; line
  | NEWLINE
      {}
  ;


(* as a list of things ? *)
any:
  | anyword
  | anysign
      {}
  ;


anysign:
  | UNDER
  | SLASH
  | SEMIC
  | RSQUARE
  | RPAREN
  | RCURLY
  | LSQUARE
  | LPAREN
  | LCURLY
  | EQUAL
  | DIESE
  | COMMA
  | COLON
  | BAR
  | SIGN
      {}
  ;

anyword:
  | WNEXUS
  | WBEGIN
  | WEND
  | WSTATE
  | WORD
  | NUMBER
      {}
  ;

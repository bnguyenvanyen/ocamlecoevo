{
  module T = Tokens
}

let white = [' ' '\t']+
let newline = '\r' | '\n' | "\r\n"

let lower = ['a'-'z']
let upper = ['A'-'Z']
let digit = ['0'-'9']
let letter = lower | upper
let sign = ['#' ':' ',' '.' '-']
let singular = [^'s']
let unused_sign = ['<' '>' '-' '@' '+' '*']

let number = digit+
let exp = 'E' ('+' | '-') digit+
let time = digit+ '.'? digit* exp*
let word = (upper? lower+) | (upper+)
let other = digit | letter | unused_sign | white | [';']
let state = other* ':' time

let wnexus = "#NEXUS"
let wstate = "STATE"
let wtree = "tree" singular
let wend = "End"
let wbeg = "Begin"


rule read =
  parse
  | white   { read lexbuf }
  | newline { Lexing.new_line lexbuf ; T.NEWLINE }
  | wstate  { T.WSTATE }
  | wnexus  { T.WNEXUS }
  | wtree   { T.WTREE }
  | wend    { T.WEND }
  | wbeg    { T.WBEGIN }
  | '_'     { T.UNDER }
  | '/'     { T.SLASH }
  | ';'     { T.SEMIC }
  | ']'     { T.RSQUARE }
  | ')'     { T.RPAREN }
  | '}'     { T.RCURLY }
  | '['     { T.LSQUARE }
  | '('     { T.LPAREN }
  | '{'     { T.LCURLY }
  | eof     { T.EOF }
  | '='     { T.EQUAL }
  | '#'     { T.DIESE }
  | ','     { T.COMMA }
  | ':'     { T.COLON }
  | '|'     { T.BAR }
  | word    { T.WORD (Lexing.lexeme lexbuf) }
  | sign    { T.SIGN (Lexing.lexeme lexbuf) }
  | number  { T.NUMBER (Lexing.lexeme lexbuf) }
  | eof     { T.EOF }


(* get demographic history from Bayesian Skyline (Constant) run *)
module T = Tree.T
module L = BatList


(* does not work and I don't know why *)
let demography groups tree =
  let rec f_decr c times =
    function
    | [] ->
        c
    | (k, x) :: kxs ->
        let _, times' = L.split_at k times in
        let t = L.hd times' in
        let c' = Jump.cons (t, x) c in
        f_decr c' times' kxs
  in
  let unsorted_times = Treelog.binode_states (T.basic tree) in
  let coal_times = L.sort (fun t t' -> compare t' t) unsorted_times in
  let event_times = 0. :: coal_times in
  (* Check things make sense *)
  let n_intervals = List.fold_left (fun n (k, _) -> n + k) 0 groups in
  let n_coals = List.length unsorted_times in
  assert (n_intervals = n_coals) ;
  let traj = f_decr Jump.empty event_times groups in
  traj


let burn ~n logchan treechan =
  Log.burn ~n logchan ;
  Treelog.burn ~n treechan


let next_demography ~ngroups logchan treechan =
  let sample = Log.next ~ngroups logchan in
  (* let height = sample.Log.treeHeight in *)
  let k, tree = Treelog.next treechan in
  Printf.eprintf "k %i\n" k ;
  (*
  Printf.eprintf "log height %f\n" height ;
  Printf.eprintf "treelog height %f\n" (T.root_value tree) ;
  *)
  assert (sample.Log.k = k) ;
  (k, demography sample.Log.groups tree)


let times ~t0 ~dt ~tf =
  let un t =
    let t' = t +. dt in
    if t' > tf then
      None
    else
      Some (t', t')
  in L.unfold t0 un


let output_header ~times chan =
  output_string chan "k" ;
  L.iter (Printf.fprintf chan ",t%f") times ;
  output_string chan "\n"


let output_row ~times chan k c =
  (output_string chan) @@ string_of_int @@ k ;
  L.iter (fun t ->
    let x = Jump.eval c t in
    Printf.fprintf chan ",%f" x
  ) times ;
  output_string chan "\n"


let write ~t0 ~dt ~tf ~prefix ~nburn =
  let logchan = Log.open_in (prefix ^ ".log") in
  let treechan = Treelog.open_in (prefix ^ ".trees") in
  let bspchan = open_out (prefix ^ ".bsp.csv") in
  burn ~n:nburn logchan treechan ;
  let ngroups = Log.ngroups logchan in
  let times = times ~t0 ~dt ~tf in
  output_header ~times bspchan ;
  let rec f () =
    match next_demography ~ngroups logchan treechan with
    | exception End_of_file ->
        ()
    | (k, c) ->
        output_row ~times bspchan k c ;
        (* failwith "Stop after 1" *)
        f ()
  in f () ;
  Log.close_in logchan ;
  Treelog.close_in treechan ;
  close_out bspchan

let t0_r = ref 0.
let dt_r = ref 1.
let tf_r = ref infinity
let prefix_r = ref None
let nburn_r = ref 0

let specl = [
  ("-t0", Arg.Set_float t0_r,
   "Time to start from.") ;
  ("-dt", Arg.Set_float dt_r,
   "Time resolution.") ;
  ("-tf", Arg.Set_float tf_r,
   "Time to stop at.") ;
  ("-prefix", Arg.String (fun s -> prefix_r := Some s),
   "Prefix for input/output.") ;
  ("-nburn", Arg.Set_int nburn_r,
   "Number of samples to ignore at the start.") ;
]

let anon_fun s = Printf.printf "Ignored anonymous argument : %s" s

let usage_msg = " Read Bayesian Skyline BEAST output and output it in a CSV file"


let main () =
  begin
    Arg.parse specl anon_fun usage_msg ;
    let t0 = !t0_r in
    let dt = !dt_r in
    let tf = !tf_r in
    let prefix =
      match !prefix_r with
      | None ->
          raise (Arg.Bad "must give a prefix")
      | Some s ->
          s
    in
    let nburn = !nburn_r in
    Tracer.Bsp.write ~t0 ~dt ~tf ~prefix ~nburn
  end;;

main ()

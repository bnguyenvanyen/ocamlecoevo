#!/usr/bin/bash

NAME="run.seq.ten.rand.mut.3333"
RUN="mcmc.phylo.fixed_mu_nu"
BASESRC="_data/sir/"$NAME
TARGET=$BASESRC".par.csv"
BASEDEST="_data/mcmc/sir/"$NAME
BASEOUT="_data/plot/mcmc/sir/"$NAME

BURN="1000000"
BETA_RANGE="beta 0. 200."
S0_RANGE="s0 0. 100000."
I0_RANGE="i0 0. 30."
ALPHA="0.1"
ALPHA2="0.2"
DPI="200."
WIDTH="300."
CTXT="paper_cramped"


# Plot the cases
python3 plot/cases.py\
  --focus $BASESRC".data.cases.csv"\
  --range 0. 300.\
  --dpi $DPI --width $WIDTH --latex --context $CTXT\
  --out "_data/plot/sir/"$NAME".cases."$CTXT".png"

# Plot the phylogeny
python3 plot/newick.py\
  --times $BASESRC".data.sequences.fasta"\
  --target $BASESRC".data.trees.newick"\
  --margin 0.1\
  --out "_data/plot/sir/"$NAME".tree.png"


# # isolated sources
# for SEED in "111" "222"; do
#   ## cases
#   python3 plot/params.py\
#     --target $TARGET\
#     --read-template $BASEDEST"."$RUN".{}."$SEED"/run.theta.csv"\
#     --read-label "Source"\
#     --read "Cases" "cases"\
#     --parameters beta s0\
#     --burn $BURN\
#     --split-params\
#     --rename "beta" "$\beta$"\
#     --rename "s0" "\$S(0)\$"\
#     --range $BETA_RANGE\
#     --range $S0_RANGE\
#     --colors 3 0\
#     --alpha $ALPHA2  --dpi $DPI  --width 160  --latex --context $CTXT\
#     --out $BASEOUT"."$RUN".cases."$SEED".beta_s0.{}."$CTXT".png"

#   ## sero
#   python3 plot/params.py\
#     --target $TARGET\
#     --read-template $BASEDEST"."$RUN".{}."$SEED"/run.theta.csv"\
#     --read-label "Source"\
#     --read "Sero" "sero"\
#     --parameters beta s0\
#     --burn $BURN\
#     --split-params\
#     --rename "beta" "$\beta$"\
#     --rename "s0" "\$S(0)\$"\
#     --range $BETA_RANGE\
#     --range $S0_RANGE\
#     --colors 3 1\
#     --alpha $ALPHA2  --dpi $DPI  --width 160  --latex  --context $CTXT\
#     --out $BASEOUT"."$RUN".sero."$SEED".beta_s0.{}."$CTXT".png"

#   ## sequences
#   python3 plot/params.py\
#     --target $TARGET\
#     --read-template $BASEDEST"."$RUN".{}."$SEED"/run.theta.csv"\
#     --read-label "Source"\
#     --read "Sequences" "sequences"\
#     --parameters beta s0\
#     --burn $BURN\
#     --split-params\
#     --rename "beta" "$\beta$"\
#     --rename "s0" "\$S(0)\$"\
#     --range $BETA_RANGE\
#     --range $S0_RANGE\
#     --colors 3 2\
#     --alpha $ALPHA2  --dpi $DPI  --width 160  --latex  --context $CTXT\
#     --out $BASEOUT"."$RUN".sequences."$SEED".beta_s0.{}."$CTXT".png"

#   ## tree
#   python3 plot/params.py\
#     --target $TARGET\
#     --read-template $BASEDEST"."$RUN".{}."$SEED"/run.theta.csv"\
#     --read-label "Source"\
#     --read "Tree" "tree"\
#     --parameters beta s0\
#     --burn $BURN\
#     --split-params\
#     --rename "beta" "$\beta$"\
#     --rename "s0" "\$S(0)\$"\
#     --range $BETA_RANGE\
#     --range $S0_RANGE\
#     --alpha $ALPHA2  --dpi $DPI  --width 160  --latex  --context $CTXT\
#     --out $BASEOUT"."$RUN".tree."$SEED".beta_s0.{}."$CTXT".png"

#   ## case with more params
#   python3 plot/params.py\
#     --target $TARGET\
#     --read-template $BASEDEST"."$RUN".{}."$SEED"/run.theta.csv"\
#     --read-label "Source"\
#     --read "Cases" "cases"\
#     --parameters beta rho s0 i0\
#     --burn $BURN\
#     --triangular\
#     --rename "beta" "$\beta$"\
#     --rename "rho" "$\rho$"\
#     --rename "s0" "\$S(0)\$"\
#     --rename "i0" "\$I(0)\$"\
#     --range $BETA_RANGE\
#     --range $I0_RANGE\
#     --alpha $ALPHA  --dpi $DPI  --width $WIDTH  --latex --context $CTXT\
#     --out $BASEOUT"."$RUN".cases."$SEED".theta.{}."$CTXT".png"
# done


# joint plots
for SEED in "111" "222"; do
# for SEED in "111"; do
  for SRC in "cases" "sero" "cases_sero" "tree" "cases_tree" "sero_tree" "cases_sero_tree"; do
    python3 scripts/thin.py\
      --burn $BURN\
      --in $BASEDEST"."$RUN"."$SRC"."$SEED"/run.theta.csv"\
      --out $BASEDEST"."$RUN"."$SRC"."$SEED"/run.burn.theta.csv"
  done

  for SRC in "sequences" "cases_sequences" "sero_sequences" "cases_sero_sequences"; do
    python3 scripts/thin.py\
      --burn 10000000\
      --multiples 20000 0\
      --in $BASEDEST"."$RUN"."$SRC"."$SEED"/run.theta.csv"\
      --out $BASEDEST"."$RUN"."$SRC"."$SEED"/run.burn.theta.csv"
  done

#   ## cases sero
#   python3 plot/params.py\
#     --target $TARGET\
#     --read-template $BASEDEST"."$RUN".{}."$SEED"/run.burn.theta.csv"\
#     --read-label "Source"\
#     --read "Cases" "cases"\
#     --read "Sero" "sero"\
#     --read "Both" "cases_sero"\
#     --parameters beta rho s0 i0\
#     --burn $BURN\
#     --triangular\
#     --rename "beta" "$\beta$"\
#     --rename "rho" "$\rho$"\
#     --rename "s0" "\$S(0)\$"\
#     --rename "i0" "\$I(0)\$"\
#     --range $BETA_RANGE\
#     --range $S0_RANGE\
#     --range $I0_RANGE\
#     --no-share-k\
#     --alpha $ALPHA  --dpi $DPI  --width $WIDTH  --latex  --context $CTXT\
#     --out $BASEOUT"."$RUN".compare_cases_sero."$SEED".theta.{}."$CTXT".png"

#   # cases sequences
#   python3 plot/params.py\
#     --target $TARGET\
#     --read-template $BASEDEST"."$RUN".{}."$SEED"/run.burn.theta.csv"\
#     --read-label "Source"\
#     --read "Sequences" "sequences"\
#     --read "Cases" "cases"\
#     --read "Both" "cases_sequences"\
#     --parameters beta rho s0 i0\
#     --burn $BURN\
#     --triangular\
#     --rename "beta" "$\beta$"\
#     --rename "rho" "$\rho$"\
#     --rename "s0" "\$S(0)\$"\
#     --rename "i0" "\$I(0)\$"\
#     --range $BETA_RANGE\
#     --range $S0_RANGE\
#     --range $I0_RANGE\
#     --no-share-k\
#     --alpha $ALPHA  --dpi $DPI  --width $WIDTH  --latex  --context $CTXT\
#     --out $BASEOUT"."$RUN".compare_cases_sequences."$SEED".theta.{}."$CTXT".png"

#   # sero sequences
#   python3 plot/params.py\
#     --target $TARGET\
#     --read-template $BASEDEST"."$RUN".{}."$SEED"/run.burn.theta.csv"\
#     --read-label "Source"\
#     --read "Sequences" "sequences"\
#     --read "Sero" "sero"\
#     --read "Both" "sero_sequences"\
#     --parameters beta rho s0 i0\
#     --burn $BURN\
#     --triangular\
#     --rename "beta" "$\beta$"\
#     --rename "rho" "$\rho$"\
#     --rename "s0" "\$S(0)\$"\
#     --rename "i0" "\$I_0\$"\
#     --range $BETA_RANGE\
#     --range $S0_RANGE\
#     --range $I0_RANGE\
#     --alpha $ALPHA  --dpi $DPI  --width $WIDTH  --latex  --context $CTXT\
#     --out $BASEOUT"."$RUN".compare_sero_sequences."$SEED".theta.{}."$CTXT".png"

#   # cases tree
#   python3 plot/params.py\
#     --target $TARGET\
#     --read-template $BASEDEST"."$RUN".{}."$SEED"/run.burn.theta.csv"\
#     --read-label "Source"\
#     --read "Tree" "tree"\
#     --read "Cases" "cases"\
#     --read "Both" "cases_tree"\
#     --parameters beta rho s0 i0\
#     --burn $BURN\
#     --triangular\
#     --rename "beta" "$\beta$"\
#     --rename "rho" "$\rho$"\
#     --rename "s0" "\$S(0)\$"\
#     --rename "i0" "\$I(0)\$"\
#     --range $BETA_RANGE\
#     --range $S0_RANGE\
#     --range $I0_RANGE\
#     --no-share-k\
#     --alpha $ALPHA  --dpi $DPI  --width $WIDTH  --latex  --context $CTXT\
#     --out $BASEOUT"."$RUN".compare_cases_tree."$SEED".theta.{}."$CTXT".png"

#   # cases sero sequences
#   python3 plot/params.py\
#     --target $TARGET\
#     --read-template $BASEDEST"."$RUN".{}."$SEED"/run.burn.theta.csv"\
#     --read-label "Source"\
#     --read "Cases+Sero+Sequences" "cases_sero_sequences"\
#     --parameters beta rho s0 i0\
#     --burn $BURN\
#     --triangular\
#     --rename "beta" "$\beta$"\
#     --rename "rho" "$\rho$"\
#     --rename "s0" "\$S(0)\$"\
#     --rename "i0" "\$I(0)\$"\
#     --range $BETA_RANGE\
#     --range $S0_RANGE\
#     --range $I0_RANGE\
#     --alpha $ALPHA  --dpi $DPI  --width $WIDTH  --latex  --context $CTXT\
#     --out $BASEOUT"."$RUN".cases_sero_sequences."$SEED".theta.{}."$CTXT".png"

#   # estimates
#   python3 plot/estimates.py\
#     --read-template $BASEDEST"."$RUN".{}."$SEED"/run.theta.csv"\
#     --read "Cases" "cases"\
#     --read "Sero" "sero"\
#     --read "Tree" "tree"\
#     --read "Sequences" "sequences"\
#     --read "Cases+Sero" "cases_sero"\
#     --read "Cases+Tree" "cases_tree"\
#     --read "Cases+Sequences" "cases_sequences"\
#     --read "Sero+Tree" "sero_tree"\
#     --read "Sero+Sequences" "sero_sequences"\
#     --read "Cases+Sero+Tree" "cases_sero_tree"\
#     --read "Cases+Sero+Sequences" "cases_sero_sequences"\
#     --parameters beta rho s0 i0\
#     --burn $BURN

  ## with peaks
  for SRC in "cases" "tree" "sequences"; do
    python3 scripts/peak.py\
      --column i\
      --in $BASEDEST"."$RUN"."$SRC"."$SEED"/run.traj.csv"\
      --out $BASEDEST"."$RUN"."$SRC"."$SEED"/run.peak.csv"

    python3 scripts/concat_columns.py\
      --in $BASEDEST"."$RUN"."$SRC"."$SEED"/run.burn.theta.csv"\
      --in $BASEDEST"."$RUN"."$SRC"."$SEED"/run.peak.csv"\
      --out $BASEDEST"."$RUN"."$SRC"."$SEED"/run.theta_peak.csv"
  done

  python3 plot/params.py\
    --target $TARGET\
    --read-template $BASEDEST"."$RUN".{}."$SEED"/run.theta_peak.csv"\
    --read-label "Source"\
    --read "Cases" "cases"\
    --read "Tree" "tree"\
    --read "Sequences" "sequences"\
    --parameters loglik-tree loglik-evo peak_time peak_magn\
    --burn $BURN\
    --triangular\
    --rename "beta" "$\beta$"\
    --rename "s0" "\$S(0)\$"\
    --rename "peak_time" "\$t_{argmax}\$"\
    --rename "peak_magn" "\$I_{max}\$"\
    --range $BETA_RANGE\
    --range $S0_RANGE\
    --alpha $ALPHA2  --dpi $DPI  --width $WIDTH  --context $CTXT\
    --out $BASEOUT"."$RUN".compare_cases_tree_sequences."$SEED".theta_peak.{}."$CTXT".png"

done


# # Amount and quality of phylogenetic data
# ## Dataset with lower mutation rate
# NAME2="run.seq.ten.rand.987"

# ## Dataset with less samples
# NAME3="run.seq.ten.rand.mut.3333.sub20"


# # for SEED in "111" "222"; do
# for SEED in "111"; do
#   for NM in $NAME $NAME2 $NAME3; do
#     for SRC in "sequences" "cases_sequences"; do
#       python3 scripts/thin.py\
#         --burn 2000000\
#         --multiples 20000 0\
#         --in "_data/mcmc/sir/"$NM"."$RUN"."$SRC"."$SEED"/run.theta.csv"\
#         --out "_data/mcmc/sir/"$NM"."$RUN"."$SRC"."$SEED"/run.thin.theta.csv"
#     done

#     for SRC in "tree" "cases_tree"; do
#       python3 scripts/thin.py\
#         --burn 400000\
#         --in "_data/mcmc/sir/"$NM"."$RUN"."$SRC"."$SEED"/run.theta.csv"\
#         --out "_data/mcmc/sir/"$NM"."$RUN"."$SRC"."$SEED"/run.thin.theta.csv"
#     done
#   done

#   python3 plot/params.py\
#     --target $TARGET\
#     --read-template $BASEDEST"."$RUN".{}."$SEED"/run.thin.theta.csv"\
#     --read-label "Source"\
#     --read "Cases+Sequences" "cases_sequences"\
#     --read "Cases+Tree" "cases_tree"\
#     --parameters beta i0\
#     --split-params\
#     --rename "beta" "\$\beta\$"\
#     --rename "i0" "\$I(0)\$"\
#     --range beta 65. 150.\
#     --range i0 0. 12.\
#     --violin-ncol 1\
#     --no-share-k\
#     --alpha $ALPHA2 --dpi $DPI --width 150 --latex --context $CTXT\
#     --out $BASEOUT"."$RUN".cases_compare_tree_sequences."$SEED".beta_i0.{}."$CTXT".png"

#   python3 plot/params.py\
#     --target $TARGET\
#     --read-template $BASEDEST"."$RUN".{}."$SEED"/run.thin.theta.csv"\
#     --read-label "Source"\
#     --read "Cases+Sequences" "cases_sequences"\
#     --read "Cases+Tree" "cases_tree"\
#     --parameters beta rho s0 i0\
#     --rename "beta" "\$\beta\$"\
#     --rename "rho" "$\rho$"\
#     --rename "s0" "\$S(0)\$"\
#     --rename "i0" "\$I(0)\$"\
#     --range $BETA_RANGE\
#     --range $S0_RANGE\
#     --no-share-k\
#     --alpha $ALPHA2 --dpi $DPI --width $WIDTH --latex --context $CTXT\
#     --out $BASEOUT"."$RUN".cases_compare_tree_sequences."$SEED".theta.{}."$CTXT".png"

#   python3 plot/params.py\
#     --target $TARGET\
#     --read-template "_data/mcmc/sir/"{}"."$RUN".cases_sequences."$SEED"/run.thin.theta.csv"\
#     --read-label "Substitution rate"\
#     --read "\$\mu = 2e-3\$" $NAME\
#     --read "\$\mu = 6e-4\$" $NAME2\
#     --parameters beta i0\
#     --split-params\
#     --rename "beta" "\$\beta\$"\
#     --rename "i0" "\$I(0)\$"\
#     --range beta 65. 150.\
#     --range i0 0. 12.\
#     --violin-ncol 1\
#     --no-share-k\
#     --alpha $ALPHA2 --dpi $DPI --width 150 --latex --context $CTXT\
#     --out "_data/plot/mcmc/sir/run.seq.ten.rand."$RUN".cases_sequences.compare_mus."$SEED".beta_i0.{}."$CTXT".png"

#   python3 plot/params.py\
#     --target $TARGET\
#     --read-template "_data/mcmc/sir/"{}"."$RUN".cases_sequences."$SEED"/run.thin.theta.csv"\
#     --read-label "Substitution rate"\
#     --read "\$\mu = 2e-3\$" $NAME\
#     --read "\$\mu = 6e-4\$" $NAME2\
#     --parameters beta rho s0 i0\
#     --rename "beta" "\$\beta\$"\
#     --rename "rho" "$\rho$"\
#     --rename "s0" "\$S(0)\$"\
#     --rename "i0" "\$I(0)\$"\
#     --range $BETA_RANGE\
#     --range $S0_RANGE\
#     --no-share-k\
#     --alpha $ALPHA2 --dpi $DPI --width $WIDTH --latex --context $CTXT\
#     --out "_data/plot/mcmc/sir/run.seq.ten.rand."$RUN".cases_sequences.compare_mus."$SEED".theta.{}."$CTXT".png"

#   python3 plot/params.py\
#     --target $TARGET\
#     --read-template "_data/mcmc/sir/"{}"."$RUN".cases_sequences."$SEED"/run.thin.theta.csv"\
#     --read-label "Sample size"\
#     --read "\$n = 51\$" $NAME\
#     --read "\$n = 20\$" $NAME3\
#     --parameters beta i0\
#     --split-params\
#     --rename "beta" "\$\beta\$"\
#     --rename "i0" "\$I(0)\$"\
#     --range beta 65. 150.\
#     --range i0 0. 12.\
#     --violin-ncol 1\
#     --no-share-k\
#     --alpha $ALPHA2 --dpi $DPI --width 150 --latex --context $CTXT\
#     --out "_data/plot/mcmc/sir/"$NAME"."$RUN".cases_sequences.compare_ns."$SEED".beta_i0.{}."$CTXT".png"

#   python3 plot/params.py\
#     --target $TARGET\
#     --read-template "_data/mcmc/sir/"{}"."$RUN".cases_sequences."$SEED"/run.thin.theta.csv"\
#     --read-label "Sample size"\
#     --read "\$n = 51\$" $NAME\
#     --read "\$n = 20\$" $NAME3\
#     --parameters beta rho s0 i0\
#     --rename "beta" "\$\beta\$"\
#     --rename "rho" "$\rho$"\
#     --rename "s0" "\$S(0)\$"\
#     --rename "i0" "\$I(0)\$"\
#     --range $BETA_RANGE\
#     --range $S0_RANGE\
#     --no-share-k\
#     --alpha $ALPHA2 --dpi $DPI --width $WIDTH --latex --context $CTXT\
#     --out "_data/plot/mcmc/sir/"$NAME"."$RUN".cases_sequences.compare_ns."$SEED".theta.{}."$CTXT".png"
# done


# # trajs and cases

# S_RANGE="s 0. 100000."
# I_RANGE="i 0. 7500."
# ASP=1.

# for SEED in "111" "222"; do
#    # cases tree
#    python3 plot/trajs.py\
#      --focus $BASESRC".traj.csv"\
#      --glob-label "Source"\
#      --glob-template $BASEDEST"."$RUN".{}."$SEED"/run.traj.csv"\
#      --glob "Tree" "tree"\
#      --glob "Cases" "cases"\
#      --glob "Both" "cases_tree"\
#      --concat\
#      --compartments s i\
#      --range $S_RANGE\
#      --range $I_RANGE\
#      --burn $BURN\
#      --alpha $ALPHA2\
#      --legend-bbox 1.1 1.\
#      --width $WIDTH --aspect $ASP\
#      --dpi $DPI \
#      --latex --context $CTXT\
#      --out $BASEOUT"."$RUN".compare_cases_tree."$SEED".trajs."$CTXT".png"

#    python3 plot/trajs.py\
#      --focus $BASESRC".traj.csv"\
#      --glob-label "Source"\
#      --glob-template $BASEDEST"."$RUN".{}."$SEED"/run.traj.csv"\
#      --glob "Sequences" "sequences"\
#      --glob "Cases" "cases"\
#      --glob "Both" "cases_sequences"\
#      --concat\
#      --compartments s i\
#      --range $S_RANGE\
#      --range $I_RANGE\
#      --burn $BURN\
#      --alpha $ALPHA\
#      --width $WIDTH --aspect $ASP\
#      --dpi $DPI\
#      --latex --context $CTXT\
#      --out $BASEOUT"."$RUN".compare_cases_sequences."$SEED".trajs."$CTXT".png"

#    python3 plot/trajs.py\
#      --focus $BASESRC".traj.csv"\
#      --glob-label "Source"\
#      --glob-template $BASEDEST"."$RUN".{}."$SEED"/run.traj.csv"\
#      --glob "Cases" "cases"\
#      --glob "Sero" "sero"\
#      --glob "Both" "cases_sero"\
#      --concat\
#      --compartments s i\
#      --range $S_RANGE\
#      --range $I_RANGE\
#      --burn $BURN\
#      --alpha $ALPHA2\
#      --legend-bbox 1.1 1\
#      --width $WIDTH --aspect $ASP\
#      --dpi $DPI\
#      --latex --context $CTXT\
#      --out $BASEOUT"."$RUN".compare_cases_sero."$SEED".trajs."$CTXT".png"

#    # cases
#    for SRC in "cases" "sero" "tree" "sequences" "cases_sero" "cases_tree" "cases_sequences" "sero_tree" "sero_sequences" ; do
#      dune exec epi-gen-data --\
#        --from reportr\
#        --concat\
#        $BASEDEST"."$RUN"."$SRC"."$SEED"/run.traj.csv"
#    done

#    python3 plot/cases.py\
#      --focus $BASESRC".data.cases.csv"\
#      --glob-label "Source"\
#      --glob-template $BASEDEST"."$RUN".{}."$SEED"/run.data.cases.csv"\
#      --glob "Cases" "cases"\
#      --range 0. 300.\
#      --concat\
#      --burn $BURN\
#      --rows\
#      --alpha $ALPHA2 --dpi $DPI --width $WIDTH --latex --context $CTXT\
#      --out $BASEOUT"."$RUN".cases."$SEED".cases."$CTXT".png"

#    python3 plot/cases.py\
#      --focus $BASESRC".data.cases.csv"\
#      --glob-label "Source"\
#      --glob-template $BASEDEST"."$RUN".{}."$SEED"/run.data.cases.csv"\
#      --glob "Tree" "tree"\
#      --glob "Cases" "cases"\
#      --glob "Both" "cases_tree"\
#      --concat\
#      --burn $BURN\
#      --rows\
#      --alpha $ALPHA2 --dpi $DPI --width $WIDTH --latex --context $CTXT\
#      --out $BASEOUT"."$RUN".compare_cases_tree."$SEED".cases."$CTXT".png"

#    python3 plot/cases.py\
#      --focus $BASESRC".data.cases.csv"\
#      --glob-label "Source"\
#      --glob-template $BASEDEST"."$RUN".{}."$SEED"/run.data.cases.csv"\
#      --glob "Sequences" "sequences"\
#      --glob "Cases" "cases"\
#      --glob "Both" "cases_sequences"\
#      --concat\
#      --burn $BURN\
#      --rows\
#      --alpha $ALPHA2 --dpi $DPI --width $WIDTH --latex --context $CTXT\
#      --out $BASEOUT"."$RUN".compare_cases_sequences."$SEED".cases."$CTXT".png"

#    python3 plot/cases.py\
#      --focus $BASESRC".data.cases.csv"\
#      --glob-label "Source"\
#      --glob-template $BASEDEST"."$RUN".{}."$SEED"/run.data.cases.csv"\
#      --glob "Cases" "cases"\
#      --glob "Sero" "sero"\
#      --glob "Both" "cases_sero"\
#      --concat\
#      --burn $BURN\
#      --rows\
#      --alpha $ALPHA2 --dpi $DPI --width $WIDTH --latex --context $CTXT\
#      --out $BASEOUT"."$RUN".compare_cases_sero."$SEED".cases."$CTXT".png"

#    # KC distance for sequences
# #    for SRC in "sequences" "cases_sequences" "sero_sequences"; do
# #      dune exec phylo-trace --\
# #        --path-target $BASESRC".data.trees.tnewick"\
# #        --path-sample $BASEDEST"."$RUN"."$SRC"."$SEED"/run.tree.tnewick"\
# #        $BASEDEST"."$RUN"."$SRC"."$SEED"/run.kc.csv"
# #    done
# done


# for SEED in "111"; do
#    python3 plot/trajs.py\
#      --focus $BASESRC".traj.csv"\
#      --glob-label "Source"\
#      --glob-template $BASEDEST"."$RUN".{}."$SEED"/run.traj.csv"\
#      --glob "Sequences" "sequences"\
#      --glob "Tree" "tree"\
#      --concat\
#      --compartments s i\
#      --range $S_RANGE\
#      --range $I_RANGE\
#      --burn $BURN\
#      --alpha $ALPHA2\
#      --legend-bbox 1.1 1\
#      --width $WIDTH --aspect $ASP\
#      --dpi $DPI\
#      --latex --context $CTXT\
#      --out $BASEOUT"."$RUN".compare_tree_sequences."$SEED".trajs."$CTXT".png"

#    python3 plot/trajs.py\
#      --focus $BASESRC".traj.csv"\
#      --glob-label "Source"\
#      --glob-template $BASEDEST"."$RUN".{}."$SEED"/run.traj.csv"\
#      --glob "Cases+Sequences" "cases_sequences"\
#      --glob "Cases+Tree" "cases_tree"\
#      --concat\
#      --compartments s i\
#      --range $S_RANGE\
#      --range $I_RANGE\
#      --burn $BURN\
#      --alpha $ALPHA2\
#      --legend-bbox 1.1 1\
#      --width $WIDTH --aspect $ASP\
#      --dpi $DPI\
#      --latex --context $CTXT\
#      --out $BASEOUT"."$RUN".cases_compare_tree_sequences."$SEED".trajs."$CTXT".png"

#   python3 plot/trajs.py\
#     --focus $BASESRC".traj.csv"\
#     --glob-label "Substitution rate"\
#     --glob-template "_data/mcmc/sir/"{}"."$RUN".cases_sequences."$SEED"/run.traj.csv"\
#     --glob "\$\mu = 2e-3\$" $NAME\
#     --glob "\$\mu = 6e-4\$" $NAME2\
#     --concat\
#     --compartments s i\
#     --range $S_RANGE\
#     --range $I_RANGE\
#     --burn $BURN\
#     --alpha $ALPHA2\
#     --legend-bbox 1.1 1\
#     --width $WIDTH --aspect $ASP\
#     --dpi $DPI\
#     --latex --context $CTXT\
#     --out "_data/plot/mcmc/sir/run.seq.ten.rand."$RUN".cases_sequences.compare_mus."$SEED".trajs."$CTXT".png"

#   python3 plot/trajs.py\
#     --focus $BASESRC".traj.csv"\
#     --glob-label "Sample size"\
#     --glob-template "_data/mcmc/sir/"{}"."$RUN".cases_sequences."$SEED"/run.traj.csv"\
#     --glob "\$n = 51\$" $NAME\
#     --glob "\$n = 20\$" $NAME3\
#     --concat\
#     --compartments s i\
#     --range $S_RANGE\
#     --range $I_RANGE\
#     --burn $BURN\
#     --alpha $ALPHA2\
#     --legend-bbox 1.1 1\
#     --width $WIDTH --aspect $ASP\
#     --dpi $DPI\
#     --latex --context $CTXT\
#     --out "_data/plot/mcmc/sir/"$NAME"."$RUN".cases_sequences.compare_ns."$SEED".trajs."$CTXT".png"
# done

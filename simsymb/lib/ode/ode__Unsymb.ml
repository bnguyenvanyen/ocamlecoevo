open Sim__Sig

module L = BatList

module Lac = Lacaml.D
module Vec = Lac.Vec

module F = Util.Float


let eval_auto : ('a -> 'a) t -> 


module Auto =
  struct
    module I = I.Auto

    module S = Symbolic.Make (I)

    (* a rate with some operations economised *)
    type 'a diff_vec =
      | Zero
      | Constant of Vec.t
      | V of (?z:Vec.t -> 'a -> Vec.t)


    let scale f e =
      match e with
      | Zero ->
          Zero
      | Constant v ->
          V (fun ?z v' ->
            let z = Lac.copy ?y:z v in
            Lacaml.D.scal (F.to_float (f v')) z ;
            z)
      | V f' ->
          V (fun ?z v ->
              let a = F.to_float (f v) in
              let z = f' ?z v in
              Lacaml.D.scal a z ;
              z)


    let add e e' =
      match e, e' with
      | Zero, any | any, Zero ->
          any
      | Constant v, Constant v' ->
          Constant (Vec.add v v')
      | V f, V f' ->
          (* Here we don't want to pass z to f and f' ! *)
          V (fun ?z v -> Vec.add ?z (f v) (f' v))
      | Constant v, V f | V f, Constant v ->
          (* Same here don't pass to f *)
          V (fun ?z v' -> Vec.add ?z v (f v'))

    let mul e e' =
      match e, e' with
      | Zero, _ | _, Zero ->
          Zero
      | Constant v, Constant v' ->
          Constant (Vec.mul v v')
      | V f, V f' ->
          (* Again here, don't pass z to f and f' -- FIXME why ? *)
          V (fun ?z v -> Vec.mul ?z (f v) (f' v))
      | Constant v, V f | V f, Constant v ->
          (* Same here *)
          V (fun ?z v' -> Vec.mul ?z v (f v'))

    let eval =
      function
      | Zero ->
          (fun ?z _ ->
             match z with
             | None ->
                 (* how do you know the dim ? :( *)
                 failwith "Don't know the dim"
             | Some z ->
                 Vec.iteri (fun i _ -> z.{i} <- 0.) z ; z
          )
          (* or (fun ~z _ -> Vec.make0 (Vec.dim z)) ? *)
      | Constant v ->
          (fun ?z _ -> Lac.copy ?y:z v)
      | V f ->
          f

    (* something wants closed_pos *)
    let wsum (type a) (wes : ((a, 'b) S.rate * a diff_vec) list) =
      let rates, es = L.split wes in
      let asvs = List.map eval es in
      V (fun ?z v ->
          let d =
            match z with
            | None ->
                failwith "Don't know the dim"
            | Some z ->
                Vec.dim z
          in
          (* FIXME closed_pos : so the problem comes from the map or the def *)
          let ws = L.map (fun f -> F.Pos.close (f v)) rates in
          let tot = L.fold_left F.Pos.add F.zero ws in
          (* How do we use z correctly here ? *)
          (* FIXME can we use Lacaml.D.axpy ? *)
          let z' = L.fold_left2 (fun v' w (f : (?z:Vec.t -> a -> Vec.t)) ->
              let a = F.to_float (F.div w tot)  in
              let y = f v in
              Lacaml.D.scal a y ;
              Vec.add v' y
          ) (Vec.make0 d) ws asvs
          in Lacaml.D.copy ?y:z z'
        )



    let rec modif :
      (* mentally we want this to only take (Vec.t -> Vec.t) as input,
       * but we'll enforce it through exceptions... *)
      type a b. (a -> b) S.Modif.symb -> (a, b) eq * a diff_vec =
      S.Modif.(
        let f () = failwith "Not_implemented" in
        function
        | Identity ->
            Eq, Zero
        | Transfo (I.Vec v) ->
            Eq, Constant v
        (*
        | Colors cols ->
            let wdvs = L.map (fun (w, e) ->
              let _, dv = modif e in
              (w, dv)
            ) cols in
            Eq, wsum wdvs
        *)
        (* we don't necessarily need to special case this *)
        | Erase (_, Identity) ->
            Eq, Zero
        | Erase (Bernoulli proba, e') ->
            let _, v = modif e' in
            Eq, scale (fun v' -> F.Proba.compl (proba v')) v
        | Chain (e, e') ->
            let eq, v = modif e in
            let eq', v' = modif e' in
            (match eq, eq' with
             | Eq, Eq -> (* prove that a = b = c = Vec.t *)
                 Eq, add v v')
        (* let's be exhaustive about cases that we don't handle *)
        | Bernoulli _ ->
            (* this should only appear inside an Erase *)
            f ()
        | Erase_lifted _ ->
            f ()
        | Bernoulli_lifted _ ->
            f ()
        | Map _ ->
            f ()
        | Chain_lifted _ ->
            f ()
        | Lift _ ->
            f ()
        (* remaining cases *)
        | Transfo _ ->
            f ()
        | Erase _ ->
            f ()
      )

   
    let field (S.Colors l) =
      let v = L.fold_left (fun v' (rate, smodif) ->
        let _, v = modif smodif in
        add v' (scale rate v)
      ) Zero l
      (* FIXME eval might not be good enough *)
      in eval v
  end


module Timedep =
  struct
    module I = I.Timedep

    module S = Symbolic.Make (I)

    (* a rate with some operations economised *)
    type 'a diff_vec =
      | Zero
      | Constant of Vec.t
      | V of (?z:Vec.t -> time -> 'a -> Vec.t)


    let scale f e =
      match e with
      | Zero ->
          Zero
      | Constant v ->
          V (fun ?z t v' ->
            let z = Lac.copy ?y:z v in
            Lacaml.D.scal (F.to_float (f t v')) z ;
            z)
      | V f' ->
          V (fun ?z t v ->
              let a = F.to_float (f t v) in
              let z = f' ?z t v in
              Lacaml.D.scal a z ;
              z)


    let add e e' =
      match e, e' with
      | Zero, any | any, Zero ->
          any
      | Constant v, Constant v' ->
          Constant (Vec.add v v')
      | V f, V f' ->
          (* Here we don't want to pass z to f and f' ! *)
          V (fun ?z t v -> Vec.add ?z (f t v) (f' t v))
      | Constant v, V f | V f, Constant v ->
          (* Same here don't pass to f *)
          V (fun ?z t v' -> Vec.add ?z v (f t v'))

    let mul e e' =
      match e, e' with
      | Zero, _ | _, Zero ->
          Zero
      | Constant v, Constant v' ->
          Constant (Vec.mul v v')
      | V f, V f' ->
          (* Again here, don't pass z to f and f' *)
          V (fun ?z t v -> Vec.mul ?z (f t v) (f' t v))
      | Constant v, V f | V f, Constant v ->
          (* Same here *)
          V (fun ?z t v' -> Vec.mul ?z v (f t v'))

    let eval =
      function
      | Zero ->
          (fun ?z _ _ ->
             match z with
             | None ->
                 (* how do you know the dim ? :( *)
                 failwith "Don't know the dim"
             | Some z ->
                 Vec.iteri (fun i _ -> z.{i} <- 0.) z ; z
          )
          (* or (fun ~z _ -> Vec.make0 (Vec.dim z)) ? *)
      | Constant v ->
          (fun ?z _ _ -> Lac.copy ?y:z v)
      | V f ->
          f


    let rec modif :
      (* mentally we want this to only take (Vec.t -> Vec.t) as input,
       * but we'll enforce it through exceptions... *)
      type a b. (a -> b) S.Modif.symb -> (a, b) eq * a diff_vec =
      S.Modif.(
        let f () = failwith "Not_implemented" in
        function
        | Identity ->
            Eq, Zero
        | Transfo (I.Vec v) ->
            Eq, Constant v
        (* we don't necessarily need to special case this *)
        | Erase (_, Identity) ->
            Eq, Zero
        | Erase (Bernoulli proba, e') ->
            let _, v = modif e' in
            Eq, scale (fun t v' -> F.Proba.compl (proba t v')) v
        (* we want only the case where the intermediate variable
         * is also "vec" : how ? *)
        | Chain (e, e') ->
            let eq, v = modif e in
            let eq', v' = modif e' in
            (match eq, eq' with
             | Eq, Eq -> (* prove that a = b = c = Vec.t *)
                 Eq, add v v')
        (* let's be exhaustive about cases that we don't handle *)
        | Bernoulli _ ->
            (* this should only appear inside an Erase *)
            f ()
        | Erase_lifted _ ->
            f ()
        | Bernoulli_lifted _ ->
            f ()
        | Map _ ->
            f ()
        | Chain_lifted _ ->
            f ()
        | Lift _ ->
            f ()
        (* remaining cases *)
        | Transfo _ ->
            f ()
        | Erase _ ->
            f ()
      )

   
    let field (S.Colors l) =
      let v = L.fold_left (fun v' (rate, smodif) ->
        let _, v = modif smodif in
        add v' (scale rate v)
      ) Zero l
      (* FIXME eval might not be good enough *)
      in eval v
  end



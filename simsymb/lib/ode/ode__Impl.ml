open Sig

module V = Lacaml.D.Vec

(* put this in Util signature ? *)
type vec = V.t


(* but where do we get ?z from ? *)

type state = unit


type _ t =
  | Apply : ('a -> 'b) t * 'a t ->
      'b t
  | Lift_left : 'a t ->
      ('b -> 'a) t
  | Lift_fun : ('a -> 'b) t ->
      (('c -> 'a) -> ('c -> 'b)) t
  | Lift_fun2 : ('a -> 'b -> 'c) t ->
      (('d -> 'a) -> ('d -> 'b) -> ('d -> 'c)) t
  | Z_apply : (?z:vec -> 'a) t * vec t ->
      'a t
  | Z_skip : (?z:vec -> 'a) t ->
      'a t
  | Z_lift : ('a -> 'b) t ->
      (?z:vec -> 'a -> 'b) t
  | Z_lift_fun : (('a -> 'b) -> ('c -> 'd)) t ->
      ((?z:vec -> 'a -> 'b) -> (?z:vec -> 'c -> 'd)) t
  | Vec : vec ->
      vec t
  | Zero :
      vec t
  | Scale :
      (?z:vec -> 'a U.pos -> vec -> vec) t
  | Add :
      (?z:vec -> vec -> vec -> vec) t
  | Dim : (* FIXME is that ok ? *)
      (vec -> int -> 'a U.pos) t


type 'a expr = 'a t


let rec simplify :
  type a. a t -> a t =
  function
  (* is this first one useful or is it already what happens in normal flow ? *) 
  | Apply (Apply (Z_skip Add, Vec v), Vec v') ->
      Vec (V.add v v')
  (* With Zero (useful) *)
  | Apply (Apply (Z_skip Add, Vec v), Zero) ->
      Vec v
  | Apply (Apply (Z_skip Add, Zero), Vec v) ->
      Vec v
  (* With lifted (useful) *)
  | Apply (Apply (Lift_fun2 (Z_skip Add), Lift_left (Vec v)), Lift_left (Vec v')) ->
      Lift_left (Vec (V.add v v'))
  (* With Zero *)
  | Apply (Apply (Lift_fun2 (Z_skip Add), Lift_left (Vec v)), Lift_left Zero) ->
      Lift_left (Vec v)
  | Apply (Apply (Lift_fun2 (Z_skip Add), Lift_left Zero), Lift_left (Vec v)) ->
      Lift_left (Vec v)
  (* all the rest is left the same *)
  | e ->
      e


let rec eval :
  type a. a t -> a =
  function
  (* special case : try to 'protect' constants *)
  | Apply (ef, ex) ->
      let f = eval ef in
      let x = eval ex in
      f x
  | Lift_left ex ->
      let x = eval ex in
      (fun _ -> x)
  | Lift_fun ef ->
      let f = eval ef in
      (fun g c -> f (g c))
  | Lift_fun2 ef ->
      let f = eval ef in
      (fun g g' c -> f (g c) (g' c))
  | Z_apply (ef, ez) ->
      let f = eval ef in
      let z = eval ez in
      f ~z
  | Z_skip ef ->
      let f = eval ef in
      f ?z:None
  | Z_lift ex ->
      let x = eval ex in
      (fun ?z -> x)
  | Z_lift_fun ef ->
      let f = eval ef in
      (fun g ?z -> f (g ?z))
  | Vec v ->
      v
  | Scale ->
      (* FIXME expensive copy *)
      (fun ?z lbd v ->
        let v' = Lacaml.D.copy ?y:z v in
        Lacaml.D.scal (F.to_float lbd) v' ;
        v'
      )
  | Add ->
      (fun ?z v v' -> V.add ?z v v')
  | Dim ->
      (fun v i -> F.Pos.of_float (v.{i}))


let eval_expr = eval


let bernoulli _ _ = failwith "Not_implemented"

open Sim__Sig


module Make (I : IMPL) :
  (SYMBOLIC with type state = I.state
             and type 'a expr = 'a I.expr) =
  struct
    include I

    type 'a event = ('a -> closed_pos * (state -> 'a)) 

    type _ t =
      | Apply : ('a -> 'b) t * 'a t ->
          'b t
      (* maybe we could avoid both of those here ? *)
      | Lift_left : 'a t ->
          ('b -> 'a) t
      | Lift_right : ('a -> 'b) t ->
          ('a -> 'c -> 'b) t
      | Lift_fun : ('a -> 'b) t ->
          (('c -> 'a) -> 'c -> 'b) t
      (* we would very much like to not need this *)
      | Lift_fun2 : ('a -> 'b -> 'c) t ->
          (('d -> 'a) -> ('d -> 'b) -> ('d -> 'c)) t
      | Expr : 'a expr ->
          'a t
      | One_rate :
          'b U.anypos t
      | Zero_modif :
          ('a -> 'a) t
      (* Can only be used on ('a -> 'a) t to guarantee
       * that it's only used on the outside of expressions *)
      | Upscale : ('a -> closed_pos) t * 'a event t ->
          'a event t
      | Downscale :
          (U.anyproba -> 'b U.anypos -> 'c U.pos) t
      (* What type to use ? forced to put state in ?
       * If it's just a 'lift' for ODE we can just remove them
       * at the last step before using the event ? *)
      | Add_events : 'a event t * 'a event t ->
          'a event t


    let apply ef ex = Apply (ef, ex)

    let lift_left ex = Lift_left ex

    let lift_right ef = Lift_right ef

    let lift_fun ef = Lift_fun ef

    let lift_fun2 ef = Lift_fun2 ef

    let expr e = Expr e

    let upscale r e = Upscale (r, e)

    let add e e' = Add_events (e, e')


    let map (ef : ('a -> 'b) t) (eg : ('b -> 'a -> 'a) t) =
      apply (apply (lift_fun2 eg) ef) Zero_modif

    let compose (ef : ('a -> 'b) t) (eg : ('b -> 'c) t) =
      apply (lift_fun eg) ef


    let rec eval : type a. a t -> a =
      function
      | Apply (ef, ex) ->
          let f = eval ef in
          let x = eval ex in
          f x
      | Lift_left ex ->
          let x = eval ex in
          (fun _ -> x)
      | Lift_right ef ->
          let f = eval ef in
          (fun x _ -> f x)
      | Lift_fun ef ->
          let f = eval ef in
          (fun g c -> f (g c))
      | Lift_fun2 ef ->
          let f = eval ef in
          (fun g g' c -> f (g c) (g' c))
      | Expr e ->
          eval_expr e
      | One_rate ->
          F.one
      | Zero_modif ->
          (fun x -> x)
      | Upscale (er, ee) ->
          let r = eval er in
          let e = eval ee in
          (fun x ->
            let lbd, m = e x in
            let lbd' = r x in
            (F.Pos.mul lbd' lbd, m)
          )
      | Downscale ->
          F.Pos.mul
      | Add_events (ee, ee') ->
          let e = eval ee in
          let e' = eval ee' in
          (fun x ->
            let r, m = e x in
            let r', m' = e' x in
            (F.Pos.add r r', 

    module Op =
      struct
        let (@@) = apply
        let (~<) = lift_left
      end
  end

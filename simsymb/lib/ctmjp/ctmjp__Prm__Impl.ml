open Ctmjp__Sig


module Make (P : Prm.PRM) =
  struct
    type state = P.point

    type 'a expr = 'a

    let eval_expr x = x

    (* FIXME problem of reusing u in the same state everywhere,
     * it needs to adapt somehow 
     * make u mutable and change it here ? *)
    let bernoulli (pt : state) (p : U.anyproba) =
      F.Op.(pt.Prm.u <= p)
  end

open Sim__Sig

module Impl =
  struct
    type state = rng

    type 'a expr = 'a

    let eval_expr x = x

    let bernoulli rng p = U.rand_bernoulli ~rng p
  end


module Symb = Symbolic.Make (Impl)


let eval_auto : (rng -> 'a -> 'a) Symb.t -> (rng -> 'a -> 'a) =
  Symb.eval


let eval_timedep : (rng -> time -> 'a -> 'a) Symb.t -> (rng -> time -> 'a -> 'a) =
  Symb.eval


let as_timedep (f : (rng -> 'a -> 'a)) =
  (fun rng _ -> f rng)


let events_auto (res : (('a -> 'b U.pos) * (rng -> 'a -> 'a) Symb.t) list) =
  let l' = L.map (fun (rate, e) -> (rate, eval_auto e)) res in
  let cumul x (_, tot) (r', m') =
    (m', F.Pos.add tot (r' x))
  in
  (fun _ x ->
    let (_, tot), revmr =
      U.cumul_fold_left (cumul x) ((fun _ _ x -> x), F.zero) l'
    in
    let choose rng = U.rand_cumul_rev_choose ~rng ~tot revmr in
    let modif rng =
      let m = choose rng in
      let partial = m rng in
      (* lift for time *)
      (fun _ -> partial)
    in
    (tot, modif)
  )


let events_timedep (res : ((time -> 'a -> 'b U.pos) * (rng -> time -> 'a -> 'a) Symb.t) list) =
  (* Warn about non autonomous system *)
  Printf.eprintf
  "Warning:
    Simulation of a non-autonomous system is not necessarily exact." ;
  (* unsymb *)
  let l' = L.map (fun (rate, e) -> (rate, eval_timedep e)) res in
  let cumul t x (_, tot) (r', m') =
    (m', F.Pos.add tot (r' t x))
  in
  (fun t x ->
    let (_, tot), revmr =
      U.cumul_fold_left (cumul t x) ((fun _ _ x -> x), F.zero) l'
    in
    let choose rng = U.rand_cumul_rev_choose ~rng ~tot revmr in
    let modif rng =
      let m = choose rng in
      m rng
    in
    (tot, modif)
  )


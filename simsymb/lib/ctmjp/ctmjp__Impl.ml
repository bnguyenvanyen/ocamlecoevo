(* annoying anonymous variables *)
open Ctmjp__Sig


(* on veut une version pour ODE.
 * Est-ce qu'on peut dire que "t" est une partie de "state" ? 
 * Pour que ce soit plus homogène *)

module Auto =
  struct
    type ('a, 'b) rate = ('a -> 'b pos)
    type ('a, 'b) lift_proba = ('a -> 'b -> proba)
    type nonrec 'a proba = ('a -> proba)

    module Make (S : sig type state end) =
      struct
        type nonrec ('a, 'b) rate = ('a, 'b) rate
        type nonrec 'a proba = 'a proba
        type nonrec ('a, 'b) lift_proba = ('a, 'b) lift_proba

        type ('a, 'b) transformation = (S.state -> 'a -> 'b)

        type 'a modif = (S.state -> 'a)
      end

    module No_state =
      struct
        type nonrec ('a, 'b) rate = ('a, 'b) rate
        type nonrec 'a proba = 'a proba
        type nonrec ('a, 'b) lift_proba = ('a, 'b) lift_proba

        type ('a, 'b) transformation = ('a -> 'b)

        type 'a modif = 'a
      end
  end


module Timedep =
  struct
    (* here float should be time, but it's got arity one... *)
    type ('a, 'b) rate = (time -> ('a, 'b) Auto.rate)
    type 'a proba = (time -> 'a Auto.proba)
    type ('a, 'b) lift_proba = (time -> ('a, 'b) Auto.lift_proba)

    module Make (S : sig type state end) =
      struct
        type nonrec ('a, 'b) rate = ('a, 'b) rate
        type nonrec 'a proba = 'a proba
        type nonrec ('a, 'b) lift_proba = ('a, 'b) lift_proba

        type ('a, 'b) transformation = (S.state -> time -> 'a -> 'b)

        type 'a modif = (S.state -> time -> 'a)
      end

    module No_state =
      struct
        type nonrec ('a, 'b) rate = ('a, 'b) rate
        type nonrec 'a proba = 'a proba
        type nonrec ('a, 'b) lift_proba = ('a, 'b) lift_proba

        type ('a, 'b) transformation = (time -> 'a -> 'b)

        type 'a modif = (time -> 'a)
      end
  end


module Rng =
  struct
    type state = rng

    module Auto = Auto.Make (struct type nonrec state = state end)
    module Timedep = Timedep.Make (struct type nonrec state = state end)
  end


module Prm =
  struct
    module Make (S : Prm.COLOR) =
      struct
        type state = S.t Prm.point

        module Auto = Auto.Make (struct type nonrec state = state end)
        module Timedep = Timedep.Make (struct type nonrec state = state end)
      end
  end



(* For Gill / Euler_Poisson input / output. Or where do we put it ?
 * Still another module ? *)

type ('a, 'b) rate = ('b anypos -> 'a -> closed_pos)

type ('a, 'b) modif = (rng -> 'b anypos -> 'a -> 'a)

type ('a, 'b, 'c) nextfun = ('b anypos -> 'a -> 'c pos * 'a)

type 'a specs = (string * 'a Lift_arg.spec * string) list



(** Types and signatures reused elsewhere. *)
exception Simulation_error

module L = BatList

module U = Util
module F = U.Float

type rng = Random.State.t
type closed_pos = F.closed_pos F.t
(* type 'a proba = 'a Util.proba *)
type time = closed_pos


module type IMPL =
  sig
    type state
    type 'a expr

    val eval_expr : 'a expr -> 'a
    val bernoulli : (state -> U.anyproba -> bool)
    (* FIXME we need to know how to add events
     * (and it's not easy) *)
  end


(* should not be exposed ? increased arity *)
module type SYMBOLIC =
  sig
    include IMPL

    type 'a event = ('a -> closed_pos * (state -> 'a)) 

    type _ t =
      | Apply : ('a -> 'b) t * 'a t ->
          'b t
      (* maybe we could avoid both of those here ? *)
      | Lift_left : 'a t ->
          ('b -> 'a) t
      | Lift_right : ('a -> 'b) t ->
          ('a -> 'c -> 'b) t
      | Lift_fun : ('a -> 'b) t ->
          (('c -> 'a) -> 'c -> 'b) t
      (* we would very much like to not need this *)
      | Lift_fun2 : ('a -> 'b -> 'c) t ->
          (('d -> 'a) -> ('d -> 'b) -> ('d -> 'c)) t
      | Expr : 'a expr ->
          'a t
      | One_rate :
          'b U.anypos t
      | Zero_modif :
          ('a -> 'a) t
      (* Can only be used on ('a -> 'a) t to guarantee
       * that it's only used on the outside of expressions *)
      | Upscale : ('a -> closed_pos) t * 'a event t ->
          'a event t
      | Downscale :
          (U.anyproba -> 'b U.anypos -> 'c U.pos) t
      (* What type to use ? forced to put state in ?
       * If it's just a 'lift' for ODE we can just remove them
       * at the last step before using the event ? *)
      | Add_events : 'a event t * 'a event t ->
          'a event t


    val apply : ('a -> 'b) t -> 'a t -> 'b t
    val lift_left : 'a t -> ('b -> 'a) t
    val lift_right : ('a -> 'b) t -> ('a -> 'c -> 'b) t
    val lift_fun : ('a -> 'b) t -> (('c -> 'a) -> 'c -> 'b) t
    val lift_fun2 :
      ('a -> 'b -> 'c) t -> (('d -> 'a) -> ('d -> 'b) -> ('d -> 'c)) t
    val expr : 'a expr -> 'a t
    val upscale : ('a -> closed_pos) t -> 'a event t -> 'a event t
    val add : 'a event t -> 'a event t -> 'a event t

    (*
    val choose : 'a t -> 'a t -> (bool -> 'a) t
    val erase : ('a -> 'a) t -> (bool -> 'a -> 'a) t
    *)
    val map : ('a -> 'b) t -> ('b -> 'a -> 'a) t -> ('a -> 'a) t
    val compose : ('a -> 'b) t -> ('b -> 'c) t -> ('a -> 'c) t
     
    val eval : 'a t -> 'a

    module Op :
      sig
        val (@@) : ('a -> 'b) t -> 'a t -> 'b t
        (* val (|>) : *)
        val (~<) : 'a t -> ('b -> 'a) t
      end
  end


(* the values should be in Impl *)

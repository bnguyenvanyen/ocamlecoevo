
type hyperparam = {
  tf : float ;  (** final time *)
  lbd : float ;  (** param of the exponential prior *)
  v : [`V of float] ;  (** variance of proposals *)
}

type param = float

module CountO = Sim.Out.Cadlag (struct
  type t = int
  type aux = int
  type nonrec param = param
  let aux_of par t n = n
end)

module I = Sim.Stopped_poisson.Make (Counting.Fixed) (CountO)

module S :
  (Mcmc.STATE with type hyperparam = hyperparam
               and type param = param
               and type data = int Jump.cadlag) =
  struct
    type nonrec hyperparam = hyperparam
    type param = float
    type data = int Jump.cadlag

    let string_of_param par =
      Printf.sprintf "%f" par

    let prior hypar =
      Dist.Exponential hypar.lbd

    let proposal hypar par =
      Dist.Normal (par, hypar.v)

    let likelihood hypar par =
      let rec diff acc cc =
        match cc with
        | (t, n) :: (t', n') :: tcc ->
          let acc' = (par *. (t' -. t), n' - n) :: acc in
          let cc' = (t', n') :: tcc in
          diff acc' cc'
        | (t, n) :: [] ->
          List.rev acc
        | [] ->
          List.rev acc
      in
      (*
      let count dc =
        let f_fold cc (lbd, dn) =
          match cc with
          | (t, n) :: tcc -> (t +. lbd /. par, n + dn) :: cc
          | [] -> failwith "need a starting point"
        in
        List.rev (List.fold_left f_fold [(0., 0)] dc)
      in
      *)
      let rng = Random.get_state () in
      let opar = {
        Sim.Out.rng = rng ;
        p = 1. ;
        res = []
      }
      in
      let auxf ?(auxi=0) par t n = n in
      let draw () =
        opar.Sim.Out.res <- [] ;
        ignore (I.simulate ~opar ~apar:{ Sim.Sig.seed = None } par auxf hypar.tf 0 [] []) ;
        (* FIXME subsample *)
        opar.Sim.Out.res
      in
      let loglik cc =
        let dc = diff [] cc in
        List.fold_left (fun ll (lbd, dn) -> (Util.logp_poisson lbd dn) :: ll) [] dc
      in  
      Dist.Base (draw, loglik)
  end


module O :
  (Mcmc.OUTSPEC with type hyperparam = hyperparam
                 and type param = param
                 and type outparam = out_channel
                 and type data = int Jump.cadlag) =
  struct
    type nonrec hyperparam = hyperparam
    type nonrec param = param
    type outparam = out_channel
    type data = int Jump.cadlag
 
    let default = stdout

    let output_begin opar apar hypar data =
      begin
        match hypar.v with
        | `V fv ->
          Printf.fprintf opar
          "MCMC run of %i iterations with n_gap=%i, tf=%f, v=%f\n"
          apar.Mcmc.n_iter apar.Mcmc.n_gap
          hypar.tf fv
      end ;
      begin
        Printf.fprintf opar "DATA :\n" ;
        Printf.fprintf opar "t;n\n" ;
        List.iter (fun (t, n) -> Printf.fprintf opar "%f;%i\n" t n) data
      end ;
      Printf.fprintf opar "n;k;lbd;lbd';v\n"
    
    let output opar hypar n k lbd lbd' p'p qq' l'l v =
      Printf.fprintf opar "%i;%i;%f;%f;%f\n" n k lbd lbd' v

    let output_end opar = ()
  end

module F = Mcmc.Make (S) (O)

(** Find the rate of an exponential variable from a sample of realisations *)

type hyperparam = {
  n : int ;  (** size of sample *)
  v : [`V of float] ;  (** variance of proposals *)
  upper : float ;  (** upper bound of prior *)
}

module S =
  struct
    type nonrec hyperparam = hyperparam
    type param = float
    type t = float
    type data = t list

    let string_of_param par =
      Printf.sprintf "%f" par

    let prior hypar =
      Dist.Uniform (0., hypar.upper)

    let proposal hypar par =
      match hypar.v with
      | `V fv ->
        Dist.Bound (
          0.,
          hypar.upper,
          Util.p_normal par fv hypar.upper -. Util.p_normal par fv 0.,
        Dist.Normal (par, hypar.v)
      )

    let likelihood hypar par =
      (* FIXME need to know List.length data *)
      let dl = Util.l_repeat hypar.n (Dist.Exponential par) in
      Dist.Many dl
  end


module O =
  struct
    type nonrec hyperparam = hyperparam
    type param = float
    type outparam = out_channel
    type data = float list

    let default = stdout

    let output_begin opar apar hypar d =
      match hypar.v with
      | `V fv ->
        Printf.fprintf opar
        "MCMC run with n_gap=%i, n_iter=%i, v=%f, upper=%f, and data:\n"
        apar.Mcmc.n_gap apar.Mcmc.n_iter
        fv hypar.upper ;
        List.iter (Printf.fprintf opar "%f;") d ;
        Printf.fprintf opar "\nn;k;lbd;lbd';v\n"

    let output opar hypar n k par par' p'p qq' l'l v =
      Printf.fprintf opar "%i;%i;%f;%f;%f\n" n k par par' v

    let output_end opar = ()

  end

module Fit = Mcmc.Make (S) (O)

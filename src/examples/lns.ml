module Sp = Seqpop
module Lns = Logistic_neutral_seq

module P = Sp.Complement (Lns.P)

let gtr_r = ref (Jc69.to_gtr { Jc69.mu = 1e-4 })

let par_r = ref Lns.default

let opar_r = ref Lns.Out.default

let k_r = ref 10000

let seed_r = ref None

let tf_r = ref 10.

let specl = (Sim.Arg.specl_of gtr_r Gtr.specl)
          @ (Sim.Arg.specl_of par_r Lns.specl)
          @ (Sim.Arg.specl_of opar_r Lns.Sp.Out.specl)
          @ [
  ("-k",
   Arg.Set_int k_r,
   "Length of initial random sequence.") ;
  ("-seed",
   Arg.Int (fun n -> seed_r := Some n),
   "Seed for the PRNG.") ;
  ("-tf",
   Arg.Set_float tf_r,
   "Simulation time.") ;
]

let anon_fun s = Printf.printf "Ignored anonymous argument : %s" s


let usage_msg =
  "Simulate the neutral mutations of sequences
   in a logistic birth death population."

let main () =
  begin
    Arg.parse specl anon_fun usage_msg ;
    let par = { !par_r with Lns.evo =
      { !par_r.Lns.evo with Mut.snp = Gtr.to_snp !gtr_r } } in
    let seq0 = Seq.random ~rng:(Random.get_state ()) !k_r in
    let z0 = P.create_empty () in
    let seqsdl0 = (seq0, [(0., Seq.Sig.Not)]) in
    ignore (P.add_new z0 seqsdl0) ;
    match !seed_r with
    | None -> Lns.simulate_until !opar_r par !tf_r z0
    | Some seed -> Lns.simulate_until ~seed !opar_r par !tf_r z0
  end ;;

main ()

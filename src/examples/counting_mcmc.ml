open Counting_mcmc

let opar_r = ref stdout
let apar_r = ref {
  Mcmc.seed = None ;
  n_iter = 100000 ;
  n_gap = 100
}
let hypar_r = ref {
  tf = 10. ;
  lbd = 1. ;
  v = `V 0.1 ;
}

let target_par_r = ref 2.

let si_apar_r = ref { Sim.Sig.seed = Some 12345 }

let specl = [
  ("-lambda", Arg.Set_float target_par_r, "Target counting rate.") ;
  ("-tf", Arg.Float (fun x -> hypar_r := { !hypar_r with tf = x }),
   "Final time.") ;
  ("-v", Arg.Float (fun x -> hypar_r := { !hypar_r with v = `V x }),
   "Variance of proposals.") ;
  ("-niter", Arg.Int (fun n -> apar_r := { !apar_r with Mcmc.n_iter = n }),
   "Number of iterations.") ;
  ("-ngap", Arg.Int (fun n -> apar_r := { !apar_r with Mcmc.n_gap = n }),
   "Save result every n steps (decorrelate).")
]

let anon_fun s = Printf.printf "Ignored anonymous argument : %s" s

let usage_msg = "  Infer a constant rate counting process via MCMC.";;

Arg.parse specl anon_fun usage_msg;;

let yc = [(0., ())]
let mc = [(0., (fun x -> x))]
let auxf ?(auxi=0) par t n = n
let opar = {
  Sim.Out.rng = Random.get_state () ;
  p = 0.2 ;
  res = []
};;
ignore (I.simulate ~opar ~apar:!si_apar_r !target_par_r auxf !hypar_r.tf 0 yc mc);;
let data = opar.Sim.Out.res;;

F.posterior ~opar:!opar_r ~apar:!apar_r !hypar_r data

open Music_pop

module I = Sim.Gill.Make (S') (CoOut)

let opar_r = ref (
  {
    blen = 44100 ;
    smplr = 44100 ;
    volume = 0.01 ;
    fname = "test_music_pop"
  },
  {
    Sim.Out.dt_print = 0.1 ;
    chan = stdout ;
  }
)

let apar_r = ref {
  Sim.Sig.seed = Some 0 ;
}

let par_r = ref {
  b = 0. ;
  d = 0. ;
  c = 0. ;
  mu = 10. ;
  v = 1. /. 12. ** 2. ;
  peaks = 10 ;
}

let tf_r = ref 1.

let specl = (Sim.Arg.specl_of apar_r Sim.Gill.specl)
          @ (Sim.Arg.specl_of opar_r CoOut.specl)
          @ (Sim.Arg.specl_of par_r specl)
          @ [("-tf", Arg.Set_float tf_r, "Simulate until (seconds)")]

let anon_fun s = Printf.printf "Ignored anonymous argument : %s" s

let usage_msg = "  Simulate a population of musical components using the Gillespie algorithm."

let main () =
  Arg.parse specl anon_fun usage_msg ;
  let z0 = P.create_empty () in
  ignore (P.add_descent 0. z0 None (Some 1) (log 440. /. log 2.)) ;
  let audiout, csvout = !opar_r in
  let blen = 1 + int_of_float (!tf_r *. float audiout.smplr) in
  let opar = ({ audiout with blen }, csvout) in
  I.simulate_until ~opar ~apar:!apar_r !par_r !tf_r z0;;


main ()

open Sdr

module I = Sim.Gill.Make (S') (O)

let apar_r = ref { Sim.Sig.seed = Some 0 }

let opar_r = ref stdout

let par_r = ref {
  b = 1. ;
  d = 0.01 ;
  mu_snp = 1e-2 ;
  mu_del = 0. ;
  mu_rep = 1e-4
}

let p_keep_r = ref 1.

let tf_r = ref 1000.

let input_o_r = ref None

let specl = (Sim.Arg.specl_of apar_r Sim.Gill.specl)
          @ (Sim.Arg.specl_of par_r specl)
          @ [("-pkeep",
              Arg.Set_float p_keep_r,
              "Probability of sampling individual x trait.") ;
             ("-input",
              Arg.String (fun s -> input_o_r := Some s),
              "Channel/file to get starting sequence(s) from.") ;
             ("-chan",
              Arg.String (fun s -> opar_r := Sim.Arg.handle_chan s ),
              "Channel/file to output to.") ;
             ("-tf",
              Arg.Set_float tf_r,
              "Simulation time.")]

let anon_fun s = Printf.printf "Ignored anonymous argument : %s" s

let usage_msg = "  Simulate the neutral mutations of sequences in a population."


let seq0 = Seq.of_string
  "ATTGCCAGTCAATGCACCTGTATCTTGGACTGCCATGTTAGACTCGTAAGCTTAGCTCGATGATCATAGGTTCTCGAAGAATGCCTCGATGCTGCTAGCTGCTCTCTCGGAGAAATCGCTGATTTCGCTTGCGCTAGCTAATAGCTAGTCTAGCTCTGCTAATGATGCTCTTGATGCGCGGTGCGCGTATATATATGCGTCTAGATCGCTGCGGCCCTGTGCTATGCTAAAAGTCGCTCGTTGATGATAGATGCGCGCGCT";;

let main () =
  begin
    Arg.parse specl anon_fun usage_msg ;
    Printf.printf "Reading fasta input\n%!" ;
    (* FIXME allow a tree to be specified for relations between individuals ? *)

    let chan_in =
      match !input_o_r with
      | None -> (Printf.printf "None\n" ; stdin)
      | Some s -> (open_in s)
    in
    let al = Seq.Io.assoc_of_fasta chan_in in
    let z0 = P.create_empty () in
    (* FIXME not too sure about this *)
    begin
      match !apar_r.Sim.Sig.seed with
      | None ->
        ()
      | Some n ->
        (z0.Seqpop.rng <- Random.State.make (Array.init 1 (fun _ -> n)))
    end ;
    z0.Seqpop.p_keep <- !p_keep_r ;
    List.iter (fun (i, seq) -> ignore (P.add_descent 0. z0 None (Some 0) (seq, []))) al ;
    (* let z0 = of_nexus (open_in "initseq_bis.nexus");; *)
    Printf.printf "Input read, starting simulation\n%!" ;
    let auxf ?(auxi=()) _ _ _ = () in
    let pred t _ _ = (t >= !tf_r) in
    I.simulate ~opar:!opar_r ~apar:!apar_r !par_r auxf pred z0
  end;;


main ()

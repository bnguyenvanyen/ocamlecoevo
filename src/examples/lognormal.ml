(* soooo, few 0., why the problems in real life ? *)
let d = Dist.Lognormal (log10 2., `V 0.1)
let d' = Dist.Bound (0., 10., 1., Dist.Lognormal (0., `V 0.1))
let rng = Random.State.make_self_init ();;

for n = 0 to 1000 do
  let x = Dist.draw_from rng d in
  let lp = Dist.likof (Dist.loglf d' x) in
  Printf.printf "%i;%f;%f\n" n x lp
done

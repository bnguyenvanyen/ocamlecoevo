
type param = unit

module System =
  struct
    type hidden = float
    type nonrec param = param
    let coal_rate () sz = 1. /. (2. *. sz)
    let sample_rate () sz = 0.
  end


module S = Coal.Make (System)

module O = Sim.Out.Csv (struct
    type t = S.t
    type param = unit
    type aux = int

    let header = [["size" ; "add"]]

    let line par t z =
      [Printf.sprintf "%f" t ;
       Printf.sprintf "%i" z.S.size ]
end)


module HI =
  struct
    type t = int
    type param = unit
    type algparam = unit

    let simulate () () tf y0 =
      let c = List.map (fun t -> (t, y0)) [0. ; 1. ; 2. ; 3. ; 4. ; 5.] in
      Jump.cut_right tf c
  end

module Integr = Stopped_poisson.Make (S) (O)

let rng = Random.State.make_self_init ()

let add_indivs n par t y z =
  let rec f i z =
    match i with
    | 0 -> z
    | _ -> f (i - 1) (S.sample_modif par rng t y z)
  in f n z

let opar = { Sim.Out.dt_print = 0. ; chan = stdout }
let apar = { Sim.Sig.seed = Some 12345 }
let par = ()
let z0 = { S.next = 0 ; size = 0 ; pop = S.Tset.empty };;
let yc = (0., 100.) :: (1., 200.) :: (2., 300.) :: []
let mc = [
  (0., (fun z -> add_indivs 10 par 0. 0. z)) ;
  (1., (fun z -> add_indivs 10 par 1. 0. z)) ;
];;

Integr.simulate ~opar ~apar par S.auxf 10. z0 yc mc

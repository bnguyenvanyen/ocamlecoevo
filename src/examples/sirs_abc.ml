open Fit
open Sirs_abc

let opar_r = ref stdout
let apar_r = ref {
  Abc.seed = None ;
  n_iter = 100000 ;
  eps = 10.
}

let hypar_r = ref default

(* FIXME find UPCA parameter values *)
let target_par_r = ref {
  Sirs.betamin = 0. ;
  betamax = 10. ;
  nu = 5. ;
  gamma = 0.1 ;
  eta = 0. ;
  freq = 1. ;
  phase = 0. ;
  rho = 0.05 ;
}

let s0p_r = ref 0.99
let i0p_r = ref 0.01

let si_apar_r = ref { Sim.Sig.seed = Some 12345 }

(* FIXME add Abc.Fit in Sirs_abc *)
(* let prog_r = ref false *)


let specl = Sim.Arg.specl_of target_par_r Sirs.specl @ [
  ("-chan",
   Arg.String (fun s -> opar_r := Sim.Arg.handle_chan s),
   "Channel or file to output to.") ;
  ("-abcseed",
   Arg.Int (fun n -> apar_r := { !apar_r with Abc.seed = Some n }),
   "Seed of the RNG for the ABC run.") ;
  ("-niter",
   Arg.Int (fun n -> apar_r := { !apar_r with Abc.n_iter = n }),
   "Number of iterations.") ;
  ("-eps",
   Arg.Float (fun x -> apar_r := { !apar_r with Abc.eps = x }),
   "Distance cutoff.") ;
  (*
  ("-prog",
   Arg.Set prog_r,
   "Flag to use progressive MCMC (stops early if it can).")
   *)
] @ (Sim.Arg.specl_of si_apar_r Sim.Gill.specl)
  @ (Sim.Arg.specl_of hypar_r specl)

let anon_fun s = Printf.printf "Ignored anonymous argument : %s" s

let usage_msg = " Infer a target stochastic SIRS model via progABC with deterministic simulations.";;

let main () =
  Arg.parse specl anon_fun usage_msg ;
  let size = !hypar_r.size in
  let r0p = 1. -. !s0p_r -. !i0p_r in
  let trec = !hypar_r.tf -. !hypar_r.tburn in
  let nrec = 100. in
  let ptf t _ = (t >= !hypar_r.tf) in
  let s0 = int_of_float (size *. !s0p_r) in
  let i0 = int_of_float (size *. !i0p_r) in
  let r0 = int_of_float (size *. r0p) in
  let x0 = (s0, i0, r0, 0) in
  (*
  let y0 = Lacaml.D.Vec.make0 Sirs.Det.n in
  y0.{1} <- size *. !s0p_r ;
  y0.{2} <- size *. !i0p_r ;
  y0.{3} <- size *. r0p ;
  *)
  let res = Sirs.si_simulate !si_apar_r !target_par_r !hypar_r.tf x0 in
  (*
  let res = Sirs.di_simulate !hypar_r.apar !target_par_r !hypar_r.tf y0 in
  *)
  let reg = Jump.regulate res ptf (trec /. nrec) in
  let y = Jump.map float_of_int reg in
  (*
  let prim = Jump.primitive res in
  let reg = Jump.p_regulate prim ptf (trec /. nrec) in
  let y = reg in
  *)
  let y = Jump.cut_left !hypar_r.tburn y in
  F.posterior !opar_r !apar_r !hypar_r y ;;

main ()

(* FIXME d around 5000 never small, why ? *)
(* FIXME Il vaut mieux couper le burn-in de chaque simulation :
 * simuler sur `tf = 50` puis couper les 20 premières années :
 * comment on peut obtenir ça ?
 * on peut ajouter un hyperparam `tburn`. *)

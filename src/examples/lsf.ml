module Lsf = Logistic_selective_float

let par_r = ref Lsf.default

let chan_r = ref stdout

let dt_print_r = ref 0.

let seed_r = ref None

let tf_r = ref 100.

let x0_r = ref 0.

let specl = (Sim.Arg.specl_of par_r Lsf.specl)
          @ [
  ("-chan",
   Arg.String (fun s -> chan_r := Sim.Arg.handle_chan s),
   "Channel or file to output to (default stdout).") ;
  ("-dtprint",
   Arg.Set_float dt_print_r,
   "Minimum time interval between prints.") ;
  ("-seed",
   Arg.Int (fun n -> seed_r := Some n),
   "Seed for the PRNG.") ;
  ("-tf",
   Arg.Set_float tf_r,
   "Simulation time (default 100.).") ;
  ("-x0",
   Arg.Set_float x0_r,
   "Initial trait value (default 0.).") ;
]

let anon_fun s = Printf.printf "Ignored anonymous argument : %s" s

let usage_msg = " Run a stochastic simulation of a logistic birth death system."

let main () =
  begin
    Arg.parse specl anon_fun usage_msg ;
    let z0 = Lsf.P.create_empty () in
    let id0 = Lsf.P.new_ident z0 in
    ignore (Lsf.P.add_descent 0. z0 None (Some id0) !x0_r) ;
    let chan = !chan_r in
    let dt = !dt_print_r in
    let tf, nf, xmin, xmax, zf =
      match !seed_r with
      | None -> Lsf.Csv.simulate_until ~chan ~dt !par_r !tf_r z0
      | Some seed -> Lsf.Csv.simulate_until ~chan ~dt ~seed !par_r !tf_r z0  in
    Printf.printf
    "Final time : %.2f
     Final population size : %i
     Final minimum trait value : %.2f
     Final maximum trait value : %.2f\n"
    tf nf xmin xmax
  end;;

main ()

open Bintree
open Stree

let tf = 10.5
let tree = Node (0.,
  Binode (1.,
    Binode (2.,
      Leaf 3.5,
      Binode (4.,
        Leaf 4.5,
        Leaf 5.5
      )
    ),
    Binode (3.,
      Binode (5.,
        Binode (6.,
          Leaf 6.5,
          Leaf 7.5
        ),
        Leaf 8.5
      ),
      Binode (7.,
        Leaf 9.5,
        Leaf 10.5
      )
    )
  )
)
let w, h = 120., 120.
let m = 10.
let n_r = ref 10

let main () =
  let specl =
    [("-n", Arg.Set_int n_r, ": Number of iterations of improvement")]
  in let anon_print s = print_endline ("Ignored anonymous argument : " ^ s)
  in let usage_msg = "Draw a simple tree and improve it `n` times."
  in Arg.parse specl anon_print usage_msg ;
  let ttoy t = t *. h /. tf in
  let color t = Draw.black in
  let gt2 = Draw.Stree.place_dyadic color ttoy w tree in
  let surface, cr = Draw.init_fig "test_dy_plot.svg" w h in
  Draw.plot cr gt2 ;
  Cairo.Surface.finish surface ;
  let lb = (0., 0.) :: [] in
  let rb = (0., w) :: [] in
  let gt = Draw.Stree.place_tree color ttoy 0.1 lb rb tree in
  let surface, cr = Draw.init_fig "test_new_plot.svg" w h in
  Draw.plot cr gt ;
  Cairo.Surface.finish surface ;
  let ot = Draw.Stree.offset_tree color ttoy w m tree in
  let surface, cr = Draw.init_fig "test_offset_plot.svg" w h in
  Draw.plot cr ot ;
  Cairo.Surface.finish surface

let () = main ()

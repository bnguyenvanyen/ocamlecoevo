let opar = stdout
let apar = {
  Abc.eps = 1000. ;
  n_iter = 100000 ;
}
let hypar = {
  Sir_abc.tf = 10. ;
  size = 10000. ;
  apar = {
    Sim.Dopri5.h0 = 0.01 ;
    delta = 0.1 ;
    min_step = 0.001 ;
    max_step = 1.
  }
}

let target_par = {
  Sir.beta = 2. ;
  nu = 0.01 ;
  rho = 0.05 ;
}

let x0 = (int_of_float (hypar.Sir_abc.size *. 0.8),
          int_of_float (hypar.Sir_abc.size *. 0.1),
          int_of_float (hypar.Sir_abc.size *. 0.1),
          0)

(* should we fix this to something constant ? *)
let si_apar = { Sim.Sig.seed = 12345 }
let sic_r = ref [];;

ignore (Sir.SI.simulate sic_r si_apar target_par hypar.Sir_abc.tf x0);;
let data = Jump.piecelin_of @@ Jump.float_of_int @@ !sic_r;;


Sir_abc.Fit.posterior opar apar hypar [data]

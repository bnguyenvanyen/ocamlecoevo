module Integr = Sim.Gill.Make (Drift.S) (Drift.O)

let opar_r = ref {
  Bdm.color = Drift.color ;
  height = 297. ;
  width = 210. ;
  margin = 25. ;
  fname = "test_drift" ;
  chan = stdout ;
}

let apar_r = ref { Sim.Sig.seed = Some 0 }

let par_r = ref {
  Drift.b = 2. ;
  d = 1. ;
  mua = 0.1 ;
  mub = 0.1 ;
}

let tf_r = ref 1.

let specl = (Sim.Arg.specl_of apar_r Sim.Gill.specl)
          @ (Sim.Arg.specl_of opar_r Drift.O.specl)
          @ (Sim.Arg.specl_of par_r Drift.specl)
          @ [("-tf", Arg.Set_float tf_r, "Simulate until (seconds)")]

let anon_fun s = Printf.printf "Ignored anonymous argument : %s" s

let usage_msg = "  Simulate the drift of an A|B allele using the Gillespie algorithm."

let z0 = Drift.System.init [(Tree.Path.End 0, Drift.A)];;

Arg.parse specl anon_fun usage_msg;;

let auxf ?(auxi=()) par t x = ()
let p t x auxi = (t >= !tf_r);;

Integr.simulate ~opar:!opar_r ~apar:!apar_r !par_r auxf p z0

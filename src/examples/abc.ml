

let sample_exp rng m lbd =
  let rec f n =
    match n with
    | 0 -> []
    | _ -> (Util.rand_exp rng lbd) :: (f (n - 1))
  in f m;;


type hyperparam = {
  sample : int ;  (** max sample size *)
  upper : float ;  (** upper bound *)
}

module S =
  struct
    type nonrec hyperparam = hyperparam
    type param = int * float
    type t = float Jump.cadlag
    type data = t list

    let draw_prior hypar rng =
      (Random.int hypar.sample, Util.rand_float rng hypar.upper)

    let sim hypar (par : param) =
      (* FIXME we cheat here again *)
      let rng = Random.State.make_self_init () in
      let m, lbd = par in
      Jump.cumul_dist_of_sample (sample_exp rng m lbd)

    let distance hypar x x' = 
      let diff = Jump.sub x x' in
      (* proba cumulate so both reach 1., anything > last jump works to integrate *)
      let tmax, _ = List.hd (List.rev diff) in
      Jump.l2_norm (Jump.sub x x') (tmax +. 1.)

  end

module O =
  struct
    type nonrec hyperparam = hyperparam
    type param = int * float
    type outparam = out_channel
    type t = float Jump.cadlag
    let output_begin opar apar hypar d =
      Printf.fprintf opar
      "ABC run of %i iterations with epsilon=%f, max sample size=%i, and max exp param value=%f\n"
      apar.Abc.n_iter apar.Abc.eps hypar.sample hypar.upper
      (*
      ;
      Printf.fprintf opar "data :%s\n"
      (List.fold_left
       (fun s x -> Printf.sprintf "%s%s;" s (Jump.sprint_jump_times x))
       "" d)
      *)

    let output opar n (m, lbd) xl d =
      Printf.fprintf opar "%i;%i;%f;%f\n" n m lbd d
      (* (Jump.sprint_jump_times xl) *)
    let output_end opar = ()
  end

module Fit = Abc.Make (S) (O)

let lbd = 1.
let m = 100
let opar = stdout
let apar = { Abc.seed = None ; Abc.eps = 0.1 ; n_iter = 1000000 }
let hypar = { sample = 200 ; upper = 10. }
let d = Jump.cumul_dist_of_sample (sample_exp (Random.State.make_self_init ()) m lbd);;

Fit.posterior opar apar hypar [d]

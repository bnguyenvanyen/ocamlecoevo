type t = A | B
type param = {
  rab : float ;
  rba : float
}

let string_of x =
  match x with
  | A -> "A"
  | B -> "B"

module S =
  struct
    type nonrec t = t
    type nonrec param = param
    type aux = unit
    let atob_rate par (t : float) x =
      match x with
      | A -> par.rab
      | B -> 0.

    let atob_modif par rng (t : float) x =
      match x with
      | A -> B
      | B -> invalid_arg "B but A to B"

    let btoa_rate par (t : float) x =
      match x with
      | A -> 0.
      | B -> par.rba

    let btoa_modif par rng (t : float) x =
      match x with
      | A -> invalid_arg "A but B to A"
      | B -> A

    let evl = [ (atob_rate, atob_modif) ; (btoa_rate, btoa_modif) ]
  end


module C =
  struct
    type nonrec t = t
    type aux = unit
    type nonrec param = param
    let header = [["t" ; "x"]]
    let line par t x = [string_of_float t ; string_of x]
  end

module O = Sim.Out.Csv (C)


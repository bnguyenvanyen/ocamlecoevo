(* Poisson counting process dependent on a coin toss *)
type t = int

module type HIDDENSYS =
  sig
    type t
    type param
    val rate : param -> t -> float
  end

module Make (S : HIDDENSYS) :
  (Sim.Stopped_poisson.DEPSYSTEM with type t = int
                                  and type hidden = S.t
                                  and type param = S.param)
    =
  struct
    type nonrec t = t
    type hidden = S.t
    type aux = int
    type param = S.param
    let inc_rate par t y x =
      S.rate par y
    let inc_modif rng par t y x =
      x + 1
    let evl = [(inc_rate, inc_modif)]
  end


module Fixed = Make(
  struct
    type t = unit
    type param = float
    let rate lbd () = lbd
  end
)

(* the simplest one *)
module Dep = Make(
  struct
    type t = float
    type param = unit
    let rate () y = y
  end
)


module Dep_cointoss = Make (
  struct
    type t = Cointoss.t
    type param = {
      ra : float ;
      rb : float
    }
    let rate par y =
      match y with
      | Cointoss.A -> par.ra
      | Cointoss.B -> par.rb
  end
)


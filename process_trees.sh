#!/bin/bash
# $1 : file to read from
IN=$1
# BASE_OUT is IN minus .newick at the end
BASE_OUT="$(dirname "$IN")/$(basename "$IN" .newick)"
# K line number
K=1
# N tree number
N=1
while read -r line; do
  if [ "$K" == "1" ] ; then
    K=2
  elif [ "$K" == "2" ] ; then
    echo "$line" | _build/default/tree/src/plot_unit_tree.exe > "${BASE_OUT}_$N.svg"
    echo "$line" | _build/default/tree/src/nlclft.exe > "${BASE_OUT}_$N.nlclft.csv"
    N=$[N+1]
    K=3
  elif [ "$K" == "3" ] ; then
    K=1
  fi
done <"$IN"


open Sig

type t = Lacaml.D.vec


type algparam = {
  h0 : U.closed_pos ;
  delta : U.closed_pos ;
  min_step : U.closed_pos ;
  max_step : U.closed_pos
}


val specl : (string * algparam Lift_arg.spec * string) list


val default : algparam


val integrate :
  ?tmp:(t * t * t * t * Lacaml.D.Mat.t) ->
  ?apar:algparam ->
  int ->
  (t -> bool) ->
  (z:t -> t -> unit) ->
  (z:t -> 'a U.pos -> t -> t) ->
  'b U.anypos ->
  t ->
    ('c U.pos * t)

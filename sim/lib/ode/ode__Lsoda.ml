(** Provide an interface to [Odepack.lsoda] similar to other modules. 
 *
 *  Because Odepack provides a different system, where one advances
 *  the solution to the times one desires.
 *  That is fundamentally different from our 'next' system.
 *  It also requires the user to provide the starting condition
 *  and final time to start the solver.
 *)

open Sig
module O = Odepack

type vofon =
  | Vec of O.vec
  | Pos of U.closed_pos
  | Nothing

type algparam = {
  atol : vofon ;
  rtol : vofon ;
  debug : bool option ;
  mxstep : int option ;
  mimic_h : U.closed_pos ;
}


module Term =
  struct
    open Cmdliner

    let atol =
      let doc =
        "Absolute tolerance parameter."
      in
      let get apar =
        match apar.atol with
        | Vec _ ->
            failwith "Not_implemented"
        | Nothing ->
            None
        | Pos x ->
            Some x
      in
      let set xo apar =
        { apar with atol =
            match xo with
            | None ->
                Nothing
            | Some x ->
                Pos x
        }
      in
      let conv = Arg.some (U.Arg.positive ()) in
      U.Arg.Update.Capture.opt_config get set conv ~doc "ode-atol"

    let rtol =
      let doc =
        "Relative tolerance parameter."
      in
      let get apar =
        match apar.rtol with
        | Vec _ ->
            failwith "Not_implemented"
        | Nothing ->
            None
        | Pos x ->
            Some x
      in
      let set xo apar =
        { apar with rtol =
            match xo with
            | None ->
                Nothing
            | Some x ->
                Pos x
        }
      in
      let conv = Arg.some (U.Arg.positive ()) in
      U.Arg.Update.Capture.opt_config get set conv ~doc "ode-rtol"

    let mimic_h =
      let doc =
        "Integration step size."
      in
      let get apar =
        apar.mimic_h
      in
      let set x apar =
        { apar with mimic_h = x }
      in
      U.Arg.Update.Capture.opt_config get set (U.Arg.positive ()) ~doc "ode-h"

    let mxstep =
      let doc =
          "Maximum number of steps allowed during one call to the solver. " 
        ^ "Defaults to 500."
      in
      let get apar =
        apar.mxstep
      in
      let set n apar =
        { apar with mxstep = n }
      in
      let conv = Arg.some Arg.int in
      U.Arg.Update.Capture.opt_config get set conv ~doc "ode-mxstep"

    let apar =
      let f atol rtol mimic_h mxstep ~config apar =
        apar
        |> atol ~config 
        |> rtol ~config
        |> mimic_h ~config
        |> mxstep ~config
      in
      Term.(const f
          $ atol
          $ rtol
          $ mimic_h
          $ mxstep
      )
  end


let specl = [
  ("-atol",
   Lift_arg.Float (fun apar x -> { apar with atol = Pos (F.Pos.of_float x) }),
   "Absolute tolerance parameter.") ;
  ("-rtol",
   Lift_arg.Float (fun apar x -> { apar with rtol = Pos (F.Pos.of_float x) }),
   "Relative tolerance parameter.") ;
  ("-h",
   Lift_arg.Float (fun apar x -> { apar with mimic_h = F.Pos.of_float x }),
   "Integration step size.") ;
  ("-mxstep", Lift_arg.Int (fun apar n -> { apar with mxstep = Some n }),
   "Maximum number of steps allowed during one call to the solver. Defaults to 500.") ;
]

let default = {
  atol = Nothing ;
  rtol = Nothing ;
  debug = None ;
  mxstep = None ;
  mimic_h = F.Pos.of_float 1e-3 ;
}

let tol : vofon -> (float option * O.vec option) = function
  | Vec v ->
      (None, Some v)
  | Pos x ->
      (Some (F.to_float x), None)
  | Nothing ->
      (None, None)


let integrate
  ?(apar=default)
  ?jac
  (domain : O.vec domain)
  field
  ?(t0=F.zero)
  x0 =
  let f t y grad =
    (* of_float_unsafe unsatisfactory but we don't want to check every time *)
    ignore (field ~result:grad (F.of_float_unsafe t) y)
  in
  let h = apar.mimic_h in
  let rtol, rtol_vec = tol apar.rtol in
  let atol, atol_vec = tol apar.atol in
  let debug = apar.debug in
  let mxstep = apar.mxstep in
  (* holds copy of y when needed (back to domain) *)
  let y_bis = Lac.Vec.make0 (Lac.Vec.dim x0) in
  (* holds result of iterations *)
  let res = Lac.Vec.make0 (Lac.Vec.dim x0) in
  let t0 = F.to_float t0 in
  let sol =
    O.lsoda ?rtol ?rtol_vec ?atol ?atol_vec ?debug ?mxstep ?jac f x0 t0 t0
  in
  let next t y =
    let t' = F.Op.(t + h) in
    O.advance ~time:(F.to_float t') sol ;
    let t'' = F.of_float_unsafe (O.time sol) in
    (* y' should share memory with y ? how to ensure that ? *)
    let y' = O.vec sol in
    let y' =
      if not (domain.is_in y') then begin
        (* copy y into y_bis *)
        let y_bis = Lac.copy ~y:y_bis y in
        domain.back_to ~result:res y_bis y' ;
        res
      end else
        Lac.copy ~y:res y'
    in
    assert F.Op.(t'' = t') ;
    (t', y')
  in next

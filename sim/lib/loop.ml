open Sig


(* annoyingly we use this a lot *)
let nar = F.narrow
let pnar = F.Pos.narrow


(* let em9 = F.Pos.of_float 1e-9 *)
let em8 = F.Pos.of_float 1e-8
let em6 = F.Pos.of_float 1e-6


let smallest ?(n=20) p t =
  assert F.Op.(t < F.infinity) ;
  let rec f n t =
    match n with
    | 0 ->
        failwith "exhausted payload"
    | _ ->
        let t' = F.Pos.mul F.two t in
        if p (nar t') then
          t'
        else
          f (n - 1) t'
  in
  let t0 = 
    match F.identify t with
    | Zero _ ->
        F.Pos.of_float 1e-1
    | Proba t ->
        pnar t
    | Pos t ->
        pnar t
    | Neg t ->
        F.Neg.neg t
  in
  f n t0


(* bissect between t and t + dt *)
let bissect ?(tol=em8) p t (dt : _ U.pos) =
  assert F.Op.(dt < F.infinity) ;
  let rec f t dt =
    if F.Op.(dt < tol) then
      F.add t dt
    else
      let dt' = F.Pos.div dt F.two in
      let t' = F.add t dt' in
      if p t' then
        (* left interval *)
        f t dt'
      else
        (* right interval *)
        f t' dt'
  in f (nar t) (pnar dt)


let simulate
    ~(next:('a, _) nextfun)
    ~(output:('a, 'b, _) output)
    ?(t0=F.zero)
    (x0:'a)
    (auxf:('a, 'c) auxfun)
    (p:('a, 'c) pred) =
  (* We need to call this function in many places to make the types
   * behave like we want them to (it should be a noop though) *)
  let pred x auxi t =
    let nauxi = auxf ~auxi (nar t) x in
    p (nar t) x nauxi
  in
  let bissect (t, x, auxi) (t', x', auxi') =
    let p = pred x auxi in
    (* we need to see if only the new state fulfills the condition *)
    if   (p t')
      && (p (F.Pos.of_anyfloat F.Op.(t' - em6))) then
      (* ok so we can actually bissect *)
      let t' =
        if F.Op.(t' = F.infinity) then
          nar (smallest p t)
        else
          nar t'
      in
      let dt = F.Pos.of_anyfloat F.Op.(t' - t) in
      let t'' = bissect p t dt in
      (nar t'', x, auxf ~auxi (nar t'') x)
    else
      (nar t', x', auxi')
  in
  let rec to_the_end t x auxi =
    output.out (F.narrow t) x ;
    let nt, nx = next (nar t) x in
    let nauxi = auxf ~auxi (nar nt) nx in
    if not (p (nar nt) nx nauxi) then
      to_the_end (nar nt) nx nauxi
    else
      let tf, xf, auxif = bissect (nar t, x, auxi) (nar nt, nx, nauxi) in
      (tf, xf, auxif)
  in
  output.start (F.narrow t0) x0 ;
  let auxi0 = auxf (nar t0) x0 in
  let tf, xf, _ = to_the_end (nar t0) x0 auxi0 in
  output.return (F.narrow tf) xf


let simulate_return ~next ?t0 x0 auxf pred =
  let output : ('a, 'b, _) output = {
    start = (fun _ _ -> ()) ;
    out = (fun _ _ -> ()) ;
    return = (fun t x -> (F.Pos.of_anyfloat t, x))
  } in
  simulate ~output ~next ?t0 x0 auxf pred


let simulate_until ~next ~output ?(t0 : _ U.anyfloat option) x0 (tf : _ U.anyfloat) =
  let auxf ?(auxi=()) _ _ = ignore auxi ; () in
  let pred t _ _ = F.Op.(t >= tf) in
  simulate ~output ~next ?t0 x0 auxf pred
  

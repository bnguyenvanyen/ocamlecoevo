(** Deterministic and stochastic simulations.
  
    This library provides deterministic and stochastic integrators.

    The {!Sim.Ode} module contains ODE solvers,
    and in particular Sim.Ode.Lsoda uses ocaml-odepack bindings
    to ODEPACK's LSODA.

    Time is represented through the {!Util.float} type.

    {!Sim.Csv} and {!Sim.Cadlag} provide output to Csv file,
    and accumulation of the trajectory in a list.

    The {!Sim.Ctmjp} module contains integrators for Continuous time
    pure Markov jump processes.

    {!Sim.Ode.Dopri5} is a classical ODE integrator
    using the Dormand-Prince method, using Lacaml.

    {!Sim.Ode.Midpoint} is the midpoint ODE integrator.

    {!Sim.Ode.Lsoda} calls on ODEPACK's LSODA solver.

    {!Sim.Ctmjp.Gill} implements Gillespie's stochastic simulation algorithm.

    {!Sim.Ctmjp.Euler_poisson} is another exact stochastic simulation algorithm.

    {!Sim.Prm} implements a Poisson Random Measure,
    which can be used for stochastic simulations,
    via {!Sim.Ctmjp.Prm_exact} and {!Sim.Ctmjp.Prm_approx}.
 *)


exception Simulation_error = Sig.Simulation_error

(** @canonical Sim.Sig *)
module Sig = Sig

(** @canonical Sim.Domain *)
module Domain = Domain

(** @canonical Sim.Ctmjp *)
module Ctmjp = Ctmjp

(** @canonical Sim.Ode *)
module Ode = Ode

(** @canonical Sim.Prm *)
module Prm = Prm

(** @canonical Sim.Prm_cheat *)
module Prm_cheat = Prm_cheat

(** @canonical Sim.Prm_internal *)
module Prm_internal = Prm_internal

(** @canonical Sim.Dbt *)
module Dbt = Dbt

(** @canonical Sim.Evm_internal *)
module Evm_internal = Evm_internal

(** @canonical Sim.Evm_internal_lazy *)
module Evm_internal_lazy = Evm_internal_lazy

(** @canonical Sim.Loop *)
module Loop = Loop

(** @canonical Sim.Cadlag *)
module Cadlag = Cadlag

(** @canonical Sim.Csv *)
module Csv = Sim__Csv

open Sig


let maybe_output ?(dt : _ U.anypos option) (output : (_ U.anyfloat -> 'a -> unit)) =
  (* previous printing time *)
  let t_r = ref F.zero in
  (* previous value of x (or None before init and after end) *)
  let xo_r = ref None in
  let maybe_output (t : _ U.anyfloat) x =
    match !xo_r with
    | None ->
        failwith "Sim.Csv.output : x not initialized\n"
    | Some x' ->
        begin match dt with
        | None ->
            output t x
        | Some dt ->
            (* until t, output with previous state x' *)
            while F.Op.((F.add !t_r dt) < t) do
              t_r := F.add !t_r dt ;
              output !t_r x'
            done ;
            (* from t, output with new state x *)
            (* FIXME should I use ~= ? *)
            if F.Op.((F.add !t_r dt) = t) then begin
              t_r := F.add !t_r dt ;
              output !t_r x
            end
        end
    ;
    (* update state to x *)
    xo_r := Some x
  in
  (t_r, xo_r, maybe_output)


let convert ?(dt : _ U.anypos option) ~header ~line ~chan =
  let chan = Csv.to_channel chan in
  let output (t : _ U.anyfloat) x =
    Csv.output_record chan (line (F.narrow t) x)
  in
  let t_r, xo_r, maybe_output = maybe_output ?dt output in
  let start (t0 : _ U.anyfloat) x0 =
    t_r := F.narrow t0 ;
    xo_r := Some x0 ;
    Csv.output_all chan header ;
    output t0 x0
  in
  let out (t : _ U.anyfloat) x =
    maybe_output t x
  in
  let return (tf : _ U.anyfloat) zf =
    maybe_output tf zf ;
    t_r := F.zero ;
    xo_r := None ;
    (F.narrow tf, zf)
  in
  Util.Out.{ start ; out ; return }


let convert_append ?(dt : _ U.anypos option) ~header ~line ~chan =
  let chan = Csv.to_channel chan in
  Csv.output_all chan header ;
  let f k =
    let output (t : _ U.anyfloat) x =
      Csv.output_record chan (line k (F.narrow t) x)
    in
    let t_r, xo_r, maybe_output = maybe_output ?dt output in
    let start (t0 : _ U.anyfloat) x0 =
      t_r := F.narrow t0 ;
      xo_r := Some x0 ;
      output t0 x0
    in
    let out (t : _ U.anyfloat) x =
      maybe_output t x
    in
    let return (tf : _ U.anyfloat) zf =
      maybe_output tf zf ;
      t_r := F.zero ;
      xo_r := None ;
      (F.narrow tf, zf)
    in
    Util.Out.{ start ; out ; return }
  in
  (
    f,
    (fun () -> Csv.close_out chan)
  )


(* compatible with cadlag *)
let convert_compat ?dt ~header ~line ~chan =
  let Util.Out.{ start ; out ; return } =
    convert ?dt ~header ~line ~chan
  in
  (* replace return *)
  let return tf zf =
    ignore (return tf zf) ;
    []
  in
  Util.Out.{ start ; out ; return }

(** Events data structure for Ctmjp.Prm_internal_lazy
 *
 *  Holds events and how to project the earlies occurrence time,
 *  and how to process them.
 *)


open Sig


(** if [f] is of type [update_fuel],
    then [f ~from_t ~to_t ~traj s] is the amount of fuel
    that remains at time [to_t] if at time [from_t] there was [s],
    and the rate of the event can be computed from the trajectory
    [traj] between [from_t] and [to_t]. *)
type 'traj update_fuel = (
  from_t:U._float ->
  to_t:U._float ->
  traj:'traj ->
  U.closed_pos ->
    U._float
)


(** if [f] is of type [project_time],
    then [f ~fuel x t] is the earliest possible time at which
    all of the [fuel] present at time [t] will have been consumed,
    even if [x] changes before then. *)
type 'a project_time = (
  fuel:U.closed_pos ->
  'a ->
  U._float ->
    U._float
)


(** if [f] is of type [update_state] *)
type 'a update_state = (
  U._float ->
  'a ->
    'a
)


type ('a, 'traj) elt = {
  last_update_time : U._float ;
  (** last time of update *)
  proj_occur_time : U._float ;
  (** earliest possible occurrence time *)
  rem_fuel : U.closed_pos ;
  (** remaining fuel after last update *)
  update_fuel : 'traj update_fuel ;
  (** how to update the remaining fuel of this event *)
  project_time : 'a project_time ;
  (** how to project the earlies possible occurrence time of this event *)
  update_state : 'a update_state ;
  (** how to update the state *)
  single : bool ;
  (** true if the event can happen only once *)
}


module type MAP =
  sig
    type key
    type +'a t

    val empty : 'a t
    val find : key -> 'a t -> 'a
    val add : key -> 'a -> 'a t -> 'a t
    val remove : key -> 'a t -> 'a t
    val extract : key -> 'a t -> 'a * 'a t
    val modify : key -> ('a -> 'a) -> 'a t -> 'a t
  end


module type S =
  sig
    type ('a, 'traj) t

    type color
    type prm

    val empty : ('a, 'traj) t

    val find : color -> ('a, 'traj) t -> ('a, 'traj) elt

    val find_next : ('a, 'traj) t -> color * ('a, 'traj) elt

    val remove : color -> ('a, 'traj) t -> ('a, 'traj) t

    (** [remaining_fuel elt traj t] is the remaining fuel for [elt]
        at time [t] *)
    val remaining_fuel :
      ('a, 'traj) elt ->
      'traj ->
      _ U.anyfloat ->
        U._float

    (** [add upd_fuel proj_t upd_state single c t x (evs, nu)]
        adds a new event of color [c]
        defined by [upd_fuel], [upd_proj_t], [upd], and [single]
        to [evs] at time [t] and state [x]. *)
    val add :
      'traj update_fuel ->
      'a project_time ->
      'a update_state ->
      bool ->
      color ->
      _ U.anyfloat ->
      'a ->
      ('a, 'traj) t * prm ->
        ('a, 'traj) t * prm

    (** [update_focal nu c t x evs] updates the event of color [c]
        after it has happened at time [t] with new state [x].
        If the event happens only once, it is removed,
        otherwise a new point is taken from [nu]. *)
    val update_focal :
      prm ->
      color ->
      _ U.anyfloat ->
      'a ->
      ('a, 'traj) t ->
        ('a, 'traj) t * prm

    (** [reproject_focal ~color ~fuel t x evs] updates the time of the event
        of color [c] after it did not happen at time [t]
        and had [fuel] left. *)
    val reproject_focal :
      color:color ->
      fuel:_ U.anypos ->
      _ U.anyfloat ->
      'a ->
      ('a, 'traj) t ->
        ('a, 'traj) t
  end


module Fmap = Evm_internal.Fmap


module Make
  (Color : COLOR)
  (Colormap : MAP with type key = Color.t)
  (Prm : INTERNAL_PRM with type color = Color.t
                       and type point = Prm_internal.Point.t) :
  (S with type color = Color.t
      and type prm = Prm.t) =
  struct
    type color = Color.t
    type prm = Prm.t

    type ('a, 'traj) t = {
      elts : ('a, 'traj) elt Colormap.t ;
      (** the events indexed by their color *)
      times : color Fmap.t ;
      (** the ordered projected occurrence times and corresponding colors *)
    }

    let empty = {
      elts = Colormap.empty ;
      times = Fmap.empty ;
    }

    let find c { elts ; _ } =
      Colormap.find c elts

    let find_next evs =
      let t, color = Fmap.min_binding evs.times in
      let elt = Colormap.find color evs.elts in
      (* check the time is exactly the right one *)
      assert (t = elt.proj_occur_time) ;
      color, elt

    let init update_fuel project_time update_state single nu c t x =
      let t = F.narrow t in
      (* the return nu needs to come back out,
       * in case there are new points *)
      let rem_fuel, nu' = Prm.find_min_point c nu in
      let nu' = Prm.remove_min_point c nu' in
      let proj_occur_time = project_time ~fuel:rem_fuel x t in
      let elt = {
        last_update_time = t ;
        proj_occur_time ;
        rem_fuel ;
        update_fuel ;
        project_time ;
        update_state ;
        single ;
      }
      in
      elt, nu'

    let add upd_fuel proj_time upd_state single c t x (evs, nu) =
      let elt, nu' =
        init upd_fuel proj_time upd_state single nu c t x
      in
      let times = Fmap.add elt.proj_occur_time c evs.times in
      let elts = Colormap.add c elt evs.elts in
      { times ; elts }, nu'

    let remove c evs =
      let elt, elts = Colormap.extract c evs.elts in
      let times = Fmap.remove elt.proj_occur_time evs.times in
      { times ; elts }

    (* remaining fuel for [elt] at [t] *)
    let remaining_fuel elt traj t =
      elt.update_fuel
        ~from_t:elt.last_update_time
        ~to_t:(F.narrow t)
        ~traj
        elt.rem_fuel

    (* when the event happens *)
    let update_focal nu c t x evs =
      let elt = Colormap.find c evs.elts in
      if elt.single then
        (* if the event can happen only once, remove it *)
        let elts = Colormap.remove c evs.elts in
        let times = Fmap.remove elt.proj_occur_time evs.times in
        { elts ; times }, nu
      else
        (* otherwise get the next point and re init *)
        let elt', nu' = init
          elt.update_fuel
          elt.project_time
          elt.update_state
          elt.single
          nu c t x
        in
        let elts = Colormap.modify c (fun _ -> elt') evs.elts in
        let times =
          Fmap.update
            elt.proj_occur_time
            elt'.proj_occur_time
            c
            evs.times
        in
        { elts ; times }, nu'

    (* when the event is considered, but does not happen *)
    let reproject_focal ~color ~fuel t x evs =
      let fuel = F.Pos.narrow fuel in
      let t = F.narrow t in
      (* update last_update_time, rem_fuel, and proj_occur_time *)
      let elt = Colormap.find color evs.elts in
      let proj_occur_time = elt.project_time ~fuel x t in
      let times =
        Fmap.update elt.proj_occur_time proj_occur_time color evs.times
      in
      let elt' = { elt with
        last_update_time = t ;
          rem_fuel = fuel ;
          proj_occur_time ;
      }
      in
      let elts = Colormap.add color elt' evs.elts in
      { elts ; times }
  end

open Sig


let convert ?dt ~conv =
  let conv' t x =
    (F.narrow t, conv t x)
  in
  (* how do we get cad back ? *)
  let cad_r = ref [] in
  let record (t : _ U.anyfloat) x =
    cad_r := (conv' (F.narrow t) x) :: !cad_r
  in
  let t_r = ref F.zero in
  let xo_r = ref None in
  let maybe_record (t : _ U.anyfloat) x =
    match !xo_r with
    | None ->
        failwith "Sim.Cadlag.output : x not initialized\n"
    | Some x' ->
        begin match dt with
        | None ->
            record t x
        | Some dt ->
            (* until t, record previous state x' *)
            while F.Op.((F.add !t_r dt) < t) do
              t_r := F.add !t_r dt ;
              record !t_r x'
            done ;
            (* from t, record new state x *)
            (* FIXME should I use ~= ? *)
            if F.Op.(F.add !t_r dt = t) then begin
              t_r := F.add !t_r dt ;
              record !t_r x
            end
        end
    ;
    xo_r := Some x
  in
  let start t0 x0 =
    let t0 = F.narrow t0 in
    t_r := t0 ;
    xo_r := Some x0 ;
    record t0 x0
  in
  let out t x =
    maybe_record t x
  in
  let return tf xf =
    maybe_record tf xf ;
    t_r := F.zero ;
    xo_r := None ;
    List.rev !cad_r
  in
  Util.Out.{ start ; out ; return }

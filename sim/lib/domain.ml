open Sig


type 'a t = 'a domain = {
  is_in : 'a -> bool ;
  back_to : result:'a -> 'a -> 'a -> unit ;
}


module Vec =
  struct
    type nonrec t = Lac.Vec.t t

    let find_first_cross n z z' =
      (* find the point at which the boundary is crossed,
       * in time and in R^d.
       * Happens when a coordinate first becomes negative.
       * So for the coordinate with largest (z.{i} - z'.{i}) / z.{i}
       *)
      if Lac.Vec.min z' >= 0. then
        None
      else begin
        let y = Lac.Vec.make0 n in
        for i = 1 to n do
          y.{i} <- (z.{i} -. z'.{i}) /. z.{i}
        done
        ;
        (* argmax of y *)
        let max_r = ref (0, neg_infinity) in
        for i = 1 to n do
          let _, max = !max_r in
          if y.{i} > max then
            max_r := (i, y.{i})
        done
        ;
        begin match assert (snd !max_r >= 1.) with
        | () ->
            ()
        | exception (Assert_failure _ as e) ->
            Printf.eprintf "Sim.Domain.Vec.find_first_cross assert_failure\n" ;
            Printf.eprintf "z :\n" ;
            Lacaml.Io.pp_fvec Format.err_formatter z ;
            Format.pp_print_newline Format.err_formatter () ;
            Printf.eprintf "z' :\n" ;
            Lacaml.Io.pp_fvec Format.err_formatter z' ;
            Format.pp_print_newline Format.err_formatter () ;
            Printf.eprintf "y :\n" ;
            Lacaml.Io.pp_fvec Format.err_formatter y ;
            Format.pp_print_newline Format.err_formatter () ;
            raise e
        end ;
        Some (fst !max_r)
      end

    (* ways to reuse memory more ?
     * (could that be typed to be safe ?) *)
    let crossing_point n z z' i =
      assert (z.{i} >= 0.) ;
      assert (z'.{i} < 0.) ;
      let fract = z.{i} /. (z.{i} -. z'.{i}) in
      (* z_cross = z + fract * (z' - z) *)
      let dz = Lac.Vec.sub ~n z' z in
      Lac.scal ~n fract dz ;
      let z_cross = Lac.Vec.add ~n z dz in
      (* exactly 0. for the crossing dimension *)
      z_cross.{i} <- 0. ;
      z_cross

    (* reflecting boundary condition for the positive quadrant *)
    (* (this doesn't work for the solution to remain in the simplex) *)
    let reflect_back_to_pos dims ~result z z' =
      (* z is positive, but z' is not *)
      begin match
        Lac.Vec.iter (fun x -> assert (x >= 0.)) z
      with
      | () ->
          ()
      | exception Assert_failure _ ->
          invalid_arg "x negative"
      end
      ;
      let rec reflect z z' =
        match find_first_cross dims z z' with
        | None ->
            (* z' already positive *)
            z'
        | Some i ->
            let z_cross = crossing_point dims z z' i in
            z'.{i} <- ~-. (z'.{i}) ;
            reflect z_cross z'
      in
      ignore (reflect z (Lac.copy ~y:result z'))
  end

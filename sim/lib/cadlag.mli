(** Iterate through the simulation of a stochastic process,
    returning the trajectory as a list. *)

open Sig


(** [convert ~conv ~dt] is the tuple of functions needed to
 *  store information in a ref. *)
val convert :
  ?dt: _ U.anypos ->
  conv: (U._float -> 'a -> 'b) ->
    ('a, (U._float * 'b) list, _) output

(** Iterate through the simulation of a stochastic process *)

open Sig


(** [simulate ~next ~output ?t0 x0 auxf p]
    simulates the system defined by the stepping function [next]
    from the initial condition [x0] until the predicate [p] becomes true,
    with a random generator seeded by [seed] (self-seeded by default).
    Before, during, and after the simulation,
    it calls [output.start], [output.out] and [output.return]. *)
val simulate :
  next : ('a, _) nextfun ->
  output : ('a, 'b, F._float) output ->
  ?t0: _ U.anyfloat ->
  'a ->
  ('a, 'c) auxfun ->
  ('a, 'c) pred ->
    'b


val simulate_return :
  next : ('a, _) nextfun ->
  ?t0: _ U.anyfloat ->
  'a ->
  ('a, 'c) auxfun ->
  ('a, 'c) pred ->
    closed_pos * 'a


(** [simulate_until ~next ~output ?t0 x0 tf]
    simulates the system defined by the stepping function [next]
    from the initial condition [x0] until the final time [tf],
    with a random generator seeded by [seed] (self-seeded by default).
    Before, during, and after the simulation,
    it calls [out_start], [out] and [out_end], which by default do nothing.
    Finally it returns the final time and state reached by the system. *)
val simulate_until :
  next : ('a, _) nextfun ->
  output : ('a, 'b, F._float) output ->
  ?t0: _ U.anyfloat ->
  'a ->
  _ U.anyfloat ->
    'b

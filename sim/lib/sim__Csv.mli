(** Iterate through the simulation of a stochastic process,
    and output the trajectory in Csv format *)

open Sig


(** [convert ?dt ~header ~line ~chan] is an output triplet to a CSV channel. *) 
val convert :
  ?dt: _ U.anypos ->
  header: string list list ->
  line: (U._float -> 'a -> string list) ->
  chan: out_channel ->
    ('a, U._float * 'a, F._float) output


(** [convert_append ?dt ~header ~line ~chan] is for repeated output
 *  to a CSV channel, and then closing the channel *)
val convert_append :
  ?dt: _ U.anypos ->
  header: string list list ->
  line: (int -> U._float -> 'a -> string list) ->
  chan: out_channel ->
    (int -> ('a, U._float * 'a, F._float) output)
  * (unit -> unit)



(** [convert_compat ~header ~line ~chan ~dt] is {!convert} but it returns
 *  an empty list, so that it is compatible with {!Cadlag.convert}. *)
val convert_compat :
  ?dt: _ U.anypos ->
  header: string list list ->
  line: (U._float -> 'a -> string list) ->
  chan: out_channel ->
    ('a, 'b list, F._float) output

(** Types and signatures reused elsewhere. *)
exception Simulation_error

module L = BatList
module H = BatHashtbl
module Lac = Lacaml.D


module U = Util
module F = U.Float
module I = U.Int


type rng = Random.State.t
type closed_pos = F.closed_pos F.t
(* type 'a proba = 'a Util.proba *)
type time = closed_pos


type 'a domain = {
  is_in : 'a -> bool ;
  back_to : result:'a -> 'a -> 'a -> unit ;
}


(** Those functions are used as arguments to other functions,
    so they should be as general (?) as possible *)
type ('a, 't) nextfun =
  U._float ->
  'a ->
    ('t U.anyfloat * 'a)


type ('a, 'b, 't) output = ('t U.anyfloat, 'a, 'b) Util.Out.t


type ('a, 'b) auxfun = ?auxi:'b -> U._float -> 'a -> 'b
type ('a, 'b) pred = (U._float -> 'a -> 'b -> bool)


module type COLOR =
  sig
    open Util.Interfaces

    type t
    include COMPARABLE with type t := t
    include EQUALABLE with type t := t
    include REPRABLE with type t := t
  end


module type POINT_BASE =
  sig
    type 'time t constraint 'time = _ U.anyfloat

    (** a point has a time *)
    val time : 'time t -> 'time

    (** a point's time can be changed *)
    val with_time : _ U.anyfloat -> _ t -> U._float t

    (** [vector pt] is the vector from the origin to [pt].
        @raise Invalid_argument if the time of [pt] is not positive. *)
    val vector : _ t -> U.closed_pos t

    (** points can be added *)
    val add : _ t -> _ t -> U._float t

    (** [volume ~vector] is the volume of the rectangular cuboid
        with the origin in one corner, and [vector] in the other. *)
    val volume :
      vector : U.closed_pos t ->
        _ U.pos

    (** [inside ~origin ~vector pt] is [true] if [pt]
        is in the rectangular cuboid formed by taking [origin] as one corner,
        and [add origin vector] as the other. *)
    val inside :
      origin : 's t ->
      vector : U.closed_pos t ->
      's t ->
        bool

    (** [lower pt pt'] is the lower (left) corner of the smallest cuboid
        containing both [pt] and [pt']. *)
    val lower : _ t -> _ t -> U._float t

    (** [upper pt pt'] is the upper (right) corner of the smallest cuboid
        containing both [pt] and [pt']. *)
    val upper : _ t -> _ t -> U._float t

    val columns : string list

    val extract : _ t -> string -> string

    (** a point can be represented as a string list. *)
    val line : _ t -> string list

    (** a point can be read from a CSV row. *)
    val read : Csv.Row.t -> U._float t option
  end


module type POINT =
  sig
    include POINT_BASE
    type color

    module Color : (COLOR with type t = color)
    module Colormap : (BatMap.S with type key = color)

    (** there is a zero point for every color *)
    val zero : color -> U._float t

    (** a point has a color *)
    val color : _ t -> color

    (** a point has a number *)
    val number : _ t -> int

    (** a point has a value *)
    val value : _ t -> _ U.pos

    (** a point's number can be changed *)
    val with_number : int -> 's t -> 's t

    (** a point's value can be changed *)
    val with_value : _ U.anypos -> 's t -> 's t

    (** a point can be randomly drawn,
        with the given color and number,
        and time and value from the volume bounded by [origin] and [vector].
        If [k] is not given, it should be 0. *)
    val rand_point :
      rng : U.rng ->
      ?k : int ->
      color ->
      origin : U._float t ->
      vector : U.closed_pos t ->
        U._float t
  end


module type COMPARABLE_POINT =
  sig
    include POINT

    val compare : _ t -> _ t -> int
  end


module type INTERNAL_POINT =
  sig
    type t

    (** a point has a time *)
    val time : t -> _ U.pos

    (** a point's time can be changed *)
    val with_time : _ U.anypos -> t -> t

    (** points can be added *)
    val add : t -> t -> t

    (** [volume ~vector] is the volume of the rectangular cuboid
        with the origin in one corner, and [vector] in the other. *)
    val volume :
      vector : t ->
        _ U.pos

    (** [inside ~origin ~vector pt] is [true] if [pt]
        is in the rectangular cuboid formed by taking [origin] as one corner,
        and [add origin vector] as the other. *)
    val inside :
      origin : t ->
      vector : t ->
      t ->
        bool

    (** [lower pt pt'] is the lower (left) corner of the smallest cuboid
        containing both [pt] and [pt']. *)
    val lower : t -> t -> t

    (** [upper pt pt'] is the upper (right) corner of the smallest cuboid
        containing both [pt] and [pt']. *)
    val upper : t -> t -> t

    val columns : string list

    val extract : t -> string -> string

    (** a point can be represented as a string list. *)
    val line : t -> string list

    (** a point can be read from a CSV row. *)
    val read : Csv.Row.t -> t option
    val compare : t -> t -> int

    val make : _ U.anypos -> t

    (** a point can be randomly drawn,
        with the given color, and time uniform in
        the volume bounded by [origin] and [vector].
     *)
    val rand_point :
      rng : U.rng ->
      origin : t ->
      vector : t ->
        t
  end


(** Module types to build Poisson Random Measure module types *)

(** BASE module types include the functions common to all users.
    They don't include anything with points, because there are no points
    in the Prm_fake cheater implementation.
  
    POINTS module types add functions for when you do have points
    (a lot of them are only exposed to be tested in sim/tests)
  
    RAND module types include random creation and modification functions
  
    REVEAL module types include functions for hiding, revealing and grafting
    sub slices.
  
    MORE functions include functions I would like to not have to expose,
    but am still forced to expose, either because of sim/tests,
    or because of some users in the other libraries
    TODO : remove these uses I can
  
 *)

(** Smallest kind of slice : base functions *)
module type UKSL_BASE =
  sig
    type t
    type color
    type bounds

    (** a uk slice has a color *)
    val color : t -> color

    (** a uk slice has a number *)
    val number : t -> int

    (** a uk slice has a count of points *)
    val count : t -> _ U.posint

    (** [count_under uksl u] counts the points of [uksl] under [u] *)
    val count_under : t -> _ U.anypos -> _ U.posint

    (** a uk slice has bounds *)
    val bounds : t -> bounds

    (** a uk slice spans a range *)
    val range : t -> _ U.pos * _ U.pos

    (** a uk slice covers a volume *)
    val volume : t -> _ U.pos

    (** a uk slice has a number of points by unit of volume *)
    val density : t -> _ U.pos

    (** a uk slice has a key (in a color slice) *)
    val key : t -> _ U.pos * int
  end


module type UKSL_RAND =
  sig
    type t
    type color
    type bounds

    (** a uk slice can be randomly drawn
        under the homogeneous Poisson process *)
    val rand_draw : rng:U.rng -> color -> bounds -> t

    (** [adjust_density ~rng dlambda uksl] changes the number of points
        in [uksl] by Poisson([volume uksl] * [dlambda]) *)
    val adjust_density : rng:U.rng -> _ U.anyfloat -> t -> t

    (** [adjust_log_pd_ratio uksl uksl'] is the ratio of the log densities
        of [uksl] and [uksl'] under the PRM measure *)
    val adjust_log_pd_ratio : t -> t -> U._float

    (** [adjust_intensity ~rng lbd' uksl] redraws [uksl] to be
        of intensity [lbd'] by adding or removing points. *)
    val adjust_intensity : rng:U.rng -> _ U.anypos -> t -> t

    (** [log_pd_ratio_intensity (uksl, lbd) (uksl', lbd')] is the ratio
        of the log densities of [uksl] of intensity [lbd]
        and [uksl'] of intensity [lbd'].
        The densities are taken w.r.t a PRM(1). *)
    val log_pd_ratio_intensity :
      (t * _ U.anypos) ->
      (t * _ U.anypos) ->
        U._float
  end


module type UKSL_POINTS =
  sig
    type t
    type color
    type bounds
    type points

    (** a uk slice has points *)
    val points : t -> points

    (** points can be added to a uk slice *)
    val add_points : points -> t -> t

    (** points can be removed from a uk slice *)
    val remove_points : points -> t -> t

    (** a uk slice can be created from specified points *)
    val create : color -> bounds -> points -> t
  end


(** Color slices (that include uk slices) *)

module type CSL_BASE =
  sig
    type t
    type color
    type bounds
    type uk_slice

    val color : t -> color

    val umax_revealed : t -> _ U.pos

    val kmax : t -> int

    val count : t -> _ U.posint

    (** [bounds csl] is the bounds of the csl *)
    val bounds : t -> bounds

    val volume : t -> _ U.pos

    val find : t -> (_ U.anypos * int) -> uk_slice

    (** [map_slices f csl] is [csl] with [f] applied to all of its
        revealed slices.
        [f] should not change the bounds of the uk slices. *)
    val map_slices :
      (uk_slice -> uk_slice) ->
      t ->
        t

    (** [fold_rvl_slices f csl x] is [f] folded over all the revealed slices
        of [csl]. *)
    val fold_rvl_slices :
      (uk_slice -> 'b -> 'b) ->
      t ->
      'b ->
        'b
  end


module type CSL_RAND =
  sig
    type t
    type color
    type bounds

    (** [rand_draw ~rng color bounds nuslices] is a graftable color slice
        with points generated under a homogeneous Poisson process
        of intensity [1.], and is otherwise like {!create}. *)
    val rand_draw :
      rng:U.rng ->
      color ->
      bounds ->
      U.anyposint ->
        t
  end


module type CSL_REVEAL =
  sig
    type t
    type points

    exception Already_hidden
    exception Already_revealed
    exception Not_all_revealed

    (** [hide_k csl k] hides the [k]-slices,
        and returns the newly hidden points.
        @raise Not_found if there are no slices of number [k].
        @raise Invalid_argument if [csl] is immutable.
        @raise Already_hidden if a slice is alrady hidden.
     *)
    val hide_k : t -> int -> points

    (** [hide_u csl] hides the uk_slices with maximum [u],
        and returns the newly hidden points.
        @raise Not_found if there are no slices or they are all hidden,
         which must not happen.
        @raise Invalid_argument if [csl] is immutable.
        @raise Already_hidden if only one revealed u slice remains.
     *)
    val hide_u : t -> points

    (** [reveal_k csl k] reveals a column of [k]-slices,
        and returns the newly revealed points.
        @raise Not_found if there are no slices with number [k].
        @raise Invalid_argument if [csl] is immutable
        @raise Already_revealed if [k]-slices are revealed. *)
    val reveal_k : t -> int -> points

    (** [reveal_u csl] reveals the next u-row of uk_slices,
        for all revealed k, and returns the revealed points.
        @raise Invalid_argument if [csl] is immutable.
        @raise Already_revealed if all u slices are already revealed.
     *)
    val reveal_u : t -> points

    (** [graft_k csl] grafts a new k-column [kmax + 1] of slices,
        and returns [kmax + 1] and the grafted points.
        @raise Invalid_argument if [csl] is immutable or ungraftable.
        @raise Not_all_revealed if not all existing k-columns are revealed. *)
    val graft_k : t -> int * points

    (** [graft_u csl] grafts a new u-row, and returns the grafted points.
        @raise Invalid_argument if [csl] is immutable or ungraftable.
        @raise Not_all_revealed if not all u-rows are revealed.
     *)
    val graft_u : t -> points
  end


module type CSL_POINTS =
  sig
    type t
    type color
    type bounds
    type points

    val points : t -> points
    val add_points : points -> t -> unit
    val remove_points : points -> t -> unit

    (** [create color bounds nuslices points] is an ungraftable color
        of color [color] over the volume bounded by [bounds],
        split into [nuslices] slices for [u],
        with points [points].
        The origin should have number [k = 0] and the total number
        of [k] slices [kmax] is given by the vector's number.
        The uk slices are then numeroted [0] to [kmax - 1]. *)
    val create :
      color ->
      bounds ->
      U.anyposint ->
      points ->
        t
  end


module type CSL_MORE =
  sig
    type t

    val u_boundaries : t -> _ U.pos list
  end


(** Time slices (that include color slices) *)

module type TSL_BASE =
  sig
    type t
    type color
    type c_slice

    val key : t -> U._float

    val range : t -> U._float * _ U.pos

    val count : t -> _ U.posint

    val volume : t -> _ U.pos

    (** [add_color csl tsl] is [tsl] with a new color slice [csl].
        If [csl]'s color is already present, [Invalid_argument] is raised.
        The result is mutable if [tsl] is mutable,
        or if [csl] is mutable. *)
    val add_color : c_slice -> t -> t

    val find : t -> color -> c_slice

    (** [with_slice csl tsl] is [tsl] with a replaced slice [csl]. *)
    val with_slice :
      c_slice ->
      t ->
        t

    val map_slices :
      (c_slice -> c_slice) ->
      t ->
        t

    val fold_slices :
      (c_slice -> 'b -> 'b) ->
      t ->
      'b ->
        'b
  end


module type TSL_POINTS =
  sig
    type t
    type point
    type vector
    type points

    val points : t -> points
    val add_points : points -> t -> unit
    val remove_points : points -> t -> unit

    (** [iter_points f tsl] applies [f] in turn to all points of the time slice [tsl]
        for its side-effects, with a refresh of the subsequent points
        when [f] returns [`Refresh]. *)
    val iter_points :
      (point -> [`Continue | `Refresh]) ->
      t ->
        unit

    (** [fold_points f tsl x] folds [f] in turn over all points of the time slice [tsl],
        with the subsequent points refreshed when [f] returns [`Refresh _]. *)
    val fold_points :
      (point -> 'b -> [`Continue of 'b | `Refresh of 'b]) ->
      t ->
      'b ->
        'b

    val create :
      key:U._float ->
      U._float * U.closed_pos ->
      (U.anyposint * vector) list ->
      points ->
        t
  end


module type TSL_REVEAL =
  sig
    type t
    type color

    val hide_k : t -> color -> int -> unit
    val hide_u : t -> color -> _ U.pos
    val reveal_k : t -> color -> int -> unit
    val reveal_u : t -> color -> _ U.pos
    val graft_k : t -> color -> int
    val graft_u : t -> color -> _ U.pos
  end


module type TSL_RAND =
  sig
    type t
    type point
    type vector
    type 'a colormap
    type c_slice

    val rand_draw :
      rngm:U.rng colormap ->
      key:U._float ->
      U._float * U.closed_pos ->
      (U.anyposint * vector) list ->
        t

    (** [rand_choose ~rng tsl] is a uniformly chosen color slice from [tsl]. *)
    val rand_choose :
      rng:Random.State.t ->
      t ->
        c_slice
  end


(** Poisson Random Measures, that contain all kinds of slices,
 *  like Russian dolls *)

module type PRM_BASE =
  sig
    type t

    type color

    module Point : (COMPARABLE_POINT with type color = color)

    type point = U._float Point.t
    type vector = U.closed_pos Point.t

    type bounds = {
      origin : point ;
      vector : vector ;
    }

    type 'a colormap = 'a Point.Colormap.t

    (** the type of a [u-k] slice of a color slice. *)
    type uk_slice

    (** the type of a color slice of a time slice. *)
    type c_slice

    (** the type of a time slice of a prm. *)
    type t_slice

    (** [rngm ~rng colors] associates a new PRNG to each color in [colors]. *)
    val rngm : rng:U.rng -> color list -> U.rng colormap

    (** {2 Basics} *)

    val copy : t -> t

    (** [time_range prm] is [(t0, dt)] the time range of [prm]. *)
    val time_range :
      t ->
        U._float * _ U.pos

    val colors : t -> color list

    val count : t -> _ U.posint

    val volume : t -> _ U.pos

    (** [logp prm] is the (approximate) log-probability of [prm]. *)
    val logp : t -> _ U.neg

    val nslices : t -> _ U.posint

    (** [slicing prm] is the range and number of time slices of [prm]. *)
    val slicing : t -> (U._float * _ U.pos) * _ U.posint

    val time_of_nth : t -> U.anyposint -> U._float

    val density : t -> _ U.pos

    (** [t_slice_at prm t] is the time slice of [prm] with key [t]
        @raise Not_found. *)
    val t_slice_at :
      t ->
      _ U.anyfloat ->
        t_slice

    (** [with_slice tsl nu] is [nu] with a time slice replaced by [tsl]. *)
    val with_slice : t_slice -> t -> t

    (** {2 Iterators} *)

    (** [map_uk_slices f prm tsl csl] replaces each [u] slice [usl] of [csl]
        by [f usl]. *)
    val map_uk_slices :
      (uk_slice -> uk_slice) ->
      t ->
      t_slice ->
      color ->
        t

    (** [map_slices f prm] replaces each [t] slice [tsl] of [prm] by [f tsl]. *)
    val map_slices :
      (t_slice -> t_slice) ->
      t ->
        t

    (** [iter_slices f prm] applies [f] in turn to all time slices
        of [prm]. *)
    val iter_slices :
      (t_slice -> unit) ->
      t ->
        unit

    (** [fold_slices f prm] folds [f] in turn over all the time slices
        of [prm]. *)
    val fold_slices :
      (t_slice -> 'b -> 'b) ->
      t ->
      'b ->
        'b

    (** {2 Hiding and revealing} *)

    (** [hide_downto tsl c r] hides points of color [c]
        from the time slice [tsl] in [prm],
        so that all remaining points have a value strictly smaller than [r],
        except for points in the smallest slice,
        which always stay revealed.
        If [tsl] is immutable, [Invalid_argument] is raised. *)
    val hide_downto :
      t_slice ->
      color ->
      _ U.anypos ->
        unit

    (** [reveal_upto tsl c r] reveals points of color [c]
        from the time slice [tsl] in [prm],
        so that no point with a value smaller than [r] stays hidden.
        If [tsl] is immutable, [Invalid_argument] is raised. *)
    val reveal_upto :
      t_slice ->
      color ->
      _ U.anypos ->
        unit

    (** [graft_upto tsl c r] grafts new slices of color [c]
        to the time slice [tsl] in [prm], so that [r] becomes covered.
        If not all [u] values are revealed, [Invalid_argument] is raised.
        If [tsl] is ungraftable, [Invalid_argument] is raised. *)
    val graft_upto :
      t_slice ->
      color ->
      _ U.anypos ->
        unit

    (** [reveal_graft_upto tsl c r] reveals if needed,
        then grafts, if needed, new slices of color [c]
        to the time slice [tsl] in [prm], so that [r] becomes covered.
        If [tsl] is immutable, [Invalid_argument] is raised.
        If [tsl] is ungraftable, [Invalid_argument] might be raised,
        if grafting was required to reach [r]. *)
    val reveal_graft_upto :
      t_slice ->
      color ->
      _ U.anypos ->
        unit

    (** {2 Random functions} *)

    (** [rand_draw ~rngm ~time_range ~ntslices ~vectors] is a graftable prm,
        with random points drawn for each color with the corresponding
        rng in [rngm], and otherwise like {!create}. *)
    val rand_draw :
      rngm : Random.State.t colormap ->
      time_range : (_ U.anyfloat * _ U.anypos) ->
      ntslices : U.anyposint ->
      vectors : (U.anyposint * vector) list ->
        t

    (** [rand_choose ~rng prm] is a uniformly chosen time slice from [prm]. *)
    val rand_choose :
      rng:Random.State.t ->
      t ->
        t_slice

    val redraw_distinct :
      rng:U.rng ->
      (rng:U.rng -> uk_slice -> uk_slice) ->
      int ->
      (color -> bool) ->
      t ->
        t

    val adjust_density :
      rng:U.rng ->
      (color * U.anyposint) list ->
      _ U.anyfloat ->
      t ->
        (color * U._float * t)

    (** {2 Merging and splitting} *)

    (** [merge prm prm'] is a [t] where for each [t_slice],
        the color slices of [prm'] are added to those of [prm].
        (with {!Tsl.add_color}).
        If [prm] and [prm'] don't cover the same range, have color(s)
        in common, or have a different time slicing,
        then [Invalid_argument] is raised. *)
    val merge : t -> t -> t

    (** [partition f prm] is [(prm_filt, prm_rem)]
     *   where [prm_filt] contains the colors that pass the predicate [f],
     *   and [prm_rem] contains the remaining colors. *)
    val partition : (color -> bool) -> t -> t * t

    val split : int -> t -> t list

    val concat : t list -> t

    (** {2 Output (and input)} *)

    val columns_grid : string list

    val extract_grid : uk_slice -> string -> string

    val output_grid : U.Csv.out_channel -> t -> unit

    module Slices : (Util.Interfaces.SEQ with type elt = uk_slice
                                          and type t = t)

    val columns_csl : string list

    val extract_csl : c_slice -> string -> string

    module Cslices : (Util.Interfaces.SEQ with type elt = c_slice
                                           and type t = t)
  end


module type PRM_POINTS =
  sig
    type t
    type point
    type vector

    module Ps : (BatSet.S with type elt = point)

    (** [rand_points_from ~rng points] is a point drawn uniformly from [points]. *)
    val rand_point_from : rng:U.rng -> Ps.t -> point

    (** [create ~time_range ~ntslices ~vectors points] is an ungraftable prm
     *  over the time range [time_range] with [ntslices] time slices,
     *  and for each color [c] and value [(umax, n_uslices)] of [cranges],
     *  an [u] range of [0.] to [umax] and [n_uslices] u slices.
     *  with the points [points]. *)
    val create :
      time_range : (_ U.anyfloat * _ U.anypos) ->
      ntslices : U.anyposint ->
      vectors : (U.anyposint * vector) list ->
      Ps.t ->
        t

    (** [output_points chan prm] outputs all points of [prm] in csv format
     *  to [chan]. *)
    val output_points : U.Csv.out_channel -> t -> unit

    (** [read ~time_range ~ntslices du0 fname] is some prm
     *  created as for {!create}, with points read from [chan],
     *  or [None] if no points are read. *)
    val read :
      time_range : (_ U.anyfloat * _ U.anypos) ->
      ntslices : U.anyposint ->
      _ U.anypos ->
      string ->
        t option

    (** [add_points points prm] adds the points that are in bounds to [prm].
     *  The points not in range are ignored. *)
    val add_points : Ps.t -> t -> t

    (** [remove_points points prm] removes the points that are
     *  in bounds from [prm].
     *  The points not in range are ignored.
     *  If a point in range is not present in [prm],
     *  an [Assertion_error] is raised. *)
    val remove_points : Ps.t -> t -> t

    module Points : (Util.Interfaces.SEQ with type elt = point
                                          and type t = t)
  end


(** PRM with kmax > 1
 *  (not super useful,
 *   as so far I've been using Prm_internal instead,
 *   which has a different interface) *)
module type PRM_K =
  sig
    type t
    type color
    type t_slice

    (** [hide_k tsl c k] hides all points of color [c] and number [k]
     *  from the time slice [tsl] in [prm].
     *  If there are no points of number [k], exception [Not_found] is raised.
     *  If [tsl] is immutable, exception [Invalid_argument] is raised.
     *  If number [k] is already hidden, the function does nothing. *)
    val hide_k :
      t_slice ->
      color ->
      int ->
        unit

    (** [reveal_k tsl c k] reveals points of color [c] and number [k]
     *  from the time slice [tsl] in [prm].
     *  If there are no points of number [k], exception [Not_found] is raised.
     *  If [tsl] is immutable, [Invalid_argument] is raised.
     *  If number [k] is already revealed, the function does nothing. *)
    val reveal_k :
      t_slice ->
      color ->
      int ->
        unit

    (** [graft_k tsl c] grafts new slices of color [c]
     *  to the time slice [tsl] in [prm], so that their number
     *  is the next unused number.
     *  If not all numbers are revealed, [Invalid_argument] is raised.
     *  If [tsl] is ungraftable, [Invalid_argument] is raised. *)
    val graft_k :
      t_slice ->
      color ->
        unit
  end


module type PRM_MORE =
  sig
    type t
    type color
    type t_slice
    type 'a colormap

    val add_many : (color * 'a) list -> 'a colormap

    (** [t_slice_of prm t] is the time slice of [prm] containing [t+]. *)
    val t_slice_of :
      t ->
      _ U.anyfloat ->
        t_slice

    val with_slice :
      t_slice ->
      t ->
        t
  end


module type UKSL_MINI =
  sig
    include UKSL_BASE
    include UKSL_RAND with type t := t
                       and type color := color
                       and type bounds := bounds
  end


(** A minimal PRM implementation (without points) *)

module type PRM_MINI =
  sig
    include PRM_BASE

    module Ps : (BatSet.S with type elt = point)

    module UKsl : (UKSL_MINI with type color = color
                              and type bounds = bounds
                              and type t = uk_slice)

    module Csl : (CSL_BASE with type color = color
                            and type bounds = bounds
                            and type uk_slice = uk_slice
                            and type t = c_slice)

    module Tsl : (TSL_BASE with type color = color
                            and type c_slice = c_slice
                            and type t = t_slice)

    (** We need these for Prm_approx tests and stuff *)

    val create :
      time_range : (_ U.anyfloat * _ U.anypos) ->
      ntslices : U.anyposint ->
      vectors : (U.anyposint * vector) list ->
      Ps.t ->
        t

    val read :
      time_range : (_ U.anyfloat * _ U.anypos) ->
      ntslices : U.anyposint ->
      _ U.anypos ->
      string ->
        t option
  end


module type UKSL_FULL =
  sig
    include UKSL_BASE
    include UKSL_RAND with type t := t
                       and type color := color
                       and type bounds := bounds
    include UKSL_POINTS with type t := t
                         and type color := color
                         and type bounds := bounds
  end


module type CSL_FULL =
  sig
    include CSL_BASE
    include CSL_REVEAL with type t := t
    include CSL_RAND with type t := t
                      and type color := color
                      and type bounds := bounds
    include CSL_POINTS with type t := t
                        and type color := color
                        and type bounds := bounds
                        and type points := points
    include CSL_MORE with type t := t
  end


module type TSL_FULL =
  sig
    include TSL_BASE
    include TSL_POINTS with type t := t
    include TSL_REVEAL with type t := t
                        and type color := color
    include TSL_RAND with type t := t
                      and type point := point
                      and type vector := vector
                      and type c_slice := c_slice
  end


(** A full PRM implementation,
 *  with points, and with internal functions targeted by sim/tests *)

module type PRM_FULL =
  sig
    include PRM_BASE
    include PRM_POINTS with type t := t
                        and type point := point
                        and type vector := vector
    include PRM_K with type t := t
                   and type color := color
                   and type t_slice := t_slice
    include PRM_MORE with type t := t
                      and type color := color
                      and type 'a colormap := 'a colormap
                      and type t_slice := t_slice

    module UKsl : (UKSL_FULL with type color = color
                              and type bounds = bounds
                              and type points = Ps.t
                              and type t = uk_slice)

    module Csl : (CSL_FULL with type color = color
                            and type bounds = bounds
                            and type points = Ps.t
                            and type uk_slice = uk_slice
                            and type t = c_slice)

    module Tsl : (TSL_FULL with type color = color
                            and type point = point
                            and type vector = vector
                            and type points = Ps.t
                            and type 'a colormap = 'a colormap
                            and type c_slice = c_slice
                            and type t = t_slice)

  end



module type INTERNAL_PRM =
  sig
    type t

    type color
    type point
    type +'a colormap

    module Color : (COLOR with type t = color)
    module Point : (INTERNAL_POINT with type t = point)
    module Points : (BatSet.S with type elt = point)

    val empty : unit -> t

    val copy : t -> t

    (** [grafted nu] are the grafted points of nu *)
    val grafted : t -> Points.t colormap

    val count_colors : t -> int

    val find_single : color -> t -> point

    val add_single : color -> point -> t -> t

    val add_ungraftable : color -> Points.t -> t -> t

    val add_graftable : rng:U.rng -> color -> Points.t -> t -> t

    val remove : color -> t -> t

    val add_graftable_point : color -> point -> t -> t

    val find_min_point : color -> t -> point * t

    val remove_min_point : color -> t -> t

    val iter : (color -> Points.t -> unit) -> t -> unit

    val iter_points : (color -> point -> unit) -> t -> unit

    val map_points : (color -> point -> point) -> t -> t

    val fold : (color -> Points.t -> 'a -> 'a) -> t -> 'a -> 'a

    val fold_points : (color -> point -> 'a -> 'a) -> t -> 'a -> 'a

    val modify_map : color -> (point -> point) -> t -> t

    val modify_filter_map : color -> (point -> point option) -> t -> t

    val replace_by_union : color -> color -> color -> t -> t
  end

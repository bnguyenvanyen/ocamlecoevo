(** Brownian motion increments,
 *
 *  to integrate against *)

(* Not a very useful reuse of Jump.Regular ? *)

open Sig

module Lac = Lacaml.D

module JR = Jump.Regular


type t = Lac.Vec.t Jump.Regular.t

type elt = U._float * Lac.Vec.t


let fold f dbts x0 =
  JR.fold_left (fun x t dbt -> f (F.of_float t, dbt) x) x0 dbts


let unfold_incr ?dt x f =
  let g x =
    U.Option.map (fun ((t, dbt), x) -> ((F.to_float t, dbt), x)) (f x)
  in
  L.unfold x g
  |> Jump.of_list
  |> JR.of_cadlag ?dt:(U.Option.map F.to_float dt)


let unfold x f =
  unfold_incr x f


let copy dbts =
  JR.map (fun _ v -> Lac.copy v) dbts


let columns ?int_to_name n =
  let int_to_name =
    match int_to_name with
    | None ->
        (fun i -> "dim" ^ string_of_int i)
    | Some f ->
        f
  in
  "t" :: (L.init n (fun i -> int_to_name (i + 1)))


let extract ?name_to_int (t, dbt) =
  let name_to_int =
    match name_to_int with
    | None ->
        (fun s ->
          match Scanf.sscanf s "dim%i" (fun i -> i) with
          | i ->
              i
          | exception Scanf.Scan_failure _ ->
              invalid_arg "unrecognized column"
        )
    | Some f ->
        f
  in
  function
  | "t" ->
      F.to_string t
  | s ->
      string_of_float dbt.{name_to_int s}


let draw_dbt ~rng ~(dt : _ U.anypos) ~n =
  let dbt = U.rand_multi_normal_std ~rng n in
  (* give it the right variance [dt] *)
  Lac.scal (sqrt (F.to_float dt)) dbt ;
  dbt


let rand_draw ~rng ~t0 ~(dt : _ U.anypos) ~tf ~n =
  unfold_incr ~dt (F.narrow t0) (fun t ->
    if F.Op.(t + dt <=~ tf) then
      let dbt = draw_dbt ~rng ~dt ~n in
      Some ((t, dbt), F.Op.(t + dt))
    else
      None
  )


let log_dens dbts =
  let dt = JR.dt dbts in
  let n = Lac.Vec.dim (L.hd (JR.values dbts)) in
  let c = Lac.Mat.identity n in
  (* set the correct variance [dt] *)
  Lac.Mat.scal dt c ;
  let cov = ref (U.Cov c) in
  JR.fold_left (fun logd _ dbt ->
    F.add logd (U.logd_multi_normal_center cov dbt)
  ) F.zero dbts



let split n dbts =
  let m = JR.length dbts in
  (* number of increments per split (except last) *)
  let nby = m / n in
  let rem = m mod n in
  assert (nby >= 1) ;
  (* TODO handle rem smarter *)
  assert (rem = 0) ;
  U.int_fold ~f:(fun dbtss k ->
    (JR.sub ~start:(k - 1) ~len:nby dbts) :: dbtss
  ) ~n ~x:[]


let concat dbtss =
  L.reduce (fun dbts dbts' ->
    match JR.append dbts dbts' with
    | res ->
        res
    | exception (Assert_failure _ as e) ->
        Printf.eprintf "Can't append a dbt[%f,%f] to a dbt[%f,%f]\n"
        (JR.t0 dbts') (JR.tf dbts') (JR.t0 dbts) (JR.tf dbts) ;
        raise e
  ) dbtss


(* directly changes dbts *)
let redraw ~rng ~n n_redraw dbts =
  let len = JR.length dbts in
  let dt = F.Pos.of_float (JR.dt dbts) in
  let choices = U.int_fold ~f:(fun set k ->
      BatSet.Int.add (k - 1) set
    ) ~x:BatSet.Int.empty ~n:len
  in
  (* [n_redraw] integers between [1] and [len] *)
  let ks =
    U.rand_subset
      (module BatSet.Int : BatSet.S with type elt = int
                                     and type t = BatSet.Int.t)
      ~rng
      choices
      n_redraw
  in
  BatSet.Int.iter (fun k ->
    JR.modify_nth k (fun _ -> draw_dbt ~rng ~dt ~n) dbts
  ) ks
  ;
  dbts


(* sets of pairs of ints *)
module I2s = BatSet.Make (struct
  type t = int * int
  let compare ((n1, m1) : t) (n2, m2) =
    let c = compare n1 n2 in
    if c = 0 then
      compare m1 m2
    else
      c
end)


(* directly changes dbts *)
let redraw_dim ~rng ~n n_redraw dbts =
  let len = JR.length dbts in
  let choices = U.int_fold_right ~f:(fun k ->
    U.int_fold_right ~f:(fun i ->
      I2s.add (k - 1, i)
    ) ~n
  ) ~n:len I2s.empty
  in
  let kis =
    U.rand_subset
      (module I2s : BatSet.S with type elt = int * int
                              and type t = I2s.t)
    ~rng
    choices
    n_redraw
  in
  let dt = F.Pos.of_float (JR.dt dbts) in
  I2s.iter (fun (k, i) ->
    JR.modify_nth k (fun dbt ->
      dbt.{i} <- U.rand_normal ~rng 0. dt ;
      dbt
    ) dbts
  ) kis
  ;
  dbts


let draw_sde_normal n drift diff sigma dt =
  let mem = Lac.Vec.make0 n in
  let mem' = Lac.Vec.make0 n in
  let mem'' = Lac.Vec.make0 n in
  let mem''' = Lac.Vec.make0 n in
  let draw rng t (th, z) (th', z') dbt =
    let b = drift ~result:mem th t z in
    let sgm = diff th t z in
    let b' = drift ~result:mem' th' t z' in
    let sgm' = diff th' t z' in
    (* (z - z') + (b - b') * dt + sgm * dbt *)
    (* b - b' -- don't need b in mem or b' in mem' anymore *)
    let db = Lac.Vec.sub ~z:mem b b' in
    Lac.scal dt db ;
    let dz = Lac.Vec.sub ~z:mem' z z' in
    let sgm_dbt = Lac.gemv ~y:mem'' sgm dbt in
    let e = Lac.Vec.add ~z:mem''' db dz in
    let e = Lac.Vec.add ~z:e e sgm_dbt in
    (* + N(0, v) *)
    let u = U.rand_multi_normal_std ~rng n in
    Lac.scal sigma u ;
    let e = Lac.Vec.add ~z:e e u in
    (* sgm' * dbt' = e *)
    (* solve for dbt' -- sgm' is a symmetric matrix *)
    Lac.sysv ~up:true ~nrhs:1 sgm' (Lac.Mat.from_col_vec e) ;
    e
  in draw


let print out dbts =
  let print_vec out v =
    let fmt = BatFormat.formatter_of_out_channel out in
    Lacaml.Io.pp_fvec fmt v ;
    BatFormat.pp_print_flush fmt ()
  in
  JR.print print_vec out dbts

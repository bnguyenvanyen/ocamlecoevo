open Sig


module Is = Prm.Is
module Fs = Prm.Fs
module PFs = Prm.PFs
module Fm = Prm.Fm
module PFIm = Prm.PFIm


module Make (Point : COMPARABLE_POINT) :
  (PRM_MINI with type color = Point.color
             and module Point = Point) =
  struct
    module Pre = U.Precompute ( )

    (* no need for points *)
    type color = Point.color
    type point = U._float Point.t
    type vector = U.closed_pos Point.t

    module Point = Point

    module Cs = BatSet.Make (Point.Color)
    module Cm = Point.Colormap

    module Ps = U.Setcard.Make (struct
      type t = U._float Point.t
      let compare = Point.compare
    end)

    type 'a colormap = 'a Cm.t

    type bounds = {
      origin : point ;
      vector : vector ;
    }
    
    (* copied from Sim.Prm *)

    let u0 { origin ; _ } =
      F.Pos.narrow (Point.value origin)

    let uf { origin ; vector } =
      F.Pos.add (Point.value origin) (Point.value vector)

    let volume { vector ; _ } =
      Point.volume ~vector

    (* assume we have the right to cut *)
    let cut_u ({ origin ; vector } as bds : bounds) =
      let du = Point.value vector in
      let left_du = Prm.prev_total_du du in
      let right_du = Prm.next_du left_du in
      let right_u0 = F.Pos.add (Point.value origin) left_du in
      ({ bds with vector = Point.with_value left_du vector },
       { vector = Point.with_value right_du vector ;
         origin = Point.with_value right_u0 origin })

    (* fold over u-cut bounds n times *)
    let bounds_fold n f x0 bounds =
      let leftmost_bounds, x' = U.int_fold ~f:(fun (bds, x) _ ->
          let lbds, rbds = cut_u bds in
          let x' = f rbds x in
          (lbds, x')
        ) ~x:(bounds, x0) ~n:(n - 1)
      in f leftmost_bounds x'

    let rngm ~rng colors =
      L.fold_left (fun m c ->
        Cm.add c (U.rand_rng rng) m
      ) Cm.empty colors

    let rng_of_color rngm c =
      Cm.find c rngm

    (* --- *)

    let rand_count ~rng bounds =
      let v = volume bounds in
      U.rand_poisson ~rng v

    module UKsl =
      struct
        type nonrec color = color
        type nonrec bounds = bounds

        type t = {
          color : color ;
          number : int ;
          bounds : bounds ;
          rng : U.rng ;  (* for cheating in count_under *)
          count : U._int ;  (* actually positive *)
        }

        let copy uksl = {
          uksl with rng = Random.State.copy uksl.rng
        }

        let color uksl =
          uksl.color

        let number uksl =
          uksl.number

        let bounds uksl =
          uksl.bounds

        let range uksl =
          let bds = uksl.bounds in
          (u0 bds, uf bds)

        let key uksl =
          let _, uf = range uksl in
          (uf, number uksl)

        let rng uksl =
          uksl.rng

        let count uksl : _ U.posint =
          I.promote_unsafe uksl.count

        let count_under uksl (u : _ U.anypos) : _ U.posint =
          let u0, uf = range uksl in
          let c = count uksl in
          if F.Op.(u <= u0) then
            I.zero
          else if F.Op.(u >= uf) then
            I.Pos.narrow c
          else
            (* in between : we cheat *)
            (* of_anyfloat wasteful but should not matter *)
            let p = F.Proba.of_anyfloat F.Op.((u - u0) / (uf - u0)) in
            (* this of_int is also wasteful *)
            I.Pos.of_int (U.rand_binomial ~rng:(rng uksl) c p)

        let volume uksl =
          volume (bounds uksl)

        let density uksl =
          F.Pos.div (I.Pos.to_float (count uksl)) (volume uksl)

        let logp uksl =
          (* Pre.logd_poisson_process (volume uksl) (count uksl) *)
          Pre.logp_poisson (volume uksl) (I.to_int (count uksl))

        let rand_draw ~rng color bounds =
          let number =
            assert (Point.number bounds.vector = 1) ;
            Point.number bounds.origin
          in
          let count = rand_count ~rng bounds in
          let rng = U.rand_rng rng in
          { color ; number ; bounds ; rng ; count }

        let create color bounds (count : _ U.posint) =
          let number =
            assert (Point.number bounds.vector = 1) ;
            Point.number bounds.origin
          in
          (* initialize a rng from nothing as we don't have a choice here *)
          let rng = U.rng None in
          { color ; number ; bounds ; rng ; count }

        let rand_move ~rng dk uksl =
          ignore rng ;
          let count = I.Pos.of_anyint I.Op.(uksl.count + dk) in
          { uksl with count }

        let adjust_density ~rng dlambda uksl =
          let vol = volume uksl in
          let dk =
            match F.identify (F.mul vol dlambda) with
            | F.Zero _ ->
                I.zero
            | F.Neg x ->
                I.neg (U.rand_poisson ~rng (F.Neg.neg x))
            | F.Proba x ->
                U.rand_poisson ~rng x
            | F.Pos x ->
                U.rand_poisson ~rng x
          in rand_move ~rng dk uksl

        let adjust_log_pd_ratio uksl uksl' =
          let vol = volume uksl in
          let k = count uksl in
          let k' = count uksl' in
          let dk = I.Op.(k' - k) in
          (* FIXME here this might be different,
           * since it's not actually the same process *)
          let dkv =
            if I.Op.(dk = I.zero) then
              F.zero
            else
              F.Op.(I.to_float dk * F.Pos.log vol)
          in
          let logfact = Pre.Int.Pos.logfact in
          F.Op.(
            logfact k - logfact k' + dkv
          )

        let adjust_intensity ~rng lbd' uksl =
          let vol = volume uksl in
          let k' = U.rand_poisson ~rng (F.Pos.mul vol lbd') in
          let k = count uksl in
          let dk = I.Op.(k' - k) in
          rand_move ~rng dk uksl

        let log_pd_ratio_intensity (uksl, lbd) (uksl', lbd') =
          let vol = volume uksl in
          (* assume that vol' = vol *)
          let k = count uksl in
          let k' = count uksl' in
          F.Op.(
              (lbd - lbd') * vol
            * ( I.to_float k * F.Pos.log lbd
              - I.to_float k' * F.Pos.log lbd')
          )

        let map_conserve_bounds f uksl =
          let uksl' = f uksl in
          assert (bounds uksl' = bounds uksl) ;
          uksl'

        let columns_grid =
              (L.map (fun s -> "origin_" ^ s) Point.columns)
            @ (L.map (fun s -> "vector_" ^ s) Point.columns)
            @ ["count"]

        let extract_grid uksl =
          function
          | "count" ->
              I.to_string (count uksl)
          | s ->
            begin match Scanf.sscanf s "origin_%s" (fun s' -> s') with
            | s' ->
                Point.extract uksl.bounds.origin s'
            | exception Scanf.Scan_failure _ ->
                begin match Scanf.sscanf s "vector_%s" (fun s' -> s') with
                | s' ->
                    Point.extract uksl.bounds.vector s'
                | exception Scanf.Scan_failure _ ->
                    invalid_arg "unrecognized column"
                end
            end
      end

    module Csl =
      struct
        exception Already_hidden
        exception Already_revealed
        exception Not_all_revealed

        type nonrec color = color
        type nonrec bounds = bounds
        type uk_slice = UKsl.t

        type t =
          | Immutable of {
              color : color ;
              umax : closed_pos ;
              uk_slices : UKsl.t PFIm.t ;
              us : PFs.t ;
              ks : Is.t ;
            }
          | Ungraftable of {
              color : color ;
              (* total maximum u value *)
              umax_total : closed_pos ;
              (* revealed maximum u value *)
              mutable umax_revealed : closed_pos ;
              uk_slices : UKsl.t PFIm.t ;
              mutable revealed_us : PFs.t ;
              mutable hidden_us : PFs.t ;
              mutable revealed_ks : Is.t ;
              mutable hidden_ks : Is.t ;
            }
          | Graftable of {
              color : color ;
              rng : U.rng ;
              (* total maximum u value *)
              mutable umax_total : closed_pos ;
              (* revealed maximum u value *)
              mutable umax_revealed : closed_pos ;
              mutable uk_slices : UKsl.t PFIm.t ;
              mutable revealed_us : PFs.t ;
              mutable hidden_us : PFs.t ;
              mutable revealed_ks : Is.t ;
              mutable hidden_ks : Is.t ;
            }
        
        (* texto from Prm *)

        let color =
          function
          | Immutable { color ; _ } ->
              color
          | Ungraftable { color ; _ } ->
              color
          | Graftable { color ; _ } ->
              color


        let umax_revealed =
          function
          | Immutable { umax ; _ } ->
              F.Pos.narrow umax
          | Ungraftable { umax_revealed ; _ } ->
              F.Pos.narrow umax_revealed
          | Graftable { umax_revealed ; _ } ->
              F.Pos.narrow umax_revealed

        let umax_total =
          function
          | Immutable { umax ; _ } ->
              F.Pos.narrow umax
          | Ungraftable { umax_total ; _ } ->
              F.Pos.narrow umax_total
          | Graftable { umax_total ; _ } ->
              F.Pos.narrow umax_total

        let time_range =
          let f uksls =
            (* assume valid csl : all uk_slices have the same time range *)
            let _, uksl = PFIm.any uksls in
            let { origin ; vector } = UKsl.bounds uksl in
            let t0 = Point.time origin in
            let dt = Point.time vector in
            (F.narrow t0, F.Pos.narrow dt)
          in
          function
          | Immutable { uk_slices ; _ } ->
              f uk_slices
          | Ungraftable { uk_slices ; _ } ->
              f uk_slices
          | Graftable { uk_slices ; _ } ->
              f uk_slices

        let kmax =
          let max2 rks hks =
            match Is.max_elt rks with
            | k ->
                begin match Is.max_elt hks with
                | k' ->
                    max k k'
                | exception Not_found ->
                    k
                end
            | exception Not_found ->
                begin match Is.max_elt hks with
                | k' ->
                    k'
                | exception (Not_found as e) ->
                    (* this should never happen *)
                    raise e
                end
          in
          function
          | Immutable { ks ; _ } ->
              Is.max_elt ks (* should not raise *)
          | Ungraftable { revealed_ks ; hidden_ks ; _ } ->
              max2 revealed_ks hidden_ks
          | Graftable { revealed_ks ; hidden_ks ; _ } ->
              max2 revealed_ks hidden_ks

        let slices =
          function
          | Immutable { uk_slices ; _ } ->
              uk_slices
          | Ungraftable { uk_slices ; _ } ->
              uk_slices
          | Graftable { uk_slices ; _ } ->
              uk_slices

        let revealed_us =
          function
          | Immutable { us ; _ } ->
              us
          | Ungraftable { revealed_us ; _ } ->
              revealed_us
          | Graftable { revealed_us ; _ } ->
              revealed_us

        let revealed_ks =
          function
          | Immutable { ks ; _ } ->
              ks
          | Ungraftable { revealed_ks ; _ } ->
              revealed_ks
          | Graftable { revealed_ks ; _ } ->
              revealed_ks

        let hidden_us =
          function
          | Immutable _ ->
              PFs.empty
          | Ungraftable { hidden_us ; _ } ->
              hidden_us
          | Graftable { hidden_us ; _ } ->
              hidden_us

        (*
        let hidden_ks =
          function
          | Immutable _ ->
              Is.empty
          | Ungraftable { hidden_ks ; _ } ->
              hidden_ks
          | Graftable { hidden_ks ; _ } ->
              hidden_ks
        *)

        let bounds csl =
          let umax = umax_total csl in
          let kmax = kmax csl in
          let _, uksl = PFIm.any (slices csl) in
          let bds = UKsl.bounds uksl in
          let origin =
            bds.origin
            |> Point.with_number 0
            |> Point.with_value F.zero
          in
          let vector =
            bds.vector
            |> Point.with_number (kmax + 1)
            |> Point.with_value umax
          in { origin ; vector }

        (* create a new identical record (that does not share memory) *)
        let copy =
          function
          | Immutable r ->
              Immutable {
                r with color = r.color ;
                       uk_slices = PFIm.map UKsl.copy r.uk_slices
              }
          | Ungraftable r ->
              Ungraftable {
                r with color = r.color ;
                       uk_slices = PFIm.map UKsl.copy r.uk_slices
              }
          | Graftable r ->
              Graftable {
                r with color = r.color ;
                       uk_slices = PFIm.map UKsl.copy r.uk_slices
              }

        let add_slice f k bounds (uksls, us, ks) =
          (* bounds is what it should be from the body of the double fold *)
          let u = uf bounds in
          let key = (u, k) in
          let uksl = f bounds in
          let uksls' = PFIm.add key uksl uksls in
          (* FIXME we actually need to do this only once *)
          let us' = PFs.add u us in
          let ks' = Is.add k ks in
          (uksls', us', ks')

        let rand_draw ~rng color bounds nuslices =
          (* a new rng is needed for each k slice to ensure reproducibility,
           * when the order of grafts changes *)
          let draw_uksl bds =
            UKsl.rand_draw ~rng color bds
          in
          assert (Point.number bounds.origin = 0) ;
          let uk_slices, us, ks = U.int_fold
            ~f:(fun uksluks j ->
              let k = pred j in
              let k_bounds = {
                origin = Point.with_number k bounds.origin ;
                vector = Point.with_number 1 bounds.vector ;
              }
              in
              bounds_fold (I.to_int nuslices) (add_slice draw_uksl k) uksluks k_bounds
            )
            ~x:(PFIm.empty, PFs.empty, Is.empty)
            ~n:(Point.number bounds.vector)
          in
          let umax = uf bounds in
          let rng = U.rand_rng rng in
          Graftable {
            color ;
            rng ;
            umax_total = umax ;
            umax_revealed = umax ;
            uk_slices ;
            revealed_us = us ;
            hidden_us = PFs.empty ;
            revealed_ks = ks ;
            hidden_ks = Is.empty ;
          }

        let create color bounds nuslices points =
          let create_uksl pts bds =
            let c = I.Pos.of_int (Ps.cardinal pts) in
            UKsl.create color bds c
          in
          let (uk_slices, us, ks), rem_pts = U.int_fold
            ~f:(fun (uksluks, pts) j ->
              let k = j - 1 in
              let k_bounds = {
                origin = Point.with_number k bounds.origin ;
                vector = Point.with_number 1 bounds.vector ;
              }
              in
              let f_fold ({ origin ; vector } as bds) (uksluks, pts) =
                let pts_in, pts_out = Ps.partition (fun pt ->
                    Point.inside ~origin ~vector pt
                  ) pts
                in
                let uksluks' = add_slice (create_uksl pts_in) k bds uksluks in
                (uksluks', pts_out)
              in
              bounds_fold (I.to_int nuslices) f_fold (uksluks, pts) k_bounds
            )
            ~x:((PFIm.empty, PFs.empty, Is.empty), points)
            ~n:(Point.number bounds.vector)
          in
          assert (rem_pts = Ps.empty) ;
          let umax = uf bounds in
          Ungraftable {
            color ;
            umax_total = umax ;
            umax_revealed = umax ;
            uk_slices ;
            revealed_us = us ;
            hidden_us = PFs.empty ;
            revealed_ks = ks ;
            hidden_ks = Is.empty ;
          }
        let find csl (u, k) =
          let u = F.Pos.close u in
          PFIm.find (u, k) (slices csl)

        (*
        let revealed csl (u, k) =
             (Fs.mem u (revealed_us csl))
          && (Is.mem k (revealed_ks csl))
        *)

        let map_slices f csl =
          (* volume doesn't change, because map conserve_bounds check *)
          let slices' =
            PFIm.map (UKsl.map_conserve_bounds f) (slices csl)
          in
          match csl with
          | Immutable r ->
              Immutable { r with uk_slices = slices' }
          | Ungraftable r ->
              Ungraftable { r with uk_slices = slices' }
          | Graftable r ->
              Graftable { r with uk_slices = slices' }

        let fold_rvl_slices f csl x0 =
          Is.fold (fun k x ->
            PFs.fold (fun u x' ->
              f (find csl (u, k)) x'
            ) (revealed_us csl) x
          ) (revealed_ks csl) x0

        (* for now compute count directly from uk_slices *)
        let count csl =
          I.Pos.narrow (fold_rvl_slices (fun uksl c ->
            I.Pos.add c (UKsl.count uksl)
          ) csl I.zero)

        let volume csl =
          F.Pos.narrow (fold_rvl_slices (fun uksl v ->
            F.Pos.add v (UKsl.volume uksl)
          ) csl F.zero)

        let logp csl =
          F.Neg.narrow (fold_rvl_slices (fun uksl x ->
            F.Neg.add x (UKsl.logp uksl)
          ) csl F.zero)

        let density csl =
          F.Pos.div (I.Pos.to_float (count csl)) (volume csl)

        let pop_penultimate_revealed_u csl =
          let u, revealed_us' = PFs.pop_max (revealed_us csl) in
          if PFs.equal revealed_us' PFs.empty then
            None
          else
            Some (u, revealed_us')

        let hide_u csl =
          match pop_penultimate_revealed_u csl with
          | None ->
              raise Already_hidden
          | Some (u, revealed_us') ->
              begin match csl with
              | Immutable _ ->
                  invalid_arg "Immutable c slice"
              | Ungraftable r ->
                  r.umax_revealed <- PFs.max_elt revealed_us' ;
                  r.revealed_us <- revealed_us' ;
                  r.hidden_us <- PFs.add u r.hidden_us
              | Graftable r ->
                  r.umax_revealed <- PFs.max_elt revealed_us' ;
                  r.revealed_us <- revealed_us' ;
                  r.hidden_us <- PFs.add u r.hidden_us
              end
          | exception Not_found ->
              failwith "Csl.hide_u : all slices are hidden"

        let pop_next_hidden_u csl =
          match PFs.pop_min (hidden_us csl) with
          | v ->
              Some v
          | exception Not_found ->
              None

        let reveal_u csl =
          match pop_next_hidden_u csl with
          | None ->
              raise Already_revealed
          | Some (u, hidden_us') ->
              begin match csl with
              | Immutable _ ->
                  invalid_arg "Immutable c slice"
              | Ungraftable r ->
                  r.umax_revealed <- u ;
                  r.hidden_us <- hidden_us' ;
                  r.revealed_us <- PFs.add u r.revealed_us
              | Graftable r ->
                  r.umax_revealed <- u ;
                  r.hidden_us <- hidden_us' ;
                  r.revealed_us <- PFs.add u r.revealed_us
              end

        let graft_u =
          function
          | Immutable _ | Ungraftable _ ->
              invalid_arg "Immutable color slice"
          | Graftable ({
              color ;
              rng ;
              umax_total ;
              revealed_us ;
              hidden_us ;
              revealed_ks ;
              hidden_ks ;
              _
            } as r) as csl ->
              if not (hidden_us = PFs.empty) then
                raise Not_all_revealed ;
              let du' = Prm.next_du umax_total in
              let umax_total' = Prm.next_total_du umax_total in
              r.umax_total <- umax_total' ;
              r.umax_revealed <- umax_total' ;
              r.revealed_us <- PFs.add umax_total' revealed_us ;
              let draw_add k =
                let uksl = find csl (umax_total, k) in
                let bounds = UKsl.bounds uksl in
                let bounds' = {
                  origin = Point.with_value umax_total bounds.origin ;
                  vector = Point.with_value du' bounds.vector ;
                }
                in
                let uksl' = UKsl.rand_draw ~rng color bounds' in
                r.uk_slices <- PFIm.add (umax_total', k) uksl' r.uk_slices
              in
              let all_ks = Is.union revealed_ks hidden_ks in
              Is.iter draw_add all_ks

        let first_slice csl =
          let first_u = PFs.min_elt (revealed_us csl) in
          let first_k = Is.min_elt (revealed_ks csl) in
          PFIm.find (first_u, first_k) (slices csl)

        let count_first csl =
          UKsl.count (first_slice csl)

        let volume_first csl =
          UKsl.volume (first_slice csl)

        let density_first csl =
          UKsl.density (first_slice csl)

        let columns = [
          "color" ;
          "origin_t" ;
          "count" ;
          "volume" ;
          "density" ;
          "count_first" ;
          "volume_first" ;
          "density_first" ;
        ]

        let extract csl =
          function
          | "color" ->
              Point.Color.to_string (color csl)
          | "origin_t" ->
              F.to_string (Point.time (bounds csl).origin)
          | "count" ->
              I.to_string (count csl)
          | "volume" ->
              F.to_string (volume csl)
          | "density" ->
              F.to_string (density csl)
          | "count_first" ->
              I.to_string (count_first csl)
          | "volume_first" ->
              F.to_string (volume_first csl)
          | "density_first" ->
              F.to_string (density_first csl)
          | s ->
              failwith (Printf.sprintf "unrecognized column %s" s)
      end

    module Tsl =
      struct
        type nonrec color = color
        type c_slice = Csl.t

        type t = {
          key : U._float ;
          range : U._float * closed_pos ;
          c_slices : Csl.t Cm.t ;
        }

        let copy tsl = {
          tsl with c_slices = Cm.map Csl.copy tsl.c_slices
        }

        let key tsl = F.narrow tsl.key

        let range tsl =
          let t0, dt = tsl.range in
          F.narrow t0, F.Pos.narrow dt

        let slices tsl = tsl.c_slices

        let find tsl c = Cm.find c (slices tsl)

        let add_color csl tsl =
          let c = Csl.color csl in
          if Cm.mem c (slices tsl) then
            invalid_arg "Tsl.add_color : color already present"
          ;
          let c_slices = Cm.add c csl (slices tsl) in
          { tsl with c_slices }

        let from_slices c_slices =
          let rgo = Cm.fold (fun _ csl ->
              function
              | None ->
                  Some (Csl.time_range csl)
              | (Some (t0, dt)) as rgo ->
                  let t0', dt' = Csl.time_range csl in
                  assert F.Op.((t0 = t0') && (dt = dt')) ;
                  rgo
            ) c_slices None
          in
          match rgo with
          | None ->
              invalid_arg "Tsl.from_slices : empty list"
          | Some ((t0, dt) as range) ->
              let key = F.add t0 dt in
              { key ; range ; c_slices }

        let rand_choose ~rng tsl =
          let binds = Cm.bindings (slices tsl) in
          let _, csl = U.rand_unif_choose ~rng binds in
          csl

        let rand_draw ~rngm ~key (t0, dt) vectors =
          let c_slices = L.fold_left (fun csls (nuslices, vector) ->
            let c = Point.color vector in
            let rng = rng_of_color rngm c in
            let origin = Point.with_time t0 (Point.zero c) in
            let vector = Point.vector (Point.with_time dt vector) in
            (* how can we create bounds ? *)
            let csl = Csl.rand_draw ~rng c { origin ; vector } nuslices in
            Cm.add c csl csls
          ) Cm.empty vectors
          in
          let range = (t0, dt) in
          {
            key ;
            range ;
            c_slices ;
          }

        let create ~key (t0, dt) vectors points =
          let rec f_c csls points vectors =
            match vectors with
            | [] ->
                assert (points =  Ps.empty) ;
                csls
            | (nuslices, vector) :: vectors' ->
                let c = Point.color vector in
                let points_in, points_out = Ps.partition (fun pt ->
                  Point.Color.equal (Point.color pt) c
                ) points
                in
                let origin = Point.with_time t0 (Point.zero c) in
                let vector = Point.vector (Point.with_time dt vector) in
                let csl =
                  Csl.create c { origin ; vector } nuslices points_in
                in
                f_c (Cm.add c csl csls) points_out vectors'
          in
          let c_slices = f_c Cm.empty points vectors in
          let range = (t0, dt) in
          {
            key ;
            range ;
            c_slices ;
          }

        let hide_u tsl c =
          let csl = find tsl c in
          Csl.hide_u csl ;
          let hid_u = Csl.umax_revealed csl in
          hid_u

        let reveal_u tsl c =
          let csl = find tsl c in
          Csl.reveal_u csl ;
          let revealed_u = Csl.umax_revealed csl in
          revealed_u

        let graft_u tsl c =
          let csl = find tsl c in
          Csl.graft_u csl ;
          let grafted_u = Csl.umax_revealed csl in
          grafted_u

        let with_slice csl' tsl =
          (* let's try not to use csl *)
          let color = Csl.color csl' in
          assert (Cm.mem color (slices tsl)) ;
          let c_slices = Cm.add color csl' (slices tsl) in
          { tsl with c_slices }

        let map_slices f tsl =
          let c_slices = Cm.map f (slices tsl) in
          { tsl with c_slices }

        let map_uk_slices f tsl c =
          let old_csl = find tsl c in
          let new_csl = Csl.map_slices f old_csl in
          let c_slices = Cm.mapi (fun color csl ->
              if Point.Color.equal c color then
                new_csl
              else
                Csl.copy csl
            ) (slices tsl)
          in
          { tsl with c_slices }

        let fold_slices f tsl x0 =
          let f' _ = f in
          Cm.fold f' (slices tsl) x0

        let count tsl =
          I.Pos.narrow (fold_slices (fun csl c ->
            I.Pos.add c (Csl.count csl)
          ) tsl I.zero)

        let volume tsl =
          F.Pos.narrow (fold_slices (fun csl v ->
            F.Pos.add v (Csl.volume csl)
          ) tsl F.zero)

        let logp tsl =
          F.Neg.narrow (fold_slices (fun csl x ->
            F.Neg.add x (Csl.logp csl)
          ) tsl F.zero)
      end

    type uk_slice = UKsl.t

    type c_slice = Csl.t

    type t_slice = Tsl.t

    type t = {
      range : U._float * closed_pos ;
      colors : color list ;
      (* slices of time, key = ending time *)
      t_slices : t_slice Fm.t ;
    }

    let copy prm =
      { prm with t_slices = Fm.map Tsl.copy prm.t_slices }

    let time_range prm =
      let t0, dt = prm.range in
      (F.narrow t0, F.Pos.narrow dt)

    let colors prm =
      prm.colors

    let slices prm =
      prm.t_slices

    let t_slice_at prm t =
      Fm.find (F.narrow t) prm.t_slices

    (*
    (* careful : returns the t_slice containing t+ *)
    let t_slice_of prm t =
      let _, _, tsls = Fm.split (F.Pos.close t) prm.t_slices in
      let _, tsl = Fm.min_binding tsls in
      tsl
    *)

    let nslices prm =
      I.Pos.of_int (Fm.cardinal (slices prm))

    let iter_slices f prm =
      let f' _ = f in
      Fm.iter f' prm.t_slices

    let fold_slices f prm x0 =
      let f' _ = f in
      Fm.fold f' (slices prm) x0

    let count prm =
      I.Pos.narrow (fold_slices (fun tsl c ->
        I.Pos.add c (Tsl.count tsl)
      ) prm I.zero)

    let volume prm =
      F.Pos.narrow (fold_slices (fun tsl v ->
        F.Pos.add v (Tsl.volume tsl)
      ) prm F.zero)

    let logp prm =
      (* Pre.logd_poisson_process (volume prm) (count prm) *)
      (* Pre.logp_poisson (volume prm) (I.to_int (count prm)) *)
      F.Neg.narrow (fold_slices (fun tsl x ->
        F.Neg.add x (Tsl.logp tsl)
      ) prm F.zero)

    let rand_draw ~rngm ~time_range ~ntslices ~vectors =
      let t0, dt = time_range in
      let subdt = Prm.subdivide dt ntslices in
      let colors = L.map (fun (_, pt) -> Point.color pt) vectors in
      let t_slices = U.int_fold ~f:(fun tsls ti ->
        let t0' = Prm.substart t0 subdt (I.Pos.of_int (ti - 1)) in
        let tf' = Prm.substart t0 subdt (I.Pos.of_int ti) in
        let tsl = Tsl.rand_draw ~rngm ~key:tf' (t0', subdt) vectors in
        (* key is final time, to then split properly *)
        Fm.add tf' tsl tsls
      ) ~x:Fm.empty ~n:(I.to_int ntslices)
      in
      let range = (F.narrow t0, F.Pos.close dt) in
      { range ; colors ; t_slices }

    let create ~time_range ~ntslices ~vectors points =
      let t0, dt = time_range in
      let subdt = Prm.subdivide dt ntslices in
      let colors = L.map (fun (_, pt) -> Point.color pt) vectors in
      let rec f_t points tsls =
        function
        | 0 ->
            (* if this fails, some points are not in time_range *)
            assert (points = Ps.empty) ;
            tsls
        | n when n < 0 ->
            invalid_arg "negative n"
        | n ->
            let t0' = Prm.substart t0 subdt (I.Pos.of_int (n - 1)) in
            let tf' = Prm.substart t0 subdt (I.Pos.of_int n) in
            let points_in, points_out = Ps.partition (fun pt ->
                let t = Point.time pt in
                F.Op.((t0' <= t) && (t < tf'))
              ) points
            in
            let tsl = Tsl.create ~key:tf' (t0', subdt) vectors points_in in
            f_t points_out (Fm.add tf' tsl tsls) (n - 1)
      in
      let t_slices = f_t points Fm.empty (I.to_int ntslices)
      in
      let range = (F.narrow t0, F.Pos.close dt) in
      { range ; colors ; t_slices }

    let rand_choose ~rng prm =
      let binds = Fm.bindings prm.t_slices in
      let _, tsl = U.rand_unif_choose ~rng binds in
      tsl

    let hide_downto tsl c r =
      let rec f u =
        if F.Op.(u > r) then begin
          match Tsl.hide_u tsl c with
          | u' ->
              f u'
          | exception Csl.Already_hidden ->
              ()
        end else
          ()
      in
      f (Csl.umax_revealed (Tsl.find tsl c))

    let reveal_upto_exn tsl c r =
      let csl = Tsl.find tsl c in
      let umax = Csl.umax_revealed csl in
      let rec f u =
        if F.Op.(u <= r) then begin
          let u' = Tsl.reveal_u tsl c in
          f u'
        end else
          ()
      in
      f umax

    let reveal_upto tsl c r =
      try
        reveal_upto_exn tsl c r
      with Csl.Already_revealed ->
        ()

    let graft_upto tsl c r =
      let rec f u =
        if F.Op.(u <= r) then
          match Tsl.graft_u tsl c with
          | u' ->
              f u'
          | exception Csl.Not_all_revealed ->
              invalid_arg "graft_upto : all u slices must be revealed"
        else
          ()
      in
      f (Csl.umax_revealed (Tsl.find tsl c))

    let reveal_graft_upto tsl c r =
      try
        (* first, reveal if needed *)
        reveal_upto_exn tsl c r
      with Csl.Already_revealed ->
        (* then, graft if needed (if reveal raises) *)
        graft_upto tsl c r

    let with_slice tsl' nu =
      let t = Tsl.key tsl' in
      assert (Fm.mem t (slices nu)) ;
      let slices' = Fm.add t tsl' (slices nu) in
      { nu with t_slices = slices' }

    let map_slices f prm =
      let slices' = Fm.map f (slices prm) in
      { prm with t_slices = slices' }

    (* this returns a new prm with a new tsl and a new csl *)
    let map_uk_slices f prm tsl c =
      let tsl' = Tsl.map_uk_slices f tsl c in
      let t = Tsl.key tsl' in
      assert (Fm.mem t prm.t_slices) ;
      (* Here it would be better to copy the slices,
       * but actually it does not matter
       * (everything is recomputed at the prm level) *)
      let slices' = Fm.add t tsl' prm.t_slices in
      { prm with t_slices = slices' }

    let merge prm prm' =
      (* prm and prm' should have no color in common *)
      let cols = Cs.of_list (colors prm) in
      let cols' = Cs.of_list (colors prm') in
      if not (Cs.is_empty (Cs.inter cols cols')) then
        invalid_arg "merge : color(s) in common" ;
      (* the two prms should have the same range *)
      if not (time_range prm = time_range prm') then
        invalid_arg "merge : different time range" ;
      (* the two prms should have the same time slicing *)
      let time_keys prm =
        let ts, _ = L.split (Fm.bindings (slices prm)) in
        Fs.of_list ts
      in
      if not (Fs.equal (time_keys prm) (time_keys prm')) then
        invalid_arg "merge : different t slicing" ;
      let range = time_range prm in
      let colors = (colors prm) @ (colors prm') in
      let t_slices = Fm.map (fun tsl ->
          let tsl' = t_slice_at prm' (Tsl.key tsl) in
          Tsl.fold_slices (fun csl' tsl'' ->
              Tsl.add_color csl' tsl''
            ) tsl' tsl
        ) (slices prm)
      in
      {
        range ;
        colors ;
        t_slices ;
      }

    let partition f prm =
      let t_slices_filt, t_slices_rem = Fm.fold (fun t tsl (filt, rem) ->
          (* partition c_slices depending on color *)
          let csl_filt, csl_rem = Cm.partition (fun c _ ->
              f c
            ) (Tsl.slices tsl)
          in
          (* now create two t_slice from those *)
          let tsl_filt = Tsl.from_slices csl_filt in
          let tsl_rem = Tsl.from_slices csl_rem in
          (Fm.add t tsl_filt filt, Fm.add t tsl_rem rem)
        ) (slices prm) (Fm.empty, Fm.empty)
      in
      let colors_filt, colors_rem = L.partition f (colors prm) in
      let range = time_range prm in
      let prm_filt = {
        range ;
        colors = colors_filt ;
        t_slices = t_slices_filt ;
      }
      in
      let prm_rem = {
        range ;
        colors = colors_rem ;
        t_slices = t_slices_rem ;
      }
      in prm_filt, prm_rem

    (* split nu into n prms of equal time widths (except last) *)
    let split n nu =
      let m = Fm.cardinal nu.t_slices in
      let ts, _ = L.split (Fm.bindings nu.t_slices) in
      (* number of t_slices by prm (except last) *)
      let nby = m / n in
      assert (nby >= 1) ;
      U.int_fold ~f:(fun nus k ->
        (* keep only appropriate t_slices *)
        let t_slices = U.int_fold ~f:(fun tsls j ->
          let t = L.at ts (((k - 1) * nby) + (j - 1)) in
          let tsl = Fm.find t nu.t_slices in
          Fm.add t tsl tsls
        ) ~n:nby ~x:Fm.empty
        in
        let _, min_tsl = Fm.min_binding t_slices in
        let t0, _ = Tsl.range min_tsl in
        let dt = Fm.fold (fun _ tsl dt ->
          let _, dt' = Tsl.range tsl in
          F.Pos.add dt dt'
        ) t_slices F.zero
        in
        let range = (t0, dt) in
        let nu' = {
          nu with range = range ;
                  t_slices = t_slices ;
        }
        in
        nu' :: nus
      ) ~n ~x:[]

    let concat nus =
      (* a list of prms with successive time ranges :
       * put all their time slices together as one prm *)
      L.reduce (fun nu nu' ->
        (* check ranges follow each other *)
        let t0, dt = time_range nu in
        let t0', dt' = time_range nu' in
        assert F.Op.(t0' =~ t0 + dt) ;
        let range = (t0, F.Pos.add dt dt') in
        (* put time slices together *)
        let t_slices = Fm.merge (fun _ tslo tslo' ->
          match tslo, tslo' with
          | None, None
          | Some _, Some _ ->
              assert false
          | Some tsl, None
          | None, Some tsl ->
              Some tsl
        ) nu.t_slices nu'.t_slices
        in
        { nu with range ; t_slices }
      ) nus

    let slicing prm =
      let time_range = time_range prm in
      let ntslices = I.Pos.of_int (Fm.cardinal (slices prm)) in
      (time_range, ntslices)

    let time_of_nth nu j =
      let (t0, dt), n_tslices = slicing nu in
      let delta_t = F.Pos.div dt (I.Pos.to_float n_tslices) in
      F.Op.(t0 + delta_t * (I.Pos.to_float j + F.one))

    let density nu =
      F.Pos.div (I.Pos.to_float (count nu)) (volume nu)

    let adjust_density ~rng slices dlambda nu =
      (* choose a slice *)
      let color, jt = U.rand_unif_choose ~rng slices in
      let t = time_of_nth nu jt in
      let tsl = t_slice_at nu t in
      let csl = Tsl.find tsl color in
      let csl' =
        Csl.map_slices (UKsl.adjust_density ~rng dlambda) csl
      in
      (* make a new nu *)
      let tsl' = Tsl.with_slice csl' tsl in
      (color, F.narrow t, with_slice tsl' nu)

    let redraw_once ~rng draw filter nu =
      let t, c, tsl, csl = U.insist ~rng (fun ~rng ->
          (* t slices *)
          let tsl = rand_choose ~rng nu in
          (* c slices *)
          let csl = Tsl.rand_choose ~rng tsl in
          let t = Tsl.key tsl in
          let c = Csl.color csl in
          if filter t c then
            Some (t, c, tsl, csl)
          else
            None
        )
      in
      (* choose a number *)
      (* number of k slices *)
      let nk = I.Pos.succ (I.Pos.of_int (Csl.kmax csl)) in
      let k = I.to_int (U.rand_int ~rng nk) in
      (* redraw all of its u slices *)
      let nu' = map_uk_slices (fun uksl ->
        if UKsl.number uksl = k then
          draw ~rng uksl
        else
          uksl
        ) nu tsl c
      in
      ((t, c), nu')

    let redraw_distinct ~rng draw nredraws filter nu =
      let _, nu' = U.int_fold ~f:(fun (fil', nu') _ ->
          let (t, c), nu'' = redraw_once ~rng draw fil' nu' in
          let fil'' = (fun t' c' ->
              if F.Op.(t = t') && (Point.Color.equal c c') then
                false
              else
                fil' t c
            )
          in (fil'', nu'')
        ) ~x:((fun _t c -> filter c), nu) ~n:nredraws
      in nu'

    module Slices =
      struct
        type elt = uk_slice

        type nonrec t = t

        let fold f nu x =
          fold_slices (Tsl.fold_slices (Csl.fold_rvl_slices f)) nu x

        let unfold _ _ =
          failwith "Not_implemented"
      end

    module Csv_grid = U.Csv.Make (Slices)

    let columns_grid = UKsl.columns_grid

    let extract_grid = UKsl.extract_grid

    let output_grid chan prm =
      let columns = UKsl.columns_grid in
      let f = U.Csv.Row.unfold ~columns UKsl.extract_grid in
      Csv.output_record chan columns ;
      Csv_grid.write ~f ~chan prm ;
      Csv.close_out chan

    module Cslices =
      struct
        type elt = c_slice

        type nonrec t = t

        let fold f nu x =
          fold_slices (Tsl.fold_slices f) nu x

        let unfold _ _ =
          failwith "Not_implemented"
      end

    let columns_csl = Csl.columns

    let extract_csl = Csl.extract

    let read ~time_range ~ntslices (* ~vectors *) du0 fname =
      Printf.eprintf "read\n%!" ;
      let exception Bad_input in
      let c =
        Csv.of_channel ~has_header:true (open_in fname)
      in
      try
        let points = Csv.Rows.fold_left ~f:(fun pts row ->
            match Point.read row with
            | None ->
                raise Bad_input
            | Some pt ->
                Ps.add pt pts
          ) ~init:Ps.empty c
        in
        Printf.eprintf "read %i points\n%!" (Ps.cardinal points) ;
        (* look for the point with max value *)
        (* we want to compare only color by color *)
        (* FIXME this doesn't take into account other dimensions
         * of a point that might exist *)
        Printf.eprintf "highest\n%!" ;
        let tops = Ps.fold (fun pt tops ->
          let c = Point.color pt in
          match Cm.find c tops with
          | top ->
              Cm.add c (Point.upper pt top) tops
          | exception Not_found -> (* first hit on this color *)
              Cm.add c pt tops
        ) points Cm.empty
        (* then how many slices are needed to reach that point ? *)
        in
        Printf.eprintf "nuslices\n%!" ;
        let nuslices umax =
          let rec f n du =
            if du > umax then
              (n, du)
            else
              f (I.Pos.succ n) (Prm.next_total_du du)
          in f I.one (F.Pos.narrow du0)
        in
        let vector_map = Cm.map (fun top ->
          let n, du = nuslices (Point.value top) in
          let dk = Point.number top + 1 in
          Printf.eprintf "du = %f, dk = %i\n%!" (F.to_float du) dk ;
          let v =
            top
            |> Point.with_value du
            |> Point.with_number dk
          in
          (n, v)
        ) tops
        in
        let _, vectors = L.split (Cm.bindings vector_map) in
        Printf.eprintf "create\n%!" ;
        Some (create ~time_range ~ntslices ~vectors points)
      with Bad_input ->
        None

  end

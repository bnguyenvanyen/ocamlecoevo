(** Poisson random measure for Ctmjp.Prm_internal
 *
 *  A PRM in one dimension,
 *  to have event occurrence times be given by Poisson(\int_0^t r_s(X_s) ds)
 *)

open Sig


type point = U.closed_pos  (** time of point *)


module Point :
  (INTERNAL_POINT with type t = point) =
  struct
    type t = point

    let make t = F.Pos.close t

    let time pt = F.Pos.narrow pt

    let with_time t _pt =
      F.Pos.close t

    let add pt pt' =
      F.Pos.add (time pt) (time pt')

    let rand_point ~rng ~origin ~vector =
      F.Pos.add (time origin) (U.rand_float ~rng (time vector))

    let volume ~vector =
      time vector

    let inside ~origin ~vector pt =
      let dest = add origin vector in
      F.Op.(
         (time origin <= time pt)
      && (time pt < time dest)
      )

    let lower pt pt' =
      F.Pos.min (time pt) (time pt')

    let upper pt pt' =
      F.Pos.max (time pt) (time pt')

    let columns = [ "t" ]

    let extract t =
      function
      | "t" ->
          F.to_string t
      | _ ->
          invalid_arg "unrecognized column"

    let line pt =
      L.map (extract pt) columns

    let read row =
      U.Csv.Row.find_conv F.Pos.of_string row "t"

    let compare pt pt' =
      F.compare (time pt) (time pt')
  end


module Make
  (Color : COLOR)
  (Colormap : BatMap.S with type key = Color.t) :
  (INTERNAL_PRM with type color = Color.t
                 and type point = Point.t
                 and type 'a colormap = 'a Colormap.t) =
  struct
    type color = Color.t
    type point = Point.t
    type +'a colormap = 'a Colormap.t

    module Color = Color
    module Colormap = Colormap
    module Point = Point

    module Points = BatSet.Make (Point)

    type elt =
      | Single of point
      | Ungraftable of Points.t
      | Graftable of { rng : U.rng ; points : Points.t }


    type t = {
      all_points : elt Colormap.t ;
      grafted : Points.t Colormap.t ;
    }

    let empty () = {
      all_points = Colormap.empty ;
      grafted = Colormap.empty ;
    }

    let copy nu =
      (* we copy the rngs in graftable elements,
       * for reproducibility *)
      let f =
        function
        | (Single _ | Ungraftable _) as elt ->
            elt
        | Graftable { rng ; points } ->
            Graftable { rng = Random.State.copy rng ; points }
      in
      let all_points = Colormap.map f nu.all_points in
      { nu with all_points }

    let grafted nu =
      nu.grafted

    let count_colors { all_points ; _ } =
      Colormap.cardinal all_points

    let find_single color nu =
      match Colormap.find color nu.all_points with
      | Single pt ->
          pt
      | Ungraftable _ | Graftable _ ->
          invalid_arg "Color not single"

    let remove color nu =
      let all_points = Colormap.remove color nu.all_points in
      { nu with all_points }

    let add_single c pt nu =
      let all_points = Colormap.add c (Single pt) nu.all_points in
      { nu with all_points }

    let add_ungraftable c pts nu =
      let all_points = Colormap.add c (Ungraftable pts) nu.all_points in
      { nu with all_points }

    let add_graftable ~rng c points nu =
      let all_points =
        Colormap.add
          c
          (Graftable { rng = U.rand_rng rng ; points })
          nu.all_points
      in
      { nu with all_points }

    let add_graftable_point c pt nu =
      let f =
        function
        | Single _
        | Ungraftable _ ->
            invalid_arg "not graftable"
        | Graftable { rng ; points } ->
            Graftable { rng ; points = Points.add pt points }
      in
      let all_points =
        Colormap.modify c f nu.all_points
      in
      { nu with all_points }

    (* the fuel of a point is the internal time that separates it
     * from the previous point
     * (the point is reached when all of that additional fuel
     *  has been consumed) *)
    let new_point ~rng c points nu =
      let t = U.rand_exp ~rng F.one in
      let new_pt = t in
      let grafted =
        Colormap.modify_def Points.empty c (Points.add new_pt) nu.grafted
      in
      let elt = Graftable { rng ; points = Points.add new_pt points } in
      let all_points = Colormap.modify c (fun _ -> elt) nu.all_points in
      new_pt, { all_points ; grafted }

    let find_min_point c nu =
      match Colormap.find c nu.all_points with
      | Single pt ->
          pt, nu
      | Ungraftable points ->
          Points.min_elt points, nu
      | Graftable { rng ; points } ->
          begin match Points.pop_min points with
          | pt, pts ->
              (* if there is only one point left, add one more after *)
              if Points.is_empty pts then
                let _, nu' = new_point ~rng c points nu in
                pt, nu'
              else
                pt, nu
          | exception Not_found ->
              new_point ~rng c points nu
          end

    let remove_min_point c nu =
      let g =
        function
        | Single _ ->
            (* FIXME *)
            Ungraftable Points.empty
        | Ungraftable points ->
            let _, points' = Points.pop_min points in
            Ungraftable points'
        | Graftable { rng ; points } ->
            let _, points' = Points.pop_min points in
            Graftable { rng ; points = points' }
      in
      let all_points = Colormap.modify c g nu.all_points in
      { nu with all_points }

    let iter f nu =
      let g c =
        function
        | Single pt ->
            f c (Points.singleton pt)
        | Ungraftable points
        | Graftable { points ; _ } ->
            f c points
      in
      Colormap.iter g nu.all_points

    let iter_points f nu =
      let g c =
        function
        | Single pt ->
            f c pt
        | Ungraftable points
        | Graftable { points ; _ } ->
            Points.iter (f c) points
      in
      Colormap.iter g nu.all_points

    let map_points f nu =
      let g c =
        function
        | Single pt ->
            Single (f c pt)
        | Ungraftable points ->
            Ungraftable (Points.map (f c) points)
        | Graftable { points ; rng } ->
            let points = Points.map (f c) points in
            Graftable { points ; rng }
      in
      let all_points = Colormap.mapi g nu.all_points in
      { nu with all_points }

    let fold f nu x =
      let g c =
        function
        | Single pt ->
            f c (Points.singleton pt)
        | Ungraftable points
        | Graftable { points ; _ } ->
            f c points
      in
      Colormap.fold g nu.all_points x

    let fold_points f nu x =
      let g c =
        function
        | Single pt ->
            f c pt
        | Ungraftable points
        | Graftable { points ; _ } ->
            Points.fold (f c) points
      in
      Colormap.fold g nu.all_points x

    let modify_map c f nu =
      let g =
        function
        | Single pt ->
            Single (f pt)
        | Ungraftable points ->
            Ungraftable (Points.map f points)
        | Graftable { rng ; points } ->
            Graftable { rng ; points = Points.map f points }
      in
      let all_points = Colormap.modify c g nu.all_points in
      { nu with all_points }

    let modify_filter_map c f nu =
      let g =
        function
        | Single pt ->
            begin match f pt with
            | Some pt' ->
                Single pt'
            | None ->
                (* FIXME for now I put an empty Ungraftable,
                 * but maybe I should remove the color ? *)
                Ungraftable Points.empty
            end
        | Ungraftable points ->
            Ungraftable (Points.filter_map f points)
        | Graftable { rng ; points } ->
            Graftable { rng ; points = Points.filter_map f points }
      in
      let all_points = Colormap.modify c g nu.all_points in
      { nu with all_points }

    let union_elts elt elt' =
      (* force the elements to be of same constructor,
       * as otherwise it doesn't make much sense *)
      match elt, elt' with
      | Single pt, Single pt' ->
          (* only keep the min *)
          let min_t = min (Point.time pt) (Point.time pt') in
          Single min_t
      | Ungraftable pts, Ungraftable pts' ->
          Ungraftable (Points.union pts pts')
      | Graftable { rng ; points }, Graftable { points = points' ; _ } ->
          Graftable { rng ; points = Points.union points points' }
      | Single _, (Ungraftable _ | Graftable _)
      | Ungraftable _, (Single _ | Graftable _)
      | Graftable _, (Single _ | Ungraftable _) ->
          invalid_arg "different constructors"

    let find_and_diagnose c points =
      match Colormap.find c points with
      | res ->
          res
      | exception (Not_found as e) ->
          Printf.eprintf "color %s not found\n" (Color.to_string c) ;
          Printf.eprintf "all colors in prm :\n" ;
          Colormap.iter (fun c' _ ->
            Printf.eprintf "%s\n" (Color.to_string c')
          ) points ;
          raise e

    let replace_by_union old_color old_color' new_color nu =
      let elt = find_and_diagnose old_color nu.all_points in
      let elt' = find_and_diagnose old_color' nu.all_points in
      let new_elt = union_elts elt elt' in
      let all_points = nu.all_points
        |> Colormap.remove old_color
        |> Colormap.remove old_color'
        |> Colormap.add new_color new_elt
      in
      { nu with all_points }
  end

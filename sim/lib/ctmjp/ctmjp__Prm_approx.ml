(** Euler-Poisson-like simulation
 *
 *  of Continuous time Markov chains (CTMC or MJP)
 *  against a given Poisson random measure (PRM).
 *)


open Ctmjp__Sig


let ounit _ _ = ()


(* algorithm is :
 * (the time slices should be relatively small)
 * For each t slice,
 * - for each c slice
 *   - compute the rate for that color at the left boundary of the t slice
 *   - reveal up to the rate
 *   - add up all the counts below the last slice
 *     (all those points are below the rate)
 *   - for the last slice, count the points below the rate
 *   - remember the total count of events
 * - once you've gone through all colors,
 *   do all the modifications *)

module type PRM_MAKER =
  (functor (Point : COMPARABLE_POINT) ->
    (PRM_MINI with type color = Point.color
               and module Point = Point)
  )


module Make (Make_prm : PRM_MAKER) (Pt : POINT) =
  struct
    (* we make sure that points are ordered by color in P
     * by calling the functor ourselves *)
    module Point = Prm.Compare_color (Pt)
    module P = Make_prm (Point)
    module Cm = Point.Colormap

    module Color = Point.Color

    type ('a, 's, 'r) ev =
        ('s U.anyfloat -> 'a -> 'r U.pos)
      * (unit -> 's U.anyfloat -> 'a -> 'a)

    let simulate_until
      ~(output : ('a,_,_) output)
      (evm : ('a,_,_) ev Cm.t)
      x0 prm =
      (* ~~ *)
      (* for each color->event, accumulate modifs in modif_acc *)
      let f tsl x color ((rate, modif) : ('a,_,_) ev) (modif_acc : 'a -> 'a) =
        let t, _ = P.Tsl.range tsl in
        match P.Tsl.find tsl color with
        | csl ->
            (* compute the rate *)
            let r : _ U.pos = rate t x in
            let m : ('a -> 'a) = modif () t in
            (* hide down to under the rate *)
            P.hide_downto tsl color r ;
            (* then reveal up to the rate :
             * now we know that precisely the right slices
             * are revealed, that is, the last revealed slice contains r *)
            P.reveal_graft_upto tsl color r ;
            (* count sure points then
             * count remaining points in last u slice *)
            let count = P.Csl.fold_rvl_slices (fun usl n ->
              let _, umax = P.UKsl.range usl in
              let count =
                if F.Op.(umax <= r) then begin
                  let c = P.UKsl.count usl in
                  (* total_points := !total_points + (I.to_int c) ; *)
                  (* accepted_points := !accepted_points + (I.to_int c) ; *)
                  c
                end else begin (* last slice *)
                  P.UKsl.count_under usl r
                end
              in I.Pos.add n count
            ) csl I.zero
            in
            (* FIXME we'd prefer a function
             * that directly does k modifications*)
            U.int_fold ~f:(fun m' _ ->
              (fun x' -> m (m' x'))
            ) ~x:modif_acc ~n:(I.to_int count)
        | exception Not_found ->
            Printf.eprintf "color %s not found at %f\n"
            (Color.to_string color)
            (F.to_float t) ;
            raise Simulation_error
      in
      (* for each time slice *)
      let g tsl x =
        let t, _ = P.Tsl.range tsl in
        output.out (F.narrow t) x ;
        (* accumulate modifs over all events of evm *)
        let modif = Cm.fold (f tsl x) evm (fun x -> x) in
        (* execute all modifs *)
        modif x
      in
      let t0, dt = P.time_range prm in
      let tf = F.add t0 dt in
      output.start (F.narrow t0) x0 ;
      let xf = P.fold_slices g prm x0 in
      output.return (F.narrow tf) xf
  end

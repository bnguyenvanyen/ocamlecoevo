(** Simulate a Markov jump process with a Poisson-driven SDE.
    
    This algorithm uses the Prm_internal and Evm_internal_lazy
    data structures.
 *)

open Sig


module Out = Ctmjp__Prm_internal.Out


module J = Jump.Cadlag_map


module type S =
  sig
    type color
    type prm
    type ('a, 'b) evm

    val simulate_until :
      output:[
        | `Single of ('a, 'b, F._float) output
        | `Pair of ('a * prm, 'b, F._float) output
        | `Trio of ('a * prm * ('a, 'traj) evm, 'b, F._float) output
      ] ->
      'traj ->
      (U._float -> 'a -> 'traj -> 'traj) ->
      ?t0:_ U.anyfloat ->
      ('a, 'traj) evm ->
      'a ->
      prm ->
      _ U.anyfloat ->
        'b

    val simulate :
      output:[
        | `Single of ('a, 'b, F._float) output
        | `Pair of ('a * prm, 'b, F._float) output
        | `Trio of ('a * prm * ('a, 'traj) evm, 'b, F._float) output
      ] ->
      'traj ->
      (U._float -> 'a -> 'traj -> 'traj) ->
      ?t0:_ U.anyfloat ->
      (U._float -> 'a -> _ U.anyfloat option) ->
      ('a, 'traj) evm ->
      'a ->
      prm ->
        'b
  end


module Make
  (Color : COLOR)
  (Prm : INTERNAL_PRM with type color = Color.t)
  (Evm : Evm_internal_lazy.S with type color = Color.t
                              and type prm = Prm.t) :
  (S with type color = Color.t
      and type prm = Prm.t
      and type ('a, 'b) evm = ('a, 'b) Evm.t) =
  struct
    type color = Evm.color
    type prm = Prm.t
    type ('a, 'b) evm = ('a, 'b) Evm.t

    let step
      traj_add
      evm prm traj color (next_event : (_,_) Evm_internal_lazy.elt) x =
      let t' = next_event.proj_occur_time in
      (* Has all the fuel been consumed at this next possible time ? *)
      let rem_fuel = Evm.remaining_fuel next_event traj t' in
      if F.Op.(F.zero =~ rem_fuel) then begin
        (* all the fuel is consumed *)
        let x' = next_event.update_state t' x in
        let traj' = traj_add t' x' traj in
        (* refresh the event that happened *)
        let evm, prm = Evm.update_focal prm color t' x' evm in
        t', evm, prm, traj', x'
      end else begin
        (* all the fuel is not consumed *)
        (* we simply need to update the fuel and projected time,
           and continue *)
        let evm =
          Evm.reproject_focal
            ~color
            ~fuel:(F.Pos.of_anyfloat rem_fuel)
            t'
            x
            evm
        in
        t', evm, prm, traj, x
      end

    let simulate_until
      ~output
      traj_empty traj_add
      ?(t0=F.zero) evm x0 prm tf =
      let rec loop t evm prm traj x =
        (* output *)
        Out.out output (F.narrow t) x prm evm ;
        (* event with smallest earliest possible time ->
           the event that might happen next *)
        let color, next_event = Evm.find_next evm in
        (* t' is the next time something might happen *)
        let t' = next_event.proj_occur_time in
        if F.Op.(t' > tf) then
          tf, x, prm, evm
        else begin
          let t', evm', prm', traj', x' =
            step traj_add evm prm traj color next_event x
          in
          loop t' evm' prm' traj' x'
        end
      in
      let t0 = F.narrow t0 in
      assert F.Op.(t0 < tf) ;
      Out.start output (F.narrow t0) x0 prm evm ;
      let traj0 = traj_add t0 x0 traj_empty in
      let tf, xf, prmf, evmf = loop t0 evm prm traj0 x0 in
      (* We can also return the changed prm *)
      Out.return output (F.narrow tf) xf prmf evmf

    let simulate
      ~output
      traj_empty traj_add
      ?(t0=F.zero) stop (evm : _ Evm.t) x0 prm =
      let rec loop t evm prm traj x =
        (* output *)
        Out.out output (F.narrow t) x prm evm ;
        (* event with smallest earliest possible time ->
           the event that might happen next *)
        let color, next_event = Evm.find_next evm in
        let t' = next_event.proj_occur_time in
        match stop t' x with
        | Some tf ->
            tf, x, prm, evm
        | None ->
            let t', evm', prm', traj', x' =
              step traj_add evm prm traj color next_event x
            in
            loop t' evm' prm' traj' x'
      in
      let t0 = F.narrow t0 in
      begin match stop t0 x0 with
      | None ->
          ()
      | Some _ ->
          invalid_arg "stop at t0"
      end
      ;
      Out.start output (F.narrow t0) x0 prm evm ;
      let traj0 = traj_add t0 x0 traj_empty in
      let tf, xf, prmf, evmf = loop t0 evm prm traj0 x0 in
      (* We can also return the changed prm *)
      Out.return output (F.narrow tf) xf prmf evmf
  end

(** Large population diffusion limit
 *
 *  of MJPs directly through events.
 *  Solves the equation
 *  dX_t = sum_i r_i(X_t) mu_i dt + sum_i sqrt(r_i(X_t)) mu_i dB^i_t
 *)


open Ctmjp__Sig


let simulate_until
  ~(output : (_,_,_) output)
  (domain : Lac.Vec.t domain)
  events
  x0
  dbts =
  let n = Lac.Vec.dim x0 in
  let m = L.length events in
  let dt = Jump.Regular.dt dbts in
  (* stores rates *)
  let r = Lac.Vec.make0 m in
  (* stores sqrt of rates *)
  let sqrt_r = Lac.Vec.make0 m in
  (* stores diffusion increments (sqrt(r_i) * dbt_i) *)
  let diff_incr = Lac.Vec.make0 m in
  (* matrix of stoichiometries, n rows for state, m columns for events *)
  let mu =
    events
    |> L.map (fun (_, mu) -> mu)
    |> Lac.Mat.of_col_vecs_list
  in
  (* stores the dx part due to drift, mu * r, then the full dx *)
  let dx = Lac.Vec.make0 n in
  (* to store temp x' *)
  let tmp_x' = Lac.Vec.make0 n in
  (* contains results of iterations *)
  let res = Lac.Vec.make0 n in
  (* holds a copy of x when needed *)
  let x_bis = Lac.Vec.make0 n in
  let loop t dbt x =
    output.out t x ;
    (* compute rates in r *)
    L.iteri (fun i (rate, _) ->
      r.{i + 1} <- F.to_float (rate t x)
    ) events
    ;
    (* sqrt rates *)
    let sqrt_r = Lac.Vec.sqrt ~y:sqrt_r r in
    (* diffusion increments *)
    let diff_incr = Lac.Vec.mul ~z:diff_incr sqrt_r dbt in
    let drift_dx = Lac.gemv ~beta:0. ~y:dx mu r in
    (* dx = dt * mu * r + mu * incr *)
    let dx = Lac.gemv ~beta:dt ~y:drift_dx ~alpha:1. mu diff_incr in
    let x' = Lac.Vec.add ~z:tmp_x' x dx in
    (* at the end, overwrite x *)
    (* FIXME or do I need a fresh vector ? *)
    if not (domain.is_in x') then begin
      (* copy x into x_bis *)
      let x_bis = Lac.copy ~y:x_bis x in
      domain.back_to ~result:res x_bis x' ;
      res
    end else
      Lac.copy ~y:res x'
  in
  let t0 = F.of_float (Jump.Regular.t0 dbts) in
  (* simulation end time tf is the last dbt time point + one more dt *)
  let tf = F.of_float (dt +. Jump.Regular.tf dbts) in
  (* protect x0 with a copy *)
  let x0 = Lac.copy ~y:res x0 in
  output.start t0 x0 ;
  let xf = Jump.Regular.fold_left (fun x t dbt ->
    loop (F.of_float t) dbt x
  ) x0 dbts
  in
  output.return tf xf

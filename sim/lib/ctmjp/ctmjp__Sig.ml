(** Types and signatures reused elsewhere. *)
include Sig

(* As this is used as argument, we do the same weird thing as in Sig,
 *  go from anypos to pos. *)

type ('a, 't) rate = (U._float -> 'a -> 't U.anypos)

type ('a, 'stoch) modif = ('stoch -> U._float -> 'a -> 'a)

type ('a, 'stoch, 't) rate_modif =
  (U._float -> 'a -> 't U.anypos * ('stoch -> 'a))


type ('a, 't) rng_modif = ('a, rng) modif

type ('a, 't) rng_rate_modif = ('a, rng, 't) rate_modif


type 'a specs = (string * 'a Lift_arg.spec * string) list



type step_algparam = {
  step_size : closed_pos
}

type epar_algparam = {
  overshoot : closed_pos ;
  step_size : closed_pos ;
}

(** Simulate a Markov jump process with a Poisson-driven SDE.
  
    This algorihtm uses the Prm_internal and Evm_internal
    data structures.
    For each event, we are given a rate, a modif, and a set of points.
    For each point, its time is a fuel, that is consumed at [rate]
    times the real time.
 *)

open Sig


module Out =
  struct
    type ('a, 'b, 'c, 'd, 's) t = [
      | `Single of ('a, 'd, 's) output
      | `Pair of ('a * 'b, 'd, 's) output
      | `Trio of ('a * 'b * 'c, 'd, 's) output
    ]

    let start : _ t -> _ =
      function
      | `Single o ->
          (fun t x _ _ -> o.start t x)
      | `Pair o ->
          (fun t x nu _ -> o.start t (x, nu))
      | `Trio o ->
          (fun t x nu evm -> o.start t (x, nu, evm))

    let out : _ t -> _ =
      function
      | `Single o ->
          (fun t x _ _ -> o.out t x)
      | `Pair o ->
          (fun t x nu _ -> o.out t (x, nu))
      | `Trio o ->
          (fun t x nu evm -> o.out t (x, nu, evm))

    let return : _ t -> _ =
      function
      | `Single o ->
          (fun t x _ _ -> o.return t x)
      | `Pair o ->
          (fun t x nu _ -> o.return t (x, nu))
      | `Trio o ->
          (fun t x nu evm -> o.return t (x, nu, evm))
  end


module type S =
  sig
    type color

    module Color : (COLOR with type t = color)
    module Prm : (INTERNAL_PRM with type color = color)
    module Evm : (Evm_internal.S with type color = color
                                and module Color = Color)

    val simulate_until :
      output:[
        | `Single of ('a, 'b, F._float) output
        | `Pair of ('a * Prm.t, 'b, F._float) output
        | `Trio of ('a * Prm.t * 'a Evm.t, 'b, F._float) output
      ] ->
      ?t0:_ U.anyfloat ->
      'a Evm.t ->
      'a ->
      Prm.t ->
      _ U.anyfloat ->
        'b

    val simulate :
      output:[
        | `Single of ('a, 'b, F._float) output
        | `Pair of ('a * Prm.t, 'b, F._float) output
        | `Trio of ('a * Prm.t * 'a Evm.t, 'b, F._float) output
      ] ->
      ?t0:_ U.anyfloat ->
      (U._float -> 'a -> _ U.anyfloat option) ->
      'a Evm.t ->
      'a ->
      Prm.t ->
        'b
  end


module Make (Evm : Evm_internal.S) :
  (S with type color = Evm.color
      and module Color = Evm.Color
      and module Prm = Evm.Prm
      and module Evm = Evm) =
  struct
    type color = Evm.color

    module Color = Evm.Color
    module Prm = Evm.Prm
    module Evm = Evm

    let step evm prm color (next_event : (_,_) Evm_internal.elt) x =
      let t' = next_event.proj_occur_time in
      let update = next_event.update in
      (* first update the affected events' remaining fuel,
       * because the fuel is consumed before the event happens *)
      let affected = update.affected x in
      let evm = Evm.update_affected_rem_fuel affected t' x evm in
      (* then update the state *)
      let x' = update.state t' x in
      (* then refresh the event that happened
       * (put a new point, or if only one point, remove the event).
       * This happens with the new time and state *)
      let evm, prm = Evm.update_focal prm color t' x' evm in
      (* Then update affected projected times,
       * also with the new time and state *)
      let evm = Evm.update_affected_proj_times affected t' x' evm in
      t', evm, prm, x'

    let simulate_until ~output ?(t0=F.zero) (evm : _ Evm.t) x0 prm tf =
      let rec loop t evm prm x =
        (* output *)
        Out.out output (F.narrow t) x prm evm ;
        (* get [next] event : the one with smallest projected time *)
        let color, next_event, evm = Evm.pop_next evm in
        let t' = next_event.proj_occur_time in
        if F.Op.(t' > tf) then
          tf, x, prm, evm
        else begin
          let t', evm, prm, x' = step evm prm color next_event x in
          loop t' evm prm x'
        end
      in
      let t0 = F.narrow t0 in
      assert F.Op.(t0 < tf) ;
      Out.start output (F.narrow t0) x0 prm evm ;
      let tf, xf, prmf, evmf = loop t0 evm prm x0 in
      (* We can also return the changed prm *)
      Out.return output (F.narrow tf) xf prmf evmf

    let simulate ~output ?(t0=F.zero) stop (evm : _ Evm.t) x0 prm =
      let rec loop t evm prm x =
        (* output *)
        Out.out output (F.narrow t) x prm evm ;
        (* get [next] event : the one with smallest projected time *)
        let color, next_event, evm = Evm.pop_next evm in
        let t' = next_event.proj_occur_time in
        (* do we stop between t and t' ? *)
        match stop t' x with
        | Some tf ->
            tf, x, prm, evm
        | None ->
            let t', evm, prm, x' = step evm prm color next_event x in
            loop t' evm prm x'
      in
      let t0 = F.narrow t0 in
      begin match stop t0 x0 with
      | None ->
          ()
      | Some _ ->
          invalid_arg "stop at t0"
      end
      ;
      Out.start output (F.narrow t0) x0 prm evm ;
      let tf, xf, prmf, evmf = loop t0 evm prm x0 in
      (* We can also return the changed prm *)
      Out.return output (F.narrow tf) xf prmf evmf
  end

(** Integrate a CTMJP with respect to a PRM exactly

    ~ Only exact for autonomous systems ~
    For a PRM implemented through Sim.Prm,
    simulate by going thourhg every necessary point,
    while revealing and hiding to try to keep rejection rate low.

    By default (auto:true), is only exact for an autonomous system.
    Otherwise, you need to pass ~auto:false,
    and more time is spent computing rates and checking bounds,
    since we know little about when they're susceptible to change.
 *)

open Ctmjp__Sig


module H = BatHashtbl


module Make (Pt : POINT) =
  struct
    (* ensure the right ordering is used *)
    module Point = Prm.Compare_time (Pt)
    module P = Prm.Make (Point)
    module Cm = Point.Colormap

    let incr = F.Pos.add (F.Pos.of_float 0.1) Prm.incrp1

    (*
     * rmaxs contains the maximum rate seen on the time slice by color
     * tsl is the current time slice
     * c is the current color
     * rate_modif is the rate and modif for the current color
     * break is [`Continue | `Refresh]
     * it says whether the bounds have changed over multiple calls
     * to increase_bounds.
     * If `Refresh, then Tsl.fold_points will continue from the right point.
     *)
    let increase_bounds
      ?(adjust=true)
      rmaxs tsl t x c rate_modif break =
      (* c_slice corresponding to the color *)
      let csl = P.Tsl.find tsl c in
      let r, _ = rate_modif t x in
      if (H.find rmaxs c) < r then
        H.replace rmaxs c r
      ;
      let uf = P.Csl.umax_revealed csl in
      if adjust then
        begin
          (* r dangerous *)
          let sclr = F.Pos.Op.(incr * r) in
          if F.Op.(sclr > uf) then begin
            (* stop : reveal or graft *)
            P.reveal_graft_upto tsl c sclr ;
            `Refresh
          end else
            break
          end
      else
        if F.Op.(r > uf) then
          failwith "Out_of_bounds"
        else
          break
      

    let lower_bounds
      ?(adjust=true)
      tsl c rmax =
      (* c_slice corresponding to the color *)
      let csl = P.Tsl.find tsl c in
      let uf = P.Csl.umax_revealed csl in
      if adjust then begin
        let sclr = F.Pos.Op.(incr * rmax) in
        if F.Pos.Op.(incr * sclr) < uf then begin
          (* hide *)
          P.hide_downto tsl (* csl *) c sclr
        end
      end


    let simulate_until
      ~(output : (_,_,_) output)
      (evm : _ Cm.t)
      x0 (prm : P.t) =
      (* ~~ *)
      (* store of max rate value observed for all colors in time slice *)
      let rmaxs = H.create (Cm.cardinal evm) in
      (* partial application *)
      let incr_bnds = increase_bounds ~adjust:true rmaxs in
      let lwr_bnds = lower_bounds ~adjust:true in
      Cm.iter (fun c _ ->
        H.add rmaxs c F.zero
      ) evm ;
      let refresh_bounds tsl t x =
        Cm.fold (incr_bnds tsl t x) evm `Continue
      in
      (* for each point *)
      let f tsl (pt : P.point) x =
        let color = Point.color pt in
        let rate_modif = Cm.find color evm in
        let t = Point.time pt in
        (* we don't use rh here because we want to be exact *)
        let r, modif = rate_modif t x in
        if F.Op.(Point.value pt <= r) then
          begin
            (* apply modif *)
            (* we need a probability *)
            let rescaled_pt =
              Point.with_value F.Pos.Op.(Point.value pt / r) pt
            in
            let x' = modif rescaled_pt in
            (* output after jump *)
            output.out (F.narrow t) x' ;
            (* refresh bounds after jump *)
            (* (also assumes autonomous *)
            match refresh_bounds tsl t x' with
            | `Continue ->
                `Continue x'
            | `Refresh ->
                `Refresh x'
          end
        else
          (* no need to refresh when no event if autonomous *)
          `Continue x
      in
      (* for each time slice *)
      let g tsl x =
        let t = fst (P.Tsl.range tsl) in
        ignore (refresh_bounds tsl t x) ;
        let x' = P.Tsl.fold_points (f tsl) tsl x in
        H.iter (lwr_bnds tsl) rmaxs ;
        (* at the end of the slice we must reinitialize rmaxs *)
        H.map_inplace (fun _ _ -> F.Pos.narrow F.zero) rmaxs ;
        x'
      in
      let t0, dt = P.time_range prm in
      let tf = F.add t0 dt in
      output.start (F.narrow t0) x0 ;
      let xf = P.fold_slices g prm x0 in
      output.return (F.narrow tf) xf
  end

module L = List

module U = Util
module F = U.Float

open Ctmjp__Sig


let specl = []


let integrate (rate_modif : (_,_) rng_rate_modif) rng =
  let next (t : _ U.anyfloat) (x : 'a) =
    (* get the rate *)
    let lambd, modif = rate_modif (F.narrow t) x in
    if F.Op.(lambd < F.zero) then
      (* TODO replace by Error `Negative_rate *)
      raise Simulation_error
    else if F.Op.(lambd = F.zero) then
      (* TODO replace by Ok *)
      (F.infinity, x)
    else
      begin
        let elapsed_t = U.rand_exp ~rng lambd in
        let next_t = F.Op.(t + elapsed_t) in
        (* get the new state *)
        let next_x = modif rng in
        (* TODO replace by Ok *)
        (next_t, next_x)
      end
  in next

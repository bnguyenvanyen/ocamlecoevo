(** Large population diffusion limit
 *
 *  of Continuous time Markov chains (CTMC)
 *  (or Markov pure Jump processes (MJP))
 *  against given Brownian increments (dBt).
 *)


open Ctmjp__Sig


let simulate_until
  ~(output : (_,_,_) output)
  (domain : Lac.Vec.t domain)
  drift
  diff
  x0
  dbts =
  let dt = Jump.Regular.dt dbts in
  let mem = Lac.Vec.make0 (Lac.Vec.dim x0) in
  let mem' = Lac.Vec.make0 (Lac.Vec.dim x0) in
  let mem'' = Lac.Vec.make0 (Lac.Vec.dim x0) in
  let loop t dbt x =
    output.out t x ;
    (* x' = x + dt * drift t x + (diff t x) * dbt *)
    (*   b = drift t x *)
    let dx = drift ~result:mem t x in
    let sgm = diff t x in
    let dx = Lac.gemv ~beta:dt ~y:dx ~alpha:1. sgm dbt in
    let x' = Lac.Vec.add ~z:mem' x dx in
    (* at the end, fresh vector *)
    let x' =
      if not (domain.is_in x') then begin
        domain.back_to ~result:mem'' x x' ;
        Lac.copy mem''
      end else
        Lac.copy x'
    in
    x'
  in
  let t0 = F.of_float (Jump.Regular.t0 dbts) in
  (* simulation end time tf is the last dbt time point + one more dt *)
  let tf = F.of_float (dt +. Jump.Regular.tf dbts) in
  output.start t0 x0 ;
  let xf = Jump.Regular.fold_left (fun x t dbt ->
    loop (F.of_float t) dbt x
  ) x0 dbts
  in
  output.return tf xf

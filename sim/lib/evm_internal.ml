(** Events data structure for Ctmjp.Prm_internal
  
    Holds events and how to update them as they happen.
 *)

open Sig


(** if [f] is of type [update_fuel],
    then [f ~from_t ~to_t x s] is the amount of fuel
    that remains at time [to_t] if at time [from_t] there was [s],
    and the rate of the event did not change between [from_t]
    and [to_t] and can be computed from state [x]. *)
type 'a update_fuel = (
  from_t:U._float ->
  to_t:U._float ->
  'a ->
  U.closed_pos ->
    U._float
)

(** if [f] is of type [project_time],
    then [f ~fuel x t] is the projected time at which
    all of the [fuel] present at time [t]
    will have been consumed, and the event will happen,
    if the rate of the event does not change until then,
    and can be computed from state [x]. *)
type 'a project_time = (
  fuel:U.closed_pos ->
  'a ->
  U._float ->
    U._float
)


type ('a, 'color) update = {
  state : (U._float -> 'a -> 'a) ;
  (** update the state of the system after the event happens *)
  affected : 'a -> 'color list ;
  (** other events affected by the focal event *)
  (* FIXME use Seq.t instead ? *)
}


(* FIXME consider making this mutable for the first three *)
type ('a, 'color) elt = {
  last_update_time : U._float ;
  (** last time of update *)
  proj_occur_time : U._float ;
  (** projected occurrence time *)
  rem_fuel : U.closed_pos ;
  (** remaining fuel after last update *)
  update_fuel : 'a update_fuel ;
  (** how to update the remaining fuel of this event *)
  project_time : 'a project_time ;
  (** how to update the projected occurrence time of this event *)
  update : ('a, 'color) update ;
  (** update the system and events due to this event *)
  single : bool ;
  (** true if the event can happen only once *)
}


module type S =
  sig
    type 'a t

    type color

    module Color : (COLOR with type t = color)

    module Colormap : (BatMap.S with type key = color)

    module Prm : (INTERNAL_PRM with type color = color)

    val empty : 'a t

    val find : color -> 'a t -> ('a, color) elt

    val pop_next : 'a t -> color * ('a, color) elt * 'a t

    val remove : color -> 'a t -> 'a t

    (** [add upd_fuel proj_t upd single c t x (evs, nu)]
        adds a new event of color [c]
        defined by [upd_fuel], [upd_proj_t], [upd], and [single]
        to [evs] at time [t] and state [x]. *)
    val add :
      'a update_fuel ->
      'a project_time ->
      ('a, color) update ->
      bool ->
      color ->
      _ U.anyfloat ->
      'a ->
      'a t * Prm.t ->
        'a t * Prm.t

    (** [update_focal nu c t x evs] updates the event of color [c]
        after it has happened at time [t] with new state [x].
        If the event happens only once, it is removed,
        otherwise a new point is taken from [nu]. *)
    val update_focal :
      Prm.t ->
      color ->
      _ U.anyfloat ->
      'a ->
      'a t ->
        'a t * Prm.t

    (** [update_affected_rem_fuel affected t x] updates the remaining fuel
     *  for the [affected] colors. *)
    val update_affected_rem_fuel :
      color list ->
      _ U.anyfloat ->
      'a ->
      'a t ->
        'a t

    (** [update_affected_proj_times affected t x] updates the projected
        occurrence times for the [affected] colors. *)
    val update_affected_proj_times :
      color list ->
      _ U.anyfloat ->
      'a ->
      'a t ->
        'a t
  end


module Fmap = BatMap.Make (struct
  type t = U._float
  let compare = F.compare
end)


module Make
  (Color : COLOR)
  (Colormap : BatMap.S with type key = Color.t)
  (Prm : INTERNAL_PRM with type color = Color.t
                       and type point = Prm_internal.Point.t
                       and type 'a colormap = 'a Colormap.t) :
  (S with type color = Color.t
      and module Colormap = Colormap
      and module Prm = Prm) =
  struct
    type color = Color.t

    module Color = Color

    module Colormap = Colormap

    module Prm = Prm

    type 'a t = {
      elts : ('a, color) elt Colormap.t ;
      (** the events indexed by their color *)
      times : color Fmap.t ;
      (** the ordered projected occurrence times and corresponding colors *)
    }

    let empty = {
      elts = Colormap.empty ;
      times = Fmap.empty ;
    }

    let find c { elts ; _ } =
      Colormap.find c elts

    let pop_next evs =
      let (t, color), times = Fmap.pop_min_binding evs.times in
      let elt = Colormap.find color evs.elts in
      (* check the time is exactly the right one *)
      assert F.Op.(t = elt.proj_occur_time) ;
      color, elt, { evs with times }

    let init update_fuel project_time update single nu c t x =
      let t = F.narrow t in
      (* FIXME do I need to do something with the return nu ? 
       * yes it needs to come back out, if there are new points
       * -- or does it get mutated ? *)
      let rem_fuel, nu' = Prm.find_min_point c nu in
      let nu' = Prm.remove_min_point c nu' in
      let proj_occur_time = project_time ~fuel:rem_fuel x t in
      let elt = {
        last_update_time = t ;
        proj_occur_time ;
        rem_fuel ;
        update_fuel ;
        project_time ;
        update ;
        single ;
      }
      in
      elt, nu'

    let add update_fuel project_time update single c t x (evs, nu) =
      let elt, nu' =
        init update_fuel project_time update single nu c t x
      in
      let times = Fmap.add elt.proj_occur_time c evs.times in
      let elts = Colormap.add c elt evs.elts in
      { times ; elts }, nu'

    (* when the event happens *)
    let update_focal nu c t x evs =
      let elt = Colormap.find c evs.elts in
      if elt.single then
        (* if the event can happen only once, remove it *)
        let elts = Colormap.remove c evs.elts in
        let times = Fmap.remove elt.proj_occur_time evs.times in
        { elts ; times }, nu
      else
        (* otherwise get the next point and re init *)
        let elt', nu' = init
          elt.update_fuel
          elt.project_time
          elt.update
          elt.single
          nu c t x
        in
        let elts = Colormap.update c c elt' evs.elts in
        let times = Fmap.update
           elt.proj_occur_time
           elt'.proj_occur_time
           c
           evs.times
        in
        { elts ; times }, nu'

    let remove c evs =
      let elt, elts = Colormap.extract c evs.elts in
      let times = Fmap.remove elt.proj_occur_time evs.times in
      { times ; elts }

    (* update the remaining fuel for affected events before the event *)
    let update_affected_rem_fuel affected t x evs =
      let t = F.narrow t in
      let update elt =
        let rem_fuel =
          elt.update_fuel
            ~from_t:elt.last_update_time
            ~to_t:t
            x
            elt.rem_fuel
        in
        let rem_fuel =
          (* check that remaining fuel is approx positive *)
          if F.Op.(F.zero <=~ rem_fuel) then
            F.positive_part rem_fuel
          else
            failwith "negative fuel"
        in
        { elt with rem_fuel ; last_update_time = t }
      in
      let elts = L.fold_right (fun c elts ->
          Colormap.modify c update elts
        ) affected evs.elts
      in
      { evs with elts }

    let update_affected_proj_times affected t x =
      let t = F.narrow t in
      let update c evs =
        let elt = Colormap.find c evs.elts in
        (* compute the new projected occurrence time *)
        let upd = elt.project_time in
        let proj_occur_time = upd ~fuel:elt.rem_fuel x t in
        (* update times *)
        let times =
          try
            Fmap.update elt.proj_occur_time proj_occur_time c evs.times
          with Not_found as e ->
            if F.classify elt.proj_occur_time = FP_infinite then
              Fmap.add proj_occur_time c evs.times
            else
              raise e
        in
        let elt' = { elt with proj_occur_time } in
        let elts = Colormap.update c c elt' evs.elts in
        { elts ; times }
      in
      L.fold_right update affected
  end


open Sim_test.Sig
open Prm_base


(* rand_point_from raises Not_found on empty set *)
let%test _ =
  match P.rand_point_from ~rng:(U.rng None) P.Ps.empty with
  | _ ->
      false
  | exception Invalid_argument _ ->
      true

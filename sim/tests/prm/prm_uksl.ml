open Sim_test.Sig

module P = Prm_base.P

let create () = Prm_base.(
  let origin = point ~k:0 ~t:F.zero ~u:F.zero in
  let vector = point ~k:1 ~t:F.one ~u:F.one in
  P.UKsl.create () P.{ origin ; vector } pointset
)


(* color *)
let%test _ =
  let uksl = create () in
  () = (P.UKsl.color uksl)

(* number *)
let%expect_test _ =
  let uksl = create () in
  Printf.printf "%i" (P.UKsl.number uksl) ;
  [%expect{| 0 |}]


(* count *)
let%expect_test _ =
  let uksl = create () in
  let c = P.UKsl.count uksl in
  Printf.printf "%i" (I.to_int c) ;
  [%expect{| 4 |}]


(* range *)
let%expect_test _ =
  let uksl = create () in
  let u0, uf = P.UKsl.range uksl in
  Printf.printf "(%f, %f)" (F.to_float u0) (F.to_float uf) ;
  [%expect{| (0.000000, 1.000000) |}]


(* add_points *)
let pt () =
  Prm_base.point ~k:0 ~t:(fpof 0.5) ~u:(fpof 0.24)

let%test _ =
  let uksl = create () in
  let pts = P.Ps.singleton (pt ()) in
  let uksl' = P.UKsl.add_points pts uksl in
  let diff_c = I.sub (P.UKsl.count uksl') (P.UKsl.count uksl) in
  I.Op.(diff_c = I.one)

let%test _ =
  let uksl = create () in
  let pts = P.Ps.singleton (pt ()) in
  let uksl' = P.UKsl.add_points pts uksl in
  let diff_pt = P.Ps.diff (P.UKsl.points uksl') (P.UKsl.points uksl) in
  P.Ps.equal pts diff_pt



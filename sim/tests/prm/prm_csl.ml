open Sim_test.Sig

module P = Prm_base.P


let create () = Prm_base.(
  let origin = point ~k:0 ~t:F.zero ~u:F.zero in
  let vector = point ~k:1 ~t:F.one ~u:F.one in
  let nuslices = I.Pos.of_int 2 in
  P.Csl.create () P.{ origin ; vector } nuslices pointset
)


let draw () = Prm_base.(
  let rng = U.rng (Some 0) in
  let origin = point ~k:0 ~t:F.zero ~u:F.zero in
  let vector = point ~k:1 ~t:F.one ~u:F.one in
  let nuslices = I.Pos.of_int 2 in
  P.Csl.rand_draw ~rng () P.{ origin ; vector } nuslices
)


(* color *)
let%test _ =
  let csl = create () in
  () = (P.Csl.color csl)


(* umax *)
let%expect_test _ =
  let csl = create () in
  let umax = P.Csl.umax_revealed csl in
  Printf.printf "%f" (F.to_float umax) ;
  [%expect{| 1.000000 |}]


(* volume *)
let%expect_test _ =
  let csl = create () in
  let vol = P.Csl.volume csl in
  Printf.printf "%f" (F.to_float vol) ;
  [%expect{| 1.000000 |}]


(* u_boundaries *)
let%expect_test _ = Prm_base.(
  let prm = create () in
  let tsl = P.t_slice_of prm (fpof 0.8) in
  let csl = P.Tsl.find tsl () in
  let us = P.Csl.u_boundaries csl in
  Printf.printf "[\n" ;
  L.iter (fun x -> Printf.printf "%f ;\n" (F.to_float x)) us ;
  Printf.printf "]" ;
  [%expect{|
    [
    0.000000 ;
    0.666667 ;
    1.000000 ;
    ] |}]
)


(* add_points *)
let pt () =
  Prm_base.point ~k:0 ~t:(fpof 0.5) ~u:(fpof 0.24)

(* add_points *)
let%test _ =
  let csl = create () in
  let points = P.Csl.points csl in
  let pts = P.Ps.singleton (pt ()) in
  P.Csl.add_points pts csl ;
  let diff_points = P.Ps.diff (P.Csl.points csl) points in
  P.Ps.equal pts diff_points

(* add then remove points *)
let%test _ =
  let csl = create () in
  let points = P.Csl.points csl in
  (* pts not in csl before add_points *)
  let pts = P.Ps.singleton (pt ()) in
  P.Csl.add_points pts csl ;
  P.Csl.remove_points pts csl ;
  let diff_points = P.Ps.diff (P.Csl.points csl) points in
  P.Ps.is_empty diff_points


(*
(* add_volume *)
let%test _ =
  let csl = create () in
  let vol = P.Csl.volume csl in
  let dvol = F.one in
  P.Csl.add_volume dvol csl ;
  let vol' = P.Csl.volume csl in
  Printf.eprintf "vol = %f ; vol' = %f\n" (F.to_float vol) (F.to_float vol') ;
  F.(Op.(abs (vol + dvol - vol') < of_float 1e-9))


(* subtract_volume *)
let%test _ =
  let csl = create () in
  let vol = P.Csl.volume csl in
  let dvol = F.one in
  P.Csl.subtract_volume dvol csl ;
  let vol' = P.Csl.volume csl in
  F.Op.(vol - dvol = vol')
*)


(* hide_k *)
(* raises Not_found on absent k *)
let%test _ =
  let csl = create () in
  match P.Csl.hide_k csl (~- 1) with
  | _ ->
      false
  | exception Not_found ->
      true

(* remaining points after hide *)
let%expect_test _ =
  let csl = create () in
  let _ = P.Csl.hide_k csl 0 in
  Prm_base.print_points (P.Csl.points csl) ;
  [%expect]

(* original points = remaining points + returned points *)
let%test _ =
  let csl = create () in
  let points = P.Csl.points csl in
  let dpts = P.Csl.hide_k csl 0 in
  let points' = P.Csl.points csl in
  (P.Ps.equal points (P.Ps.union points' dpts))

(*
(* original volume = remaining volume + returned volume *)
let%test _ =
  let csl = create () in
  let vol = P.Csl.volume csl in
  let _, dvol = P.Csl.hide_k csl 0 in
  let vol' = P.Csl.volume csl in
  F.Op.(vol = vol' + dvol)
*)

(* raises Already_hidden if we hide twice *)
let%test _ =
  let csl = create () in
  let _ = P.Csl.hide_k csl 0 in
  match P.Csl.hide_k csl 0 with
  | _ ->
      false
  | exception P.Csl.Already_hidden ->
      true


(* hide_u *)
(* remaining points and new umax_revealed after hide *)
let%expect_test _ =
  let csl = create () in
  let _ = P.Csl.hide_u csl in
  Printf.printf "%f" (F.to_float (P.Csl.umax_revealed csl)) ;
  [%expect{| 0.666667 |}] ;
  Prm_base.print_points (P.Csl.points csl) ;
  [%expect{|
    (0.350000, 0.250000) ;
    (0.600000, 0.150000) ; |}]

(* original points = remaining points + returned points
 * && original volume = remaining volume + returned volume *)
let%test _ =
  let csl = create () in
  let points = P.Csl.points csl in
  (* let vol = P.Csl.volume csl in *)
  let dpts = P.Csl.hide_u csl in
  let points' = P.Csl.points csl in
  (* let vol' = P.Csl.volume csl in *)
     (P.Ps.equal points (P.Ps.union points' dpts))
  (* && F.Op.(vol = vol' + dvol) *)

(* raises Already_hidden if we hide too much *)
let%test _ =
  let csl = create () in
  let _ = P.Csl.hide_u csl in
  match P.Csl.hide_u csl with
  | _ ->
      false
  | exception P.Csl.Already_hidden ->
      true


(* reveal_k *)
(* raises Not_found on absent k *)
let%test _ =
  let csl = create () in
  match P.Csl.reveal_k csl (~- 1) with
  | _ ->
      false
  | exception Not_found ->
      true

(* raises Already_revealed on already revealed k *)
let%test _ =
  let csl = create () in
  match P.Csl.reveal_k csl 0 with
  | _ ->
      false
  | exception P.Csl.Already_revealed ->
      true

(* hide then reveal changes nothing *)
let%test _ =
  let csl = create () in
  let points = P.Csl.points csl in
  let vol = P.Csl.volume csl in
  let _ = P.Csl.hide_k csl 0 in
  let _ = P.Csl.reveal_k csl 0 in
  let points' = P.Csl.points csl in
  let vol' = P.Csl.volume csl in
     (P.Ps.equal points points')
  && F.Op.(vol = vol')

(* original points + returned points = remaining points
 * && original volume + returned volume = remaining volume *)
let%test _ =
  let csl = create () in
  let _ = P.Csl.hide_k csl 0 in
  let points = P.Csl.points csl in
  (* let vol = P.Csl.volume csl in *)
  let dpts = P.Csl.reveal_k csl 0 in
  let points' = P.Csl.points csl in
  (* let vol' = P.Csl.volume csl in *)
     (P.Ps.equal (P.Ps.union points dpts) points')
  (* && F.Op.(vol + dvol = vol') *)


(* reveal_u *)
(* raises Already_revealed when everything is revealed *)
let%test _ =
  let csl = create () in
  match P.Csl.reveal_u csl with
  | _ ->
      false
  | exception P.Csl.Already_revealed ->
      true

(* hide then reveal changes nothing *)
let%test _ =
  let csl = create () in
  let points = P.Csl.points csl in
  let vol = P.Csl.volume csl in
  let _ = P.Csl.hide_u csl in
  let _ = P.Csl.reveal_u csl in
  let points' = P.Csl.points csl in
  let vol' = P.Csl.volume csl in
     (P.Ps.equal points points')
  && F.Op.(vol = vol')

(* original points + returned points = remaining points
 * && original volume + returned volume = remaining volume *)
let%test _ =
  let csl = create () in
  let _ = P.Csl.hide_u csl in
  let points = P.Csl.points csl in
  (* let vol = P.Csl.volume csl in *)
  let dpts = P.Csl.reveal_u csl in
  let points' = P.Csl.points csl in
  (* let vol' = P.Csl.volume csl in *)
     (P.Ps.equal (P.Ps.union points dpts) points')
  (* && F.Op.(vol + dvol = vol') *)


(* graft_k *)
(* raises Invalid_argument on ungraftable csl *)
let%test _ =
  let csl = create () in
  match P.Csl.graft_k csl with
  | _ ->
      false
  | exception Invalid_argument _ ->
      true

(* max k is 1 after graft *)
let%test _ =
  let csl = draw () in
  let k, _ = P.Csl.graft_k csl in
  k = 1

(* raises Not_all_revealed if we hide then graft *)
let%test _ =
  let csl = draw () in
  let _ = P.Csl.hide_k csl 0 in
  match P.Csl.graft_k csl with
  | _ ->
      false
  | exception P.Csl.Not_all_revealed ->
      true

(* original points + returned points = remaining points
 * && original volume + returned volume = remaining volume *)
let%test _ =
  let csl = draw () in
  let points = P.Csl.points csl in
  (* let vol = P.Csl.volume csl in *)
  let _, dpts = P.Csl.graft_k csl in
  let points' = P.Csl.points csl in
  (* let vol' = P.Csl.volume csl in *)
     (P.Ps.equal (P.Ps.union points dpts) points')
  (* && F.(Op.(abs (vol + dvol - vol') < of_float 1e-9)) *)


(* graft_u *)
(* raises Invalid_argument on ungraftable csl *)
let%test _ =
  let csl = create () in
  match P.Csl.graft_u csl with
  | _ ->
      false
  | exception Invalid_argument _ ->
      true

(* raises Not_all_revealed if we hide then graft *)
let%test _ =
  let csl = draw () in
  let _ = P.Csl.hide_u csl in
  match P.Csl.graft_u csl with
  | _ ->
      false
  | exception P.Csl.Not_all_revealed ->
      true

(* original points + returned points = remaining points
 * && original volume + returned volume = remaining volume *)
let%test _ =
  let csl = draw () in
  let points = P.Csl.points csl in
  (* let vol = P.Csl.volume csl in *)
  let dpts = P.Csl.graft_u csl in
  let points' = P.Csl.points csl in
  (* let vol' = P.Csl.volume csl in *)
     (P.Ps.equal (P.Ps.union points dpts) points')
  (* && F.Op.(vol + dvol = vol') *)


(* map_slices *)

(* map with identity -> volume stays the same if one slice was hidden *)
let%test _ =
  let csl = draw () in
  let _ = P.Csl.hide_u csl in
  let vol = P.Csl.volume csl in
  let csl' = P.Csl.map_slices (fun uksl -> uksl) csl in
  let vol' = P.Csl.volume csl' in
  F.(Op.(abs (vol' - vol) < of_float 1e-9))

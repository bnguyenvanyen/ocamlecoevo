open Sig


(* test redraw shares memory *)
let%expect_test _ =
  let rng = U.rng (Some 0) in
  let dbts =
    Sim.Dbt.rand_draw
      ~rng
      ~t0:F.zero
      ~dt:(F.Pos.of_float 0.5)
      ~tf:F.one
      ~n:3
  in
  let dbts' = Sim.Dbt.redraw ~rng ~n:3 2 dbts in
  Jump.Regular.print
    BatFloat.print
    BatIO.stdout
    (Jump.Regular.map (fun _ dbt -> dbt.{1}) dbts)
  ;
  [%expect{|
    0.: 1.25070344184
    0.5: -0.406273774619 |}] ;
  Jump.Regular.print
    BatFloat.print
    BatIO.stdout
    (Jump.Regular.map (fun _ dbt -> dbt.{1}) dbts')
  ;
  [%expect{|
    0.: 1.25070344184
    0.5: -0.406273774619 |}]

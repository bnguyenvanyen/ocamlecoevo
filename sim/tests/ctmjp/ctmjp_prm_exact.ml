open Sim_test.Sig
open Ctmjp_prm_base


module Ce = SC.Prm_exact.Make (Pb.Point)

(* test with one event with constant rate 0.5.
 * Of the 4 points, some are below, some are above *)
let%expect_test _ =
  let evm = combine (evm_constant ()) in
  let time_range = Pb.time_range in
  let ntslices = Pb.ntslices in
  let vectors = Pb.vectors () in
  let points = L.fold_left (fun pts pt ->
    Ce.P.Ps.add pt pts
  ) Ce.P.Ps.empty Pb.points 
  in
  let prm = Ce.P.create ~time_range ~ntslices ~vectors points in
  ignore (Ce.simulate_until ~output:B.output_int evm 0 prm) ;
  [%expect{|
    0.,0
    0.35,1
    0.6,2
    1.,2 |}]


(* test with linear birth rate *)
let%expect_test _ =
  let evm = combine (evm_linear ()) in
  let time_range = Pb.time_range in
  let ntslices = Pb.ntslices in
  let vectors = Pb.vectors () in
  let points = L.fold_left (fun pts pt ->
    Ce.P.Ps.add pt pts
  ) Ce.P.Ps.empty Pb.points 
  in
  let prm = Ce.P.create ~time_range ~ntslices ~vectors points in
  ignore (Ce.simulate_until ~output:B.output_int evm 1 prm) ;
  [%expect{|
    0.,1
    0.6,2
    1.,2 |}]


(* test with non-autonomous rate, and autonomous mode,
 * to show that it's wrong.
 * Normally, we should accept the points at 0.35, at 0.6, at 0.9,
 * Instead none is accepted *)
let%expect_test _ =
  let evm = combine (evm_linear ()) in
  let time_range = Pb.time_range in
  let ntslices = Pb.ntslices in
  let vectors = Pb.vectors () in
  let points = L.fold_left (fun pts pt ->
    Ce.P.Ps.add pt pts
  ) Ce.P.Ps.empty Pb.points 
  in
  let prm = Ce.P.create ~time_range ~ntslices ~vectors points in
  ignore (Ce.simulate_until ~output:B.output_int evm 0 prm) ;
  [%expect{|
    0.,0
    1.,0 |}]

open Sim_test.Sig

module B = Ctmjp_base


let%expect_test _ =
  (* linear birth *)
  let ev = (
    (fun _ x -> F.of_float (0.2 *. x.{1})),
    (Lac.Vec.make 1 1.)
  )
  in
  let domain = Sim.Sig.{
    is_in = (fun x -> x.{1} >= 0.) ;
    back_to = Sim.Domain.Vec.reflect_back_to_pos 1 ;
  }
  in
  let x0 = Lac.Vec.make 1 1. in
  let rng = U.rng (Some 0) in
  let dbts =
    Sim.Dbt.rand_draw
      ~rng
      ~t0:F.zero
      ~dt:(fpof 0.1)
      ~tf:F.one
      ~n:1
  in
  (* show dbts *)
  Sim.Dbt.print BatIO.stdout dbts ;
  [%expect{|
    0.: 0.587315
    0.1: 0.24396
    0.2: 0.333781
    0.3: -0.151034
    0.4: 0.291392
    0.5: 0.508597
    0.6: -0.0275774
    0.7: 0.559332
    0.8: -0.109635
    0.9: 0.14535 |}] ;
  (* show traj *)
  ignore (Sim.Ctmjp.Sde_limit.simulate_until
    ~output:B.output_vec1
    domain
    [ev]
    x0
    dbts
  ) ;
  [%expect{|
    0.,1.
    0.,1.
    0.1,1.28265532809
    0.2,1.43187151596
    0.3,1.63912844162
    0.4,1.58543477948
    0.5,1.78122789708
    0.6,2.1204150721
    0.7,2.1448645055
    0.8,2.55410173718
    0.9,2.52682597032
    1.,2.68069022047 |}]

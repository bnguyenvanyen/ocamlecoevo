open Sim_test.Sig

let incr_modif _ _ n =
  n + 1


let output_int =
  Sim.Csv.convert
    ?dt:None
    ~header:[[]]
    ~line:(fun t n -> [ F.to_string t ; string_of_int n ])
    ~chan:stdout


let output_vec1 =
  Sim.Csv.convert
    ?dt:None
    ~header:[[]]
    ~line:(fun t (v : Lac.Vec.t) -> [ F.to_string t ; string_of_float v.{1} ])
    ~chan:stdout

open Sim_test.Sig

module B = Ctmjp_base
module Pb = Prm_test.Prm_base


(* The Poisson process with rate 0.5 *)
let evm_constant () =
  Pb.Point.Colormap.singleton () (
    (fun _ _ -> fpof 0.5),
    B.incr_modif
  )


(* The pure linear birth process (Yule) *)
let evm_linear () =
  Pb.Point.Colormap.singleton () (
    (fun _ n -> fpof (0.2 *. float_of_int n)),
    B.incr_modif
  )


(* Non-autonomous system where the rate is time *)
let evm_time () =
  Pb.Point.Colormap.singleton () (
    (fun t _ -> F.Pos.of_anyfloat t),
    B.incr_modif
  )


let combine evm =
  Pb.Point.Colormap.map (fun (rate, modif) ->
    SCU.combine rate modif
  ) evm

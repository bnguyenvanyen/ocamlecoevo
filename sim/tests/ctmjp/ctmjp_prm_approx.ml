open Sim_test.Sig
open Ctmjp_prm_base


module Ca = SC.Prm_approx.Make (Sim.Prm.Make) (Pb.Point)


let%expect_test _ =
  let evm = evm_constant () in
  let time_range = Pb.time_range in
  let ntslices = Pb.ntslices in
  let vectors = Pb.vectors () in
  let points = L.fold_left (fun pts pt ->
    Ca.P.Ps.add pt pts
  ) Ca.P.Ps.empty Pb.points 
  in
  let prm = Ca.P.create ~time_range ~ntslices ~vectors points in
  ignore (Ca.simulate_until ~output:B.output_int evm 0 prm) ;
  [%expect{|
    0.,0
    0.,0
    0.5,1
    1.,2 |}]

(** MCMC for the seasonal SEIRS model, with case count and sequence data
 *
 *  I don't try to be to clever about reusability here,
 *  and just do a SEIRS + JC69 version
 *)

open Sig

(* we only simulate the forward model deterministically *)
module Forward_seirs = Epi.Process.Continuous.Make (Epi.Seirs.Model)


(* Trait for coalescence *)
module Trait_seirs =
  struct
    (* a lineage can only be a E or a I *)
    type t =
      | E
      | I

    let equal = (=)
    let compare = compare
    let to_string =
      function
      | E ->
          "E"
      | I ->
          "I"
    let of_string =
      function
      | "E" ->
          Some E
      | "I" ->
          Some I
      | _ ->
          None
  end


module Coal_seirs = Coal.Generic.Make (Trait_seirs)


module Theta =
  struct
    class t = object
      (* ecological parameters *)
      inherit Epi.Param.t_unit
      inherit Epifit.Param.init_sepir
      
      val prm_internal_v : Coal_seirs.Evm.Prm.t option = None
      val mu_v = 1e-4

      method prm_internal = U.Option.some prm_internal_v
      method mu = mu_v

      method with_prm_internal nu = {< prm_internal_v = Some nu >}
      method with_mu x = {< mu_v = x >}
    end

    let default = new t

    let extract (th : t) =
      function
      | "mu" ->
          string_of_float th#mu
      | s ->
          (* I lack a proof that the column "prm" won't be passed.
           * Might be possible if built from tags ? *)
          Epifit.Seirs.Fit.extract th s

    let load_from =
      let doc =
        "Load theta parameter values from $(docv)."
      in
      Cmdliner.Arg.(
        value
        & opt (some string) None
        & info ["load-theta-from"] ~docv:"LOAD-THETA-FROM" ~doc
      )

    let mu =
      let doc =
        "Mutation rate"
      in
      let get th = th#mu in
      let set x th = th#with_mu x in
      U.Arg.Update.Capture.opt_config get set Cmdliner.Arg.float ~doc "mu"

    let term =
      (* FIXME also take the other pars *)
      let f mu config capture =
        (new t)
        |> U.Arg.Capture.empty
        |> mu ~config
        |> U.Arg.Capture.output capture
      in Cmdliner.Term.(
        const f
        $ mu
        $ load_from
      )
  end


module Hyper =
  struct
    class t = object
      inherit Generic.Hyper.context
      inherit Generic.Hyper.prior_evo
      inherit Generic.Hyper.prop_evo
      inherit Generic.Hyper.prm_able
      inherit Epifit.Seirs.hyper_prior
      inherit Epifit.Seirs.hyper_prop
    end

    let dt hy =
      F.Pos.of_float hy#dt

    let with_prm_regraft_rng hy rng =
      hy#with_prm_regraft_rng rng

    let prm_mutations_drift_coeff hy =
      F.Pos.of_float hy#prm_mutations_drift_coeff

    let prm_mutations_volatility hy =
      F.Pos.of_float hy#prm_mutations_volatility

    let prm_merge_jump hy =
      F.Pos.of_float hy#prm_merge_jump

    let mu_var hy =
      F.Pos.of_float hy#mu_var

    let mu_jump hy =
      F.Pos.of_float hy#mu_jump

    let term =
      (* For now, no 'par-from' *)
      let f epi_base epi_latent epi_circular context evo prm load_theta config capture =
        (new t)
        |> U.Arg.Capture.empty
        |> epi_base load_theta ~config
        |> epi_latent ~config
        |> epi_circular ~config
        |> context ~config
        |> evo ~config
        |> prm ~config
        |> U.Arg.Capture.output capture
      in
      Cmdliner.Term.(
        const f
        $ Epifit.Term.Hyper.base ()
        $ Epifit.Term.Hyper.latent ()
        $ Epifit.Term.Hyper.circular ()
        $ Generic.Hyper.Term.context ()
        $ Generic.Hyper.Term.evo ()
        $ Generic.Hyper.Term.prm_able ()
        $ Generic.Theta.load_from
        $ Generic.Hyper.load_from
      )
  end


module Eco =
  struct
    type theta = Theta.t
    type trait = Trait_seirs.t
    type hyper = Hyper.t

    module Data = Coalfit.Generic.Data.Make (Trait_seirs)

    type data = Data.t

    module Forward =
      struct
        type t = Traj.t

        let sim (hy : hyper) (th : theta) =
          let t0 = hy#t0 in
          let tf = hy#tf in
          let dt = Hyper.dt hy in
          (* simulate from t0 to tf with the right initialization,
           * and parameter values.
           * TODO we might want to pass parameters to Lsoda *)
          let par = (th :> Epi.Param.t_unit) in
          let z0 =
            th
            |> Epifit.Param.sepir ~latency:Epi.Seirs.Model.spec.latency
            |> Forward_seirs.sepir_to_seir th
            |> Forward_seirs.of_seir
          in
          (* TODO this also changes with the type *)
          let output =
            Sim.Cadlag.convert ~dt ~conv:(Forward_seirs.conv par)
          in
          let duration = F.Pos.of_float (tf -. t0) in
          let epi_traj = Forward_seirs.Ode.sim par z0 ~output duration in
          Traj.create ~t0 ~tf epi_traj

        let output chan =
          let columns = "k" :: Epi.Traj.columns in
          Csv.output_record chan columns ;
          (fun k traj ->
            let f (t, pt) =
              function
              | "k" ->
                  string_of_int k
              | s ->
                  Epi.Traj.extract (F.of_float t, pt) s
            in
            let traj' = Jump.Regular.to_list traj.Traj.forward in
            U.Csv.write ~f:(U.Csv.Row.unfold ~columns f) ~chan traj'
          )
      end

    module Coal =
      struct
        module Trait = Trait_seirs

        module Pop = Coal_seirs.Pop
        module Evm = Coal_seirs.Evm
        module Color = Coal_seirs.Color
        module Colormap = Evm.Colormap
        module Prm = Coal_seirs.Prm
        module Point = Prm.Point

        module R = Traj.Rates

        type color = Color.t
        type point = Point.t
        type prm = Prm.t
        type tree = Pop.tree
        type t = Pop.t

        let rand_choose_int ~rng intset =
          U.rand_unif_choose_set
            (module Intset : BatSet.S with type elt = int
                                       and type t = Intset.t)
            ~rng
            intset

        let prm (th : theta) =
          (* FIXME not the right way to deal with this *)
          Prm.copy th#prm_internal

        let with_prm th (x : prm) =
          th#with_prm_internal x

        let forest (z : t) =
          Pop.to_forest z

        let rate_infect th t pt =
          (* FIXME check t *)
          R.infection_of Epi.Seirco.var_infectivity th t pt

        (* Rate of mutation I -> E in the coalescent
         * Total rate of E -> I in Forward : sigma * E
         * (I take (E - 1) because cadlag and I look in backward time)
         * We observe this on our focal lineage with probability 1 / I *)
        let rate_leave_exposed th traj =
          let comp =
            function
            | Trait.E ->
                `Constant 0.
            | Trait.I ->
                `Traj (Jump.Regular.map (fun t pt ->
                  let pt_before = Epi.Traj.{
                    pt with e = F.Pos.of_anyfloat F.Op.(pt.e - F.one)
                  }
                  in
                  let fr = R.leave_exposed th (F.of_float t) pt_before in
                  let r = F.Pos.Op.(fr / pt.i) in
                  F.to_float r
                ) traj)
          in comp

        (* TODO Differentiate between infections from inside
         * and from outside with a lineage immigration event *)

        (* Rate of mutation E -> I in the coalescent, due to an unobserved infection
         * Total rate of infection S + I -> E + I in Forward : beta(t) S/N (I + eta)
         * (I take (E - 1) and (S + 1),
         * because backward on cadlag ('I' stays constant))
         * We observe on the focal lineage with probability 1 / E * (I - A_I) / I *)
        let rate_unobserved_infection th traj =
          let comp =
            function
            | Trait.I ->
                `Constant 0.
            | Trait.E ->
                `Traj (Jump.Regular.map (fun t pt ->
                  let pt_before = Epi.Traj.{
                    pt with e = F.Pos.of_anyfloat F.Op.(pt.e - F.one) ;
                            s = F.Pos.Op.(pt.s + F.one) ;
                  }
                  in
                  let ri = rate_infect th (F.of_float t) pt_before in
                  (* number of i lineages *)
                  (* if [a_i << i] always, we can ignore this correction.
                   * But it can actually happen that [ai > i],
                   * then the rate of unobserved merge is zero.
                   * FIXME what to do for rate_merge when [ai > i] ?
                   *)
                  (* FIXME ai needs z *)
                  (* let ai = I.to_float (Pop.count Trait.I z) in *)
                  let ai = F.zero in
                  let correction = F.positive_part F.Op.((pt.i - ai) / pt.i) in
                  let r = F.Pos.Op.(ri / pt.e * correction) in
                  F.to_float r
                ) traj)
          in comp

        let rate_merge_observed_infection th traj =
          let comp x x' =
            match x, x' with
            | Trait.I, Trait.I
            | Trait.E, Trait.E ->
                `Constant 0.
            | Trait.I, Trait.E
            | Trait.E, Trait.I ->
                `Traj (Jump.Regular.map (fun t pt ->
                  let pt_before = Epi.Traj.{
                    pt with e = F.Pos.of_anyfloat F.Op.(pt.e - F.one) ;
                            s = F.Pos.of_anyfloat F.Op.(pt.s + F.one) ;
                  }
                  in
                  let ri = rate_infect th (F.of_float t) pt_before in
                  let r = F.Pos.Op.(ri / (pt.e * pt.i)) in
                  F.to_float r
                ) traj)
          in comp

        let mutations = [
          (Trait.I, Trait.E, rate_leave_exposed) ;
          (Trait.E, Trait.I, rate_unobserved_infection) ;
        ]

        let merges = [
          (Trait.I, Trait.E, Trait.I, rate_merge_observed_infection) ;
        ]

        let rand_prm ~rng ~samples hy th (traj : Forward.t) =
          (* FIXME need better uf *)
          let mutations =
            List.map (fun (src, dst, rate) ->
              (src, dst, rate th traj.forward)
            ) mutations
          in
          let merges =
            List.map (fun (src, src', dst, rate) ->
              (src, src', dst, rate th traj.forward)
            ) merges
          in
          let tf = F.Pos.of_float hy#tf in
          Coal_seirs.Backward.rand_prm
            ~rng
            ~mutations
            ~merges
            ~samples
            ~tf

        let logd_prm nu = Coal_seirs.Prm.logd_prm nu

        (* on the starting nu, colors for all possible events are present *)

        let rand_merge_color ~rng nm1 =
          let j = I.Pos.succ (U.rand_int ~rng nm1) in
          let i = U.rand_int ~rng j in
          let is = Intset.singleton (I.to_int i + 1) in
          let js = Intset.singleton (I.to_int j + 1) in
          Color.merge Trait.E Trait.I Trait.I is js

        let rand_mutation_color ~rng nm1 =
          let i = I.Pos.succ (U.rand_int ~rng nm1) in
          let ip1 = I.to_int i + 1 in
          let is = Intset.singleton ip1 in
          if U.rand_bool ~rng then
            Color.mutation Trait.I Trait.E is
          else
            Color.mutation Trait.E Trait.I is

        let update_merge_point color dx nu =
          let nu' = Prm.copy nu in
          Prm.update_merge_point color dx nu'

        let update_merge_points k k' dx nu =
          (* all relevant colors : only one in this case *)
          let color = Color.merge
            Trait.E Trait.I Trait.I
            (Intset.singleton k)
            (Intset.singleton k')
          in
          Prm.update_merge_point color dx nu

        let update_mutation_points color f nu =
          let nu' = Prm.copy nu in
          Prm.update_mutation_points color f nu'

        let rand_unmerged_color ~rng =
          function
          | (Color.Sample _ | Color.Merge _) as c ->
              invalid_arg (
                Printf.sprintf "not mutation color %s" (Color.to_string c)
              )
          | Color.Mutation (x, y, ks) ->
              let k = rand_choose_int ~rng ks in
              Color.mutation x y (Intset.singleton k)

        let regraft_from ~rng grafted nu =
          (* for each merged color of grafted,
           * for each point,
           * choose an unmerged color randomly,
           * and add the point to nu *)
          Colormap.fold (fun c pts ->
            Prm.Points.fold (fun pt ->
              (* FIXME problem of reproducibility because of rng here *)
              let color = rand_unmerged_color ~rng c in
              Prm.add_graftable_point color pt
            ) pts
          ) grafted nu

        let sim data (hy : hyper) (th : theta) (traj : Forward.t) =
          (* as a (faster) function *)
          let t0 = F.Pos.of_float hy#t0 in
          let tf = F.Pos.of_float hy#tf in
          let samples = Data.sorted data in
          let mutations = L.map (fun (src, dst, rate) ->
              (src, dst, rate th traj.forward)
            ) mutations
          in
          let merges = L.map (fun (src, src', dst, rate) ->
              (src, src', dst, rate th traj.forward)
            ) merges
          in
          let nu = th#prm_internal in
          (* runs on a copy of nu *)
          let zf, nuf =
            Coal_seirs.Backward.until
              ~t0
              ~tf
              ~mutations
              ~merges
              ~samples
              nu
          in
          (* get the newly grafted points during simulation,
           * and add them to the original nu *)
          let grafted = Prm.grafted nuf in
          (* need a rng to draw decide the lineages
           * that are given the mutations *)
          let rng = hy#prm_regraft_rng in
          let nu' = regraft_from ~rng grafted nu in
          (* then what do we do with it ? *)
          zf, th#with_prm_internal nu'

        let output data chan =
          let n = Data.length data in
          Csv.output_record chan (Pop.header_all_mrca n) ;
          (fun k z ->
            let mrcas = Pop.time_mrcas n z in
            Pop.out_all_mrca chan k mrcas
          )

        let output_prm chan =
          Csv.output_record chan ["k" ; "color" ; "t"] ;
          (fun k nu ->
            Prm.iter_points (fun c pt ->
              Csv.output_record chan [
                string_of_int k ;
                Color.to_string c ;
                F.to_string (Point.time pt) ;
              ]
            ) nu
          )
      end
  end


module Evo =
  struct
    type theta = Theta.t
    type trait = Trait_seirs.t
    type tree = Eco.Coal.Pop.tree

    module Lik = Coalfit.Likelihood.Make (Eco.Coal.Pop.S) (Eco.Coal.Pop.T)

    let loglik kseqs theta forest =
      Lik.loglik kseqs theta forest
  end


module Infer =
  struct
    type tag = [
      | `Prm
      | `Prm_mutations
      | `Mu
      | Epifit.Tag.base
      | Epifit.Tag.latent
      | Epifit.Tag.circular
    ]

    type theta = Theta.t
    type hyper = Hyper.t
    type prm = Coal_seirs.Prm.t

    class t = object
      (* epi stuff *)
      inherit Epifit.Seirs.infer_pars_ode

      (* coal stuff *)
      val prm_internal_v = Fit.Infer.adapt

      method prm_internal = prm_internal_v

      method with_prm_internal inf = {< prm_internal_v = inf >}

      (* evo stuff *)
      val mu_v = Fit.Infer.adapt

      method mu = mu_v

      method with_mu inf = {< mu_v = inf >}
    end

    let prm infr =
      infr#prm_internal

    let prm_mutations_tag = `Prm_mutations

    let prm_tag = `Prm

    let propose_prm_mutations = true

    let mu hy infer =
      Fit.Infer.maybe_adaptive_float
        ~tag:`Mu
        ~get:(fun th -> th#mu)
        ~set:(fun th x -> th#with_mu x)
        ~prior:(Fit.Dist.Lognormal (hy#mu_mean, Hyper.mu_var hy))
        ~proposal:(Fit.Propose.endo_dist (fun x ->
           Fit.Dist.Normal (x, Hyper.mu_jump hy)
         ))
        ~jump:hy#mu_jump
        infer

    module ES = Epifit.Seirs

    let tag_to_string =
      function
      | `Mu ->
          "mu"
      | `Prm ->
          "prm"
      | Epifit.Tag.(
          #base
        | #latent
        | #circular
        ) as tag ->
          Epifit.Seirs.Fit.tag_to_string tag

    let tag_to_columns =
      function
      | `Mu ->
          ["mu"]
      | `Prm | `Prm_mutations ->
          (* no columns for prm *)
          []
      | Epifit.Tag.(
          #base
        | #latent
        | #circular
        ) as tag ->
          Epifit.Seirs.Fit.tag_to_columns tag

    let params (hy : hyper) infr =
      (* FIXME problem : type of tag in pars *)
      let pars =
        ES.Fit.params (infr :> ES.infer_pars_ode) hy
      in
      Coalfit.Generic.El (mu hy infr#mu :: pars)

    let infer_tags =
        [`Mu ; `Prm]
      @ Epifit.Symbols.(base @ latent @ circular)

    let with_infer_tag is_adapt tag infer =
      match tag with
      | `Mu ->
          infer#with_mu (Fit.Infer.free is_adapt)
      | `Prm ->
          infer#with_prm_internal (Fit.Infer.free is_adapt)
      | #Epifit.Tag.base as tag ->
          Epifit.Infer.infer_base is_adapt tag infer
      | #Epifit.Tag.latent as tag ->
          Epifit.Infer.infer_latent is_adapt tag infer
      | #Epifit.Tag.circular as tag ->
          Epifit.Infer.infer_circular is_adapt tag infer

    let with_fix_tag =
      function
      | `Mu ->
         (fun infr -> infr#with_mu Fit.Infer.fixed)
      | `Prm ->
         (fun infr -> infr#with_prm_internal Fit.Infer.fixed)
      | #Epifit.Tag.base as tag ->
          Epifit.Infer.fix_base tag
      | #Epifit.Tag.latent as tag ->
          Epifit.Infer.fix_latent tag
      | #Epifit.Tag.circular as tag ->
          Epifit.Infer.fix_circular tag

    module Term =
      struct
        let tags_to_enum l =
          L.map (fun tag -> (tag_to_string tag, tag)) l

        let infer_custom =
          Fit.Term.infer_custom
            (tags_to_enum infer_tags)
            (with_infer_tag `Custom)

        let infer_adapt =
          Fit.Term.infer_adapt
            (tags_to_enum infer_tags)
            (with_infer_tag `Adapt)

        let infer_prior =
          Fit.Term.infer_prior
            (tags_to_enum infer_tags)
            (with_infer_tag `Prior)

        let fix =
          Fit.Term.fix
            (tags_to_enum infer_tags)
            with_fix_tag
      end

    let term =
      let f infr_cus infr_adp infr_prr fix =
        (new t)
        |> infr_cus
        |> infr_adp
        |> infr_prr
        |> fix
      in Cmdliner.Term.(
        const f
        $ Term.infer_custom
        $ Term.infer_adapt
        $ Term.infer_prior
        $ Term.fix
      )
  end


module Mcmc = Coalfit.Generic.Make
  (Theta)
  (Hyper)
  (Eco)
  (Evo)
  (Infer)


let info =
  let doc =
    "Estimate the genealogy of sequence samples and the parameters
     under the seasonal SEIRS model with internal PRM augmentation
     and Robust Adaptive Metropolis MCMC."
  in
  Cmdliner.Term.info
    "epicoalfit-seirs"
    ~version:"%%VERSION%%"
    ~doc
    ~exits:Cmdliner.Term.default_exits
    ~man:[]

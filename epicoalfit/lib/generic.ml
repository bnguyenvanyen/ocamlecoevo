(** MCMC for S(E)IR(S) models, with case count and sequence data,
 *
 *  With JC69 model, ODE forward simulations,
 *  and Coal.Simple backward simulations.
 *)


open Sig


module Trait =
  struct
    (* a lineage can only be a E or a I *)
    type t =
      | E
      | I

    let equal = (=)
    let compare = compare
    let to_string =
      function
      | E ->
          "E"
      | I ->
          "I"
    let of_string =
      function
      | "E" ->
          Some E
      | "I" ->
          Some I
      | _ ->
          None
  end


module Coal = Coal.Generic.Make (Trait)


module Data = Coalfit.Generic.Data.Make (Trait)


module Theta =
  struct
    class t = object
      (* ecological forward parameters *)
      inherit Epi.Param.t_unit
      inherit Epifit.Param.init_sepir
     
     (* backward eco and evo parameters *)
      val prm_internal_v : Coal.Prm.t option = None
      val mu_v = 1e-4

      method prm_internal = U.Option.some prm_internal_v
      method mu = mu_v

      method with_prm_internal nu = {< prm_internal_v = Some nu >}
      method with_mu x = {< mu_v = x >}
    end

    let load_from =
      let doc =
        "Load theta parameter values from $(docv)."
      in
      Cmdliner.Arg.(
        value
        & opt (some string) None
        & info ["load-theta-from"] ~docv:"LOAD-THETA-FROM" ~doc
      )

    let mu =
      let doc =
        "Mutation rate"
      in
      let get th = th#mu in
      let set x th = th#with_mu x in
      U.Arg.Update.Capture.opt_config get set Cmdliner.Arg.float ~doc "mu"

    module Make (Model : MODEL) =
      struct
        type nonrec t = t

        let default = new t

        let extract (th : t) =
          function
          | "mu" ->
              string_of_float th#mu
          | s ->
              Model.extract_ode th s

        let term =
          (* FIXME other terms from epi *)
          let f mu config capture =
            (new t)
            |> U.Arg.Capture.empty
            |> mu ~config
            |> U.Arg.Capture.output capture
          in Cmdliner.Term.(
            const f
            $ mu
            $ load_from
          )
      end
  end


module type THETA = (Coalfit.Generic.THETA with type t = Theta.t)


module Hyper =
  struct
    class context = object
      val t0_v = 0.
      val tf_v = 1.
      val dt_v = 0.02

      method t0 = t0_v
      method tf = tf_v
      method dt = dt_v

      method with_t0 x =
        {< t0_v = x >}

      method with_tf x =
        {< tf_v = x >}

      method with_dt x =
        {< dt_v = x >}
    end

    class prior_evo = object
      val mu_mean_v = 1e-4
      val mu_var_v = 1e-2

      method mu_mean = mu_mean_v
      method mu_var = mu_var_v

      method with_mu_mean x =
        {< mu_mean_v = x >}

      method with_mu_var x =
        {< mu_var_v = x >}
    end

    class prop_evo = object
      val mu_jump_v = 1e-6

      method mu_jump = mu_jump_v

      method with_mu_jump x =
        {< mu_jump_v = x >}
    end

    class prm_able = object
      val prm_regraft_rng_v : U.rng option = None
      val prm_mutations_drift_coeff_v = 1e-2
      val prm_mutations_volatility_v = 1e-2
      val prm_merge_jump_v = 1e-4

      method prm_regraft_rng =
        U.Option.some prm_regraft_rng_v

      method prm_mutations_drift_coeff =
        prm_mutations_drift_coeff_v

      method prm_mutations_volatility =
        prm_mutations_volatility_v

      method prm_merge_jump =
        prm_merge_jump_v

      method with_prm_regraft_rng rng =
        {< prm_regraft_rng_v = Some rng >}

      method with_prm_mutations_drift_coeff x =
        {< prm_mutations_drift_coeff_v = x >}

      method with_prm_mutations_volatility x =
        {< prm_mutations_volatility_v = x >}

      method with_prm_merge_jump x =
        {< prm_merge_jump_v = x >}
    end

    let t0 hy =
      hy#t0

    let tf hy =
      hy#tf

    let dt hy =
      F.Pos.of_float hy#dt

    let prm_regraft_rng hy =
      hy#prm_regraft_rng

    let with_prm_regraft_rng hy rng =
      hy#with_prm_regraft_rng rng

    let prm_mutations_drift_coeff hy =
      F.Pos.of_float hy#prm_mutations_drift_coeff

    let prm_mutations_volatility hy =
      F.Pos.of_float hy#prm_mutations_volatility

    let prm_merge_jump hy =
      F.Pos.of_float hy#prm_merge_jump

    let mu_var hy =
      F.Pos.of_float hy#mu_var

    let mu_jump hy =
      F.Pos.of_float hy#mu_jump

    module Term =
      struct
        let float_arg = Epifit.Term.float_arg

        (* context *)

        let t0 () =
          let doc =
            "Start the simulations from time $(docv).
             $(docv) should be smaller than the first sampling time."
          in
          let get hy = hy#t0 in
          let set x hy = hy#with_t0 x in
          float_arg ~doc ~get ~set "t0"

        let tf () =
          let doc =
            "End the simulations at time $(docv).
             $(docv) should be larger than the last sampling time."
          in
          let get hy = hy#tf in
          let set x hy = hy#with_tf x in
          float_arg ~doc ~get ~set "tf"

        let dt () =
          let doc =
            "Record the state every $(docv) in forward simulations."
          in
          let get hy = hy#dt in
          let set x hy = hy#with_dt x in
          float_arg ~doc ~get ~set "dt"

        let context () =
          let f t0 tf dt ~config hy =
            hy
            |> t0 ~config
            |> tf ~config
            |> dt ~config
          in
          Cmdliner.Term.(
            const f
            $ t0 ()
            $ tf ()
            $ dt ()
          )

        (* prior evo *)

        let mu_mean () =
          let doc =
            "Mean of the mutation rate mu prior."
          in
          let get hy = hy#mu_mean in
          let set x hy = hy#with_mu_mean x in
          float_arg ~doc ~get ~set "mu-mean"

        let mu_var () =
          let doc =
            "Variance of the mutation rate mu prior."
          in
          let get hy = hy#mu_var in
          let set x hy = hy#with_mu_var x in
          float_arg ~doc ~get ~set "mu-var"

        (* prop evo *)

        let mu_jump () =
          let doc =
            "Standard deviation of the mutation rate mu proposal."
          in
          let get hy = hy#mu_jump in
          let set x hy = hy#with_mu_jump x in
          float_arg ~doc ~get ~set "mu-jump"

        let evo () =
          let f mu_m mu_v mu_j ~config hy =
            hy
            |> mu_m ~config
            |> mu_v ~config
            |> mu_j ~config
          in
          Cmdliner.Term.(
            const f
            $ mu_mean ()
            $ mu_var ()
            $ mu_jump ()
          )

        (* prm able *)

        let prm_mutations_drift_coeff () =
          let doc =
            "Drift coefficient for the Ornstein-Uhlenbeck proposal
             for internal mutation times."
          in
          let get hy = hy#prm_mutations_drift_coeff in
          let set x hy = hy#with_prm_mutations_drift_coeff x in
          float_arg ~doc ~get ~set "prm-mutations-drift-coeff"

        let prm_mutations_volatility () =
          let doc =
            "Volatility for the Ornstein-Uhlenbeck proposal
             for internal mutations times."
          in
          let get hy = hy#prm_mutations_volatility in
          let set x hy = hy#with_prm_mutations_volatility x in
          float_arg ~doc ~get ~set "prm-mutations-volatility"

        let prm_merge_jump () =
          let doc =
            "Standard deviation of merge internal times proposals."
          in
          let get hy = hy#prm_merge_jump in
          let set x hy = hy#with_prm_merge_jump x in
          float_arg ~doc ~get ~set "prm-merge-jump"

        let prm_able () =
          let f pmdc pmv pmj ~config hy =
            hy
            |> pmdc ~config
            |> pmv ~config
            |> pmj ~config
          in
          Cmdliner.Term.(
            const f
            $ prm_mutations_drift_coeff ()
            $ prm_mutations_volatility ()
            $ prm_merge_jump ()
          )
      end

    let load_from =
      let doc =
        "Load (hyper) parameter values from $(docv)."
      in
      Cmdliner.Arg.(
        value
        & opt (some string) None
        & info ["load-par-from"] ~docv:"LOAD-PAR-FROM" ~doc
      )

    (* for now, read from '-load-par-from', and don't capture *)
    let term () =
      (* For now, no 'par-from' *)
      let f context evo prm ~config hy =
        hy
        |> context ~config
        |> evo ~config
        |> prm ~config
      in
      Cmdliner.Term.(
        const f
        $ Term.context ()
        $ Term.evo ()
        $ Term.prm_able ()
      )
  end


module Eco =
  struct
    module Make (Model : MODEL) (Theta : THETA) (Hyper : HYPER) =
      struct
        type theta = Theta.t
        type trait = Trait.t
        type hyper = Hyper.t
        type data = Data.t

        module Data = Data

        module Forward =
          struct
            module Fw = Epi.Process.Continuous.Make (Model)
            module Episim = Epifit.Episim.Make (Model)
            
            type t = Traj.t

            let sim (hy : hyper) (th : theta) =
              let t0 = Hyper.t0 hy in
              let tf = Hyper.tf hy in
              let dt = Hyper.dt hy in
              (* simulate from t0 to tf with the right initialization,
               * and parameter values.
               * TODO we might want to pass parameters to Lsoda *)
              let par = (th :> Epi.Param.t_unit) in
              let z0 = Episim.to_pop Model.state_settings Continuous th in
              let output =
                Sim.Cadlag.convert ~dt ~conv:(Fw.conv par)
              in
              let duration = F.Pos.of_float (tf -. t0) in
              let epi_traj = Fw.Ode.sim par z0 ~output duration in
              Traj.create ~t0 ~tf epi_traj

            let output chan =
              let columns = "k" :: Epi.Traj.columns in
              Csv.output_record chan columns ;
              (fun k traj ->
                let f (t, pt) =
                  function
                  | "k" ->
                      string_of_int k
                  | s ->
                      Epi.Traj.extract (F.of_float t, pt) s
                in
                let traj' = Jump.Regular.to_list traj.Traj.forward in
                U.Csv.write ~f:(U.Csv.Row.unfold ~columns f) ~chan traj'
              )
          end

        module Coal =
          struct
            module Trait = Trait
            module Pop = Coal.Pop
            module Evm = Coal.Evm
            module Color = Coal.Color
            module Colormap = Evm.Colormap
            module Prm = Coal.Prm
            module Point = Prm.Point

            module Rates = Traj.Rates

            type color = Color.t
            type point = Point.t
            type prm = Prm.t
            type tree = Pop.tree
            type t = Pop.t

            let rand_choose_int ~rng intset =
              U.rand_unif_choose_set
                (module Intset : BatSet.S with type elt = int
                                           and type t = Intset.t)
                ~rng
                intset

            let prm (th : theta) =
              Prm.copy th#prm_internal

            let with_prm th (x : prm) =
              th#with_prm_internal x

            let forest (z : t) =
              Pop.to_forest z

            let infectivity th t =
              Epi.Process.infectivity Model.spec th t

            let rate_infect th t pt =
              (* FIXME check t *)
              Rates.infection_of infectivity th t pt

            (* Rate of mutation I -> E in the coalescent
             * Total rate of E -> I in Forward : sigma * E
             * (E - 1) because cadlag and look backward in time
             * We observe this on our focal lineage with probability 1 / I *)
            let rate_leave_exposed th traj =
              let comp =
                function
                | Trait.E ->
                    `Constant 0.
                | Trait.I ->
                    `Traj (Jump.Regular.map (fun t pt ->
                      let pt_before = Epi.Traj.{
                        pt with e = F.Pos.of_anyfloat F.Op.(pt.e - F.one)
                      }
                      in
                      let fr =
                        Rates.leave_exposed th (F.of_float t) pt_before
                      in
                      let r = F.Pos.Op.(fr / pt.i) in
                      F.to_float r
                    ) traj)
              in comp

            (* TODO Differentiate between infections from inside
             * and from outside with a lineage immigration event *)

            (* Rate of mutation E -> I in the coalescent,
             * due to an unobserved (latent) infection
             * Total rate of infection S + I -> E + I in Forward
             * depending on Model.spec.circular (but like:)
             * beta(t) S/N (I + eta)
             * given by rate_infect
             * (I take (E - 1) and (S + 1),
             * because backward on cadlag ('I' stays constant))
             * We observe on the focal lineage with probability
             * 1 / E * (I - A_I) / I *)
            let rate_unobserved_latent_infection th traj =
              let comp =
                function
                | Trait.I ->
                    `Constant 0.
                | Trait.E ->
                    `Traj (Jump.Regular.map (fun t pt ->
                        let pt_before = Epi.Traj.{
                          pt with e = F.Pos.of_anyfloat F.Op.(pt.e - F.one) ;
                                  s = F.Pos.of_anyfloat F.Op.(pt.s + F.one) ;
                        }
                        in
                        let ri =
                          rate_infect th (F.of_float t) pt_before
                        in
                        (* number of i lineages *)
                        (* if [a_i << i], we can ignore this correction.
                         * But it can actually happen that [ai > i],
                         * then the rate of unobserved merge is zero.
                         * FIXME what to do for rate_merge when [ai > i] ?
                         *)
                        (* FIXME ai needs z *)
                        (* let ai = I.to_float (Pop.count Trait.I z) in *)
                        let ai = F.zero in
                        let correction =
                          F.positive_part F.Op.((pt.i - ai) / pt.i)
                        in
                        let r = F.Pos.Op.(ri / pt.e * correction) in
                        F.to_float r
                    ) traj)
              in comp

            (* Rate of merge E + I -> I on observed (latent) infection.
             * Total rate given by rate_infect.
             * Observed on a pair of lineages with probability
             * 1 / E * 1 / I
             *)
            let rate_merge_observed_latent_infection th traj =
              let comp x x' =
                match x, x' with
                | Trait.I, Trait.I
                | Trait.E, Trait.E ->
                    `Constant 0.
                | Trait.I, Trait.E
                | Trait.E, Trait.I ->
                    `Traj (Jump.Regular.map (fun t pt ->
                      let pt_before = Epi.Traj.{
                        pt with e = F.Pos.of_anyfloat F.Op.(pt.e - F.one) ;
                                s = F.Pos.Op.(pt.s + F.one) ;
                      }
                      in
                      let ri =
                        rate_infect th (F.of_float t) pt_before
                      in
                      let r = F.Pos.Op.(ri / (pt.e * pt.i)) in
                      F.to_float r
                    ) traj)
              in comp

            (* Rate of merge I + I -> I on observed infection
             * (no latency)
             * Total rate given by rate_infect
             * Then probability of observation on a pair of focal lineages
             * 1 / I * 1 / (I - 1)
             *)
            let rate_merge_observed_direct_infection th traj =
              let comp x x' =
                match x, x' with
                | Trait.E, Trait.E
                | Trait.I, Trait.E
                | Trait.E, Trait.I ->
                    assert false
                | Trait.I, Trait.I ->
                    `Traj (Jump.Regular.map (fun t pt ->
                        let pt_before = Epi.Traj.{
                          pt with i = F.Pos.of_anyfloat F.Op.(pt.i - F.one) ;
                                  s = F.Pos.Op.(pt.s + F.one) ;
                        }
                        in
                        let ri =
                          rate_infect th (F.of_float t) pt_before
                        in
                        (* for (I - 1), we take the positive part
                         * so that the rate is 0 if I <= 1 *)
                        let im1 = F.positive_part F.Op.(pt.i - F.one) in
                        let r = F.Pos.Op.(ri / (pt.i * im1)) in
                        F.to_float r
                    ) traj)
              in comp

            let mutations_latent = [
              (Trait.I, Trait.E, rate_leave_exposed) ;
              (Trait.E, Trait.I, rate_unobserved_latent_infection) ;
            ]

            let mutations =
              if Model.spec.latency then
                mutations_latent
              else
                []

            let merges =
              if Model.spec.latency then
                [
                  (
                    Trait.I,
                    Trait.E,
                    Trait.I,
                    rate_merge_observed_latent_infection
                  ) ;
                ]
              else
                [
                  (
                    Trait.I,
                    Trait.I,
                    Trait.I,
                    rate_merge_observed_direct_infection
                  ) ;
                ]

            let rand_prm ~rng ~samples hy th (traj : Forward.t) =
              (* FIXME need better alloc_fuel for mutations *)
              let tf = F.Pos.of_float (Hyper.tf hy) in
              let mutations = L.map (fun (src, dst, rate) ->
                  (src, dst, rate th traj.forward)
                ) mutations
              in
              let merges = L.map (fun (src, src', dst, rate) ->
                  (src, src', dst, rate th traj.forward)
                ) merges
              in
              Coal.Backward.rand_prm
                ~rng
                ~mutations
                ~merges
                ~samples
                ~tf

            let logd_prm nu = Prm.logd_prm nu
          
            let color_merge =
              if Model.spec.latency then
                Color.merge Trait.E Trait.I Trait.I
              else
                Color.merge Trait.I Trait.I Trait.I

            let rand_merge_color ~rng nm1 =
              let j = I.Pos.succ (U.rand_int ~rng nm1) in
              let i = U.rand_int ~rng j in
              let is = Intset.singleton (I.to_int i + 1) in
              let js = Intset.singleton (I.to_int j + 1) in
              color_merge is js

            let rand_mutation_color_latency ~rng nm1 =
              let i = I.Pos.succ (U.rand_int ~rng nm1) in
              let ip1 = I.to_int i + 1 in
              let is = Intset.singleton ip1 in
              if U.rand_bool ~rng then
                Color.mutation Trait.I Trait.E is
              else
                Color.mutation Trait.E Trait.I is

            let rand_mutation_color =
              if Model.spec.latency then
                rand_mutation_color_latency
              else
                (fun ~rng _ -> ignore rng ; assert false)

            let update_merge_point color dx nu =
              let nu' = Prm.copy nu in
              Prm.update_merge_point color dx nu'

            let update_merge_points k k' dx nu =
              let color =
                color_merge (Intset.singleton k) (Intset.singleton k')
              in
              Prm.update_merge_point color dx nu

            let update_mutation_points color f nu =
              let nu' = Prm.copy nu in
              Prm.update_mutation_points color f nu'

            let rand_unmerged_color ~rng =
              function
              | (Color.Sample _ | Color.Merge _) as c ->
                  invalid_arg (
                    Printf.sprintf "not mutation color %s" (Color.to_string c)
                  )
              | Color.Mutation (x, y, ks) ->
                  let k = rand_choose_int ~rng ks in
                  Color.mutation x y (Intset.singleton k)

            let regraft_from ~rng grafted nu =
              (* for each mutation color of grafted,
               * for each point, add the point to nu *)
              Colormap.fold (fun c pts ->
                Prm.Points.fold (fun pt ->
                  (* FIXME reproducibility *)
                  let color = rand_unmerged_color ~rng c in
                  Prm.add_graftable_point color pt
                ) pts
              ) grafted nu

            (* TODO Or write two versions depending on latency ?
             * To avoid mutations and grafted *)
            let sim data (hy : hyper) (th : theta) (traj : Forward.t) =
              (* as a (faster) function *)
              (* let etraj = Traj.Backward.eval traj in *)
              let t0 = F.Pos.of_float (Hyper.t0 hy) in
              let tf = F.Pos.of_float (Hyper.tf hy) in
              let samples = Data.sorted data in
              let mutations = L.map (fun (src, dst, rate) ->
                  (src, dst, rate th traj.forward)
                ) mutations
              in
              let merges = L.map (fun (src, src', dst, rate) ->
                  (src, src', dst, rate th traj.forward)
                ) merges
              in
              let nu = th#prm_internal in
              (* runs on a copy of nu *)
              let zf, nuf =
                Coal.Backward.until
                  ~t0
                  ~tf
                  ~mutations
                  ~merges
                  ~samples
                  nu
              in
              (* get the newly grafted points during simulation,
               * and add them to the original nu *)
              let grafted = Prm.grafted nuf in
              (* need a rng to draw decide the lineages
               * that are given the mutations *)
              let rng = Hyper.prm_regraft_rng hy in
              let nu' = regraft_from ~rng grafted nu in
              (* then what do we do with it ? *)
              zf, th#with_prm_internal nu'

            let output data chan =
              let n = Data.length data in
              Csv.output_record chan (Pop.header_all_mrca n) ;
              (fun k z ->
                let mrcas = Pop.time_mrcas n z in
                Pop.out_all_mrca chan k mrcas
              )

            let output_prm chan =
              Csv.output_record chan ["k" ; "color" ; "t"] ;
              (fun k nu ->
                Prm.iter_points (fun c pt ->
                  Csv.output_record chan [
                    string_of_int k ;
                    Color.to_string c ;
                    F.to_string (Point.time pt) ;
                  ]
                ) nu
              )
          end
      end
  end


module Evo =
  struct
    type trait = Trait.t
    type tree = Coal.Pop.tree

    module Lik = Coalfit.Likelihood.Make (Coal.Pop.S) (Coal.Pop.T)

    module Make (Theta : THETA) =
      struct
        type nonrec trait = trait
        type nonrec tree = tree
        type theta = Theta.t

        let loglik kseqs invs =
          Lik.loglik kseqs invs
      end
  end


module Infer =
  struct
    type prm = Coal.Prm.t

    type tag = [
      | `Prm
      | `Mu
    ]

    type tag_mut = [
      | `Prm_mutations
    ]

    class prm_able = object
      (* coal stuff *)
      val prm_internal_v = Fit.Infer.adapt

      method prm_internal = prm_internal_v

      method with_prm_internal inf = {< prm_internal_v = inf >}
    end

    class evo_able = object
      (* evo stuff *)
      val mu_v = Fit.Infer.adapt

      method mu = mu_v

      method with_mu inf = {< mu_v = inf >}
    end

    let prm infr =
      infr#prm_internal

    let prm_mutations_tag = `Prm_mutations

    let prm_tag = `Prm

    let mu hy infer =
      let prior =
        let m = log hy#mu_mean /. log 10. in
        Fit.Dist.Lognormal (m, Hyper.mu_var hy)
      in
      let proposal =
        Fit.Propose.endo_dist (fun x -> Fit.Dist.Normal (x, Hyper.mu_jump hy))
      in
      Fit.Infer.maybe_adaptive_float
        ~tag:`Mu
        ~get:(fun th -> th#mu)
        ~set:(fun th x -> th#with_mu x)
        ~prior
        ~proposal
        ~jump:hy#mu_jump
        infer

    let tag_to_string =
      function
      | `Mu ->
          "mu"
      | `Prm ->
          "prm"
      | Epifit.Tag.(
          #base
        | #latent
        | #circular
        ) as tag ->
          Epifit.Seirs.Fit.tag_to_string tag

    let tag_to_columns =
      function
      | `Mu ->
          ["mu"]
      | `Prm | `Prm_mutations ->
          (* no columns for prm *)
          []
      | Epifit.Tag.(
          #base
        | #latent
        | #circular
        ) as tag ->
          Epifit.Seirs.Fit.tag_to_columns tag

    let infer_tags =
       [`Mu ; `Prm]

    let with_infer_tag is_adapt tag infer =
      match tag with
      | `Mu ->
          infer#with_mu (Fit.Infer.free is_adapt)
      | `Prm ->
          infer#with_prm_internal (Fit.Infer.free is_adapt)

    let with_fix_tag =
      function
      | `Mu ->
          (fun infr -> infr#with_mu Fit.Infer.fixed)
      | `Prm ->
          (fun infr -> infr#with_prm_internal Fit.Infer.fixed)


    let tags_to_enum l =
      L.map (fun tag -> (tag_to_string tag, tag)) l
  end


module type INFER = (
  Coalfit.Generic.INFER with type theta = Theta.t
                         and type prm = Coal.Prm.t
)


module Make
  (Model : MODEL)
  (Hyper : HYPER)
  (Infer : INFER with type hyper = Hyper.t) =
  struct
    module Theta = Theta.Make (Model)

    module Eco = Eco.Make (Model) (Theta) (Hyper)

    module Evo = Evo.Make (Theta)

    include Coalfit.Generic.Make (Theta) (Hyper) (Eco) (Evo) (Infer)
  end


let info_doc name = Printf.sprintf
  "Estimate the genealogy of sequence samples and the parameters
   under the %s model with internal PRM augmentation
   and Robust Adaptive Metropolis MCMC."
   name

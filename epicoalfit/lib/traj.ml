open Sig


module Point = Epi.Traj.Point

module Rates = Epi.Rates.Make (Point)


type t = {
  t0 : float ;
  tf : float ;
  forward : Point.t Jump.Regular.t ;
}


let create ~t0 ~tf epi_traj =
  let forward =
    epi_traj
    |> Epi.Traj.translate_out (F.of_float t0)
    |> L.map (fun (t, pt) -> (F.to_float t, pt))
    |> Jump.of_list
    |> Jump.Regular.of_cadlag
  in { t0 ; tf ; forward }


let unforward t0 _tf t =
  t0 +. F.to_float t


let unbackward _t0 tf t =
  tf -. F.to_float t


module Forward =
  struct
    (* for [t0 <= t <= tf] this is the forward time from [0] to [tf - t0] *)
    let time t0 _tf t =
      F.Pos.of_float (t -. t0)

    let of_backward t0 tf t =
      F.Pos.of_float (tf -. t0 -. F.to_float t)
  end


module Backward =
  struct
    (* for [t0 <= t <= tf] this is the backward time from [tf - t0] to [0] *)
    let time _t0 tf t =
      F.Pos.of_float (tf -. t)
  end

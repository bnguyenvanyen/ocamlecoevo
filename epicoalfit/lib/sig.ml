module L = BatList

module U = Util
module I = U.Int
module F = U.Float

module Intset = BatSet.Int


module type MODEL =
  sig
    val spec : Epi.Sig.model_spec

    type 'a par = 'a
      constraint 'a = #Epifit.Param.init_sepir
      constraint 'a = #Epi.Param.t_unit

    type state

    val state_settings : state Epifit.Sig.state_settings

    val extract_ode : _ par -> string -> string
  end


module type HYPER =
  sig
    include Coalfit.Generic.HYPER

    val prm_regraft_rng : t -> U.rng
    val t0 : t -> float
    val tf : t -> float
    val dt : t -> _ U.pos
  end

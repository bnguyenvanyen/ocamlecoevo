open Sig


module Model =
  struct
    let spec = Epi.Sir.Model.spec

    type 'a par = 'a
      constraint 'a = #Epifit.Param.init_sepir
      constraint 'a = #Epi.Param.t_unit

    type state = Epifit.Sir.Fit.state

    let state_settings = Epifit.Sir.Fit.state_settings

    let extract_ode th =
      Epifit.Sir.Fit.extract th
  end


module Hyper =
  struct
    include Generic.Hyper

    class t = object
      inherit context
      inherit prior_evo
      inherit prop_evo
      inherit prm_able
      inherit Epifit.Sir.hyper_prior
      inherit Epifit.Sir.hyper_prop
    end

    let term =
      let f epi_base generic load_theta config capture =
        (new t)
        |> U.Arg.Capture.empty
        |> epi_base load_theta ~config
        |> generic ~config
        |> U.Arg.Capture.output capture
      in Cmdliner.Term.(
        const f
        $ Epifit.Term.Hyper.base ()
        $ term ()
        $ Generic.Theta.load_from
        $ load_from
      )
  end


module Infer =
  struct
    type tag = [
      | Generic.Infer.tag
      | `Prm_mutations  (* should never be used *)
      | Epifit.Tag.base
    ]

    type theta = Generic.Theta.t
    type hyper = Hyper.t
    type prm = Generic.Infer.prm

    class t = object
      inherit Generic.Infer.prm_able
      inherit Generic.Infer.evo_able
      inherit Epifit.Sir.infer_pars_ode
    end

    let prm =
      Generic.Infer.prm

    let prm_mutations_tag = Generic.Infer.prm_mutations_tag
    let prm_tag = Generic.Infer.prm_tag

    let propose_prm_mutations = false

    let tag_to_string =
      Generic.Infer.tag_to_string

    let tag_to_columns =
      Generic.Infer.tag_to_columns

    let params (hy : hyper) (infr : t) =
      let pars =
        Epifit.Sir.Fit.params (infr :> Epifit.Sir.infer_pars_ode) hy
      in
      Coalfit.Generic.El (Generic.Infer.mu hy infr#mu :: pars)

    let infer_tags =
        Generic.Infer.infer_tags
      @ Epifit.Symbols.base

    let with_infer_tag is_adapt tag infer =
      match tag with
      | #Generic.Infer.tag as tag ->
          Generic.Infer.with_infer_tag is_adapt tag infer
      | #Epifit.Tag.base as tag ->
          Epifit.Infer.infer_base is_adapt tag infer

    let with_fix_tag =
      function
      | #Generic.Infer.tag as tag ->
          Generic.Infer.with_fix_tag tag
      | #Epifit.Tag.base as tag ->
          Epifit.Infer.fix_base tag

    module Term =
      struct
        let tte = Generic.Infer.tags_to_enum

        let infer_custom =
          Fit.Term.infer_custom
            (tte infer_tags)
            (with_infer_tag `Custom)

        let infer_adapt =
          Fit.Term.infer_adapt
            (tte infer_tags)
            (with_infer_tag `Adapt)

        let infer_prior =
          Fit.Term.infer_prior
            (tte infer_tags)
            (with_infer_tag `Prior)

          let fix =
            Fit.Term.fix
              (tte infer_tags)
              with_fix_tag
      end


    let term =
      let f infr_cus infr_adp infr_prr fix =
        (new t)
        |> infr_cus
        |> infr_adp
        |> infr_prr
        |> fix
      in Cmdliner.Term.(
        const f
        $ Term.infer_custom
        $ Term.infer_adapt
        $ Term.infer_prior
        $ Term.fix
      )
  end


module Mcmc = Generic.Make (Model) (Hyper) (Infer)


let info =
  let doc = Generic.info_doc "SIR" in
  Cmdliner.Term.info
    "epicoalfit-sir"
    ~version:"%%VERSION%%"
    ~doc
    ~exits:Cmdliner.Term.default_exits
    ~man:[]

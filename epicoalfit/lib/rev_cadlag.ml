module L = BatList


type 'a t = (float * 'a) list

let cons (t1, x1) c =
  match c with
  | [] ->
      (t1, x1) :: []
  | (t2, _) :: _ ->
      assert (t2 < t1) ;
      (t1, x1) :: c


let empty = []


let of_list l = L.fold_right cons l []


let to_list c = c


(* we already know that the times are in increasing order of time *)
let of_cadlag c = L.rev (Jump.to_list c)


let cut_left t c =
  let rec f c =
    match c with
    | [] ->
        []
    | (t', _) :: _ when t >= t' ->
        c
    | (_, _) :: c' (* when t' > t -- exhaustivity *) ->
        f c'
  in f c


let eval_cut_left t c =
  let cc = cut_left t c in
  match cc with
  | [] ->
      invalid_arg "not defined at t"
  | (_, x) :: _ ->
      x, cc


let eval c t =
  let x, _ = eval_cut_left t c in
  x

(** See Jump library *)

type 'a t

val cons : (float * 'a) -> 'a t -> 'a t

val empty : 'a t

val of_list : (float * 'a) list -> 'a t

val to_list : 'a t -> (float * 'a) list

val of_cadlag : 'a Jump.cadlag -> 'a t

val cut_left : float -> 'a t -> 'a t

val eval : 'a t -> float -> 'a

val eval_cut_left : float -> 'a t -> 'a * 'a t

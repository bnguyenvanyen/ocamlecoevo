open Epicoalfit.Sir


let () =
  Printexc.record_backtrace true ;
  Cmdliner.Term.exit @@ Cmdliner.Term.eval (Mcmc.Term.estimate_posterior, info)

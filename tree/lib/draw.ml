open Sig

module B = Base

type rgb = draw_rgb

type point = draw_point


let black = (0., 0., 0.)


module Point : 
  sig
    include STATE with type t = point
    include TIMED with type t := t
  end = 
  struct
    type t = point
    let to_time pt =
      pt.y
    let at_time y pt =
      { pt with y = y }
    let to_string pt =
      let r, g, b = pt.c in
      Printf.sprintf "[%.3f|%.1f;%.1f;%.1f]:%.3f" pt.x r g b pt.y
    let of_string s =
      match Scanf.sscanf s "[%.3f|%.1f;%.1f;%.1f]:%.3f" (fun x r g b y ->
        (x, r, g, b, y)) with
      | (x, r, g, b, y) ->
          Some { x ; y ; c = (r, g, b) }
      | exception Scanf.Scan_failure _ ->
          None
  end


let xmap f = B.map (fun pt -> { pt with x = f pt.x })


let init_fig fname w h =
  let surface = Cairo.SVG.create fname ~w ~h in
  let cr = Cairo.create surface in
  Cairo.set_line_width cr 0.1 ;
  Cairo.set_line_join cr Cairo.JOIN_ROUND ;
  (surface, cr)


let get_rgb cr =
  let (r, g, b, _) = Cairo.Pattern.get_rgba (Cairo.get_source cr) in
  (r, g, b)


let plot_line cr (pt : Point.t) (npt : Point.t) =
  let r, g, b = npt.c in
  if get_rgb cr <> (r, g, b) then
    (Cairo.stroke cr ;
     Cairo.set_source_rgb cr r g b) ;
  Cairo.move_to cr pt.x pt.y ;
  Cairo.line_to cr npt.x npt.y


(* alternative = get all segments, group by color, then plot each group *)
let plot cr gt =
  let rec f pt gt =
    match gt with
    | B.Leaf npt ->
        plot_line cr pt npt
    | B.Node (npt, sgt) ->
        plot_line cr pt npt ;
        f npt sgt
    | B.Binode (npt, lgt, rgt) ->
        plot_line cr pt npt ;
        let nlpt = { (B.root_value lgt) with y = npt.y } in
        plot_line cr npt nlpt ;
        let nrpt = { (B.root_value rgt) with y = npt.y } in
        plot_line cr npt nrpt ;
        f nlpt lgt ;
        f nrpt rgt
  in
  match gt with
  | B.Node (z0, sgt) ->
      (f z0 sgt ; Cairo.stroke cr)
  | (B.Leaf _ | B.Binode _) ->
      failwith "Unrooted tree"


let rec contour_left margin c t =
  match t with
  | B.Leaf pt ->
      Jump.cut_left (pt.y +. margin) c
  | B.Node (pt, st) ->
      Jump.cons (pt.y, pt.x) (contour_left margin c st)
  | B.Binode (pt, lt, rt) ->
      let ic = contour_left margin c rt in
      let rc = Jump.cons (pt.y, (B.root_value rt).x) ic in
      let lc = contour_left margin rc lt in
      Jump.cons (pt.y, (B.root_value lt).x) lc


let rec contour_right margin c t =
  match t with
  | B.Leaf pt ->
      Jump.cut_left (pt.y +. margin) c
  | B.Node (pt, st) ->
      Jump.cons (pt.y, pt.x) (contour_right margin c st)
  | B.Binode (pt, lt, rt) ->
      let ic = contour_right margin c lt in
      let lc = Jump.cons (pt.y, (B.root_value lt).x) ic in
      let rc = contour_right margin lc rt in
      Jump.cons (pt.y, (B.root_value rt).x) rc


module type S =
  sig
    include Timed.S
    include PLOT_TREE with type t := t and type state := state
  end


module Complement (T : Timed.S) :
  (S with type t = T.t and type state = T.state) =
  struct
    include T

    let offset_tree color ttoy width margin tree =
      let min_step = 1. in
      let to_offs_pt offs z =
        { x = offs ; y = ttoy (T.State.to_time z) ; c = color z }
      in  
      let rec f offs tree =
        let leaf z =
          B.leaf (to_offs_pt offs z)
        in
        let node z st =
          B.node (to_offs_pt offs z) (f offs st)
        in
        let binode z lt rt =
          let pt = to_offs_pt 0. z in
          let olt = f 0. lt in
          let ort = f 0. rt in
          let ilc = Jump.cons
            (pt.y, pt.x)
            (contour_right min_step Jump.empty olt)
          in
          let irc = Jump.cons
            (pt.y, pt.x)
            (contour_left min_step Jump.empty ort)
          in
          (* problem if empty *)
          (* min offset to separate the subtrees *)
          let min_offs = ~-. (Jump.min (Jump.Float.sub irc ilc)) in
          (* with an additional min_step margin *)
          let offs' = min_offs +. min_step in
          (* adjust the returned subtree *)
          B.binode
            pt
            (xmap (fun x -> x -. offs' /. 2.) olt)
            (xmap (fun x -> x +. offs' /. 2.) ort)
        in
        T.case ~leaf ~node ~binode tree
      in
      let ot = f 0. tree in
      let lc = contour_left min_step Jump.empty ot in
      let rc = contour_right min_step Jump.empty ot in
      let xmin = Jump.min lc in
      let xmax = Jump.max rc in
      let m = (width -. 2. *. margin) /. (xmax -. xmin) in
      let p = margin -. m *. xmin in
      xmap (fun x -> m *. x +. p) ot

    let plot cr color ttoy width margin tree =
      plot cr (offset_tree color ttoy width margin tree)
  end


module Make (S : sig include STATE include TIMED with type t := t end) :
  (S with type state = S.t) =
  struct
    module T = Timed.Make (S)

    include Complement (T)
  end

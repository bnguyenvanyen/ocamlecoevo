type t =
  | Forward of t
  | Left of t
  | Right of t
  | End of int  (* of int, to make forests *)

type split =
  | Sforward of split
  | Sleft of split
  | Sright of split
  | Split of split * split  (* FIXME can we get rid of Split ? *)
  | Send

(* concatenate paths *)
(* (hd p) @| (tl p) = p *)
let concat p1 p2 =
  let rec f p =
    match p with
    | Forward ptl -> Forward (f ptl)
    | Left ptl -> Left (f ptl)
    | Right ptl -> Right (f ptl)
    | End _ -> p2
  in f p1

let (@|) = concat

let tree_index p =
  let rec f p =
    match p with
    | (Forward ptl | Left ptl | Right ptl) -> f ptl
    | End n -> n
  in f p

let go_forward p =
  let n = tree_index p in
  concat p (Forward (End n))

let go_left p =
  let n = tree_index p in
  concat p (Left (End n))

let go_right p =
  let n = tree_index p in
  concat p (Right (End n))

let hd p =
  let n = tree_index p in
  match p with
  | Forward _ -> Forward (End n)
  | Left _ -> Left (End n)
  | Right _ -> Right (End n)
  | End n -> End n

let tl p =
  match p with
  | (Forward ptl | Left ptl | Right ptl) -> ptl
  | End n -> End n

let rec length p =
  match p with
  | (Forward ptl | Left ptl | Right ptl) -> 1 + length ptl
  | End _ -> 0

let string_of_path p =
  let rec f p =
    match p with
    | Forward ps -> Printf.sprintf "f-%s" (f ps)
    | Left pl -> Printf.sprintf "l-%s" (f pl)
    | Right pr -> Printf.sprintf "r-%s" (f pr)
    | End n -> Printf.sprintf "%i" n
  in f p

(* unused
let rec cut_end p =
  match p with
  | (Forward (End n) | Left (End n) | Right (End n)) -> End n
  | Forward tp -> Forward (cut_end tp)
  | Left tp -> Left (cut_end tp)
  | Right tp -> Right (cut_end tp)
  | End n -> End n
*)

let rev ?(i=0) p =
  let rec f acc p =
    match p with
    | Forward tp -> f (Forward acc) tp
    | Left tp -> f (Right acc) tp
    | Right tp -> f (Left acc) tp
    | End _ -> acc
  in f (End i) p

let cut_extant p =
  let rp = rev p in
  let rec f p =
    match p with
    | Forward tp -> f tp
    | (Left tp | Right tp) -> tp
    | End n -> End n
  in rev (f rp)


let rec split_of p =
  match p with
  | Forward ptl -> Sforward (split_of ptl)
  | Left ptl -> Sleft (split_of ptl)
  | Right ptl -> Sright (split_of ptl)
  | End _ -> Send

let rec inter p1 p2 =
  match p1, p2 with
  | Forward tp1, Forward tp2 ->
    Forward (inter tp1 tp2)
  | Left tp1, Left tp2 ->
    Left (inter tp1 tp2)
  | Right tp1, Right tp2 ->
    Right (inter tp1 tp2)
  | ((Forward _, (Left _ | Right _ | End _))
   | (Left _, (Forward _ | Right _ | End _))
   | (Right _, (Forward _ | Left _ | End _))
   | (End _, (Forward _ | Left _ | Right _ | End _))) ->
    End 0

(* Split pretty annoying *)
let rec union sp1 sp2 =
  match sp1, sp2 with
  | Sforward tp1, Sforward tp2 ->
    Sforward (union tp1 tp2)
  | Sleft tp1, Sleft tp2 ->
    Sleft (union tp1 tp2)
  | Sright tp1, Sright tp2 ->
    Sright (union tp1 tp2)
  | ((Sleft tp1, Sright tp2) | (Sright tp2, Sleft tp1)) ->
    Split (tp1, tp2)
  | Split (lp1, rp1), Split (lp2, rp2) ->
    Split (union lp1 lp2, union rp1 rp2)
  | ((Split (lp1, rp), Sleft lp2) | (Sleft lp1, Split (lp2, rp))) ->
    Split (union lp1 lp2, rp)
  | ((Split (lp, rp1), Sright rp2) | (Sright rp1, Split (lp, rp2))) ->
    Split (lp, union rp1 rp2)
  | Send, (Sforward _ | Sleft _ | Sright _ | Split _) ->
    sp2
  | (Sforward _ | Sleft _ | Sright _ | Split _), Send ->
    sp1
  | Send, Send ->
    Send
  | ((Sforward _, (Sleft _ | Sright _ | Split _))
   | ((Sleft _ | Sright _ | Split _), Sforward _)) ->
    Send

(* TODO test this change *)
let combine_paths p_l =
  List.fold_left (fun sp p -> union sp (split_of p)) Send p_l


module Hashed =
  struct
    type nonrec t = t
    let equal = (=)
    let rec pow a = function
      | 0 -> 1
      | 1 -> a
      | n -> 
        let b = pow a (n / 2) in
        b * b * (if n mod 2 = 0 then 1 else a)
    (* risk of overflow ? better to use BatInt.pow, or Zarith, or Hashtbl.hash ? *)
    (* FIXME doesn't work *)
    let hash p =
      let rec f i p =
        match p with
        | End n -> n * (pow 4 i)
        | Forward sp -> 1 * (pow 4 i) + (f (i + 1) sp)
        | Left lp -> 2 * (pow 4 i) + (f (i + 1) lp)
        | Right rp -> 3 * (pow 4 i) + (f (i + 1) rp)
      in f 0 p
  end


module Ordered =
  struct
    type nonrec t = t
    (*
    let compare p1 p2 =
      let n = (Hashed.hash p1) - (Hashed.hash p2) in
      if n = 0 then assert (p1 = p2) ;
      n
    *)
    let compare = compare
  end

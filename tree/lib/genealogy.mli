(** Build trees from int hash tables *)

(** Hashtbl for integers *)
module Intbl : (BatHashtbl.S with type key = int)

(** Maps ordered by increasing time *)
module Tmap : (BatMap.S with type key = float)

(** an edge in the genealogy *)
type indiv = {
  t_birth : float ;  (** time of birth *)
  t_death : float ;  (** time of death *)
  parent_id : int ;  (** id of parent *)
  children_ids : int list ;  (** ids of children, ordered by birth time *)
}

type t = indiv Intbl.t


(** [forest_root] is the index reserved for the root of the forest,
    from which we can find all trees (its children) *)
val forest_root : int


(** [root_forest gen ids] adds the forest root to [gen], linked to [ids].
    The [ids] should be given by increasing order of introduction.
    @raise Assert_failure if the root is already present. *)
val root_forest : t -> int list -> unit


(** [coalesce rng p z] is a coalescent formed by sampling individuals
    with probability [p] from [z] *)
val coalesce : Util.rng -> Util.anyproba -> t -> t


val prune : Util.rng -> Util.anyproba -> t -> unit

(** [to_tree z] forms a tree from [z] *)
val to_tree : t -> int -> Lt.t

(** [to_forestf z] forms a list of tree from [z] *)
val to_forest : t -> Lt.t list

(** [of_forest trees] is a genealogy converted from the [trees] *)
val of_forest : Lt.t list -> t

open Sig


module type S =
  sig
    type state
    type tree
    type path
    type t

    (** [place_cursor tree] is [tree] zipped open
        with the cursor placed at the root *)
    val place_cursor : tree -> t

    (** [remove_cursor zipper] is the full unzipped tree without the cursor.
        The zip is closed by moving the cursor to the top. *)
    val remove_cursor : t -> tree

    (** [subtree zipper] is the subtree under the cursor of [zipper]. *)
    val subtree : t -> tree

    (** [state zipper] is the state under the cursor in [zipper].
        @raise Failure if the cursor points to the top. *)
    val state : t -> state

    (** [parent zipper] is the parent path of the cursor in [zipper].
        @raise Failure if the cursor points to the top. *)
    val parent : t -> path

    (** [sibling zipper] is the sibling tree of the cursor in [zipper].
        @raise Failure if the cursor is at the root or an unary node. *)
    val sibling : t -> tree

    (** [direction zipper] is the direction that the cursor points to. *) 
    val direction : t -> [ `Top | `Straight | `Left | `Right ]


    (** [move_up zipper] moves the cursor up.
        @raise Failure if the cursor already points to the top *)
    val move_up : t -> t

    (** [move_side zipper] moves the cursor to the sibling tree.
        @raise Failure if the cursor points to the top or to an unary node. *)
    val move_side : t -> t

    (** [move_down_straight zipper] moves the cursor straight down.
        @raise Failure if the node under the cursor is a leaf or a binary node. *)
    val move_down_straight : t -> t

    (** [move_down_left zipper] moves the cursor down left.
        @raise Failure if the node under the cursor is a leaf or an unary node. *)
    val move_down_left : t -> t

    (** [move_down_right zipper] moves the cursor down right.
        @raise Failure if the node under the cursor is a leaf or an unary node. *)
    val move_down_right : t -> t


    (** [replace_subtree zipper tree] replaces the subtree under the cursor
        in [zipper] with [tree]. *)
    val replace_subtree : t -> tree -> t

    (** [prune zipper] removes the subtree under the cursor,
        and places the cursor on the parent.
        @raise Failure if the cursor is not under a binary node. *)
    val prune : t -> t * state * tree

    (** [graft_left x graft zipper] is [zipper] with the subtree [graft]
        added under the starting cursor on the left,
        and with the cursor moved to the new tree. *)
    val graft_left : state -> tree -> t -> t

    (** [graft_right] is {!graft_left} but on the right. *)
    val graft_right : state -> tree -> t -> t


    (** [zip tree path] is the zipper with [tree] under the cursor in [path]. *)
    val zip : tree -> path -> t

    (** [case f zipper] is the result of [f] on the deconstructed
        tree and path of [zipper]. *)
    val case : (tree -> path -> 'a) -> t -> 'a
  end


module Make :
  functor (T : BINTREE) ->
    (S with type state = T.state
        and type tree = T.t)


type ('a, 'tree) t

(** [place_cursor tree] is [tree] zipped open
    with the cursor placed at the root *)
val place_cursor : 'a Base.tree -> ('a, 'a Base.tree) t


(** [remove_cursor zipped] is the full unzipped tree without the cursor.
    The zip is closed by moving the cursor to the top. *)
val remove_cursor : ('a, 'a Base.tree) t -> 'a Base.tree


(** [subtree zipped] is the subtree under the cursor of [zipped]. *)
val subtree : ('a, 'a Base.tree) t -> 'a Base.tree


(** [replace_subtree zipped tree] replaces the subtree under the cursor
    in [zipped] with [tree]. *)
val replace_subtree :
  ('a, 'a Base.tree) t ->
  'a Base.tree ->
    ('a, 'a Base.tree) t


(** [move_up zipped] moves the cursor up.
    @raise Failure if the cursor already points to the top *)
val move_up : ('a, 'a Base.tree) t -> ('a, 'a Base.tree) t


(** [move_side zipped] moves the cursor to the sibling tree.
    @raise Failure if the cursor points to the top or to an unary node. *)
val move_side : ('a, 'a Base.tree) t -> ('a, 'a Base.tree) t


(** [move_down_straight zipped] moves the cursor straight down.
    @raise Failure if the node under the cursor is a leaf or a binary node. *)
val move_down_straight : ('a, 'a Base.tree) t -> ('a, 'a Base.tree) t


(** [move_down_left zipped] moves the cursor down left.
    @raise Failure if the node under the cursor is a leaf or an unary node. *)
val move_down_left : ('a, 'a Base.tree) t -> ('a, 'a Base.tree) t


(** [move_down_right zipped] moves the cursor down right.
    @raise Failure if the node under the cursor is a leaf or an unary node. *)
val move_down_right : ('a, 'a Base.tree) t -> ('a, 'a Base.tree) t

open Sig

module B = Base


module type INPUT =
  sig
    include STATE
    include LABELED with type t := t
  end


module type S =
  sig
    include BINTREE
    include LABEL_TREE with type t := t and type state := state

    module State :
      sig
        include STATE
        include LABELED with type t := t
      end
  end


module Make_core (State : INPUT) =
  struct
    type label = State.label

    let has_label id x' =
      let id' = State.to_label x' in
      State.Label.equal id id'

    let find_label tree id =
      Base.find_node (has_label id) tree

    let label_mrca tree id1 id2 =
      Base.mrca tree (has_label id1) (has_label id2)
  end


module Make (State : INPUT) :
  (S with type state = State.t
      and type label = State.label) =
  struct
    module T = Bintree.Make_min (State)
    include T
    include Map.Make (T)
    include Bintree.Make_core (State)
    include Make_core (State)

    module State = State

    let validate tree = tree  [@@ inline]

    include Io.Make (State) (T)
  end

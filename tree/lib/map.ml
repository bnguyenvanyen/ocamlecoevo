open Sig


module Make (T : MINTREE) =
  struct
    let map f tree =
      let rec loop t =
        T.case
          ~leaf:(fun x -> T.leaf (f x))
          ~node:(fun x st -> T.node (f x) (loop st))
          ~binode:(fun x lt rt -> T.binode (f x) (loop lt) (loop rt))
        t
      in loop tree

    (* map in CPS *)
    let map_cps f tree =
      let rec map tree treef =
        let leaf x =
          treef (T.leaf (f x))
        in
        let node x stree =
          map stree (fun mstree -> treef (T.node (f x) mstree))
        in
        let binode x ltree rtree =
          map ltree (fun mltree ->
            map rtree (fun mrtree ->
              treef (T.binode (f x) mltree mrtree)
            )
          )
        in
        T.case ~leaf ~node ~binode tree
      in
      map tree (fun mtree -> mtree)

    let map_case ~leaf ~node ~binode tree =
      let rec loop t =
        T.case
          ~leaf:(fun x -> T.leaf (leaf x))
          ~node:(fun x st -> T.node (node x) (loop st))
          ~binode:(fun x lt rt -> T.binode (binode x) (loop lt) (loop rt))
        t
      in loop tree
  end



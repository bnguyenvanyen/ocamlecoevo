(** Paths along trees can serve to identify nodes *)


(** type for paths, for most uses like list *)
type t =
  | Forward of t
  | Left of t
  | Right of t
  | End of int

(** type for split paths, like a tree *)
type split =
  | Sforward of split
  | Sleft of split
  | Sright of split
  | Split of split * split
  | Send


(** [concat p1 p2] puts [p2] at the end of [p1]. Split invalid in [p1] *)
val concat : t -> t -> t

(** operator for concat *)
val (@|) : t -> t -> t

(** {1:convenience Convenience functions for the regularly used concatenations} *)

(** [go_forward p] is [p @| (Forward End)] *)
val go_forward : t -> t

(** [go_left p] is [p @| (Left End)] *)
val go_left : t -> t

(** [go_right p] is [p @| (Right End)] *)
val go_right : t -> t

(** [hd p] is the head of the path [p] *)
val hd : t -> t

(** [tl p] is the tail of the path [p] *)
val tl : t -> t

(** [length p] is the number of moves in [p]. Split invalid. *)
val length : t -> int

(** convenience string conversion function *)
val string_of_path : t -> string

(** [rev p] is [p] reversed. Split invalid *)
val rev : ?i:int -> t -> t

(** [cut_extant p] is [p] until the last Left/Right. Split invalid. *)
val cut_extant : t -> t

(** [inter p1 p2] is the common path from the start *)
val inter : t -> t -> t

(** [union sp1 sp2] is the union (possibly Split) *)
val union : split -> split -> split

(** union of the list (fold union) *)
val combine_paths : t list -> split

(** HashedType for paths *)
module Hashed : Hashtbl.HashedType with type t = t

(** OrderedType for paths *)
module Ordered : Set.OrderedType with type t = t

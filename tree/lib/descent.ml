module P = Path
module B = Base

module Intbl = Hashtbl.Make (struct
  type t = int
  let hash n = n
  let equal = (=)
end)

module Pathtbl = Hashtbl.Make (P.Hashed)

type 'a t = {
  alive : P.t Intbl.t ;
  ancestry : (float * 'a) Pathtbl.t ;
  mutable maxtree : int ;
}

let tree_of ?(i=0) descent =
  let look_for_next_path p =
    try
      let pdir = P.Forward (P.End i) in
      let p' = P.concat p pdir in
      (pdir, p', Pathtbl.find descent p')
    with Not_found ->
      try
        let pdir = P.Left (P.End i) in
        let p' = P.concat p pdir in
        (pdir, p', Pathtbl.find descent p')
      with Not_found ->
        (P.End i, p, Pathtbl.find descent p)
  in
  let rec f x p =
    let pdir, p', x' = look_for_next_path p in
    (* problem with times / kinds in tree *)
    match pdir with
    | P.End _ ->
        B.leaf x  (* = nz *)
    | P.Forward _ ->
        B.node x (f x' p')
    | P.Left _ ->
        let pr = P.go_right p in
        let xr = Pathtbl.find descent pr in
        B.binode x (f x' p') (f xr pr)
    | P.Right _ ->
      failwith "unused case"
  in
  let x0 = Pathtbl.find descent (P.End i) in
  f x0 (P.End i)


let forest_of z =
  (* FIXME are we missing paths because of alive ? *)
  (* (I think yes) *)
  let reversed_paths pt =
    let rpt = Pathtbl.create (Pathtbl.length pt) in
    Pathtbl.iter (fun p y -> Pathtbl.add rpt (P.rev p) y) pt ;
    rpt
  in
  let descent = reversed_paths z.ancestry in
  (* FIXME on peut partir de maxtree *)
  let rec f i =
    match i with
    | 0 -> []
    | _ -> (tree_of ~i descent) :: (f (i - 1))
  in f z.maxtree


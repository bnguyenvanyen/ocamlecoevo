module U = Util


module type INPUT = Labeltimed.S


module type S =
  sig
    type tree

    val characterize : float -> tree -> tree -> float list * float list
    val distance : float -> tree -> tree -> float
  end


module Make (T : INPUT) =
  struct
    type tree = T.t

    let repeat = BatList.make

    (* FIXME should we go the last Binode, or also previous Node ? *)
    let pendant_length tree (p, x) =
      let p_int = Path.cut_extant p in
      let t_int = T.State.to_time (T.root_value (T.descend p_int tree)) in
      (T.State.to_time x) -. t_int

    let eq (_, x1) (_, x2) =
      (T.State.to_label x1) = (T.State.to_label x2)

    (* change this should use labels instead *)
    let rec assoc_leaves lv1 lv2 =
      match lv1 with
      | (p1, x1) :: tl ->
        ((p1, x1), List.find (eq (p1, x1)) lv2) :: assoc_leaves tl lv2
      | [] ->
        []

    let pair_up x l = List.combine (repeat (List.length l) x) l

    let rec pairs l =
      match l with
      | hd :: tl -> (pair_up hd tl) @ (pairs tl)
      | [] -> []

    let find_node tree x =
      let p x' =
          (T.State.Label.equal (T.State.to_label x) (T.State.to_label x'))
       && (U.nearly_equal (T.State.to_time x) (T.State.to_time x'))
      in
      T.find_node p tree
        
    (* possibly put them in a Lacaml.Vec instead *)
    let vec lbd tree pairs leaves =
      let n = List.length leaves in
      let leaves1, leaves2 = List.split pairs in
      let mrca_l = List.map2 (fun (_, x1) (_, x2) ->
          (* they should all have a mrca *)
          Util.Option.some (
            T.label_mrca tree (T.State.to_label x1) (T.State.to_label x2)
          )
        ) leaves1 leaves2
      in
      (* number of nodes from mrca to the root *)
      (* length - 1 because we ignore the "artificial" root *)
      let v1 =
        mrca_l
        |> List.map (fun x ->
            x
            |> find_node tree
            |> Util.Option.some
            |> (fun (p, _) -> Path.length p - 1)
          )
        |> (fun l -> l @ (repeat n 1))
      in
      (* time from mrca to root *)
      let t_root = T.case
        ~leaf:(fun _ -> invalid_arg "unrooted tree")
        ~binode:(fun _ _ _ -> invalid_arg "unrooted tree")
        ~node:(fun _ stree -> T.State.to_time (T.root_value stree))
        tree
      in
      let v2 = List.map (fun x -> T.State.to_time x -. t_root) mrca_l in
      let v2 = v2 @ (List.map (pendant_length tree) leaves) in
      let convx x1 x2 =
        (1. -. lbd) *. float_of_int x1 +. lbd *. x2
      in List.map2 convx v1 v2 

    let characterize lbd tree1 tree2 =
      let lv1 = T.leaves tree1 in
      let lv2 = T.leaves tree2 in
      assert (List.length lv1 = List.length lv2) ;
      let assoc = assoc_leaves lv1 lv2 in
      (* sort the leaves of tree2 in the same order (by label) as tree1 *)
      let _, lv2 = List.split assoc in
      let pairs1 = pairs lv1 in
      let pairs2 = pairs lv2 in
      let v1 = vec lbd tree1 pairs1 lv1 in
      let v2 = vec lbd tree2 pairs2 lv2 in
      (v1, v2)

    let distance lbd tree1 tree2 =
      let v1, v2 = characterize lbd tree1 tree2 in
      sqrt (List.fold_left2 (fun y x1 x2 ->
        y +. (x1 -. x2) ** 2.
      ) 0. v1 v2)
  end

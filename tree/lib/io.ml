open Sig

module B = Base


module Make (State : STATE) (T : MINTREE with type state = State.t) =
  struct
    module P = Parse.Make (State) (T)

    let read_newick chan =
      match P.tree Lex.read (Lexing.from_channel chan) with
      | Some tree ->
          tree
      | None ->
          failwith "Empty input"

    let of_newick s =
      P.tree Lex.read (Lexing.from_string s)

    let output_newick chan tree =
      B.output_newick State.to_string chan tree

    let to_newick tree =
      B.to_newick State.to_string tree
  end

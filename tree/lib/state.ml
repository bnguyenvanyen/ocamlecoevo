open Sig


module Lengthed =
  struct
    type t = float
    let length_of t = t
    let with_length t' _ = t'
    let to_string t = Printf.sprintf ":%.2f" t
    let parse s = Scanf.sscanf s ":%f" (fun t -> t)
  end


module Timed =
  struct
    type t = float
    let to_time t = t
    let at_time t' _ = t'
    let to_string t = Printf.sprintf ":%.3f" t
    let of_string s =
      match Scanf.sscanf s "%s@:%f" (fun _ t -> t) with
      | t ->
          Some t
      | exception Scanf.Scan_failure _ ->
          None
  end


module Labeled =
  struct
    type t = int
    type label = int

    module Label = Label.Int

    let to_label k = k  [@@ inline]
    let to_string = Label.to_string
    let of_string = Label.of_string
  end


module Also_timed
  (S : STATE) :
  sig
    include STATE with type t = float * S.t
    include TIMED with type t := t
  end =
  struct
    type t = float * S.t
    let to_time (t, _) = t
    let at_time t (_, x) = (t, x)
    let to_string (t, x) = Printf.sprintf "%s:%.3f" (S.to_string x) t
    let of_string s =
      match Scanf.sscanf s "%s@:%f" (fun ss t -> (ss, t)) with
      | (ss, t) ->
          Util.Option.map (fun x -> (t, x)) (S.of_string ss)
      | exception Scanf.Scan_failure _ ->
          None
  end


module Timed_also_labeled
  (S :
    sig
      include STATE
      include TIMED with type t := t
    end
  ) :
  sig
    include STATE with type t = S.t * int
    include TIMED with type t := t
    include LABELED with type t := t and type label = int
  end =
  struct
    type t = S.t * int
    type label = int

    module Label = Label.Int

    let to_time (x, _) = S.to_time x
    let at_time t (x, n) = (S.at_time t x, n)
    let to_string (x, n) =
      Printf.sprintf "%i%s" n (S.to_string x)
    let of_string s =
      match Scanf.sscanf s "%i%s" (fun n s' -> (n, s')) with
      | n, s' ->
          Util.Option.map (fun x -> (x, n)) (S.of_string s')
      | exception Scanf.Scan_failure _ ->
          None
    let to_label (_, n) = n
  end


module Labeled_also_timed
  (S :
    sig
      include STATE
      include LABELED with type t := t
    end
  ) :
  sig
    include STATE with type t = float * S.t
    include LABELED with type t := t
    include TIMED with type t := t
  end =
  struct
    include Also_timed (S)
    type label = S.label
    module Label = S.Label
    let to_label (_, x) = S.to_label x
  end

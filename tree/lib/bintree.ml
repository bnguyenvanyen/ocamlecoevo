open Sig

module B = Base


module Make_min (State : STATE) :
  (MINTREE with type state = State.t and type t = State.t tree) =
  struct
    type state = State.t
    type t = state tree
    
    let leaf = B.leaf
    let node = B.node
    let binode = B.binode

    let case = B.case
  end


module Make_core (State : STATE) =
  struct
    let basic tree = tree  [@@ inline]
    let flat = B.flat
    let leaves = B.leaves
    let map = B.map
    let left_fold = B.left_fold
    let contour_iter = B.contour_iter
    let root_value = B.root_value
    let drop_unary = B.drop_unary
    let filter_leaves = B.filter_leaves
    let find_node = B.find_node
    let descend = B.descend
    let replace_subtree = B.replace_subtree
    let genealogy = B.genealogy
    let all_paths = B.all_paths
    let mrca = B.mrca

    module M = B.Merge(Set.Make(struct
      type t = State.t
      let compare = compare
    end))
    let merge = M.merge

  end


module Make (State : STATE) : (BINTREE with type state = State.t) =
  struct
    module T = Make_min (State)
    include T
    include Map.Make (T)
    include Make_core (State)

    let validate tree = tree  [@@ inline]

    include Io.Make (State) (T)
  end

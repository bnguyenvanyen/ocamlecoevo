
%%

%public
state_tree(binode, snode, leaf, state):
  | EOF
      { None }
  | t = node(binode, snode, leaf, state) ; SEMIC
      { t }
  ;


node(binode, snode, leaf, state):
  | t = binode(node(binode, snode, leaf, state), state)
      { t }
  | t = snode(node(binode, snode, leaf, state), state)
      { t }
  | t = leaf(state)
      { t }
  ;

open Sig


module type INPUT =
  sig
    include STATE
    include TIMED with type t := t
    include LABELED with type t := t
  end


module type S =
  sig
    include BINTREE
    include TIME_TREE with type t := t and type state := state
    include HAS_LEN_TREE with type t := t and type state := state
    include LABEL_TREE with type t := t and type state := state

    module State : INPUT with type t = state and type label = label
  end


module Make (State : INPUT) :
  (S with type state = State.t and type label = State.label) =
  struct
    module State = State

    module T = Timed.Make_min (State)
    include T
    include Map.Make (T)
    include Bintree.Make_core (State)
    include Labeled.Make_core (State)
    include Timed.Make_core (State)
    include Timed.Make_io (State) (T)
    include Timed.Make_has_len (State) (T)
  end

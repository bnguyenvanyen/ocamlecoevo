(* This is an implementation of the zipper structure,
 * described by Gérard Huet (1997).
 * The code is nearly directly transposed from the article.
 *
 * This should replace the tree Path module, as it's strictly more powerful ?
 *)

open Sig


module type S =
  sig
    type state
    type tree
    type path
    type t

    val place_cursor : tree -> t
    val remove_cursor : t -> tree

    val subtree : t -> tree
    val state : t -> state
    val parent : t -> path
    val sibling : t -> tree
    val direction : t -> [ `Top | `Straight | `Left | `Right ]

    val move_up : t -> t
    val move_side : t -> t
    val move_down_straight : t -> t
    val move_down_left : t -> t
    val move_down_right : t -> t

    val replace_subtree : t -> tree -> t
    val prune : t -> t * state * tree
    val graft_left : state -> tree -> t -> t
    val graft_right : state -> tree -> t -> t

    val zip : tree -> path -> t
    val case : (tree -> path -> 'a) -> t -> 'a
  end


type ('a, 'tree) path =
  | Top
  | Straight of {
      state : 'a ;
      parent : ('a, 'tree) path ;
    }
  | Left of {
      state : 'a ;
      parent : ('a, 'tree) path ;
      sibling : 'tree ;
    }
  | Right of {
      state : 'a ;
      parent : ('a, 'tree) path ;
      sibling : 'tree ;
    }


type ('a, 'tree) t = Zip of 'tree * ('a, 'tree) path  [@@unbox]


let place_cursor tree =
  Zip (tree, Top)
 

let subtree (Zip (tree, _)) = tree

let replace_subtree (Zip (_, path)) tree =
  Zip (tree, path)


let move_up (Zip (tree, path)) =
  match path with
  | Top ->
      failwith "move up of top"
  | Straight { state ; parent } ->
      Zip (Base.node state tree, parent)
  | Left { state ; parent ; sibling } ->
      Zip (Base.binode state tree sibling, parent)
  | Right { state ; parent ; sibling } ->
      Zip (Base.binode state sibling tree, parent)


let move_side (Zip (tree, path)) =
  match path with
  | Top ->
      failwith "move side of top"
  | Straight _ ->
      failwith "move side of straight"
  | Left { state ; parent ; sibling } ->
      Zip (sibling, Right { state ; parent ; sibling = tree })
  | Right { state ; parent ; sibling } ->
      Zip (sibling, Left { state ; parent ; sibling = tree })


let move_down_straight (Zip (tree, path)) =
  match tree with
  | Base.Leaf _ ->
      failwith "move down leaf"
  | Base.Node (x, stree) ->
      Zip (stree, Straight { state = x ; parent = path })
  | Base.Binode _ ->
      failwith "move down binode straight"


let move_down_left (Zip (tree, path)) =
  match tree with
  | Base.Leaf _ ->
      failwith "move down leaf"
  | Base.Node _ ->
      failwith "move down node left"
  | Base.Binode (x, ltree, rtree) ->
      Zip (ltree, Left { state = x ; parent = path ; sibling = rtree })


let move_down_right (Zip (tree, path)) =
  match tree with
  | Base.Leaf _ ->
      failwith "move down leaf"
  | Base.Node _ ->
      failwith "move down node right"
  | Base.Binode (x, ltree, rtree) ->
      Zip (rtree, Right { state = x ; parent = path ; sibling = ltree })


let remove_cursor zipped =
  let rec f z =
    match z with
    | Zip (tree, Top) ->
        tree
    | _ ->
        f (move_up z)
  in
  f zipped


let prune (Zip (tree, path)) =
  match path with
  | Top ->
      failwith "prune top"
  | Straight _ ->
      failwith "prune straight"
  | Left { state ; parent ; sibling }
  | Right { state ; parent ; sibling } ->
      Zip (sibling, parent), state, tree


let graft_left x graft (Zip (tree, path)) =
  Zip (graft, Left { state = x ; parent = path ; sibling = tree })


let graft_right x graft (Zip (tree, path)) =
  Zip (graft, Right { state = x ; parent = path ; sibling = tree })


module Make (T : BINTREE) :
  (S with type state = T.state
      and type tree = T.t) =
  struct
    type state = T.state
    type tree = T.t
    type nonrec path = (state, tree) path
    type nonrec t = (state, tree) t

    let place_cursor = place_cursor
    let subtree = subtree

    let state (Zip (_, path)) =
      match path with
      | Top ->
          failwith "state of top"
      | Straight { state ; _ }
      | Left { state ; _ }
      | Right { state ; _ } ->
          state

    let parent (Zip (_, path)) =
      match path with
      | Top ->
          failwith "parent of top"
      | Straight { parent ; _ }
      | Left { parent ; _ }
      | Right { parent ; _ } ->
          parent

    let sibling (Zip (_, path)) =
      match path with
      | Top ->
          failwith "sibling of top"
      | Straight _ ->
          failwith "sibling of straight"
      | Left { sibling ; _ }
      | Right { sibling ; _ } ->
          sibling

    let direction (Zip (_, path)) =
      match path with
      | Top ->
          `Top
      | Straight _ ->
          `Straight
      | Left _ ->
          `Left
      | Right _ ->
          `Right

    let move_up (Zip (tree, path)) =
      match path with
      | Top ->
          failwith "move up of top"
      | Straight { state ; parent } ->
          Zip (T.node state tree, parent)
      | Left { state ; parent ; sibling } ->
          Zip (T.binode state tree sibling, parent)
      | Right { state ; parent ; sibling } ->
          Zip (T.binode state sibling tree, parent)

    let move_side (Zip (tree, path)) =
      match path with
      | Top ->
          failwith "move side of top"
      | Straight _ ->
          failwith "move side of straight"
      | Left { state ; parent ; sibling } ->
          Zip (sibling, Right { state ; parent ; sibling = tree })
      | Right { state ; parent ; sibling } ->
          Zip (sibling, Left { state ; parent ; sibling = tree })

    let move_down_straight (Zip (tree, path)) =
      T.case
        ~leaf:(fun _ -> failwith "move down leaf")
        ~node:(fun x stree ->
           Zip (stree, Straight { state = x ; parent = path })
         )
        ~binode:(fun _ _ _ -> failwith "move down binode straight")
        tree

    let move_down_left (Zip (tree, path)) =
      T.case
        ~leaf:(fun _ -> failwith "move down leaf")
        ~node:(fun _ -> failwith "move down node left")
        ~binode:(fun x ltree rtree ->
           Zip (ltree, Left { state = x ; parent = path ; sibling = rtree })
         )
        tree

    let move_down_right (Zip (tree, path)) =
      T.case
        ~leaf:(fun _ -> failwith "move down leaf")
        ~node:(fun _ -> failwith "move down node right")
        ~binode:(fun x ltree rtree ->
           Zip (rtree, Right { state = x ; parent = path ; sibling = ltree })
         )
        tree

    let remove_cursor zipped =
      let rec f z =
        match z with
        | Zip (tree, Top) ->
            tree
        | _ ->
            f (move_up z)
      in
      f zipped

    let replace_subtree = replace_subtree
    let prune = prune
    let graft_left = graft_left
    let graft_right = graft_right

    let zip tree path = Zip (tree, path)

    let case f (Zip (tree, path)) = f tree path

  end

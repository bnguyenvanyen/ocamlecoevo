

module type INPUT = Labeltimed.S

module type S =
  sig
    type tree

    val characterize : float -> tree -> tree -> float list * float list
    val distance : float -> tree -> tree -> float
  end


module Make : functor (T : INPUT) ->
  (S with type tree = T.t)

open Sig

module B = Base


module type INPUT =
  sig
    include STATE
    include TIMED with type t := t
  end


module type S =
  sig
    include BINTREE
    include TIME_TREE with type t := t and type state := state
    include HAS_LEN_TREE with type t := t and type state := state

    module State :
      sig
        include STATE with type t = state
        include TIMED with type t := t
      end
  end


module Make_min (State : INPUT) :
  (MINTREE with type state = State.t and type t = State.t tree) =
  struct
    type state = State.t
    type t = state tree

    let leaf x = B.leaf x

    let node x stree =
      let t = State.to_time (B.root_value stree) in
      if (State.to_time x <= t) then
        B.node x stree
      else
        invalid_arg "times not ordered"

    let binode x ltree rtree =
      let lt = State.to_time (B.root_value ltree) in
      let rt = State.to_time (B.root_value rtree) in
      let t = State.to_time x in
      if (t <= lt && t <= rt) then
        B.binode x ltree rtree
      else
        invalid_arg "times not ordered"

    let case =
      B.case
  end


module Make_core (State : INPUT) =
  struct
    let tip_value tree =
      let lv = B.leaves tree in
      let x0 = snd (List.hd lv) in
      List.fold_left (fun x (_, x') ->
        if State.to_time x' >= State.to_time x then
          x'
        else
          x
      ) x0 lv

    (* FIXME make tail rec ? *)
    let count_leaves tree =
      let t0 = State.to_time (B.root_value tree) in
      let rec count =
        function
        | B.Leaf x ->
            Jump.cons (t0, 0) (
              Jump.cons (State.to_time x, 1) Jump.empty
            )
        | B.Node (_, stree) ->
            count stree
        | B.Binode (_, ltree, rtree) ->
            Jump.Int.add (count ltree) (count rtree)
      in count tree

    (* FIXME make tail rec ? *)
    let count_leaves_backwards tree =
      let t0 = State.to_time (B.root_value tree) in
      let rec count =
        function
        | B.Leaf x ->
            Jump.empty
            |> Jump.cons (State.to_time x, 0)
            |> Jump.cons (t0, 1)
        | B.Node (_, stree) ->
            count stree
        | B.Binode (_, ltree, rtree) ->
            Jump.Int.add (count ltree) (count rtree)
      in count tree

    (* FIXME make tail rec ? *)
    let count_coals tree =
      let t0 = State.to_time (B.root_value tree) in
      let rec count =
        function
        | B.Leaf _ ->
            Jump.empty
        | B.Node (_, stree) ->
            count stree
        | B.Binode (x, ltree, rtree) ->
            let t = State.to_time x in
            Jump.Int.add (Jump.base (t, 1) (count ltree))
                         (Jump.base (t, 0) (count rtree))
      in Jump.base (t0, 0) (count tree)

    (* FIXME make tail rec ? *)
    let count_coals_backwards tree =
      let t0 = State.to_time (B.root_value tree) in
      let rec count =
        function
        | B.Leaf _ ->
            Jump.cons (t0, 0) Jump.empty
        | B.Node (_, stree) ->
            count stree
        | B.Binode (x, ltree, rtree) ->
            let t = State.to_time x in
            let ccoal =
              Jump.empty
              |> Jump.cons (t, 0)
              |> Jump.cons (t0, 1)
            in
            let cl = count ltree in
            let cr = count rtree in
            let c = Jump.Int.add cl cr
            in Jump.Int.add ccoal c
      in count tree

    let nlineages tree =
      let t0 = State.to_time (B.root_value tree) in
      let negleaves = Jump.map (~-) (count_leaves tree) in
      let coals = count_coals tree in
      let root = Jump.cons (t0, 1) Jump.empty in
      Jump.Int.add root (Jump.Int.add coals negleaves)

    let nlineages_backwards tree =
      let leaves = count_leaves_backwards tree in
      let negcoals = Jump.map (~-) (count_coals_backwards tree) in
      Jump.Int.add leaves negcoals

    let cut_before t0 tree =
      let rec f =
        function
        | (B.Leaf x as node) ->
            if State.to_time x <= t0 then
              `Forest []
            else
              `Tree node
        | (B.Node (x, stree) as node) ->
            (match f stree with
             | (`Forest _ as forest) ->
                 assert (State.to_time x < t0) ;
                 forest
             | `Tree snode ->
                 if State.to_time x < t0 then
                   `Forest [snode]
                 else
                   `Tree node)
        | (B.Binode (x, ltree, rtree) as node) ->
            (match f ltree, f rtree with
             | `Forest ltrees, `Forest rtrees ->
                 assert (State.to_time x < t0) ;
                 `Forest (ltrees @ rtrees)
             | `Forest strees, `Tree snode
             | `Tree snode, `Forest strees ->
                 assert (State.to_time x < t0) ;
                 `Forest (snode :: strees)
             | `Tree lnode, `Tree rnode ->
                 if State.to_time x < t0 then
                   `Forest [lnode ; rnode]
                 else
                   `Tree node)
      in
      let root_at_t0 tree =
        let x = B.root_value tree in
        let y = State.at_time t0 x in
        (* this should also be safe *)
        B.node y tree
      in
      match f tree with
      | `Forest trees ->
          List.map root_at_t0 trees
      | `Tree node ->
          [root_at_t0 node]

    let cut_after tf tree =
      let rec f tree =
        let i = B.root_value tree in
        let t = State.to_time i in
        (* print_float y ; print_string " " ; *)
        if t >= tf then
          B.leaf (State.at_time tf i)  (* > or >= ? *)
        else
          (* here we use the Base constructors because
           * we know the tree is valid, and we don't have the right functions*)
          match tree with
          | B.Leaf i ->
              B.leaf i
          | B.Node (i, stree) ->
              B.node i (f stree)
          | B.Binode (i, ltree, rtree) ->
              B.binode i (f ltree) (f rtree)
      in f tree

    let cut_between t0 tf tree =
      cut_before t0 (cut_after tf tree)

    let ultra_paths x t =
      let cond p =
        let ts = B.descend p t in
        let y = State.to_time (B.root_value ts) in
        (y >= x)
      in
      let ct = cut_after x t in
      List.filter cond (B.all_paths ct)

    let ultra_restr x t =
      let ct = cut_after x t in 
      (* we look forward to the leaves *)
      let p_l = ultra_paths x t in
      let sp = Path.combine_paths p_l in
      B.genealogy sp ct
  end


module Make_io (State : INPUT) (T : MINTREE with type state = State.t) =
  struct
    let rec validate =
      function
      | B.Leaf x ->
          T.leaf x
      | B.Node (x, stree) ->
          T.node x (validate stree)
      | B.Binode (x, ltree, rtree) ->
          T.binode x (validate ltree) (validate rtree)

    include Io.Make (State) (T)
  end


module Make_has_len (State : INPUT) (T : MINTREE with type state = State.t) =
  struct
    module State' =
      struct
        type t = State.t
        let to_length = State.to_time
        let with_length = State.at_time
        let to_string = State.to_string
        let of_string = State.of_string
      end
    module Len = Lengthed.Make (State')

    type t' = Len.t

    let time_tree ?(root=0.) tree =
      let rec f t tree =
        let leaf x =
          let t' = t +. State'.to_length x in
          T.leaf (State.at_time t' x)
        in
        let node x stree =
          let t' = t +. State'.to_length x in
          T.node (State.at_time t' x) (f t' stree)
        in
        let binode x ltree rtree =
          let t' = t +. State'.to_length x in
          T.binode (State.at_time t' x) (f t' ltree) (f t' rtree)
        in
        Len.case ~leaf ~node ~binode tree
      in f root tree

    let untime_tree tree =
      let rec f t tree =
        let leaf x =
          let l = State.to_time x -. t in
          Len.leaf (State'.with_length l x)
        in
        let node x stree =
          let t' = State.to_time x in
          Len.node (State'.with_length (t' -. t) x) (f t' stree)
        in
        let binode x ltree rtree =
          let t' = State.to_time x in
          Len.binode
            (State'.with_length (t' -. t) x)
            (f t' ltree)
            (f t' rtree)
        in
        T.case ~leaf ~node ~binode tree
      in
      T.case
        ~leaf:(fun _ -> invalid_arg "unrooted tree")
        ~binode:(fun _ _ _ -> invalid_arg "unrooted tree")
        ~node:(fun x stree ->
          let root_t = State.to_time x in
          Len.node (State'.with_length 0. x) (f root_t stree)
        )
        tree

    let read_lengthed_newick ?root chan =
      let lengthed = Len.read_newick chan in
      time_tree ?root lengthed

    let output_lengthed_newick chan tree =
      (Len.output_newick chan) @@ untime_tree @@ tree

    let branch_length tree =
      Len.branch_length (untime_tree tree)

    (* FIXME this didn't work. why ?
    let branch_length' tree =
      let tmax = S.to_time (tip_value tree) in
      let c = Jump.map (fun n -> float_of_int n) (nlineages tree) in
      Jump.integr c tmax
    *)

  end


module Make (State : INPUT) :
  (S with type state = State.t) =
  struct
    module State = State

    module T = Make_min (State)
    include T
    include Map.Make (T)
    include Bintree.Make_core (State)
    include Make_core (State)
    include Make_io (State) (T)
    include Make_has_len (State) (T)
  end

(* tracking relationships with int idents *)

module B = Base

module Intbl = BatHashtbl.Make (struct
  type t = int
  let hash n = n
  let equal = (=)
end)

(* new approach : just go up event by event from the most recent one *)
(* might use a set to keep sorted times into *)
module Tmap = BatMap.Make (struct
  type t = float
  let compare x y = ~- (compare x y)
end)

module Iset = BatSet.Make (struct
  type t = int
  let compare = compare
end)

module Imap = BatMap.Make (struct
  type t = int
  let compare = compare
end)

type ev =
  | Co of Iset.t
  | Ap of Iset.t
  | Apco of Iset.t * Iset.t  (* simultaneously one death one coalescence (= mutation) *)

(* an edge in the genealogy *)
type indiv = {
  t_birth : float ;  (** time of birth *)
  t_death : float ;  (** time of death *)
  parent_id : int ;  (** id of parent *)
  children_ids : int list ;  (** id of children, by order of birth *)
}

type t = indiv Intbl.t


let forest_root = ~-1


let root_forest gen tree_ids =
  assert (not (Intbl.mem gen forest_root)) ;
  Intbl.add gen forest_root {
    t_birth = neg_infinity ;
    t_death = infinity ;
    parent_id = forest_root ;
    children_ids = tree_ids
  }


(* FIXME can add trait_of : int -> 'a *)
let iter_descend f gen =
  let rec g_step i =
    let ind = Intbl.find gen i in
    f i ind ;
    List.iter g_step ind.children_ids
  in g_step forest_root


let get_birth_time i gen =
  (Intbl.find gen i).t_birth


let ancestry i gen =
  let rec add_ancestor anc_l k =
    let parent = (Intbl.find gen k).parent_id in
    if k = forest_root then (* reached the root *)
      parent :: anc_l
    else
      add_ancestor (parent :: anc_l) parent
  in add_ancestor [i] i


let tmrca anc anc' gen =
  let rec descend_ancestry l l' =
    match l, l' with
    | [], [] -> (* identical ancestries *)
      get_birth_time forest_root gen
    | (i :: _, []) | ([], i :: _) -> (* one descends from the other *)
      get_birth_time i gen 
    | i :: tl, i' :: tl' when i = i' ->
      descend_ancestry tl tl'
    | i :: _, i' :: _ ->
      let t = get_birth_time i gen in
      let t' = get_birth_time i' gen in
      min t t'
  in descend_ancestry anc anc'


let ev_merge _ iseto iseto' =
  match iseto, iseto' with
  | Some iset, None ->
    Some (Co iset)
  | None, Some iset ->
    Some (Ap iset)
  | Some iset, Some iset' ->
    Some (Apco (iset', iset))
  | None, None ->
    None


let pool_mrca iset gen =
  let anc_m = Iset.fold (fun i m -> Imap.add i (ancestry i gen) m) iset Imap.empty in
  (* first get tmrca for each unordered pair as a (t, i, j) list *)
  let rec f i anc sanc_m pairs =
    let sub_append i' anc' mrca_l = (tmrca anc anc' gen, i, i') :: mrca_l in
    let pairs' = Imap.fold sub_append sanc_m pairs in
    (* then call f on an element of sanc_m and the rest of sanc_m *)
    let i', anc' = Imap.min_binding sanc_m in
    let sanc_m' = Imap.remove i' sanc_m in
    if sanc_m' = Imap.empty then
      pairs'
    else
      f i' anc' sanc_m' pairs'
  in
  let i, anc = Imap.choose anc_m in
  let sanc_m = Imap.remove i anc_m in
  let pairs = f i anc sanc_m [] in
  (* then pool identical times *)
  let f_pool pool_m (t, i, i') =
    let iset =
      try
        Tmap.find t pool_m
      with Not_found ->
        Iset.empty
    in Tmap.add t (Iset.add i (Iset.add i' iset)) pool_m
  in List.fold_left f_pool Tmap.empty pairs


let choose_alive t iset ngen =
  (* hypothesis only two indivs of iset are alive at t *)
  let p_alive i =
    let ind = Intbl.find ngen i in
    (ind.t_birth <= t) && (t <= ind.t_death)
  in
  let fiset = Iset.filter p_alive iset in
  (* min_elt on the left : guarantees we keep 0 *)
  let mi = Iset.min_elt fiset in
  let rem = Iset.remove mi fiset in
  mi, rem
    

let coalesce rng p gen =
  (* decide ids that we keep (with proba p) *)
  (* we suppose all event times different *)
  (* also have death time ? *)
  let f_keep (i : int) _ iset =
    (* missing 0 in test_seq : why ? *)
    if (Util.rand_bernoulli ~rng p) then
      Iset.add i iset
    else
      iset
  in
  let keep_i_s = Intbl.fold f_keep gen (Iset.singleton forest_root) in
  let tmrca_m = pool_mrca keep_i_s gen in
  let f_death i dm =
    let td = (Intbl.find gen i).t_death in
    let iset =
      try
        Tmap.find td dm
      with Not_found ->
        Iset.empty
    in Tmap.add td (Iset.add i iset) dm
  in
  let death_m = Iset.fold f_death keep_i_s Tmap.empty in
  let ev_m = Tmap.merge ev_merge tmrca_m death_m in
  let nindivs = Iset.cardinal keep_i_s in
  let ngen = Intbl.create nindivs in
  (* and now we can only take out events and not add them *)
  (* no need to reparent either, when we reach a Co
   * we just look for the only two indivs alive in ngen *)
  let appear t iset =
    Iset.iter (fun i ->
      Intbl.add ngen i { t_birth = 0. ;
                         t_death = t ;
                         parent_id = forest_root ;
                         children_ids = [] }
    ) iset
  in
  let coal t iset =
    let i, jset = choose_alive t iset ngen in
    let jl = Iset.elements jset in
    (* arbitrarily, keep i, kill jset *)
    let indi = Intbl.find ngen i in
    Intbl.replace ngen i { indi with children_ids = jl @ indi.children_ids } ;
    let f_reparent j =
      let indj = Intbl.find ngen j in
      Intbl.replace ngen j { indj with t_birth = t ;
                                       parent_id = i }
    in List.iter f_reparent jl
  in
  let rec f_back tm =
    (* FIXME how do we know when to stop ? in Co somewhere *)
    (* last event happening *)
    if tm = Tmap.empty then
      ()
    else
      begin
        let t, ev = Tmap.min_binding tm in
        match ev with
        | Ap iset ->  (* appearance of i *)
          (* first time we see i *)
          appear t iset ;
          let tm = Tmap.remove t tm in
          f_back tm
        | Co iset ->  (* coalescence of iset *)
          coal t iset ;
          let tm = Tmap.remove t tm in
          f_back tm
        | Apco (iset, iset') ->
          (* first time we see i *)
          (* handle Ap *)
          appear t iset ;
          (* then handle Co *)
          coal t iset' ;
          let tm = Tmap.remove t tm in
          f_back tm
      end
  in
  (* I think we necessarily start from a leaf so no children *)
  f_back ev_m ;
  ngen



  

(* FIXME more economic : build the tree pruned *)
let prune rng p gen =
  (* start from index 0, keep the roots, then descend through children
   * for each index, with proba p, forget the individual :
   * remove it from the children of its parents, add to these children
   * its children, and change the parent of all its children *)
  let forget i ind =
    (* FIXME can raise Not_found : why ? *)
    let pind = Intbl.find gen ind.parent_id in
    (* Pb : for tree_of we count on order of siblings so we need to sort
     * which is costly
     * What if we replaced by set ? *)
    let nsiblings = List.sort
      (fun j k -> compare (get_birth_time j) (get_birth_time k))
      (ind.children_ids @ (List.filter (fun j -> j <> i) pind.children_ids))
    in
    (* FIXME this replace doesn't necessarily work *)
    Intbl.replace gen ind.parent_id { pind with children_ids = nsiblings } ;
    let replace_parent k =
      let cind = Intbl.find gen k in
      (* normally parentc = i *)
      Intbl.replace gen k { cind with parent_id = ind.parent_id }
    in
    List.iter replace_parent ind.children_ids ;
    Intbl.remove gen i
  in
  let f_iter i value =
    (* forget with proba p except if it's the root (then don't forget) *)
    if (Util.rand_bernoulli ~rng p) || (i = forest_root) then
      ()
    else
      forget i value
  in iter_descend f_iter gen


(* FIXME does this put the root twice ? *)
let to_tree gen =
  (* the unrooted (not rooted at birth) sub tree for i *)
  let rec unrooted_sub_tree i =
    let ind = Intbl.find gen i in
    (* check that birth times are increasing *)
    (* I think this is actually checked later by Lt constructors but well *)
    let birth_times = List.map (fun j ->
      let child = Intbl.find gen j in
      child.t_birth
    ) ind.children_ids
    in
    ignore (
      List.fold_left (fun t t' ->
        assert (t <= t') ; t'
      ) neg_infinity birth_times
    ) ;
    let unrooted =
      (* inspect children starting from the last *)
      match List.rev ind.children_ids with
      | [] ->
          Lt.leaf (ind.t_death, i)
      | j :: ks ->
          (* the first index here is the last child,
           * which must be treated specially *)
          let last_left_tree = root_last_sub_tree ind.t_death i j in
          List.fold_left (fun ltree k ->
            root_sub_tree ltree ind.t_death i k
          ) last_left_tree ks
    in
    ind.t_birth, unrooted
  (* root the sub tree *)
  and root_sub_tree left_tree parent_t_death parent_id child_id =
    let tb, child_tree = unrooted_sub_tree child_id in
    assert (parent_t_death > tb) ;
    Lt.binode (tb, parent_id) left_tree child_tree
  (* root the last sub tree *)
  and root_last_sub_tree parent_t_death parent_id child_id =
    let tb, child_tree = unrooted_sub_tree child_id in
    if Util.nearly_equal tb parent_t_death then
      Lt.node (tb, child_id) child_tree
    else
      let parent_death = Lt.leaf (parent_t_death, parent_id) in
      Lt.binode (tb, parent_id) parent_death child_tree
  in
  (fun i ->
    let tb, unrooted = unrooted_sub_tree i in
    Lt.node (tb, i) unrooted
  )


let to_forest gen =
  let ind0 = Intbl.find gen forest_root in
  List.map (fun i ->
    let tree = to_tree gen i in
    Lt.case
     ~leaf:(fun _ -> failwith "unrooted tree")
     ~binode:(fun _ _ _ -> failwith "unrooted tree")
     ~node:(fun _ _ -> ())
     tree
    ;
    tree
  ) (List.rev ind0.children_ids)
  (* rev : start by the most recent tree *) 


let of_forest tree_l =
  let gen = Intbl.create 997 in
  (* follow convention that parent is on the left *)
  (* difft rec functions for left / right branch ? *)
  let rec descend tree tb parent_id children_ids =
    let leaf (t, i) =
      (* leaf = death of the indiv we were following *)
      Intbl.add gen i
      { t_birth = tb ; t_death = t ; parent_id ; children_ids } ;
      i
    in
    let node _ stree =
      (* node = mutation of the indiv we are following
       * ignore it *)
      descend stree tb parent_id children_ids
    in
    let binode (t, i) ltree rtree =
      (* rtree should be new child born at t from parent i *)
      (* check ltree is of id i *)
      let _, i' = Lt.root_value ltree in
      assert (i = i') ;
      let child = descend rtree t i [] in
      descend ltree tb parent_id (child :: children_ids)
    in
    Lt.case ~leaf ~node ~binode tree
  in
  let extract_root_and_descend tree =
    Lt.case
      ~leaf:(fun _ -> failwith "unrooted tree")
      ~binode:(fun _ _ _ -> failwith "unrooted tree")
      ~node:(fun (t, i) stree -> ignore (descend stree t forest_root []) ; i)
      tree
  in
  let roots = List.fold_left (fun roots tree ->
    let root = extract_root_and_descend @@ tree in
    root :: roots
  ) [] tree_l
  in
  root_forest gen roots ;
  gen

module Int =
  struct
    type t = int

    let root_label = 0

    let equal (x : int) (y : int) =
      x = y

    let compare (x : int) (y : int) =
      compare x y

    let hash n =
      n

    let to_string = string_of_int

    let of_string s =
      match int_of_string s with
      | n ->
          Some n
      | exception Failure _ ->
          None
  end


module String =
  struct
    type t = string

    let root_label = ""

    let equal (x : t) (y : t) =
      x = y

    let compare (x : t) (y : t) =
      compare x y

    let hash x =
      Hashtbl.hash x

    let to_string x = x

    let of_string s = Some s
  end

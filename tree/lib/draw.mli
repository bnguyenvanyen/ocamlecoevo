(** Draw time-embedded trees *)

open Sig


type rgb = draw_rgb


(** [black] is the color black *)
val black : rgb


(** [init_fig fname w h] initiates a SVG figure at path [fname]
 *  with width [w] and height [h] *)
val init_fig :
  string ->
  float ->
  float ->
  (Cairo.Surface.t * Cairo.context)


(** [plot cr tree] plots [tree] in the context [cr] *)
val plot :
  Cairo.context ->
  Sig.draw_point Base.tree ->
  unit


module type S =
  sig
    include Timed.S
    include PLOT_TREE with type t := t and type state := state
  end


module Complement :
  functor (T : Timed.S) -> (S with type t = T.t and type state = T.state)


module Make : functor
  (S :
    sig
      include STATE
      include TIMED with type t := t
    end
  ) ->
    (S with type state = S.t)

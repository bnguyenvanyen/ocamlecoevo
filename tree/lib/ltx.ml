open Sig

module Make (L : LABEL) (X : TRAIT) :
  sig
    include STATE
    include TIMED with type t := t
    include LABELED with type t := t and type label = L.t
    include TRAITED with type t := t and type trait = X.t
  end =
  struct
    type label = L.t
    type trait = X.t

    type t = float * label * trait

    module Label = L
    module Trait = X

    let to_time (t, _, _) = t
    let to_label (_, k, _) = k
    let to_trait (_, _, x) = x
    let at_time s (_, k, x) = (s, k, x)
    let to_string (t, k, x) =
      Printf.sprintf "%s[%s]:%.2f" (L.to_string k) (X.to_string x) t
    let of_string s =
      match Scanf.sscanf s "%s[%s]:%.2f" (fun ks xs t -> (ks, xs, t)) with
      | ks, xs, t ->
          begin match L.of_string ks, X.of_string xs with
          | Some k, Some x ->
              Some (t, k, x)
          | Some _, None
          | None, Some _
          | None, None ->
              None
          end
      | exception Scanf.Scan_failure _ ->
          None
  end

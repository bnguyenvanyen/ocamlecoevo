open Sig


let%expect_test _ =
  B.(right_iter (Printf.printf "%s ") (binode "A" (leaf "B") (leaf "C"))) ;
  [%expect{| A C B |}]

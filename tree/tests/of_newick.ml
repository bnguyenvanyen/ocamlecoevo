open Sig


let%test _ =
  match B.of_newick "" with
  | None ->
      true
  | Some _ ->
      false


(* string letter tree *)

let%test _ =
  match B.of_newick "A;" with
  | None ->
      false
  | Some tree ->
      tree = B.leaf "A"

let%test _ =
  let tree = B.(node "A" (binode "B" (leaf "C") (leaf "D"))) in
  let newick = B.to_newick (fun s -> s) tree in
  match B.of_newick newick with
  | None ->
      false
  | Some tree' ->
      tree = tree'



(* time tree *)

let%test _ =
  match T.of_newick ":2.014e-6;" with
  | None ->
      false
  | Some tree ->
      tree = T.leaf 2.014e-6


let%test _ = 
  let tree = T.(node 0. (binode 0.623 (leaf 1.01) (leaf 2.07))) in
  let newick = T.to_newick tree in
  match T.of_newick newick with
  | None ->
      false
  | Some tree' ->
      tree = tree'

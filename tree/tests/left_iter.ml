open Sig


let%expect_test _ =
  B.(left_iter (Printf.printf "%s ") (binode "A" (leaf "B") (leaf "C"))) ;
  [%expect{| A B C |}]

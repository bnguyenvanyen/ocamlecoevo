module T = Tree.T


let tree () =
  T.node 0. (
    T.binode 1.
      (T.leaf 3.)
      (T.binode 2. (T.leaf 4.) (T.leaf 5.))
  )


let%expect_test _ =
  let tree = tree () in
  let lengthed = T.untime_tree tree in
  let s = T.Len.to_newick lengthed in
  Printf.printf "%s" s ;
  [%expect{| ((:2.000,(:2.000,:3.000):1.000):1.000):0.000; |}]


let%expect_test _ =
  let tree = tree () in
  let len = T.branch_length tree in
  Printf.printf "%f" len ;
  [%expect{| 9.000000 |}]

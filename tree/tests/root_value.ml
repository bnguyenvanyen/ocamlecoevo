open Sig


let%expect_test _ =
  P.printf "%s\n" B.(root_value (node "A" (leaf "B"))) ;
  [%expect{| A |}]

module KC = Tree.Kendall_colijn.Make (Tree.Lt)


let%expect_test _ =
  let tree1 = Util.Option.some (Tree.Lt.of_newick
    "(((4:3.5,5:4.2)2:2.0,(6:4.5,7:5.0)3:3.0)1:0.5)0:0.0;"
  )
  in
  let tree2 = Util.Option.some (Tree.Lt.of_newick
    "(((4:3.5,6:4.5)2:2.5,(5:4.2,7:5.0)3:3.0)1:0.5)0:0.0;"
  )
  in
  let v1, v2 = KC.characterize 0.5 tree1 tree2 in
  BatList.print BatFloat.print BatIO.stdout v1 ;
  [%expect{| [1.25; 0.; 0.; 0.; 0.; 1.75; 1.25; 1.6; 1.25; 1.5] |}] ;
  BatList.print BatFloat.print BatIO.stdout v2 ;
  [%expect{| [0.; 1.5; 0.; 0.; 1.75; 0.; 1.; 1.1; 1.5; 1.5] |}]

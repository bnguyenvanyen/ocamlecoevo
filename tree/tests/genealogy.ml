open Tree


let%test _ =
  let tree =
    Lt.node (0., 1)
      (Lt.binode (1., 1)
        (Lt.leaf (2., 1))
        (Lt.leaf (2.5, 2))
      )
  in
  (* of_forest is broken, or needs to make its assumptions clearer *)
  let gen = Genealogy.of_forest [tree] in
  (* looks like trees' is weirdly empty : but it should never happen *)
  let trees' = Genealogy.to_forest gen in
  let tree' = List.hd trees' in
  (* a bit risky because float equality *)
  (tree = tree')

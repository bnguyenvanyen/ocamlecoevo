open Sig


let%expect_test _ =
  B.(
    output_newick
      (fun s -> s)
      stdout
      (node "A" (binode "B" (leaf "C") (leaf "D")))
  ) ;
  [%expect{| ((C,D)B)A; |}]

module T = Tree.T

let tree () =
  T.node 0. (
    T.binode 1.
      (T.leaf 3.)
      (T.binode 2. (T.leaf 4.) (T.leaf 5.))
  )


let%expect_test _ =
  let tree = tree () in
  let nlin = T.nlineages tree in
  Jump.print BatInt.print BatIO.stdout nlin ;
  [%expect{|
    0.: 1
    1.: 2
    2.: 3
    3.: 2
    4.: 1
    5.: 0 |}]


let%expect_test _ =
  let tree = tree () in
  let nlin = T.nlineages_backwards tree in
  Jump.print BatInt.print BatIO.stdout nlin ;
  [%expect{|
    0.: 1
    1.: 2
    2.: 3
    3.: 2
    4.: 1
    5.: 0 |}]

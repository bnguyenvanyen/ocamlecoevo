open Sig


let%expect_test _ =
  P.printf "%i\n" B.(nnodes (binode "A" (leaf "B") (leaf "C"))) ;
  [%expect{| 3 |}]

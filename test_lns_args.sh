ROOT_NAME="test_data/lns_mu1_k3_n4_b3_pkeep6"
./test_lns.native -tf 10. -b 1e3 -c 1e-1 -d 0. -mu 1e-1 -k 1000 -pkeep 1e-6 -dtprint 0.1 -out "${ROOT_NAME}"
tail -n 2 "${ROOT_NAME}.newick" | head -n 1 | ./nlclft.native -out "${ROOT_NAME}_nlclft.csv"
tail -n 2 "${ROOT_NAME}.newick" | head -n 1 | ./plot_unit_tree.native -out "${ROOT_NAME}.svg"

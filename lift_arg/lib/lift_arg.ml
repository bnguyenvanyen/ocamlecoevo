type 'a spec =
  | Bool of ('a -> bool -> 'a)
  | Int of ('a -> int -> 'a)
  | Float of ('a -> float -> 'a)
  | String of ('a -> string -> 'a)
  | Symbol of string list * ('a -> string -> 'a)
  | Symbol_value of string list * (string -> ('a -> string -> 'a))
  | Tuple of 'a spec list

type 'a method_spec =
  | Mbool of (bool -> 'a)
  | Mint of (int -> 'a)
  | Mfloat of (float -> 'a)
  | Mstring of (string -> 'a)
  | Msymbol of string list * (string -> 'a)
  | Mtuple of 'a method_spec list


let rec set z sp =
  match sp with
  | Bool f ->
      Arg.Bool (fun x -> z := f !z x)
  | Int f ->
      Arg.Int (fun x -> z := f !z x)
  | Float f ->
      Arg.Float (fun x -> z := f !z x)
  | String f ->
      Arg.String (fun x -> z := f !z x)
  | Symbol (symbs, f) ->
      Arg.Symbol (symbs, (fun x -> z := f !z x))
  | Symbol_value (symbs, symb_spec) ->
      let f_r = ref None in
      Arg.Tuple [
        Arg.Symbol (symbs, (fun s -> f_r := Some (symb_spec s))) ;
        Arg.String (fun x ->
          match !f_r with
          | None ->
              assert false
          | Some f ->
              z := f !z x
        )
      ]
  | Tuple spl ->
      Arg.Tuple (List.map (set z) spl)


let rec spec_of_method (z : 'a ref) (msp : 'a method_spec) =
  match msp with
  | Mbool f ->
      Arg.Bool (fun x -> z := f x)
  | Mint f ->
      Arg.Int (fun x -> z := f x)
  | Mfloat f ->
      Arg.Float (fun x -> z := f x)
  | Mstring f ->
      Arg.String (fun x -> z := f x)
  | Msymbol (symbs, f) ->
      Arg.Symbol (symbs, (fun x -> z := f x))
  | Mtuple mspl ->
      Arg.Tuple (List.map (spec_of_method z) mspl)
  

let spec_of (z : 'a ref) (k, sp, h) =
  (k, set z sp, h)


let specl_of (z : 'a ref) l =
  List.map (spec_of z) l


let specl_of_method (z : 'a ref) l =
  List.map (fun (k, msp, h) -> (k, spec_of_method z msp, h)) l


let replace
  (type a) (type b) (type c)
  (f : (a -> c -> a))
  (g : (b -> a -> b))
  (g' : (b -> a)) :
  (b -> c -> b) =
  (fun z x -> g z (f (g' z) x))


let map g g' l =
  let rec f_map sp =
    match sp with
    | Bool f ->
        Bool (replace f g g')
    | Int f ->
        Int (replace f g g')
    | Float f ->
        Float (replace f g g')
    | String f ->
        String (replace f g g')
    | Symbol (symbs, f) ->
        Symbol (symbs, replace f g g')
    | Symbol_value (symbs, symb_spec) ->
        Symbol_value (symbs, (fun s -> replace (symb_spec s) g g'))
    | Tuple spl ->
        Tuple (List.map f_map spl)
  in List.map (fun (k, sp, h) -> (k, f_map sp, h)) l


let handle_chan ?(open_flags=None) ?(perm=640) s =
  if s = "stdout" then
    stdout
  else if s = "stderr" then
    stderr
  else
    match open_flags with
    | None -> open_out s
    | Some l -> open_out_gen l perm s


let handle_chan_in s =
  if (s = "-") || (s = "stdin") then
    stdin
  else
    open_in s


let open_chans (out_fname : ?line:int -> string -> string) input output lines =
  match input, output, lines with
  (* if no filename is specified, stdin --> stdout,
   * with specified lines, and 1 by default *)
  | None, None, [] ->
      stdin,
      [(1, stdout)]
  | None, None, (_ :: _ as lines) ->
      stdin,
      List.map (fun i -> (i, stdout)) lines
  (* if both in fname and out fname are specified, use those,
   * with specified lines, and 1 by default *)
  | Some s, Some s', [] ->
      (open_in s),
      [(1, open_out s')]
  | Some s, Some s', (_ :: _ as lines) ->
      (open_in s),
      List.map (fun i -> (i, open_out s')) lines
  (* if only in fname is given, out fname is given by out_csv,
   * with line number inserted if there are more than one read *)
  | Some s, None, [] ->
      (open_in s),
      [(1, open_out (out_fname s))]
  | Some s, None, i :: [] ->
      (open_in s),
      [(i, open_out (out_fname s))]
  | Some s, None, (_ :: _ as lines) ->
      (open_in s),
      List.map (fun i -> (i, open_out (out_fname ~line:i s))) lines
  (* if only out fname is given, read from stdin,
   * with specified lines and 1 by default *)
  | None, Some s', [] ->
      stdin,
      [(1, open_out s')]
  | None, Some s', (_ :: _ as lines) ->
      stdin,
      List.map (fun i -> (i, open_out s')) lines


(*
let spec_in = (
  "-in",
  String (fun _ s -> handle_chan_in s),
  "File (or handle) to read from."
)

let spec_out = (
  "-out",
  String (fun _ s -> handle_chan s),
  "File (or handle) to output to. Default stdout."
)
*)

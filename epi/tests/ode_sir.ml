module F = Util.Float

module Cont = Epi.Process.Continuous.Make (Epi.Sir.Model)


let%expect_test _ =
  let par = (new Epi.Param.t_unit)#with_host_birth 0. in
  let header = Epi.Traj.header in
  let line = Cont.line par in
  let x = F.Pos.of_float 4. in
  let z0 = Cont.of_sir (x, x, x) in
  let tf = F.Pos.of_float 0.1 in
  let dt = F.Pos.of_float 0.02 in
  let output = Sim.Csv.convert ~dt ~header ~line ~chan:stdout in
  ignore (Cont.Ode.sim ~output par z0 tf) ;
  [%expect {|
    t,s,e,i,r,c,o,n,reff,coal,reportr
    0.,4,0,4,4,0,0,12,0.448718,5.83333,9.33333
    0.02,2.82764,0,2.0815,7.08822,0,0,11.9974,0.317273,7.92611,3.43409
    0.04,2.37929,0,0.992725,8.62376,0,0,11.9958,0.267002,13.9858,1.37831
    0.06,2.19404,0,0.457388,9.3433,0,0,11.9947,0.246235,27.9942,0.58565
    0.08,2.11386,0,0.207686,9.67239,0,0,11.9939,0.237252,59.4025,0.256224
    0.1,2.07821,0,0.0937036,9.82134,0,0,11.9933,0.233264,129.448,0.11366 |}]



let%expect_test _ =
  let par = (new Epi.Param.t_unit)#with_host_birth 0. in
  let x = F.Pos.of_float 4. in
  let z0 = Cont.of_sir (x, x, x) in
  let tf = F.Pos.of_float 0.1 in
  let dt = F.Pos.of_float 0.02 in
  let output = Cont.Cadlag.convert ~dt par in
  let traj = Cont.Ode.sim ~output par z0 tf in
  Printf.printf "t,cases\n" ;
  traj
  |> Epi.Data.binned_reportr_data
  |> Jump.to_list
  |> List.iter (fun (t, r) -> Printf.printf "%f,%f\n" t (F.to_float r))
  ;
  [%expect{|
    t,cases
    0.020000,0.186667
    0.040000,0.068682
    0.060000,0.027566
    0.080000,0.011713
    0.100000,0.005124 |}]

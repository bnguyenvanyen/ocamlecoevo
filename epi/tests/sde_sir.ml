open Epi
open Sig

module C = Process.Continuous.Make (Sir.Model)


let%expect_test _ =
  let par = (new Param.t_unit)#with_host_birth 0. in
  let t0 = F.zero in
  let dt = F.Pos.of_float 0.05 in
  let tf = F.Pos.of_float 0.1 in
  let rng = U.rng (Some 0) in
  let dbts = Sim.Dbt.rand_draw ~rng ~t0 ~dt ~tf ~n:C.n_events in
  let sir = (F.Pos.of_float 2., F.Pos.of_float 4., F.Pos.of_float 6.) in
  let header = Traj.header in
  let line = C.line par in
  let output = Sim.Csv.convert ?dt:None ~header ~line ~chan:stdout in
  ignore (C.Sde_limit.sim ~output par (C.of_sir sir) dbts) ;
  [%expect{|
    t,s,e,i,r,c,o,n,reff,coal,reportr
    0.,2,0,4,6,0,0,12,0.224359,2.91667,4.66667
    0.,2,0,4,6,0,0,12,0.224359,2.91667,4.66667
    0.05,3.15371,0,3.76969,18.7783,0,0,25.7017,0.165179,2.27851,3.2379
    0.1,1.62166,0,10.0338,34.0005,0,0,45.6559,0.0478141,0.247795,2.49475 |}]

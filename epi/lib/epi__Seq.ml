module Ss = Seqsim


class mutating = object
  val evov = Ss.Mut.default
  val p_keepv = 1.

  method evo = evov
  method p_keep = p_keepv

  method with_evo x = {< evov = x >}
  method with_p_keep x = {< p_keepv = x >}
end


(*
module P = struct
  type x = Seq.t * (float * Seq.Diff.t) list
  let dflt = (Seq.of_string "", [])  (* unused *)
  include Pop.Simple.Make (Trait.Seq)
end
*)

let update_on_record t (seq, sdl) =
  (seq, (t, Seqs.Sig.Not) :: sdl)


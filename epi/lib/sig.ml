(* FIXME or do we include Pop.Sig ? *)
open Pop.Sig

module L = BatList
module Lac = Lacaml.D

module U = Util
module F = U.Float
module I = U.Int

module SCU = Sim.Ctmjp.Util


type vec = Lac.Vec.t
type mat = Lac.Mat.t

type nonrec id = id
type nonrec nonid = nonid

(* We are forced to not use the same type anymore for Unit / Seq ?
 * That's really annoying, anyway to go through that ?
 * we can also keep it phantom and give a different function
 * for each constructor and change them.
 * But it's dumb, and also then we can't use the actual constructors,
 * which is bad. *)


(* for groups, only group_of can help us know whether it should be id / nonid *)

(*
type 'a group = [ `Sus | `Exp | `Inf | `Rem | `Cas | `Out ]
*)

(* colors for all models and versions *)

(* can we reuse colors more ?
 * so, be less specific :
 * - don't talk about the cases
 * - don't talk about the record / outseed
 * - for infections don't distinguish between an infection of an I
 *   or an infection of an E, because only one of them happens in a model *)

type color_base = [
  | `I_infection
  | `Recovery
  | `S_birth
  | `S_death
  | `E_death
  | `I_death
  | `R_death
]

type color_latent = [
  | `Leave_exposed
]

type color_circular = [
  | `Immunity_loss
  | `O_infection
]


type color_seq = [
  | `O_death
  | `I_subst
  | `I_rep
  | `I_del
  | `E_subst
  | `E_rep
  | `E_del
]


type color = [
  | color_base
  | color_latent
  | color_circular
  | color_seq
]


type color_unit = [
  | color_base
  | color_latent
  | color_circular
]


type model_spec = {
  latency : bool ;
  circular : bool ;
}


type (_, _) version =
  | Unit_gill :
      (int, int) version
  | Unit_prm :
      (int, int) version
  | Unit_prm_approx :
      (int, int) version
  | Unit_ode :
      (float, float) version
  | Seq_gill :
      (int, (int * Seqs.t) list) version


(* existential for versions *)
type e_version = V : (_, _) version -> e_version


module type PAYLOAD =
  sig
    type idor
    type 'a t

    (* idor is only either id or nonid (but not anyid) *)
    val isid : idor isid
    val default : idor t
    val copy : 'a t -> 'a t
    val score : 'a t -> float
  end


module type TRAIT =
  sig
    (* Careful : it's NOT actually the same 'isid' as in Pop...
     * here isid should be id or nonid but not anyid *)
    type idor

    type 'a payload

    module Payload : (PAYLOAD with type idor = idor
                               and type 'a t = 'a payload)

    type _ t =
      | S : nonid t
      | E : idor payload -> idor t
      | I : idor payload -> idor t
      | R : nonid t
      | C : nonid t
      | O : idor payload -> idor t

    type _ group =
      | Sus : nonid group
      | Exp : idor group
      | Inf : idor group
      | Rem : nonid group
      | Cas : nonid group
      | Out : idor group

    (* This might not work with idor=nonid ? *)
    val payload : idor t -> idor payload

    val score : idor t -> float

    include (TRAIT with type 'a t := 'a t
                    and type 'a group := 'a group)
  end


(* We would also like something like
module type POP = (POP with module T : TRAIT)
How can we achieve this ?
*)


(* Here t can only return positive values *)
module type GET =
  sig
    type t
    (* for now like this, to clean up later *)
    (* val get : 'a group -> t -> float *)
    val sus : t -> _ U.pos
    val exp : t -> _ U.pos
    val inf : t -> _ U.pos
    val rem : t -> _ U.pos
    val cas : t -> _ U.pos
    val out : t -> _ U.pos
    val tot : t -> _ U.pos
  end


module type SET =
  sig
    type t

    val sus : t -> _ U.anyfloat -> unit
    val exp : t -> _ U.anyfloat -> unit
    val inf : t -> _ U.anyfloat -> unit
    val rem : t -> _ U.anyfloat -> unit
    val cas : t -> _ U.anyfloat -> unit
    val out : t -> _ U.anyfloat -> unit
  end


module type SETPOS =
  sig
    type t

    val sus : t -> _ U.anypos -> unit
    val exp : t -> _ U.anypos -> unit
    val inf : t -> _ U.anypos -> unit
    val rem : t -> _ U.anypos -> unit
    val cas : t -> _ U.anypos -> unit
    val out : t -> _ U.anypos -> unit
  end


module type MATSET =
  sig
    module T : TRAIT

    type 'a group = 'a T.group

    val inc : mat -> 'a group -> 'b group -> _ U.anyfloat -> unit
    val set : mat -> 'a group -> 'b group -> _ U.anyfloat -> unit
  end


module type RATES =
  sig
    type t
  end


module type MODIFS =
  sig
    type t

    type 'a indiv_modif

    type pop_modif

    val infection_nonid : nonid indiv_modif

    val leave_exposed_nonid : pop_modif

    val recovery_nonid : pop_modif

    val immunity_loss : pop_modif

    val inf_inf_infection_id : pop_modif

    val inf_exp_infection_id : pop_modif

    val out_inf_infection_id : pop_modif

    val out_exp_infection_id : pop_modif

    val leave_exposed_id : pop_modif

    val recovery_id : nonid indiv_modif

  end


module type EVENTS =
  sig
    module T : TRAIT
    module P : (POP_WITH_ALL with type 'a trait = 'a T.t
                              and type 'a group = 'a T.group)
    module Get : (GET with type t = P.t)
    module Rates : (RATES with type t = P.t)
    module Modifs : (MODIFS with type t = P.t)
  end


module type MODEL_SPEC =
  sig
    val spec : model_spec
  end

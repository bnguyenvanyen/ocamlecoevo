open Sig


let model_name sp =
    "S"
  ^ (if sp.latency then "E" else "")
  ^ "IR"
  ^ (if sp.circular then "S" else "")


(*
let string_of_group =
  function
  | `Sus -> "`Sus"
  | `Exp -> "`Exp"
  | `Inf -> "`Inf"
  | `Rem -> "`Rem"
  | `Cas -> "`Cas"
  | `Out -> "`Out"
*)

(* Only beta *)
let fixed_infectivity par =
  let beta = Param.beta par in
  let f _ = beta
  in f


(* periodic with a mean value and a delta value *)
let var_infectivity par =
  let beta = Param.beta par in
  let betavar = Param.betavar par in
  let betamin = F.Pos.mul (F.Proba.compl betavar) beta in
  let dbeta = F.Pos.mul betavar beta in
  let freq = Param.freq par in
  let phase = Param.phase par in
  let f t =
    let t' = F.Op.(F.two * F.pi * (freq * t + phase)) in
    F.Pos.add betamin (F.Pos.bcos dbeta t')
  in f


(* FIXME not true with e ? *)
let reff infct par =
  F.Pos.Op.(infct / (Param.nu par))

(** SIR models for stochastic and deterministic simulations *)
open Sig


let model_spec = {
  latency = false ;
  circular = false ;
}


module Model =
  struct
    let spec = model_spec
  end

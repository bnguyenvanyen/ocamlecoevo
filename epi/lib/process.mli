open Sig


val infectivity :
  model_spec ->
  #Param.t_unit ->
  _ U.anyfloat ->
    _ U.pos


module Unit :
  sig
    module Make : functor (Model : MODEL_SPEC) ->
      sig
        type pop = Epi__Pop.Unit.t

        val specl :
          unit ->
            (string * #Param.t_unit Lift_arg.spec * string) list

        val of_tuple : int * int * int * int -> pop

        val of_sir : int * int * int -> pop

        val of_seir : int * int * int * int -> pop

        val to_tuple : pop -> float * float * float * float

        val to_sir : pop -> float * float * float

        val to_seir : pop -> float * float * float * float

        val sepir_to_seir :
          #Param.t_unit ->
          _ U.anypos * _ U.anypos * _ U.anypos ->
          _ U.pos * _ U.pos * _ U.pos * _ U.pos

        val conv :
          #Param.t_unit ->
          _ U.anyfloat ->
          pop ->
            Traj.point

        val line :
          #Param.t_unit ->
          ?k:int ->
          _ U.anyfloat ->
          pop ->
            string list

        module Csv :
          sig
            val header : append: bool -> string list list

            val convert :
              ?seed: int ->
              ?dt:_ U.anypos ->
              out: string ->
              t0:_ U.anyfloat ->
              Param.t_unit ->
              _ U.anyfloat ->
              pop ->
                (pop, U._float * pop, F._float) Sim.Sig.output

            val convert_append :
              ?seed: int ->
              ?dt:_ U.anypos ->
              out: string ->
              t0:_ U.anyfloat ->
              Param.t_unit ->
              _ U.anyfloat ->
              pop ->
                (int -> (pop, U._float * pop, F._float) Sim.Sig.output)
              * (unit -> unit)
          end

        module Cadlag :
          sig
            val convert :
              ?dt:_ U.anypos ->
              Param.t_unit ->
                (pop, (U._float * Traj.point) list, _ F.anyfloat) Sim.Sig.output
          end

        module Gill :
          sig
            val sim :
              output: (pop, 'a, F._float) Sim.Sig.output ->
              ?seed: int ->
              ?t0: _ U.anyfloat ->
              Param.t_unit ->
              pop ->
              _ U.anyfloat ->
                'a
          end

        module Prm :
          sig
            module type S =
              (Sim.Sig.PRM_MINI
                 with type color = Color.t
                  and type 's Point.t = (Color.t, 's) Sim.Prm.point
              )

            val colors : Color.t list

            val color_groups : Color.t list list

            val csv_convert :
              (module S with type t = 'a) ->
              ?seed: int ->
              ?dt: _ U.anypos ->
              output_grid: bool ->
              out: string ->
              t0:_ U.anyfloat ->
              Param.t_unit ->
              _ U.anyfloat ->
              pop ->
              'a ->
                (U._float, pop, U._float * pop) U.Out.t
          end

        module type SIM_PRM =
          sig
            type prm

            val sim :
              output: (pop, 'a, F._float) Sim.Sig.output ->
              Param.t_unit ->
              pop ->
              prm ->
                'a

            val sim_csv :
              ?seed:int ->
              ?dt:_ U.anypos ->
              nosim:bool ->
              out:string ->
              output_grid:bool ->
              t0:_ U.anyfloat ->
              Param.t_unit ->
              _ U.anyfloat ->
              pop ->
              prm ->
                U._float * pop

            val rand_draw_prm :
              ?seed:int ->
              width:_ U.anypos ->
              time_range:(_ U.anypos * _ U.anypos) ->
              ntslices:U.anyposint ->
                prm

            val read_prm :
              time_range:(_ U.anypos * _ U.anypos) ->
              ntslices:U.anyposint ->
              string ->
                prm option

            val read_or_draw_prm :
              width:_ U.anypos ->
              time_range:(_ U.anypos * _ U.anypos) ->
              ntslices:U.anyposint ->
              int option ->
              string option ->
                prm

            val many_sim_csv :
              niter:int ->
              ?seed:int ->
              ?dt:_ U.anypos ->
              out:string ->
              width:_ U.anypos ->
              time_range:(_ U.anypos * _ U.anypos) ->
              ntslices:U.anyposint ->
              t0:_ U.anyfloat ->
              Param.t_unit ->
              _ U.anyfloat ->
              pop ->
                unit
          end
          
        module Prm_approx : (SIM_PRM with type prm = Ctmjp_prm_approx.P.t)
            
        module Prm_fast : (SIM_PRM with type prm = Ctmjp_prm_fast.P.t)

        module Prm_exact : (SIM_PRM with type prm = Ctmjp_prm_exact.P.t)
      end
  end


module Continuous :
  sig
    module Make : functor (Model : MODEL_SPEC) ->
      sig
        type pop = Lac.Vec.t

        val dims : int

        val n_events : int

        val specl :
          unit ->
            (string * #Param.t_unit Lift_arg.spec * string) list

        val of_sir :
          ?z:Lac.Vec.t ->
          _ U.anypos * _ U.anypos * _ U.anypos ->
            pop

        val of_seir :
          ?z:Lac.Vec.t ->
          _ U.anypos * _ U.anypos * _ U.anypos * _ U.anypos ->
            pop

        val to_sir : pop -> float * float * float

        val to_seir : pop -> float * float * float * float

        val to_tuple : pop -> float * float * float * float

        val sepir_to_seir :
          #Param.t_unit ->
          _ U.anypos * _ U.anypos * _ U.anypos ->
          _ U.pos * _ U.pos * _ U.pos * _ U.pos

        val color_namer : (string -> int) * (int -> string)
       
        val conv :
          #Param.t_unit ->
          _ U.anyfloat ->
          pop ->
            Traj.point

        val line :
          #Param.t_unit ->
          ?k:int ->
          _ U.anyfloat ->
          pop ->
            string list

        module Csv :
          sig
            val convert :
              ?dt: _ U.anypos ->
              out: string ->
              t0:_ U.anyfloat ->
              Param.t_unit ->
              _ U.anyfloat ->
              pop ->
                (pop, U._float * pop, F._float) Sim.Sig.output

            val convert_append :
              ?dt: _ U.anypos ->
              out: string ->
              t0:_ U.anyfloat ->
              Param.t_unit ->
              _ U.anyfloat ->
              pop ->
                (int -> (pop, U._float * pop, F._float) Sim.Sig.output)
              * (unit -> unit)
          end

        module Cadlag :
          sig
            val convert :
              ?dt:_ U.anypos ->
              Param.t_unit ->
                (pop, (U._float * Traj.point) list, _ F.anyfloat) Sim.Sig.output
          end

        module Ode :
          sig
            val sim :
              output: (pop, 'a, F._float) Sim.Sig.output ->
              ?apar: Sim.Ode.Lsoda.algparam ->
              ?t0:_ U.anyfloat ->
              Param.t_unit ->
              pop ->
              _ U.anyfloat ->
                'a
          end

        module Sde :
          sig
            val sim :
              output: (pop, 'a, F._float) Sim.Sig.output ->
              Param.t_unit ->
              pop ->
              Sim.Dbt.t ->
                'a
          end

        module Sde_limit :
          sig
            val sim :
              output: (pop, 'a, F._float) Sim.Sig.output ->
              Param.t_unit ->
              pop ->
              Sim.Dbt.t ->
                'a
          end
      end
  end


module Seq :
  sig
    val empty_record : (float * Seqs.Diff.t) list

    module Make : functor (Model : MODEL_SPEC) ->
      sig
        type pop = Epi__Pop.Seq.t

        val specl :
          unit ->
            (string * #Param.t_seq Lift_arg.spec * string) list

        val of_tuple :
            int
          * (int * Seqs.t) list
          * (int * Seqs.t) list
          * int
          * (int * Seqs.t) list ->
            pop

        val of_sir : int * (int * Seqs.t) list * int -> pop

        val of_seiro :
            int
          * (int * Seqs.t) list
          * (int * Seqs.t) list
          * int
          * (int * Seqs.t) list ->
            pop

        val to_tuple : pop -> float * float * float * float


        module Gill :
          sig
            val sim :
              output: (pop, 'a, F._float) Sim.Sig.output ->
              ?seed: int ->
              Param.t_seq ->
              ?t0: _ U.anypos ->
              pop ->
              _ U.anypos ->
                'a
          end

        module Csv :
          sig
            val convert :
              ?seed: int ->
              ?dt: _ U.anypos ->
              channels: Seqsim.Seqpop.channels ->
              t0:_ U.anyfloat ->
              Param.t_seq ->
              _ U.anyfloat ->
              pop ->
                (pop, U._float * pop, F._float) Sim.Sig.output
          end
      end
  end

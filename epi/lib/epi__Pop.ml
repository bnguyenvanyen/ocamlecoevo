
(* FIXME also need to go through With_events ? *)

module Unit = Pop.With_events.Augment (
  Pop.With_dummy_id.Augment (
    Pop.Nonid.Make (Trait.Unit)
  )
)


module Seq = Pop.With_events.Make (Trait.Seq)

open Sig

module J = Jump
module Reg = J.Regular


let bin_cases =
  J.Int.diff


let bin_reportr r =
  r
  |> J.Pos.areas


let bin_at times prim =
  let _, bins = L.fold_left_map (fun t t' ->
      let diff = F.Pos.of_anyfloat F.Op.(
        Reg.Pos.eval prim t' - Reg.Pos.eval prim t
      )
      in
      (t', (t', diff))
    ) (Reg.t0 prim) times
  in
  J.of_list bins


let bin_reportr_at ?tol times r =
  r
  |> J.Regular.of_cadlag ?tol
  |> J.Regular.Pos.primitive
  |> bin_at times


let case_data traj =
  traj
  |> Traj.cumulative_cases
  |> J.map (fun c -> int_of_float (F.to_float c))
  |> bin_cases


let binned_reportr_data traj =
  traj
  |> Traj.reporting_rate
  |> bin_reportr


let binned_reportr_data_at ?tol times traj =
  traj
  |> Traj.reporting_rate
  |> bin_reportr_at ?tol times


let noisy_reportr_data ~rng ovd traj =
  traj
  |> binned_reportr_data
  |> J.map (U.rand_negbin_over ~rng ovd)
  |> J.map I.to_int


module type PBINO =
  sig
    val rand_binomial :
      rng:U.rng ->
      U.anyposint ->
      U.anyproba ->
        int
  end


let prevalence_data (module P : PBINO) ~rng rho traj =
  traj
  |> Traj.infected
  |> J.map (fun i -> P.rand_binomial ~rng (I.Pos.ceil i) rho)


let expected_prevalence_data rho traj =
  traj
  |> Traj.infected
  |> J.map (fun i -> F.Pos.mul rho i)


let sero_agg_data (module P : PBINO) ~rng seros traj =
  let cr = Traj.removed_proportion traj in
  J.of_list (L.map (fun (t, n) ->
    let r = Jump.eval cr t in
    (t, (P.rand_binomial ~rng n r, n))
  ) seros)


let expected_sero_agg_data seros traj =
  let cr = Traj.removed_proportion traj in
  J.of_list (L.map (fun (t, n) ->
    let r = Jump.eval cr t in
    (t, (F.Pos.mul r (I.Pos.to_float n), n))
  ) seros)

let neff_data traj =
  Traj.expected_neff traj


let load_cases path =
  let f =
    function
    | "t" ->
        Some (fun s (_, c) -> (float_of_string s, c))
    | "cases" ->
        Some (fun s (t, _) -> (t, int_of_float (float_of_string s)))
    | _ ->
        None
  in
  match U.Csv.read_all_and_fold ~f ~path (0., 0) with
  | res ->
      Some (J.of_list res)
  | exception Sys_error _ ->
      None

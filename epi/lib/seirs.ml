(** SEIRS models *)
open Sig


let model_spec = {
  latency = true ;
  circular = true ;
}


module Model =
  struct
    let spec = model_spec
  end

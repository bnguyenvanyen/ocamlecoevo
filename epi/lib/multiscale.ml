
module F = Util.Float
module I = Util.Int


module Param =
  struct
    type t = {
      bh : float ;  (** rate of immigration of new (naïve) hosts *)
      bi : float ;  (** birth rate of immune cells per virus *)
      bv : float ;  (** birth rate of viruses *)
      dh : float ;  (** rate of natural death of hosts *)
      di : float ;  (** death rate of immune cells *)
      kiv : float ; (** killing rate of viruses by immune cells *)
      kvh : float ; (** killing rate of hosts by viruses *)
      vsat : float ; (** parameterizes the infection probability on contact *)
      kappa : float ; (** contact rate between hosts *)
      mu : float ;  (** mutation rate of viruses *)
    }

    let birth_host p = F.Pos.of_float p.bh
    let birth_immune p = F.Pos.of_float p.bi
    let birth_virus p = F.Pos.of_float p.bv
    let death_host p = F.Pos.of_float p.dh
    let death_immune p = F.Pos.of_float p.di
    let kill_virus p = F.Pos.of_float p.kiv
    let kill_host p = F.Pos.of_float p.kvh
  end


module Trait =
  struct
    open Pop.Sig

    (* we sort of cheat : we give host ids ourselves *)
    type _ t =
      | Host : int -> id t
      | Immune : int -> nonid t
      | Virus : int * float -> id t

    type _ group =
      | H : id group
      | I : int -> nonid group
      | V : int -> id group

    let group_of : type a. a t -> a group =
      function
      | Host _ -> H
      | Immune i -> I i
      | Virus (i, _) -> V i

    let of_group =
      function
      | I i -> Immune i

    let copy x = x
  end


module Events
  (P : Pop.Sig.POP_WITH_ALL
       with type 'a isid = 'a
        and type 'a trait = 'a Trait.t
        and type 'a group = 'a Trait.group) =
  struct
    type 'a trait = 'a Trait.t
    type 'a group = 'a Trait.group

    let host_influx_rate par _ _ =
      Param.birth_host par

    let host_natdeath_rate par _ z =
      F.Pos.mul
       (Param.death_host par)
       (I.Pos.to_float (P.count ~group:Trait.H z))

    (* broken...
    let host_influx_modif par _ t z =
      let i = P.new_ident z in
      P.add_descent t z None (Some i) (Host i)

    let host_influx = (host_influx_rate, host_influx_modif)

    let host_natdeath_modif par rng t z =
      match P.choose rng z H with
      | None -> failwith "Host_natdeath_modif"
      | Some i ->
        (* FIXME implement a "MULTIPOP.remove_group" to make this easier *)
        (* get the immune pop of the host and remove it *)
        let n = P.count z (I i) in
        remove_k t z (Immune i) n ;
        (* get the viral pop of the host and remove it *)
        remove_group t z (V i) ;
        (* remove the host *)
        P.remove t z (Some i) (Host i)

    let host_natdeath = (host_natdeath_rate, host_natdeath_modif)

    (* let immune_births *)

    let evl = [host_influx ; host_natdeath]
    *)
  end

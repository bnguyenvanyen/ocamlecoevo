open Sig


(* we gather all rates / part modifs / modifs here
 * that we end up needing with names as descriptive as we can *)
module Make_partial
  (T : TRAIT)
  (P : Pop.Sig.POP_WITH_EVENTS
       with type 'a isid = 'a
        and type 'a trait = 'a T.t
        and type 'a group = 'a T.group) =
  struct
    module G = Get.Make (T) (P)

    module Rates_partial = Rates.Make (G)

    (* trait modifs that just convert from one group to another *)

    let i_to_e _ _ = T.(
      function
      | I y -> E y
      | _ -> failwith "not `I"
    )

    let e_to_i _ _ = T.(
      function
      | E y -> I y
      | _ -> failwith "not E"
    )

    let i_to_o _ _ = T.(
      function
      | I y -> O y
      | _ -> failwith "not I"
    )

    let o_to_i _ _ = T.(
      function
      | O y -> I y
      | _ -> failwith "not O"
    )

    let o_to_e _ _ = T.(
      function
      | O y -> E y
      | _ -> failwith "not O"
    )
  
    let mut_first mut = 
      Some (fun stoch t x _ -> mut stoch t x)

    module Modifs_partial =
      struct
        (* a lot of (m rng t z) are scattered around,
         * this is just for eta-expansion
         * (generalization of anonymous variables) *)

        module SCU = Sim.Ctmjp.Util

        let ifg = P.indiv_from_group

        let no_case_proba par _ _ =
          F.Proba.compl (Param.rho par)

        let immunity_loss stoch t z =
          let m = SCU.chain
            (P.death_modif (ifg T.Rem))
            (P.birth_modif (ifg T.Sus))
          in m stoch t z

        let infection_from_ind ~mut ind =
          P.pair_bd_modif ~mut ind (ifg T.Sus)

        let inf_inf_infection stoch t z =
          let ind = P.choose ~rng:stoch ~group:T.Inf z in
          infection_from_ind ~mut:None ind stoch t z

        let inf_exp_infection stoch t z =
          let ind = P.choose ~rng:stoch ~group:T.Inf z in
          infection_from_ind ~mut:(mut_first i_to_e) ind stoch t z

        let out_inf_infection stoch t z =
          let ind = P.choose ~rng:stoch ~group:T.Out z in
          infection_from_ind ~mut:(mut_first o_to_i) ind stoch t z

        let out_exp_infection stoch t z =
          let ind = P.choose ~rng:stoch ~group:T.Out z in
          infection_from_ind ~mut:(mut_first o_to_e) ind stoch t z

        let inf_inf_infection_and case =
          SCU.chain inf_inf_infection case

        let out_inf_infection_and case =
          SCU.chain out_inf_infection case

        let leave_exposed_on_ind ind =
          P.pair_bd_modif ~mut:(mut_first e_to_i) ind ind

        let leave_exposed stoch t z =
          try
            let ind = P.choose ~rng:stoch ~group:T.Exp z in
            leave_exposed_on_ind ind stoch t z
          with Not_found ->
            z

        let leave_exposed_and case =
          SCU.chain leave_exposed case

        let record_proba par _ _ =
          Param.p_keep par

        (*
        let no_record_proba par _ _ =
          F.Proba.compl (Param.p_keep par)
        *)

        let recovery_of_ind ind =
          P.pair_bd_modif ~mut:None (ifg T.Rem) ind

        let recovery stoch t z =
          try
            let ind = P.choose ~rng:stoch ~group:T.Inf z in
            recovery_of_ind ind stoch t z
          with Not_found ->
            z

        let outseed_from_ind ind =
          P.birth_modif ~mut:i_to_o ind

        let no_outseed_proba par _ _ =
          F.Proba.compl (Param.p_outseed par)

        let maybe_outseed par ind =
          SCU.erase (no_outseed_proba par) (outseed_from_ind ind)

        let out_death stoch t z =
          let ind = P.choose ~rng:stoch ~group:T.Out z in
          P.death_modif ind stoch t z

        let case stoch =
          P.birth_modif (ifg T.Cas) stoch

        (* just identity *)
        let no_case _ _ _ z =
          z

        let maybe_case_rng par =
          SCU.erase (no_case_proba par) case

        let maybe_case_pt par =
          SCU.erase_point (no_case_proba par) case

        let host_birth stoch =
          P.birth_modif (ifg T.Sus) stoch

        let host_death group stoch t z =
          try
            let ind = P.choose ~rng:stoch ~group z in
            P.death_modif ind stoch t z
          with Not_found ->
            z

        let sus_host_death stoch =
          host_death T.Sus stoch

        let exp_host_death stoch =
          host_death T.Exp stoch

        let inf_host_death stoch =
          host_death T.Inf stoch

        let rem_host_death stoch =
          host_death T.Rem stoch
      end

  module Out = Rates_partial.Out
end


module Unit =
  struct
    module T = Trait.Unit
    module P = Epi__Pop.Unit

    include Make_partial (Trait.Unit) (Epi__Pop.Unit)

    module Rates = Rates_partial

    module Modifs = Modifs_partial
  end


module Seq = 
  struct
    module T = Trait.Seq
    module P = Epi__Pop.Seq

    include Make_partial (T) (P)

    module Rates =
      struct
        include Rates_partial

        let mutation (grp : Pop.Sig.id P.group) mut_rate par t z =
          let f y =
            let n = I.Pos.to_float (P.count ~group:grp z) in
            F.Pos.mul n (mut_rate par#evo t y)
          in T.(
            match P.max ~i:0 z with
            | _, E y ->
                f y
            | _, I y ->
                f y
            | _, O y ->
                f y
          )
      end

    module Modifs =
      struct
        include Modifs_partial

        (* This needs WITH_GENEALOGY - split in another functor ? *)
        let maybe_record par ind rng t z =
          let m = SCU.choose_between
              (record_proba par)
              (fun _ _ z -> P.forbid_erase z ind ; z)
              (fun _ _ z -> P.erase z ind ; z)
          in
          (*
          let m = SCU.erase
              (no_record_proba par)
              (fun _ _ z -> P.erase z ind ; z)
          in
          *)
          m rng t z

        let case_inf_inf_infection par =
          inf_inf_infection_and (maybe_case_rng par)

        let case_out_inf_infection par =
          out_inf_infection_and (maybe_case_rng par)

        let case_leave_exposed par =
          leave_exposed_and (maybe_case_rng par)

        (* choose an 'Inf', remove it and record it, then add a 'R' *)
        let record_recovery par rng t z =
          let ind = P.choose ~rng ~group:T.Inf z in
          let m = SCU.chain (recovery_of_ind ind) (maybe_record par ind) in
          m rng t z

        (* This can only exist when idor = id *)
        let mutation_on_ind mut' par ind stoch t z =
          let mutate stoch t (x : Pop.Sig.id P.trait) = T.(
            let f y =
              mut' par#evo stoch t y
            in
            match x with
            | E y ->
                E (f y)
            | I y ->
                I (f y)
            | O y ->
                O (f y)
          )
          in
          let mut = mut_first mutate in
          P.pair_bd_modif ~mut ind ind stoch t z

        let mutation group mut par rng t z =
          let ind = P.choose ~rng ~group z in
          mutation_on_ind mut par ind rng t z

        (*
        let out_death rng t z =
          let ind = P.choose ~rng ~group:T.Out z in
          P.death_modif ind rng t z
        *)

        let record_outseed_recovery par rng t z =
          let ind = P.choose ~rng ~group:T.Inf z in
          let m =
            SCU.chain
              (SCU.chain (recovery_of_ind ind) (maybe_outseed par ind))
              (maybe_record par ind)
          in m rng t z
    end
  end

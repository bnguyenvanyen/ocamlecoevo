(** @canonical Epi.Sig *)
module Sig = Sig

(** @canonical Epi.Seirco *)
module Seirco = Seirco

(** @canonical Epi.Param *)
module Param = Param

(** @canonical Epi.Pop *)
module Pop = Epi__Pop

(** @canonical Epi.Color *)
module Color = Color

(** @canonical Epi.Point *)
module Point = Point

(** @canonical Epi.Prm_time *)
module Prm_time = Prm_time

(** @canonical Epi.Prm_color *)
module Prm_color = Prm_color

(** @canonical Epi.Prm_cheat *)
module Prm_cheat = Prm_cheat

(** @canonical Epi.Ctmjp_prm_exact *)
module Ctmjp_prm_exact = Ctmjp_prm_exact

(** @canonical Epi.Ctmjp_prm_approx *)
module Ctmjp_prm_approx = Ctmjp_prm_approx

(** @canonical Epi.Default_init *)
module Default_init = Default_init

(** @canonical Epi.Specs *)
module Specs = Specs

(** @canonical Epi.Read *)
module Read = Read

(** @canonical Epi.Term *)
module Term = Term

(** @canonical Epi.Cli *)
module Cli = Cli

(** @canonical Epi.Rates *)
module Rates = Rates

(** @canonical Epi.Process *)
module Process = Process

(** @canonical Epi.Sir *)
module Sir = Sir

(** @canonical Epi.Seir *)
module Seir = Seir

(** @canonical Epi.Sirs *)
module Sirs = Sirs

(** @canonical Epi.Seirs *)
module Seirs = Seirs

(** @canonical Epi.Panic *)
module Panic = Panic

(** @canonical Epi.Traj *)
module Traj = Traj

(** @canonical Epi.Data *)
module Data = Data

(** @canonical Epi.Map_csv *)
module Map_csv = Epi__Map_csv

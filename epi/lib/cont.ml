open Sig


module Make (P : sig
    type t = vec
    module T : TRAIT
    module Get : (GET with type t = t)
    module Set : (SET with type t = t)
    module Mat : (MATSET with module T = T)
  end) =
  struct
    (* write the f components to build a f from
     * give a general domain thing
     * event matrixes and do the sum ?
     * or matrix modification and fold -- seems more efficient *)
    
    module R = Rates.Make (P.Get)

    module Modifs =
      struct
        let immunity_loss z =
          P.Set.sus z F.one ;
          P.Set.rem z F.neg_one ;
          z

        let inf_inf_infection z =
          P.Set.sus z F.neg_one ;
          P.Set.inf z F.one ;
          z

        let inf_exp_infection z =
          P.Set.sus z F.neg_one ;
          P.Set.exp z F.one ;
          z

        let leave_exposed z =
          P.Set.exp z F.neg_one ;
          P.Set.inf z F.one ;
          z

        let recovery z =
          P.Set.inf z F.neg_one ;
          P.Set.rem z F.one ;
          z

        let host_birth z =
          P.Set.sus z F.one ;
          z

        let sus_host_death z =
          P.Set.sus z F.neg_one ;
          z

        let exp_host_death z =
          P.Set.exp z F.neg_one ;
          z

        let inf_host_death z =
          P.Set.inf z F.neg_one ;
          z

        let rem_host_death z =
          P.Set.rem z F.neg_one ;
          z
      end

    let is_in_domain ?n y =
      match Lac.Vec.iter (fun x ->
        assert (x >= 0.)) ?n y
      with
      | () ->
          true
      | exception Assert_failure _ ->
          false

   
    (* here z and y cannot be the same *)
    let conserve_back_to_domain dims ~result _ y =
      for i = 1 to dims do
        if y.{i} < 0. then begin
          let a = y.{i} /. float (dims - 1) in
          for j = 1 to (i - 1) do
            result.{j} <- y.{j} +. a
          done ;
          for j = (i + 1) to dims do
            result.{j} <- y.{j} +. a
          done ;
          result.{i} <- 0.
        end
      done

    let domain dims = Sim.Sig.{
      is_in = is_in_domain ~n:dims ;
      back_to =
        (* conserve_back_to_domain dims *)
        Sim.Domain.Vec.reflect_back_to_pos dims
      ;
    }

    module Out = R.Out
  end


(** Useful functions for an interface *)

open Sig

module La = Lift_arg
module Di = Default_init


let load_par_row =
  Read.Reader.load


(* read the k-th row, or the last one *)
let load_par_from reads theta ?k s =
  let csv = Csv.Rows.load ~separator:',' ~has_header:true s in
  let row =
    match k with
    | None ->
        csv |> L.rev |> L.hd
    | Some k ->
        (* we would like to check that the column "k" exists *)
        L.find (fun row ->
          int_of_string (Csv.Row.find row "k") = k
        ) csv
  in
  load_par_row reads row theta




module Unit =
  struct
    class param = Param.t_unit

    class init = object
      inherit param
      inherit Param.t_able
      inherit Param.dt_able
    end


    module Gill =
      struct
        let cv s =
          s |> float_of_string |> int_of_float

        class immediate_init = object
          inherit init
          inherit Param.seed_able
          inherit Param.init_sir_int
        end

        class latent_init = object
          inherit init
          inherit Param.seed_able
          inherit Param.init_seir_int
        end

        module Make (Model : MODEL_SPEC) =
          struct
            module E = Process.Unit.Make (Model)

            let reads = Read.Reader.(
                [s0 cv]
              @ (if Model.spec.latency then [e0 cv] else [])
              @ [i0 cv ; r0 cv ; seed]
              @ (if Model.spec.circular then circular () else [])
              @ (if Model.spec.latency then latent () else [])
              @ base ()
            )

            (* FIXME would like to also pass ?k somehow *)
            let load_par s x =
              load_par_from reads s x

            (* Here we would like to distinguish between the case
             * with and without latency,
             * but we actually don't need to,
             * since init_seir_int is a subtype of init_sir_int *)
            let init_r = ref (new latent_init)

            let out_r = ref None

            let specl = Specs.(
                [ La.spec_of out_r out_string ]
              @ La.specl_of init_r [
                dt ;
                seed ;
                t0 ;
                tf ;
                s0_int
              ]
              @ La.specl_of init_r (
                if Model.spec.latency then [e0_int] else []
              )
              @ La.specl_of init_r [
                i0_int ;
                r0_int ;
                (init_from load_par) ;
            ]) @ (La.specl_of init_r (E.specl ()))

            
            let anon_fun s =
              Printf.printf "Ignored anonymous argument : %s" s

            let model_name = Seirco.model_name Model.spec

            let usage_msg =
                " Run a stochastic simulation of a "
              ^ model_name 
              ^ " system, with the Gillespie algorithm."

            let sim ?niter init =
              begin
                let seed = init#seed in
                let dt = Param.dt init in
                let t0 = Param.t0 init in
                let tf = Param.tf init in
                let par = (init :> param) in
                let out =
                  match !out_r with
                  | None -> raise (Arg.Bad "-out argument is required")
                  | Some s -> s
                in
                let z0 =
                  if Model.spec.latency then
                    E.of_seir init#seir
                  else
                    E.of_sir init#sir
                in
                match niter with
                | None ->
                    let output =
                      E.Csv.convert ?seed ?dt ~out ~t0 par tf z0
                    in
                    ignore (E.Gill.sim ?seed ~output ~t0 par z0 tf)
                | Some n ->
                    let output, close =
                      E.Csv.convert_append ?seed ?dt ~out ~t0 par tf z0
                    in
                    let rng = U.rng seed in
                    for k = 1 to n do
                      Printf.printf "Simulation %i\n%!" k ;
                      let output = output k in
                      let seed = Random.State.bits rng in
                      ignore (E.Gill.sim
                        ~seed
                        ~output
                        ~t0
                        par
                        (Epi__Pop.Unit.copy z0)
                        tf
                      )
                    done ;
                    close ()
              end

            let run () =
              begin
                Arg.parse specl anon_fun usage_msg ;
                let init = !init_r in
                sim init
              end


            let niter_r = ref 1

            let specl_many =
                 (Specs.niter niter_r)
              :: specl

            let run_many () =
              begin
                Arg.parse specl_many anon_fun usage_msg ;
                let init = !init_r in
                let niter = !niter_r in
                sim ~niter init
              end
          end
      end


    module Prm =
      struct
        let cv = Gill.cv

        class latent_init = object
          inherit Gill.latent_init
          inherit Param.prm_width_able
        end

        module Make (Model : MODEL_SPEC) =
          struct
            module E = Process.Unit.Make (Model)

            type 'a mod_prm = (module E.SIM_PRM with type prm = 'a)

            type e_mod_prm = E : 'a mod_prm -> e_mod_prm

            let reads = Read.Reader.(
                [s0 cv]
              @ (if Model.spec.latency then [e0 cv] else [])
              @ [i0 cv ; r0 cv ; seed]
              @ (if Model.spec.circular then circular () else [])
              @ (if Model.spec.latency then latent () else [])
              @ base ()
            )

            let load_par ipar s =
              load_par_from reads ipar s

            (* Here we would like to distinguish between the case
             * with and without latency,
             * but we actually don't need to,
             * since init_seir_int is a subtype of init_sir_int *)
            let init_r = ref (new latent_init)

            let input_prm_r = ref None

            let out_r = ref None

            let exact_prm_r = ref false

            let output_grid_r = ref false

            let no_sim_r = ref false

            let specl = Specs.(
                [ La.spec_of out_r out_string ]
              @ [ exact_prm exact_prm_r ]
              @ La.specl_of init_r [
                dt ;
                prm_width ;
                seed ;
                t0 ;
                tf ;
                s0_int
              ]
              @ La.specl_of init_r (
                if Model.spec.latency then [e0_int] else []
              )
              @ La.specl_of init_r [
                i0_int ;
                r0_int ;
                (init_from load_par) ;
            ]) @ (La.specl_of init_r (E.specl ()))

            
            let anon_fun s =
              Printf.printf "Ignored anonymous argument : %s" s

            let model_name = Seirco.model_name Model.spec

            let usage_msg =
                " Run a stochastic simulation of a "
              ^ model_name 
              ^ " system, with the Prm integrator."

            let specl_single =
              Specs.(
                [ La.spec_of input_prm_r init_prm_from ]
              @ [ output_grid output_grid_r ]
              @ [ no_sim no_sim_r ]
              @ specl
              )

            let run () =
              begin
                Arg.parse specl_single anon_fun usage_msg ;
                let init = !init_r in
                let dt = Param.dt init in
                let width =
                  match Param.prm_width init with
                  | pos ->
                      pos
                  | exception Invalid_argument _ ->
                      raise (Arg.Bad "must supply a positive '-width argument.")
                in
                let t0 = Param.t0 init in
                let tf = Param.tf init in
                let par = (init :> param) in
                let out =
                  match !out_r with
                  | None ->
                      raise (Arg.Bad "missing required '-out' argument.")
                  | Some s ->
                      s
                in
                let z0 =
                  if Model.spec.latency then
                    E.of_seir init#seir
                  else
                    E.of_sir init#sir
                in
                (* prm *)
                let seed = init#seed in
                let mod_sim_prm =
                  if !exact_prm_r then
                    (module E.Prm_exact : E.SIM_PRM)
                  else
                    (module E.Prm_approx : E.SIM_PRM)
                in
                let module Sim_Prm = (val mod_sim_prm : E.SIM_PRM) in
                let outsim prm = Sim_Prm.sim_csv
                  ?seed
                  ?dt
                  ~nosim:!no_sim_r
                  ~out
                  ~output_grid:!output_grid_r
                  par
                  tf
                  z0
                  prm
                in
                let input_prm = !input_prm_r in
                let time_range = (t0, F.Pos.of_anyfloat F.Op.(tf - t0)) in
                let ntslices = Point.ntslices ~width tf in
                let prm =
                  Sim_Prm.read_or_draw_prm ~width ~time_range ~ntslices seed input_prm 
                in
                outsim prm
              end

            let niter_r = ref 1

            let specl_many =
                 Specs.niter niter_r
              :: specl

            let run_many () =
              begin
                Arg.parse specl_many anon_fun usage_msg ;
                let init = !init_r in
                let niter = !niter_r in
                let dt = Param.dt init in
                let width =
                  match Param.prm_width init with
                  | pos ->
                      pos
                  | exception Invalid_argument _ ->
                      raise (Arg.Bad "must supply a positive '-width' argument.")
                in
                let t0 = Param.t0 init in
                let tf = Param.tf init in
                let par = (init :> param) in
                let out =
                  match !out_r with
                  | None ->
                      raise (Arg.Bad "missing required '-out' argument.")
                  | Some s ->
                      s
                in
                let z0 =
                  if Model.spec.latency then
                    E.of_seir init#seir
                  else
                    E.of_sir init#sir
                in
                (* prm *)
                let seed = init#seed in
                let time_range = (t0, F.Pos.of_anyfloat F.Op.(tf - t0)) in
                let ntslices = Point.ntslices ~width tf in
                let mod_sim_prm =
                  if !exact_prm_r then
                    (module E.Prm_exact : E.SIM_PRM)
                  else
                    (module E.Prm_approx : E.SIM_PRM)
                in
                let module Sim_Prm = (val mod_sim_prm : E.SIM_PRM) in
                Sim_Prm.many_sim_csv
                  ~niter
                  ?seed
                  ?dt
                  ~out
                  ~width
                  ~time_range
                  ~ntslices
                  par
                  tf
                  z0
              end

          end
      end


    module Ode =
      struct
        let cv = float_of_string

        class immediate_init = object
          inherit init
          inherit Param.lsoda_able
          inherit Param.init_sir_float
        end

        class latent_init = object
          inherit init
          inherit Param.lsoda_able
          inherit Param.init_seir_float
        end

        module Make (Model : MODEL_SPEC) =
          struct
            module Cont = Process.Continuous.Make (Model)

            let reads = Read.Reader.(
                [s0 cv]
              @ (if Model.spec.latency then [e0 cv] else [])
              @ [i0 cv ; r0 cv]
              @ (if Model.spec.circular then circular () else [])
              @ (if Model.spec.latency then latent () else [])
              @ base ()
            )

            let load_par s x =
              load_par_from reads s x

            let init_r = ref (new latent_init)

            let out_r = ref None

            let dt_print_r = ref 0.

            (* let apar_r = ref Sim.Ode.Lsoda.default *)

            let specl = Specs.(
                [ La.spec_of out_r out_string ]
              @ La.specl_of init_r [
                dt ;
                t0 ;
                tf ;
                s0_float ;
              ]
              @ La.specl_of init_r
                (if Model.spec.latency then [e0_float] else [])
              @ La.specl_of init_r [
                i0_float ;
                r0_float ;
                (init_from load_par) ;
              ]
              @ La.specl_of init_r (lsoda_par ())
              ) @ (La.specl_of init_r (Cont.specl ()))
                 (* @ (La.specl_of apar_r Sim.Ode.Lsoda.specl) *)

            let anon_fun s =
              Printf.printf "Ignored anonymous argument : %s" s

            let model_name = Seirco.model_name Model.spec

            let usage_msg =
                " Run a deterministic simulation of a "
              ^ model_name
              ^ " system."

            let run () =
              begin
                Printf.eprintf "Simulate %s as a ODE with Lsoda\n" model_name ;
                Arg.parse specl anon_fun usage_msg ;
                let init = !init_r in
                let apar = init#lsoda_par in
                let dt = Param.dt init in
                let par = (init :> param) in
                let t0 = Param.t0 init in
                let tf = Param.tf init in
                let out =
                  match !out_r with
                  | None -> raise (Arg.Bad "-out argument is required")
                  | Some s -> s
                in
                let z0 =
                  if Model.spec.latency then
                    Cont.of_seir (Param.seir init)
                  else
                    Cont.of_sir (Param.sir init)
                in
                let output = Cont.Csv.convert ?dt ~out ~t0 par tf z0 in
                Cont.Ode.sim ~output ~apar ~t0 par z0 tf
              end
          end
      end

    module Sde_limit =
      struct
        let cv = Ode.cv

        class latent_init = object
          inherit init
          inherit Param.seed_able
          inherit Param.dbt_width_able
          inherit Param.init_seir_float
        end

        module Make (Model : MODEL_SPEC) =
          struct
            module Cont = Process.Continuous.Make (Model)

            let reads = Read.Reader.(
                [s0 cv]
              @ (if Model.spec.latency then [e0 cv] else [])
              @ [i0 cv ; r0 cv]
              @ (if Model.spec.circular then circular () else [])
              @ (if Model.spec.latency then latent () else [])
              @ base ()
            )

            let load_par s x =
              load_par_from reads s x

            let init_r = ref (new latent_init)

            let out_r = ref None

            let dt_print_r = ref 0.

            let specl = Specs.(
                [ La.spec_of out_r out_string ]
              @ La.specl_of init_r [
                dt ;
                t0 ;
                tf ;
                dbt_width ;
                seed ;
                s0_float ;
              ]
              @ La.specl_of init_r
                (if Model.spec.latency then [e0_float] else [])
              @ La.specl_of init_r [
                i0_float ;
                r0_float ;
                (init_from load_par) ;
              ]
              ) @ (La.specl_of init_r (Cont.specl ()))

            let anon_fun s =
              Printf.printf "Ignored anonymous argument : %s" s

            let model_name = Seirco.model_name Model.spec

            let usage_msg =
                " Run a diffusion limit simulation of a "
              ^ model_name
              ^ " system."

            let sim ?niter init =
              begin
                let dt = Param.dt init in
                (* will need a width *)
                let width =
                  match Param.dbt_width init with
                  | pos ->
                      pos
                  | exception Invalid_argument _ ->
                      raise (Arg.Bad "must supply a positive '-width' argument.")
                in
                let t0 = Param.t0 init in
                let tf = Param.tf init in
                let par = (init :> param) in
                let out =
                  match !out_r with
                  | None ->
                      raise (Arg.Bad "missing required '-out' argument.")
                  | Some s ->
                      s
                in
                let z0 =
                  if Model.spec.latency then
                    Cont.of_seir (Param.seir init)
                  else
                    Cont.of_sir (Param.sir init)
                in
                match niter with
                | None ->
                    let output = Cont.Csv.convert ?dt ~out ~t0 par tf z0 in
                    (* dimension the number of events *)
                    let dbts =
                      Sim.Dbt.rand_draw
                      ~rng:(U.rng init#seed)
                      ~t0
                      ~dt:width
                      ~tf
                      ~n:Cont.n_events
                    in
                    ignore (Cont.Sde_limit.sim
                      ~output
                      par
                      z0
                      dbts
                    )
                | Some n ->
                    let output, close =
                      Cont.Csv.convert_append ?dt ~out ~t0 par tf z0
                    in
                    let rng = U.rng init#seed in
                    for k = 1 to n do
                      Printf.printf "Simulation %i\n%!" k ;
                      let output = output k in
                      let seed = Random.State.bits rng in
                      let dbts =
                        Sim.Dbt.rand_draw
                        ~rng:(U.rng (Some seed))
                        ~t0
                        ~dt:width
                        ~tf
                        ~n:Cont.n_events
                      in
                      ignore (Cont.Sde_limit.sim
                        ~output
                        par
                        (Lac.copy z0)
                        dbts
                      )
                    done ;
                    close ()
              end

            let run () =
              begin
                Printf.eprintf "Simulate %s as a SDE\n" model_name ;
                Arg.parse specl anon_fun usage_msg ;
                let init = !init_r in
                sim init
              end

            let niter_r = ref 1

            let specl_many =
                 (Specs.niter niter_r)
              :: specl

            let run_many () =
              begin
                Arg.parse specl_many anon_fun usage_msg ;
                let init = !init_r in
                let niter = !niter_r in
                sim ~niter init
              end
          end
      end
  end


module Seq =
  struct
    module Ss = Seqsim

    class param = Param.t_seq

    class init = object
      (* Param.t_seq - Param.mutating = Param.t_unit + Param.outpool *)
      (* because mutating already in gtrable *)
      inherit Param.t_unit
      inherit Param.outpool
      (* ~ *)
      inherit Param.t_able
      inherit Param.dt_able
      inherit Param.seed_able
      inherit Param.gtr_able
    end


    module Gill =
      struct
        let cv s =
          s |> float_of_string |> int_of_float

        class immediate_init = object
          inherit init
          inherit Param.init_sir_int
        end

        class latent_init = object
          inherit init
          inherit Param.init_seir_int
        end

        module Make (Model : MODEL_SPEC) =
          struct
            module E = Process.Seq.Make (Model)

            let reads = Read.Reader.(
                [s0 cv ; r0 cv ; seed]
              @ (if Model.spec.circular then outpool () else [])
              @ mutating ()
              @ (if Model.spec.circular then circular () else [])
              @ (if Model.spec.latency then latent () else [])
              @ base ()
            )

            let load_par s x =
              load_par_from reads s x

            (* Same remark as in Cli.Unit : we don't need to differentiate *)
            let init_r = ref (new latent_init)

            let opar_r = ref Seqsim.Seqpop.Out.default

            let e_seq_o_r = ref (Some "initdenv1.fasta")

            let i_seq_o_r = ref (Some "initdenv1.fasta")

            let specl = Specs.(
                [ La.spec_of i_seq_o_r i0_seq ]
              @ (if Model.spec.latency then [La.spec_of e_seq_o_r e0_seq] else [])
              @ La.specl_of init_r [
                  dt ;
                  seed ;
                  t0 ;
                  tf ;
                  s0_int ;
                  r0_int ;
                  (init_from load_par) ;
              ]) @ (La.specl_of init_r (La.map
                      (fun th -> th#with_gtr)
                      (fun th -> th#gtr)
                    Seqsim.Gtr.specl))
                 @ (La.specl_of opar_r Seqsim.Seqpop.Out.specl)
                 @ (La.specl_of init_r (E.specl ()))

            let anon_fun s =
              Printf.printf "Ignored anonymous argument : %s" s

            let model_name = Seirco.model_name Model.spec

            let usage_msg =
                " Run a stochastic simulation of a "
              ^ model_name 
              ^ " system, with pathogen sequences."

            let run () =
              begin
                Arg.parse specl anon_fun usage_msg ;
                let channels = !opar_r in
                let init = !init_r in
                let seed = init#seed in
                let dt = Param.dt init in
                let t0 = Param.t0 init in
                let tf = Param.tf init in
                let s, _, r = init#sir in
                let par = (init :> param) in
                Printf.printf "Reading fasta input\n%!" ;
                (* FIXME allow a tree to be specified for relations between individuals ? *)
                let i0_chan =
                  match !i_seq_o_r with
                  | None -> stdin
                  | Some s -> (open_in s)
                in
                let assoc_of_fasta chan =
                  match Seqs.Io.assoc_of_fasta chan with
                  | None ->
                      invalid_arg "Bad integer format in fasta file"
                  | Some ks ->
                      ks
                in
                let i0_al = assoc_of_fasta i0_chan in
                (* FIXME the genealogy from merge does not correspond to the initial roots *)
                let z0 =
                  if Model.spec.latency then
                    let e0_chan =
                      match !e_seq_o_r with
                      | None -> stdin
                      | Some s -> (open_in s)
                    in
                    let e0_al = assoc_of_fasta e0_chan in
                    let o0_al = List.map (fun (_, seq) ->
                      (1, seq)
                    ) (e0_al @ i0_al) in
                    E.of_seiro (s, e0_al, i0_al, r, o0_al)
                  else
                    E.of_sir (s, i0_al, r)
                in
                Printf.printf "Input read, starting simulation\n%!" ;
                let output = E.Csv.convert
                  ?seed
                  ?dt
                  ~channels
                  ~t0
                  par
                  tf
                  z0
                in
                E.Gill.sim ?seed ~output ~t0 par z0 tf
              end
          end
      end
  end

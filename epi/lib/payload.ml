open Pop.Sig
open Sig


module Unit =
  struct
    type idor = nonid
    type 'a t = unit

    let isid = Isnotid Eq

    let default = ()
    let copy () = ()
    let score () = 0.
  end


module Seq =
  struct
    type idor = id
    type raw = Seqs.t * (float * Seqs.Diff.t) list
    type 'a t = raw

    let isid = Isid Eq
    let default = (Seqs.of_string "", [])
    let copy = Seqsim.Trait.copy
    let score (seq, _) = F.to_float (I.to_float (Seqs.length seq))
  end


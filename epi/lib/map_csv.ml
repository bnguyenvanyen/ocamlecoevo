(* Putting this here because I don't really know where to put it. *)
module L = BatList

let output_row chan row =
  Csv.output_record chan (Csv.Row.to_list row)

(*
let to_channel ?header ?separator ?backslash_escape
               ?excel_tricks ?quote_all chan =
  let csv_chan =
    Csv.to_channel ?separator ?backslash_escape ?excel_tricks ?quote_all chan
  in
  begin
    match header with
    | None ->
        ()
    | Some row ->
        output_record csv_chan row
  end ;
  csv_chan
*)

(* we ignore the problem of the header in out_chan here *)
let map ?header ?(nthin=1) ?(nburn=0)
        f in_chan out_chan =
  let rec next k : unit =
    match Csv.Rows.next in_chan with
    | exception End_of_file ->
        ()
    | in_row ->
        Printf.eprintf "k=%i\n%!" k ;
        if (k > nburn) && ((k mod nthin) = 0) then
          begin
            let out_row = f in_row in
            Csv.output_record out_chan out_row
          end ;
        next (k + 1)
  in
  begin
    match header with
    | None ->
        ()
    | Some g ->
        (Csv.output_record out_chan) @@ g @@ (Csv.Rows.header in_chan)
  end ;
  Printf.eprintf "nburn = %i\n%!" nburn ;
  next 1


let col s t = Printf.sprintf "%s:%f" s t

let times ~dt ~tf =
  let n = int_of_float (tf /. dt) in
  L.init (n + 1) (fun k -> dt *. float k)
  (*
  let un t =
    let t' = t +. dt in
    if t' > tf +. dt then
      None
    else
      Some (t', t')
  in 0. :: (L.unfold 0. un)
  *)


let header_sim ?(concat=false) colnames incolnames =
  if concat then
    incolnames @ colnames
  else
    colnames


let row_sim ?(concat=false) sim row =
  let data = sim row in
  let left = if concat then Csv.Row.to_list row else [] in
  L.fold_left (@) left data 



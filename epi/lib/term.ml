open Sig

module Cmd = Cmdliner
module Cap = U.Arg.Update.Capture


(* optional float with reading from optional file, and capture *)
let opt_float get set doc name =
  Cap.opt_config get set Cmd.Arg.float ~doc name


(* go through term only if flag, otherwise identity *)
let maybe_term flag term =
  if flag then
    term
  else
    Cmd.Term.const (fun ~config:_ x -> x)


(* lift to config capture term *)
let lift_term term =
  let f g ~config:_ =
    Cap.ignore g
  in
  Cmd.Term.(const f $ term)


module Par =
  struct
    (* base *)

    let load_from =
      let doc =
        "Load parameter values from $(docv)."
      in
      Cmd.Arg.(
        value
        & opt (some string) None
        & info ["load-par-from"] ~docv:"LOAD-PAR-FROM" ~doc
      )

    let host_birth () =
      let doc =
        "Host per capita birth rate, by host source unit."
      in
      let get par = par#host_birth in
      let set x par = par#with_host_birth x in
      opt_float get set doc "birth"

    let host_death () =
      let doc =
        "Host per capita death rate."
      in
      let get par = par#host_death in
      let set x par = par#with_host_death x in
      opt_float get set doc "death"

    let popsize =
      let doc =
        "Host population size if at equilibirum."
      in
      U.Arg.Capture.opt_config Cmd.Arg.float ~doc "popsize"

    let host_popsize () =
      let set x par = par#with_host_popsize x in
      let f pop ~config =
        U.Arg.Update.Capture.update set (pop ~config)
      in
      Cmdliner.Term.(
        const f
        $ popsize
      )

    let beta () =
      let doc =
        "Effective contact rate $(docv)."
      in
      let get par = par#beta in
      let set x par = par#with_beta x in
      opt_float get set doc "beta"

    let nu () =
      let doc =
        "Individual recovery rate."
      in
      let get par = par#nu in
      let set x par = par#with_nu x in
      opt_float get set doc "nu"

    let rho () =
      let doc =
        "Case reporting probability. $(docv) should be <= 1."
      in
      let get par = par#rho in
      let set x par = par#with_rho x in
      opt_float get set doc "rho"

    let base () =
      let f birth death source beta nu rho ~config par =
        par
        |> birth ~config
        |> death ~config
        |> source ~config
        |> beta ~config
        |> nu ~config
        |> rho ~config
      in
      Cmd.Term.(
        const f
        $ host_birth ()
        $ host_death ()
        $ host_popsize ()
        $ beta ()
        $ nu ()
        $ rho ()
      )


    (* latent *)

    let sigma () =
      let doc =
        "Rate of becoming infectious (1 / latency)"
      in
      let get par = par#sigma in
      let set x par = par#with_sigma x in
      opt_float get set doc "sigma"

    let latent () =
      let f sigma ~config par =
        par
        |> sigma ~config
      in
      Cmd.Term.(
        const f
        $ sigma ()
      )

    (* circular *)

    let betavar () =
      let doc =
        "Relative amplitude of beta variations. $(docv) should be <= 1."
      in
      let get par = par#betavar in
      let set x par = par#with_betavar x in
      opt_float get set doc "betavar"

    let freq () =
      let doc =
        "Frequency of beta variations."
      in
      let get par = par#freq in
      let set x par = par#with_freq x in
      opt_float get set doc "freq"

    let phase () =
      let doc =
        "Phase of beta variations. $(docv) should be <= 1."
      in
      let get par = par#phase in
      let set x par = par#with_phase x in
      opt_float get set doc "phase"

    let gamma () =
      let doc =
        "Rate of loss of immunity"
      in
      let get par = par#gamma in
      let set x par = par#with_gamma x in
      opt_float get set doc "gamma"

    let eta () =
      let doc =
        "Infectious hosts from the outside contacting the inside."
      in
      let get par = par#eta in
      let set x par = par#with_eta x in
      opt_float get set doc "eta"

    let circular () =
      let f betavar freq phase gamma eta ~config par =
        par
        |> betavar ~config
        |> freq ~config
        |> phase ~config
        |> gamma ~config
        |> eta ~config
      in
      Cmd.Term.(
        const f
        $ betavar ()
        $ freq ()
        $ phase ()
        $ gamma ()
        $ eta ()
      )

    (* stochastic *)

    let p_keep () =
      let doc =
        "Probability of sampling an individual on its recovery."
      in
      let get par = par#p_keep in
      let set x par = par#with_p_keep x in
      opt_float get set doc "p-keep"

    let p_outseed () =
      let doc =
        "Probability of seeding the outside global pool on recovery."
      in
      let get par = par#p_outseed in
      let set x par = par#with_p_outseed x in
      opt_float get set doc "p-outseed"

    let r_outdeath () =
      let doc =
        "Rate of competitive death for sequences in the global pool."
      in
      let get par = par#r_outdeath in
      let set x par = par#with_r_outdeath x in
      opt_float get set doc "r-outdeath"

    let mut () =
      let f upd ~config par =
        let upd' =
          U.Arg.Update.Capture.map
            (fun p -> p#evo)
            (fun mut p -> p#with_evo mut)
            (upd ~config)
        in
        upd' par
      in
      Cmd.Term.(const f $ Seqsim.Mut.update_term)

    let mutating () =
      let f p_keep p_outseed r_outdeath mut ~config par =
        par
        |> p_keep ~config
        |> p_outseed ~config
        |> r_outdeath ~config
        |> mut ~config
      in
      Cmd.Term.(
        const f
        $ p_keep ()
        $ p_outseed ()
        $ r_outdeath ()
        $ mut ()
      )

    let term circ lat mut =
      let f base circular latent mutating ~config par =
        par
        |> base ~config
        |> circular ~config
        |> latent ~config
        |> mutating ~config
      in Cmd.Term.(
        const f
        $ base ()
        $ maybe_term circ (circular ())
        $ maybe_term lat (latent ())
        $ maybe_term mut (mutating ())
      )
  end


let out =
  let doc =
    "Prefix of names of files to output to." in
  Cmd.Arg.(
    required
    & pos 0 (some string) None
    & info [] ~docv:"OUT" ~doc
  )


(* simulation settings *)

let dt () =
  let doc =
    "$(docv) is the minimum time separating outputs."
  in
  let get p = p#dt in
  let set x p = p#with_dt x in
  Cap.opt_config get set Cmd.Arg.(some float) ~doc "dt"


let t0 () =
  let doc =
    "Start simulations at $(docv)."
  in
  let get p = p#t0 in
  let set x p =p#with_t0 x in
  opt_float get set doc "t0"


let tf () =
  let doc =
    "Stop simulations at $(docv)."
  in
  let get p = p#tf in
  let set x p =p#with_tf x in
  opt_float get set doc "tf"


let seed () =
  let doc =
    "Seed the PRNG with $(docv)."
  in
  let get p = p#seed in
  let set x p = p#with_seed x in
  Cap.opt_config get set Cmd.Arg.(some int) ~doc "seed"


(* prm stuff *)

module Prm =
  struct
    let exact =
      let doc =
        "Simulate the system with the exact prm method."
      in
      Cmd.Arg.(
        value
        & flag
        & info ~doc ~docv:"PRM-EXACT" ["prm-exact"]
      )

    let load_from =
      let doc =
        "Load PRM value from $(docv)."
      in
      Cmd.Arg.(
        value
        & opt (some string) None
        & info ["load-prm-from"] ~docv:"LOAD-PRM-FROM" ~doc
      )

    let output_grid =
      let doc =
        "Output the prm grid to '{out}.prm.grid.csv'."
      in
      Cmd.Arg.(
        value
        & flag
        & info ["output-prm-grid"] ~docv:"GRID" ~doc
      )

    let output_points =
      let doc =
        "Output the prm points to '{out}.prm.points.csv'."
      in
      Cmd.Arg.(
        value
        & flag
        & info ["output-prm-points"] ~docv:"POINTS" ~doc
      )

    let width () =
      let doc =
        "Generate PRM with time slice width of $(docv)."
      in
      let get p = p#prm_width in
      let set x p = p#with_prm_width x in
      opt_float get set doc "prm-width"
  end



(* initial conditions *)

let s0 cv =
  let doc =
    "Initial number of susceptible hosts."
  in
  let get p = p#s0 in
  let set x p = p#with_s0 x in
  Cap.opt_config get set cv ~doc "s0"


let e0 cv =
  let doc =
    "Initial number of exposed hosts."
  in
  let get p = p#e0 in
  let set x p = p#with_e0 x in
  Cap.opt_config get set cv ~doc "e0"


let i0 cv =
  let doc =
    "Initial number of infectious hosts."
  in
  let get p = p#i0 in
  let set x p = p#with_i0 x in
  Cap.opt_config get set cv ~doc "i0"


let r0 cv =
  let doc =
    "Initial number of resistant hosts."
  in
  let get p = p#r0 in
  let set x p = p#with_r0 x in
  Cap.opt_config get set cv ~doc "r0"


(* where and how do these come in ?
 * Maybe don't use the same argument ? *)

let seqs_from_file fname =
  Seqs.Io.assoc_of_fasta (open_in fname)


let update_with_seqs set =
  function
  | None ->
      (fun x -> x)
  | Some fname ->
      set (U.Option.some (seqs_from_file fname))


let e0_seq () =
  let doc =
    "Path to initial sequences for exposed hosts'pathogens."
  in
  let arg = U.Arg.opt Cmd.Arg.file ~doc "e0" in
  let set x p = p#with_e0 x in
  Cmd.Term.(
    const (update_with_seqs set)
    $ arg
  )


let i0_seq () =
  let doc =
    "Path to initial sequences for infectious hosts'pathogens."
  in
  let arg = U.Arg.opt Cmd.Arg.file ~doc "i0" in
  let set x p = p#with_i0 x in
  Cmd.Term.(
    const (update_with_seqs set)
    $ arg
  )


type (_, _) pop =
  | Continuous : (float, float) pop
  | Discrete : (int, int) pop
  | Seq : (int, (int * Seqs.t) list) pop


let seir0 (type a b) (pop : (a, b) pop) lat :
  (config:string option ->
   (a, b) Param.init_seir U.Arg.Capture.t ->
   (a, b) Param.init_seir U.Arg.Capture.t
  ) Cmd.Term.t =
  let f s0 e0 i0 r0 ~config init =
    init
    |> s0 ~config
    |> e0 ~config
    |> i0 ~config
    |> r0 ~config
  in
  match pop with
  | Continuous ->
      Cmd.Term.(
        const f
        $ s0 Cmd.Arg.float
        $ maybe_term lat (e0 Cmd.Arg.float)
        $ i0 Cmd.Arg.float
        $ r0 Cmd.Arg.float
      )
  | Discrete ->
      Cmd.Term.(
        const f
        $ s0 Cmd.Arg.int
        $ maybe_term lat (e0 Cmd.Arg.int)
        $ i0 Cmd.Arg.int
        $ r0 Cmd.Arg.int
      )
  | Seq ->
      Cmd.Term.(
        const f
        $ s0 Cmd.Arg.int
        $ maybe_term lat (lift_term (e0_seq ()))
        $ lift_term (i0_seq ())
        $ r0 Cmd.Arg.int
      )


(*

let tech_tag =
  let doc = "Technique to simulate the system." in
  let symbols = Arg.[
    `Gill, info ["gill"] ~doc ;
    `Prm, info ["prm"] ~doc ;
    `Prm_approx, info ["prm-approx"] ~doc ;
    `Ode, info ["ode"] ~doc ;
  ]
  in
  Arg.(value & vflag `Ode symbols)


let model_tag = Model.(
  let doc = "Model of transmission." in
  let symbols = Arg.[
    `Sir, info ["sir"] ~doc ;
    `Seir, info ["seir"] ~doc ;
    `Sirs, info ["sirs"] ~doc ;
    `Seirs, info ["seirs"] ~doc ;
  ]
  in
  Arg.(value & vflag `Sir symbols)
)


let model_f tech_tag model_tag =
  let tech =
    match tech_tag with
    | `Gill ->
        V Unit_gill
    | `Prm ->
        V Unit_prm
    | `Prm_approx ->
        V Unit_prm_approx
    | `Ode ->
        V Unit_ode
  in
  match tech with V v ->
  match model_tag with
  | `Sir ->
      Model.E (Sir v)
  | `Seir ->
      Model.E (Seir v)
  | `Sirs ->
      Model.E (Sirs v)
  | `Seirs ->
      Model.E (Seirs v)


let model = Term.(const model_f $ tech_tag $ model_tag)


(* We want to use different terms depending on tech and model...
 * How can we achieve this ?
 * What is the general function that we can write ?
 * Can we have *)


let simulate (type par c a) emodel from tf dt out =
  (* depending on model, create the right starting parameter value,
   * and modify it by reading the appropriate way from 'from',
   * and from the command line.
   * That second part sounds difficult ? *)
  match emodel with Model.E model ->
    let par = Model.new_ model in
    (* read from 'from' if it's some *)
    let par = Model.read_from model from in
    (* read what you should from the command line *)
    (* the only function that relates to that is eval_choices
     * but it doesn't define a term so we can't do it twice.
     * The list of arguments mostly depends on model,
     * but also on technique (for ode_apar and such)
     * (basically it defines the simulation function to use)
     * Or we build one term per model_x_tech choice
     * We just need the right read functions.
     * But it's not very elegant.
     * We would much prefer a function to build a term which,
     * upon evaluation, depending on the evaluation of the first,
     * will evaluate one of the others,
     * but I think the only term combinator, ($), always evaluates everything.
     *)

*)

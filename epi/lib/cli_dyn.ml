(* This aims to go with a Cmdliner refactor,
 * as a unique 'cli' module to subsume Cli.Unit and Cli_seq *)
  
(*
open Sig

module La = Lift_arg


(* Stay functional here *)

let conv : type a. a version -> (string -> Csv.Row.t -> a) =
  (* Why is Unit_gill different from Seq_gill *)
  function
  | Unit_gill ->
      Cli.int_from_float
  | Unit_prm ->
      Cli.int_from_float
  | Unit_prm_approx ->
      Cli.int_from_float
  | Unit_ode ->
      Cli.float_from
  (*
  | Seq_gill ->
      (fun r s -> int_of_float (Cli.float_from r s))
  *)


let reads :
  type par c. (par, c) Model.t -> (par -> Csv.Row.t -> par) list = Cli.(
  function
  | Model.Sir v ->
      let cv = conv v in
      [rws0 cv ; rwi0 cv ; rwr0 cv ; rwseed]
      @ base
  | Model.Seir v ->
      let cv = conv v in
      [rws0 cv ; rwe0 cv ; rwi0 cv ; rwr0 cv ; rwseed]
      @ latent
      @ base
  | Model.Sirs v ->
      let cv = conv v in
      [rws0 cv ; rwi0 cv ; rwr0 cv ; rwseed]
      @ circular
      @ base
  | Model.Seirs v ->
      let cv = conv v in
      [rws0 cv ; rwe0 cv ; rwi0 cv ; rwr0 cv ; rwseed]
      @ circular
      @ latent
      @ base
)


let tuple0 (type par c) (model : (par, c) Model.t) :
  (string * par La.spec * string) list = Specs.(
  let sir_int = [s0_int ; i0_int ; r0_int] in
  let seir_int = [s0_int ; e0_int ; i0_int ; r0_int] in
  let sir_float = [s0_float ; i0_float ; r0_float] in
  let seir_float = [s0_float ; e0_float ; i0_float ; r0_float] in
  match model with
  | Model.Sir v ->
      begin match v with
      | Unit_gill ->
          sir_int
      | Unit_prm ->
          sir_int
      | Unit_prm_approx ->
          sir_int
      | Unit_ode ->
          sir_float
      end
  | Model.Sirs v ->
      begin match v with
      | Unit_gill ->
          sir_int
      | Unit_prm ->
          sir_int
      | Unit_prm_approx ->
          sir_int
      | Unit_ode ->
          sir_float
      end
  | Model.Seir v ->
      begin match v with
      | Unit_gill ->
          seir_int
      | Unit_prm ->
          seir_int
      | Unit_prm_approx ->
          seir_int
      | Unit_ode ->
          seir_float
      end
  | Model.Seirs v ->
      begin match v with
      | Unit_gill ->
          seir_int
      | Unit_prm ->
          seir_int
      | Unit_prm_approx ->
          seir_int
      | Unit_ode ->
          seir_float
      end
)


let load_par model ipar =
  Cli.load_par (reads model) ipar


let specs_periph_v : type a. a version -> 'b =
  function
  | Unit_gill ->
      Specs.[ tf ; dt ; seed ]
  | Unit_prm ->
      Specs.[ tf ; dt ; seed ]
  | Unit_prm_approx ->
      Specs.[ tf ; dt ; seed ]  (* also h ? *)
  | Unit_ode ->
      Specs.[ tf ; dt ] (* also ode_apar ? *)


let specs_periph :
  type par c. (par, c) Model.t -> (string * par La.spec * string) list =
  function
  | Model.Sir v ->
      specs_periph_v v
  | Model.Seir v ->
      specs_periph_v v
  | Model.Sirs v ->
      specs_periph_v v
  | Model.Seirs v ->
      specs_periph_v v


let specs_init (type par c) (model : (par, c) Model.t) =
  let from = Specs.init_from (load_par model) in
  from :: (specs_periph model)


(* FIXME this might need to contain something about Ode in some cases *)
let specs (type par c) (model : (par, c) Model.t) out_r init_r =
  init_r := Model.new_ model ;
  Specs.(
    [ La.spec_of out_r out_string ]
  @ La.specl_of init_r (specs_init model)
  @ La.specl_of init_r (tuple0 model)
  @ La.specl_of init_r (Model.specl model)
)


(* can't call it version *)
let tech = (
  "-tech",
  La.String (fun _ s -> Cli.version s),
  "Technique to simulate the system."
)


let model tech_r out_r init_r model_r = (
  "-model",
  La.Symbol (["sir" ; "seir" ; "sirs" ; "seirs"],
             (fun base s ->
               match !tech_r with V tech ->
               let emodel = Cli.model tech s in
               model_r := Some emodel ;
               match emodel with Model.E model ->
               (specs model out_r init_r :: base)
              )),
  "Which model to use."
)


module Instance ( ) =
  struct
    let tech_r = ref (V Unit_ode)

    let out_r = ref None
   
    (* problem for init : what type to use ?
     * if we put None at the start, can it work ?
     * it's a bit annoying but well... *)
    let init_r = ref None

    let model_r = ref None

    let anon_fun s =
      Printf.printf "Ignored anonymous argument : %s" s

    let usage_msg =
      " Run a stochastic simulation of the requested SEIRS-type system,
        with the specified algorithm."

    let specl_r = ref [];;

    specl_r := [
      La.spec_of out_r Specs.out_string ;
      La.spec_of tech_r tech ;
      La.spec_of specl_r (model tech_r out_r init_r model_r) ;
    ]

    
    let main () =
      begin
        Arg.parse_dynamic specl_r anon_fun usage_msg ;
        let
        let init = !init_r in
        let seed = init#seed in
        let dt = Param.dt init in
        let tf = Param.tf init in
        let out =
          match !out_r with
          | None ->
              raise (Arg.Bad "missing mandatory '-out' argument")
          | Some s ->
              s
        in
        let z0 =
          if
      end
  end

*)

(*
(* experimental module to see if we can integrate with Prm
 * then we'll make a more general version *)

open Sig

(* we can reuse the Rates module *)

module Color =
  struct
    type t = [
      | `Infection
      | `Case
      | `Recovery
    ]

    let int_of =
      function
      | `Infection ->
          0
      | `Case ->
          1
      | `Recovery ->
          2

    let compare c c' =
      compare (int_of c) (int_of c')

    let to_string =
      function
      | `Infection -> "infection"
      | `Case -> "case"
      | `Recovery -> "recovery"
  end


module P = Sim.Prm.Make (Color)

module S = Sim.Ctmjp.Prm.Make (P)

module T = Trait.Unit
module E = Events.Unit


let create () =
  E.P.create ~ngroups:7

let of_sir (s, i, r) =
  let z = create () in
  E.P.add_new ~k:s z S ;
  E.P.add_new ~k:i z (I ()) ;
  E.P.add_new ~k:r z R ;
  z


let infectivity = Seirco.fixed_infectivity


let total_infection_rate par t z =
  E.Rates.infection_of infectivity par t z


let infection_rate par t z =
  F.Pos.mul (F.Proba.compl (Param.rho par)) (total_infection_rate par t z)


let case_rate par t z =
  F.Pos.mul (Param.rho par) (total_infection_rate par t z)


let recovery_rate =
  E.Rates.recovery


let infection_modif _ _ z =
  (* Printf.eprintf "infection at %f\n" t ; *)
  E.P.remove z (E.P.I.nonid T.S) ;
  E.P.add z (E.P.I.nonid (T.I ())) ;
  z

let case_modif _ _ _ z =
  (* Printf.eprintf "case at %f\n" t ; *)
  E.P.remove z (E.P.I.nonid T.S) ;
  E.P.add z (E.P.I.nonid (T.I ())) ;
  E.P.add z (E.P.I.nonid T.C) ;
  z


let recovery_modif _ _ z =
  (* Printf.eprintf "recovery at %f\n" t ; *)
  E.P.remove z (E.P.I.nonid (T.I ())) ;
  E.P.add z (E.P.I.nonid T.R) ;
  z


let evm par =
  let m = P.Cm.empty in
  let m = P.Cm.add `Infection (Sim.Ctmjp.Util.combine
    (infection_rate par)
    infection_modif
  ) m
  in
  let m = P.Cm.add `Case (Sim.Ctmjp.Util.combine
    (case_rate par)
    (case_modif par)
  ) m
  in
  let m = P.Cm.add `Recovery (Sim.Ctmjp.Util.combine
    (recovery_rate par)
    recovery_modif
  ) m
  in m


let rngm ?seed () =
  match seed with
  | None ->
      None
  | Some _ ->
      let m = P.Cm.empty in
      let m = P.Cm.add `Infection (U.rng seed) m in
      let m = P.Cm.add `Case (U.rng seed) m in
      let m = P.Cm.add `Recovery (U.rng seed) m in
      Some m

let rng_of c =
  function
  | None ->
      None
  | Some m ->
      Some (P.Cm.find c m)


let uranges n uf_inf uf_case uf_reco =
  let m = P.Cm.empty in
  let m = P.Cm.add `Infection (uf_inf, n) m in
  let m = P.Cm.add `Case (uf_case, n) m in
  let m = P.Cm.add `Recovery (uf_reco, n) m in
  m


let coal, reportr = E.Rates.(coal_ii, reportr_i)

let header = E.Out.header

let line par t z =
  E.Rates.Out.line infectivity coal reportr par t z

let simulate_until ?(k=1) ?seed ?chan ?dt par tf y0 =
  let evm = evm par in
  let line = line par in
  let rngm = rngm ?seed () in
  (* should scale with pop size *)
  let uranges = uranges
      (I.Pos.of_int 20)
      (F.Pos.of_float 50_000.)
      (F.Pos.of_float 5_000.)
      (F.Pos.of_float 30_000.)
  in
  let prm = P.rand_draw ?rngm (F.zero, tf) (I.Pos.of_int 20) uranges in
  (*
  P.iter_on_t_slices (fun tsl ->
    P.Cm.iter (fun c (umax, _) ->
      let rng = rng_of c rngm in
      let csl = P.c_slice_at tsl c in
      P.graft_upto ?rng tsl csl (2. *. umax)
    ) uranges
  ) prm ;
  *)
  Printf.printf "After prm draw : %f\n%!" (Sys.time ()) ;
  for j = 1 to k do
    Printf.eprintf "j=%i at %f\n%!" j (Sys.time ()) ;
    let y0' = E.P.copy y0 in (* why is this not enough ? *)
    S.Csv.simulate_until evm ~header ~line ?chan ?dt prm y0'
  done
*)

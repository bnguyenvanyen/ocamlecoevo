open Sig

module J = Jump


type point = {
  s : U.closed_pos ;
  e : U.closed_pos ;
  i : U.closed_pos ;
  r : U.closed_pos ;
  c : U.closed_pos ;
  o : U.closed_pos ;
  n : U.closed_pos ;
  reff : U.closed_pos ;
  coal : U.closed_pos ;
  reportr : U.closed_pos ;
}

module Point =
  struct
    type t = point

    let sus pt =
      F.Pos.narrow pt.s

    let exp pt =
      F.Pos.narrow pt.e

    let inf pt =
      F.Pos.narrow pt.i

    let rem pt =
      F.Pos.narrow pt.r

    let cas pt =
      F.Pos.narrow pt.c

    let out pt =
      F.Pos.narrow pt.o

    let tot pt =
      F.Pos.narrow pt.n
  end


type t = (U._float * point) list


let columns = [
  "t" ; "s" ; "e" ; "i" ; "r" ; "c" ; "o" ; "n" ;
  "reff" ; "coal" ; "reportr"
]


let header =
  [columns]


let extract (t, pt) =
  let sot x =
    F.to_string x
  in 
  let soaf x =
    Printf.sprintf "%g" (F.to_float x)
  in
  function
  | "t" ->
      sot t
  | "s" ->
      soaf pt.s
  | "e" ->
      soaf pt.e
  | "i" ->
      soaf pt.i
  | "r" ->
      soaf pt.r
  | "c" ->
      soaf pt.c
  | "o" ->
      soaf pt.o
  | "n" ->
      soaf pt.n
  | "reff" ->
      soaf pt.reff
  | "coal" ->
      soaf pt.coal
  | "reportr" ->
      soaf pt.reportr
  | s ->
      invalid_arg (Printf.sprintf "column %s not recognized" s)


let line t pt =
  L.map (extract (t, pt)) columns


let infected traj =
  traj
  |> L.map (fun (t, pt) -> (F.to_float t, pt.i))
  |> J.of_list


let cumulative_cases traj =
  traj
  |> L.map (fun (t, pt) -> (F.to_float t, pt.c))
  |> J.of_list


let reporting_rate traj =
  traj
  |> L.map (fun (t, pt) -> (F.to_float t, pt.reportr))
  |> J.of_list


let expected_neff traj =
  traj
  |> L.map (fun (t, pt) -> (F.to_float t, F.Pos.invert pt.coal))
  |> J.of_list


let removed_proportion traj =
  traj
  |> L.map (fun (t, pt) ->
      (F.to_float t, F.Proba.of_pos (F.Pos.div pt.r pt.n))
     )
  |> J.of_list


let translate delta_t =
  L.map (fun (t, pt) ->
    let t' = F.Pos.of_anyfloat F.Op.(t + delta_t) in
    (t', pt)
  )


let translate_out delta_t =
  L.map (fun (t, pt) ->
    (F.Op.(t + delta_t), pt)
  )


let read =
  let pos s =
    s
    |> F.Pos.of_string
    |> U.Option.some
    |> F.Pos.close
  in
  let read g =
    Some (fun s pt -> g pt (pos s))
  in
  let f' =
    function
    | "s" ->
        read (fun pt s -> { pt with s })
    | "e" ->
        read (fun pt e -> { pt with e })
    | "i" ->
        read (fun pt i -> { pt with i })
    | "r" ->
        read (fun pt r -> { pt with r })
    | "c" ->
        read (fun pt c -> { pt with c })
    | "o" ->
        read (fun pt o -> { pt with o })
    | "n" ->
        read (fun pt n -> { pt with n })
    | "reff" ->
        read (fun pt reff -> { pt with reff })
    | "coal" ->
        read (fun pt coal -> { pt with coal })
    | "reportr" ->
        read (fun pt reportr -> { pt with reportr })
    | _ ->
        None
  in
  function
  | "t" ->
      Some (fun s (_, pt) -> (pos s, pt))
  | s ->
      U.Option.map (fun h ->
        (fun s (t, pt) -> (t, h s pt))
      ) (f' s)


let load ~path =
  let z = F.zero in
  let x0 = (
    z,
    {
      s = z ; e = z ; i = z ; r = z ;
      c = z ; o = z ; n = z ;
      reff = z ; coal = z ; reportr = z
    }
  ) in
  U.Csv.read_all_and_fold ~f:read ~path x0

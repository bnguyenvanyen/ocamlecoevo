open Sig


type t = color


let to_int =
  function
  | `S_birth ->
      0
  | `I_infection ->
      1
  | `Leave_exposed ->
      2
  | `Recovery ->
      3
  | `Immunity_loss ->
      4
  | `S_death ->
      5
  | `R_death ->
      6
  | `O_infection ->
      7
  | `O_death ->
      8
  | `E_death ->
      9
  | `I_death ->
      10
  | `I_rep ->
      11
  | `I_del ->
      12
  | `E_rep ->
      13
  | `E_del ->
      14
  | `I_subst ->
      15
  | `E_subst ->
      16


let compare c c' =
  (* where is the int function ? just (to_int c') - (to_int c) ? *)
  (to_int c) - (to_int c')


let equal c c' = (compare c c' = 0)


let to_string =
  function
  | `I_infection ->
      "I_infection"
  | `Leave_exposed ->
      "Leave_exposed"
  | `Recovery ->
      "Recovery"
  | `Immunity_loss ->
      "Immunity_loss"
  | `O_infection ->
      "O_infection"
  | `O_death ->
      "O_death"
  | `I_rep ->
      "I_rep"
  | `I_del ->
      "I_del"
  | `E_rep ->
      "E_rep"
  | `E_del ->
      "E_del"
  | `I_subst ->
      "I_subst"
  | `E_subst ->
      "E_subst"
  | `S_birth ->
      "S_birth"
  | `S_death ->
      "S_death"
  | `E_death ->
      "E_death"
  | `I_death ->
      "I_death"
  | `R_death ->
      "R_death"


let of_string =
  function
  | "I_infection" ->
      Some `I_infection
  | "Leave_exposed" ->
      Some `Leave_exposed
  | "Recovery" ->
      Some `Recovery
  | "Immunity_loss" ->
      Some `Immunity_loss
  | "O_infection" ->
      Some `O_infection
  | "O_death" ->
      Some `O_death
  | "I_rep" ->
      Some `I_rep
  | "I_del" ->
      Some `I_del
  | "E_rep" ->
      Some `E_rep
  | "E_del" ->
      Some `E_del
  | "I_subst" ->
      Some `I_subst
  | "E_subst" ->
      Some `E_subst
  | "S_birth" ->
      Some `S_birth
  | "S_death" ->
      Some `S_death
  | "E_death" ->
      Some `E_death
  | "I_death" ->
      Some `I_death
  | "R_death" ->
      Some `R_death
  | _ ->
      None

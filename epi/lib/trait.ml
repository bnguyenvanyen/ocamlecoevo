open Pop.Sig
open Sig


module Make (Pay : PAYLOAD) :
  (TRAIT with type idor = Pay.idor
          and type 'a payload = 'a Pay.t) =
  struct
    type idor = Pay.idor
    type 'a payload = 'a Pay.t

    type _ t = 
      | S : nonid t
      | E : idor payload -> idor t
      | I : idor payload -> idor t
      | R : nonid t
      (* should C also carry a payload ? *)
      | C : nonid t
      | O : idor payload -> idor t

    type _ group =
      | Sus : nonid group
      | Exp : idor group
      | Inf : idor group
      | Rem : nonid group
      | Cas : nonid group
      | Out : idor group 

    module Payload = Pay

    (* How do we write this when we don't know what idor actually is ?
     * We maybe need a isid to come from Pay ? *)
    let isid : type a. a group -> a isid =
      function
      (* the easy cases first *)
      | Sus -> Isnotid Eq
      | Rem -> Isnotid Eq
      | Cas -> Isnotid Eq
      (* dependind on Pay *)
      | Exp -> Pay.isid
      | Inf -> Pay.isid
      | Out -> Pay.isid

    let group_of : type a. a t -> a group =
      function
      | S -> Sus
      | E _ -> Exp
      | I _ -> Inf
      | R -> Rem
      | C -> Cas
      | O _ -> Out

    let of_group : nonid group -> nonid t =
      function
      | Sus -> S
      | Rem -> R
      | Cas -> C
      (* Also need to handle those cases for when idor is nonid,
       * but using Pay.default is 'safe' then *)
      | Exp -> E Pay.default
      | Inf -> I Pay.default
      | Out -> O Pay.default

    let copy : type isid. isid t -> isid t =
      function
      | S ->
          S
      | E y ->
          E (Pay.copy y)
      | I y ->
          I (Pay.copy y)
      | R ->
          R
      | C ->
          C
      | O y ->
          O (Pay.copy y)

    let payload : idor t -> idor payload =
      function
      | E w ->
          w
      | I w ->
          w
      | O w ->
          w
      (* sadly idor might be nonid so we need to handle the other cases
       * but I think we can avoid raising and return Pay.default,
       * since it will be a nonid value (so, unit) *)
      | S ->
          Pay.default
      | R ->
          Pay.default
      | C ->
          Pay.default

    let score x = Pay.score (payload x)
end


module Unit = Make (Payload.Unit)


module Seq = Make (Payload.Seq)

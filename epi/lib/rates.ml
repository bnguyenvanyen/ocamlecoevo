open Sig


module Make (Get : GET) =
  struct
    type t = Get.t

    (* building blocks *)
    let infect beta z =
      let s = Get.sus z in
      let n = Get.tot z in
      F.Pos.Op.(beta * s / n)

    (* actual rates: beta(t) * S / N * I *)
    let infection_of infctvt par t z =
      let i = Get.inf z in
      F.Pos.mul (infect (infctvt par (F.narrow t)) z) i

    let leave_exposed par _ z =
      let e = Get.exp z in
      F.Pos.Op.((Param.sigma par) * e)

    let recovery par _ z =
      let i = Get.inf z in
      F.Pos.Op.((Param.nu par) * i)

    let immunity_loss par _ z =
      let r = Get.rem z in
      F.Pos.Op.((Param.gamma par) * r)

    (* beta(t) * S / N * eta *)
    let out_infection_of infctvt par t z =
      F.Pos.mul (infect (infctvt par (F.narrow t)) z) (Param.eta par)

    let out_death par _ z =
      let o = Get.out z in
      (* o - 1 *)
      let om1 = F.Pos.of_anyfloat (F.sub o F.one) in
      F.Pos.Op.(Param.r_outdeath par * om1 * o)

    let host_birth par _ _ =
      F.Pos.Op.(Param.host_popsize par * Param.host_birth par)

    let sus_host_death par _ z =
      F.Pos.Op.(Param.host_death par * Get.sus z)

    let exp_host_death par _ z =
      F.Pos.Op.(Param.host_death par * Get.exp z)

    let inf_host_death par _ z =
      F.Pos.Op.(Param.host_death par * Get.inf z)

    let rem_host_death par _ z =
      F.Pos.Op.(Param.host_death par * Get.rem z)

    (* case observed on infection *)
    let reportr_infection infct par z =
      let i = Get.inf z in
      F.Pos.Op.((Param.rho par) * infct * i)

    (* case observed on infection from inside or outside *)
    let reportr_infection_and_out infct par z =
      let i = Get.inf z in
      let eta = Param.eta par in
      F.Pos.Op.((Param.rho par) * infct * (i + eta))

    (* case observed on becoming infectious (after incubation) *)
    let reportr_infectious par z =
      let e = Get.exp z in
      F.Pos.Op.((Param.rho par) * (Param.sigma par) * e)

    (* case observed on recovery *)
    let reportr_recovery par z =
      let i = Get.inf z in
      F.Pos.Op.((Param.rho par) * (Param.nu par) * i)

    (* These coalescence rates are a bit unconvincing here *)

    (* coalescence rate between two `I lineages *)
    let coal_ii infct z =
      let i = Get.inf z in
      (* maybe remove this condition for easier estimation ? *)
      F.Pos.Op.(infct / i)

    (* coalescence rate between a `E and a `I lineage *)
    let coal_ei infct z =
      let e = Get.exp z in
      F.Pos.Op.(infct / e)

  module Out =
    struct
      let header ~append =
        if append then
          ["k" :: Traj.columns]
        else
          Traj.header

      let conv infctvt coal reportr par t z =
        let get_seirco z =
          (Get.sus z, Get.exp z, Get.inf z, Get.rem z, Get.cas z, Get.out z)
        in
        let f par t z =
          let s, e, i, r, c, o = get_seirco z in
          let n = Get.tot z in
          let infct = infect (infctvt par t) z in
          let reffv = Seirco.reff infct par in
          let coalv = coal infct z in
          let reportrv = reportr infct par z in
          Traj.{ s ; e ; i ; r ; c ; o ; n ;
                 reff = reffv ;
                 coal = coalv ;
                 reportr = reportrv
               }
        in f par t z

      let line infctvt coal reportr par ?k t z =
        let pt = conv infctvt coal reportr par t z in
        match k with
        | None ->
            Traj.line t pt
        | Some k ->
            (string_of_int k) :: (Traj.line t pt)
    end
  end 


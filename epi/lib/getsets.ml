(** Getting and setting in vector via its group *)

open Sig

(* for the ode case *)

module Make (T : 
  sig
    include TRAIT
    (* this is needed as input because
     * we might want to not consider some of the groups
     * to reduce the size of the system *)
    val group_to_int : 'a group -> int
  end) :
  (
    sig
      module T : TRAIT
      type t = vec
      module Get : (GET with type t = t)
      module Set : (SET with type t = vec)
      module Setpos : (SETPOS with type t = vec)
      module Mat : (MATSET with module T = T)
      val to_tuple : vec -> (float * float * float * float)
    end
  ) =
  struct
    module T = T

    type t = Lacaml.D.Vec.t

    type 'a group = 'a T.group

    let iog = T.group_to_int

    (* on a positive vector *)
    let get (type a) (grp : a group) =
      match iog grp with
      | i ->
          (fun v : _ F.pos F.t -> F.Pos.of_float v.{i})
      | exception Invalid_argument _ ->
          (fun _ : _ F.pos F.t -> F.zero)

    module Get =
      struct
        type nonrec t = t

        let sus v = get T.Sus v
        let exp v = get T.Exp v
        let inf v = get T.Inf v
        let rem v = get T.Rem v
        let cas v = get T.Cas v
        let out v = get T.Out v
        let tot v = F.Pos.Op.(sus v + exp v + inf v + rem v)
      end

    let set grp =
      match iog grp with
      | i ->
          let f v x =
            v.{i} <- F.to_float x
          in f
      | exception (Invalid_argument _ as e) ->
          let f _ _ =
            raise e
          in f

    module Set =
      struct
        type t = vec
        
        let sus = set T.Sus
        let exp = set T.Exp
        let inf = set T.Inf
        let rem = set T.Rem
        let cas = set T.Cas
        let out = set T.Out
      end

    module Setpos = Set
    
    module Mat =
      struct
        module T = T
        type nonrec 'a group = 'a group

        let inc mat grp grp' x =
          let i = iog grp in
          let i' = iog grp' in
          mat.{i, i'} <- mat.{i, i'} +. F.to_float x

        let set mat grp grp' x =
          let i = iog grp in
          let i' = iog grp' in
          mat.{i, i'} <- F.to_float x
      end

    let to_tuple v =
      (* Printf.eprintf "v.{3} (inf) = %f\n" v.{3} ; *)
      let s = F.to_float (Get.sus v) in
      let e = F.to_float (Get.exp v) in
      let i = F.to_float (Get.inf v) in
      let r = F.to_float (Get.rem v) in
      (s, e, i, r)
  end

open Sig

module SCU = Sim.Ctmjp.Util

let infectivity (sp : model_spec) =
  if sp.circular then
    Seirco.var_infectivity
  else
    Seirco.fixed_infectivity


module Unit =
  struct
    module E = Events.Unit
    module T = E.T
    module P = E.P
    module G = E.G
    module R = E.Rates
    module M = E.Modifs
    module Out = R.Out

    let specl (sp : model_spec) = Specs.(
        [host_birth ; host_death ; host_popsize]
      @ [(if sp.circular then betavar else beta)]
      @ (if sp.latency then [sigma] else [])
      @ [nu ; rho]
      @ (if sp.circular then [gamma ; eta ; freq ; phase] else [])
    )


    (* FIXME use Util.Csv.output *)
    let output_par (sp : model_spec) c ?seed ?dt ~t0 p tf (s0, e0, i0, r0) =
      let pf = Printf.fprintf in
      let dt = match dt with
        | None -> 0.
        | Some x -> F.to_float x
      in
      let t0 = F.to_float t0 in
      let tf = F.to_float tf in
      let seed = match seed with
        | None -> "NA"
        | Some n -> string_of_int n
      in
      pf c "birth,death,popsize," ;
      pf c "beta,nu,rho" ;
      if sp.latency then pf c ",sigma" ;
      if sp.circular then pf c ",betavar,gamma,eta,freq,phase" ;
      pf c ",seed,dt,t0,tf,s0,e0,i0,r0" ;
      pf c "\n" ;
      (* ********************** *)
      pf c "%f,%f,%f," p#host_birth p#host_death p#host_popsize ;
      pf c "%f,%f,%f" p#beta p#nu p#rho ;
      if sp.latency then pf c ",%f" p#sigma ;
      if sp.circular then pf c ",%f,%f,%f,%f,%f"
        p#betavar p#gamma p#eta p#freq p#phase ;
      pf c ",%s,%f,%f,%f,%f,%f,%f,%f"
            seed dt t0 tf s0 e0 i0 r0 ;
      pf c "\n"


    let infectivity sp =
      infectivity sp

    let color_groups sp =
        (if sp.latency then
           [[`I_infection] ; [`Leave_exposed]]
        else
           [[`I_infection]])
      @ [[`Recovery]]
      @ (if sp.circular then [[`Immunity_loss]] else [])
      @ (if sp.circular then [[`O_infection]] else [])
      @ (if sp.latency then
           [[`S_birth ; `S_death ; `E_death ; `I_death ; `R_death]]
         else
           [[`S_birth ; `S_death ; `I_death ; `R_death]])

    let colors sp = L.concat (color_groups sp)

    (* by list order *)
    let color_name_to_int sp =
      let cols = colors sp in
      let f s =
        let c = U.Option.some (Color.of_string s) in
        U.Option.some (L.index_of c cols) + 1
      in f

    let int_to_color_name sp =
      let cols = colors sp in
      let f i =
        let c = L.at cols (i - 1) in
        Color.to_string c
      in f

    let rate sp =
      function
      | `I_infection ->
          R.infection_of (infectivity sp)
      | `Leave_exposed ->
          R.leave_exposed
      | `Recovery ->
          R.recovery
      | `Immunity_loss ->
          R.immunity_loss
      | `O_infection ->
          R.out_infection_of (infectivity sp)
      | `S_birth ->
          R.host_birth
      | `S_death ->
          R.sus_host_death
      | `E_death ->
          R.exp_host_death
      | `I_death ->
          R.inf_host_death
      | `R_death ->
          R.rem_host_death
      (* annoying but required to be compatible with type color *)
      | #Sig.color_seq ->
          invalid_arg "invalid color"

    let modif
      (type b)
      (maybe_case : (_ -> b -> _ U.pos -> _))
      sp par =
      function
      | `I_infection ->
          if sp.latency then
            M.inf_exp_infection
          else
            M.inf_inf_infection_and (maybe_case par)
      | `Leave_exposed ->
          M.leave_exposed_and (maybe_case par)
      | `Recovery ->
          M.recovery
      | `Immunity_loss ->
          M.immunity_loss
      | `O_infection ->
          if sp.latency then
            M.out_exp_infection
          else
            M.out_inf_infection_and (maybe_case par)
      | `S_birth ->
          M.host_birth
      | `S_death ->
          M.sus_host_death
      | `E_death ->
          M.exp_host_death
      | `I_death ->
          M.inf_host_death
      | `R_death ->
          M.rem_host_death
      (* annoying but required to be compatible with type color *)
      | #Sig.color_seq ->
          invalid_arg "invalid color"

    let events maybe_case sp par =
      L.map (fun c ->
        (c, (rate sp c par, modif maybe_case sp par c))
      ) (colors sp)

    let event_sum sp par =
      let evs = events M.maybe_case_rng sp par in
      let _, rate_and_modifs = L.split evs in
      let rate_modifs = L.map (fun (r, m) ->
        SCU.combine r m
      ) rate_and_modifs
      in
      SCU.add_many rate_modifs


    module Cm = BatMap.Make (Color)


    module Make (Model : MODEL_SPEC) =
      struct
        type pop = Epi__Pop.Unit.t

        let specl () = specl Model.spec

        let output_par chan ?seed ?dt ~t0 par tf y0 =
          output_par Model.spec chan ?seed ?dt ~t0 par tf y0

        let infectivity par t = infectivity Model.spec par t

        let coal =
          if Model.spec.latency then
            R.coal_ei
          else
            R.coal_ii

        let reportr infct par z =
          if Model.spec.latency then
            R.reportr_infectious par z
          else if (not Model.spec.latency) && Model.spec.circular then
            R.reportr_infection_and_out infct par z
          else if (not Model.spec.latency) && (not Model.spec.circular) then
            R.reportr_infection infct par z
          else
            assert false

        let create ~nindivs =
          (* never more than two indivs in one event -> mem:3 
           * 6 groups -> ngroups:7 *)
          ignore nindivs ;
          P.create ~ngroups:7

        let of_tuple (s, e, i, r) =
          let z = create ~nindivs:(s + e + i + r) in
          ignore (P.add_new ~k:s z T.S) ;
          if Model.spec.latency then ignore (P.add_new ~k:e z (T.E ())) ;
          ignore (P.add_new ~k:i z (T.I ())) ;
          ignore (P.add_new ~k:r z T.R) ;
          z

        let e_i_equilibrium par e_plus_i =
          if not Model.spec.latency then
            assert false
          else
            let death = Param.host_death par in
            let nu = Param.nu par in
            let sigma = Param.sigma par in
            let coeff = F.Pos.Op.((death + nu) / sigma) in
            let i_eq = F.Pos.Op.(e_plus_i / (coeff + F.one)) in
            let e_eq = F.Pos.mul coeff i_eq in
            (e_eq, i_eq)

        let sepir_to_seir par (s, e_plus_i, r) =
          let e, i = e_i_equilibrium par e_plus_i in
          (F.Pos.narrow s, F.Pos.narrow e, F.Pos.narrow i, F.Pos.narrow r)

        (* call this only when you should *)
        let of_sir (s, i, r) = of_tuple (s, 0, i, r)

        let of_seir = of_tuple

        (*
        let max_of_sir n = of_sir (n, n, n)

        let max_of_seir n = of_seir (n, n, n, n)
        *)

        let to_tuple z =
          let s = F.to_float (G.sus z) in
          let e = F.to_float (G.exp z) in
          let i = F.to_float (G.inf z) in
          let r = F.to_float (G.rem z) in
          (s, e, i, r)

        let to_sir z =
          let (s, _, i, r) = to_tuple z in
          (s, i, r)

        let to_seir z =
          to_tuple z

        let conv par t z =
          Out.conv infectivity coal reportr par t z

        let line par ?k t z =
          Out.line infectivity coal reportr par ?k t z

        module Csv =
          struct
            let header = Out.header

            let convert ?seed ?dt ~out ~t0 par tf y0 =
              let header = header ~append:false in
              let line = line par in
              let chan = open_out (out ^ ".traj.csv") in
              let chan_par = open_out (out ^ ".par.csv") in
              output_par chan_par ?seed ?dt ~t0 par tf (to_tuple y0) ;
              close_out chan_par ;
              let output = Sim.Csv.convert ?dt ~header ~line ~chan in
              let return tf yf =
                let ret = output.return tf yf in
                close_out chan ;
                ret
              in
              { output with return = return }

            let convert_append ?seed ?dt ~out ~t0 par tf y0 =
              let header = header ~append:true in
              let line k = line par ~k in
              let chan = open_out (out ^ ".traj.csv") in
              let chan_par = open_out (out ^ ".par.csv") in
              output_par chan_par ?seed ?dt ~t0 par tf (to_tuple y0) ;
              close_out chan_par ;
              Sim.Csv.convert_append ?dt ~header ~line ~chan
          end

        module Cadlag =
          struct
            let convert ?dt par =
              let conv = conv par in
              Sim.Cadlag.convert ?dt ~conv
          end

        module Gill =
          struct
            let next ?seed par =
              let ev = event_sum Model.spec par in
              Sim.Ctmjp.Gill.integrate ev (Util.rng seed)

            let sim ~output ?seed ?t0 par =
              let next = next ?seed par in
              Sim.Loop.simulate_until ~next ~output ?t0
          end

        module Cm = Point.Colormap

        let no_sim ~(output : (_, _, _) Sim.Sig.output) z0 tf =
          output.start F.zero z0 ;
          output.return tf z0

        module Prm =
          struct
            let colors = colors Model.spec

            let color_groups = color_groups Model.spec

            module type S =
              (Sim.Sig.PRM_MINI
                 with type color = Color.t
                  and type 's Point.t = (Color.t, 's) Sim.Prm.point
              )

            let csv_convert
              (type prm)
              (module Prm : S with type t = prm)
              ?seed ?dt ~output_grid ~out ~t0 par tf y0 (prm : prm) =
              let output = Csv.convert ?seed ?dt ~out ~t0 par tf y0 in
              let return tf yf =
                (* Here we're counting on prm being mutable *)
                let ret = output.return tf yf in
                if output_grid then begin
                  let chan_grid = U.Csv.open_out (out ^ ".prm.grid.csv") in
                  Prm.output_grid chan_grid prm ;
                  U.Csv.close_out chan_grid
                end ;
                ret
              in
              { output with return = return }

            let sim_csv
              (type prm)
              (module Prm : S with type t = prm)
              sim
              ?seed ?dt ~nosim ~out ~output_grid ~t0 par tf z0 prm =
              let output = csv_convert
                (module Prm : S with type t = prm)
                ?seed
                ?dt
                ~output_grid
                ~out
                ~t0
                par
                (F.narrow tf)
                z0
                prm
              in
              if nosim then
                no_sim ~output z0 (F.narrow tf)
              else
                sim ~output par z0 prm

            let rand_draw_prm
              (type prm)
              (module Prm : S with type t = prm)
              ?seed ~width ~time_range ~ntslices : prm =
              let rng = U.rng seed in
              let rngm = Prm.rngm ~rng colors in
              let vectors = Point.vectors ~width colors in
              Prm.rand_draw ~rngm ~time_range ~ntslices ~vectors

            let read_prm
              (type prm)
              (module Prm : S with type t = prm)
              ~time_range ~ntslices fname =
              Prm.read ~time_range ~ntslices Point.u0 fname

            let read_or_draw_prm
              (type prm)
              (module Prm : S with type t = prm)
              ~width ~time_range ~ntslices seed fname : prm =
              match seed, fname with
              | None, None | Some _, Some _ ->
                  invalid_arg
              "Only seed or only path to prm must be given"
              | (Some _) as seed, None ->
                  rand_draw_prm
                    (module Prm : S with type t = prm)
                    ?seed ~width ~time_range ~ntslices
              | None, Some fname ->
                  begin match
                    read_prm
                      (module Prm : S with type t = prm)
                      ~time_range ~ntslices fname
                  with
                  | Some prm ->
                      prm
                  | None ->
                      failwith "Bad file format for input prm file"
                  | exception Sys_error _ ->
                      failwith "No file at input prm path"
                  end

            let many_sim_csv
              (type prm)
              (module Prm : S with type t = prm)
              sim
              ~niter ?seed ?dt ~out ~width ~time_range ~ntslices ~t0 par tf z0 =
              let output, close = Csv.convert_append
                ?seed
                ?dt
                ~out
                ~t0
                par
                tf
                z0
              in
              let rng = U.rng seed in
              for k = 1 to niter do
                Printf.printf "Simulation %i\n%!" k ;
                let seed = Random.State.bits rng in
                let prm =
                  rand_draw_prm
                    (module Prm : S with type t = prm)
                    ~seed ~width ~time_range ~ntslices
                in
                let output = output k in
                ignore (sim ~output par (P.copy z0) prm)
              done ;
              close ()
          end

        module type SIM_PRM =
          sig
            type prm

            val sim :
              output: (pop, 'a, F._float) Sim.Sig.output ->
              Param.t_unit ->
              pop ->
              prm ->
                'a

            val sim_csv :
              ?seed:int ->
              ?dt:_ U.anypos ->
              nosim:bool ->
              out:string ->
              output_grid:bool ->
              t0:_ U.anyfloat ->
              Param.t_unit ->
              _ U.anyfloat ->
              pop ->
              prm ->
                U._float * pop

            val rand_draw_prm :
              ?seed:int ->
              width:_ U.anypos ->
              time_range:(_ U.anypos * _ U.anypos) ->
              ntslices:U.anyposint ->
                prm

            val read_prm :
              time_range:(_ U.anypos * _ U.anypos) ->
              ntslices:U.anyposint ->
              string ->
                prm option

            val read_or_draw_prm :
              width:_ U.anypos ->
              time_range:(_ U.anypos * _ U.anypos) ->
              ntslices:U.anyposint ->
              int option ->
              string option ->
                prm

            val many_sim_csv :
              niter:int ->
              ?seed:int ->
              ?dt:_ U.anypos ->
              out:string ->
              width:_ U.anypos ->
              time_range:(_ U.anypos * _ U.anypos) ->
              ntslices:U.anyposint ->
              t0:_ U.anyfloat ->
              Param.t_unit ->
              _ U.anyfloat ->
              pop ->
                unit
          end

        (* needs to be before Prm because it shadows Epi.Prm *)
        module Prm_approx :
          (SIM_PRM with type prm = Ctmjp_prm_approx.P.t) =
          struct
            module C = Ctmjp_prm_approx

            type prm = C.P.t

            let events par =
              let evs = events M.no_case Model.spec par in
              let evm =
                L.fold_left (fun m (c, rm) ->
                  Cm.add c rm m
                ) Cm.empty evs
              in evm

            let sim ~output par =
              let evm = events par in
              C.simulate_until ~output evm

            let sim_csv =
              Prm.sim_csv
              (module C.P : Prm.S with type t = prm)
              sim

            let rand_draw_prm =
              Prm.rand_draw_prm
              (module C.P : Prm.S with type t = prm)

            let read_prm =
              Prm.read_prm
              (module C.P : Prm.S with type t = prm)

            let read_or_draw_prm =
              Prm.read_or_draw_prm
              (module C.P : Prm.S with type t = prm)

            let many_sim_csv =
              Prm.many_sim_csv
              (module C.P : Prm.S with type t = prm)
              sim
          end

        module Prm_fast :
          (SIM_PRM with type prm = Ctmjp_prm_fast.P.t) =
          struct
            module C = Ctmjp_prm_fast

            type prm = C.P.t

            let events par =
              let evs = events M.no_case Model.spec par in
              let evm =
                L.fold_left (fun m (c, rm) ->
                  Cm.add c rm m
                ) Cm.empty evs
              in evm

            let sim ~output par =
              let evm = events par in
              C.simulate_until ~output evm

            let sim_csv =
              Prm.sim_csv
              (module C.P : Prm.S with type t = prm)
              sim

            let rand_draw_prm =
              Prm.rand_draw_prm
              (module C.P : Prm.S with type t = prm)

            let read_prm =
              Prm.read_prm
              (module C.P : Prm.S with type t = prm)

            let read_or_draw_prm =
              Prm.read_or_draw_prm
              (module C.P : Prm.S with type t = prm)

            let many_sim_csv =
              Prm.many_sim_csv
              (module C.P : Prm.S with type t = prm)
              sim
          end

        module Prm_exact :
          (SIM_PRM with type prm = Ctmjp_prm_exact.P.t) =
          struct
            module C = Ctmjp_prm_exact

            type prm = C.P.t

            let events par =
              let evs = events M.maybe_case_pt Model.spec par in
              let evm =
                L.fold_left (fun map (c, (r, m)) ->
                  Cm.add c (SCU.combine r m) map
                ) Cm.empty evs
              in evm

            let sim ~output par =
              let evm = events par in
              C.simulate_until ~output evm

            let sim_csv =
              Prm.sim_csv
              (module C.P : Prm.S with type t = prm)
              sim

            let rand_draw_prm =
              Prm.rand_draw_prm
              (module C.P : Prm.S with type t = prm)

            let read_prm =
              Prm.read_prm
              (module C.P : Prm.S with type t = prm)

            let read_or_draw_prm =
              Prm.read_or_draw_prm
              (module C.P : Prm.S with type t = prm)

            let many_sim_csv =
              Prm.many_sim_csv
              (module C.P : Prm.S with type t = prm)
              sim
          end
      end
  end


(* could be under Unit, but I put it on its own *)
module Continuous =
  struct
    module Make (Model : MODEL_SPEC) =
      struct
        type pop = Lac.Vec.t

        module E = Unit.Make (Model)

        (* replaces 'Pop' in the continuous case *)
        module P = Getsets.Make (struct
          include Trait.Unit
          let group_to_int : type a. a group -> int =
            if Model.spec.latency then
              let fail () = invalid_arg "not SEIR" in
              function
              | Sus -> 1
              | Exp -> 2
              | Inf -> 3
              | Rem -> 4
              | Cas -> fail ()
              | Out -> fail ()
            else
              let fail () = invalid_arg "not SIR" in
              function
              | Sus -> 1
              | Inf -> 2
              | Rem -> 3
              | Exp -> fail ()
              | Cas -> fail ()
              | Out -> fail ()
        end)

        module O = Cont.Make (P)

        include P

        let dims = if Model.spec.latency then 4 else 3

        (* eta-expansion *)
        let infectivity par t = infectivity Model.spec par t

        let coal =
          if Model.spec.latency then
            O.R.coal_ei
          else
            O.R.coal_ii

        let reportr infct par z =
          if Model.spec.latency then
            O.R.reportr_infectious par z
          else if (not Model.spec.latency) && Model.spec.circular then
            O.R.reportr_infection_and_out infct par z
          else if (not Model.spec.latency) && (not Model.spec.circular) then
            O.R.reportr_infection infct par z
          else
            assert false

        let colors = Unit.colors Model.spec

        let n_events = L.length colors

        let color_namer = 
          (Unit.color_name_to_int Model.spec,
           Unit.int_to_color_name Model.spec)

        let rate =
          function
          | `I_infection ->
              O.R.infection_of infectivity
          | `Leave_exposed ->
              O.R.leave_exposed
          | `Recovery ->
              O.R.recovery
          | `Immunity_loss ->
              O.R.immunity_loss
          | `O_infection ->
              O.R.out_infection_of infectivity
          | `S_birth ->
              O.R.host_birth
          | `S_death ->
              O.R.sus_host_death
          | `E_death ->
              O.R.exp_host_death
          | `I_death ->
              O.R.inf_host_death
          | `R_death ->
              O.R.rem_host_death

        let modif =
          function
          | `I_infection ->
              if Model.spec.latency then
                O.Modifs.inf_exp_infection
              else
                O.Modifs.inf_inf_infection
          | `Leave_exposed ->
              O.Modifs.leave_exposed
          | `Recovery ->
              O.Modifs.recovery
          | `Immunity_loss ->
              O.Modifs.immunity_loss
          | `O_infection ->
              if Model.spec.latency then
                O.Modifs.inf_exp_infection
              else
                O.Modifs.inf_inf_infection
          | `S_birth ->
              O.Modifs.host_birth
          | `S_death ->
              O.Modifs.sus_host_death
          | `E_death ->
              O.Modifs.exp_host_death
          | `I_death ->
              O.Modifs.inf_host_death
          | `R_death ->
              O.Modifs.rem_host_death

        let events par =
          L.map (fun c ->
            (rate c par, modif c (Lac.Vec.make0 dims))
          ) colors

        let field par =
          let death = Param.host_death par in
          let sigma = Param.sigma par in
          let nu = Param.nu par in
          let gamma = Param.gamma par in
          let eta = Param.eta par in
          let infity = infectivity par in
          if (not Model.spec.latency) && (not Model.spec.circular) then
            (* SIR *)
            let f ~result t y =
              let birth = O.R.host_birth par t y in
              let betas = O.R.infect (infity t) y in
              let s = Get.sus y in
              let i = Get.inf y in
              let r = Get.inf y in
              Set.sus result F.Op.(~- betas * i - death * s + birth) ;
              Set.inf result F.Op.((betas - nu - death) * i) ;
              Set.rem result F.Op.(nu * i - death * r) ;
              result
            in f
          else if Model.spec.latency && (not Model.spec.circular) then
            (* SEIR *)
            let f ~result t y =
              let birth = O.R.host_birth par t y in
              let betas = O.R.infect (infity t) y in
              let s = Get.sus y in
              let e = Get.exp y in
              let i = Get.inf y in
              let r = Get.inf y in
              Set.sus result F.Op.(~- betas * i - death * s + birth) ;
              Set.exp result F.Op.(betas * i - (sigma + death) * e) ;
              Set.inf result F.Op.(sigma * e - (nu + death) * i) ;
              Set.rem result F.Op.(nu * i - death * r) ;
              result
            in f
          else if (not Model.spec.latency) && Model.spec.circular then
            (* SIRS *)
            let f ~result t y =
              let birth = O.R.host_birth par t y in
              let betas = O.R.infect (infity t) y in
              let s = Get.sus y in
              let i = Get.inf y in
              let i' = F.Pos.Op.(i + eta) in
              let r = Get.rem y in
              Set.sus result F.Op.(
                ~- betas * i' + gamma * r - death * s + birth
              ) ;
              Set.inf result F.Op.(betas * i' - (nu + death) * i) ;
              Set.rem result F.Op.(nu * i - (gamma + death) * r) ;
              result
            in f
          else
            (* SEIRS *)
            let f ~result t y =
              let birth = O.R.host_birth par t y in
              let betas = O.R.infect (infity t) y in
              let s = Get.sus y in
              let e = Get.exp y in
              let i = Get.inf y in
              let i' = F.Pos.Op.(i + eta) in
              let r = Get.rem y in
              Set.sus result F.Op.(
                ~- betas * i' + gamma * r - death * s + birth
              ) ;
              Set.exp result F.Op.(betas * i' - (sigma + death) * e) ;
              Set.inf result F.Op.(sigma * e - (nu + death) * i) ;
              Set.rem result F.Op.(nu * i - (gamma + death) * r) ;
              result
            in f

        (* FIXME more divisions by popsize *)
        (* I fill only the upper triangle *)
        let sq_diff par =
          let death = Param.host_death par in
          let sigma = Param.sigma par in
          let nu = Param.nu par in
          let gamma = Param.gamma par in
          (* let eta = Param.eta par in *)
          if (not Model.spec.latency) && (not Model.spec.circular) then
            (* SIR *)
            let sgm = Lac.Mat.make0 3 3 in
            let f t y =
              let s = Get.sus y in
              let i = Get.inf y in
              let r = Get.rem y in
              let birth = O.R.host_birth par t y in
              let inf = O.R.infection_of infectivity par t y in
              let reco = F.Op.(nu * i) in
              Mat.set sgm T.Sus T.Sus F.Op.(inf + birth + death * s) ;
              Mat.set sgm T.Inf T.Inf F.Op.(inf + (death + nu) * i) ;
              Mat.set sgm T.Rem T.Rem F.Op.(reco + death * r) ;
              Mat.set sgm T.Sus T.Inf F.Op.(~- inf) ;
              Mat.set sgm T.Inf T.Rem F.Op.(~- reco) ;
              sgm
            in f
          else if Model.spec.latency && (not Model.spec.circular) then
            (* SEIR *)
            let sgm = Lac.Mat.make0 4 4 in
            let f t y =
              let s = Get.sus y in
              let e = Get.exp y in
              let i = Get.inf y in
              let r = Get.rem y in
              let birth = O.R.host_birth par t y in
              let inf = O.R.infection_of infectivity par t y in
              let lat = F.Op.(sigma * e) in
              let reco = F.Op.(nu * i) in
              Mat.set sgm T.Sus T.Sus F.Op.(inf + birth + death * s) ;
              Mat.set sgm T.Exp T.Exp F.Op.(inf + (death + sigma) * e) ;
              Mat.set sgm T.Inf T.Inf F.Op.(lat + (death + nu) * i) ;
              Mat.set sgm T.Rem T.Rem F.Op.(reco + death * r) ;
              Mat.set sgm T.Sus T.Exp F.Op.(~- inf) ;
              Mat.set sgm T.Exp T.Inf F.Op.(~- lat) ;
              Mat.set sgm T.Inf T.Rem F.Op.(~- reco) ;
              sgm
            in f
          else if (not Model.spec.latency) && Model.spec.circular then
            (* SIRS *)
            let sgm = Lac.Mat.make0 3 3 in
            let f t y =
              let s = Get.sus y in
              let i = Get.inf y in
              let r = Get.rem y in
              let birth = O.R.host_birth par t y in
              let inf = O.R.infection_of infectivity par t y in
              let reco = F.Op.(nu * i) in
              let immu_loss = F.Op.(gamma * r) in
              (* mostly like SIR with R -> S *)
              Mat.set sgm T.Sus T.Sus F.Op.(inf + birth + immu_loss + death * s) ;
              Mat.set sgm T.Inf T.Inf F.Op.(inf + (death + nu) * i) ;
              Mat.set sgm T.Rem T.Rem F.Op.(reco + immu_loss + death * r) ;
              Mat.set sgm T.Sus T.Inf F.Op.(~- inf) ;
              Mat.set sgm T.Inf T.Rem F.Op.(~- reco) ;
              Mat.set sgm T.Sus T.Rem F.Op.(~- immu_loss) ;
              sgm
            in f
          else
            (* SEIRS *)
            (* combine SIRS + SEIR *)
            let sgm = Lac.Mat.make0 4 4 in
            let f t y =
              let s = Get.sus y in
              let e = Get.exp y in
              let i = Get.inf y in
              let r = Get.rem y in
              let birth = O.R.host_birth par t y in
              let inf = O.R.infection_of infectivity par t y in
              let lat = F.Op.(sigma * e) in
              let reco = F.Op.(nu * i) in
              let immu_loss = F.Op.(gamma * r) in
              Mat.set sgm T.Sus T.Sus F.Op.(inf + birth + immu_loss + death * s) ;
              Mat.set sgm T.Exp T.Exp F.Op.(inf + (death + sigma) * e) ;
              Mat.set sgm T.Inf T.Inf F.Op.(lat + (death + nu) * i) ;
              Mat.set sgm T.Rem T.Rem F.Op.(reco + immu_loss + death * r) ;
              Mat.set sgm T.Sus T.Exp F.Op.(~- inf) ;
              Mat.set sgm T.Exp T.Inf F.Op.(~- lat) ;
              Mat.set sgm T.Inf T.Rem F.Op.(~- reco) ;
              Mat.set sgm T.Sus T.Rem F.Op.(~- immu_loss) ;
              sgm
            in f

        (* FIXME this is going to be slow... *)
        let diff par =
          let sqdiff = sq_diff par in
          let f t y =
            let sgm = sqdiff t y in
            (* get eigenvalues and eigenvectors *)
            let p = Lac.lacpy sgm in
            U.Mat.sym_compute_root ~up:true p
          in f

        let sepir_to_seir = E.sepir_to_seir

        let of_sir ?z (s, i, r) =
          let z =
            match z with
            | None ->
                Lac.Vec.make0 dims
            | Some z ->
                z
          in
          Setpos.sus z s ;
          Setpos.inf z i ;
          Setpos.rem z r ;
          z

        let of_seir ?z (s, e, i, r) =
          let z =
            match z with
            | None ->
                Lac.Vec.make0 dims
            | Some z ->
                z
          in
          Setpos.sus z s ;
          Setpos.exp z e ;
          Setpos.inf z i ;
          Setpos.rem z r ;
          z

        (*
        let max_of_sir x =
          of_sir (x, x, x)

        let max_of_seir x =
          of_seir (x, x, x, x)
        *)

        let to_sir z =
          let (s, _, i, r) = to_tuple z in
          (s, i, r)

        let to_seir = to_tuple

        let conv par t y =
          O.Out.conv infectivity coal reportr par t y

        let line par ?k t y =
          O.Out.line infectivity coal reportr par ?k t y

        module Csv =
          struct
            let convert ?dt ~out ~t0 par tf y0 =
              let header = O.Out.header ~append:false in
              let line = line par in
              let chan = open_out (out ^ ".traj.csv") in
              let chan_par = open_out (out ^ ".par.csv") in
              E.output_par chan_par par ?dt ~t0 tf (to_tuple y0) ;
              let output = Sim.Csv.convert ?dt ~header ~line ~chan in
              let return tf yf =
                let ret = output.return tf yf in
                close_out chan_par ; close_out chan ;
                ret
              in { output with return = return }

            let convert_append ?dt ~out ~t0 par tf y0 =
              let header = O.Out.header ~append:true in
              let line k = line par ~k in
              let chan = open_out (out ^ ".traj.csv") in
              let chan_par = open_out (out ^ ".par.csv") in
              E.output_par chan_par ?dt ~t0 par tf (to_tuple y0) ;
              close_out chan_par ;
              Sim.Csv.convert_append ?dt ~header ~line ~chan
          end

        module Cadlag =
          struct
            let convert ?dt par =
              let conv = conv par in
              Sim.Cadlag.convert ?dt ~conv
          end

        let specl = E.specl

        module Ode =
          struct
            let sim ~output ?apar ?t0 par y0 =
              let f = field par in
              let domain = O.domain dims in
              let next = Sim.Ode.Lsoda.integrate ?apar domain f ?t0 y0 in
              Sim.Loop.simulate_until ~output ~next ?t0 y0
          end

        module Sde =
          struct
            let sim ~output par =
              let drift = field par in
              let diff = diff par in
              let domain = O.domain dims in
              Sim.Ctmjp.Sde.simulate_until ~output domain drift diff
          end

        module Sde_limit =
          struct
            let sim ~output par =
              let domain = O.domain dims in
              let events = events par in
              Sim.Ctmjp.Sde_limit.simulate_until ~output domain events
          end
      end
  end


module Seq =
  struct
    module Mut = Seqsim.Mut

    module E = Events.Seq
    module T = E.T
    module P = E.P
    module G = E.G
    module R = E.Rates
    module M = E.Modifs
    module Out = E.Out

    (*
    type pop = E.P.t
             * (int * (Seqs.t * (float * Seqs.Diff.t) list)) list
    *)

    let empty_record = [(0., Seqs.Sig.Not)]


    let specl (sp : model_spec) = Specs.(
        [host_birth ; host_death ; host_popsize]
      @ [(if sp.circular then betavar else beta)]
      @ (if sp.latency then [sigma] else [])
      @ [nu ; rho]
      @ (if sp.circular then
           [gamma ; eta ; freq ; phase ; p_outseed ; r_outdeath]
         else
           [])
      @ [p_keep]
      @ (mut ())
    )


    (* FIXME use Util.Csv.output *)
    let output_par (sp : model_spec) c ?seed ?dt ~t0 p tf (s0, e0, i0, r0) =
      let pf = Printf.fprintf in
      let dt = match dt with
        | None -> 0.
        | Some x -> F.to_float x
      in
      let t0 = F.to_float t0 in
      let tf = F.to_float tf in
      let seed = match seed with
        | None -> "NA"
        | Some n -> string_of_int n
      in
      pf c "birth,death,popsize," ;
      pf c "beta,nu,rho" ;
      if sp.latency then pf c ",sigma" ;
      if sp.circular then pf c ",betavar,gamma,eta,freq,phase" ;
      pf c "," ; Mut.out_par_head c ;
      pf c ",p_keep" ;
      if sp.circular then pf c ",p_outseed,r_outdeath" ;
      pf c ",seed,dt,t0,tf,s0,e0,i0,r0" ;
      pf c "\n" ;
      (* ********************** *)
      pf c "%f,%f,%f," p#host_birth p#host_death p#host_popsize ;
      pf c "%f,%f,%f" p#beta p#nu p#rho ;
      if sp.latency then pf c ",%f" p#sigma ;
      if sp.circular then pf c ",%f,%f,%f,%f,%f"
        p#betavar p#gamma p#eta p#freq p#phase ;
      pf c "," ; Mut.out_par_values c p#evo ;
      pf c ",%f" p#p_keep ;
      if sp.circular then pf c ",%f,%f"
        p#p_outseed p#r_outdeath ;
      pf c ",%s,%f,%f,%f,%f,%f,%f,%f"
            seed dt t0 tf s0 e0 i0 r0 ;
      pf c "\n"


    let infectivity sp =
      infectivity sp


    let event (sp : model_spec) =
      let infctvt = infectivity sp in
      let inf_infection_modif par =
        if sp.latency then
          M.inf_exp_infection
        else
          M.case_inf_inf_infection par
      in
      let recovery_modif par =
        if sp.circular then
          M.record_outseed_recovery par
        else
          M.record_recovery par
      in
      let out_infection_modif par =
        if sp.latency then
          M.out_exp_infection
        else
          M.case_out_inf_infection par
      in
      let infection_event par =
        [`I_infection,
         SCU.combine (R.infection_of infctvt par) (inf_infection_modif par)]
      in
      let leave_exposed_event par =
        if sp.latency then
          [`Leave_exposed,
           SCU.combine (R.leave_exposed par) (M.case_leave_exposed par)]
        else
          []
      in
      let recovery_event par =
        [`Recovery,
         SCU.combine (R.recovery par) (recovery_modif par)]
      in
      let immunity_loss_event par =
        if sp.circular then
          [`Immunity_loss,
           SCU.combine (R.immunity_loss par) M.immunity_loss]
        else
          []
      in
      let inf_subst_mutation_event par =
        [`I_subst,
         SCU.combine
          (R.mutation T.Inf Mut.subst_rate par)
          (M.mutation T.Inf Mut.subst_modif par)]
      in
      let inf_rep_mutation_event par =
        [`I_rep,
         SCU.combine
          (R.mutation T.Inf Mut.rep_subseq_rate par)
          (M.mutation T.Inf Mut.rep_subseq_modif par)]
      in
      let inf_del_mutation_event par =
        [`I_del,
         SCU.combine
          (R.mutation T.Inf Mut.del_subseq_rate par)
          (M.mutation T.Inf Mut.del_subseq_modif par)]
      in
      let exp_subst_mutation_event par =
        if sp.latency then
          [`E_subst,
           SCU.combine
            (R.mutation T.Exp Mut.subst_rate par)
            (M.mutation T.Exp Mut.subst_modif par)]
        else
          []
      in  
      let exp_rep_mutation_event par =
        if sp.latency then
          [`E_rep,
           SCU.combine
            (R.mutation T.Exp Mut.rep_subseq_rate par)
            (M.mutation T.Exp Mut.rep_subseq_modif par)]
        else
          []
      in
      let exp_del_mutation_event par =
        if sp.latency then
          [`E_del,
           SCU.combine
            (R.mutation T.Exp Mut.del_subseq_rate par)
            (M.mutation T.Exp Mut.del_subseq_modif par)]
        else
          []
      in
      let out_infection_event par =
        if sp.circular then
          [`O_infection,
           SCU.combine
            (R.out_infection_of infctvt par)
            (out_infection_modif par)
          ]
        else
          []
      in
      let out_death_event par =
        if sp.circular then
          [`O_death, SCU.combine (R.out_death par) (M.out_death)]
        else
          []
      in
      let host_birth_event par =
        [`S_birth,
         SCU.combine (R.host_birth par) M.host_birth]
      in
      let sus_death_event par =
        [`S_death,
         SCU.combine (R.sus_host_death par) M.sus_host_death]
      in
      let exp_death_event par =
        if sp.latency then
          [`E_death,
           SCU.combine (R.exp_host_death par) M.exp_host_death]
        else
          []
      in
      let inf_death_event par =
        [`I_death,
         SCU.combine (R.inf_host_death par) M.inf_host_death]
      in
      let rem_death_event par =
        [`R_death,
         SCU.combine (R.rem_host_death par) M.rem_host_death]
      in
      let event par =
        let events = 
            (infection_event par)
          @ (leave_exposed_event par)
          @ (recovery_event par)
          @ (immunity_loss_event par)
          @ (inf_subst_mutation_event par)
          @ (inf_rep_mutation_event par)
          @ (inf_del_mutation_event par)
          @ (exp_subst_mutation_event par)
          @ (exp_rep_mutation_event par)
          @ (exp_del_mutation_event par)
          @ (out_infection_event par)
          @ (out_death_event par)
          @ (host_birth_event par)
          @ (sus_death_event par)
          @ (exp_death_event par)
          @ (inf_death_event par)
          @ (rem_death_event par)
        in
        let _, rate_modifs = L.split events in
        SCU.add_many rate_modifs
      in event


    module Make (Model : MODEL_SPEC) =
      struct
        type pop = Epi__Pop.Seq.t

        let specl () = specl Model.spec

        let output_par chan ?seed ?dt ~t0 par tf y0 =
          output_par Model.spec chan ?seed ?dt ~t0 par tf y0

        let infectivity par t = infectivity Model.spec par t

        let coal =
          if Model.spec.latency then
            R.coal_ei
          else
            R.coal_ii

        let reportr infct par z =
          if Model.spec.latency then
            R.reportr_infectious par z
          else if (not Model.spec.latency) && Model.spec.circular then
            R.reportr_infection_and_out infct par z
          else if (not Model.spec.latency) && (not Model.spec.circular) then
            R.reportr_infection infct par z
          else
            assert false

        (* FIXME Put this in Pop.Events *)
        let add_many z0 =
          L.iter (fun (k, state) ->
            ignore (P.add_new ~k z0 state)
          )

        let create ~nindivs =
          (* never more than two indivs in one event -> mem:3
           * 6 groups -> ngroups:7 *)
          P.create ~mem:3 ~ngroups:7 ~scores:[Trait.Seq.score] ~nindivs

        let of_tuple (s, e, i, r, o) =
          let er = empty_record in
          let len = L.length in
          let z0 = create ~nindivs:(s + len e + len i + r + len o)
          in
          ignore (P.add_new ~k:s z0 T.S) ;
          (add_many z0) @@ (L.map (fun (k, seq) -> (k, T.E (seq, er)))) @@ e ;
          (add_many z0) @@ (L.map (fun (k, seq) -> (k, T.I (seq, er)))) @@ i ;
          ignore (P.add_new ~k:r z0 T.R) ;
          (add_many z0) @@ (L.map (fun (k, seq) -> (k, T.O (seq, er)))) @@ o ;
          z0

        let of_sir (s, i, r) = of_tuple (s, [], i, r, [])

        let of_seiro = of_tuple

        let to_tuple z =
          let s = F.to_float (E.G.sus z) in
          let e = F.to_float (E.G.exp z) in
          let i = F.to_float (E.G.inf z) in
          let r = F.to_float (E.G.rem z) in
          (s, e, i, r)

        let output_seqs chan record =
          let fmap (i, (t_death, trait)) =
            let seq, _ = Trait.Seq.payload trait in
            (i, t_death, seq)
          in
          record
          |> Tree.Genealogy.Intbl.to_list
          |> L.map fmap
          |> Seqs.Io.stamped_fasta_of_assoc chan

        let line par t z =
          R.Out.line infectivity coal reportr par t z

        (*
        let conv par t z =
          R.Out.conv infectivity coal reportr par t z
        *)

        module Gill =
          struct
            let next ?seed par =
              Sim.Ctmjp.Gill.integrate (event Model.spec par) (Util.rng seed)

            let sim ~output ?seed par =
              let next = next ?seed par in
              Sim.Loop.simulate_until ~next ~output
          end


        module Csv =
          struct
            let header = Out.header

            open Seqsim.Seqpop
            
            let convert ?seed ?dt ~channels ~t0 par tf z0 =
              let chan = channels.traj in
              output_par channels.par ?seed ?dt ~t0 par tf (to_tuple z0) ;
              Out.close_par channels ;
              let header = header ~append:false in
              let line = line par in
              let output = Sim.Csv.convert ?dt ~header ~line ~chan in
              let return tf zf =
                let ret = output.return tf zf in
                P.remove_erase_living (F.to_float tf) zf ;
                P.erase_dangling_edges zf ;
                let genealogy = P.to_tree_genealogy zf in
                let record = P.to_recorded_leaf_death_traits zf in
                output_seqs channels.seq record ;
                Out.output_trees_from_genealogy channels genealogy ;
                Out.close_csv channels ;
                Out.close_seq channels ;
                Out.close_tree channels ;
                ret
              in { output with return = return }
          end
      end
  end

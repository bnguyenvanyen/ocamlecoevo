open Sig


include Sim.Prm.Point (Color)

(* The base slice umax and volume are important parameters for
 * optimization of simulation time.
 * At the maximum, when the [width] is [0],
 * the height [umax = max_height],
 * At the minimum, when the [width] is infinite,
 * the volume is [max_height * min_width].
 *)


(* reference volume for base slices *)
let u0 = F.Pos.of_float 11.
let hinf = F.Pos.of_float 10.


let ntslices ~width duration =
  I.Pos.of_int (int_of_float (F.to_float (F.div duration width)))


let vectors ?(max_height=u0) ?(min_width=hinf) ~width colors =
  let umax = F.Pos.Op.(max_height / (F.one + min_width * width)) in
  L.map (fun c ->
    (I.one, Sim.Prm.{ c ; k = 1 ; t = F.zero ; u = umax })
  ) colors

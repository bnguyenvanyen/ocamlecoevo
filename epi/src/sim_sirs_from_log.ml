(* we just output "reportr" and "neff" directly *)

open Epi
open Sig

module La = Lift_arg
module Di = Default_init

module Om = Ode_model.Make (Sirs.Model)

let name_out s =
  (* drops s's separator, and replaces the end by '.cut.csv' *)
  let n = String.length s in
  let suffix = String.sub s (n - 4) 4 in
  assert (suffix = ".csv") ;
  let s' = String.sub s 0 (n - 4) in
  s' ^ ".sim.csv"
  

let cv = Cli.float_from
let reads = Cli.([
  rws0 cv ;
  rwi0 cv ;
  rwr0 cv ;
  rwbeta ;
  rwnu ;
  rwrho ;
] @ circular) 

let load_row ipar row =
  Cli.fold_read reads ipar row

let sim ?times ~dt ~tf row =
  let rej = Csv.Row.find row "rejected" in
  if rej = "accept" then
    begin
      let init = load_row (new Cli.Unit.Ode.latent_init) row in
      let par = (init :> Param.t_unit) in
      let conv = Om.conv par in
      let dt = F.Pos.of_float dt in
      let tf = F.Pos.of_float tf in
      let z0 = Om.of_sir (Param.sir init) in
      Printf.eprintf "s0 = %f\n" z0.{1} ;
      let traj = Sim.Cadlag.output ~dt ~conv (Om.sim par z0) z0 tf in
      let traj = L.map (fun (t, x) -> (F.to_float t, x)) traj in
      let data =
        Jump.map (fun (_,_,_,_,_,_,_,_,c,r) ->
          1. /. F.to_float c, F.to_float r
        ) traj
      in
      let neff, rate = Jump.split data in
      let cumul_rate = Jump.primitive rate in
      let binned_rate = Jump.p_diff cumul_rate in
      let times' = Jump.times data in
      begin
        match times with
        | None ->
            ()
        | Some xs ->
            assert (L.length xs = L.length times') ;
            assert (L.fold_left2 (fun sum t t' ->
              sum +. (t -. t') ** 2.
            ) 0. xs times' < F.to_float dt)
      end ;
      let values = List.combine (Jump.values neff) (Jump.values binned_rate) in
      L.map (fun (neff, br) -> [ string_of_float neff ; string_of_float br]) values
    end
  else
    match times with
    | None ->
        failwith "oops"
    | Some l ->
        L.init (L.length l) (fun _ -> [ "" ; "" ])
  


let input_r = ref None
let output_r = ref None
let n_thin_r = ref None
let n_burn_r = ref None
let dt_r = ref 0.
let tf_r = ref Di.tf
let apar_r = ref Sim.Ode.Lsoda.default


let specl = [
  ("-in", Arg.String (fun s -> input_r := Some s),
   "Path to input. Default stdin.") ;
  ("-out", Arg.String (fun s -> output_r := Some s),
   "Path to output. Default stdout, then if an input filename is given,
    same filename, suffixed by '.sim.csv'.") ;
  ("-nthin", Arg.Int (fun n -> n_thin_r := Some n),
   "Only simulate for 1 in every n rows.") ;
  ("-nburn", Arg.Int (fun n -> n_burn_r := Some n),
   "Only simulate after n rows.") ;
  ("-dt", Arg.Set_float dt_r,
   "Simulation interval.") ;
  ("-tf", Arg.Set_float tf_r,
   "Simulation final time.") ;
] @ (La.specl_of apar_r Sim.Ode.Lsoda.specl)


let anon_fun s = Printf.printf "Ignored anonymous argument : %s" s


let usage_msg =
  "Run many deterministic simulations of a SIRS system,
   with parameter values from an input file."


let main () =
  begin
    Arg.parse specl anon_fun usage_msg ;
    let chan_in, chan_out =
      match !input_r, !output_r with
      | None, None ->
        stdin, stdout
      | Some s, Some s' ->
        (open_in s), (open_out s')
      | Some s, None ->
        (open_in s), (open_out (name_out s))
      | None, Some s' ->
        stdin, (open_out s')
    in
    let nthin = !n_thin_r in
    let nburn = !n_burn_r in
    let dt = !dt_r in
    let tf = !tf_r in
    let times = Map_csv.times ~dt ~tf in
    let colnames = List.fold_left (fun l t ->
      l @ [Map_csv.col "neff" t ; Map_csv.col "reportr" t]
    ) [] times
    in
    let header = Map_csv.header_sim ~concat:true colnames in
    let f = Map_csv.row_sim ~concat:true (sim ~times ~dt ~tf) in
    let in_chan = Csv.of_channel ~has_header:true chan_in in
    let out_chan = Csv.to_channel chan_out in
    Map_csv.map ~header ?nthin ?nburn f in_chan out_chan ;
    Csv.close_in in_chan ;
    Csv.close_out out_chan
  end;;

main ()

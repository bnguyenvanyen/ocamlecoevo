module U = Util
module F = U.Float
module I = U.Int


let cases_csv s =
  (* drops s suffix, and replaces the end by '.data.cases.csv' *)
  let n = String.length s in
  let suffix = String.sub s (n - 9) 9 in
  assert (suffix = ".traj.csv") ;
  let s' = String.sub s 0 (n - 9) in
  s' ^ ".data.cases.csv"


let prev_csv s =
  let n = String.length s in
  let suffix = String.sub s (n - 9) 9 in
  assert (suffix = ".traj.csv") ;
  let s' = String.sub s 0 (n - 9) in
  s' ^ ".data.prevalence.csv"


let sero_csv s =
  let n = String.length s in
  let suffix = String.sub s (n - 9) 9 in
  assert (suffix = ".traj.csv") ;
  let s' = String.sub s 0 (n - 9) in
  s' ^ ".data.sero.csv"


let neff_csv s =
  (* drops s suffix, and replaces the end by '.data.neff.csv' *)
  let n = String.length s in
  let suffix = String.sub s (n - 9) 9 in
  assert (suffix = ".traj.csv") ;
  let s' = String.sub s 0 (n - 9) in
  s' ^ ".data.neff.csv"


let sof = string_of_float

let soaf = F.to_string

let soi = string_of_int

let soai = I.to_string


let columns_cases =
  ["t" ; "cases"]

let extract_cases (t, cases) =
  function
  | "t" ->
      sof t
  | "cases" ->
      sof cases
  | _ ->
      failwith "invalid column"


let columns_prevalence =
  ["t" ; "prevalence"]

let extract_prevalence (t, prev) =
  function
  | "t" ->
      sof t
  | "prevalence" ->
      soi prev
  | _ ->
      failwith "invalid column"


let columns_sero =
  ["t" ; "seropos" ; "cohort"]

let extract_sero (t, (n_seropos, n_cohort)) =
  function
  | "t" ->
      sof t
  | "seropos" ->
      soi n_seropos
  | "cohort" ->
      soai n_cohort
  | _ ->
      failwith "invalid column"


let columns_neff =
  ["t" ; "neff"]

let extract_neff (t, neff) =
  function
  | "t" ->
      sof t
  | "neff" ->
      soaf neff
  | _ ->
      failwith "invalid column"


let case_data from traj =
  match from with
  | `C ->
      traj
      |> Epi.Data.case_data
      |> Jump.map float_of_int
      |> Jump.to_list
  | `Reportr ->
      traj
      |> Epi.Data.binned_reportr_data
      |> Jump.map F.to_float
      |> Jump.to_list
  | `Reportr_like (tol, path') ->
      let cases_from = U.Option.some (Epi.Data.load_cases path') in
      traj
      |> Epi.Data.binned_reportr_data_at ?tol (Jump.times cases_from)
      |> Jump.map F.to_float
      |> Jump.to_list
  | `Reportr_noisy (ovd, rng) ->
      traj
      |> Epi.Data.noisy_reportr_data ~rng ovd
      |> Jump.map float_of_int
      |> Jump.to_list


module P = U.Precompute ( )

let prev_data ~no_random rng rho traj =
  if no_random then
    traj
    |> Epi.Data.expected_prevalence_data rho
    |> Jump.map (fun x -> int_of_float (F.to_float x))
    |> Jump.to_list
  else
    traj
    |> Epi.Data.prevalence_data (module P : Epi.Data.PBINO) ~rng rho
    |> Jump.to_list


let sero_data ~no_random rng seros traj =
  if no_random then
    traj
    |> Epi.Data.expected_sero_agg_data seros
    |> Jump.map (fun (x, n) -> (int_of_float (F.to_float x), n))
    |> Jump.to_list
  else
    traj
    |> Epi.Data.sero_agg_data (module P : Epi.Data.PBINO) ~rng seros
    |> Jump.to_list


let neff_data traj =
  traj
  |> Epi.Data.neff_data
  |> Jump.to_list


let write_cases from path traj =
  let data = case_data from traj in
  U.Csv.output
    ~columns:columns_cases
    ~extract:extract_cases
    ~path:(cases_csv path)
    data


let write_prev ~no_random rng rho path traj =
  let data = prev_data ~no_random rng rho traj in
  U.Csv.output
    ~columns:columns_prevalence
    ~extract:extract_prevalence
    ~path:(prev_csv path)
    data


let write_sero ~no_random rng seros path traj =
  let data = sero_data ~no_random rng seros traj in
  U.Csv.output
    ~columns:columns_sero
    ~extract:extract_sero
    ~path:(sero_csv path)
    data


let write_neff path traj =
  let data = neff_data traj in
  U.Csv.output
    ~columns:columns_neff
    ~extract:extract_neff
    ~path:(neff_csv path)
    data


let write_concat columns extract to_data path trajs =
  let data = List.fold_left (fun pts (k, traj) ->
    pts @ (List.map (fun pt -> (k, pt)) (to_data traj))
  ) [] trajs
  in
  let extract (k, pt) =
    function
    | "k" ->
        soi k
    | s ->
        extract pt s
  in
  U.Csv.output
    ~columns:("k" :: columns)
    ~extract
    ~path
    data


let write_cases_concat from path trajs =
  write_concat
    columns_cases
    extract_cases
    (case_data from)
    (cases_csv path)
    trajs


let write_prev_concat ~no_random rng rho path trajs =
  write_concat
    columns_prevalence
    extract_prevalence
    (prev_data ~no_random rng rho)
    (prev_csv path)
    trajs


let write_sero_concat ~no_random rng seros path trajs =
  write_concat
    columns_sero
    extract_sero
    (sero_data ~no_random rng seros)
    (sero_csv path)
    trajs


let write_neff_concat path trajs =
  write_concat
    columns_neff
    extract_neff
    neff_data
    (neff_csv path)
    trajs


let cases_repack ~no_random rng tol path' =
  function
  | `C ->
      `C
  | `Reportr ->
      if no_random then
        match path' with
        | None ->
            `Reportr
        | Some path' ->
            `Reportr_like (tol, path')
      else
        `Reportr_noisy (F.Pos.of_float 0.1, rng)


let traj_load_concat path =
  let f =
    function
    | "k" ->
        Some (fun s (_, tpt) -> (int_of_string s, tpt))
    | col ->
        Option.map (fun h ->
          (fun v (k, tpt) -> (k, h v tpt))
        ) (Epi.Traj.read col)
  in
  let z = F.zero in
  let x0 = (0, (
    z,
    Epi.Traj.{
      s = z ; e = z ; i = z ; r = z ;
      c = z ; o = z ; n = z ;
      reff = z ; coal = z ; reportr = z ;
    }
  ))
  in
  let ktpts = U.Csv.read_all_and_fold ~f ~path x0 in
  let kf, rev_traj, rev_ktrajs =
    List.fold_left (fun (k, rev_traj, rev_ktrajs) (k', tpt) ->
      if k = k' then
        (k, tpt :: rev_traj, rev_ktrajs)
      else
        (k', [tpt], (k, List.rev rev_traj) :: rev_ktrajs)
    ) (0, [], []) ktpts
  in
  List.rev ((kf, List.rev rev_traj) :: rev_ktrajs)


let write_data
  ?seed
  ~no_random ~no_cases ~prev ~sero ~neff ~concat
  cases_from cases_like cases_tol prev_rho seros path =
  let rng = U.rng seed in
  if not concat then begin
    let traj = Epi.Traj.load ~path in
    if not no_cases then begin
      Printf.printf "Generate cases data\n" ;
      write_cases
        (cases_repack ~no_random rng cases_tol cases_like cases_from)
        path
        traj
    end
    ;
    if prev then begin
      Printf.printf "Generate prevalence data\n" ;
      let rho = U.Option.some prev_rho in
      write_prev ~no_random rng rho path traj
    end
    ;
    if sero then begin
      Printf.printf "Generate seroprevalence data\n" ;
      write_sero ~no_random rng seros path traj
    end
    ;
    if neff then
      write_neff path traj
  end else begin
    (* concat mode *)
    let trajs = traj_load_concat path in
    if not no_cases then begin
      Printf.printf "Generate cases data\n" ;
      write_cases_concat
        (cases_repack ~no_random rng cases_tol cases_like cases_from)
        path
        trajs
    end
    ;
    if prev then begin
      Printf.printf "Generate prevalence data\n" ;
      let rho = U.Option.some prev_rho in
      write_prev_concat ~no_random rng rho path trajs
    end
    ;
    if sero then begin
      Printf.printf "Generate seroprevalence data\n" ;
      write_sero_concat ~no_random rng seros path trajs
    end
    ;
    if neff then
      write_neff_concat path trajs
  end


module Term =
  struct
    open Cmdliner

    let seed =
      let doc = "Initialize the PRNG with the seed $(docv)." in
      Arg.(value
         & opt (some int) None
         & info ["seed"] ~docv:"SEED" ~doc
      )

    let no_random =
      let doc =
        "Use the expected value instead of generating random data."
      in
      Arg.(value
         & flag
         & info ["no-random"] ~docv:"NO-RANDOM" ~doc
      )

    let no_cases =
      let doc = "Don't generate case count data." in
      Arg.(value
         & flag
         & info ["no-cases"] ~docv:"NO-CASES" ~doc
      )

    let cases_from =
      let doc = "What to build the case data with." in
      Arg.(value
         & opt (enum [
             ("cases", `C) ;
             ("reportr", `Reportr) ;
           ]) `C
         & info ["from"] ~docv:"FROM" ~doc
      )

    let cases_like =
      let doc =
        "Give case counts at the same time points as in $(docv)."
      in
      Arg.(value
         & opt (some string) None
         & info ["like"] ~docv:"LIKE" ~doc
      )

    let cases_tol =
      let doc =
        "Tolerance for jump regular conversion when 'like' is used."
      in
      Arg.(value
         & opt (some float) None
         & info ["tol"] ~docv:"TOL" ~doc
      )

    let prev =
      let doc =
        "Generate prevalence data, using the supplied 'rho'."
      in
      Arg.(value
         & flag
         & info ["prev"] ~docv:"PREV" ~doc
      )

    let prev_rho =
      let doc =
        "Reporting probability (prevalence)."
      in
      Arg.(value
         & opt (some (U.Arg.proba ())) None
         & info ["rho"] ~docv:"RHO" ~doc
      )

    let sero =
      let doc =
        "Generate sero-prevalence data, from 'sero-point's."
      in
      Arg.(value
         & flag
         & info ["sero"] ~docv:"SERO" ~doc
      )

    let sero_point =
      let doc =
        "Add a sero-prevalence datapoint."
      in
      Arg.(value
         & opt_all (pair float (U.Arg.posint ())) [] 
         & info ["sero-point"] ~docv:"SERO-POINT" ~doc
      )

    let neff =
      let doc =
        "Generate neff data."
      in
      Arg.(value
         & flag
         & info ["neff"] ~docv:"NEFF" ~doc
      )

    let concat =
      let doc =
        "Proceed in concat mode."
      in
      Arg.(value
         & flag
         & info ["concat"] ~docv:"CONCAT" ~doc
      )

    let path =
      let doc =
        "Path to the trajectory, that has to end in '.traj.csv'. " ^
        "Also used the data paths, as '.data.cases.csv', etc."
      in
      Arg.(required
         & pos 0 (some non_dir_file) None
         & info [] ~docv:"PATH" ~doc
      )

    let write_data =
      let flat
        seed
        no_random no_cases cas_lik cas_lik_j cas_from
        prev prev_rho
        sero sero_pts
        neff concat path =
        write_data
          ?seed 
          ~no_random ~no_cases ~prev ~sero ~neff ~concat
          cas_from cas_lik cas_lik_j
          prev_rho sero_pts path
      in
      Term.(const flat
          $ seed
          $ no_random
          $ no_cases
          $ cases_like
          $ cases_tol
          $ cases_from
          $ prev
          $ prev_rho
          $ sero
          $ sero_point
          $ neff
          $ concat
          $ path
      )

    let info =
      let doc =
        "Write the incidence case count, prevalence count, and
         effective population size associated with a trajectory.
         From the trajectory 'input.traj.csv', the files
         'input.data.cases.csv',
         'input.data.prevalence.csv',
         'input.data.neff.csv'
         are created."
      in Term.info
        "epi-gen-data"
        ~version:"%%VERSION%%"
        ~doc
        ~exits:Term.default_exits
        ~man:[]
  end

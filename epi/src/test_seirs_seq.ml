open Epi
open Sig

module Evu = Events_unit
module Evs = Events_seq

(* not too sure what this does or how to fix it *)

module T =
  struct
    type t = float * float * float * float
    let compare = compare
  end

module Tmap = Map.Make (T)

let populate f p =
  let rec g m =
    function
    | 0 ->
        m
    | n when n < 0 ->
        invalid_arg "negative"
    | n ->
        let x = f p in
        match Tmap.find_opt x m with
        | None -> g (Tmap.add x 1 m) (n - 1)
        | Some k -> g (Tmap.add x (k + 1) m) (n - 1)
  in g

let s0 = 878_833
let e0 = 365
let i0 = 160
let r0 = 120_642

let main () =
  begin
    let rng = Util.rng None in
    let ev_seq = Evs.event Seirs_seq.model_spec in
    let ev_unit = Evu.event Seirs.model_spec in
    let x0_unit = Seirs.Ev.of_tuple (
      s0,
      e0,
      i0,
      r0
    ) in
    let x0_seq = Seirs_seq.Ev.of_tuple (
      s0,
      [(e0, Seqs.of_string "A")],
      [(i0, Seqs.of_string "A")],
      r0,
      [(1, Seqs.of_string "A")]
    ) in
    let par_unit = Seirs.default in
    let par_seq = Seirs_seq.default in
    let f_unit p =
      let _, m = ev_unit p F.zero (Evu.E.P.copy x0_unit) in
      let x0u' = m rng in
      let s', e', i', r' = Seirs.Ev.to_tuple x0u' in
      (s' -. float s0, e' -. float e0, i' -. float i0, r' -. float r0)
    in
    let f_seq p =
      let _, m = ev_seq p F.zero (Evs.E.P.copy x0_seq) in
      let x0s' = m rng in
      let s', e', i', r' = Seirs_seq.Ev.to_tuple x0s' in
      (s' -. float s0, e' -. float e0, i' -. float i0, r' -. float r0)
    in
    let n = 1_000_000 in
    let test pu ps =
      let lu = Tmap.bindings (populate f_unit pu Tmap.empty n) in
      let ls = Tmap.bindings (populate f_seq ps Tmap.empty n) in
      let relds = List.map2 (fun (t, k) (t', k') ->
        assert (t = t') ;
        float (k' - k) /. float n
      ) lu ls in
      List.iter (fun dx -> print_float dx ; print_newline ()) relds
    in
    test par_unit par_seq ;
    print_newline () ;
    test (par_unit#with_eta 10.) (par_seq#with_eta 10.) ;
    print_newline () ;
    test (par_unit#with_nu 100.) (par_seq#with_nu 100.) ;
    print_newline () ;
    test (par_unit#with_beta 100.) (par_seq#with_beta 100.) ;
    print_newline () ;
    test (par_unit#with_gamma 1.) (par_seq#with_gamma 1.) ;
  end;;

main ()

(* most complete model is Seirs_seq *)
module Di = Epi.Default_init

let foi = float_of_int

let chan_r = ref stdout

let seed_r = ref Di.seed

let tf_r = ref Di.tf

let s0_r = ref (foi Di.s0)

let e0_r = ref (foi Di.e0)

let i0_r = ref (foi Di.i0)

let r0_r = ref (foi Di.r0)

let o0_r = ref (foi Di.o0)


let specl = [
  ("-out",
   Arg.String (fun s -> chan_r := Lift_arg.handle_chan s),
   "File (or handle) to write to. Default stdout.") ;
  ("-seed",
   Arg.Set_int seed_r,
   "Seed for the PRNG.") ;
  ("-tf",
   Arg.Set_float tf_r,
   "Simulation time.") ;
  ("-s0",
   Arg.Set_float s0_r,
   "Initial number of susceptibles.") ;
  ("-e0",
   Arg.Set_float e0_r,
   "Initial number of exposed.") ;
  ("-i0",
   Arg.Set_float i0_r,
   "Initial number of infected.") ;
  ("-r0",
   Arg.Set_float r0_r,
   "Initial number of removed.") ;
  ("-o0",
   Arg.Set_float o0_r,
   "Initial number of outsiders.") ;
]


let anon_fun s = Printf.printf "Ignored anonymous argument : %s" s

let usage_msg = "  Output default parameters of the SEIRCO models,
  plus hyperparameters specified to a CSV file."


let main () =
  begin
    Arg.parse specl anon_fun usage_msg ;
    let par = Epi.Seirs_seq.default in
    let s0 = !s0_r in
    let e0 = !e0_r in
    let i0 = !i0_r in
    let r0 = !r0_r in
    let o0 = !o0_r in
    let n = s0 +. e0 +. i0 +. r0 in
    let ch = !chan_r in
    let pf = Printf.fprintf in
    pf ch "seed,tf,n,s0,e0,i0,r0,o0," ;
    pf ch "beta,nu," ;
    pf ch "sigma," ;
    pf ch "betavar,gamma,eta,freq,phase," ;
    pf ch "rho," ;
    pf ch "pkeep," ;
    pf ch "mu_rep,mu_del," ;
    pf ch "mu_snp,relmu_a,relmu_t,relmu_c,relmu_g,p_at,p_ac,p_ag,p_ta,p_tc,p_tg,p_ca,p_ct,p_cg,p_ga,p_gt,p_gc," ;
    pf ch "p_outseed,r_outdeath\n" ;

    pf ch "%i,%f,%f,%f,%f,%f,%f,%f,"
      !seed_r !tf_r n s0 e0 i0 r0 o0 ;
    pf ch "%f,%f,"
      par#beta par#nu ;
    pf ch "%f,"
      par#sigma ;
    pf ch "%f,%f,%f,%f,%f,"
      par#betavar par#gamma par#eta par#freq par#phase ;
    pf ch "%f,"
      par#rho ;
    pf ch "%f,"
      par#p_keep ;
    Seqsim.Mut.(pf ch "%f,%f,"
      par#evo.mu_rep par#evo.mu_del) ;
    Seqsim.(Mut.(Snp.(pf ch "%f,%f,%f,%f,%f,"
      par#evo.snp.mu par#evo.snp.relmu_a par#evo.snp.relmu_t par#evo.snp.relmu_c par#evo.snp.relmu_g))) ;
    let a = Seqsim.(Mut.(Snp.(par#evo.snp.snproba))) in
    pf ch "%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,"
      a.(0).(1) a.(0).(2) a.(0).(3)
      a.(1).(0) a.(1).(2) a.(1).(3)
      a.(2).(0) a.(2).(1) a.(2).(3)
      a.(3).(0) a.(3).(1) a.(3).(2) ;
    pf ch "%f,%f\n"
      par#p_outseed par#r_outdeath ;
    close_out ch
  end;;

main ()


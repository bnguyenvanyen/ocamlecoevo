open Epi
open Sig

module U = Util
module La = Lift_arg

module E = Sirs.Events ( )

module Di = Default_init

let cv r s = int_of_float (Cli.float_from r s)
let reads = Cli.([rws0 cv ; rwi0 cv ; rwr0 cv] @ circular @ base) 

let load_par ipar =
  Cli.load_par reads ipar

let init_r = ref Sirs.default_ctmjp_init

let prefix_r = ref "data/sim_sirs"

let seed_r = ref 0

let k_r = ref 1

let specl = Specs.([
  ("-prefix",
   Arg.Set_string prefix_r,
   "Prefix of file to output to.") ;
  ("-seed",
   Arg.Set_int seed_r,
   "Seed for the PRNG that will draw the seeds. Default 0.") ;
  ("-k",
   Arg.Set_int k_r,
   "Number of trajectories to simulate.") ;
] @ La.specl_of init_r [
    dtprint ;
    tf ;
    s0_int ;
    i0_int ;
    r0_int ;
    (init_from load_par) ;
]) @ (La.specl_of init_r (Sirs.specl ()))

let anon_fun s = Printf.printf "Ignored anonymous argument : %s" s

let usage_msg = " Run many stochastic simulations of a SIRS system."

let main () =
  begin
    Arg.parse specl anon_fun usage_msg ;
    let init = !init_r in
    let dt = F.Pos.of_float init#dt in
    let par = (init :> Sirs.param) in
    let tf = F.Pos.of_float init#tf in
    let sir = init#sir in
    let rng = Random.State.make [| !seed_r |] in
    for _ = 1 to !k_r do
      let seed = U.rand_int ~rng 1_000_000 in
      let z0 = E.of_sir sir in
      let out = !prefix_r ^ "_" ^ (string_of_int seed) in
      ignore (E.Csv.simulate_until ~out ~dt ~seed par tf z0)
    done
  end;;

main ()

open Cmdliner

let () =
  Printexc.record_backtrace true ;
  Term.exit @@ Term.eval (Data.Term.write_data, Data.Term.info)

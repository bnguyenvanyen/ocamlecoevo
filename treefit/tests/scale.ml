module F = Util.Float
module T = Tree.T

module P = Treefit.Propose.Make (T)


let tree () =
  T.node 0. (
    T.binode 1.
      (T.leaf 3.)
      (T.binode 2. (T.leaf 4.) (T.leaf 5.))
  )


(* scaling by alpha = 1. changes nothing *)
let%test _ =
  let tree = tree () in
  let tree' = P.scale F.one tree in
  T.to_newick tree = T.to_newick tree'


(* scale is reversible *)
let%test _ =
  let tree = tree () in
  let tree' = P.scale (F.Pos.of_float (1./.2.)) (P.scale F.two tree) in
  T.to_newick tree = T.to_newick tree'


(* when scale hits a leaf, Invalid_argument is raised *)
let%test _ =
  match P.scale (F.Pos.of_float (1./.4.)) (tree ()) with
  | _ ->
      false
  | exception Invalid_argument _ ->
      true


(* look at the output *)
let%expect_test _ =
  let tree = tree () in
  let tree' = P.scale F.two tree in
  Printf.printf "%s" (T.to_newick tree') ;
  [%expect{| ((:3.000,(:4.000,:5.000):-1.000):-3.000):-4.000; |}]

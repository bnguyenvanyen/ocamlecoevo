module F = Util.Float
module T = Tree.T

module P = Treefit.Propose.Make (T)

let tree () =
  T.node 0. (
    T.binode 1.
      (T.leaf 3.)
      (T.binode 2. (T.leaf 4.) (T.leaf 5.))
  )


(* scale_node with alpha = 1. changes nothing *)
let%test _ =
  let tree = tree () in
  let _, tree' = P.scale_node 0 F.one tree in
  T.to_newick tree = T.to_newick tree'


(* scale_node is reversible *)
let%test _ =
  let tree = tree () in
  let _, tree' =  P.scale_node
    0
    F.two
    tree
  in
  let _, tree'' = P.scale_node
    0
    (F.Pos.of_float (1./.2.))
    tree'
  in
  T.to_newick tree = T.to_newick tree''


(* when scale_node hits a node, Invalid_argument is raised *)
let%test _ =
  match P.scale_node 1 (F.Pos.of_float 3.) (tree ()) with
  | _ ->
      false
  | exception Invalid_argument _ ->
      true


(* when scale_node is passed a non existent node, Invalid_argument is raised *)
let%test _ =
  match P.scale_node 2 F.one (tree ()) with
  | _ ->
      false
  | exception Invalid_argument _ ->
      true


(* look at output with k = 0 *)
let%expect_test _ =
  let tree = tree () in
  (* slightly surprising that this is ok *)
  let _, tree' = P.scale_node 0 F.two tree in
  Printf.printf "%s" (T.to_newick tree) ;
  [%expect{| ((:3.000,(:4.000,:5.000):2.000):1.000):0.000; |}] ;
  Printf.printf "%s" (T.to_newick tree') ;
  [%expect{| ((:3.000,(:4.000,:5.000):2.000):1.000):-1.000; |}]


(* look at output with k = 1 *)
(* small problem with equal times -- should we reject from the offset ? *)
let%expect_test _ =
  let tree = tree () in
  let _, tree' = P.scale_node 1 F.two tree in
  Printf.printf "%s" (T.to_newick tree) ;
  [%expect{| ((:3.000,(:4.000,:5.000):2.000):1.000):0.000; |}] ;
  Printf.printf "%s" (T.to_newick tree') ;
  [%expect{| ((:3.000,(:4.000,:5.000):2.000):0.000):0.000; |}]

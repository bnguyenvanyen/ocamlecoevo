module F = Util.Float
module T = Tree.T

module P = Treefit.Propose.Make (T)


let tree () =
  T.node 0. (
    T.binode 1.
      (T.leaf 3.)
      (T.binode 2. (T.leaf 4.) (T.leaf 5.))
  )


(* can't place_cursor_after a length too important *)
let%test _ =
  match P.place_cursor_after 5. (F.Pos.of_float 10.) 0. (tree ()) with
  | _ ->
      false
  | exception Invalid_argument _ ->
      true


(* can't place_cursor_after a length too important compared with tf *)
let%test _ =
  match P.place_cursor_after 2. (F.Pos.of_float 4.) 0. (tree ()) with
  | _ ->
      false
  | exception Invalid_argument _ ->
      true


(* places cursor at the root for u = 0. *)
let%expect_test _ =
  let tree = tree () in
  let zip, t_reached = P.place_cursor_after 5. F.zero 10. tree in
  Printf.printf "%f\n" (P.Z.state zip) ;
  Printf.printf "%f" t_reached ;
  [%expect{|
    0.000000
    0.000000 |}]


let%expect_test _ =
  let tree = tree () in
  let zip, t_reached = P.place_cursor_after 5. (F.Pos.of_float 4.5) 0. tree in
  Printf.printf "%f\n" (P.Z.state zip) ;
  Printf.printf "%f" t_reached ;
  [%expect{|
    2.000000
    2.500000 |}]


let%expect_test _ =
  let tree = tree () in
  let zip, t_reached = P.place_cursor_after 2.5 (F.Pos.of_float 4.) 0. tree in
  Printf.printf "%f\n" (P.Z.state zip) ;
  Printf.printf "%f" t_reached ;
  [%expect{|
    2.000000
    2.500000 |}]

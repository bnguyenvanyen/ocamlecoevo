module S = BatSet
module M = BatMap
module Lac = Lacaml.D

module F = Util.Float
module T = Tree.Lt

module P = Treefit.Propose_guided.Make (T)


let tree () =
  T.node (0., 8) (
    T.binode (1., 6)
      (T.leaf (3., 1))
      (T.binode (2., 5) (T.leaf (4., 2)) (T.leaf (5., 3)))
  )


let probas () =
  Util.int_fold_right ~f:(fun k ->
    M.Int.add k (Lac.Mat.make0 4 0, S.Int.empty)
  ) ~n:8 M.Int.empty


let%expect_test _ =
  let tree = tree () in
  let probas = probas () in
  let x_prune_sub = (3.5, 7) in
  let qs = P.graft_weights Homogen F.one x_prune_sub probas tree in
  M.Int.print
    BatInt.print
    F.print
    BatIO.stdout
    qs
  ;
  [%expect{|
    {
    1: 2.,
    2: 2.,
    3: 3.,
    5: 1.,
    6: 1.
    } |}]

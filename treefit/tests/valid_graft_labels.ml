module F = Util.Float
module T = Tree.Lt

module P = Treefit.Propose_guided.Make (T)


let tree () =
  T.node (0., 6) (
    T.binode (1., 5)
      (T.leaf (3., 1))
      (T.binode (2., 4) (T.leaf (4., 2)) (T.leaf (5., 3)))
  )


let%expect_test _ =
  let tree = tree () in
  let x_prune_sub = (3.5, 7) in
  let labels = P.valid_graft_labels x_prune_sub tree in
  BatMap.Int.print
    BatInt.print
    F.print
    BatIO.stdout
    labels
  ;
  [%expect{|
    {
    1: 2.,
    2: 2.,
    3: 3.,
    4: 1.,
    5: 1.
    } |}]

open Sig


module Make (T : Tree.Labeltimed.S with type label = int) =
  struct
    module Z = Tree.Zipper.Make (T)
    module P = Propose.Make (T)

    let union_disjoint a b =
      M.Int.merge (fun k ao bo ->
        match ao, bo with
        | None, None ->
            None
        | Some _, Some _ ->
            failwith (
              Printf.sprintf "binding %i present in both maps" k
            )
        | Some x, None
        | None, Some x ->
            Some x
      ) a b

    (* map of labels to sum of squared differences
     * between probas at the node and at sibling *)
    let sibling_ssqr_diff stgs probas tree =
      (* TODO or qs is created beforehand,
       * and we just update the matrices ? *)
      let rec f tree =
        let leaf _ =
          M.Int.empty
        in
        let node _ stree =
          f stree
        in
        let binode _ ltree rtree =
          let lk = T.State.to_label (T.root_value ltree) in
          let rk = T.State.to_label (T.root_value rtree) in
          let lp, _ = M.Int.find lk probas in
          let rp, _ = M.Int.find rk probas in
          let q = F.Pos.of_float (Seqsim.Probas.Mat.ssqr_diff stgs lp rp) in
          union_disjoint (f ltree) (f rtree)
          |> M.Int.add lk q
          |> M.Int.add rk q
        in
        T.case ~leaf ~node ~binode tree
      in
      f tree

    let prune_weights stgs sgm probas tree =
      let qs = sibling_ssqr_diff stgs probas tree in
      M.Int.map (fun x -> F.exp (F.div x (F.sq sgm))) qs

    let place_cursor_over ~k tree =
      let rec loop zipper =
        (* check the subtree root *)
        let subtree = Z.subtree zipper in
        let i = T.State.to_label (T.root_value subtree) in
        if i = k then
          `Reached zipper
        else
          (* match on the subtree *)
          let leaf _ =
            `Not_reached
          in
          let node _ _ =
            loop (Z.move_down_straight zipper)
          in
          let binode _ _ _ =
            (* try left *)
            match loop (Z.move_down_left zipper) with
            | `Not_reached ->
                (* try right *)
                loop (Z.move_down_right zipper)
            | (`Reached _) as reached ->
                reached
          in
          T.case ~leaf ~node ~binode subtree
      in
      match loop (Z.place_cursor tree) with
      | `Not_reached ->
          invalid_arg "label k not found in tree"
      | `Reached zipper ->
          zipper

    (* map of labels to upward edge lengths of all interior nodes
     * over which it's ok to graft a subtree starting by x_prune_sub.
     * It's ok if the label is different,
     * and the parent is older than x_prune_sub.
     * Note that the first binode can be valid
     *)
    let valid_graft_labels x_prune_sub tree =
      let t_prune_sub = T.State.to_time x_prune_sub in
      let rec f tree =
        let leaf _ =
          M.Int.empty
        in
        let node x_parent stree =
          let t_parent = T.State.to_time x_parent in
          let x = T.root_value stree in
          let k = T.State.to_label x in
          let t = T.State.to_time x in
          let dt = F.Pos.of_float (t -. t_parent) in
          let ks = f stree in
          if t_parent < t_prune_sub then
            M.Int.add k dt ks
          else
            ks
        in
        let binode x_parent ltree rtree =
          let t_parent = T.State.to_time x_parent in
          let lx = T.root_value ltree in
          let rx = T.root_value rtree in
          let lk = T.State.to_label lx in
          let rk = T.State.to_label rx in
          let lt = T.State.to_time lx in
          let rt = T.State.to_time rx in
          let ldt = F.Pos.of_float (lt -. t_parent) in
          let rdt = F.Pos.of_float (rt -. t_parent) in
          let lks = f ltree in
          let rks = f rtree in
          if t_parent < t_prune_sub then
            union_disjoint lks rks
            |> M.Int.add lk ldt
            |> M.Int.add rk rdt
          else
            union_disjoint lks rks
        in
        T.case ~leaf ~node ~binode tree
      in
      let ks = f tree in
      let k_prune_sub = T.State.to_label x_prune_sub in
      M.Int.filter (fun k _ -> k <> k_prune_sub) ks

    let graft_weights stgs sgm x_prune_sub probas tree =
      (* not all nodes are valid graft nodes :
       * the parent of the node needs to be older than the prune node *)
      let ks = valid_graft_labels x_prune_sub tree in
      let k_prune_sub = T.State.to_label x_prune_sub in
      let p_prune, _ = M.Int.find k_prune_sub probas in
      (* compute the ssqr diff between p and all nodes but k,
       * then take the exp of the neg, and multiply by edge length,
       * to serve as probas *)
      M.Int.filter_map (fun k (p, _) ->
        match M.Int.find k ks with
        | dt ->
            let q =
              F.Pos.of_float (Seqsim.Probas.Mat.ssqr_diff stgs p_prune p)
            in
            Some F.(Pos.Op.(dt * exp (neg (q / sq sgm))))
        | exception Not_found ->
            None
      ) probas


    module Prune =
      struct
        let draw stgs sgm rng (tree, probas) =
          (* compute for each node the sum of squared differences
           * with its sibling node q_i *)
          let qs = prune_weights stgs sgm probas tree in
          (* choose any node with probability q_i / sum_j q_j *)
          let qtot = M.Int.fold (fun _ -> F.Pos.add) qs F.zero in
          let k =
            (* TODO avoid going through a list ? *)
            if F.Op.(qtot =~ F.zero) then
              U.rand_unif_choose ~rng (L.of_enum (M.Int.keys qs))
            else
              U.rand_choose ~rng (M.Int.bindings qs)
          in
          (* place the cursor there *)
          let zipper = place_cursor_over ~k tree in
          (* prune and return the stuff *)
          let zipper, x_prune, pruned = Z.prune zipper in
          (zipper, x_prune, pruned)
      end

    module Graft =
      struct
        (* It could seem that we need the probas to be updated
         * between Prune and Graft,
         * but in fact the log_pd_ratio will still be correct *)
        let draw stgs sgm rng (zipper, x_prune, pruned, probas) =
          let x_prune_sub = T.root_value pruned in
          let tree = Z.remove_cursor zipper in
          let qs = graft_weights stgs sgm x_prune_sub probas tree in
          let k_graft_sibling = U.rand_choose ~rng (M.Int.bindings qs) in
          (* we want to graft in the edge up from k_graft_sibling *)
          let zipper = place_cursor_over ~k:k_graft_sibling tree in 
          (* in zipper, the cursor is under the parent of k_graft_sibling *)
          let x_graft_parent = Z.state zipper in
          let t_graft_parent = T.State.to_time x_graft_parent in
          let x_graft_sibling =
            zipper
            |> Z.subtree
            |> T.root_value
          in
          let t_prune_sub = T.State.to_time x_prune_sub in
          let t_graft_sibling = T.State.to_time x_graft_sibling in
          let t_graft =
            min t_prune_sub t_graft_sibling -. t_graft_parent
            |> F.Pos.of_float
            |> U.rand_float ~rng
            |> F.to_float
            |> (fun t -> t_graft_parent +. t)
          in
          let x_graft = T.State.at_time t_graft x_prune in
          let zipper =
            if U.rand_bool ~rng then
              Z.graft_left x_graft pruned zipper
            else
              Z.graft_right x_graft pruned zipper
          in
          zipper, x_graft_parent, x_graft_sibling
      end

    module Spr =
      struct
        type context = {
          x_prune : T.state ;
          (** the state of the pruned node *)
          x_prune_sub : T.state ;
          (** the root state of the pruned subtree *)
          x_prune_parent : T.state ;
          (** the state of the parent of the pruned node *)
          x_prune_sibling : T.state ;
          (** the root state of the sibling subtree of the pruned subtree *)
          x_graft_parent : T.state ;
          (** the state of the parent of the graft edge *)
          x_graft_sibling : T.state ;
          (** the state of the child of the graft edge *)
        }

        let context_to_string c =
          Printf.sprintf (
          "{\n" ^^
          "  x_prune = (%f, %i) ;\n" ^^
          "  x_prune_sub = (%f, %i) ;\n" ^^
          "  x_prune_parent = (%f, %i) ;\n" ^^
          "  x_prune_sibling = (%f, %i) ;\n" ^^
          "  x_graft_parent = (%f, %i) ;\n" ^^
          "  x_graft_sibling = (%f, %i) ;\n" ^^
          "}"
          )
           (T.State.to_time c.x_prune)
           (T.State.to_label c.x_prune)
           (T.State.to_time c.x_prune_sub)
           (T.State.to_label c.x_prune_sub)
           (T.State.to_time c.x_prune_parent)
           (T.State.to_label c.x_prune_parent)
           (T.State.to_time c.x_prune_sibling)
           (T.State.to_label c.x_prune_sibling)
           (T.State.to_time c.x_graft_parent)
           (T.State.to_label c.x_graft_parent)
           (T.State.to_time c.x_graft_sibling)
           (T.State.to_label c.x_graft_sibling)

        let draw stgs update sgm rng (tree, probas) =
          let zipper_prune, x_prune, pruned =
            Prune.draw stgs sgm rng (tree, probas)
          in
          let x_prune_sub = T.root_value pruned in
          let x_prune_sibling =
            zipper_prune
            |> Z.subtree
            |> T.root_value
          in
          let x_prune_parent = Z.state zipper_prune in
          let zipper_graft, x_graft_parent, x_graft_sibling =
            Graft.draw stgs sgm rng (zipper_prune, x_prune, pruned, probas)
          in
          let context = {
            x_prune ;
            x_prune_sub ;
            x_prune_parent ;
            x_prune_sibling ;
            x_graft_parent ;
            x_graft_sibling ;
          }
          in
          match Z.remove_cursor zipper_graft with
          | tree' ->
              let probas' = update tree' context probas in
              (tree', probas', context)
          | exception (Invalid_argument _ as e) ->
              Printf.eprintf "context:\n%s\n" (context_to_string context) ;
              raise e


        (* density is the product of:
         * - choose a prune node (q_prune / sum_qs_prune)
         * - choose a graft node (q_graft / sum_qs_graft)
         * - choose a graft point (1 / dt_graft)
         * - choose a graft direction (1 / 2)
         *)
        let density stgs sgm tree probas context =
          (*
          Printf.eprintf "context:\n%s\n" (context_to_string context) ;
          *)
          (* prune *)
          let k_prune_sub = T.State.to_label context.x_prune_sub in
          let qs_prune = prune_weights stgs sgm probas tree in
          let q_prune = M.Int.find k_prune_sub qs_prune in
          let sum_qs_prune =
            M.Int.fold (fun _ -> F.Pos.add) qs_prune F.zero
          in
          let p_prune =
            if F.Op.(sum_qs_prune = F.zero) then
              (* it can be any positive constant *)
              F.one
            else
              F.Pos.Op.(q_prune / sum_qs_prune)
          in
          (* graft *)
          (* We compute on { tree \ pruned },
           * but we don't update the probas.
           * Put it into context to not compute it 3 times ? *)
          let tree_minus =
            tree
            |> place_cursor_over ~k:k_prune_sub
            |> Z.prune
            |> (fun (z, _, _) -> z)
            |> Z.remove_cursor
          in
          let k_graft_sibling =
            T.State.to_label context.x_graft_sibling
          in
          let qs_graft = graft_weights
            stgs
            sgm
            context.x_prune_sub
            probas
            tree_minus
          in
          let q_graft = M.Int.find k_graft_sibling qs_graft in
          let sum_qs_graft =
            M.Int.fold (fun _ -> F.Pos.add) qs_graft F.zero
          in
          let p_graft =
            if F.Op.(sum_qs_graft = F.zero) then
              (* it can be any positive constant *)
              F.one
            else
              F.Pos.Op.(q_graft / sum_qs_graft)
          in
          let t_prune_sub = T.State.to_time context.x_prune_sub in
          let t_graft_sibling = T.State.to_time context.x_graft_sibling in
          let t_graft_parent = T.State.to_time context.x_graft_parent in
          (* range of time in which we could graft *)
          let dt_graft = F.Pos.of_float (
            min t_prune_sub t_graft_sibling -. t_graft_parent
          ) in
          F.Pos.Op.(p_prune * p_graft / (dt_graft * F.two))

        let reverse context =
          { context with
            x_prune_parent = context.x_graft_parent ;
            x_prune_sibling = context.x_graft_sibling ;
            x_graft_parent = context.x_prune_parent ;
            x_graft_sibling = context.x_prune_sibling ;
          }

        let log_pd_ratio stgs sgm
          (tree, probas)
          (tree', probas', context) =
          let p_fwd = density stgs sgm tree probas context in
          let p_bwd = density stgs sgm tree' probas' (reverse context) in
          F.Op.(F.log p_bwd - F.log p_fwd)

        let move_to_sample (tree, probas, _) =
          (tree, probas)

        let move stgs update sgm =
          Fit.Propose.Base {
            draw = draw stgs update sgm ;
            log_pd_ratio = (fun x y -> [F.to_float (log_pd_ratio stgs sgm x y)]) ;
            move_to_sample ;
          }
      end
  end

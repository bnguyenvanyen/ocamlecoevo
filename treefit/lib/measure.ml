open Sig


module Make
  (T : Tree.Timed.S)
  (Z : Tree.Zipper.S with type state = T.state
                      and type tree = T.t) =
  struct
    let max_time ?store tree =
      (* reimplement tip_value with poor man's memoization *)
      (* len should be enough for my common uses *)
      let store = Option.value ~default:(H.create 251) store in
      let rec tip_time tree =
        let leaf x =
          T.State.to_time x
        in
        let node _ stree =
          tip_time stree
        in
        let binode _ ltree rtree =
          max (tip_time ltree) (tip_time rtree)
        in
        match H.find store tree with
        | t ->
            t
        | exception Not_found ->
            let t = T.case ~leaf ~node ~binode tree in
            H.add store tree t ;
            t
      in
      tip_time tree

    let max_length_side ?store ?tf z =
      let t0 = T.State.to_time (Z.state z) in
      let t_max = max_time ?store (Z.sibling z) in
      match tf with
      | Some tf ->
          F.Pos.of_float (min tf t_max -. min tf t0)
      | None ->
          F.Pos.of_float (t_max -. t0)

    (* Do I need down_left, down_right and down_straight ? *)
    let max_length_down ?store ?tf z =
      let t0 = T.State.to_time (Z.state z) in
      let t_max = max_time ?store (Z.subtree z) in
      match tf with
      | Some tf ->
          F.Pos.of_float (min tf t_max -. min tf t0)
      | None ->
          F.Pos.of_float (t_max -. t0)

    let max_length_down_children ?store ?tf z =
      let leaf _ =
        failwith "not binode"
      in
      let node _ _ = 
        failwith "not binode"
      in
      let binode _ ltree rtree =
        let lt_max = max_time ?store ltree in
        let rt_max = max_time ?store rtree in
        (lt_max, rt_max)
      in
      let lt_max, rt_max = T.case ~leaf ~node ~binode (Z.subtree z) in
      let t0 = T.State.to_time (Z.state z) in
      match tf with
      | Some tf ->
          F.Pos.of_float (min tf lt_max -. min tf t0),
          F.Pos.of_float (min tf rt_max -. min tf t0)
      | None ->
          F.Pos.of_float (lt_max -. t0),
          F.Pos.of_float (rt_max -. t0)

    let max_length_up ?store ?tf z =
      (* max length in z starting from child at t_child *)
      let rec loop t_child z =
        match Z.direction z with
        | `Top ->
            F.zero
        | `Straight ->
            let t = T.State.to_time (Z.state z) in
            let len = F.Pos.of_float (t_child -. t) in
            F.Pos.add len (loop t (Z.move_up z))
        | `Left | `Right ->
            let t = T.State.to_time (Z.state z) in
            let len = F.Pos.of_float (t_child -. t) in
            let len_side = max_length_side ?store ?tf z in
            let len_up = loop t (Z.move_up z) in
            F.Pos.add len (F.Pos.max len_side len_up)
      in
      match Z.direction z with
      | `Top ->
          failwith "no length up from top"
      | `Straight | `Right | `Left ->
          let t = T.State.to_time (Z.state z) in
          (* can't start at t > tf *)
          Option.iter (fun tf -> if t > tf then invalid_arg "t > tf") tf ;
          loop t (Z.move_up z)
  end

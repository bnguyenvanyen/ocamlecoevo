(** Proposal for phylogenetic inference on the tree

    Basically I reimplement proposals from BEAST2

 *)

open Sig


(* proposals on timed trees *)
(* Should I constrain the tree state ? *)
module Make (T : Tree.Timed.S) =
  struct
    module Z = Tree.Zipper.Make (T)
    module Me = Measure.Make (T) (Z)

    (* rescale interior node times by a factor [alpha].
     * The distance of every interior node to the tip is rescaled by alpha,
     * [t_node' = alpha * t_node + (1 - alpha) * t_tip].
     * Except for the root, which is just kept at a constant distance
     * from the first binode.
     * If [alpha] is too small the move can be invalid.
     * This is reversible : scale (1 / alpha) (scale alpha tree) = tree.
     * @raise Invalid_argument if the scaled tree is not time ordered. *)
    let scale (alpha : _ U.anypos) tree =
      let a = F.to_float alpha in
      let t_tip = T.State.to_time (T.tip_value tree) in
      let t_tipma = t_tip *. (1. -. a) in
      let sc x =
        let t = T.State.to_time x in
        let t' = a *. t +. t_tipma in
        T.State.at_time t' x
      in
      (* During MCMC, a weird thing happens where the root gets pushed
       * far back in time if we let it be scaled.
       * Since anyway the root can not matter, we simply keep it
       * a fixed length away from the first binode. *)
      T.case
        ~binode:(fun _ _ _ -> invalid_arg "unrooted tree")
        ~leaf:(fun _ -> invalid_arg "unrooted tree")
        ~node:(fun x stree ->
          let y = T.root_value stree in
          let len = T.State.to_time y -. T.State.to_time x in
          let stree' = T.map_case
            ~leaf:(fun x -> x)
            ~node:sc
            ~binode:sc
            stree
          in
          let y' = T.root_value stree' in
          let x' = T.State.at_time (T.State.to_time y' -. len) x in
          T.node x' stree'
        )
        tree

    (* change distance of node k to its parent by a factor alpha.
     * @raise Invalid_argument if the new tree is not time ordered *)
    let move_node slide k tree =
      let reached i tree =
        T.case
          ~leaf:(fun _ -> false)
          ~node:(fun _ _ -> false)
          ~binode:(fun _ _ _ -> i = 0)
          tree
      in
      let rec f i tree =
        let leaf _ =
          `Not_reached i
        in
        let node x stree =
          if reached i stree then
            let t_child = T.State.to_time (T.root_value stree) in
            let x' = slide t_child x in
            (* stree stays as it is *)
            `Reached (x', T.node x' stree)
          else
            match f i stree with
            | (`Not_reached _) as not_reached ->
                not_reached
            | `Reached (x_rc, stree') ->
                `Reached (x_rc, T.node x stree')
        in
        let binode x ltree rtree =
          (* we passed one more node *)
          let i = i - 1 in
          if reached i ltree then
            let lt = T.State.to_time (T.root_value ltree) in
            let x' = slide lt x in
            (* then the children remain the same *)
            `Reached (x', T.binode x' ltree rtree)
          else begin
            (* for the children subtrees *)
            match f i ltree with
            | `Reached (x_rc, ltree') ->
                `Reached (x_rc, T.binode x ltree' rtree)
            | `Not_reached i ->
                if reached i rtree then
                  let rt = T.State.to_time (T.root_value rtree) in
                  let x' = slide rt x in
                  `Reached (x', T.binode x' ltree rtree)
                else
                  match f i rtree with
                  | `Reached (x_rc, rtree') ->
                      `Reached (x_rc, T.binode x ltree rtree')
                  | (`Not_reached _) as not_reached ->
                      not_reached
          end
        in
        T.case ~leaf ~node ~binode tree
      in
      match f k tree with
      | `Reached (x, tree') ->
          (x, tree')
      | `Not_reached _ ->
          invalid_arg
          "k greater than number of interior nodes in tree"

    let scale_node k (alpha : _ U.anypos) tree =
      let a = F.to_float alpha in
      let slide t_child x =
        let t = T.State.to_time x in
        let t' = (1. -. a) *. t_child +. a *. t in
        T.State.at_time t' x
      in
      move_node slide k tree

    let slide_node k dt tree =
      let slide _ x =
        let t = T.State.to_time x in
        let t' = t +. dt in
        T.State.at_time t' x
      in
      move_node slide k tree

    (* place a cursor into the tree left or right under the [k]-th node
     * descending left first *)
    let place_cursor_under ~left ~k tree =
      (* i is the remaining number of interior nodes to pass *)
      let rec loop i zipper =
        (* match on zipper *)
        let f tree _ =
          (* match on tree in zipper *)
          let leaf _ =
            `Not_reached i
          in
          let node _ _ =
            (* this also means that we can't pick the root *)
            loop i (Z.move_down_straight zipper)
          in
          let binode _ _ _ =
            if i = 0 then
              (* cut *)
              if left then
                (* put the cursor left side under the node *)
                `Reached (Z.move_down_left zipper)
              else
                (* put the cursor right side under the node *)
                `Reached (Z.move_down_right zipper)
            else
              (* we have passed one more interior node *)
              let i = i - 1 in
              (* descend left *)
              match loop i (Z.move_down_left zipper) with
              | `Not_reached i ->
                  (* this is where the not_reached case
                   * given by a leaf is useful *)
                  (* then descend right *)
                  loop i (Z.move_down_right zipper)
              | (`Reached _) as reached ->
                  reached
          in
          T.case ~leaf ~node ~binode tree
        in Z.case f zipper
      in
      match loop k (Z.place_cursor tree) with
      | `Not_reached _ ->
          invalid_arg "k greater than number of interior nodes in tree"
      | `Reached zipper ->
          zipper

    (* total branch length of tree from root to tf *)
    let cut_branch_length tf tree =
      let len_to_tf =
        tree
        |> T.cut_after tf
        |> T.branch_length
      in
      (* more info for error if negative *)
      match F.Pos.of_float len_to_tf with
      | len ->
          len
      | exception (Invalid_argument _ as e) ->
          Printf.eprintf "tf = %f ; len_to_tf = %f\n"
          tf len_to_tf ;
          raise e

    (* place cursor once we have exhausted a branch length budget [u],
     * descending left first *)
    (* naive implementation, very similar to place_cursor_under *)
    let place_cursor_after tf (u : _ U.anypos) x_prune tree =
      (* if [u] is reached,
       * then [`Reached (zipper, x_reached)],
       * else [`Not_reached rem_u] *)
      let rec loop rem zipper =
        (* match on zipper *)
        let prev_t = T.State.to_time (Z.state zipper) in
        assert (prev_t <= tf) ;
        let f tree _ =
          (* match on tree in zipper *)
          let leaf x =
            let t = T.State.to_time x in
            if prev_t +. rem > min tf t then begin
              let dt = (min tf t) -. prev_t in
              let rem' = rem -. dt in
              `Not_reached rem'
            end else
              `Reached (zipper, T.State.at_time (prev_t +. rem) x_prune)
          in
          let node x _ =
            let t = T.State.to_time x in
            if prev_t +. rem <= min tf t then
              `Reached (zipper, T.State.at_time (prev_t +. rem) x_prune)
            else if t >= tf then
              (* no need to descend further *)
              let rem' = rem -. (tf -. prev_t) in
              `Not_reached rem'
            else (* t < tf and not reached -> descend *)
              let rem' = rem -. (t -. prev_t) in
              loop rem' (Z.move_down_straight zipper)
          in
          let binode x _ _ =
            (* mostly same as for node *)
            let t = T.State.to_time x in
            if prev_t +. rem <= min tf t then
              `Reached (zipper, T.State.at_time (prev_t +. rem) x_prune)
            else if t >= tf then
              (* no need to descend further *)
              let rem' = rem -. (tf -. prev_t) in
              `Not_reached rem'
            else (* t < tf and not reached -> descend *)
              (* descend left first *)
              let rem' = rem -. (t -. prev_t) in
              match loop rem' (Z.move_down_left zipper) with
              | `Not_reached rem' ->
                  (* rem' changed *)
                  loop rem' (Z.move_down_right zipper)
              | (`Reached _) as reached ->
                  reached
          in
          T.case ~leaf ~node ~binode tree
        in Z.case f zipper
      in
      let zipper = Z.place_cursor tree in
      let zipper' = Z.move_down_straight zipper in
      match loop (F.to_float u) zipper' with
      | `Not_reached _ ->
          Printf.eprintf "tree = %s\nu = %f ; tf = %f ; len = %f\n"
          (T.to_newick (T.cut_after tf tree))
          (F.to_float u)
          tf
          (F.to_float (cut_branch_length tf tree)) ;
          invalid_arg "u greater than total accessible tree length"
      | `Reached (z, x) ->
          (z, x)

    let rand_place_cursor ninterior rng tree =
      (* choose a uniform interior node index *)
      let k = I.to_int (U.rand_int ~rng ninterior) in
      (* choose which subtree moves *)
      let left = U.rand_bool ~rng in
      (* place zipper cursor on node *)
      place_cursor_under ~left ~k tree

    let rand_contour rng tf tree =
      let len = cut_branch_length tf tree in
      let u = U.rand_float ~rng len in
      (u, F.to_float len)

    module Prune =
      struct
        module Uniform =
          struct
            (* pick the prune point uniformly among interior nodes *)

            let draw ninterior rng tree =
              (* choose prune node *)
              let zipper = rand_place_cursor ninterior rng tree in
              (* prune *)
              let zipper, x_prune, pruned = Z.prune zipper in
              zipper, x_prune, pruned

            let dens_proba ninterior =
              (* this is actually a proba
               * but I don't have a constructive proof here *)
              F.Pos.invert (I.Pos.to_float ninterior)
          end
      end

    module Graft =
      struct
        module Uniform =
          struct
            (* graft the subtree at a uniform position among possible points *)

            let draw rng (zipper, x_prune, pruned) =
              (* unzip *)
              let tree = Z.remove_cursor zipper in
              (* the graft point should be older than first node of pruned *)
              let tf = T.State.to_time (T.root_value pruned) in
              (* choose the new contour position *)
              let u, cut_branch_length = rand_contour rng tf tree in
              (* place cursor at the new position *)
              let zipper, x_graft = place_cursor_after tf u x_prune tree in
              (* pick graft direction *)
              let zipper =
                if U.rand_bool ~rng then
                  Z.graft_left x_graft pruned zipper
                else
                  Z.graft_right x_graft pruned zipper
              in
              (* unzip *)
              (zipper, cut_branch_length)

            let dens_proba len =
              F.Pos.invert len
          end

        module Divergence =
          struct
            (* graft the subtree a normal distance along the branches
               from the pruning point *)

            (* The difficulty is that some down directions are not valid
             * once the distance to cross is determined.
             * That makes the move not reversible, and forces us to track
             * some things about the move to compute the densities *)

            (** moves at ends of paths *)
            type limit_move = [
              | `U_start of float * float
                (** start by going up, (t_prune, max_len_down) *)
              | `D_start of float * float
                (** start by going down, (t_prune, max_len_up) *)
              | `U_end of float * float
                (** end up, (t_graft, max_len_up) *)
              | `D_end of float * float
                (** end down, (t_graft, max_len_down) *)
            ]

            type inside_move = [
              | `U_straight of float
                (** go straight up at time t *)
              | `U_from_left of float * float
                (** go up coming from the left ,
                    (t, max_len_side) *)
              | `U_from_right of float * float
                (** go up coming from the right,
                    (t, max_len_side) *)
              | `D_straight of float
                (** go straight down at time t *)
              | `D_side of float * float
                (** go down coming from the side,
                    (t, max_len_up) *)
              | `D_left of float * float
                (** go down left, (t, max_len_side) *)
              | `D_right of float * float
                (** go down right, (t, max_len_side) *)
            ]

            type move = [
              | limit_move
              | inside_move
            ]

            (* draw a graft point at a distance d ~ Normal(0, varc)
             * from the prune point. *)
            let draw varc rng (zipper, x_prune, pruned) =
              (* zipper has its cursor placed under the parent of the pruned
               * subtree at the start.
               * To find the graft point, we progressively move the cursor
               * until a distance d has been crossed,
               * but only in valid directions.
               * Valid directions are those in which there exists
               * points farther than d on the tree.
               * When going down, we can only continue moving down.
               * When moving up, we can choose to go down
               * the sibling tree at some point.
               * We consider that we are at time t,
               * in between the cursor, above, and the subtree, below. 
               * So we look at the subtree time to consume distance.
               * Instead when going up, we look at the cursor time
               * to consume distance.
               * Except for the first move, the time t will be
               * the subtree time when going up,
               * and the cursor time when going down.
               *)
              (* memoize tree lengths *)
              let store = H.create 251 in
              (* cannot move pruned beyond tf *)
              let tf = T.State.to_time (T.root_value pruned) in
              (* length from cursor to t *)
              let dt_up ~t zipper =
                zipper
                |> Z.state
                |> T.State.to_time
                |> (fun t' -> t -. t')
                |> F.Pos.of_float
              in
              (* is it valid to go down into tree by a distance d,
               * when we are at time t somewhere under zipper cursor ? *)
              let valid_down ~t d zipper =
                let len = Me.max_length_down ~store ~tf zipper in
                let dt = F.to_float (dt_up ~t zipper) in
                let len' = F.to_float len -. dt in
                (d <= len'), len'
              in
              let valid_down_children ~t d zipper =
                let llen, rlen =
                  Me.max_length_down_children ~store ~tf zipper
                in
                let dt = F.to_float (dt_up ~t zipper) in
                let llen' = F.to_float llen -. dt in
                let rlen' = F.to_float rlen -. dt in
                (d <= llen'), llen', (d <= rlen'), rlen'
              in
              (* is it valid to go up into tree by a distance d,
               * when we are at time t somewhere under zipper cursor ? *)
              let valid_up ~t d zipper =
                let len =
                  F.to_float (Me.max_length_up ~store ~tf zipper)
                in
                let dt = F.to_float (dt_up ~t zipper) in
                let len' = len +. dt in
                (d <= len'), len'
              in
              let valid_side ~t d zipper =
                let len =
                  F.to_float (Me.max_length_side ~store ~tf zipper)
                in
                let dt = F.to_float (dt_up ~t zipper) in
                let len' = len +. dt in
                (d <= len'), len'
              in
              (* do the move and continue *)
              let rec move
                ~t'
                ~d'
                (direction : inside_move)
                (moves : move list)
                zipper =
                let dir, zipper' = 
                  match direction with
                  | `U_straight _
                  | `U_from_left _
                  | `U_from_right _ ->
                      `Up, Z.move_up zipper
                  | `D_side _ ->
                      `Down, Z.move_side zipper
                  | `D_straight _ ->
                      `Down, Z.move_down_straight zipper
                  | `D_left _ ->
                      `Down, Z.move_down_left zipper
                  | `D_right _ ->
                      `Down, Z.move_down_right zipper
                in
                loop d' t' dir ((direction :> move) :: moves) zipper'
              (* we were moving up, now where do we go ? *)
              and choose_dir_after_up ~t' ~d' zipper =
                (* look if moving down the sibling tree is valid *)
                match valid_side ~t:t' d' zipper with
                | v_side, max_len_side ->
                    let v_up, _ = valid_up ~t:t' d' zipper in
                    if (v_side && not v_up)
                    || (v_side && v_up && U.rand_bool ~rng) then
                      (* down side *)
                      `D_side (t', max_len_side)
                    else if (v_side && v_up) then
                      (* && not U.rand_bool *)
                      (* up *)
                      match Z.direction zipper with
                      | `Top | `Straight ->
                          assert false
                      | `Left ->
                          `U_from_left (t', max_len_side)
                      | `Right ->
                              `U_from_right (t', max_len_side)
                    else  (* not v_side && not v_up *)
                      (* I think this can't be reached *)
                      failwith "no valid direction"
                | exception Failure _ ->
                    (* unary node -> straight *)
                    `U_straight t'
               (* we were moving down, now where do we go ? *)
               and choose_dir_after_down ~t' ~d' zipper =
                  (* look if the children trees are valid *)
                  match valid_down_children ~t:t' d' zipper with
                  | v_left, len_left, v_right, len_right ->
                      if v_left && v_right && U.rand_bool ~rng then
                        `D_left (t', len_right)
                      else if v_left && v_right then
                        `D_right (t', len_left)
                      else if v_left && not v_right then
                        `D_left (t', len_right)
                      else if not v_left && v_right then
                        `D_right (t', len_left)
                      else  (* not v_left && not v_right *)
                        (* I think this can't be reached *)
                        failwith "no valid direction"
                  | exception Failure _ ->
                      (* unary node -> straight *)
                      `D_straight t'
               (* from a previous move to the next *)
               and loop d t prev_dir moves zipper =
                match prev_dir with
                | `Up ->
                    (* next time is up : cursor time *)
                    let x' = Z.state zipper in
                    let t' = T.State.to_time x' in
                    assert (t' < t) ;
                    let d' = d -. (t -. t') in
                    (* FIXME handle case d' = 0. *)
                    if d' < 0. then
                      (* reached *)
                      let t_graft = t -. d in
                      (* remaining length up from t' *)
                      let max_len_up =
                        Me.max_length_up ~store ~tf zipper
                      in
                      (* add length from t_graft to t' *)
                      let max_len_up =
                        F.to_float max_len_up +. t_graft -. t'
                      in
                      let moves = `U_end (t_graft, max_len_up) :: moves in
                      zipper, t_graft, moves
                    else
                      let dir =
                        choose_dir_after_up ~t' ~d' zipper
                      in
                      move ~t' ~d' dir moves zipper
                | `Down ->
                    (* next time is down : subtree time *)
                    let x' = T.root_value (Z.subtree zipper) in
                    let t' = T.State.to_time x' in
                    assert (t < t') ;
                    let d' = d -. (t' -. t) in
                    (* FIXME handle case d' = 0. *)
                    if d' < 0. then
                      (* reached *)
                      let t_graft = t +. d in
                      (* remaining length down from t *)
                      let max_len_down =
                        Me.max_length_down ~store ~tf zipper
                      in
                      (* remove length from t to t_graft *)
                      let max_len_down =
                        F.to_float max_len_down -. (t_graft -. t)
                      in
                      let moves = `D_end (t_graft, max_len_down) :: moves in
                      zipper, t_graft, moves
                    else
                      let dir =
                        choose_dir_after_down ~t' ~d' zipper
                      in
                      move ~t' ~d' dir moves zipper
              in
              let choose_dir_start t_prune d zipper =
                let v_down, max_len_down = valid_down ~t:t_prune d zipper in
                let v_up, max_len_up = valid_up ~t:t_prune d zipper in
                (* TODO factorize *)
                if v_up && v_down && U.rand_bool ~rng then
                  `D_start (t_prune, max_len_up)
                else if v_up && v_down then
                  `U_start (t_prune, max_len_down)
                else if not v_up && v_down then
                  `D_start (t_prune, max_len_up)
                else if v_up && not v_down then
                  `U_start (t_prune, max_len_down)
                else  (* not v_up && not v_down *)
                  failwith "no valid direction"
              in
              (* normal distance to go across *)
              let d = abs_float (U.rand_normal ~rng 0. varc) in
              let t_prune = T.State.to_time x_prune in
              (* actually do the moves from the start *)
              let zipper', t_graft, moves =
                match choose_dir_start t_prune d zipper with
                | (`D_start _ as move) ->
                    loop d t_prune `Down [move] zipper
                | (`U_start _ as move) ->
                    loop d t_prune `Up [move] zipper
              in
              let x_graft = T.State.at_time t_graft x_prune in
              (* do the regraft *)
              let zipper' =
                if U.rand_bool ~rng then
                  Z.graft_left x_graft pruned zipper'
                else
                  Z.graft_right x_graft pruned zipper'
              in
              (zipper', t_graft, d, (moves : move list))

            let p_moves d (moves : move list) =
              let half = F.Proba.of_float (1. /. 2.) in
              let f (prev_t, d, p) =
                function
                | `U_start _
                | `D_start _ ->
                    (* already handled *)
                    assert false
                | `U_from_left (t, max_len_side)
                | `U_from_right (t, max_len_side) ->
                    assert (t <= prev_t) ;
                    let d' = d -. abs_float (t -. prev_t) in
                    let p' =
                      if d' <= max_len_side then
                        F.Proba.mul half p
                      else
                        p
                    in
                    (t, d', p')
                | `U_straight t
                | `D_straight t ->
                    let d' = d -. abs_float (t -. prev_t) in
                    (t, d', p)
                | `D_side (t, max_len_up) ->
                    assert (t <= prev_t) ;
                    let d' = d -. abs_float (t -. prev_t) in
                    let p' =
                      if d' <= max_len_up then
                        F.Proba.mul half p
                      else
                        p
                    in
                    (t, d', p')
                | `D_left (t, max_len_side)
                | `D_right (t, max_len_side) ->
                    assert (prev_t <= t) ;
                    let d' = d -. abs_float (t -. prev_t) in
                    let p' =
                      if d' <= max_len_side then
                        F.Proba.mul half p
                      else
                        p
                    in
                    (t, d', p')
                | `U_end (t, _)
                | `D_end (t, _) ->
                    let d' = d -. abs_float (t -. prev_t) in
                    (t, d', p)
              in
              match moves with
              | [] ->
                  failwith "no moves"
              | mv :: tl_mv ->
                  begin match mv with
                  | `U_start (t, max_len_other)
                  | `D_start (t, max_len_other) ->
                      let p =
                        if d <= max_len_other then
                          half
                        else
                          F.one
                      in
                      L.fold_left f (t, d, p) tl_mv
                  | _ ->
                      invalid_arg "invalid first move"
                  end

            let reverse_move : move -> move =
              function
              | `U_start (t, max_len_down) ->
                  `D_end (t, max_len_down)
              | `D_end (t, max_len_down) ->
                  (* max_len_down in D_end useful for here *)
                  `U_start (t, max_len_down)
              | `D_start (t, max_len_up) ->
                  `U_end (t, max_len_up)
              | `U_end (t, max_len_up) ->
                  (* max_len_up in U_end useful for here *)
                  `D_start (t, max_len_up)
              | `U_straight t ->
                  `D_straight t
              | `D_straight t ->
                  `U_straight t
              | (`D_side _) as move ->
                  move
              | `D_left (t, max_len_side) ->
                  `U_from_left (t, max_len_side)
              | `D_right (t, max_len_side) ->
                  `U_from_right (t, max_len_side)
              | `U_from_left (t, max_len_side) ->
                  `D_left (t, max_len_side)
              | `U_from_right (t, max_len_side) ->
                  `D_right (t, max_len_side)

            let dens_proba varc d moves =
              (* a normal draw with variance varc *)
              let dens = U.d_normal 0. varc d in
              (* examine the moves *)
              (* the last move comes first in the list *)
              let moves = L.rev moves in
              let _, _, p = p_moves d moves in
              F.Pos.mul dens p

            let log_pd_ratio d moves =
              (* the normal densities cancel each other *)
              (* forward path *)
              let fwd_moves = L.rev moves in
              let _, _, fwd_p = p_moves d fwd_moves in
              (* backward path *)
              let bwd_moves = L.map reverse_move moves in
              let _, _, bwd_p = p_moves d bwd_moves in
              F.Op.(F.log bwd_p - F.log fwd_p)
          end
      end

    module Spr =
      struct
        (* Subtree-Prune and Regraft proposals on trees with [n] leaves,
           and [n - 1] interior nodes where the cut can happen *)
        module Uniform =
          struct
            type context = {
              x_prune : T.state ;
              x_prune_parent : T.state ;
              x_graft_parent : T.state ;
              length : float ;
            }
              
            (* any interior node is chosen with the same probability,
               then the graft-point is chosen uniformly along the tree *)

            let draw ninterior rng tree =
              (* prune *)
              let (z, x_prune, _) as ztp = Prune.Uniform.draw
                ninterior
                rng
                tree
              in
              let x_prune_parent = Z.state z in
              (* graft *)
              let z, length = Graft.Uniform.draw rng ztp in
              let x_graft_parent = Z.state (Z.move_up z) in
              let tree = Z.remove_cursor z in
              let context = {
                x_prune ;
                x_prune_parent ;
                x_graft_parent ;
                length
              }
              in
              (tree, context)

            let dens_proba ninterior _ (_, ctxt) =
              let d = Prune.Uniform.dens_proba ninterior in
              let len = F.Pos.of_float ctxt.length in
              let d' =  Graft.Uniform.dens_proba len in
              F.Pos.mul d d'

            (* The ratio is null because the move is already reversible
             * In both directions, the move is :
             * - pick an interior node uniformly.
             *   There is the same number of nodes both ways.
             * - pick a point uniformly along the tree
             *   minus the pruned sub tree.
             *   This intermediary tree is the same both ways.
             * CQFD
             *)
            let log_pd_ratio _ _ =
              F.zero

            let move_to_sample (tree, _) =
              tree

            let move ninterior =
              Fit.Propose.Base {
                draw = draw ninterior ;
                log_pd_ratio = (fun x y -> [F.to_float (log_pd_ratio x y)]) ;
                move_to_sample
              }
          end
      
        module Divergence =
          struct
            (* context for a divSPR move *)
            type context = {
              x_prune : T.state ;
              x_prune_parent : T.state ;
              x_graft_parent : T.state ;
              t_graft : float ;
              distance : float ;
              moves : Graft.Divergence.move list ;
            }

            let draw ninterior varc rng tree =
              let z, x_prune, _ as zxp = Prune.Uniform.draw
                ninterior
                rng
                tree
              in
              let x_prune_parent = Z.state z in
              match Graft.Divergence.draw varc rng zxp with
              | z, t_graft, distance, moves ->
                  let x_graft_parent = Z.state (Z.move_up z) in
                  let context = {
                    x_prune ;
                    x_prune_parent ;
                    x_graft_parent ;
                    t_graft ;
                    distance ;
                    moves ;
                  }
                  in
                  Z.remove_cursor z, context
              | exception (Failure _ as e) ->
                  Fit.raise_draw_error e

            let dens_proba ninterior varc _ (_, ctxt) =
              let d = Prune.Uniform.dens_proba ninterior in
              let d' = Graft.Divergence.dens_proba
                varc
                ctxt.distance
                ctxt.moves
              in
              F.Pos.mul d d'

            let log_pd_ratio _ (_, ctxt) =
              (* The two uniform prunes cancel each other out *)
              Graft.Divergence.log_pd_ratio ctxt.distance ctxt.moves

            let move_to_sample (tree, _) =
              tree

            let move ninterior varc =
              Fit.Propose.Base {
                draw = draw ninterior varc ;
                log_pd_ratio = (fun x y -> [F.to_float (log_pd_ratio x y)]) ;
                move_to_sample ;
              }
          end
      end
  end

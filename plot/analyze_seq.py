#!/usr/bin/python3
# -*- coding: utf-8 -*-

# we have a file where each line contains a time
# and a ',' separated list of {trait value}:{number of carriers}.
# we want to plot that in time
# (more intense color where there are more individuals)
# I know I got pretty good plots of this kind of thing using pyqtgraph
# in Vancouver

def read_csv(fname) :
  times = list()
  values = list()
  utimes = list()
  totals = list()
  with open(fname, 'r') as fich :
    for line in fich :
      splits = line.split(",")
      t = float(splits[0])
      c = 0
      for s in splits[1:] :
        subsplit = s.split(":")
        val = float(subsplit[0])
        count = int(subsplit[1])
        c += count
        for i in range(count) :
          times.append(t)
          values.append(val)
      utimes.append(t)
      totals.append(c)

  return times, values, utimes, totals


def plot(times, values, ax) :
  ax.plot(
    times, values,
    marker='.',
    linestyle='None',
    alpha=0.1,
  )

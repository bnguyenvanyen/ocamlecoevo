#!/usr/bin/python3
# -*- coding: utf-8 -*-

import argparse
from math import floor

import numpy as np
import pandas as pd

from diagnosis import single_ess, multi_ess


parser = argparse.ArgumentParser(
  description=
  "Write ESS of different MCMC runs."
)

parser.add_argument(
  "--read-template",
  default="{}",
  help="template string to pass the 'read' arguments through."
)

parser.add_argument(
  "--read",
  nargs=2,
  action="append",
  help="Each element (label, s) specifies a location"
       "to be read as '{read_template}.format(s)'."
)

parser.add_argument(
  "--burn",
  type=int,
  default=0,
  help="Plot only for iterations > {burn}."
)

parser.add_argument(
  "--multiples",
  type=int,
  nargs=2,
  default=[1, 0],
  help="with multiples '(mult, rem)', ignore samples with "
       "'k mod mult != rem'.",
)

parser.add_argument(
  "--parameters",
  nargs="+",
  help="Parameters to compute (m)ESS for."
)

parser.add_argument(
  "--out-template",
  help="Optional template for output files. By default, re-use {read_template}."
)


def not_log_prior_lik(params) :
  return [ p for p in params if p not in ["logprior", "loglik"] ]


args = parser.parse_args()

print("Parameters:", args.parameters)


read_pairs = [ (label, args.read_template.format(read))
               for (label, read) in args.read ]

read_labels = [ lab for (lab, _) in read_pairs ]

print("Labels:", read_labels)



def read(s) :
  print("read", s)
  try :
    return pd.read_csv(
      s,
      delimiter=',',
      header=0
    )
  except FileNotFoundError :
    return None


df_d = {
  label : read(readv)
  for (label, readv) in read_pairs
  if read(readv) is not None
}


for label, read_v in read_pairs :
  try :
    df_d[label]["label"] = label
  except KeyError :
    pass


df = pd.concat(
  list(df_d.values()),
  ignore_index=True,
  sort=False,
)


burn = args.burn
multiples, remainder = args.multiples

cond = ((df["k"] > args.burn) & (df["k"] % multiples == remainder))

df = df[(cond)]

cols = not_log_prior_lik(args.parameters)

ess = dict()
for lab in read_labels :
  ess[lab] = dict()

## Multi ESS
if len(cols) > 0 :
  for lab in read_labels :
    mess = multi_ess(
      df[df["label"] == lab],
      cols
    )
    ess[lab]["multi"] = mess
    print("The multiESS for the label", lab, "is", mess)


## Single ESS
for col in args.parameters :
  print("For column", col, ":")
  for lab in read_labels :
    sub = df[df["label"] == lab]
    sess = single_ess(
      sub,
      col
    )
    ess[lab][col] = sess
    print("The ESS for the label", lab, "is", sess)

dfs_ess = {
  lab : pd.DataFrame.from_records([ess[lab]])
  for lab in read_labels
}

print(dfs_ess)

## Min ESS
for lab in read_labels :
  dfs_ess[lab] = dfs_ess[lab].assign(**{
    "min":dfs_ess[lab][args.parameters].min(axis="columns")
  })


if args.out_template :
  tmpl = args.out_template
if not args.out_template :
  if args.read_template[-10:] == ".theta.csv" :
    tmpl = args.read_template[:-10] + ".ess.csv"
  elif args.read_template[-13:] == ".theta_kc.csv" :
    tmpl = args.read_template[:-13] + ".ess.csv"
  else :
    raise ValueError("No 'out-template' and unexpected 'read-template'")

for lab, read in args.read :
  out = tmpl.format(read)
  df_ess = dfs_ess[lab]
  df_ess.to_csv(
    out,
    index=False
  )

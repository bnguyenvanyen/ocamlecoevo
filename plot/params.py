#!/usr/bin/python3
# -*- coding: utf-8 -*-

import argparse
from math import floor

import numpy as np
import pandas as pd
import matplotlib
from matplotlib import pyplot as plt
import seaborn as sb

from diagnosis import single_ess, multi_ess
import parse
import aspect as asp
import context
import formatter

plt.switch_backend("Qt5Agg")


parser = argparse.ArgumentParser(
  description=
  "Compare parameter estimations from different MCMC runs."
)

parser.add_argument(
  "--read-template",
  default="{}",
  help="template string to pass the 'read' arguments through."
)

parser.add_argument(
  "--read",
  nargs=2,
  action="append",
  help="Each element (label, s) specifies a location"
       "to be read as '{read_template}.format(s)'."
)

parser.add_argument(
  "--read-label",
  default="posterior",
  help="Label to put on the legend."
)

parser.add_argument(
  "--target",
  help="Target parameter values."
)

parser.add_argument(
  "--burn",
  type=int,
  default=0,
  help="Plot only for iterations > {burn}."
)

parser.add_argument(
  "--until",
  type=int,
  help="Plot only for iterations <= {until}."
)

parser.add_argument(
  "--multiples",
  type=int,
  nargs=2,
  default=[1, 0],
  help="with multiples '(mult, rem)', ignore iterations with "
       "'k mod mult != rem'.",
)

parser.add_argument(
  "--parameters",
  nargs="+",
  help="Parameters to plot for."
)

parser.add_argument(
  "--replace-parameter",
  action="append",
  nargs=3,
  metavar=("label", "param", "stand-in"),
  help="Use an alternate column for one label."
)

parser.add_argument(
  "--format-parameter",
  action="append",
  nargs=2,
  default=[],
  metavar=("param", "fmt"),
  help="Format string for this parameter's values."
)

parser.add_argument(
  "--rotate-parameter",
  action="append",
  nargs=2,
  default=[],
  metavar=("param", "angle"),
  help="Rotate this parameter's ticks by 'angle' on the pair plot x-axis."
)

parser.add_argument(
  "--accepted-only",
  action="store_true",
  help="Only plot accepted samples (old MCMC run compatibility).",
)

parser.add_argument(
  "--out",
  help="Optional path to output. if not given, interactive plot.",
)

parser.add_argument(
  "--triangular",
  action="store_true",
  help="Plot only the lower triangle"
)

parser.add_argument(
  "--range",
  action="append",
  nargs=3,
  default=[],
  metavar=("param", "lower", "upper"),
  help="Set the plot range for the parameter."
)

parser.add_argument(
  "--rename",
  action="append",
  nargs=2,
  default=[],
  metavar=("param", "label"),
  help="Label to use on the plots for this parameter."
)

parser.add_argument(
  "--no-share-k",
  action="store_true",
  help="Do not share the k values on the x-axis for the trace plot."
)

parser.add_argument(
  "--trace-superpose",
  action="store_true",
  help="Superpose trace plots."
)

parser.add_argument(
  "--violin-ncol",
  type=int,
  default=2,
  help="Number of columns to wrap at for the violin plot."
)

parser.add_argument(
  "--no-trace",
  action="store_true",
  help="Don't plot the traces.",
)

parser.add_argument(
  "--no-pair",
  action="store_true",
  help="Don't plot the pair scatterplots.",
)

parser.add_argument(
  "--no-violin",
  action="store_true",
  help="Don't plot the violin plots.",
)

parser.add_argument(
  "--colors",
  type=int,
  nargs=2,
  help="Number of colors to use in the read palette and which to start from."
)

parser.add_argument(
  "--alpha",
  type=float,
  default=0.5,
  help="Opacity for points in the pair plot."
)

parse.add_argument_context(parser)

parse.add_argument_latex(parser)

parse.add_argument_width(parser)

parse.add_argument_dpi(parser)

parser.add_argument(
  "--split-labels",
  action="store_true",
  help="Split the pair plot by read label.",
)

parser.add_argument(
  "--split-params",
  action="store_true",
  help="Split the pair plot by parameter.",
)


def not_log_prior_lik(params) :
  return [ p for p in params if p not in ["logprior", "loglik"] ]


def set_ax_limits(g, row_params, col_params, ranges, triangular) :
  for (par, lower, upper) in ranges :
    try :
      i = row_params.index(par)
      for (j, cpar) in enumerate(col_params) :
        if par != cpar and (not triangular or i >= j) :
          g.axes[i,j].set_ylim(lower, upper)
    except ValueError :
      pass

    try :
      j = col_params.index(par)
      for (i, _) in enumerate(row_params) :
        if (not triangular) or (i >= j) :
          g.axes[i,j].set_xlim(lower, upper)
    except ValueError :
      pass


def hist_scaled(x, bins, color, scl=1., **kwargs) :
  hist, bin_edges = np.histogram(x, bins)
  bin_centers = (bin_edges[:-1] + bin_edges[1:]) / 2.
  tot = hist.sum()
  scl_hist = hist / tot
  plt.fill_between(bin_centers, scl_hist, color=color, alpha=0.75)


def split_pair_plot(df, df_target, label_col, labels,
                    i, j, row_param, col_param, ranges=None,
                    palette=None, color=None, alpha=0.5, width=None) :
  data=df[df[label_col].isin(labels)]

  if palette and not color :
    kw = {
      "palette": palette,
    }
  elif not palette and color :
    kw = {
      "color": color,
    }
  elif not palette and not color :
    kw = {}
  else :
    raise ValueError("Incompatible palette and color both given")

  aspect = 1.618

  if width :
    # compute height to get the right width (in pts) is wanted
    w = asp.pts_to_inches(width)
    height = asp.facet_height(
      ncol=1,
      width=w * 0.9,
      aspect=aspect
    )
    h = height * 1.1
  else :
    # default value
    width = 2.5
    height = 2.5

  f, ax = plt.subplots(figsize=(w, h))

  if row_param == col_param :
    ax = sb.histplot(
      data=data,
      x=col_param,
      hue=label_col,
      hue_order=labels,
      stat="probability",
      bins=25,
      common_norm=True,
      ax=ax,
      **kw
    )
  else :
    if i > j :
      ax = sb.kdeplot(
        data=data,
        x=col_param,
        y=row_param,
        hue=label_col,
        hue_order=labels,
        alpha=0.7,
        levels=6,
        common_norm=False,
        ax=ax,
        **kw
      )
    else :
      ax = sb.scatterplot(
        data=data,
        x=col_param,
        y=row_param,
        hue=label_col,
        hue_order=labels,
        alpha=alpha,
        marker=',',
        s=.4,
        ax=ax,
        **kw
      )

  ax.legend(
    frameon=False
  )

  # set ax limits
  # Should do it outside the function
  rg = { par : (low, up) for (par, low, up) in ranges }
  try :
    low, up = rg[col_param]
    ax.set_xlim(low, up)
  except KeyError :
    pass

  if row_param != col_param :
    try :
      low, up = rg[row_param]
      ax.set_ylim(low, up)
    except KeyError :
      pass

  # better labels
  try :
    lab = param_labels[col_param]
    ax.set_xlabel(lab)
  except KeyError :
    pass

  if row_param != col_param :
    try :
      lab = param_labels[row_param]
      ax.set_ylabel(lab)
    except KeyError :
      pass

  def plot_targets() :
    try :
      x = df_target[col_param].values[0]
      ax.axvline(x=x, color='black', linewidth=1)
    except KeyError :
      pass

    if row_param != col_param :
      try :
        y = df_target[row_param].values[0]
        ax.axhline(y=y, color='black', linewidth=1)
      except KeyError :
        pass

  try :
    plot_targets()
  except TypeError :
    pass

  return f

      
def pair_plot(df, df_target, label_col, labels,
              row_params, col_params, triangular=False, ranges=None,
              palette=None, color=None, alpha=0.5, width=None) :
  aspect = 1.
  if width :
    # compute height to get the right width (in pts) is wanted
    height = asp.facet_height(
      ncol=len(col_params),
      width=asp.pts_to_inches(width),
      aspect=aspect
    )
  else :
    height = 2.5  # default value

  print("Requested facet height:", height)
  print("nrow * height:", height * len(row_params))


  g = sb.PairGrid(
    df[df[label_col].isin(labels)],
    hue=label_col,
    hue_order=labels,
    x_vars=row_params,
    y_vars=col_params,
    palette=palette,
    corner=triangular,
    despine=triangular,
    aspect=aspect,
    height=height
  )

  if len(ranges) > 0 :
    set_ax_limits(g, row_params, col_params, ranges, triangular)

  if color :
    kw = {"color" : color}
  else :
    kw = {}

  try :
    g = g.map_diag(hist_scaled, color=color, bins=25)
    # Could be better but bins are used outside range
    # g = g.map_diag(
    #   sb.histplot,
    #   stat="probability",
    #   common_norm=False,
    #   bins=25,
    #   alpha=0.75,
    #   **kw
    # )
  except (TypeError, ValueError, IndexError) :
    # sometimes this fails (on split_params for example)
    pass

  g = g.map_lower(
    plt.scatter,
    alpha=alpha,
    marker=',',
    s=.2,
    **kw
  )

  if not triangular :
    g = g.map_upper(
      sb.kdeplot,
      alpha=0.7,
      levels=4,
      common_norm=False,
      **kw
    )

  # Use better axis labels
  for ax, par in zip(g.axes[-1, :], col_params) :
    try :
      lab = param_labels[par]
      ax.set_xlabel(lab)
    except KeyError :
      pass

  for ax, par in zip(g.axes[:, 0], row_params) :
    try :
      lab = param_labels[par]
      ax.set_ylabel(lab)
    except KeyError :
      pass

  # Use better axis formatter
  for ax, par in zip(g.axes[-1, :], col_params) :
    try :
      fmt = param_formats[par]
      fmtr = formatter.Scalar(format=fmt, useOffset=False)
    except KeyError :
      fmtr = formatter.Scalar(useOffset=False)
    fmtr.set_scientific(False)
    ax.xaxis.set_major_formatter(fmtr)

  for ax, par in zip(g.axes[:, 0], row_params) :
    try :
      fmt = param_formats[par]
      fmtr = formatter.Scalar(format=fmt, useOffset=False)
    except KeyError :
      fmtr = formatter.Scalar(useOffset=False)
    fmtr.set_scientific(False)
    ax.yaxis.set_major_formatter(fmtr)

  # Maybe rotate some ticks
  for ax, par in zip(g.axes[-1, :], col_params) :
    try :
      angle = param_rotations[par]
      plt.setp(
        ax.get_xticklabels(),
        rotation=angle,
        ha="right"
      )
    except KeyError :
      pass

  # if the figure is not triangular,
  # put axis labels on the top and right too
  if not triangular :
    ## on the top
    for ax, par in zip(g.axes[0, :], col_params) :
      try :
        lab = param_labels[par]
      except KeyError :
        lab = par
      ax.set_xlabel(lab)
      ax.xaxis.label.set_visible(True)
      ax.xaxis.set_label_position("top")

    ## on the right
    # If the figure is not triangular,
    # the legend is out of the plot, and center left
    # Then it overlaps with the label of the center facet
    # on the right when the number of rows is odd.
    # In that case, we don't plot the label
    is_odd = (len(row_params) % 2 == 1)
    center = len(row_params) // 2
    for i, (ax, par) in enumerate(zip(g.axes[:, -1], row_params)) :
      if is_odd and i == center :
        pass
      else :
        try :
          lab = param_labels[par]
        except KeyError :
          lab = par
        ax.set_ylabel(lab)
        ax.yaxis.label.set_visible(True)
        ax.yaxis.set_label_position("right")

  # Fix label alignment
  g.fig.align_xlabels(g.axes[-1, :])
  g.fig.align_ylabels(g.axes[:, 0])

  # if corner, put the legend in the plot
  # (no way to acces this through the PairGrid call)
  if triangular :
    # g._legend_out = False

    # FIXME wrong legend placement (inside first ax)
    g = g.add_legend(
      frameon=False,
      # safest location
      loc="upper right",
    )
  else :
    # FIXME Here I should let seaborn add the legend
    g = g.add_legend(
      frameon=False,
    )

  pal = g.palette

  def improve_legend() :
    # set alpha and size for markers in legend
    for lh in g._legend.legendHandles :
      lh.set_alpha(1)
      lh._sizes = [100]

  improve_legend()

  def plot_targets() :
    # plot black lines for target
    for i, nmi in enumerate(row_params) :
      for j, nmj in enumerate(col_params) :
        if (not triangular) or (i >= j) :
          try :
            x = df_target[nmj].values[0]
            g.axes[i, j].axvline(x=x, color='black', linewidth=1)
          except KeyError :
            pass
        if (not triangular and i != j) or (triangular and i > j) :
          try :
            y = df_target[nmi].values[0]
            g.axes[i, j].axhline(y=y, color='black', linewidth=1)
          except KeyError :
            pass

  try :
    plot_targets()
  except TypeError :
    print("No targets to plot")

  # Problem with figure size
  asp.set_width_aspect(g.fig, width)

  return g


def trace_plot(df, label_col, labels, params,
               superpose, share_k=True, width=None) :
  data=df.melt(
    id_vars=[label_col, "k"],
    value_vars=params,
    var_name="param",
    value_name="value",
  )

  if superpose :
    legend = "full"
  else :
    # the columns give the info
    legend = False

  aspect = 2.

  if width :
    # a total width is wanted
    if superpose :
      ncol = 1
    else :
      ncol = len(data[label_col].unique())
    height = asp.facet_height(
      width=asp.pts_to_inches(width),
      ncol=ncol,
      aspect=aspect
    )
  else :
    height = 2.5  # default value

  if superpose :
    kw_col = {}
  else :
    kw_col = {
      "col":label_col,
      "col_order":labels,
    }

  g = sb.relplot(
    kind="line",
    x="k",
    y="value",
    hue=label_col,
    hue_order=labels,
    row="param",
    facet_kws={
      "sharey":False,
      "sharex":share_k,
      # "legend_out":True,
    },
    data=data,
    aspect=aspect,
    height=height,
    legend=legend,
    **kw_col
  )
  # No ax titles
  g = g.set_titles("")

  # Use better y axis labels
  for ax, par in zip(g.axes[:, 0], params) :
    try :
      lab = param_labels[par]
      ax.set_ylabel(lab)
    except KeyError :
      ax.set_ylabel(par)

  # Don't use offsets
  for ax_row in g.axes :
    for ax in ax_row :
      ax.ticklabel_format(axis="y", useOffset=False)

  # Use scientific style more easily for k
  for ax in g.axes[-1, :] :
    ax.ticklabel_format(axis="x", scilimits=(-4, 4))

  def plot_targets() :
    for i, nmi in enumerate(params) :
      try :
        y = df_target[nmi].values[0]
        for ax in g.axes[i, :].flat :
          ax.axhline(y=y, color='black', linewidth=1)
      except KeyError :
        pass

  # Fix y-label alignment
  g.fig.align_ylabels(g.axes[:, 0])

  try :
    plot_targets()
  except TypeError :
    print("No targets to plot")

  # I only use superpose for the KC figure, so I cheat here
  # to place the legend correctly
  if superpose :
    g.legend.set_bbox_to_anchor((0.95, 0.85))

  # Problem with figure size
  asp.set_width_aspect(g.fig, width)

  g.fig.tight_layout(
    pad=0.4,
    h_pad=0.1,
    w_pad=0.1,
  )

  return g


def violin_plot(df, label_col, labels, params, ncol, width=None) :
  data=df.melt(
    id_vars=[label_col, "k"],
    value_vars=params,
    var_name="param",
    value_name="value",
  )

  # the aspect depends on the number of labels
  aspect = len(labels) / 2
  if width :
    # a total width is wanted
    height = asp.facet_height(
      ncol=ncol,
      width=asp.pts_to_inches(width),
    )
  else :
    height = 2.5  # default value

  g = sb.catplot(
    kind="violin",
    x=label_col,
    y="value",
    hue=label_col,
    col="param",
    col_wrap=ncol,
    sharey=False,
    data=data,
    height=height,
    aspect=aspect,
  )

  # Use better y axis labels
  for ax, par in zip(g.axes.flat, params) :
    try :
      lab = param_labels[par]
      ax.set_ylabel(lab)
    except KeyError :
      ax.set_ylabel(par)

  # Remove facet titles
  g = g.set_titles("")

  # Don't use offsets
  for ax in g.axes.flat :
    ax.ticklabel_format(axis="y", useOffset=False)

  def plot_targets() :
    for i, nmi in enumerate(params) :
      try :
        y = df_target[nmi].values[0]
        # axes one dim because col_wrap
        g.axes[i].axhline(
          y=y,
          color='black',
          linewidth=1
        )
      except KeyError :
        pass

  # Fix y-label alignment
  g.fig.align_ylabels(g.axes)

  try :
    plot_targets()
  except TypeError :
    print("No targets to plot")

  # Problem with figure size
  asp.set_width_aspect(g.fig, width)

  g.fig.tight_layout(
    pad=0.4,
    h_pad=0.1,
    w_pad=0.1,
  )

  return g


args = parser.parse_args()


print("Parameters:", args.parameters)


context.set(args.context)

if args.latex :
  context.set_latex()


if len(args.read) == 0 :
  read_args = [
    ("prior", "none.none"),
    ("cases", "negbin.none"),
    ("coal", "none.negbin"),
    ("joint", "negbin.negbin"),
  ]
else :
  read_args = args.read


read_pairs = [ (label, args.read_template.format(read))
               for (label, read) in read_args ]

read_labels = [ lab for (lab, _) in read_pairs ]

print("Labels:", read_labels)

label_col = args.read_label


def read(s) :
  print("read", s)
  try :
    return pd.read_csv(
      s,
      delimiter=',',
      header=0
    )
  except FileNotFoundError :
    return None


df_d = {
  label : read(readv)
  for (label, readv) in read_pairs
  if read(readv) is not None
}


for label, read_v in read_pairs :
  try :
    df_d[label][label_col] = label
  except KeyError :
    pass


# Overwrite replaced columns
if args.replace_parameter is not None :
  for label, param, replace in args.replace_parameter :
    df_d[label][param] = df_d[label][replace]


df = pd.concat(
  list(df_d.values()),
  ignore_index=True,
  sort=False,
)


burn = (df["k"] > args.burn)
mult, rem = args.multiples
mult = (df["k"] % mult == rem)

if args.accepted_only :
  accept = (df["rejected"] == "accept")
  cond = (burn & mult & accept)
else :
  cond = (burn & mult)

if args.until :
  until = (df["k"] <= args.until)
  cond = (cond & until)

df_hist = df[(cond)]


try :
  df_target = pd.read_csv(args.target, delimiter=',', header=0)
except (FileNotFoundError, ValueError) :
  df_target = None


if args.colors :
  n_colors, start_color = args.colors
else :
  n_colors = len(read_labels)
  start_color = 0

pal = sb.color_palette(n_colors=n_colors)

glob_palette = { lab : pal[i + start_color]
                 for i, lab in enumerate(read_labels) }


ranges = [
  (par, float(lower), float(upper))
  for (par, lower, upper) in args.range
  if par in args.parameters
]

param_labels = {
  par: name
  for (par, name) in args.rename
  if par in args.parameters
}

param_formats = {
  par : fmt
  for (par, fmt) in args.format_parameter
  if par in args.parameters
}

param_rotations = {
  par : float(angle)
  for (par, angle) in args.rotate_parameter
  if par in args.parameters
}


# ESS stuff

## Multi ESS
cols = not_log_prior_lik(args.parameters)
if len(cols) > 0 :
  for lab in read_labels :
    mess = multi_ess(
      df_hist[df_hist[label_col] == lab],
      not_log_prior_lik(args.parameters)
    )
    print("The multiESS for the label", lab, "is", mess)


## Single ESS (including for 'logprior' 'loglik')
if len(args.parameters) > 0 :
  for col in args.parameters :
    print("For column", col, ":")
    for lab in read_labels :
      sub = df_hist[df_hist[label_col] == lab]
      ess = single_ess(
        sub,
        col
      )
      print("The ESS for the label", lab, "is", ess)


# Expected values
for col in args.parameters :
  print("For column", col, ":")
  for lab in read_labels :
    sub = df_hist[df_hist[label_col] == lab]
    print("The mean for the label", lab, "is", sub[col].mean())

###


if not args.no_trace :
  g_trace = trace_plot(
    df_hist,
    label_col=label_col,
    labels=read_labels,
    params=args.parameters,
    share_k=not args.no_share_k,
    superpose=args.trace_superpose,
    width=args.width,
  )


if not args.no_violin :
  g_violin = violin_plot(
    df_hist,
    label_col=label_col,
    labels=read_labels,
    params=args.parameters,
    ncol=args.violin_ncol,
    width=args.width,
  )


if not args.no_pair :
  if (not args.split_labels) and (not args.split_params) :
    g_pair = pair_plot(
      df_hist,
      df_target,
      label_col=args.read_label,
      labels=read_labels,
      row_params=args.parameters,
      col_params=args.parameters,
      triangular=args.triangular,
      ranges=ranges,
      palette=glob_palette,
      alpha=args.alpha,
      width=args.width,
    )
  elif args.split_labels and (not args.split_params) :
    g_pairs = {
      label : pair_plot(
        df_hist,
        df_target,
        label_col=args.read_label,
        labels=[label],
        row_params=args.parameters,
        col_params=args.parameters,
        triangular=args.triangular,
        ranges=ranges,
        alpha=args.alpha,
        color=glob_palette[label],
        width=args.width,
      )
      for label in read_labels
    }
  elif (not args.split_labels) and args.split_params :
    g_pairs = {
      row_par : {
        col_par : split_pair_plot(
          df_hist,
          df_target,
          label_col=args.read_label,
          labels=read_labels,
          i=i, j=j,
          row_param=row_par,
          col_param=col_par,
          ranges=ranges,
          alpha=args.alpha,
          palette=glob_palette,
          width=args.width,
        )
        for j, col_par in enumerate(args.parameters)
      }
      for i, row_par in enumerate(args.parameters)
    }
  else :  # split both
    g_pairs = {
      label : {
        row_par : {
          col_par : split_pair_plot(
            df_hist,
            df_target,
            label_col=args.read_label,
            labels=[label],
            i=i, j=j,
            row_param=row_par,
            col_param=col_par,
            ranges=ranges,
            alpha=args.alpha,
            color=glob_palette[label],
            width=args.width,
          )
          for col_par in args.parameters
        }
        for row_par in args.parameters
      }
      for label in read_labels
    }


def save(g, out, dpi=600, **kw) :
  try :
    print("Size:", g.fig.get_size_inches())
  except AttributeError :
    print("Size:", g.get_size_inches())
  g.savefig(out, dpi=dpi, **kw)


if args.out :
  print(args.out)
  if not args.no_trace :
    save(g_trace, args.out.format("trace"), dpi=args.dpi)
    
  if not args.no_violin :
    save(g_violin, args.out.format("violin"), dpi=args.dpi)

  if not args.no_pair :
    if args.split_labels and args.split_params :
      for label, gpars in g_pairs.items() :
        for row_par, gcols in gpars.items() :
          for col_par, g in gcols.items() :
            save(
              g,
              args.out.format("{}.pair.{}.{}".format(label, row_par, col_par)),
              dpi=args.dpi,
              bbox_inches="tight",
            )
    elif args.split_labels and (not args.split_params) :
      for label, g in g_pairs.items() :
        save(
          g,
          args.out.format("{}.pair".format(label)),
          dpi=args.dpi
        )
    elif (not args.split_labels) and args.split_params :
      for row_par, gcols in g_pairs.items() :
        for col_par, g in gcols.items() :
          save(
            g,
            args.out.format("pair.{}.{}".format(row_par, col_par)),
            dpi=args.dpi,
            bbox_inches="tight",
          )
    else :
      save(
        g_pair,
        args.out.format("pair"),
        dpi=args.dpi
      )
else :
  plt.show()

#!/usr/bin/python3
# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd


def pairify(df, label_col) :
  # for one fname, one k, one t, we have a number of colors,
  # each with a density
  # we double that into two colors,
  # to use with relplot and for cross-correlations
  paired = pd.merge(
    df, df,
    how="inner",
    on=["fname", label_col, "k", "origin_t"],
  )
  return paired.sort_values(by=[label_col, "k", "origin_t"])


# compute (auto and) cross-correlations between color densities
# on aggregated dataframe
def correlate(df) :
  def f(group) :
    x = group["density_x"] - 1
    y = group["density_y"] - 1
    n = len(x)
    corr = np.correlate(x, y, mode='full')[-n:]
       
    return group.assign(corr=corr)

  return df.fillna({
    "fname" : "",
    "k" : -1,
    "color_x" : "",
    "color_y" : ""
  }).groupby(
    ["fname", "k", "color_x", "color_y"],
    as_index=False,
    sort=False,
  ).apply(
    f
  ).reset_index(
    drop=True
  ).replace(
    {"fname":"", "k":-1, "color_x":"", "color_y":""},
    np.nan
  )

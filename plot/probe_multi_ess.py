#!/usr/bin/python3
# -*- coding: utf-8 -*-

import argparse
import numpy as np
import pandas as pd
import seaborn as sb
from matplotlib import pyplot as plt

from diagnosis import multi_ess
from read_trajs import read_all


parser = argparse.ArgumentParser(
  description="Multi ESS of a MCMC run"
)


parser.add_argument(
  "--run-template",
  default="{}",
  help="template string to pass 'run' elements through.",
)

parser.add_argument(
  "--run",
  nargs=2,
  action="append",
  help="MCMC run root name.",
)

parser.add_argument(
  "--parameters",
  nargs="+",
  help="parameters to take into account for computing multi ESS."
)

parser.add_argument(
  "--compartments",
  nargs="+",
  help="compartments to take into account for computing multi ESS."
)

parser.add_argument(
  "--thin-times",
  type=int,
  default=1,
  help="Keep one time sample every n samples."
)

parser.add_argument(
  "--n-batches",
  type=int,
  default=100,
  help="Number of batches for batch means sample cov."
)

parser.add_argument(
  "--load",
  choices=["glob", "prm_mcmc", "fintzi", "ssm"],
  default="glob",
)

parser.add_argument(
  "--save-trajs",
  action="store_true",
)

parser.add_argument(
  "--burn",
  type=int,
  default=0,
  help="ignore trajectory files with 'k < {burn}'.",
)

parser.add_argument(
  "--plot",
  action="store_true",
)


def run_trajs(template, run) :
  return "{}.*.traj.csv".format(
    template.format(run)
  )


def run_params(template, run) :
  return "{}.theta.csv".format(
    template.format(run)
  )


def trajs_path(template, run) :
  return "{}.trajs.csv".format(
    template.format(run)
  )


args = parser.parse_args()


patterns = [ (lab, run_trajs(args.run_template, run))
             for (lab, run) in args.run ]

reads = [ (lab, run_params(args.run_template, run))
          for (lab, run) in args.run ]


# Load trajs

if args.load == "prm_mcmc" :
  df_trajs = pd.concat(
    [ pd.read_csv(trajs_path(args.run_template, run)) for _, run in args.run ]
  )
  df_trajs = df_trajs[df_trajs["k"] > args.burn]
elif args.load == "fintzi" :
  df_trajs = pd.concat(
    [ pd.read_csv(trajs_path(args.run_template, run)) for _, run in args.run ]
  )
  df_trajs = df_trajs[df_trajs["k"] > args.burn]
  df_trajs = df_trajs.melt(
    id_vars=["k", "t"],
    value_vars=["S", "I", "R"],
    var_name="compartment",
    value_name="count",
  )
elif args.load == "ssm" :
  df_trajs = pd.concat(
    [ pd.read_csv(
        trajs_path(args.run_template, run),
        parse_dates=["date"],
      ) for _, run in args.run ]
  )
  df_trajs = df_trajs.assign(k=df_trajs["index"])
  df_trajs = df_trajs.assign(t=df_trajs["date"])
  df_trajs = df_trajs[df_trajs["k"] > args.burn]
  df_trajs = df_trajs.melt(
    id_vars=["k", "t"],
    value_vars=["s", "i", "r"],
    var_name="compartment",
    value_name="count",
  )
else :
  df_trajs = read_all(
    patterns=patterns,
    focuses=[],
    pattern_label="run",
    compartments=args.compartments,
    burn=args.burn,
    multiples=1,
    remainder=0
  )

  if args.save_trajs :
    for lab, run in args.run :
      df_trajs[df_trajs["run"] == lab].to_csv(
        trajs_path(args.run_template, run)
      )



df_trajs = df_trajs[["k", "t", "compartment", "count"]]


# Load params

df_params_d = {
  label : pd.read_csv(readv)
  for (label, readv) in reads
}

for label, readv in reads :
  df_params_d[label]["run"] = label


df_params = pd.concat(
  list(df_params_d.values()),
  ignore_index=True,
  sort=False,
)


try :
  df_params = df_params.assign(k=df_params["index"])
except KeyError :
  pass

df_params = df_params[df_params["k"] > args.burn]


times = df_trajs["t"].unique()[::args.thin_times]


df_trajs_pivot = pd.pivot_table(
  df_trajs[df_trajs["t"].isin(times)],
  index="k",
  columns=["compartment", "t"],
  values="count"
)


# Dummy multi index for df_params

df_params_multi = df_params[["k"] + args.parameters]\
  .set_index("k")\
  .set_axis(
    pd.MultiIndex.from_tuples(
      [ (par, "single") for par in args.parameters ]
    ),
    axis="columns",
    inplace=False,
  )


df = pd.concat(
  [df_trajs_pivot, df_params_multi],
  axis=1,
  join="inner",
  keys=["trajs", "params"],
  sort=False,
)


print("Separate multi ESS")

mess_pars = multi_ess(
    df_params,
    cols=args.parameters,
    n_batches=args.n_batches
  )

mess_trajs = multi_ess(
    df_trajs_pivot,
    cols=df_trajs_pivot.columns,
    n_batches=args.n_batches
  )

mess_joint = multi_ess(
    df,
    cols=df.columns,
    n_batches=args.n_batches
  )

print("For parameters :", mess_pars)

print("For trajectories :", mess_trajs)

print("Joint multi ESS :", mess_joint)


melted = df.stack([0, 1, 2])\
           .reset_index()\
           .rename(columns={0:"value"})


def dimension(row) :
  return "{}_{}_{}".format(
    row["level_1"],
    row["compartment"],
    row["t"]
  )
 

melted["dimension"] = melted.apply(dimension, axis="columns")

subts = pd.concat([pd.Series(["single"]), pd.Series(times[::2])])

melted = melted[melted["t"].isin(subts)]

if args.plot :
  sb.relplot(
    data=melted,
    kind="line",
    x="k",
    y="value",
    col="dimension",
    col_wrap=3,
    facet_kws = { "sharey":False }
  )

  plt.show()

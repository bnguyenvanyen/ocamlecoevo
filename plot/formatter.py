#!/usr/bin/python3
# -*- coding: utf-8 -*-

import matplotlib.ticker

class Scalar(matplotlib.ticker.ScalarFormatter) :
  def __init__(self, format=None, **kw):
    self.fformat = format
    super().__init__(
      **kw
    )

  def _set_format(self):
    if self.fformat is not None :
      self.format = self.fformat
      if self._useMathText:
        self.format = '$%s$' % matplotlib.ticker._mathdefault(
            self.format
        )
    else :
      super()._set_format()

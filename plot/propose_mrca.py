#!/usr/bin/python3
# -*- coding: utf-8 -*-

import argparse
import re
from itertools import chain

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from Bio.Phylo import draw

from read import\
  read_matrix_traj,\
  read_fasta,\
  parse_time_stamp,\
  parse_matrix_position_hyphen

from forest import build_forest, finalize


plt.switch_backend("Qt5Agg")


parser = argparse.ArgumentParser(
  description="Plot a tree move from MRCAs"
)

parser.add_argument(
  "--samples",
  help="Path to CSV file of MRCA sample values."
)

parser.add_argument(
  "--moves",
  help="Path to CSV files of MRCA proposal values."
)

parser.add_argument(
  "--times",
  help="Lineage sampling times."
)

parser.add_argument(
  "--k",
  type=int,
  help="Which sample to plot."
)

parser.add_argument(
  "--t0",
  type=float,
  default=0.,
  help="Initial time."
)


def plot_tree(tree, d, ax) :
  rooted = finalize(tree, d)
  return draw(
    rooted,
    label_func=lambda c : str(c.name) if c.name else "",
    do_show=False,
    axes=ax
  )
  

def plot(sample, move, t0) :
  n = max(len(sample), len(move))

  f, ax = plt.subplots(n, 2, sharex=True, squeeze=False)

  x_max = 0

  for i, (t, tree) in enumerate(forest_sample.values()) :
    d = t - t0
    plot_tree(tree, d, ax[i, 0])
    ax[i,0].set_xlabel("")
    x_max = max(x_max, ax[i,0].get_xlim()[1])

  for i, (t, tree) in enumerate(forest_move.values()) :
    d = t - t0
    plot_tree(tree, d, ax[i, 1])
    ax[i,1].set_xlabel("")
    x_max = max(x_max, ax[i,0].get_xlim()[1])

  ax[n - 1, 0].set_xlabel("branch length")
  ax[n - 1, 1].set_xlabel("branch length")
  # set xlim for all
  ax[0, 0].set_xlim([0, x_max])

  plt.show()


args = parser.parse_args()

ts = read_fasta(args.times, lambda rec : parse_time_stamp(rec.id))
times = { i + 1 : t for i, t in enumerate(sorted(ts, reverse=True)) }

df_samples, samples = read_matrix_traj(
  args.samples,
  parse=parse_matrix_position_hyphen
)
df_moves, moves = read_matrix_traj(
  args.moves,
  parse=parse_matrix_position_hyphen
)

# row number for the right k
k = df_samples.loc[df_samples["k"] == args.k].index[0]

forest_sample = build_forest(times, samples[k])
forest_move = build_forest(times, moves[k])

plot(forest_sample, forest_move, args.t0)

#!/usr/bin/python3
# -*- coding: utf-8 -*-

def pts_to_inches(pts) :
  """Convert pts to inches."""
  return pts / 72.27


def facet_height(width, ncol=1, decoration_width=0.15, decoration_height=0.15, aspect=1.618) :
  """
  Determine a good facet height.

  To make a plot with `ncol` columns, each facet having aspect `aspect`,
  to take up a total width of `width`.
  """
  # looking for h'
  # h = w / aspect
  # width = ncol * w + decoration_width
  # height = nrow * h + decoration_height
  # h' = height / nrow
  # h = (width - decoration_width) / (ncol * aspect)
  # h' = h + decoration_height / nrow
  ## Fragile
  if decoration_width < 0. or decoration_width > 1. :
    raise ValueError("decoration_width must be between 0 and 1.")
  else :
    h = (1 - decoration_width) * width / (ncol * aspect)

  if decoration_height < 0. or decoration_height > 1. :
    raise ValueError("decoration_height must be between 0 and 1.")
  else :
    return h / (1 - decoration_height)


def set_width_aspect(fig, width) :
  # width is in pts
  if width is not None :
    (w1, h1) = fig.get_size_inches()
    w2 = pts_to_inches(width)
    h2 = h1 * w2 / w1
    fig.set_size_inches((w2, h2))

  return fig

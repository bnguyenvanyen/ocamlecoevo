#!/usr/bin/python3
# -*- coding: utf-8 -*-

import argparse
from math import floor

import numpy as np
import pandas as pd

from diagnosis import single_ess, multi_ess


parser = argparse.ArgumentParser(
  description=
  "Write means and credible intervals for different MCMC runs."
)

parser.add_argument(
  "--read-template",
  default="{}",
  help="template string to pass the 'read' arguments through."
)

parser.add_argument(
  "--read",
  nargs=2,
  action="append",
  help="Each element (label, s) specifies a location"
       "to be read as '{read_template}.format(s)'."
)

parser.add_argument(
  "--burn",
  type=int,
  default=0,
  help="Plot only for iterations > {burn}."
)

parser.add_argument(
  "--multiples",
  type=int,
  nargs=2,
  default=[1, 0],
  help="with multiples '(mult, rem)', ignore samples with "
       "'k mod mult != rem'.",
)

parser.add_argument(
  "--parameters",
  nargs="+",
  help="Parameters to compute (m)ESS for."
)

parser.add_argument(
  "--interval",
  type=float,
  default=95.,
  help="Credible interval width.",
)

parser.add_argument(
  "--out-template",
  help="Optional template for output files. By default, re-use {read_template}."
)


args = parser.parse_args()

print("Parameters:", args.parameters)


read_pairs = [ (label, args.read_template.format(read))
               for (label, read) in args.read ]

read_labels = [ lab for (lab, _) in read_pairs ]

print("Labels:", read_labels)


def compute_estimates(df, interval, cols) :
  assert(interval < 100)
  low = (100 - interval) / (2 * 100)
  high = 1 - low

  est = list()
  for col in cols :
    mean = df[col].mean()
    median = df[col].median()
    qlow = df[col].quantile(q=low)
    qhigh = df[col].quantile(q=high)
    record = {
      "parameter": col,
      "mean": mean,
      "median": median,
      "low": qlow,
      "high": qhigh,
    }
    est.append(record)

  df = pd.DataFrame.from_records(est)
  return df


def read(s) :
  print("read", s)
  try :
    return pd.read_csv(
      s,
      delimiter=',',
      header=0
    )
  except FileNotFoundError :
    return None


df_d = {
  label : read(readv)
  for (label, readv) in read_pairs
  if read(readv) is not None
}


for label, read_v in read_pairs :
  try :
    df_d[label]["label"] = label
  except KeyError :
    pass


df = pd.concat(
  list(df_d.values()),
  ignore_index=True,
  sort=False,
)


burn = args.burn
multiples, remainder = args.multiples

cond = ((df["k"] > args.burn) & (df["k"] % multiples == remainder))

df = df[(cond)]


estimates = dict()
for lab in read_labels :
  est = compute_estimates(
    df[df["label"] == lab],
    interval=args.interval,
    cols=args.parameters,
  )
  estimates[lab] = est


if args.out_template :
  tmpl = args.out_template
if not args.out_template :
  # check that args.read_template ends with '.theta.csv'
  if not args.read_template[-10:] == ".theta.csv" :
    raise ValueError("No 'out-template' and unexpected 'read-template'")
  tmpl = args.read_template[:-10] + ".estimates.csv"

for lab, read in args.read :
  out = tmpl.format(read)
  df_est = estimates[lab]
  df_est.to_csv(
    out,
    index=False
  )

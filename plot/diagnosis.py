#!/usr/bin/python3
# -*- coding: utf-8 -*-

from math import floor
import pandas as pd
import numpy as np
from ot.lp import emd
from ot.utils import dist


def sample_cov(df, cols=None) :
  """Sample covariance for a column subset"""
  if cols is not None :
    sub = df[cols]
  else :
    sub = df
  cov = sub.cov()

  return cov


def batch_means_sample_cov(df, cols, n_batches=100) :
  """
  Batch means sample covariance

  Take the means of `n_batches` batches of the `cols` subset of `df`,
  and return the rescaled covariance of those means.
  """
  a = df[cols].values
  n_rows = df.shape[0]
  batch = floor(n_rows / n_batches)
  print("Computing batch means sample cov with batch size", batch)
  batch_means = np.stack([
    a[k*batch:(k+1)*batch, :].mean(axis=0)
    for k in range(n_batches)
  ])
  cov = batch * np.cov(batch_means, rowvar=False)

  return cov


def single_ess(df, col, expt=(1/2)) :
  """
  One dimensional Effective Sample Size (ESS)

  Taken from github.com/xlabmedical/mcmcse
  The one dimensional ESS for the column `col` of the dataframe `df`.
  """
  x = df[col]
  n = len(x)
  if n == 0 :
    return 0
  else :
    shape = x.shape
    batch = int(np.floor(n ** expt))
    n_batches = int(np.floor(n / batch))
    end = n_batches * batch
    y = np.mean(
      np.reshape(
        x[:end].values,
        (n_batches, batch, *shape[1:])
      ),
      axis=1
    )
    mu_hat = np.mean(x, axis=0)
    sgm = batch * np.sum((y - mu_hat)**2, axis=0) / (n_batches - 1)
    lbd = np.var(x, ddof=1, axis=0)
    
    return n * lbd / sgm


def multi_ess(df, cols, **kw) :
  """
  Multi dimensional Effective Sample Size (ESS)

  ESS for the columns `cols` of `df` (can be quite unstable).
  """
  n = df.shape[0]
  p = len(cols)
  print("Multi ESS for", n, "samples and", p, "dimensions")
  if p >= 2 :  # apparently this needs to be 2
    sgn_det_lbd, log_det_lbd = np.linalg.slogdet(
      sample_cov(df, cols)
    )
    sgn_det_sgm, log_det_sgm = np.linalg.slogdet(
      batch_means_sample_cov(df, cols, **kw)
    )
    det_ratio = sgn_det_lbd * sgn_det_sgm * np.exp(log_det_lbd - log_det_sgm)
    print(det_ratio)
    mess = n * det_ratio ** (1 / p)
    return mess
  elif p == 1 :
    return single_ess(df, cols[0])
  else :
    raise ValueError("invalid number of columns")


def histdd(df, bins, cols) :
  v = df[cols].dropna().values
  # number of (non nan) samples
  n = v.shape[0]
  # For now let's just consider the int (number of bins) case
  bins = [ bins[c] for c in cols ]

  h, e = np.histogramdd(v, bins=bins)
  return (h / n, e)


def centers(edges) :
  d = len(edges)
  centers = [ e[:-1] + (e[1:] - e [:-1]) / 2 for e in edges ]
  return np.dstack(np.meshgrid(*centers)).reshape(-1, d)


def wasserstein(a, b, loss) :
  sol, log = emd(a, b, loss, numItermax=500000, log=True)

  return (sol, log["cost"])


def dirac_wasserstein_distance(df1, df2, cols) :
  points1 = df1[cols].values
  points2 = df2[cols].values
  h1 = np.ones((len(df1),)) / len(df1)
  h2 = np.ones((len(df2),)) / len(df2)
  loss = dist(points1, points2)
  return wasserstein(h1, h2, loss)


def binned_wasserstein_distance(df1, df2, bins, cols) :
  # To compute the multi dim wasserstein distance,
  # we need to discretize the measures by binning observations
  # in the dataframes.
  h1, edges = histdd(df1, bins, cols)
  h2, _ = histdd(df2, bins, cols)
  # (edges = _)
  # loss matrix : compute the distances between bins :
  # just take the euclidean distance between the centers of bins
  points = centers(edges)
  loss = dist(points, points)
  # not totally sure about the flatten 'F' but it seems to work
  return wasserstein(h1.flatten('F'), h2.flatten('F'), loss)

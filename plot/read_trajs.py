#!/usr/bin/python3
# -*- coding: utf-8 -*-

import re
import glob
import numpy as np
import pandas as pd


all_compartments = ["s", "e", "i", "r", "c", "o", "n"]

def compartment_label(comp) :
  d = {
    "s" : "Susceptibles",
    "e" : "Latent",
    "i" : "Infected",
    "r" : "Removed",
    "c" : "Cases",
    "o" : "Out",
    "n" : "Total population"
  }
  try :
    return d[comp]
  except KeyError :
    return comp


k_pat = re.compile(r".(\d+).traj.csv")

def get_k(fname) :
  match = k_pat.search(fname)
  if match :
    return int(match.group(1))
  else :
    return None


def keep_fname(fname, burn, multiples, remainder) :
  k = get_k(fname)
  if k is not None :
    return ((k > burn) and (k % multiples == remainder))
  else :
    return True


# also differentiate between different patterns and focuses :
def read(fname, pattern_label, compartments, focus, k=np.nan, pattern_name=np.nan, pattern=np.nan) :
  try :
    df = pd.read_csv(fname)
  except pd.errors.EmptyDataError :
    df = pd.DataFrame([], columns=["t"] + compartments)

  # only keep compartments and change to long form
  df = df[["t"] + compartments].melt(
    id_vars=["t"],
    var_name="compartment",
    value_name="count",
  )
  df = df.assign(focus=focus)
  df = df.assign(fname=fname)
  df = df.assign(k=k)
  df = df.assign(pattern=pattern)
  df = df.assign(**{pattern_label:pattern_name})
  df = df.assign(**{"Compartment":df["compartment"].apply(compartment_label)})

  return df.astype({pattern_label:str})


def read_all(patterns, focuses, pattern_label, compartments, burn, multiples, remainder) :
  reads_glob = [
    read(
      fname=fname,
      pattern_label=pattern_label,
      compartments=compartments,
      focus=False,
      k=get_k(fname),
      pattern_name=pattern_name,
      pattern=pattern,
    )
    for pattern_name, pattern in patterns
    for fname in glob.glob(pattern)
    if keep_fname(fname, burn=burn, multiples=multiples, remainder=remainder)
  ]

  if reads_glob == [] :
    nb_comp = len(compartments)
    df_glob = pd.DataFrame({
      "t" : [np.nan] * nb_comp,
      "compartment" : compartments,
      "Compartment" : [ compartment_label(comp) for comp in compartments ],
      "pattern" : ["no_glob"] * nb_comp,
      pattern_label : [""] * nb_comp,
      "fname" : ["no_glob"] * nb_comp,
      "k" : [np.nan] * nb_comp,
      "focus" : [False] * nb_comp,
    })
  else :
    df_glob = pd.concat(
      reads_glob,
      ignore_index=True,
    )

  reads_focus = [ read(
      fname=fname,
      pattern_label=pattern_label,
      compartments=compartments,
      focus=True,
    ) for fname in focuses ]

  if reads_focus == [] :
    df_focus = pd.DataFrame()
  else :
    df_focus = pd.concat(
      reads_focus,
      ignore_index=True,
    )

  df = pd.concat(
    [df_glob, df_focus],
    sort=False
  )

  return df


def read_concat(fname, concat_label, compartments,
                burn, multiples, remainder,
                concat_name=np.nan) :
  try :
    df = pd.read_csv(fname)
  except pd.errors.EmptyDataError :
    df = pd.DataFrame([], columns=["k", "t"] + compartments)

  # filter out unwanted k
  df = df[(df["k"] > burn) & (df["k"] % multiples == remainder)]

  df = df[["k", "t"] + compartments].melt(
    id_vars=["k", "t"],
    var_name="compartment",
    value_name="count",
  )
  df = df.assign(focus=False)
  df = df.assign(fname=fname)
  df = df.assign(**{concat_label:concat_name})
  df = df.assign(**{"Compartment":df["compartment"].apply(compartment_label)})

  return df.astype({concat_label:str})


def read_concat_all(concat_paths, focuses, concat_label, compartments, burn, multiples, remainder) :
  reads_many = [
    read_concat(
      fname=concat_path,
      concat_label=concat_label,
      compartments=compartments,
      burn=burn,
      multiples=multiples,
      remainder=remainder,
      concat_name=concat_name
    )
    for concat_name, concat_path in concat_paths
  ]

  if reads_many == [] :
    nb_comp = len(compartments)
    df_many = pd.DataFrame({
      "t" : [np.nan] * nb_comp,
      "compartment" : compartments,
      "Compartment" : [ compartment_label(comp) for comp in compartments ],
      "pattern" : ["no_glob"] * nb_comp,
      concat_label : [""] * nb_comp,  # no label
      "fname" : ["no_glob"] * nb_comp,
      "k" : [np.nan] * nb_comp,
      "focus" : [False] * nb_comp,
    })
  else :
    df_many = pd.concat(
      reads_many,
      ignore_index=True,
    )

  reads_focus = [ read(
      fname=fname,
      pattern_label=concat_label,
      compartments=compartments,
      focus=True,
    ) for fname in focuses ]

  if reads_focus == [] :
    df_focus = pd.DataFrame()
  else :
    df_focus = pd.concat(
      reads_focus,
      ignore_index=True,
    )

  df = pd.concat(
    [df_many, df_focus],
    sort=False
  )

  return df

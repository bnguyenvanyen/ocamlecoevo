#!/usr/bin/python3
# -*- coding: utf-8 -*-

import argparse
from functools import partial

import numpy as np
import pandas as pd
from scipy.stats import linregress
from matplotlib import pyplot as plt
from matplotlib.ticker import ScalarFormatter
import seaborn as sb

import parse
import context
import aspect as asp

plt.switch_backend("Qt5Agg")


parser = argparse.ArgumentParser(
  description=
  "Compare complexity relationships for different MCMC methods."
)

parser.add_argument(
  "--read-legend-title",
  default="label",
  help="Legend label for different colors.By default, 'label'."
)

parser.add_argument(
  "--read",
  nargs=2,
  action="append",
  help="Read elements (label, s), where 'label' is for the figure legend,"
       "and 's' for the location into 'read-template'."
)

parser.add_argument(
  "--xaxis-label",
  default="x",
  help="x-Axis label for the different values. By default, 'x'."
)

parser.add_argument(
  "--yaxis-label",
  default="Complexity",
  help="y-Axis label for the measure. By default, 'Complexity'."
)

parser.add_argument(
  "--objective",
  default="ess",
  help="The value to fill in the template for the objective file,"
       "to compute the yaxis values with. By default, 'ess'."
)

parser.add_argument(
  "--objective-column",
  help="The column to read in the objective file."
)

parser.add_argument(
  "--cost",
  default="duration",
  help="The value to fill in the template for the cost file."
       "By default, 'duration'."
)

parser.add_argument(
  "--cost-column",
  default="wall_time",
  help="The column to read in the cost file. By default 'wall_time'."
)

parser.add_argument(
  "--xaxis-values",
  nargs="+",
  type=int,
  help="x-Axis values keys to pass through the template."
)

parser.add_argument(
  "--map-x",
  nargs=2,
  type=int,
  action="append",
  help="x-Axis values corresponding to keys. By default `.astype(int)`."
)

parser.add_argument(
  "--xaxis-log-base",
  type=int,
  default=10,
  help="Base for the x-axis log scale."
)

parser.add_argument(
  "--xaxis-plain-format",
  action="store_true",
  help="Do not use scientific notation on the x-axis."
)

parser.add_argument(
  "--read-template",
  default="{label}{measure}{x}",
  help="Template string to pass the 'read' arguments through."
       "It should have three appropriately named holes to locate the files."
)

parser.add_argument(
  "--staged-template",
  action="store_true",
  help="Process 'read-template' by stages, first 'label', then 'measure' and 'x'."
)

parser.add_argument(
  "--power-exponents",
  nargs="*",
  type=float,
  default=[1., 2., 3.],
  help="Power law exponents for which to plot reference lines."
)

parser.add_argument(
  "--power-factor",
  type=float,
  default=1.,
  help="Power law factor to move the reference lines up or down."
)

parser.add_argument(
  "--out",
  help="Path to save the '.png' output to. If not set, interactive plot."
)

parse.add_argument_context(parser)

parse.add_argument_latex(parser)

parse.add_argument_width(parser)

parse.add_argument_dpi(parser)


def read(read_tmpl, staged_tmpl, read, objective, objective_column,
         cost, cost_column, xaxis_values) :
  d = dict()
  for x in xaxis_values :
    if staged_tmpl :
      lab_tmpl = partial(
        read_tmpl.format(label=read).format,
      )
    else :
      lab_tmpl = partial(
        read_tmpl.format,
        label=read
      )
    obj_path = lab_tmpl(
      measure=objective,
      x=x,
    )
    try :
      df_obj = pd.read_csv(obj_path)
      obj_v = df_obj[objective_column]
    except FileNotFoundError :
      print("Missing objective value for label", read, "x =", x)
      obj_v = np.nan

    cost_path = lab_tmpl(
      measure=cost,
      x=x,
    )
    try :
      df_cost = pd.read_csv(cost_path)
      cost_v = df_cost[cost_column]
    except FileNotFoundError :
      # finished with a bug
      print("Missing cost value for label", read, "x =", x)
      cost_v = np.nan

    d[x] = float(cost_v / obj_v)

  return d


def read_all(read_tmpl, staged_tmpl, reads, objective, objective_column,
             cost, cost_column, xaxis_values, map_x, read_legend_title) :
  d = dict()
  for read_label, read_value in reads :
    d[read_label] = read(
      read_tmpl=read_tmpl,
      staged_tmpl=staged_tmpl,
      read=read_value,
      objective=objective,
      objective_column=objective_column,
      cost=cost,
      cost_column=cost_column,
      xaxis_values=xaxis_values
    )

  # transform into a long dataframe
  records = [
    {
      read_legend_title : read_label,
      "x" : map_x(x),
      "measure" : d[read_label][x]
    }
    for read_label, _ in reads
    for x in xaxis_values
  ]

  return pd.DataFrame.from_records(records)


def reference_lines(exponents, factor, xaxis_values, map_x, read_legend_title) :
  x_0 = min([ map_x(x) for x in xaxis_values ])
  records = [
    {
      "power" : pow,
      "x" : map_x(x),
      "measure" : factor * (map_x(x) / x_0) ** pow,
    }
    for pow in exponents
    for x in xaxis_values
  ]

  return pd.DataFrame.from_records(records)


def plot(df, refs, read_legend_title, xaxis_label, yaxis_label,
         xaxis_log_base, xaxis_plain_format, width) :
  aspect = 1.618
  if width :
    width = asp.pts_to_inches(width)
    height = width / aspect
  else :
    width = asp.pts_to_inches(345.)
    height = width / aspect

  f, ax = plt.subplots(
    1, 1,
    constrained_layout=True,
    figsize=(width, height),
  )

  sb.lineplot(
    x="x",
    y="measure",
    style="power",
    legend="brief",
    markers=False,
    data=refs,
    color="black",
    alpha=0.5,
    ax=ax,
  )

  # grab legend stuff
  h1, l1 = ax.get_legend_handles_labels()

  sb.lineplot(
    x="x",
    y="measure",
    hue=read_legend_title,
    legend="brief",
    marker='o',
    data=df,
    ax=ax,
  )

  # grab updated legend stuff
  h2, l2 = ax.get_legend_handles_labels()
  # filter to keep only new legend stuff (unzip zip)
  h2, l2 = zip(*[ (h, l) for h, l in zip(h2, l2) if l not in l1 ])

  leg2 = ax.legend(
    h2,
    l2,
    title=read_legend_title,
    bbox_to_anchor=(0, 1),
    loc="upper left",
    borderaxespad=0.,
    frameon=False,
  )

  leg1 = ax.legend(
    h1,
    l1,
    title="$O(x^a)$",
    bbox_to_anchor=(1.05, 0),
    loc="lower left",
    borderaxespad=0.,
    frameon=False,
  )

  # readd leg2
  ax.add_artist(leg2)

  ax.set_xscale("log", base=xaxis_log_base)
  ax.set_xlabel(xaxis_label)
  if xaxis_plain_format :
    fmt = ScalarFormatter()
    fmt.set_scientific(False)
    ax.xaxis.set_major_formatter(fmt)

  ax.set_yscale("log")
  ax.set_ylabel(yaxis_label)

  # Ensure that there is more than one ticklabel
  ymin, ymax = ax.get_ylim()
  # yscale is always base 10, so compare with base
  if ymax / ymin < 100 :
    ymin = 10 ** np.floor(np.log(ymin) / np.log(10))
    ymax = 10 ** np.ceil(np.log(ymax) / np.log(10))
    ax.set_ylim(ymin, ymax)

  return f, ax


args = parser.parse_args()

context.set(args.context)

if args.latex :
  context.set_latex()


if args.map_x :
  d = dict(args.map_x)
  print(d)
  def map_x(x_key) :
    return int(d[x_key])
else :
  def map_x(x_key) :
    return int(x_key)


df = read_all(
  read_tmpl=args.read_template,
  staged_tmpl=args.staged_template,
  reads=args.read,
  objective=args.objective,
  objective_column=args.objective_column,
  cost=args.cost,
  cost_column=args.cost_column,
  xaxis_values=args.xaxis_values,
  map_x=map_x,
  read_legend_title=args.read_legend_title
)

print(df)

refs = reference_lines(
  exponents=args.power_exponents,
  factor=args.power_factor,
  xaxis_values=args.xaxis_values,
  map_x=map_x,
  read_legend_title=args.read_legend_title
)

f, ax = plot(
  df=df,
  refs=refs,
  read_legend_title=args.read_legend_title,
  xaxis_label=args.xaxis_label,
  yaxis_label=args.yaxis_label,
  xaxis_log_base=args.xaxis_log_base,
  xaxis_plain_format=args.xaxis_plain_format,
  width=args.width
)


if args.out :
  f.savefig(
    args.out,
    dpi=args.dpi
  )
else :
  plt.show()

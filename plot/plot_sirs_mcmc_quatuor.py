#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import seaborn as sb

root_name = sys.argv[1]
target = sys.argv[2]
burnin = int(sys.argv[3])
names = sys.argv[4:]

liks = [ "prior", "cases", "coal", "joint" ]

df_d = {
  lik : pd.read_csv(
    "{}.{}.csv".format(root_name, lik),
    delimiter=',',
    header=0
  ) for lik in liks
}

if names == [] :
  names = None

print(names)

for lik in liks :
  df_d[lik]["posterior"] = lik

df = pd.concat(
  [ df_d[lik] for lik in liks],
  ignore_index=True
)

accept = (df["rejected"] == "accept")
burn = (df["k"] > burnin)
df_hist = df[(burn) & (accept)]

try :
  df_target = pd.read_csv(target, delimiter=',', header=0)
  ranges = [ (0.25 * df_target[nm].values[0], 1.75 * df_target[nm].values[0])
             for nm in names ]
except FileNotFoundError :
  df_target = None



def hist_ranged(x, **kwargs) :
  q10 = np.percentile(x, q=10)
  q90 = np.percentile(x, q=90)
  plt.hist(x, range=(q10, q90), normed=True, **kwargs)


def hist_scaled(x, bins, scl=1., **kwargs) :
  hist, bin_edges = np.histogram(x, bins)
  bin_centers = (bin_edges[:-1] + bin_edges[1:]) / 2.
  scl_hist = scl * hist
  plt.fill_between(bin_centers, scl_hist, alpha=0.75)


def rug(x, **kwargs) :
  sb.rugplot(x[:10:], **kwargs)

def swarm(x, **kwargs) :
  sb.swarmplot(x=x[:10:], **kwargs)


sb.set_context("poster")

g = sb.PairGrid(
  df_hist,
  hue="posterior",
  vars=names,
)


g = g.map_diag(swarm)
g = g.map_offdiag(plt.scatter, alpha=0.2, marker=',', s=2)
g = g.add_legend()

pal = g.palette

def improve_legend() :
  # set alpha and size for markers in legend
  for lh in g._legend.legendHandles :
    lh.set_alpha(1)
    lh._sizes = [100]


improve_legend()


def hist_by_hand() :
  for i, nmi in enumerate(names) :
    x = df_target[nmi].values[0]

    if nmi == "s0" :
      rg = None
    else :
      rg = (0.25 * x, 1.75 * x)

    if nmi != "rho" :
      for j, hue in enumerate(liks) :
        g.axes[i, i].hist(
          df_hist.loc[df_hist["posterior"]==hue, nmi],
          bins=100,
          range=rg,
          color=pal[j],
        )



lims = {
  "beta" : (0, 100),
  "i0" : (0, 2000),
}

def set_lims() :
  # set lims as [ 0.5 target , 1.5 target ]
  for i, nmi in enumerate(names) :
    for j, nmj in enumerate(names) :
      # x = df_target[nmj].values[0]
      # xlim = (0.25 * x, 1.75 * x)
      try :
        g.axes[i, j].set_xlim(lims[nmj])
      except KeyError :
        print(nmj)
      if (i != j) :
        # y = df_target[nmi].values[0]
        # ylim = (0.25 * y, 1.75 * y)
        try :
          g.axes[i, j].set_ylim(lims[nmi])
        except KeyError :
            print(nmi)


# set_lims()


def plot_targets() :
  # plot black lines for target
  for i, nmi in enumerate(names) :
    for j, nmj in enumerate(names) :
      x = df_target[nmj].values[0]
      g.axes[i, j].axvline(x=x, color='black', linewidth=1)
      if i != j :
        y = df_target[nmi].values[0]
        g.axes[i, j].axhline(y=y, color='black', linewidth=1)

try :
  plot_targets()
except TypeError :
  print("No targets to plot")

g.savefig(
  "{}.pairplot.png".format(root_name),
  dpi=200
)

#!/usr/bin/python3
# -*- coding: utf-8 -*-

import argparse
import re
from functools import partial

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

from Bio.Phylo.BaseTree import Clade, Tree
from Bio.Phylo import draw

from read import\
  read_fasta,\
  float_pat,\
  parse_fasta_header,\
  read_newick,\
  read_newick_forests


plt.switch_backend("Qt5Agg")


parser = argparse.ArgumentParser(
  description="Compare inferred trees to the target tree from Newick files."
)


parser.add_argument(
  "--tree-sample",
  nargs=2,
  action="append",
  default=[],
  help="Newick file of trees and index of tree into it."
)

parser.add_argument(
  "--forest-sample",
  nargs=2,
  action="append",
  default=[],
  help="Newick file of forests and index of forest into it."
)

parser.add_argument(
  "--target",
  help="Path to Newick file of target tree."
)

parser.add_argument(
  "--times",
  help="Lineage sampling times."
)

parser.add_argument(
  "--dont-relabel-target",
  action="store_true",
  help="Do not relabel the target tree from label order."
)

parser.add_argument(
  "--dont-print-labels",
  action="store_true",
  help="Do not print labels on trees."
)

parser.add_argument(
  "--fasta-header-regex",
  help="Regex for <label> and <time>."
       "The float part can be left as a '{}' to be filled in."
)

parser.add_argument(
  "--margin",
  type=float,
  default=2.,
  help="Margin on root time."
)

parser.add_argument(
  "--size",
  type=float,
  nargs=2,
  help="Size (w, h) of the figure in inches."
)

parser.add_argument(
  "--orient",
  choices=["rows", "columns"],
  default="rows",
  help="Plot the trees on rows or columns."
)

parser.add_argument(
  "--out",
  help="Optional path to output. if not given, interactive plot.",
)


samples_header_pat = re.compile(r">(?P<k>\d+)")
def parse_header_sample(header) :
  match = samples_header_pat.search(header)
  if match :
    k = int(match.group("k"))
    return k
  else :
    return None


target_header_pat = re.compile(r"#tree (?P<k>\d+)")
def parse_header_target(header) :
  match = target_header_pat.search(header)
  if match :
    k = int(match.group("k"))
    return k
  else :
    return None


tree_header_pat = re.compile(r"#tree (?P<js>[{};0-9]+)")
def parse_header_tree(header) :
  match = tree_header_pat.search(header)
  if match :
    return match.group("js")
  else :
    return None


def relabel(kts) :
  d = {
    k : i + 1
    for i, (k, _) in enumerate(
      sorted(
        kts,
        key=lambda kt : kt[1],
        reverse=True
      )
    )
  }
  return (lambda k : str(d[k]))


def time(kts) :
  d = {
    i + 1 : t
    for i, (_, t) in enumerate(
      sorted(
        kts,
        key=lambda kt : kt[1],
        reverse=True
      )
    )
  }
  return (lambda i : d[int(i)])



# Adjust root branch length so that the leaves have a fixed position
def adjust_root(tree, time, margin) :
  def name_length_left(c) :
    if c.is_terminal() :
      return (c.name, c.branch_length)
    else :
      name, l = name_length_left(c[0])
      return (name, l + c.branch_length)
  
  name, l = name_length_left(tree.clade)
  t = time(name)
  dx = margin + t - l

  c = tree.clade[0]
  clade = Clade(
    name=c.name,
    branch_length=c.branch_length + dx,
    clades=c.clades
  )
  return Tree.from_clade(clade)


def relabel_tree(tree, relabel) :
  def relabel_clade(c) :
    if c.is_terminal() :
      c = Clade(
        name=relabel(c.name),
        branch_length=c.branch_length,
        clades=c.clades,
      )
      return c
    else :
      clades = [ relabel_clade(child) for child in c ]
      c = Clade(
        name=c.name,
        branch_length=c.branch_length,
        clades=clades,
      )
      return c

  c = relabel_clade(tree.clade)
  return Tree.from_clade(c)


def sort_tree(tree) :
  def sort_clade(c) :
    if c.is_terminal() :
      return int(c.name), c
    elif len(c) == 1 :
      i, c0_sorted = sort_clade(c[0])
      c_sorted = Clade(
        name=c.name,
        branch_length=c.branch_length,
        clades=[c0_sorted],
      )
      return i, c_sorted
    else :
      assert len(c) == 2
      c1, c2 = c[0], c[1]
      i1, c1_sorted = sort_clade(c1)
      i2, c2_sorted = sort_clade(c2)
      if i1 < i2 :
        clades = [c1_sorted, c2_sorted]
        i = i1
      elif i2 < i1 :
        clades = [c2_sorted, c1_sorted]
        i = i2
      else :
        raise ValueError("i1 = i2")
      c_sorted = Clade(
        name=c.name,
        branch_length=c.branch_length,
        clades=clades,
      )
      return i, c_sorted

  _, sorted_clade = sort_clade(tree.clade)
  return Tree.from_clade(sorted_clade)


def plot_tree(tree, time, label_func, margin, ax) :
  tree = sort_tree(adjust_root(tree, time, margin))
  draw(
    tree,
    label_func=label_func,
    show_confidence=False,
    do_show=False,
    axes=ax
  )


def plot_one_tree(tree, time, label_func, margin, ax) :
  plot_tree(
    tree,
    time,
    label_func=label_func,
    margin=margin,
    ax=ax
  )
  

def plot_forest(forest, time, label_func, margin, ax) :
  # FIXME handle a forest
  assert (len(forest) == 1)
  tree = list(forest.values())[0]
  plot_tree(
    tree,
    time,
    label_func=label_func,
    margin=margin,
    ax=ax
  )


def plot(target, trees, forests, time, margin, dont_print_labels, orient="rows", size=None) :
  n = 1 + len(trees) + len(forests)
  if size is not None :
    kw_subplots = { "figsize" : size }
  else :
    kw_subplots = {}

  if orient == "rows" :
    nrow = n
    ncol = 1
    def at(ax, i) :
      return ax[i, 0]
  elif orient == "columns" :
    ncol = n
    nrow = 1
    def at(ax, i) :
      return ax[0, i]
  else :
    raise ValueError("orient not in 'rows'|'columns'")

  f, ax = plt.subplots(
    nrow, ncol,
    sharex=True,
    squeeze=False,
    **kw_subplots
  )

  if not dont_print_labels :
    label_func = lambda c : str(c.name) if c.is_terminal() else ""
  else :
    label_func = lambda c : ""

  plot_one_tree(
    target,
    time,
    label_func,
    margin,
    ax[0, 0]
  )

  for i, tree in enumerate(trees) :
    plot_one_tree(
      tree,
      time,
      label_func,
      margin,
      at(ax, 1 + i)
    )

  for i, forest in enumerate(forests) :
    plot_forest(
      forest,
      time,
      margin,
      at(ax, 1 + len(trees) + i)
    )

  ax[0, 0].set_title("Target")

  return f


args = parser.parse_args()

# Change regex for fasta header if passed
if args.fasta_header_regex :
  parse_fasta_header = partial(
    parse_fasta_header,
    regex=re.compile(args.fasta_header_regex.format(float_pat))
  )

kts = read_fasta(
  args.times,
  lambda rec : parse_fasta_header(rec.id)
)


time = time(kts)

def get_sample(d, k) :
  if k == "last" :
    smpl = list(d.values())[-1]
  else :
    smpl = d[int(k)]

  return smpl


trees = [
  get_sample(
    read_newick(
      path,
      parse_header=parse_header_sample
    ),
    k=k
  )
  for path, k in args.tree_sample
]

forests = [
  get_sample(
    read_newick_forests(
      path,
      parse_header_sample=parse_header_sample,
      parse_header_tree=parse_header_tree
    ),
    k=k
  )
  for path, k in args.forest_sample
]

targets = read_newick(
  args.target,
  parse_header=parse_header_target
)
assert(len(targets) == 1)
target = list(targets.values())[0]

if not args.dont_relabel_target :
  relabel = relabel(kts)
  target = relabel_tree(target, relabel)


f = plot(
  target,
  trees,
  forests,
  time,
  margin=args.margin,
  dont_print_labels=args.dont_print_labels,
  size=args.size,
  orient=args.orient,
)


if args.out :
  f.savefig(args.out, dpi=300)
else :
  plt.show()

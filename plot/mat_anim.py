#!/usr/bin/python3
# -*- coding: utf-8 -*-

import argparse

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from matplotlib import animation

from read import read_matrix_traj


parser = argparse.ArgumentParser(
  description="Plot an animation for successive values of a matrix."
)


parser.add_argument(
  "--read",
  help="Path to CSV file of matrix values."
)


def plot(df, a) :
  f, ax = plt.subplots()

  m = ax.matshow(a[0])
  f.colorbar(m)
  t = ax.set_title("k = 0")

  def update(frame) :
    arr = a[frame]
    m.set_array(arr)
    m.set_clim(np.nanmin(arr), np.nanmax(arr))
    k = df["k"].iloc[frame]
    t.set_text("k = {}".format(k))

  ani = animation.FuncAnimation(
    f,
    update,
    frames=range(a.shape[0])
  )

  return ani



args = parser.parse_args()

df, a = read_matrix_traj(args.read)

ani = plot(df, a)

plt.show()

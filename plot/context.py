#!/usr/bin/python3
# -*- coding: utf-8 -*-

import matplotlib
from matplotlib import rc
import seaborn as sb

def cramped_paper_context() :
  # Notebook has basic dimensions
  # Paper is 0.8 * notebook
  # For ticks, axes, markers, etc, we go down to 0.4 * notebook,
  # so 0.5 * paper
  c = sb.plotting_context("paper")

  def rescale(fraction, keys) :
    for key in keys :
      c[key] = fraction * c[key]

  rescale(fraction=0.75, keys=[
    "axes.linewidth",
    "grid.linewidth",
    "lines.linewidth",
    "lines.markersize",
    "patch.linewidth",
    "xtick.labelsize",
    "ytick.labelsize",
    "xtick.major.width",
    "ytick.major.width",
    "xtick.minor.width",
    "ytick.minor.width",
    "xtick.major.size",
    "ytick.major.size",
    "xtick.minor.size",
    "ytick.minor.size",
  ])

  # set some other things
  c["axes.titlesize"] = "medium"
  c["axes.titlepad"] = 4.
  c["axes.labelpad"] = 2.7
  c["xtick.major.pad"] = 2.5
  c["xtick.minor.pad"] = 2.3
  c["ytick.major.pad"] = 2.5
  c["ytick.minor.pad"] = 2.3

  # set some legend things
  c["legend.fontsize"] = "small"
  c["legend.title_fontsize"] = "medium"
  c["legend.labelspacing"] = 0.4
  c["legend.handlelength"] = 1.
  c["legend.handletextpad"] = 0.6
  c["legend.columnspacing"] = 1.

  return c


def cramped_talk_context() :
  # For ticks, axes, markers, etc
  # 0.5 * talk
  c = sb.plotting_context("talk")

  def rescale(fraction, keys) :
    for key in keys :
      c[key] = fraction * c[key]

  rescale(fraction=0.75, keys=[
    "axes.linewidth",
    "grid.linewidth",
    "lines.linewidth",
    "lines.markersize",
    "patch.linewidth",
    "xtick.labelsize",
    "ytick.labelsize",
    "xtick.major.width",
    "ytick.major.width",
    "xtick.minor.width",
    "ytick.minor.width",
    "xtick.major.size",
    "ytick.major.size",
    "xtick.minor.size",
    "ytick.minor.size",
  ])

  # set some other things
  c["axes.titlesize"] = "large"
  c["axes.titlepad"] = 4.
  c["axes.labelpad"] = 2.7
  c["xtick.major.pad"] = 2.5
  c["xtick.minor.pad"] = 2.3
  c["ytick.major.pad"] = 2.5
  c["ytick.minor.pad"] = 2.3

  # set some legend things
  c["legend.fontsize"] = "medium"
  c["legend.title_fontsize"] = "large"
  c["legend.labelspacing"] = 0.4
  c["legend.handlelength"] = 1.
  c["legend.handletextpad"] = 0.6
  c["legend.columnspacing"] = 1.

  return c


def determine(context) :
  if context == "paper_cramped" :
    return cramped_paper_context()
  elif context == "talk_cramped" :
    return cramped_talk_context()
  else :
    return sb.plotting_context(context)


def set(context) :
  matplotlib.rcParams.update(determine(context))


def set_latex() :
  rc('font', **{
    'family':'sans-serif',
    'sans-serif':'CMU Sans Serif'
  })
  rc('text', usetex=True)

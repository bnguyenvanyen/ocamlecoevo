#!/usr/bin/python3
# -*- coding: utf-8 -*-

import argparse
from functools import partial

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import seaborn as sb


parser = argparse.ArgumentParser(
  description=
  "Compare an empirical cdf to the exponential cdf."
)

parser.add_argument(
  "--model",
  choices=["SIR", "SIRS", "SEIR", "SEIRS"],
  help="Model specifying the event rates."
)

parser.add_argument(
  "--traj",
  help="Path to the trajectory file."
)

parser.add_argument(
  "--params",
  help="Path to the file with parameter values."
)

parser.add_argument(
  "--first",
  type=int,
  default=1,
  help="Only count events up to t."
)


def total_rate(x, par, model) :
  birth = par["birth"] * par["popsize"]
  death = par["death"] * x["n"]

  if model == "SEIRS" :
    beta_t = par["beta"] + par["betavar"] * np.sin(2 * np.pi * x["t"] / par["freq"] + par["phase"])
    infect = beta_t * x["s"] / x["n"] * (x["i"] + par["eta"])
  elif model == "SIR" :
    infect = par["beta"] * x["s"] / x["n"] * x["i"]
  else :
    raise NotImplementedError

  if model == "SEIRS" :
    become_infectious = par["sigma"] * x["e"]
    immu_loss = par["gamma"] * x["r"]
  elif model == "SIR" :
    become_infectious = 0
    immu_loss = 0
  else :
    raise NotImplementedError

  recover = par["nu"] * x["i"]

  return birth + death + infect + become_infectious + recover + immu_loss


def exp_cdf(rate, max_t) :
  x = np.linspace(0, max_t, 1000)
  y = 1 - np.exp(- rate * x)

  return (x, y)


def initial_rate(group, par, model) :
  return total_rate(group.iloc[0], par, model)


# the duration to the first event is the first time
# bigger than the initial time
def first_waiting_time(group) :
  t0 = group["t"].iloc[0]
  first_row = group[group["t"] > t0].iloc[0]
  return (first_row["t"] - t0)


def waiting_times(group) :
  # if the initial state is present twice,
  # the first waiting time might be 0,
  # but we still keep it,
  return (group["t"].iloc[1:].values - group["t"].iloc[:-1].values)


def extract(group, n) :
    dt = waiting_times(group)
    rate = group["rate"].iloc[:-1].values
    df = pd.DataFrame({
      "dt" : dt,
      "rate" : rate
    })
    df = df[df["dt"] > 0]
    if n :
      df = df.iloc[:n]
    return df


def plot(df) :
  df["ds"] = df["dt"] * df["rate"]
  max_s = df["ds"].dropna().max()
  x, y = exp_cdf(1., max_s)
  f, ax = plt.subplots()
  ax.hist(
    df["ds"].dropna(),
    cumulative=True,
    density=True,
    bins=100
  )
  ax.plot(x, y)

  plt.show()


args = parser.parse_args()

model = args.model

# read from args.params path, and only keep the first row
par = pd.read_csv(args.params).iloc[0]

traj = pd.read_csv(args.traj)

traj = traj.assign(rate = total_rate(traj, par, model))

print(traj)

df = traj.groupby("k")\
    .apply(partial(extract, n=args.first))\
    .reset_index()

print(df)

plot(df)

#!/usr/bin/python3
# -*- coding: utf-8 -*-

import glob
import argparse

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import seaborn as sb


all_compartments = ["s", "e", "i", "r", "c", "o", "n"]

def compartment_label(comp) :
  d = {
    "s" : "Susceptibles",
    "e" : "Latent",
    "i" : "Infected",
    "r" : "Removed",
    "c" : "Cases",
    "o" : "Out",
    "n" : "Total population"
  }

  return d[comp]


parser = argparse.ArgumentParser(
  description="Compare approximate and exact PRM simulations."
)

parser.add_argument(
  "--pattern",
  type=str,
  help="glob patterns once formatted with 'exact' and 'approx'.",
)

parser.add_argument(
  "--compartments",
  nargs="+",
  default=all_compartments,
  help="compartments for which to plot trajectories."
)

parser.add_argument(
  "--only-diff",
  action="store_true",
  help="Plot only the diff curves.",
)

parser.add_argument(
  "--estim",
  action="store_true",
  help="Plot the envelopes instead of individual trajectories.",
)

parser.add_argument(
  "--context",
  choices=["paper", "notebook", "talk", "poster"],
  default="notebook",
  help="seaborn plotting context.",
)


def read(fname, compartments) :
  try :
    df = pd.read_csv(fname)
  except pd.errors.EmptyDataError :
    df = pd.DataFrame([], columns=["t"] + compartments)

  # only keep compartments and change to long form
  df = df[["t"] + compartments].melt(
    id_vars=["t"],
    var_name="compartment",
    value_name="count",
  )
  df = df.assign(fname=fname)
  df = df.assign(**{"Compartment":df["compartment"].apply(compartment_label)})

  return df


def read_all(pattern, compartments) :
  dfs = dict()
  for sim in ["exact", "approx"] :
    reads = [ read(fname, compartments) for fname in glob.glob(pattern.format(sim)) ]

    if reads == [] :
      nb_comp = len(compartments)
      df = pd.DataFrame({
        "t" : [np.nan] * nb_comp,
        "fname" : ["no_glob"] * nb_comp,
        "sim" : sim,
        "compartment" : compartments,
        "Compartment" : [ compartment_label(comp) for comp in compartments ],
      })
    else :
      df = pd.concat(
        reads,
        ignore_index=True,
      )
      df["sim"] = sim
    dfs[sim] = df

  dfs["diff"] = dfs["exact"].copy()
  dfs["diff"]["sim"] = "diff"
  dfs["diff"]["count"] = dfs["exact"]["count"] - dfs["approx"]["count"]

  df = pd.concat(
    dfs.values(),
    ignore_index=True,
    sort=False,
  )

  return df


def plot(df, estim=False, only_diff=False) :
  n = int(len(df["sim"].unique()) / 3)
  if only_diff :
    kw_diff = {
      "data":df[df["sim"] == "diff"],
    }
  else :
    kw_diff = {
      "data":df,
      "hue":"sim",
  }
  if estim :
    kw_estim = {
      "ci" : 99.,
    }
  else :
    kw_estim = {
      "units": "fname",
      "estimator" : None,
      "alpha": 1 / n,
    }
  kw = {**kw_estim, **kw_diff}
  f = sb.relplot(
    kind="line",
    x="t",
    y="count",
    row="Compartment",
    facet_kws = {"sharey":"row"},
    **kw
  )
  f = f.set_xlabels("time")
  f = f.set_ylabels("count")

  return f



args = parser.parse_args()

sb.set_context(args.context)

df = read_all(args.pattern, args.compartments)

plot(df, args.estim, args.only_diff)

plt.show()

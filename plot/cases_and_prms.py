#!/usr/bin/python3
# -*- coding: utf-8 -*-

from functools import partial
import argparse

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from matplotlib.patches import Patch
import seaborn as sb

import grid
import read
import context
import aspect as asp
import corr
import parse


plt.switch_backend("Qt5Agg")


parser = argparse.ArgumentParser(
  description=
  "Diagnose PRM-augmented MCMC posterior."
)

parser.add_argument(
  "--template",
  default="{}",
  help="Template string to format paths with."
)

parser.add_argument(
  "--columns",
  nargs="+",
  default=[],
  help="Columns names."
)

parser.add_argument(
  "--events",
  nargs="+",
  default=None,
  help="Events to plot point density for."
)

parser.add_argument(
  "--case-data",
  nargs=2,
  action="append",
  metavar=("column", "path"),
  help="Path to case data to be associated with the column name."
)

parser.add_argument(
  "--prm-data",
  nargs=2,
  action="append",
  metavar=("column", "path"),
  help="Path to prm data to be associated with the column name."
)

parser.add_argument(
  "--observations",
  help="Path to case counts observations."
)

parser.add_argument(
  "--rename",
  nargs=2,
  action="append",
  default=[],
  metavar=("name", "label"),
  help="How to label a column, hue or event on the graph."
)

parser.add_argument(
  "--rename-title",
  nargs=2,
  action="append",
  default=[],
  metavar=("name", "label"),
  help="How to label in a title."
)

parser.add_argument(
  "--autocorr",
  action="store_true",
  help="Also plot auto-correlation functions for the events."
)

parser.add_argument(
  "--keep-every",
  type=int,
  help="Only keep one sample in every."
)

parser.add_argument(
  "--ylimits",
  action="append",
  nargs=3,
  default=[],
  help="Set y-axis limits for one of the rows."
)

parser.add_argument(
  "--alpha",
  type=float,
  default=1.,
  help="Opacity of the trajectories.",
)

parser.add_argument(
  "--out",
  help="Path to output."
)

parse.add_argument_context(parser)

parse.add_argument_latex(parser)

parse.add_argument_width(parser)

parse.add_argument_aspect(parser)

parse.add_argument_dpi(parser)


hue_label = "Source"

action_labels = {
  "cases" : "cases",
  "density" : "density",
  "autocorr" : "acf",
}

focus_sources = ["observations"]


# cases

def load_case_data(cases_paths, obs_path, template, multiples) :
  estim_cases = read.read_glob_cases(
    patterns=[ (name, template.format(s) + ".*.data.cases.csv")
               for name, s in cases_paths ],
    pattern_label=hue_label,
  )
  estim_cases = read.burn_and_thin(estim_cases, mult=multiples)
  if obs_path :
    focuses = [("observations", obs_path)]
  else :
    focuses = []
  obs_cases = read.read_focus_cases(
    focuses=focuses,
    pattern_label=hue_label,
  )
  return (estim_cases, obs_cases)


# prms

def load_prm_data(prms_paths, template, events, autocorr, multiples) :
  prms = read.read_all_prms(
    reads=[ (name, s)
            for name, s in prms_paths ],
    read_label=hue_label,
    read_template=template + ".prm.aggregate.csv"
  )
  if multiples :
    prms = read.burn_and_thin(prms, mult=multiples)
  if events :
    prms = prms[prms["color"].isin(events)]

  if autocorr :
    prms_pairs = corr.pairify(prms, label_col=hue_label)
    prms_corr = corr.correlate(prms_pairs)
    prms_auto = prms_corr[prms_corr["color_x"] == prms_corr["color_y"]]
  else :
    # fragile
    prms_auto = None

  return (prms, prms_auto)


def renamer(renames, events, defaults=[]) :
  # also event + density etc
  renames = { name : label for (name, label) in renames }
  for name, label in defaults :  # action_labels.items() :
    # if renames doesn't give a label for actions,
    # take the one from action labels
    renames.setdefault(name, label)
  # set combinations for events and actions
  if events :
    for ev in events :
      for act in ["density", "autocorr"] :
        renames[ev + "_" + act] = "{}\n{}".format(
          # value associated with ev, or ev itself
          renames.get(ev, ev),
          renames.get(act, act)
        )
  print(renames)
  def f(name) :
    # value associated with name, or name itself
    return renames.get(name, name)

  return f
      

# prepare to plot
def palette(columns) :
  pal = {
    name : color
    for name, color in zip(
      columns,
      sb.color_palette(n_colors=len(columns))
    )
  }
  pal["observations"] = (0, 0, 0)  # "black"

  return pal


def init_grid(columns, events, autocorr, renamer, palette, width, aspect) :
  n = len(events)
  if autocorr and events and n > 1 :
    rows = ["cases"] + [
      ev + "_" + act
      for ev in events
      for act in ["density", "autocorr"]
    ]
  elif autocorr and (not events or len(events) == 1) :
    rows = ["cases", "density", "autocorr"]
  elif not autocorr and events and len(events) > 1 :
    rows = ["cases"] + [ ev + "_density" for ev in events ]
  elif not autocorr and (not events or len(events) == 1) :
    rows = ["cases", "density"]
  else :
    raise ValueError("events must contain at least one element")

  hues = columns + ["observations"]

  # y sharing across cases axes, across density axes, and across autocorr axes
  sharey = np.zeros([len(rows), len(columns), 2], dtype=int)
  for i, row in enumerate(rows) :
    for j, col in enumerate(columns) :
      if "cases" in row :
        sharey[i, j, :] = [0, 0]
      if "density" in row :
        sharey[i, j, :] = [1, 0]
      if "autocorr" in row :
        sharey[i, j, :] = [2, 0]

  w = asp.pts_to_inches(width)
  facet_h = asp.facet_height(
    ncol=len(columns),
    width=w,
    decoration=0.15,
    aspect=aspect
  )
  h = facet_h * len(rows) * 1.2

  g = grid.Mapper(
    rows=rows,
    columns=columns,
    hues=hues,
    palette=palette,
    legend="horizontal",
    # legend_title=hue_label,
    label_legend=renamer, 
    sharex="all",
    sharey=sharey,
    figsize=(w, h)
  )

  return g


def plot_facet(
      ax, row, column,
      palette, alpha, renamer,
      autocorr,
      estim_cases, obs_cases, prms, prms_auto
  ) :
  if row == "cases" :
    sb.lineplot(
      x="t",
      y="cases",
      legend=False,
      units="k",
      estimator=None,
      data=estim_cases[estim_cases[hue_label] == column],
      alpha=alpha,
      color=palette[column],
      ax=ax
    )
    obs_cases.plot(
      "t",
      "cases",
      legend=False,
      color=palette["observations"],
      linewidth=0.5,
      ax=ax
    )
    ax.set_ylabel(renamer(row))
  elif row[-7:] == "density" :
    act = row[-7:]
    try :
      assert (row[-8] == "_")
      # several events
      ev = row[:-8]
      data = prms[(prms["color"] == ev) & (prms[hue_label] == column)]
    except IndexError :
      # only one event
      data = prms[prms[hue_label] == column]
    sb.lineplot(
      x="origin_t",
      y="density",
      ci="sd",
      legend=False,
      color=palette[column],
      data=data,
      ax=ax
    )
    ax.set_xlabel(renamer("t"))
    ax.set_ylabel(renamer(act))
  elif row[-8:] == "autocorr" and autocorr :
    act = row[-8:]
    try :
      assert (row[-9] == "_")
      # several events
      ev = row[:-9]
      data = prms_auto[(prms_auto["color"] == ev)
                     & (prms_auto[hue_label] == column)]
    except IndexError :
      # only one event
      data = prms_auto[prms_auto[hue_label] == column]
    sb.lineplot(
      x="origin_t",
      y="corr",
      ci="sd",
      legend=False,
      color=palette[column],
      data=data,
      ax=ax
    )
    ax.set_xlabel(renamer("t"))
    ax.set_ylabel(renamer(act))
    ax.set_ylim(-10, 10)
  else :
    raise ValueError("Invalid row")
 

def title(row, renamer) :
  if row == "cases" :
    return renamer(row)
  elif row == "density" :
    return renamer(row)
  elif row == "autocorr" :
    return renamer(row)
  elif row[-7:] == "density" :
    ev = row[:-8]
    return renamer(ev)
  elif row[-8:] == "autocorr" :
    return ""
  else :
    raise ValueError("Invalid row")
  

def plot(
      columns, events,
      autocorr,
      estim_cases, obs_cases, prms, prms_auto,
      renames, rename_titles, width, aspect, alpha, ylimits
  ) :
  context.set("paper_cramped")
  context.set_latex()
  pal = palette(columns)
  ren = renamer(
    renames=renames,
    events=events,
    defaults=action_labels.items()
  )
  rent = renamer(
    renames=rename_titles,
    events=events,
    defaults=list(action_labels.items()) + renames
  )
  g = init_grid(
    columns=columns,
    events=events,
    autocorr=autocorr,
    renamer=ren,
    palette=pal,
    width=width,
    aspect=aspect
  )
  g.iter_facets(
    f=partial(
      plot_facet,
      palette=pal,
      alpha=alpha,
      renamer=ren,
      autocorr=autocorr,
      estim_cases=estim_cases,
      obs_cases=obs_cases,
      prms=prms,
      prms_auto=prms_auto,
    )
  )

  g.set_row_titles(partial(title, renamer=rent))

  def set_yaxis_limits(axes, row) :
    try :
      ylim = ylimits[row]
      axes.iloc[0].set_ylim(ylim)
    except KeyError :
      pass
  g.iter_rows(set_yaxis_limits)
      
  return g


args = parser.parse_args()

estim_cases, obs_cases = load_case_data(
  cases_paths=args.case_data,
  obs_path=args.observations,
  template=args.template,
  multiples=args.keep_every
)

prms, prms_auto = load_prm_data(
  prms_paths=args.prm_data,
  template=args.template,
  events=args.events,
  autocorr=args.autocorr,
  multiples=args.keep_every
)

context.set(args.context)
if args.latex :
  context.set_latex()

g = plot(
  columns=args.columns,
  events=args.events,
  autocorr=args.autocorr,
  estim_cases=estim_cases,
  obs_cases=obs_cases,
  prms=prms,
  prms_auto=prms_auto,
  renames=args.rename,
  rename_titles=args.rename_title,
  width=args.width,
  aspect=args.aspect,
  alpha=args.alpha,
  ylimits={ row : (float(down), float(up))
            for (row, down, up) in args.ylimits },
)

if args.out :
  g.savefig(
    args.out,
    bbox_inches="tight",
    dpi=600
  )
else :
  plt.show()

#!/usr/bin/python3
# -*- coding: utf-8 -*-

import pandas as pd
from matplotlib import pyplot as plt
import seaborn as sb
import context
import aspect

context.set("paper_cramped")

df = pd.read_csv("_data/mcmc/sir/run.1234.benchmark.melted.csv")

w = aspect.pts_to_inches(172.5)
h = 2 * w / 1.618

f, ax = plt.subplots(
  figsize=(w, h),
  constrained_layout=True
)

methods = { "approx" : "Approximate", "exact" : "Exact" }
df = df.assign(**{"Method" : df["mode"].apply(methods.get)})

f, ax = plt.subplots(figsize=(w, h))
sb.lineplot(
  x="size",
  y="duration",
  hue="Method",
  legend="brief",
  data=df,
  ax=ax
)
ax.set_xscale("log")
ax.set_xlabel("Population size (log)")
ax.set_yscale("log")
ax.set_ylabel("duration (log)")
ax.get_legend().set_frame_on(False)

f.savefig(
  "_data/plot/benchmark_sir_run_1234_exact.paper_cramped.png",
  bbox_inches="tight",
  dpi=600
)

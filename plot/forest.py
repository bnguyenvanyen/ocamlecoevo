#!/usr/bin/python3
# -*- coding: utf-8 -*-

from itertools import chain
from io import StringIO

import numpy as np

from Bio.Phylo.BaseTree import Clade as Clade_bio


def clade_bio(pre_clade, dist) :
  name, clades = pre_clade
  return Clade_bio(name=name, branch_length=dist, clades=clades)


def leaf(k) :
  # I can't create the 'Clade' yet, because I need the branch_length
  # so I create a 'pre clade'
  return (k, [])


def merge(tree_i, d_i, tree_j, d_j) :
  clades = [
    clade_bio(tree_i, d_i),
    clade_bio(tree_j, d_j)
  ]
  return (None, clades)


def iter_leaf_names(pre_clade) :
  _, clades = pre_clade
  return chain(*[ [c.name for c in c.get_terminals()] for c in clades ])


def finalize(pre_clade, d) :
  _, clades = pre_clade
  return Clade_bio(branch_length=d, clades=clades)


def mrcas_to_forests(times, mrcas) :
  # mrcas is a 2D array of mrca time for each pair
  # build the tree by coalescence from the most recent leaves
  coal = dict()
  # track to which subtree nodes belong
  belongs = dict()

  # indices that sort the array, from the largest mrca to the smallest,
  # then nan (which correspond to no coalescence)
  # ~ as two arrays for two dimensions
  inds1, inds2 = np.unravel_index(
    np.argsort(- mrcas, axis=None),
    mrcas.shape
  )

  def get_tree(i) :
    try :
      tree_i, set_i = belongs[i]
    except KeyError :
      tree_i = leaf(i)
      set_i = frozenset([i])
      coal[set_i] = times[i], tree_i
    
    return tree_i, set_i

  
  # do the coalescence following the mrcas
  for i, j in zip(inds1, inds2) :
    ip1 = i + 1
    jp1 = j + 1

    # find the tree associated with each label
    tree_i, set_i = get_tree(ip1)
    tree_j, set_j = get_tree(jp1)

    t = mrcas[i, j]

    if (not (set_i == set_j)) and (not np.isnan(t)) :
      # merge
      t = mrcas[i, j]
      t_i, _ = coal[set_i]
      t_j, _ = coal[set_j]
      d_i = t_i - t
      d_j = t_j - t
      assert(d_i > 0)
      assert(d_j > 0)
      tree = merge(tree_i, d_i, tree_j, d_j)
      coal.pop(set_i)
      coal.pop(set_j)
      set_ij = set_i.union(set_j)
      coal[set_ij] = t, tree
      # replace belongs
      for lf in iter_leaf_names(tree) :
        belongs[lf] = tree, set_ij

  return coal

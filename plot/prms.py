#!/usr/bin/python3
# -*- coding: utf-8 -*-

import argparse

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import seaborn as sb

import parse
import context


plt.switch_backend("Qt5Agg")


parser = argparse.ArgumentParser(
  description=
    "Plot PRM densities and cross-correlations."
)

parser.add_argument(
  "--read-label",
  default="read_name",
  help="How to label runs (legend title for instance).",
)

parser.add_argument(
  "--read-template",
  default="{}",
  help="Template string to pass 'read' elements through.",
)

parser.add_argument(
  "--read",
  type=str,
  action="append",
  nargs=2,
  default=[],
  help="Each element 's' specifies a pattern "
       "'{read_template}.format(s)' pointing to prm color slice files.",
)

parser.add_argument(
  "--colors",
  nargs="+",
  help="Event colors to plot for."
)

parser.add_argument(
  "--rename",
  action="append",
  nargs=2,
  default=[],
  metavar=("color", "label"),
  help="Label to use on the plots for this event color."
)

parser.add_argument(
  "--burn",
  type=int,
  default=0,
  help="Ignore trajectory files with 'k < {burn}'.",
)

parser.add_argument(
  "--multiples",
  type=int,
  nargs=2,
  default=[1, 0],
  help="With multiples '(mult, rem)', ignore samples with "
       "'k mod mult != rem'.",
)

parser.add_argument(
  "--pairs",
  action="store_true",
  help="Plot pair phase space density trajectories."
)

parser.add_argument(
  "--autocorr",
  action="store_true",
  help="Plot autocorrelation functions for chosen colors."
)

parser.add_argument(
  "--crosscorr",
  action="store_true",
  help="Plot a matrix of cross-correlation functions for the chosen colors."
)

parser.add_argument(
  "--titles",
  action="store_true",
  help="Plot facet titles."
)

parser.add_argument(
  "--columns",
  action="store_true",
  help="Plot point density trajectories on different columns."
)

parser.add_argument(
  "--interval",
  choices=["false", "estim", "sd"],
  default="false",
  help=(
    "Plot individual trajectories,\
     or the mean estimator and its confidence interval,\
     or the mean and the standard deviation of the data as interval."
  )
)

parser.add_argument(
  "--plot-colors",
  type=int,
  nargs=2,
  help="Number of colors for the density palette and which one to start with.",
)

parser.add_argument(
  "--out",
  help="Optional template to build paths to output."
)

parser.add_argument(
  "--alpha",
  type=float,
  default=0.1,
  help="Opacity of the density trajectories.",
)

parse.add_argument_context(parser)

parse.add_argument_latex(parser)

parse.add_argument_width(parser)

parse.add_argument_dpi(parser)


aspect = 3.


def pairify(df, label_col) :
  # for one fname, one k, one t, we have a number of colors,
  # each with a density
  # we double that into two colors,
  # to use with relplot and for cross-correlations
  paired = pd.merge(
    df, df,
    how="inner",
    on=["fname", label_col, "k", "origin_t"],
  )
  return paired.sort_values(by=[label_col, "k", "origin_t"])


# compute (auto and) cross-correlations between color densities
# on aggregated dataframe
def correlate(df) :
  def f(group) :
    x = group["density_x"] - 1
    y = group["density_y"] - 1
    n = len(x)
    corr = np.correlate(x, y, mode='full')[-n:]
       
    return group.assign(corr=corr)

  return df.groupby(
    ["fname", "k", "color_x", "color_y"],
    as_index=False,
    sort=False,
  ).apply(
    f
  ).reset_index(
    drop=True
  )


def estimator_kw(interval, alpha) :
  if interval == "false" :
    kw = {
      "units" : "k",
      "estimator" : None,
      "alpha" : alpha,
    }
  elif interval == "estim" :
    kw = {
      "ci" : 99.,
    }
  elif interval == "sd" :
    kw = {
      "ci" : "sd",
    }
  else :
    raise ValueError("Bad value for 'interval'")

  return kw


def columns_kw(cols, label_col, labels, width) :
  if cols :
    if width :
      ncol = len(labels)
      width = asp.pts_to_inches(width)
      height = asp.facet_height(ncol, width=width, aspect=aspect)
      kw = {
        "col":label_col,
        "col_order":labels,
        "height":height
      }
    else :
      kw = {
        "col":label_col,
        "col_order":labels,
      }
  else :
    kw = {}

  return kw


def legend_kw(labels) :
  if len(labels) <= 1 :
    kw = {
      "legend" : False
    }
  else :
    kw = {
      "legend" : "full"
    }
  
  return kw


def plot_traj(df, interval, label_col, labels, cols, titles,
              palette, alpha, width=None) :
  kw_est = estimator_kw(interval, alpha)
  kw_cols = columns_kw(cols, label_col, labels, width)
  kw_leg = legend_kw(labels)

  f = sb.relplot(
    kind="line",
    x="origin_t",
    y="density",
    row="color",
    hue=label_col,
    hue_order=labels,
    facet_kws={
      "sharey":True,
    },
    data=df,
    palette=palette,
    aspect=aspect,
    **kw_est,
    **kw_cols,
    **kw_leg,
  )
  f = f.set_xlabels("Time")
  f = f.set_ylabels("Point density")

  if titles :
    if cols :
      f = f.set_titles("{col_name}")
    else :
      f = f.set_titles("{row_name}")
      # Set better titles following rename
      for ax, color in zip(g.axes[:, 0], colors) :
        try :
          lab = color_labels[color]
          ax.set_title(lab)
        except KeyError :
          pass
  else :
    f = f.set_titles("")

  return f


def plot_pairs(df, interval, label_col, labels,
               palette, alpha, width=None) :
  kw = estimator_kw(interval, alpha)

  if width :
    ncol = len(df["color_y"].unique())
    width = asp.pts_to_inches(width)
    height = asp.facet_height(ncol, width=width, aspect=aspect)
  else :
    height= 2.5

  f = sb.relplot(
    kind="line",
    x="density_x",
    y="density_y",
    row="color_x",
    col="color_y",
    hue=label_col,
    hue_order=labels,
    data=df,
    sort=False,
    palette=palette,
    aspect=aspect,
    height=height,
    **kw
  )

  for i, _ in enumerate(labels) :
    for j, _ in enumerate(labels) :
      if i > j :
        f.axes[i, j].axhline(y=1.)
        f.axes[i, j].axvline(x=1.)

  return f


def plot_auto(df, interval, label_col, labels, colors, cols, titles,
              palette, alpha, width=None) :
  kw_est = estimator_kw(interval, alpha)
  kw_cols = columns_kw(cols, label_col, labels, width)

  f = sb.relplot(
    kind="line",
    x="origin_t",
    y="corr",
    row="color",
    hue=label_col,
    hue_order=labels,
    data=df,
    palette=palette,
    aspect=aspect,
    facet_kws={ "sharey":"row" },
    **kw_est,
    **kw_cols,
  )
  f = f.set_xlabels("Time")
  f = f.set_ylabels("Auto correlation")

  # Set titles
  if titles :
    if cols :
      f = f.set_titles("{col_name}")
    else :
      f = f.set_titles("{row_name}")
      # Set better titles following rename
      for ax, color in zip(g.axes[:, 0], colors) :
        try :
          lab = color_labels[color]
          ax.set_title(lab)
        except KeyError :
          pass
  else :
    f = f.set_titles("")

  for i, _ in enumerate (colors) :
    if cols :
      for j, _ in enumerate(labels) :
        f.axes[i, j].set_ylim(-10, 10)
    else :
      f.axes[i].set_ylim(-10, 10)

  return f


def plot_cross(df, interval, label_col, labels, colors,
               palette, alpha, width=None) :
  kw = estimator_kw(interval, alpha)

  if width :
    ncol = len(df["color_y"].unique())
    width = asp.pts_to_inches(width)
    height = asp.facet_height(ncol, width=width, aspect=aspect)
  else :
    height= 2.5

  f = sb.relplot(
    kind="line",
    x="origin_t",
    y="corr",
    row="color_x",
    col="color_y",
    hue=label_col,
    hue_order=labels,
    data=df,
    palette=palette,
    aspect=aspect,
    **kw
  )
  f = f.set_xlabels("Time")
  f = f.set_ylabels("Cross correlation")

  # set a better ylim
  for i, ci in enumerate(colors) :
    for j, cj in enumerate(colors) :
      f.axes[i, j].set_ylim(-10, 10)

  return f


def burn_and_thin(df, burn, mult, rem) :
  return df[(df["k"] > burn) & (df["k"] % mult == rem)]
  

def read(fname, read_label, read_name, burn, mult, rem) :
  print(fname)
  df = pd.read_csv(fname)
  df = burn_and_thin(df, burn, mult, rem)
  df = df.assign(fname=fname)
  df = df.assign(**{read_label:read_name})

  return df


def read_all(reads, read_label, read_template, burn, mult, rem) :
  dfs = [
    read(
      read_template.format(read_value),
      read_label=read_label,
      read_name=read_name,
      burn=burn,
      mult=mult,
      rem=rem,
    )
    for read_name, read_value in reads
  ]

  df = pd.concat(
    dfs,
    ignore_index=True,
    sort=False,
  )

  return df


def save(f, out, dpi=600) :
  f.tight_layout()
  f.savefig(
    out,
    dpi=dpi,
  )


args = parser.parse_args()

context.set(args.context)

if args.latex :
  context.set_latex()


read_names = [ name for (name, _) in args.read ]

mult, rem = args.multiples


if args.plot_colors :
  n_colors, start_color = args.plot_colors
  palette = sb.color_palette(n_colors=n_colors)
  palette = { name : palette[i + start_color]
              for i, name in enumerate(read_names) }
else :
  palette = None


df = read_all(
  reads=args.read,
  read_label=args.read_label,
  read_template=args.read_template,
  burn=args.burn,
  mult=mult,
  rem=rem,
)


color_labels = { c: name for (c, name) in args.rename
                         if par in args.colors }

df_red = df[df["color"].isin(args.colors)]

if args.autocorr or args.pairs or args.crosscorr :
  df_pairs = pairify(df_red, label_col=args.read_label)

  df_corr = correlate(df_pairs)

  df_autocorr = df_corr[df_corr["color_x"] == df_corr["color_y"]]
  df_autocorr = df_autocorr.assign(color=df_autocorr["color_x"])


f_dens = plot_traj(
  df_red,
  interval=args.interval,
  label_col=args.read_label,
  labels=read_names,
  cols=args.columns,
  titles=args.titles,
  palette=palette,
  alpha=args.alpha,
  width=args.width
)

if args.autocorr :
  f_auto = plot_auto(
    df_autocorr,
    interval=args.interval,
    label_col=args.read_label,
    labels=read_names,
    colors=args.colors,
    cols=args.columns,
    titles=args.titles,
    palette=palette,
    alpha=args.alpha,
    width=args.width
  )

if args.pairs :
  f_pair = plot_pairs(
    df_pairs,
    interval=args.interval,
    label_col=args.read_label,
    labels=read_names,
    palette=palette,
    alpha=args.alpha,
    width=args.width
  )

if args.crosscorr :
  f_cross = plot_cross(
    df_corr,
    interval=args.interval,
    label_col=args.read_label,
    labels=read_names,
    colors=args.colors,
    palette=palette,
    alpha=args.alpha,
  )


if args.out :
  save(
    f_dens.fig,
    args.out.format("traj"),
    dpi=args.dpi
  )
  if args.autocorr :
    save(
      f_auto.fig,
      args.out.format("auto"),
      dpi=args.dpi
    )
  if args.pairs :
    save(
      f_pair.fig,
      args.out.format("pair"),
      dpi=args.dpi
    )
  if args.crosscorr :
    save(
      f_cross.fig,
      args.out.format("cross"),
      dpi=args.dpi
    )
else :
  plt.show()

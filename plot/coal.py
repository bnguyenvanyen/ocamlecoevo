#!/usr/bin/python3
# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import seaborn as sb


def last_coal(df) :
  coal_times = sorted(df.loc["2-1":].unique())
  try :
    last = coal_times[1] - coal_times[0]
  except IndexError :
    last = np.nan
  return last


df_ori = pd.read_csv("test_constant_1234.mrca.csv")
df_ori = df_ori.assign(last=df_ori.apply(last_coal, axis="columns"))

df_bis = pd.read_csv("test_constant_bis_1234.mrca.csv")
df_bis = df_bis.assign(last=df_bis.apply(last_coal, axis="columns"))

df = pd.concat(
  [df_ori, df_bis],
  keys=["original", "bis"],
  names=["version"],
  # ignore_index=True
).reset_index(level=0)

print(df.head())
print(df["version"].unique())

f, ax = plt.subplots()

for v in ["original", "bis"] :
  ax.hist(
    df[df["version"]==v]["last"].dropna(),
    cumulative=True,
    density=True,
    label=v,
    bins=50,
    alpha=0.5
  )

x = np.linspace(0, 8, 100)
y_one = 1 - np.exp(-x)
y_half = 1 - np.exp(-1/2 * x)
y_third = 1 - np.exp(-2/3 * x)
y_quart = 1 - np.exp(-3/4 * x)

ax.plot(x, y_one, label="E(1)")
ax.plot(x, y_half, label="E(0.5)")
ax.plot(x, y_third, label="E(0.66)")
ax.plot(x, y_quart, label="E(0.75)")

plt.legend()
plt.show()

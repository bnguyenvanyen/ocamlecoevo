#!/usr/bin/python3
# -*- coding: utf-8 -*-

import re
import glob
from io import StringIO

import numpy as np
import pandas as pd
from Bio import SeqIO
from Bio.Phylo import read as phylo_read


def parse_matrix_position(colname, pattern) :
  match = pattern.search(colname)
  if match :
    row = int(match.group("row"))
    col = int(match.group("col"))
    return (row, col)
  else :
    return None

# columns for matrix values should be of the form '{i|j}',
# starting from 1
# need to escape '|' (else it means or)
bracket_pat = re.compile(r"{(?P<row>\d+)\|(?P<col>\d+)}")
def parse_matrix_position_brackets(colname) :
  return parse_matrix_position(colname, bracket_pat)


hyphen_pat = re.compile(r"(?P<row>\d+)-(?P<col>\d+)")
def parse_matrix_position_hyphen(colname) :
  return parse_matrix_position(colname, hyphen_pat)


def read_matrix_traj(path, parse=parse_matrix_position_brackets) :
  df = pd.read_csv(path)
  positions = [ (c, parse(c)[0], parse(c)[1])
                for c in df.columns if parse(c) ]
  n = max([ i for _, i, _ in positions ])
  m = max([ j for _, _, j in positions ])
  a = np.full([len(df), n, m], np.nan)

  for k, (_, row) in enumerate(df.iterrows()) :
    for c, i, j in positions :
      a[k, i - 1, j - 1] = row[c]

  return df, a


float_pat = r"[+-]?(\d+(\.\d*)?|\.\d+)([eE][+-]?\d+)?"
stamp_pat = re.compile(":(?P<time>{})".format(float_pat))
def parse_time_stamp(s) :
  match = stamp_pat.search(s)
  if match :
    t = float(match.group("time"))
    return t
  else :
    return None


header_pat = re.compile("(?P<label>\d+):(?P<time>{})".format(float_pat))
def parse_fasta_header(s, regex=header_pat) :
  match = regex.search(s)
  if match :
    label = match.group("label")
    t = float(match.group("time"))
    return (label, t)
  else :
    return None


def read_fasta(path, function) :
  with open(path, "rU") as handle :
    v = [ function(r)
          for r in SeqIO.parse(handle, "fasta")
          if function(r) is not None ]

  return v


def read_newick(path, parse_header) :
  trees = dict()
  with open(path, "r") as f :
    for i, line in enumerate(f) :
      if i % 3 == 0 :
        header = parse_header(line)
      if i % 3 == 1 :
        tree = phylo_read(StringIO(line), "newick")
      if i % 3 == 2 :
        assert(line == "\n")
        trees[header] = tree

  return trees


def read_newick_forests(path, parse_header_sample, parse_header_tree) :
  forests = dict()
  with open(path, "r") as f :
    i = 0
    for line in f :
      if i == 0 :
        sample_header = parse_header_sample(line)
        forest = dict()
      if i == 1 :
        # try to read a tree header
        tree_header = parse_header_tree(line)
        if tree_header is None :
          # not a tree header -> a new block starts,
          # with a sample header
          forests[sample_header] = forest
          sample_header = parse_header_sample(line)
          forest = dict()
          i = 0
      if i == 2 :
        tree = phylo_read(StringIO(line), "newick")
      if i == 3 :
        assert(line == "\n")
        forest[tree_header] = tree
        # try to read a tree header
        i = 0
      i += 1

  return forests


k_pat = re.compile(r".(\d+).data.cases.csv")
def parse_k(fname) :
  match = k_pat.search(fname)
  if match :
    return int(match.group(1))
  else :
    raise ValueError("does not contain a value for k")


def burn_and_thin(df, burn=0, mult=1, rem=0) :
  return df[(df["k"] > burn) & (df["k"] % mult == rem)]


def read_cases(fname, pattern_label, pattern_name, pattern, focus=False) :
  try :
    df = pd.read_csv(fname)
  except pd.errors.EmptyDataError :
    df = pd.DataFrame([], columns=["t", "cases"])

  df = df.assign(focus=focus)
  df = df.assign(fname=fname)
  df = df.assign(pattern=pattern)
  df = df.assign(**{pattern_label:pattern_name})
  try :
    k = parse_k(fname)
  except ValueError :
    k = None
  df = df.assign(k=k)

  return df
  

def read_glob_cases(patterns, pattern_label) :
  reads_glob = [
    read_cases(
      fname,
      pattern_label,
      pattern_name=pattern_name,
      pattern=pattern,
      focus=False
    )
    for pattern_name, pattern in patterns
    for fname in glob.glob(pattern)
  ]

  if reads_glob == [] :
    df_glob = pd.DataFrame({
      "k" : [np.nan],
      "t" : [np.nan],
      "cases" : [np.nan],
      "pattern" : ["no_glob"],
      pattern_label : ["no_glob"],
      "fname" : ["no_glob"],
      "focus" : [False],
    })
  else :
    df_glob = pd.concat(
      reads_glob,
      ignore_index=True,
    )

  return df_glob


def read_focus_cases(focuses, pattern_label) :
  reads_focus = [ read_cases(
    fname,
    pattern_label,
    pattern_name=focus_name,
    pattern=np.nan,
    focus=True
  ) for (focus_name, fname) in focuses ]

  if reads_focus == [] :
    df_focus = pd.DataFrame()
  else :
    df_focus = pd.concat(
      reads_focus,
      ignore_index=True,
    )

  return df_focus


def read_all_cases(patterns, focuses, pattern_label) :
  df_glob = read_glob_cases(
    patterns=patterns,
    pattern_label=pattern_label,
    burn=burn,
    multiples=multiples,
  )

  df_focus = read_focus_cases(
    focuses=focuses,
    pattern_label=pattern_label,
  )

  df = pd.concat(
    [df_glob, df_focus],
    sort=False
  )

  return df


def read_prms(fname, read_label, read_name) :
  df = pd.read_csv(fname)
  df = df.assign(fname=fname)
  df = df.assign(**{read_label:read_name})

  return df


def read_all_prms(reads, read_label, read_template) :
  dfs = [
    read_prms(
      read_template.format(read_value),
      read_label=read_label,
      read_name=read_name,
    )
    for read_name, read_value in reads
  ]

  if reads == [] :
    df = pd.DataFrame({
      "k" : [np.nan],
      "origin_t" : [np.nan],
      "density" : [np.nan],
      "color" : [np.nan],
      "fname" : [np.nan],
      read_label : [np.nan],
    })
  else :
    df = pd.concat(
      dfs,
      ignore_index=True,
      sort=False,
    )

  return df
  

#!/usr/bin/python3
# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd
from matplotlib.patches import Patch
from matplotlib.gridspec import GridSpec
from matplotlib import pyplot as plt
import matplotlib
import seaborn as sb


class Mapper(object) :
  def __init__(self,
               rows=None, columns=None, hues=None,
               palette=None,
               legend=None, legend_title=None, label_legend=None,
               sharex=False, sharey=False,
               aspect=None, figsize=None) :
    self.rows = rows
    self.columns = columns
    self.hues = hues
    self.palette = palette

    ncol = 1 if columns is None else len(columns)
    nrow = 1 if rows is None else len(rows)
    if aspect :
      subplot_kw = {
        "aspect" : aspect
      }
    else :
      subplot_kw = {}

    f = plt.figure(
      constrained_layout=True,
      figsize=figsize,
    )
    self.fig = f

    self.spec = self.set_legend_grid(
      legend,
      title=legend_title,
      relabel=label_legend
    )

    # improve constrained layout
    w, h = figsize
    f.set_constrained_layout_pads(
      w_pad = 0,
      h_pad = 0,
      wspace = 0.01,
      hspace = 0.01,
    )

    self.add_subplots(sharex, sharey, subplot_kw)

  def set_legend_grid(self, legend, title, relabel) :
    ncol = 1 if self.columns is None else len(self.columns)
    nrow = 1 if self.rows is None else len(self.rows)

    if legend == "horizontal" :
      handles, labels = self.legend_entries(relabel=relabel)
      if title :
        handles = (Patch(alpha=0),) + handles
        labels = (title,) + labels
      # get legend size
      lw, lh = self.get_legend_size_inches(
        handles=handles,
        labels=labels,
        ncol=1 + len(labels),
      )

      # make a grid to partition between plots and legend
      w, h = self.fig.get_size_inches()
      needed = lh / h + 0.01
      print("Space needed:", needed)
      g_all = self.fig.add_gridspec(
        2, 1,
        height_ratios=[1 - needed, needed]
      )
      # put the plots into the first row
      g = g_all[0].subgridspec(nrow, ncol)
      # put the legend into the second row
      ax_legend = self.fig.add_subplot(g_all[1])
      # hide ax_legend
      ax_legend.axis("off")
      self.legend = ax_legend.legend(
        handles=handles,
        labels=labels,
        ncol=1 + len(labels),
        frameon=False,
      )

      # return grid for plots
      return g
    elif legend == "vertical" :
      handles, labels = self.legend_entries(relabel=relabel)
      # get legend size
      lw, lh = self.get_legend_size_inches(
        handles=handles,
        labels=labels,
        ncol=1,
        title=title,
      )

      # make a grid to partition between plots and legend
      w, h = self.fig.get_size_inches()
      needed = lw / w + 0.01
      print("Space needed:", needed)
      g_all = self.fig.add_gridspec(
        1, 2,
        width_ratios=[1 - needed, needed]
      )
      # put the plots into the first column
      g = g_all[0].subgridspec(nrow, ncol)
      # put the legend into the second column
      ax_legend = self.fig.add_subplot(g_all[1])
      # hide ax_legend
      ax_legend.axis("off")
      self.legend = ax_legend.legend(
        handles=handles,
        labels=labels,
        ncol=1,
        title=title,
        frameon=False,
      )

      # return grid for plots
      return g
    elif legend is None :
      return self.fig.add_gridspec(nrow, ncol)
    else :
      raise ValueError("legend not 'vertical', 'horizontal', or None")

  def shared_with(self, axes, row, col, share) :
    if isinstance(share, np.ndarray) :
      i, j = share[row, col]
      return axes[i, j]
    elif share is None :
      return None
    elif share == "all" :
      return axes[0, 0]
    elif share == "row" :
      return axes[row, 0]
    elif share == "col" :
      return axes[0, col]
    else :
      raise ValueError("Invalid value for share")

  def add_subplots(self, sharex, sharey, subplot_kw) :
    ncol = 1 if self.columns is None else len(self.columns)
    nrow = 1 if self.rows is None else len(self.rows)
    # copied from Figure.subplots
    axes = np.empty((nrow, ncol), dtype=object)
    for row in range(nrow):
      for col in range(ncol):
        subplot_kw["sharex"] = self.shared_with(axes, row, col, sharex)
        subplot_kw["sharey"] = self.shared_with(axes, row, col, sharey)
        axes[row, col] = self.fig.add_subplot(
          self.spec[row, col],
          **subplot_kw
        )

    # turn off redundant tick labeling
    if sharex in ["col", "all"]:
      # turn off all but the bottom row
      for ax in axes[:-1, :].flat:
        ax.xaxis.set_tick_params(
          which='both',
          labelbottom=False,
          labeltop=False
        )
        ax.xaxis.offsetText.set_visible(False)

    # I cheat with the isinstance, for the more complex case
    # for instance used in cases_and_prms.py
    if isinstance(sharey, np.ndarray) or sharey in ["row", "all"] :
      # turn off all but the first column
      for ax in axes[:, 1:].flat:
        ax.yaxis.set_tick_params(
          which='both',
          labelleft=False,
          labelright=False
        )
        ax.yaxis.offsetText.set_visible(False)

    # despine all axes (top and right)
    for side in ["top", "right"] :
      for ax in axes[:, :].flat :
        ax.spines[side].set_visible(False)

    self.fig.align_ylabels(axs=axes[:, 0])

    # set up the axes relations
    df_axes = pd.DataFrame({
      col : pd.Series({
          row : axes[i, j] for (i, row) in enumerate(self.rows)
      })
      for (j, col) in enumerate(self.columns)
    })

    self.axes = df_axes

  def get_legend_size_inches(self, **kwargs) :
    legend = self.fig.legend(**kwargs)
    lwe = legend.get_window_extent(self.fig.canvas.get_renderer())
    lw = lwe.width / self.fig.dpi
    lh = lwe.height / self.fig.dpi
    legend.remove()

    return (lw, lh)

  def iter_columns(self, f) :
    for column in self.columns :
      f(axes=self.axes[column],
        column=column)

  def iter_rows(self, f) :
    for row in self.rows :
      f(axes=self.axes.loc[row, :],
        row=row)

  def iter_facets(self, f) :
    for row in self.rows :
      for column in self.columns :
        f(ax=self.axes.loc[row, column],
          row=row,
          column=column)

  def iter(self, f) :
    for row in self.rows :
      for column in self.columns :
        for hue in self.hues :
          f(ax=self.axes.loc[row, column],
            row=row,
            column=column,
            hue=hue)

  def iter_rows_column(self, f, column) :
    for row in self.rows :
      f(ax=self.axes.loc[row, column],
        row=row)

  def iter_columns_row(self, f, row) :
    for column in self.columns :
      f(ax=self.axes.loc[row, column],
        column=column)

  def legend_entries(self, relabel=None) :
    def patch(hue) :
      return Patch(
        facecolor=self.palette[hue]
      )
    def lab(hue) :
      if relabel :
        return relabel(hue)
      else :
        return hue

    handles, labels = zip(*[
      (patch(hue), relabel(hue)) for hue in self.hues
    ])
    return (handles, labels)
      
  def _redraw(self) :
    self.fig.draw(self.fig.canvas.get_renderer())

  def set_facet_titles(self, f, **kwargs) :
    def g(ax, row, column) :
      title = f(row=row, column=column)
      ax.set_title(title, **kwargs)
    self.iter_facets(g)

  def set_row_titles(self, f, **kwargs) :
    def g(axes, row) :
      title = f(row=row)
      axes.iloc[0].set_title(title, loc="left", **kwargs)
    self.iter_rows(g)

  def set_column_titles(self, f, **kwargs) :
    def g(axes, column) :
      title = f(column=column)
      axes.iloc[0].set_title(title, loc="center", **kwargs)
    self.iter_columns(g)

  def savefig(self, fname, **kwargs) :
    self.fig.savefig(
      fname,
      **kwargs
    )

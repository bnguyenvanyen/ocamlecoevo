#!/usr/bin/python3
# -*- coding: utf-8 -*-

import argparse
import pandas as pd
from matplotlib import pyplot as plt
import seaborn as sb

from diagnosis import \
  dirac_wasserstein_distance, \
  binned_wasserstein_distance


parser = argparse.ArgumentParser(
    description=
    "Compare two empirical distributions."
)

parser.add_argument(
  "--samples",
  nargs=2,
  action="append",
  default=[],
  help="Label and path for distribution samples.",
)

parser.add_argument(
  "--columns",
  nargs="+",
  help="Columns to compute the distance on"
)

parser.add_argument(
  "--keep-time",
  type=float,
  help="Pass val to keep rows where df['t'] == val."
)

parser.add_argument(
  "--every",
  type=int,
  help="Compute the EMD every increment of 'every' samples."
)

parser.add_argument(
  "--stop",
  type=int,
  help="Stop at 'stop' samples in total."
)

def read(path, name, keep) :
  df = pd.read_csv(path)

  if keep :
    return df[df["t"] == keep]
  else :
    return df


def distances(df1, df2, cols, every, stop) :
  ks = list()
  ds = list()
  if not stop :
    stop = len(df1)
    assert (len(df2) == stop)
  k = every
  while k <= stop :
    # _, d = dirac_wasserstein_distance(
    #   df1[df1["k"] <= k],
    #   df2[df2["k"] <= k],
    #   cols
    # )
    _, d = binned_wasserstein_distance(
      df1[df1["k"] <= k],
      df2[df2["k"] <= k],
      { c : 10 for c in cols },
      cols
    )
    ks.append(k)
    ds.append(d)
    k = k + every

  return pd.DataFrame({"k":ks, "distance":ds})


args = parser.parse_args()


if len(args.samples) != 2 :
  raise ValueError("Can only compare two empirical distributions")


name1, path1 = args.samples[0]
df1 = read(path1, name1, args.keep_time)

name2, path2 = args.samples[1]
df2 = read(path2, name2, args.keep_time)

print(df1)
print(df2)

ds = distances(df1, df2, args.columns, args.every, args.stop)

print(ds)

plt.plot(ds["k"], ds["distance"])

agg = pd.concat(
  [df1.assign(source="first"), df2.assign(source="second")],
  ignore_index=True
)

print(agg)

melted = agg[["k", "source"] + args.columns].melt(
  id_vars=["k", "source"],
  var_name="compartment"
)

print(melted)

g = sb.FacetGrid(
  data=melted,
  hue="source",
  row="compartment",
)

g.map(plt.hist, "value", bins=30)


plt.show()


#!/usr/bin/python3
# -*- coding: utf-8 -*-

import argparse

# Common figure aesthetics parameters

def add_argument_width(p) :
  p.add_argument(
    "--width",
    type=float,
    default=None,
    help="Width of the plots in pts."
  )


def add_argument_aspect(p) :
  p.add_argument(
    "--aspect",
    type=float,
    default=1.618,
    help="Aspect ratio of the plot facets."
  )


def add_argument_decoration_width(p) :
  p.add_argument(
    "--decoration-width",
    type=float,
    default=0.15,
    help="Space left for axes decorations."
  )


def add_argument_decoration_height(p) :
  p.add_argument(
    "--decoration-height",
    type=float,
    default=0.15,
    help="Space left for axes decorations."
  )


def add_argument_legend_bbox(p) :
  p.add_argument(
    "--legend-bbox",
    nargs=2,
    type=float,
    help="Bbox to anchor the figure legend upper left corner at."
  )


def add_argument_dpi(p) :
  p.add_argument(
    "--dpi",
    type=float,
    default=300,
    help="Dots per inches for the figures."
  )


def add_argument_latex(p) :
  p.add_argument(
    "--latex",
    action="store_true",
    help="Use LaTeX to format all text.",
  )


def add_argument_context(p) :
  p.add_argument(
    "--context",
    choices=[
      "paper",
      "notebook",
      "talk",
      "poster",
      "paper_cramped",
      "talk_cramped"
    ],
    default="notebook",
    help="Seaborn plotting context.",
  )

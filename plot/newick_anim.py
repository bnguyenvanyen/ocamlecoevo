#!/usr/bin/python3
# -*- coding: utf-8 -*-

import argparse
import re

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from matplotlib import animation

from Bio.Phylo import draw

from read import\
  read_fasta,\
  parse_time_stamp,\
  read_newick


plt.switch_backend("Qt5Agg")


parser = argparse.ArgumentParser(
  description="Plot an animation for successive values of a tree from Newick file."
)


parser.add_argument(
  "--samples",
  help="Path to Newick file of trees."
)


parser.add_argument(
  "--times",
  help="Lineage sampling times."
)


parser.add_argument(
  "--t0",
  type=float,
  default=0.,
  help="Initial time."
)


header_pat = re.compile(r">(?P<k>\d+)")
def parse_header(header) :
  match = header_pat.search(header)
  if match :
    k = int(match.group("k"))
    return k
  else :
    return None


def plot_tree(tree, d, ax) :
  draw(
    tree.clade[0][0],
    label_func=lambda c : str(c.name) if c.name else "",
    do_show=False,
    axes=ax
  )


def plot(trees, t0) :
  f, ax  = plt.subplots()

  ax.set_title("k = 0")
  plot_tree(trees["tree"].iloc[0], t0, ax)

  def update(frame) :
    tree = trees["tree"].iloc[frame]
    ax.clear()
    plot_tree(tree, t0, ax)
    k = trees["k"].iloc[frame]
    ax.set_title("k = {}".format(k))

  ani = animation.FuncAnimation(
    f,
    update,
    frames=len(trees)
  )

  return ani


args = parser.parse_args()


ts = read_fasta(args.times, lambda rec : parse_time_stamp(rec.id))
times = { i + 1 : t for i, t in enumerate(sorted(ts, reverse=True)) }

trees = read_newick(
  args.samples,
  parse_header=parse_header
)

df_tree = pd.DataFrame({"k":list(trees.keys()), "tree":list(trees.values())})

ani = plot(df_tree, args.t0)

plt.show()

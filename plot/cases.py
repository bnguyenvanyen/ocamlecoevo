#!/usr/bin/python3
# -*- coding: utf-8 -*-

from os.path import splitext
import glob
import re
import argparse

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import seaborn as sb

import parse
import context
import aspect as asp


plt.switch_backend("Qt5Agg")


parser = argparse.ArgumentParser(
    description=
    "Plot 'case counts' trajectories for many simulations."
)

parser.add_argument(
  "--concat",
  action="store_true",
  help="Indicate that the paths point to concatenated data files."
)

parser.add_argument(
  "--glob-label",
  default="pattern_name",
  help="How to label patterns in the legend.",
)

parser.add_argument(
  "--glob-template",
  default="{}",
  help="template string to pass 'glob' elements through.",
)

parser.add_argument(
  "--glob",
  nargs=2,
  default=[],
  action="append",
  help="each element '(lab, s)' specifies a glob pattern "
       "'{glob_template}.format(s)' pointing to trajectory files.",
)

parser.add_argument(
  "--focus",
  default=[],
  action="append",
  help="each element is a focus trajectory. "
       "Plotted in black with style variations (solid, dashed, etc).",
)

parser.add_argument(
  "--cases-column",
  default="cases",
  help="Name of the column to use."
)

parser.add_argument(
  "--range",
  nargs=2,
  type=float,
  metavar=("lower", "upper"),
  help="Set the ylimits."
)

parser.add_argument(
  "--burn",
  type=int,
  default=0,
  help="ignore trajectory files with 'k < {burn}'.",
)

parser.add_argument(
  "--multiples",
  type=int,
  nargs=2,
  default=[1, 0],
  help="with multiples '(mult, rem)', ignore trajectory files with "
       "'k mod mult != rem'.",
)

parser.add_argument(
  "--rows",
  action="store_true",
  help="Plot different labels on different rows",
)

parser.add_argument(
  "--columns",
  action="store_true",
  help="Plot different labels on different columns.",
)

parser.add_argument(
  "--interval",
  choices=["false", "estim", "sd"],
  default="false",
  help=(
    "Plot individual trajectories,\
     or the mean estimator and its confidence interval,\
     or the mean and the standard deviation of the data as interval."
  )
)

parser.add_argument(
  "--alpha",
  type=float,
  default=0.1,
  help="Opacity of the cases glob trajectories.",
)

parser.add_argument(
  "--colors",
  type=int,
  nargs=2,
  help="Number of colors to use in the glob palette and which to start from."
)

parser.add_argument(
  "--save",
  action="store_true",
  help="Save and load plot data as possible."
)

parser.add_argument(
  "--out",
  help="Output filename. If not given, interactive plot.",
)

parser.add_argument(
  "--no-xlabels",
  action="store_true",
  help="Don't plot x labels."
)

parser.add_argument(
  "--no-titles",
  action="store_true",
  help="Don't plot axes titles."
)

parse.add_argument_context(parser)

parse.add_argument_latex(parser)

parse.add_argument_width(parser)

parse.add_argument_decoration_width(parser)

parse.add_argument_decoration_height(parser)

parse.add_argument_aspect(parser)

parse.add_argument_dpi(parser)


parser.add_argument("--split", action="store_true")


k_pat = re.compile(r".(\d+).data.(\w+).csv")

def get_k(fname) :
  match = k_pat.search(fname)
  if match :
    return int(match.group(1))
  else :
    return None

def keep_fname(fname, burn, multiples, remainder) :
  match = k_pat.search(fname)
  if match :
    k = int(match.group(1))
    return ((k > burn) and (k % multiples == remainder))
  else :
    return True


# also differentiate between different patterns and focuses :
def read(fname, cases_column, pattern_label,
         k=np.nan, pattern_name=np.nan, pattern=np.nan, focus=False) :
  try :
    df = pd.read_csv(fname)
  except pd.errors.EmptyDataError :
    df = pd.DataFrame([], columns=["t", cases_column])

  df = df.assign(focus=focus)
  df = df.assign(fname=fname)
  df = df.assign(k=k)
  df = df.assign(pattern=pattern)
  df = df.assign(**{pattern_label:pattern_name})

  return df


def read_all(patterns, focuses, cases_column, pattern_label,
             burn, multiples, remainder) :
  reads_glob = [
    read(
      fname,
      cases_column=cases_column,
      pattern_label=pattern_label,
      k=get_k(fname),
      pattern_name=pattern_name,
      pattern=pattern,
      focus=False
    )
    for pattern_name, pattern in patterns
    for fname in glob.glob(pattern)
    if keep_fname(fname, burn, multiples, remainder)
  ]

  if reads_glob == [] :
    df_glob = pd.DataFrame({
      "t" : [np.nan],
      cases_column : [np.nan],
      "pattern" : ["no_glob"],
      pattern_label : [""],  # no title
      "fname" : ["no_glob"],
      "k" : [np.nan],
      "focus" : [False],
    })
  else :
    df_glob = pd.concat(
      reads_glob,
      ignore_index=True,
      sort=False,
    )

  reads_focus = [ read(
    fname,
    cases_column=cases_column,
    pattern_label=pattern_label,
    focus=True
  ) for fname in focuses ]

  if reads_focus == [] :
    df_focus = pd.DataFrame()
  else :
    df_focus = pd.concat(
      reads_focus,
      ignore_index=True,
      sort=False,
    )

  df = pd.concat(
    [df_glob, df_focus],
    ignore_index=True,
    sort=False
  )

  return df


def read_concat(fname, cases_column, concat_label,
                burn, multiples, remainder,
                concat_name=np.nan) :
  try :
    df = pd.read_csv(fname)
  except pd.errors.EmptyDataError :
    df = pd.DataFrame([], columns=["k", "t", cases_column])

  # filter out unwanted ks
  df = df[(df["k"] > burn) & (df["k"] % multiples == remainder)]

  df = df.assign(focus=False)
  df = df.assign(fname=fname)
  df = df.assign(**{concat_label:concat_name})

  return df.astype({concat_label:str})


def read_concat_all(concat_paths, focuses, cases_column, concat_label,
                    burn, multiples, remainder) :
  reads_many = [
    read_concat(
      fname=concat_path,
      cases_column=cases_column,
      concat_label=concat_label,
      burn=burn,
      multiples=multiples,
      remainder=remainder,
      concat_name=concat_name
    )
    for concat_name, concat_path in concat_paths
  ]

  if reads_many == [] :
    df_many = pd.DataFrame({
      "k": [np.nan],
      "t": [np.nan],
      cases_column: [np.nan],
      "pattern": ["no_glob"],
      concat_label: [""],
      "fname": ["no_glob"],
      "focus": [False],
    })
  else :
    df_many = pd.concat(
      reads_many,
      ignore_index=True,
    )

  reads_focus = [ read(
    fname=fname,
    cases_column=cases_column,
    pattern_label=concat_label,
    focus=True,
  ) for fname in focuses ]

  if reads_focus == [] :
    df_focus = pd.Dataframe()
  else :
    df_focus = pd.concat(
      reads_focus,
      ignore_index=True,
    )

  df = pd.concat(
    [df_many, df_focus],
    ignore_index=True,
    sort=False
  )

  return df


focus_palette = sb.color_palette(["black"])

def plot_patterns(df, cases_column, pattern_label, pattern_names,
                  rows, columns, alpha, width, aspect,
                  decoration_width, decoration_height, interval) :
  kw = {
    "kind" : "line",
    "x" : "t",
    "y" : cases_column,
    "hue" : pattern_label,
    "hue_order" : pattern_names,
  }

  if not aspect :
    aspect = 2.

  if rows :
    height = asp.facet_height(
      ncol=1,
      width=asp.pts_to_inches(width),
      aspect=aspect,
      decoration_width=decoration_width,
      decoration_height=decoration_height,
    )
    kw_row = {
      "row": pattern_label,
      "row_order": pattern_names,
      "height": height,
    }
  else :
    kw_row = {}

  if columns :
    height = asp.facet_height(
      ncol=len(pattern_names),
      width=asp.pts_to_inches(width),
      aspect=aspect,
      decoration_width=decoration_width,
      decoration_height=decoration_height,
    )
    kw_col = {
      "col" : pattern_label,
      "col_order" : pattern_names,
      "height": height,
    }
  else :
    kw_col = {}

  if len(pattern_names) <= 1 or columns or rows :
    kw_leg = {
      "legend" : False
    }
  else :
    kw_leg = {
      "legend" : "full"
    }

  if interval == "false" :
    kw_estim = {
      "units" : "k",
      "estimator" : None,
      "alpha" : alpha,
    }
  elif interval == "estim" :
    kw_estim = {
      "ci" : 99.,
    }
  elif interval == "sd" :
    kw_estim = {
      "ci" : "sd",
    }
  else :
    raise ValueError("Bad value for 'interval'")

  f = sb.relplot(
    palette=glob_palette,
    facet_kws={
      "sharey":"row",
    },
    data=df,
    aspect=aspect,
    **kw,
    **kw_row,
    **kw_col,
    **kw_estim,
    **kw_leg,
  )

  return f


def plot(df, cases_column, pattern_label, pattern_names, rows, columns,
         interval, alpha, xlabels=True, titles=True,
         range=None, width=None, aspect=None,
         decoration_width=None, decoration_height=None) :
  def plot_focuses(x, y, data, **kw) :
    # keep only the focus data
    data = df[df["focus"]]
    ax = plt.gca()
    sb.lineplot(
      x=x,
      y=y,
      estimator=None,
      hue="fname",
      style="fname",
      palette=focus_palette,
      # We cheat to try to control the linewidth
      size="fname",
      sizes=(0.5, 0.5),
      data=data,
      ax=ax
    )

  f = plot_patterns(
    df,
    cases_column=cases_column,
    pattern_label=pattern_label,
    pattern_names=pattern_names,
    rows=rows,
    columns=columns,
    alpha=alpha,
    width=width,
    aspect=aspect,
    decoration_width=decoration_width,
    decoration_height=decoration_height,
    interval=interval,
  )

  f = f.map_dataframe(plot_focuses, "t", cases_column)

  if not xlabels :
    # remove xlabels
    f.set_xlabels("")

  if not titles :
    # remove titles
    f.set_titles("")
  else :
    if columns :
      f.set_titles("{col_name}")
    elif rows :
      f.set_titles("{row_name}")
    else :
      f.set_titles("")

  for ax in f.axes.flat :
    # don't use offsets or scientific notation for time
    ax.ticklabel_format(axis="x", useOffset=False, style="plain")
    # don't use offsets for cases
    ax.ticklabel_format(axis="y", useOffset=False)
   
  if range is not None :
    f.set(ylim=range)

  return f


args = parser.parse_args()


patterns = [ (lab, args.glob_template.format(pn))
             for (lab, pn) in args.glob ]
if len(args.glob) == 0 :
  pattern_names = [ "" ]  # no title
else :
  # Careful : pattern names should not be numbers
  pattern_names = [ pn for pn, _ in patterns ]


focuses = args.focus

burn = args.burn
multiples, remainder = args.multiples

context.set(args.context)

if args.latex :
  context.set_latex()


if args.save and args.out :
  try :
    fname_save = splitext(args.out)[0] + ".csv"
    df = pd.read_csv(fname_save)
  except (TypeError, FileNotFoundError, AttributeError) :
    if args.concat :
      df = read_concat_all(
        concat_paths=patterns,
        focuses=focuses,
        cases_column=args.cases_column,
        concat_label=args.glob_label,
        burn=burn,
        multiples=multiples,
        remainder=remainder,
      )
    else :
      df = read_all(
        patterns=patterns,
        focuses=focuses,
        cases_column=args.cases_column,
        pattern_label=args.glob_label,
        burn=burn,
        multiples=multiples,
        remainder=remainder,
      )
    df.to_csv(
      splitext(args.out)[0] + ".csv",
      index=False
    )
else :
  if args.concat :
    df = read_concat_all(
      concat_paths=patterns,
      focuses=focuses,
      cases_column=args.cases_column,
      concat_label=args.glob_label,
      burn=burn,
      multiples=multiples,
      remainder=remainder,
    )
  else :
    df = read_all(
      patterns=patterns,
      focuses=focuses,
      cases_column=args.cases_column,
      pattern_label=args.glob_label,
      burn=burn,
      multiples=multiples,
      remainder=remainder,
    )


if args.colors :
  n_colors, start_color = args.colors
else :
  n_colors = len(pattern_names)
  start_color = 0

pal = sb.color_palette(n_colors=n_colors)
glob_palette = { pn : pal[i + start_color]
                 for i, pn in enumerate(pattern_names) }


if not args.split :
  f = plot(
    df,
    cases_column=args.cases_column,
    pattern_label=args.glob_label,
    pattern_names=pattern_names,
    rows=args.rows,
    columns=args.columns,
    interval=args.interval,
    alpha=args.alpha,
    xlabels=not args.no_xlabels,
    titles=not args.no_titles,
    range=args.range,
    width=args.width,
    aspect=args.aspect,
    decoration_width=args.decoration_width,
    decoration_height=args.decoration_height,
  )
else :
  if patterns == [] :
    patts = [ (np.nan, np.nan) ]
  else :
    patts = patterns

  df_nan = pd.DataFrame({
    "t" : [ np.nan for _ in patts ],
    args.cases_column : [ np.nan for _ in patts ],
    "pattern" : [ patt for _, patt in patts ],
    args.glob_label : [ pn for pn, _ in patts ],
    "focus" : [ False for _ in patts ]
  })
  f_split = {
    pn : plot(
        pd.concat(
          [
            df[(df[args.glob_label] == pn)],
            df_nan,
          ],
          axis=0,
          join="outer",
        ),
        cases_column=args.cases_column,
        pattern_label=args.glob_label,
        pattern_names=pattern_names,
        rows=args.rows,
        columns=args.columns,
        interval=args.interval,
        alpha=args.alpha,
        range=args.range,
        width=args.width,
        aspect=args.aspect,
        decoration=args.decoration_width,
      )
    for pn, _ in patts
  }


def save(f, out, dpi=600) :
  f.fig.tight_layout(
    pad=0.3
  )
  print("Size:", f.fig.get_size_inches())
  f.savefig(out, dpi=dpi)


if args.out :
  if not args.split :
    save(
      f,
      args.out,
      dpi=args.dpi
    )
  else :
    print("split")
    for pn, d in f_split.items() :
      for src, f in d.items() :
        save(
          f,
          args.out.format("{}.{}".format(pn, src)),
          dpi=args.dpi
        )
else :
  plt.show()

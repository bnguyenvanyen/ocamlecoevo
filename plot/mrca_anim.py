#!/usr/bin/python3
# -*- coding: utf-8 -*-

import argparse

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from matplotlib import animation

from Bio.Phylo.BaseTree import Clade as Clade_bio
from Bio.Phylo import draw

from read import\
  read_matrix_traj,\
  read_fasta,\
  parse_time_stamp,\
  parse_matrix_position_hyphen

from forest import mrcas_to_forests, finalize


plt.switch_backend("Qt5Agg")


parser = argparse.ArgumentParser(
  description="Plot an animation for successive values of a tree from MRCA matrix."
)


parser.add_argument(
  "--samples",
  help="Path to CSV file of MRCA sample values."
)


parser.add_argument(
  "--times",
  help="Lineage sampling times."
)


parser.add_argument(
  "--t0",
  type=float,
  default=0.,
  help="Initial time."
)


def plot_tree(tree, d, ax) :
  rooted = finalize(tree, d)
  draw(
    rooted,
    label_func=lambda c : str(c.name) if c.name else "",
    do_show=False,
    axes=ax
  )


def plot_forest(forest, t0, ax) :
  for i, (t, tree) in enumerate(forest.values()) :
    d = t - t0
    plot_tree(tree, d, ax)
  

def plot(df, forests, t0) :
  f, ax  = plt.subplots()

  ax.set_title("k = 0")
  plot_forest(forests[0], t0, ax)

  def update(frame) :
    forest = forests[frame]
    ax.clear()
    plot_forest(forest, t0, ax)
    k = df["k"].iloc[frame]
    ax.set_title("k = {}".format(k))

  ani = animation.FuncAnimation(
    f,
    update,
    frames=len(forests)
  )

  return ani


args = parser.parse_args()


ts = read_fasta(args.times, lambda rec : parse_time_stamp(rec.id))
times = { i + 1 : t for i, t in enumerate(sorted(ts, reverse=True)) }

df_samples, samples = read_matrix_traj(
  args.samples,
  parse=parse_matrix_position_hyphen
)

forests = [ mrcas_to_forests(times, samples[k]) for k in range(samples.shape[0]) ]


ani = plot(df_samples, forests, args.t0)

plt.show()

#!/usr/bin/python3
# -*- coding: utf-8 -*-

from os.path import splitext
import glob
import re
import argparse

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import seaborn as sb

from diagnosis import multi_ess
import parse
import aspect as asp
import context
from read_trajs import \
  all_compartments, \
  compartment_label, \
  read_all, \
  read_concat_all


plt.switch_backend("Qt5Agg")


parser = argparse.ArgumentParser(
    description=
    "Plot 'S', 'E', 'I', and 'R' trajectories for many simulations."
)

parser.add_argument(
  "--concat",
  action="store_true",
  help="Indicate that the paths point to concatenated trajectory files."
)

parser.add_argument(
  "--glob-label",
  default="pattern_name",
  help="How to label patterns in the legend.",
)

parser.add_argument(
  "--glob-template",
  default="{}",
  help="template string to pass 'glob' elements through.",
)

parser.add_argument(
  "--glob",
  nargs=2,
  default=[],
  action="append",
  help="each element '(lab, s)' specifies a glob pattern "
       "'{glob_template}.format(s)' pointing to trajectory files.",
)

parser.add_argument(
  "--focus",
  default=[],
  action="append",
  help="each element is a focus trajectory. "
       "Plotted in black with style variations (solid, dashed, etc).",
)

parser.add_argument(
  "--burn",
  type=int,
  default=0,
  help="ignore trajectory files with 'k < {burn}'.",
)

parser.add_argument(
  "--multiples",
  type=int,
  nargs=2,
  default=[1, 0],
  help="with multiples '(mult, rem)', ignore trajectory files with "
       "'k mod mult != rem'.",
)

parser.add_argument(
  "--compartments",
  nargs="+",
  default=all_compartments,
  help="compartments for which to plot trajectories."
)

parser.add_argument(
  "--estim",
  action="store_true",
  help="plot 99 confidence intervals instead of low opacity lines "
       "for glob trajectories.",
)

parser.add_argument(
  "--range",
  action="append",
  nargs=3,
  default=[],
  metavar=("compartment", "lower", "upper"),
  help="set the plot range for the compartment."
)

parser.add_argument(
  "--alpha",
  type=float,
  default=0.1,
  help="opacity of the trajectories if non estim. Default 0.1",
)

parser.add_argument(
  "--colors",
  type=int,
  nargs=2,
  help="Number of colors to use in the glob palette and which to start from."
)

parser.add_argument(
  "--save",
  action="store_true",
  help="Save and load plot data as possible."
)

parser.add_argument(
  "--out",
  help="optional output filename (template). Interactive plot if not given.",
)

parser.add_argument(
  "--no-titles",
  action="store_true",
  help="Don't plot axes titles."
)

parse.add_argument_context(parser)

parse.add_argument_latex(parser)

parse.add_argument_width(parser)

parse.add_argument_decoration_width(parser)

parse.add_argument_decoration_height(parser)

parse.add_argument_aspect(parser)

parse.add_argument_dpi(parser)

parse.add_argument_legend_bbox(parser)

parser.add_argument(
  "--plot-globs",
  choices=["columns", "superpose", "rows"],
  default="columns",
  help="How to plot glob trajectories. On columns, rows, or superposed.",
)

parser.add_argument(
  "--split",
  action="store_true",
  help="plot each compartment and glob on a different figure. "
       "Each figure is saved to '{out}.format(glob_name, compartment)'.",
)


args = parser.parse_args()

# def glob_label(pattern_name) :
#     try :
#       int(pattern_name)
#       float(pattern_name)
#     except ValueError :
#       return pattern_name
#     else :
#       return "glob_{}".format(pattern_name)


patterns = [ (lab, args.glob_template.format(pn))
             for (lab, pn) in args.glob ]
if len(args.glob) == 0 :
  pattern_names = [ "" ]  # no title
else :
  # Careful : pattern names should not be numbers
  pattern_names = [ pn for pn, _ in patterns ]



focuses = args.focus

burn = args.burn

multiples, remainder = args.multiples


context.set(args.context)

if args.latex :
  context.set_latex()


compartments = args.compartments

if args.plot_globs == "rows" :
  assert(len(compartments) == 1)


print("Compartments:", compartments)
print("Pattern names:", pattern_names)


if args.save and args.out :
  try :
    fname_save = splitext(args.out)[0] + ".csv"
    df = pd.read_csv(fname_save)
  except (TypeError, FileNotFoundError, AttributeError) :
    if args.concat :
      df = read_concat_all(
        concat_paths=patterns,
        focuses=focuses,
        concat_label=args.glob_label,
        compartments=compartments,
        burn=burn,
        multiples=multiples,
        remainder=remainder
      )
    else :
      df = read_all(
        patterns=patterns,
        focuses=focuses,
        pattern_label=args.glob_label,
        compartments=compartments,
        burn=burn,
        multiples=multiples,
        remainder=remainder
      )
    df.to_csv(
      splitext(args.out)[0] + ".csv",
      index=False
    )  
else :
  if args.concat :
    df = read_concat_all(
      concat_paths=patterns,
      focuses=focuses,
      concat_label=args.glob_label,
      compartments=compartments,
      burn=burn,
      multiples=multiples,
      remainder=remainder
    )
  else :
    df = read_all(
      patterns=patterns,
      focuses=focuses,
      pattern_label=args.glob_label,
      compartments=compartments,
      burn=burn,
      multiples=multiples,
      remainder=remainder
    )


focus_palette = sb.color_palette(["black"])

if args.colors :
  n_colors, start_color = args.colors
else :
  n_colors = len(pattern_names)
  start_color = 0

pal = sb.color_palette(n_colors=n_colors)
glob_palette = { pn : pal[i + start_color] for i, pn in enumerate(pattern_names) }


ranges = { comp : (float(lower), float(upper))
           for (comp, lower, upper) in args.range
           if comp in args.compartments }


def plot_patterns(df, estim, plot_globs, split, alpha,
                  pattern_label, width=None, aspect=None,
                  decoration_width=None, decoration_height=None) :
  if width :
    if plot_globs in ["rows", "superpose"] :
      ncol = 1
    elif plot_globs == "columns" :
      ncol = len(pattern_names)
    else :
      raise ValueError("invalid plot_globs value")

    height = asp.facet_height(
      ncol=ncol,
      width=asp.pts_to_inches(width),
      aspect=aspect,
      decoration_width=decoration_width,
      decoration_height=decoration_height,
    )
  else :
    height = 2.5

  kw = {
    "kind" : "line",
    "x" : "t",
    "y" : "count",
    "aspect" : aspect,
    "height" : height,
  }
    
  if estim :
    kw_estim = {
      "ci" : 99.,
    }
  else :  # default
    kw_estim = {
      "units" : "k",
      "estimator" : None,
      "alpha" : alpha,
    }

  kw_full = {
    "hue" : pattern_label,
    "hue_order" : pattern_names,
    "palette" : glob_palette,
    "facet_kws" : {
      "sharey":"row",
      "legend_out":True,
    },
  }

  kw_leg = {
    "legend" : False
  }

  if plot_globs == "columns" :
    kw_plot_globs = {
      "row" : "Compartment",
      "col" : pattern_label,
      "col_order" : pattern_names
    }
  elif plot_globs == "superpose" :
    kw_plot_globs = {
      "row" : "Compartment"
    }
  elif plot_globs == "rows" :
    kw_plot_globs = {
      "row" : pattern_label,
      "row_order" : pattern_names,
    }
  else :
    raise ValueError("invalid plot_globs value")

  def plot_full() :
    f = sb.relplot(
      data=df,
      **kw,
      **kw_estim,
      **kw_full,
      **kw_leg,
      **kw_plot_globs,
    )

    return f

  def plot_split(pn, comp) :
    color = glob_palette[pn]
    data = df[(df[pattern_label] == pn)&(df["compartment"]==comp)]
    f = sb.relplot(
      data=data,
      color=color,
      **kw,
      **kw_estim,
    )
    f.axes[0,0].set_title(compartment_label(comp))
    return f

  if split :
    return {
      pn : {
        comp : plot_split(pn, comp)
        for comp in compartments
      }
      for pn in pattern_names
    }
  else :
    return plot_full()


def plot(df, estim, plot_globs, split, alpha, pattern_label,
         ranges, latex=False, width=None, aspect=None, legend_bbox=None,
         decoration_width=None, decoration_height=None) :

  def plot_focuses(x, y, data, **kw) :
    # we just need to find what compartment we're on
    if plot_globs in ["columns", "superpose"] :
      comps = data["Compartment"].unique()
      assert (len(comps) == 1)
      comp = comps[0]
      data = df[(df["Compartment"] == comp) & df["focus"]]
    elif plot_globs == "rows" :
      # only one compartment
      data = df[df["focus"]]
    else :
      raise ValueError("invalid plot_globs value")

    ax = plt.gca()
    cp = data["compartment"].iloc[0]

    sb.lineplot(
      x=x,
      y=y,
      estimator=None,
      style="fname",
      legend=False,
      # We cheat to try to control the linewidth
      size="fname",
      sizes=(0.5, 0.5),
      data=data,
      ax=ax,
      color="black",
    )

  def set_limits_labels(f, ranges) :
    def set_limits(cp, ax) :
      try :
        lower, upper = ranges[cp]
        ax.set_ylim(lower, upper)
      except KeyError :
        pass
    
    def set_label(cp, ax) :
      if latex :
        lab = "${}$".format(cp.upper())
      else :
        lab = cp.upper()
      ax.set_ylabel(lab)
     
    if plot_globs in ["rows", "superpose"] :
      for ax in f.axes[:, 0] :
        # only one compartment
        cp = compartments[0]
        set_limits(cp, ax)
        set_label(cp, ax)
    elif plot_globs == "columns" :
      for ax, cp in zip(f.axes[:, 0], compartments) :
        set_limits(cp, ax)
        set_label(cp, ax)
    else :
      raise ValueError("invalid plot_globs value")

    return f

  def plot_focuses_and_improve(f) :
    if plot_globs in ["columns", "superpose"] :
      if legend_bbox is not None :
        f.add_legend(
          bbox_to_anchor=legend_bbox,
          loc="upper left"
        )
      else :
        f.add_legend()
    elif plot_globs == "rows" :
      pass
    else :
      raise ValueError("invalid plot_globs value")

    # if there are focus trajectories
    if not df[df["focus"]].empty :
      f = f.map_dataframe(plot_focuses, "t", "count")

    if plot_globs == "columns" :
      f = f.set_titles(
        col_template="{col_name}",
        template=""
      )
    elif plot_globs in ["superpose", "rows"] :
      f = f.set_titles(
        row_template="{row_name}",
        template="{row_name}",
      )
    else :
      raise ValueError("invalid plot_globs value")
      
    f = set_limits_labels(f, ranges=ranges)
    return f

  if split :
    fs = plot_patterns(
      df,
      estim=estim,
      plot_globs=plot_globs,
      split=split,
      alpha=alpha,
      pattern_label=pattern_label,
      width=width,
      aspect=aspect,
      decoration_width=decoration_width,
      decoration_height=decoration_height,
    )
    return {
      pn : {
        comp : plot_focuses_and_improve(f)
        for comp, f in figs.items()
      }
      for pn, figs in fs.items()
    }
  else :  
    f = plot_patterns(
      df,
      estim=estim,
      plot_globs=plot_globs,
      split=split,
      alpha=alpha,
      pattern_label=pattern_label,
      width=width,
      aspect=aspect,
      decoration_width=decoration_width,
      decoration_height=decoration_height,
    )

    f = plot_focuses_and_improve(f)
    return f

  return f


f = plot(
  df,
  estim=args.estim,
  plot_globs=args.plot_globs,
  split=args.split,
  alpha=args.alpha,
  pattern_label=args.glob_label,
  ranges=ranges,
  latex=args.latex,
  width=args.width,
  aspect=args.aspect,
  legend_bbox=args.legend_bbox,
  decoration_width=args.decoration_width,
  decoration_height=args.decoration_height,
)


def save(f, out, dpi=600) :
  f.fig.tight_layout(
    pad=0.3
  )
  print("Size:", f.fig.get_size_inches())
  f.savefig(out, dpi=dpi)


if args.out :
  if not args.split :
    save(
      f,
      args.out,
      dpi=args.dpi
    )
  else :
    for pn, d in f.items() :
      for comp, fig in d.items() :
        save(
          fig,
          args.out.format("{}.{}".format(pn, comp)),
          dpi=args.dpi
        )
else :
  plt.show()

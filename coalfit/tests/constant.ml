module F = Util.Float

module CC = Coalfit.Constant


let run path =
  let seed = 1234 in
  let theta_every = 1 in
  let hypar = (new CC.Param.hyper)#with_tf 3. in
  let infer = CC.Infer.default in
  let tseqs = [
    (1., Seqs.Io.of_string "ATCGAACG") ;
    (1.5, Seqs.Io.of_string "ATGGAACG") ;
    (2., Seqs.Io.of_string "ATCGATCG") ;
    (2.5, Seqs.Io.of_string "ATGGAACG") ;
  ]
  in
  let theta = (new CC.Param.theta) in
  ignore (CC.Estimate.posterior
    ~seed
    ~theta_every
    ~path
    ~n_iter:4
    hypar
    infer
    tseqs
    theta
  )


(* Test default proposal weights *)
let%expect_test _ =
  let path = "/tmp/ocamlecoevo.coalfit.constant.test" in
  run path ;
  [%expect] ;
  let s = BatFile.with_file_in
    ~perm:BatFile.user_read
    (path ^ ".theta.csv")
    BatIO.read_all
  in
  Printf.printf "%s" s ;
  [%expect{|
    k,outcome,logprior,loglik,logu,p'p,qq',l'l,logv,mu,lambda,move_mu,move_lambda
    0,accept,-15.0702511551,-40.1729734116,0.,inf,inf,inf,inf,1.,1.,1.,1.
    0,accept,-15.0700899183,-40.1725652372,0.,0.000161236861997,0.,0.000408174324278,0.000569411186275,1.,1.,1.,1.
    1,accept,-15.0698846001,-40.1708700355,0.,0.000205318211039,0.,0.00169520179895,0.00190052000999,1.,1.,1.,1.
    2,accept,-15.0689755285,-40.1685671337,0.,0.000909071608714,0.,0.00230290173781,0.00321197334652,1.,1.,1.,1.
    3,accept,-15.0689970126,-40.1685671337,-1.55731000127,-2.14841607313e-05,0.,0.,-2.14841607313e-05,1.,1.,1.,1. |}]

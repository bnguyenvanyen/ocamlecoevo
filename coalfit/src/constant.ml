open Cmdliner
module CC = Coalfit.Constant
module CB = Coalfit.Boosted

let () =
  Printexc.record_backtrace true ;
  Term.exit @@ Term.eval_choice
    (CC.Term.run_unit, CC.Term.info)
    [
      (CC.Term.run_unit, CC.Term.info_sub) ;
      (CB.Term.run_unit, CB.Term.info_sub) ;
    ]

module Kc = Tree.Kendall_colijn.Make (Tree.Lt)


let string_sub_max s k len =
  match String.sub s k len with
  | sub ->
      sub
  | exception Invalid_argument _ ->
      (* s too short to be cut *)
      s


let read_one_forest chan first =
  let k = Scanf.sscanf first ">%i" (fun k -> k) in
  let rec read_trees () =
    match input_line chan with
    | line ->
        if string_sub_max line 0 5 = "#tree" then begin
          let newick = input_line chan in
          match Tree.Lt.of_newick newick with
          | Some tree ->
              (* consume '\n' line *)
              ignore (input_line chan) ;
              let trees, next = read_trees () in
              tree :: trees, next
          | None ->
              failwith "invalid newick string"
        end else begin
          if string_sub_max line 0 1 = ">" then
            [], Some line
          else
            failwith "invalid format"
        end
    | exception End_of_file ->
        [], None
  in
  let forest, next = read_trees () in
  k, forest, next


let read_tree_on_line ?(line=2) chan =
  for _ = 1 to line - 1 do
    ignore (input_line chan)
  done
  ;
  let newick = input_line chan in
  Tree.Lt.of_newick newick


let read_timed_forests chan =
  (* Here a "forest" looks like
     >%i
     #tree ...
     tree

     #tree ...
     tree
     ...
     
     The problem is that there is no end mark,
     so we need to backtrack, when we reach the next forest.
   *)
  let rec f first_o =
    let first =
      match first_o with
      | None ->
          input_line chan
      | Some first ->
          first
    in
    match read_one_forest chan first with
    | (k, forest, Some next) ->
        (k, forest) :: (f (Some next))
    | (k, forest, None) ->
        (* reached the end of the file *)
        [(k, forest)]
  in f None


let relabel tree =
  let lv = List.map (fun (_, tk) -> tk) (Tree.Lt.leaves tree) in
  let sorted = List.sort (fun i i' -> compare i' i) lv in
  Tree.Lt.map_case
    ~leaf:(fun (t, k) ->
      let i, _ = BatList.findi (fun _ (_, k') -> k = k') sorted in
      (t, i + 1)
    )
    ~node:(fun x -> x)
    ~binode:(fun x -> x)
    tree


let vec_columns n =
  let n_mrcas = 2 * n - 2 in
  let mrca_cols =
    List.init n_mrcas (fun k -> Printf.sprintf "mrca_%i" k)
  in
  let extant_cols =
    List.init n (fun k -> Printf.sprintf "extant_%i" k)
  in
  mrca_cols @ extant_cols


let extract_vec n vec c =
  begin match Scanf.sscanf c "mrca_%i" (fun j -> j) with
  | j ->
      Option.fold
        ~none:""
        ~some:(fun vec -> string_of_float (List.nth vec j))
        vec
  | exception Scanf.Scan_failure _ ->
      begin match Scanf.sscanf c "extant_%i" (fun j -> j) with
      | j ->
          Option.fold
            ~none:""
            ~some:(fun vec ->
               string_of_float (List.nth vec (j + 2 * n - 2))
             )
            vec
      | exception Scanf.Scan_failure _ ->
          invalid_arg (Printf.sprintf "unrecognized column : %s" c)
      end
  end
  

let output ~repr path n kdvs =
  let columns =
    let cs = ["k" ; "distance"] in
    if repr then
      cs @ vec_columns n
    else
      cs
  in
  let extract (k, d, vec) =
    function
    | "k" ->
        string_of_int k
    | "distance" ->
        string_of_float d
    | c ->
        extract_vec n vec c   
  in
  Util.Csv.output ~columns ~extract ~path kdvs


let output_target_vec path n v =
  let columns = vec_columns n in
  let extract vec = extract_vec n vec in
  Util.Csv.output ~columns ~extract ~path v


let run repr path_target target_line path_samples out_target out =
  let tree =
    let c = open_in path_target in
    match read_tree_on_line ?line:target_line c with
    | Some tree ->
        close_in c ;
        relabel tree
    | None ->
        failwith "could not read newick input"
  in
  let kforests =
    let c = open_in path_samples in
    let kforests = read_timed_forests c in
    close_in c ;
    kforests
  in
  let kdvs = List.map (fun (k, fr) ->
    match fr with
    | tree' :: [] ->
        let vo =
          if repr then
            let _, v = Kc.characterize 0.5 tree tree' in
            Some v
          else
            None
        in
        (k, Kc.distance 0.5 tree tree', vo)
    | _ ->
        (k, Float.nan, None)
  ) kforests
  in
  let n = List.length (Tree.Lt.leaves tree) in
  output ~repr out n kdvs ;
  Option.iter (fun out_target ->
    let v, _ = Kc.characterize 0.5 tree tree in
    output_target_vec out_target n [Some v]
  ) out_target



module Term =
  struct
    module C = Cmdliner

    let path_target =
      let doc =
        "Path to 'tnewick' file containing target tree."
      in
      C.Arg.(
        required
        & opt (some string) None
        & info ["path-target"] ~docv:"PATH-TARGET" ~doc
      )

    let path_samples =
      let doc =
        "Path to 'tnewick' file containing samples to score."
      in
      C.Arg.(
        required
        & opt (some string) None
        & info ["path-samples"] ~docv:"PATH-SAMPLES" ~doc
      )

    let target_line =
      let doc =
        "Line to read target tree from. By default 2."
      in
      C.Arg.(
        value
        & opt (some int) None
        & info ["target-line"] ~docv:"TARGET-LINE" ~doc
      )

    let repr =
      let doc =
        "Whether to output the Kendall-Colijn representation too."
      in
      C.Arg.(
        value
        & flag
        & info ["repr"] ~docv:"REPR" ~doc
      )

    let out_target =
      let doc =
        "Optional path for Kendall-Colijn representation of the target."
      in
      C.Arg.(
        value
        & opt (some string) None
        & info ["out-target"] ~docv:"OUT-TARGET" ~doc
      )

    let out =
      let doc =
        "Path to CSV file to output to."
      in
      C.Arg.(
        required
        & pos 0 (some string) None
        & info [] ~docv:"OUT" ~doc
      )

    let run =
      C.Term.(
        const run
        $ repr
        $ path_target
        $ target_line
        $ path_samples
        $ out_target
        $ out
      )

    let info =
      let doc =
        "Plot the trace of the Kendall-Colijn distance to some target tree."
      in
      C.Term.info
        "phylo-trace-kc"
        ~version:"%%VERSION%%"
        ~doc
        ~exits:C.Term.default_exits
        ~man:[]
  end


let () =
  Printexc.record_backtrace true ;
  Cmdliner.Term.exit @@ Cmdliner.Term.eval (Term.run, Term.info)

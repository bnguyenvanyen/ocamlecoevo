open Cmdliner
module CC = Coalfit.Boosted

let () =
  Printexc.record_backtrace true ;
  Util.Gc.tune () ;
  Term.exit @@ Term.eval (CC.Term.run, CC.Term.info)

open Abbrevs


type (_, _) elist =
  El : ('tag, 'theta, _, _) Fit.Param.List.t -> ('tag, 'theta) elist


module Data =
  struct
    module type S =
      sig
        type t
        type trait

        (** [read txseqs] is a dataset built by reading
            the list of sampling times, trait values
            and corresponding sequences in [txseqs]. *)
        val read : (float * trait * Seqs.t) list -> t

        val term : t Cmdliner.Term.t

        (** [length data] is the number of samples in data. *)
        val length : t -> int

        (** [sorted data] are the samples ordered backward in time *)
        val sorted : t -> (int * U._float * trait) list

        (** [sequences data] are the indexed sequences
            with indices corresponding to sorted samples,
            without the invariant sites. *)
        val sequences : t -> Seqs.t Intmap.t

        (** [invariants data] are the number of invariants sites
            for each nucleotide. *)
        val invariants : t -> int Seqsim.Tree.Ntm.t
      end

    module Make (Trait : Util.Interfaces.REPRABLE) :
      (S with type trait = Trait.t) =
      struct
        type t = {
          length : int ;
          samples : (int * U._float * Trait.t) list ;
          sequences : Seqs.t Intmap.t ;
          invariants : int Seqsim.Tree.Ntm.t ;
        }
        
        type trait = Trait.t

        let read txseqs =
          let length = L.length txseqs in
          let sorted = L.sort (fun (t,_,_) (t',_,_) -> compare t' t) txseqs in
          let samples = L.mapi (fun k (t, x, _) ->
              (k + 1, F.of_float t, x)
            ) sorted
          in
          let sequences = L.fold_lefti (fun seqs i (_, _, seq) ->
            (* start from 1 *)
            let k = i + 1 in
            Intmap.add k seq seqs
          ) Intmap.empty sorted
          in
          let invariants, sequences =
            Seqsim.Base.split_invariant sequences
          in
          { length ; samples ; sequences ; invariants }

        let term =
          let doc =
            "Path to the file of sampling times, trait values and sequences."
          in
          let fname = Cmdliner.Arg.(
            required
            & opt (some string) None
            & info ["data"] ~docv:"DATA" ~doc
          )
          in
          let of_string s =
            match Scanf.sscanf s "%f:%s" (fun t s -> (t, s)) with
            | (t, s) ->
                s
                |> Trait.of_string
                |> U.Option.map (fun x -> (t, x))
            | exception Scanf.Scan_failure _ ->
                None
          in
          let read_from fname =
            let chan = open_in fname in
            let txseqs =
              match Seqs.Io.assoc_of_any_fasta of_string chan with
              | None ->
                  invalid_arg "Badly formatted headers in fasta file"
              | Some txs ->
                  L.map (fun ((t, x), seq) -> (t, x, seq)) txs
            in
            close_in chan ;
            read txseqs
          in
          Cmdliner.Term.(const read_from $ fname)

        let length { length ; _ } =
          length

        let sorted { samples ; _ } =
          samples

        let sequences { sequences ; _ } =
          sequences

        let invariants { invariants ; _ } =
          invariants
      end
  end


module type THETA =
  sig
    type t

    val default : t

    val extract : t -> string -> string

    (** [(eval term) capture] reads a [t] value,
        and optionally captures the values read to the file
        at [capture]. *)
    val term : (string option -> t) Cmdliner.Term.t
  end


module type HYPER =
  sig
    type t

    val with_prm_regraft_rng :
      t ->
      U.rng ->
        t

    val prm_mutations_drift_coeff :
      t ->
        _ U.pos

    val prm_mutations_volatility :
      t ->
        _ U.pos

    val prm_merge_jump :
      t ->
        _ U.pos

    (** [(eval term) capture] reads a [t] value,
        and optionally captures the values read to the file
        at [capture]. *)
    val term : (string option -> t) Cmdliner.Term.t
  end


module type ECO =
  sig
    type theta
    type hyper
    type trait
    
    module Data : (Data.S with type trait = trait)

    type data = Data.t

    module Forward :
      sig
        type t

        val sim :
          hyper ->
          theta ->
            t

        val output :
          U.Csv.out_channel ->
          int ->
          t ->
            unit
      end

    module Coal :
      sig
        (* TODO I would prefer to not have to put those modules in *)
        type color
        type point
        type prm
        type tree
        type t

        val prm : theta -> prm

        val with_prm : theta -> prm -> theta

        val forest : t -> (Intset.t * trait * tree) list

        val rand_prm :
          rng:U.rng ->
          samples:(int * _ U.anyfloat * trait) list ->
          hyper ->
          theta ->
          Forward.t ->
            prm

        val logd_prm : prm -> float

        (** [rand_merge_color ~rng nm1] where [nm1] is
         *  the number of lineages minus 1 *)
        val rand_merge_color :
          rng:U.rng ->
          U.anyposint ->
            color

        val rand_mutation_color :
          rng:U.rng ->
          U.anyposint ->
            color

        val update_merge_point :
          color ->
          float ->
          prm ->
            prm 

        val update_merge_points :
          int ->
          int ->
          float ->
          prm ->
            prm

        (* here I would like to put (_ U.anypos -> _ U.pos),
         * but it doesn't work
         * problems of subtyping and variance etc... *)
        val update_mutation_points :
          color ->
          (U.closed_pos -> U.closed_pos) ->
          prm ->
            prm

        val sim :
          data ->
          hyper ->
          theta ->
          Forward.t ->
            t * theta

        val output :
          data ->
          U.Csv.out_channel ->
          int ->
          t ->
            unit

        val output_prm :
          U.Csv.out_channel ->
          int ->
          prm ->
            unit
      end
  end


module type EVO =
  sig
    type theta
    type trait
    type tree

    val loglik :
      (Seqs.t Intmap.t) ->
      (int Seqsim.Tree.Ntm.t) ->
      theta ->
      (Intset.t * trait * tree) list ->
        float
  end


module type INFER =
  sig
    type tag
    type theta
    type hyper
    type prm
    type t

    val prm : t -> Fit.Infer.t

    (* need to ask this since type tag is given by the user,
     * it seems I can't unify constraints with
     * type 'a tag = [> `Prm_mutations | `Prm ] *)

    (** the tag for the prm_mutations proposals param,
     *  typically [`Prm_mutations] *)
    val prm_mutations_tag : tag

    (** the tag for the prm prior and adaptive proposal param,
     *  typically [`Prm] *)
    val prm_tag : tag

    (** whether to propose prm mutations or not
     *  (should be false only if there are no mutations) *)
    val propose_prm_mutations : bool

    val tag_to_columns : tag -> string list

    val params :
      hyper ->
      t ->
        (tag, theta) elist

    val term : t Cmdliner.Term.t
  end


module Make
  (Theta : THETA)
  (Hyper : HYPER)
  (Eco : ECO with type theta = Theta.t
              and type hyper = Hyper.t)
  (Evo : EVO with type theta = Theta.t
              and type trait = Eco.trait
              and type tree = Eco.Coal.tree)
  (Infer : INFER with type theta = Theta.t
                  and type hyper = Hyper.t
                  and type prm = Eco.Coal.prm) =
  struct
    module Data = Eco.Data

    (* the user should not have to worry about internal prm :
     * we can fill in the stuff for him ? *)
    module Prior =
      struct
        let prm =
          let draw _ =
            (* I would need more context to draw: th and traj *)
            failwith "Not_implemented"
          in
          let log_dens_proba nu =
            [Eco.Coal.logd_prm nu]
          in Fit.Dist.Base { draw ; log_dens_proba }
      end

    module Propose =
      struct
        (* OU process transformed by exp *)
        (* FIXME can return a nan ? *)
        let rand_linear ~rng hy =
          (* I write it by hand here because I'm too lazy to do something
           * cleaner *)
          let alpha = Hyper.prm_mutations_drift_coeff hy in
          let sigma = Hyper.prm_mutations_volatility hy in
          let until t x t' =
            let dt = F.Pos.of_anyfloat F.Op.(t' - t) in
            let v = F.Pos.sqrt dt in
            let dbt = F.of_float (U.rand_normal ~rng 0. v) in
            F.Op.(~- alpha * x * dt + sigma * dbt)
          in
          let x_r = ref F.zero in
          let t_r = ref F.zero in
          let f t' =
            let x' = until !t_r !x_r t' in
            x_r := x' ;
            t_r := t' ;
            let y = F.(Pos.Op.(t' * exp x')) in
            assert (not (F.is_nan y)) ;
            y
          in f

        (* choose a random merge point and update it *)
        let update_rand_merge_point ~rng nm1 v nu =
          let dx = U.rand_normal ~rng 0. v in
          let color = Eco.Coal.rand_merge_color ~rng nm1 in
          Eco.Coal.update_merge_point color dx nu

        let update_rand_mutation_points ~rng nm1 hy nu =
          let f = rand_linear ~rng hy in
          let color = Eco.Coal.rand_mutation_color ~rng nm1 in
          Eco.Coal.update_mutation_points color f nu

        let prm_mutations data hy =
          let nm1 = I.Pos.of_int (Data.length data - 1) in
          let draw rng nu =
            update_rand_mutation_points ~rng nm1 hy nu
          in
          let log_pd_ratio _ _ =
            (* FIXME not too convinced about this either :
             * depends on rand_linear *)
            [0.]
          in
          let move_to_sample nu =
            nu
          in
          if Infer.propose_prm_mutations then
            Fit.Propose.Base { draw ; log_pd_ratio ; move_to_sample }
          else
            Fit.Propose.Constant_flat

        let prm_merges data hy =
          let nm1 = I.Pos.of_int (Data.length data - 1) in
          let draw rng nu =
            update_rand_merge_point ~rng nm1 (Hyper.prm_merge_jump hy) nu
          in
          let log_pd_ratio _ _ =
            (* Here I think that is true *)
            [0.]
          in
          let move_to_sample nu =
            nu
          in
          Fit.Propose.Base { draw ; log_pd_ratio ; move_to_sample }
      end

    module Jumps =
      struct
        let prm_merge_pairs n j =
          (* for a given lineage *)
          let jump k =
            let set dx nu =
              (* for all merge events on that lineage, add dx *)
              (* k' goes from 1 to n, except k *)
              U.int_fold_right ~f:(fun k' nu ->
                if k = k' then
                  nu
                else
                  Eco.Coal.update_merge_points k k' dx nu
              ) ~n nu
            in
            (* FIXME *)
            let diff _ _ =
              failwith "Not_implemented"
            in
            Fit.Param.SJ.{ set ; diff ; jump = F.to_float j }
          in
          U.int_fold_right ~f:(fun k jumps ->
            (jump k) :: jumps
          ) ~n []
      end

    (* Additional prm 'params' for inference *)
    module Infer =
      struct
        (* custom-only for mutations *)
        let prm_mutations data hy =
          Fit.Infer.only_custom
            ~tag:Infer.prm_mutations_tag
            ~get:Eco.Coal.prm
            ~set:Eco.Coal.with_prm
            ~prior:Fit.Dist.Flat
            ~proposal:(Propose.prm_mutations data hy)
            Fit.Infer.custom

        (* for the rest : the full prior, plus proposals for the merge *)
        let prm_merges data hy =
          let jumps =
            Jumps.prm_merge_pairs (Data.length data) (Hyper.prm_merge_jump hy)
          in
          Fit.Infer.maybe_adaptive
            ~tag:Infer.prm_tag
            ~get:Eco.Coal.prm
            ~set:Eco.Coal.with_prm
            ~prior:Prior.prm
            ~proposal:(Propose.prm_merges data hy)
            ~jumps

        let tag_to_columns = Infer.tag_to_columns

        let params data hy infer =
          let El pars = Infer.params hy infer in
          El (prm_mutations data hy
           :: prm_merges data hy (Infer.prm infer)
           :: pars
          )

        let term = Infer.term
      end

    (* compute the likelihood *)
    module Likelihood =
      struct
        let compute hy data =
          let loglik = Evo.loglik
            (Data.sequences data)
            (Data.invariants data)
          in
          let draw _ =
            failwith "Not_implemented"
          in
          let log_dens_proba th =
            let traj = Eco.Forward.sim hy th in
            let zf, _th' = Eco.Coal.sim data hy th traj in
            (* TODO eco_loglik *)
            [loglik th (Eco.Coal.forest zf)]
          in Fit.Dist.Base { draw ; log_dens_proba }
      end

    (* put it all together *)
    module Estimate =
      struct
        let output_theta pars chan =
          (* so `Prm is in here, is that a problem ? *)
          let tags = Fit.Param.List.infer_tags pars in
          let th_cols = L.fold_left (fun cs tag ->
            cs @ (Infer.tag_to_columns tag)
          ) [] tags
          in
          let move_cols = L.map (fun c -> "move_" ^ c) th_cols in
          let columns =
              Fit.Csv.columns_sample_move
            @ th_cols
            @ move_cols
          in
          (* here we should not extract prm stuff -- is that ok ? *)
          let extract = Fit.Csv.extract_sample_move
            ~move_to_sample:(fun th -> th)
            ~extract:Theta.extract
          in
          (* output header *)
          Csv.output_record chan columns ;
          (fun k res ->
            (* also k ? *)
            let row = U.Csv.Row.unfold ~columns (extract k) res in
            U.Csv.Row.output ~chan row
          )

        let output
          ?theta_every ?forward_every ?coal_every ?prm_every ?chol_every
          ?(n_burn=0) data hy chol pars path =
          (* FIXME but no need to open the channels
           * if we actually don't output to them *)
          let out_theta, close_theta =
            match theta_every with
            | None ->
                (fun _ _ -> ()),
                (fun () -> ())
            | Some _ ->
                let chan_theta = U.Csv.open_out (path ^ ".theta.csv") in
                output_theta pars chan_theta,
                (fun () -> U.Csv.close_out chan_theta)
          in
          let out_forward, out_forward_move, close_forward =
            match forward_every with
            | None ->
                (fun _ _ -> ()),
                (fun _ _ -> ()),
                (fun () -> ())
            | Some _ ->
                let chan_fwd = U.Csv.open_out (path ^ ".forward.csv") in
                let chan_fwd_mv = U.Csv.open_out (path ^ ".forward.move.csv") in
                Eco.Forward.output chan_fwd,
                Eco.Forward.output chan_fwd_mv,
                (fun () ->
                  U.Csv.close_out chan_fwd ;
                  U.Csv.close_out chan_fwd_mv
                )
          in
          (* TODO not necessarily a CSV file ? *)
          let out_coal, out_coal_move, close_coal =
            match coal_every with
            | None ->
                (fun _ _ -> ()),
                (fun _ _ -> ()),
                (fun () -> ())
            | Some _ ->
                let chan_coal = U.Csv.open_out (path ^ ".coal.csv") in
                let chan_coal_mv = U.Csv.open_out (path ^ ".coal.move.csv") in
                Eco.Coal.output data chan_coal,
                Eco.Coal.output data chan_coal_mv,
                (fun () ->
                  U.Csv.close_out chan_coal ;
                  U.Csv.close_out chan_coal_mv
                )
          in
          let out_prm, close_prm =
            match prm_every with
            | None ->
                (fun _ _ -> ()),
                (fun () -> ())
            | Some _ ->
                let chan_prm = U.Csv.open_out (path ^ ".prm.csv") in
                Eco.Coal.output_prm chan_prm,
                (fun () -> U.Csv.close_out chan_prm)
          in
          let out_chol = Simfit.Out.convert_chol ~n_burn ?chol_every path in
          (* combine theta + forward + coal + prm
           * by hand to be more explicit *)
          (* if every is not given, then do not output at all *)
          let is_multiple k =
            function
            | None ->
                false
            | Some n ->
                (k mod n = 0)
          in
          let do_sim_forward k =
            match forward_every, coal_every with
            | None, None ->
                false
            | Some n, None
            | None, Some n ->
                (k mod n = 0)
            | Some n, Some m ->
                (k mod n = 0) || (k mod m = 0)
          in
          let output k res =
            let th = Fit.Mh.Return.sample res in
            let mv = Fit.Mh.Return.move res in
            if is_multiple k theta_every then
              out_theta k res
            ;
            if is_multiple k prm_every then
                out_prm k (Eco.Coal.prm th)
            ;
            if do_sim_forward k then begin
              let traj = Eco.Forward.sim hy th in
              let traj_mv = Eco.Forward.sim hy mv in
              if is_multiple k forward_every then begin
                out_forward k traj ;
                out_forward_move k traj_mv
              end ;
              if is_multiple k coal_every then begin
                let zf, _ = Eco.Coal.sim data hy th traj in
                let zf_mv, _ = Eco.Coal.sim data hy mv traj_mv in
                out_coal k zf ;
                out_coal_move k zf_mv
              end
            end
          in
          let start k0 res0 =
            output k0 res0
          in
          let out k res =
            if k > n_burn then output k res
          in
          let return kf resf =
            if kf > n_burn then output kf resf ;
            close_theta () ;
            close_forward () ;
            close_coal () ;
            close_prm () ;
            (kf, resf)
          in
          let out_res = U.Out.{ start ; out ; return } in
          let out_comb = U.Out.combine_tuple (fun (k,res) (_,chol) ->
              (k,res,chol)
            ) out_res out_chol
          in
          (* I actually don't have chol passed,
           * so I need to get it from outside
           * (it's mutable so that's easy) *)
          Fit.Csv.map
            ~return:(fun k res -> (k, res, chol))
            (fun res -> (res, chol))
            out_comb


        (* TODO add steps *)
        let posterior
          ?seed ?theta_every ?forward_every ?coal_every ?prm_every ?chol_every 
          ~path ~n_iter hypar infer data theta =
          (* set a rng in hypar *)
          let hypar = Hyper.with_prm_regraft_rng hypar (U.rng seed) in
          (* existential *)
          let El pars = Infer.params data hypar infer in
          let prior = Fit.Param.List.prior Theta.default pars in
          let proposal = Fit.Param.List.custom_proposal pars in
          let likelihood = Likelihood.compute hypar data in
          let draws = A.of_list (Fit.Param.List.adapt_draws pars) in
          let ndraws = A.length draws in
          let vdraw = Fit.Propose.vector_draw draws in
          let draws_ratio = Fit.Param.List.log_pd_ratio pars in
          let chol =
            pars
            |> Fit.Param.List.adapt_jumps
            |> A.of_list
            |> Lac.Vec.of_array
            |> Lac.Mat.of_diag
          in
          let output =
            output
              ?theta_every
              ?forward_every
              ?coal_every
              ?prm_every
              ?chol_every
              data
              hypar
              chol
              pars
              path
          in
          let nu =
            let rng = U.rng seed in
            let samples = Data.sorted data in
            let traj = Eco.Forward.sim hypar theta in
            Eco.Coal.rand_prm
              ~rng
              ~samples
              hypar
              theta
              traj
          in
          (* this does need to change though *)
          let theta' = Eco.Coal.with_prm theta nu in
          let ram_proposal = Fit.Ram.Also.proposal
            ?draws_ratio
            (fun x -> x)
            ndraws
            chol
            prior
            vdraw
            proposal
            likelihood
          in
          let res0 = Fit.Mh.Return.accepted
            (fun x -> x)
            (Fit.Mh.Sample.weight prior likelihood theta')
          in
          let _, ret, _ = Fit.Markov_chain.simulate
            ~output
            ?seed
            ram_proposal
            res0
            n_iter
          in
          Fit.Mh.Return.sample ret
      end

    module Term =
      struct
        let estimate_posterior =
          let flat
            seed theta_every forward_every coal_every prm_every chol_every
            path n_iter read_hypar infer data read_theta =
            let capt_hy = Some (Printf.sprintf "%s.par.csv" path) in
            let capt_th = Some (Printf.sprintf "%s.theta0.csv" path) in
            let hypar = read_hypar capt_hy in
            let theta = read_theta capt_th in
            U.time_it_to
              (Printf.sprintf "%s.duration.csv" path)
              (Estimate.posterior
                ?seed ?theta_every ?forward_every ?coal_every ?prm_every ?chol_every
                ~path ~n_iter hypar infer data)
              theta
          in
          Cmdliner.Term.(
                const flat
              $ Term.seed
              $ Term.theta_every
              $ Term.forward_every
              $ Term.coal_every
              $ Term.prm_every
              $ Term.chol_every
              $ Term.path
              $ Term.n_iter
              $ Hyper.term
              $ Infer.term
              $ Data.term
              $ Theta.term
          )
      end
  end

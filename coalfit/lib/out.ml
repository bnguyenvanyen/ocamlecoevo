open Abbrevs


let mrcas ?n_burn ?n_thin ~sim_traj ~path n =
  let sofo = Coal.Pop.string_of_float_option in
  let columns = Coal.Pop.mrca_columns n in
  let extract k res =
    match sim_traj (Fit.Mh.Return.sample res) with
    | exception Invalid_argument _ ->
        None
    | mrcas ->
    let e_mrca s =
      Scanf.sscanf s "%i-%i" (fun i j -> sofo mrcas.(i-1).(j-1))
    in
    let f =
      function
      | "k" ->
          string_of_int k
      | s ->
          e_mrca s
    in Some f
  in
  match n_thin with
  | Some n_thin ->
      let chan = Util.Csv.open_out path in
      (Fit.Csv.convert ?n_burn ~n_thin ~columns ~extract ~chan,
       fun () -> Util.Csv.close_out chan)
  | None ->
      (Util.Out.convert_null,
       fun () -> ())


let forests ?n_thin ~sim_traj ~chan_lengthed ~chan_timed =
  let out_one output chan k forest =
    Printf.fprintf chan ">%i\n" k ;
    L.iter (fun (js, (), tree) ->
      Printf.fprintf chan "#tree %s\n" (Coal.Inter.to_string js) ;
      output chan tree ;
      Printf.fprintf chan "\n\n"
    ) forest
  in
  let out k res =
    match sim_traj (Fit.Mh.Return.sample res) with
    | exception Invalid_argument _ ->
        ()
    | forest ->
    (* lengthed output *)
    out_one Tree.Lt.output_lengthed_newick chan_lengthed k forest ;
    (* timed output *)
    out_one Tree.Lt.output_newick chan_timed k forest
  in
  match n_thin with
  | None ->
      Util.Out.convert_null
  | Some n_thin ->
      let start k res =
        out k res
      in
      let out k res =
        if (k mod n_thin) = 0 then
          out k res
        else
          ()
      in
      let return kf resf =
        (kf, resf)
      in
      Util.Out.{ start ; out ; return }


let all
  ?theta_every ?traj_every ?chol_every ?prm_every ?n_burn
  ~columns ~extract ~sim_mrcas ~sim_forest ~convert_prm ~chol
  nsamples path =
  let output_theta =
    Simfit.Out.convert_csv ?n_burn ?n_thin:theta_every ~columns ~extract path
  in
  let output_prm =
    match prm_every with
    | Some n_thin ->
        convert_prm ?append:None ?n_burn ?n_thin:(Some n_thin) path
    | None ->
        Util.Out.convert_null
  in
  let path_traj = Printf.sprintf "%s.traj.csv" path in
  let path_traj_move = Printf.sprintf "%s.move.traj.csv" path in
  let output_traj, close_chan_traj =
    mrcas
      ?n_burn
      ?n_thin:traj_every
      ~sim_traj:sim_mrcas
      ~path:path_traj
      nsamples
  in
  let output_move_traj, close_chan_move =
    mrcas
      ?n_burn
      ?n_thin:traj_every
      ~sim_traj:sim_mrcas
      ~path:path_traj_move
      nsamples
  in
  let chan_lengthed_forest =
    open_out (Printf.sprintf "%s.forest.newick" path)
  in
  let chan_timed_forest =
    open_out (Printf.sprintf "%s.forest.tnewick" path)
  in
  let output_forest =
    forests
      ?n_thin:traj_every
      ~sim_traj:sim_forest
      ~chan_lengthed:chan_lengthed_forest
      ~chan_timed:chan_timed_forest
  in
  let output_chol =
    Simfit.Out.convert_chol ?n_burn ?chol_every path
  in
  let drop _ kres =
    kres
  in
  let out =
    output_theta
    |> Util.Out.combine drop output_prm
    |> Util.Out.combine drop output_traj
    |> Util.Out.combine drop output_move_traj
    |> Util.Out.combine drop output_forest
    (* here we need to exchange order *)
    |> (fun outres ->
        Util.Out.combine_tuple (fun (k,res) (_,chol) ->
          (* this function only gets called at return,
           * so we can close channels here *)
          close_chan_traj () ;
          close_chan_move () ;
          close_out chan_lengthed_forest ;
          close_out chan_timed_forest ;
          (k,res,chol)
        ) outres output_chol
      )
  in
  Fit.Csv.map
    ~return:(fun k res -> (k, res, chol))
    (fun res -> (res, chol))
    out


let all_no_chol
  ?theta_every ?traj_every ?prm_every ?n_burn
  ~columns ~extract ~sim_mrcas ~sim_forest ~convert_prm
  nsamples path =
  let output_theta =
    Simfit.Out.convert_csv ?n_burn ?n_thin:theta_every ~columns ~extract path
  in
  let output_prm =
    match prm_every with
    | Some n_thin ->
        convert_prm ?append:None ?n_burn ?n_thin:(Some n_thin) path
    | None ->
        Util.Out.convert_null
  in
  let path_traj = Printf.sprintf "%s.traj.csv" path in
  let path_traj_move = Printf.sprintf "%s.move.traj.csv" path in
  let output_traj, close_chan_traj =
    mrcas
      ?n_burn
      ?n_thin:traj_every
      ~sim_traj:sim_mrcas
      ~path:path_traj
      nsamples
  in
  let output_move_traj, close_chan_move =
    mrcas
      ?n_burn
      ?n_thin:traj_every
      ~sim_traj:sim_mrcas
      ~path:path_traj_move
      nsamples
  in
  let chan_lengthed_forest =
    open_out (Printf.sprintf "%s.forest.newick" path)
  in
  let chan_timed_forest =
    open_out (Printf.sprintf "%s.forest.tnewick" path)
  in
  let output_forest =
    forests
      ?n_thin:traj_every
      ~sim_traj:sim_forest
      ~chan_lengthed:chan_lengthed_forest
      ~chan_timed:chan_timed_forest
  in
  let drop _ kres =
    kres
  in
  let out =
    output_theta
    |> Util.Out.combine drop output_prm
    |> Util.Out.combine drop output_traj
    |> Util.Out.combine drop output_move_traj
    |> Util.Out.combine drop output_forest
  in
  Fit.Csv.map
    ~return:(fun k res ->
      (* this function only gets called at return,
       * so we can close channels here *)
      close_chan_traj () ;
      close_chan_move () ;
      close_out chan_lengthed_forest ;
      close_out chan_timed_forest ;
      (k, res)
    )
    (fun res -> res)
    out

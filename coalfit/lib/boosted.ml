(* Estimate under Kingman's coalescent with distance boosting
 *
 * Like Constant_bis,
 * but we introduce a model of evolutionary distance
 * to also estimate,
 * and to renormalize the merge times.
 *
 * We still sample from the same distribution,
 * except for the additional dimensions of the distance model
 *)


(* Before simulating the model, the merge times are mapped
 * through the distance.
 *
 * This also needs to be taken into account in the prior.
 *
 * Alternatively I could change only the jumps to rescale them by the distance,
 * but by changing the simulations, we can hope to converge much faster
 * if the initial distance model is good
 *)

open Abbrevs


module Cons = Constant

module Color = Cons.Color
module Sim = Cons.Sim
module Pop = Cons.Pop
module Prm = Cons.Prm
module Point = Cons.Point


(* Distance model *)
module Distance =
  struct
    let three_fourths = F.Pos.of_float (3. /. 4.)
    let four_thirds = F.Pos.of_float (4. /. 3.)

    (* expected evolutionary time separating seq from seq' under JC 69. *)
    (* This can be undefined, so not really what we want,
     * we want more like the expected value
     * conditional on it being defined or something like that *)
    (* - 3 / 4 * log (1 - 4 / 3 p_diff) *)
    let compute_normalized a a' =
      let len = A.length a in
      assert (A.length a' = len) ;
      let flen = F.Pos.of_float (float_of_int len) in
      (* number of differences *)
      let n_diffs = A.fold_lefti (fun nb k (i : int) ->
        if i = a'.(k) then
          nb
        else
          I.Pos.succ nb
      ) I.zero a
      in
      (* proportion of differences *)
      let p_diff = F.Proba.of_anyfloat F.Pos.Op.(
        (I.Pos.to_float n_diffs) / flen
      )
      in
      (* avoid saturation *)
      (* p_diff has to be < 0.75 *)
      assert F.Op.(p_diff <= F.Pos.of_float 0.5) ;
      (* thus, this can raise *)
      let a =
        match F.Proba.of_anyfloat (F.Pos.mul four_thirds p_diff) with
        | p ->
            p
        | exception Invalid_argument _ ->
            (* a > 1, and so p_diff > 0.75 which is forbidden *)
            invalid_arg "p_diff > 0.75"
      in
      a
      |> F.Proba.compl
      |> F.Proba.log
      |> F.Neg.neg
      |> F.Pos.mul three_fourths
      (* - 3 / 4 * log (1 - 4 / 3 p_diff) *)
  end


module Data =
  struct
    type 'a t = {
      length : int ;
      samples : (int * float * unit) list ;  (** sample times *)
      sequences : Seqs.t Intmap.t ;  (** indexed sequences *)
      invariants : int Seqsim.Tree.Ntm.t ; (** count invariant sites *)
      distances : 'a U.pos array array ;
      (** the normalized (for the mutation rate) expected time
       *  to coalescence (distance / 2) *)
    }

    let normalized n sequences =
      let seqcodes = Intmap.map Seqs.to_int_array sequences in
      let norm_exp_ts = A.make_matrix n n F.zero in
      U.int_fold ~f:(fun () k ->
        U.int_fold ~f:(fun () k' ->
          (* compute the expected time to coalescence *)
          let a = Intmap.find k seqcodes in
          let a' = Intmap.find k' seqcodes in
          let expected_t = Distance.compute_normalized a a' in
          norm_exp_ts.(k - 1).(k' - 1) <- expected_t
        ) ~n:(k - 1) ~x:()
      ) ~n ~x:() ;
      norm_exp_ts

    let read tseqs =
      let length = L.length tseqs in
      let sorted = L.sort (fun (t, _) (t', _) -> compare t' t) tseqs in
      let samples = L.mapi (fun k (t, _) -> (k + 1, t, ())) sorted in
      let sequences = L.fold_lefti (fun seqs i (_, seq) ->
        (* start from 1 *)
        let k = i + 1 in
        Intmap.add k seq seqs
      ) Intmap.empty sorted
      in
      (* distances computed on full sequences *)
      let distances = normalized length sequences in
      (* split off invariant sites *)
      let invariants, sequences =
        Seqsim.Base.split_invariant sequences
      in
      { length ; samples ; sequences ; invariants ; distances }


    let length { length ; _ } =
      length

    let sorted { samples ; _ } =
      L.map (fun (k, t, x) -> (k, F.Pos.of_float t, x)) samples

    let sequences { sequences ; _ } =
      sequences

    let invariants { invariants ; _ } =
      invariants

    let sample_time { samples ; _ } k =
      let _, t, () = L.at samples (k - 1) in
      F.Pos.of_float t

    let normalized_expected_time { distances ; _ } k k' =
      distances.(k - 1).(k' - 1)

    let to_constant_bis { length ; samples ; sequences ; invariants ; _ } =
      { Cons.Data.length ; samples ; sequences ; invariants }
  end


module Param =
  struct
    class theta = object
      inherit Cons.Param.theta

      val min_exp_fuel_v = 0.1

      method min_expected_fuel = min_exp_fuel_v

      method with_min_expected_fuel x = {< min_exp_fuel_v = x >}
    end

    class hyper = object
      inherit Param.hyper
    end

    let lambda = Param.lambda
    let mu = Param.mu
    let prm = Param.prm
    let prm_internal = Param.prm_internal
    let with_prm_internal = Param.with_prm_internal
    let min_expected_fuel = Param.min_expected_fuel

    let lambda_mean = Param.lambda_mean
    let lambda_var = Param.lambda_var

    let mu_mean = Param.mu_mean
    let mu_var = Param.mu_var


    let lambda_jump = Param.lambda_jump
    let mu_jump = Param.mu_jump
    let prm_jump = Param.prm_jump
    let min_exp_fuel_jump = Param.min_exp_fuel_jump

    let t0 = Param.t0
    let tf = Param.tf
    let h = Param.h
    let prm_nredraws = Param.prm_nredraws
  end


module Boost =
  struct
    let expected_internal_time data th =
      let mu = Param.mu th in
      let lbd = Param.lambda th in
      let eps = Param.min_expected_fuel th in
      (fun k k' ->
        (* We have [s] the evolutionary distance,
         * and [delta_t] the time between samples,
         * and we want to compute [t] the expected internal time to coalescence.
         * We have [s = mu * (2 * t / lbd + delta_t)],
         * so [t = lbd * (s / mu - delta_t) / 2]
         * [t = max(lbd * (s / mu - delta_t / 2), eps)]
         *)
        let s = Data.normalized_expected_time data k k' in
        let delta_t =
          F.abs F.Op.(
            (Data.sample_time data k) - (Data.sample_time data k')
          )
        in
        F.Pos.max eps F.Op.(lbd * (s / mu - delta_t / F.two))
      )

    let rescale data th =
      let exp_int_time = expected_internal_time data th in
      (fun k k' s ->
        let eit = exp_int_time k k' in
        F.Pos.mul eit s
      )

    let point_add eit dx pt =
      pt
      |> (fun s -> F.Pos.div s eit)
      |> (fun s ->
          match F.Pos.of_anyfloat (F.add s (F.of_float dx)) with
          | t ->
              t
          | exception (Invalid_argument _ as e) -> (* negative *)
              Fit.raise_draw_error e
         )
      |> (fun s -> F.Pos.mul s eit)

    let transform data th nu =
      let rsc = rescale data th in
      let n = Data.length data in
      U.int_fold_right ~f:(fun k ->
        U.int_fold_right ~f:(fun k' ->
          let color =
            Sim.Merge.color k k'
          in
          Prm.modify_map color (rsc k k')
        ) ~n:(k - 1)
      ) ~n nu

    let rand_prm data hy th rng =
      let tf = F.Pos.of_float (Param.tf hy) in
      let samples = Data.sorted data in
      let nu = Sim.Backward.rand_prm ~rng ~samples ~tf in
      match transform data th nu with
      | nu ->
          nu
      | exception (Invalid_argument _ as e) ->
          raise (Fit.Draw_error e)
  end


module Prior =
  struct
    let lambda = Cons.Prior.lambda
    let mu = Cons.Prior.mu

    (* FIXME should rescale by distance before evaluating,
       to keep the distribution the same
       problem : we need to know about th !
       !!! IS THIS STILL TRUE OR IS THIS OK ? !!! *)
    let prm data hy =
      let constant = Cons.Prior.prm (Data.to_constant_bis data) hy in
      (* Draw without taking the distances into account :
       * if the distance is good, we should converge nearly instantaneously *)
      let draw rng =
        Fit.Dist.draw_from rng constant
      in
      let log_dens_proba nu =
        (* FIXME evaluate on the transformed nu *)
        Fit.Dist.log_dens_proba constant nu
      in Fit.Dist.Base { draw ; log_dens_proba }
  end


module Propose =
  struct
    let lambda = Cons.Propose.lambda

    let mu = Cons.Propose.mu

    (* like Cons.Propose.prm_draw but with Boost.rescale on dx *)
    let prm_draw data th dx rng nu =
      let n = Data.length data in
      let nm1 = I.Pos.of_int (n - 1) in
      (* choose a random Merge point *)
      (* add a normal variable to its exponential variable *)
      match Boost.expected_internal_time data th with
      | exp_int_time ->
          let f k k' (pt : Point.t) : Point.t =
            let eit = exp_int_time k k' in
            Boost.point_add eit dx pt
          in
          let j = I.Pos.succ (U.rand_int ~rng nm1) in
          let i = U.rand_int ~rng j in
          let k = I.to_int i + 1 in
          let k' = I.to_int j + 1 in
          let color =
            Sim.Merge.color k k'
          in
          Prm.modify_map color (f k k') nu
      | exception (Invalid_argument _ as e) ->
          raise (Fit.Draw_error e)

    let prm_all data th dx nu =
      match Boost.expected_internal_time data th with
      | exp_int_time ->
          let f (c : Color.t) pt =
            match c with
            | Merge (_, _, _, k, k') ->
                let eit = exp_int_time k k' in
                Boost.point_add eit dx pt
            | Sample _ | Mutation _ ->
                pt
          in
          Prm.map_points f nu
      | exception (Invalid_argument _ as e) ->
          raise (Fit.Draw_error e)

    let prm data hy th =
      let v = Param.prm_jump hy in
      let draw rng nu =
        let dx = U.rand_normal ~rng 0. v in
        prm_draw data th dx rng nu
      in
      let log_pd_ratio _ _ =
        (* assume they come from the proposal,
         * then it's 0. (by symmetry)
         * (otherwise it would be neg_infinity but it's annoying to prove it,
         * and it should never happen in the normal run of the MCMC)
         *)
        [0.]
      in
      let move_to_sample nu =
        nu
      in
      Fit.Propose.Base { draw ; log_pd_ratio ; move_to_sample }
  end


(* copied from Cons.Draws, but with boosting *)
module Draws =
  struct
    let prm_single_random data infer th =
      let f rng dx nu =
        Propose.prm_draw data th dx rng nu
      in
      if Fit.Infer.is_free_adapt infer then
        [f]
      else
        []

    let len_prm_single_random infer =
      if Fit.Infer.is_free_adapt infer then
        1
      else
        0

    let prm_pairs data infer th =
      let n = Data.length data in
      let exp_int_time =
        match Boost.expected_internal_time data th with
        | eit ->
            eit
        | exception (Invalid_argument _ as e) ->
            raise (Fit.Draw_error e)
      in
      let add eit dx (pt : Point.t) : Point.t =
        Boost.point_add eit dx pt
      in
      let draw k k' dx nu =
        if k = k' then
          nu
        else
          let eit = exp_int_time k k' in
          let color =
            Sim.Merge.color k k'
          in
          Prm.modify_map color (add eit dx) nu
      in
      (* for a given lineage *)
      let f k _ dx nu =
        (* for all merge events on that lineage, add dx *)
        (* k' goes from 1 to n, <> k *)
        U.int_fold_right ~f:(fun k' nu ->
          draw k k' dx nu
        ) ~n nu
      in
      if Fit.Infer.is_free_adapt infer then
        U.int_fold_right ~f:(fun k draws ->
          f k :: draws
        ) ~n []
      else
        []

    let len_prm_pairs data infer =
      if Fit.Infer.is_free_adapt infer then
        Data.length data
      else
        0

    let prm_pairs_random data hy infer th =
      let n_redraw = Param.prm_nredraws hy in
      let n = Data.length data in
      let exp_int_time =
        match Boost.expected_internal_time data th with
        | eit ->
            eit
        | exception (Invalid_argument _ as e) ->
            raise (Fit.Draw_error e)
      in
      (* the set of 1 to n *)
      let first_n = U.int_fold_right ~f:(fun k ->
          Intset.add k
        ) ~n Intset.empty
      in
      let add eit dx (pt : Point.t) : Point.t =
        Boost.point_add eit dx pt
      in
      let draw k rng dx nu =
        let possible = Intset.remove k first_n in
        (* for n_redraw merge events on that lineage, add dx *)
        let selected =
          U.rand_subset
            (module Intset : BatSet.S with type elt = int
                                    and type t = Intset.t)
            ~rng
            possible
            n_redraw
        in
        Intset.fold (fun k' nu ->
          (* for all selected pairs (k, k') *)
          if k = k' then
            nu
          else
            let eit = exp_int_time k k' in
            let color =
              Sim.Merge.color k k'
            in
            Prm.modify_map color (add eit dx) nu
        ) selected nu
      in
      if Fit.Infer.is_free_adapt infer then
        U.int_fold_right  ~f:(fun k draws ->
          draw k :: draws
        ) ~n []
      else
        []

    let len_prm_pairs_random data infer =
      if Fit.Infer.is_free_adapt infer then
        Data.length data
      else
        0

    let prm_all data infer th =
      let f _ dx nu =
        Propose.prm_all data th dx nu
      in
      if Fit.Infer.is_free_adapt infer then
        [f]
      else
        []

    let len_prm_all infer =
      if Fit.Infer.is_free_adapt infer then
        1
      else
        0
  end


module Jumps = Cons.Jumps


module Likelihood =
  struct
    let sim ~t0 ~tf ~samples theta =
      let lambda = Param.lambda theta in
      let nu = Param.prm_internal theta in
      let zf, _, _ = Sim.Backward.until ~lambda ~samples ~t0 ~tf nu in
      zf

    let loglik = Cons.Likelihood.loglik

    let relabel = Cons.Likelihood.relabel

    let compute hy data =
      let t0 = F.Pos.of_float (Param.t0 hy) in
      let tf = F.Pos.of_float (Param.tf hy) in
      let samples = Data.sorted data in
      let sequences = Data.sequences data in
      let invs = Data.invariants data in
      let loglik = loglik sequences invs in
      let draw _ =
        failwith "Not_implemented"
      in
      let log_dens_proba th =
        let z = sim ~t0 ~tf ~samples th in
        [loglik th (Pop.to_forest z)]
      in Fit.Dist.Base { draw ; log_dens_proba }
  end


module Infer =
  struct
    type t = {
      lambda : Fit.Infer.t ;
      mu : Fit.Infer.t ;
      prm : Fit.Infer.t ;
      epsilon : Fit.Infer.t ;
    }

    let default = {
      lambda = Fit.Infer.free `Adapt ;
      mu = Fit.Infer.free `Adapt ;
      prm = Fit.Infer.free `Adapt ;
      epsilon = Fit.Infer.free `Adapt ;
    }

    let lambda = Cons.Infer.lambda

    let mu = Cons.Infer.mu

    (* can't be used for proposals, only for prior *)
    let prm data hy =
      Fit.Infer.maybe_adaptive_random
        ~tag:`Prm
        ~get:(fun th -> Prm.copy th#prm_internal)
        ~set:Param.with_prm_internal
        ~prior:(Prior.prm data hy)
        ~proposal:Fit.Propose.Constant_flat
        ~jumps:[]
        ~log_pd_ratio:None

    let min_expected_fuel hy =
      Fit.Infer.maybe_adaptive_float
        ~tag:`Epsilon
        ~get:(fun th -> th#min_expected_fuel)
        ~set:(fun th x -> th#with_min_expected_fuel x)
        ~prior:(Fit.Dist.Bound {
           low = 0. ;
           high = infinity ;
           probin = 1. ;
           base = Fit.Dist.Flat
         })
        ~proposal:Fit.Propose.Constant_flat
        ~jump:(F.to_float (Param.min_exp_fuel_jump hy))

    let fix tag infer =
      match tag with
      | `Lambda ->
          { infer with lambda = Fit.Infer.fixed }
      | `Mu ->
          { infer with mu = Fit.Infer.fixed }
      | `Prm ->
          { infer with prm = Fit.Infer.fixed }
      | `Epsilon ->
          { infer with epsilon = Fit.Infer.fixed }

    let infer is_adapt tag infer =
      match tag with
      | `Lambda ->
          { infer with lambda = Fit.Infer.free is_adapt }
      | `Mu ->
          { infer with mu = Fit.Infer.free is_adapt }
      | `Prm ->
          { infer with prm = Fit.Infer.free is_adapt }
      | `Epsilon ->
          { infer with epsilon = Fit.Infer.free is_adapt }
  end


module Estimate =
  struct
    module E = Cons.Estimate

    let columns =
      E.columns @ [
        "epsilon" ;
        "move_epsilon" ;
      ]

    let th_extract th =
      let sof = string_of_float in
      function
      | "epsilon" ->
          sof th#min_expected_fuel
      | s ->
          E.th_extract th s

    let extract k res =
      Some (Fit.Csv.extract_sample_move
        ~move_to_sample:(fun th -> th)
        ~extract:th_extract
        k res
      )

    let params_th hy (infer : Infer.t) =
      E.El Fit.Param.List.[
        Infer.lambda hy infer.lambda ;
        Infer.mu hy infer.mu ;
        Infer.min_expected_fuel hy infer.epsilon ;
      ]

    let params_all data hy (infer : Infer.t) =
      let E.El pars = params_th hy infer in
      E.El Fit.Param.List.(
           Infer.prm data hy infer.prm
        :: pars
      )

    (* ! prm *must* come after mu for the dependence to be
     * correctly taken into account *)
    let params data hy (infer : Infer.t) =
      Cons.Estimate.El Fit.Param.List.[
        Infer.lambda hy infer.lambda ;
        Infer.mu hy infer.mu ;
        Infer.prm data hy infer.prm ;
        Infer.min_expected_fuel hy infer.epsilon ;
      ]
   
    let sim_traj data hy th =
      let samples = Data.sorted data in
      let t0 = F.Pos.of_float (Param.t0 hy) in
      let tf = F.Pos.of_float (Param.tf hy) in
      Likelihood.sim ~t0 ~tf ~samples th

    let sim_mrcas data hy th =
      let n = Data.length data in
      let z = sim_traj data hy th in
      Pop.time_mrcas n z

    let sim_forest data hy th =
      let n = Data.length data in
      let z = sim_traj data hy th in
      let forest = Pop.to_forest z in
      Likelihood.relabel n forest

    let prm_vector_draw_dep draws =
      let vd th =
        let ad = A.of_list (draws th) in
        (fun rng v x ->
          A.fold_lefti (fun x' i d -> d rng v.{1 + i} x') x ad
        )
      in
      (fun rng v th ->
        th
        |> Param.prm_internal
        |> Prm.copy
        |> vd th rng v
        |> th#with_prm_internal
      )

    (* joint draws on prm and th,
       where th draws on prm depend on th.
       First draw for th, with the end part of the random vector,
       then draw for prm with the first part of the random vector,
       and the updated th value *)
    let prm_th_vector_draw_dep len_prm_draws prm_draws th_draws =
      let prm_vd th =
        let ad = A.of_list (prm_draws th) in
        (fun rng v x ->
          A.fold_lefti (fun x' i d -> d rng v.{1 + i} x') x ad
        )
      in
      let th_vd = Fit.Propose.vector_draw
        ~start:(1 + len_prm_draws)
        (A.of_list th_draws)
      in
      (fun rng v th ->
        let th = th_vd rng v th in
        th
        |> Param.prm_internal
        |> Prm.copy
        |> prm_vd th rng v
        |> th#with_prm_internal
      )

    let prior data hy infer =
      let E.El pars = params_all data hy infer in
      Fit.Param.List.prior (new Param.theta) pars

    let prop_th_mh
      hypar infer output prior likelihood =
      let El pars = params_th hypar infer in
      let proposal = Fit.Param.List.custom_proposal pars in
      Fit.Mh.Loop.proposal
        ~output
        (fun x -> x)
        prior
        proposal
        likelihood

    let prop_th_ram
      hypar infer output prior likelihood =
      let El pars = params_th hypar infer in
      let draws = Fit.Param.List.adapt_draws pars in
      let draws_ratio = Fit.Param.List.log_pd_ratio pars in
      let chol =
        pars
        |> Fit.Param.List.adapt_jumps
        |> E.to_chol
      in
      let vdraw = E.vector_draw ~before:Fun.id ~after:Fun.id draws in
      Fit.Ram.Loop.proposal
        ~output
        ?draws_ratio
        (fun x -> x)
        (fun x -> x)
        (L.length draws)
        chol
        prior
        vdraw
        likelihood

    let prop_prm_single_ram
      hy data (infer : Infer.t) output prior likelihood =
      let draws = Draws.prm_single_random data infer.prm in
      let len = Draws.len_prm_single_random infer.prm in
      let chol =
        E.to_chol (Jumps.prm_single_random hy#prm_jump infer.prm)
      in
      let vdraw = prm_vector_draw_dep draws in
      Fit.Ram.Loop.proposal
        ~output
        (fun x -> x)
        (fun x -> x)
        len
        chol
        prior
        vdraw
        likelihood

    (* split it into more proposals *)
    let prop_prm_pairs_ram
      hy data (infer : Infer.t) output prior likelihood =
      let n = Data.length data in
      let draws = Draws.prm_pairs data infer.prm in
      let len = Draws.len_prm_pairs data infer.prm in
      let chol =
        E.to_chol (Jumps.prm_pairs n hy#prm_jump infer.prm)
      in
      let vdraw = prm_vector_draw_dep draws in
      Fit.Ram.Loop.proposal
        ~output
        (fun x -> x)
        (fun x -> x)
        len
        chol
        prior
        vdraw
        likelihood
     
    let prop_prm_pairs_redraw_ram
      hy data (infer : Infer.t) output prior likelihood =
      let n = Data.length data in
      let draws = Draws.prm_pairs_random data hy infer.prm in
      let len = Draws.len_prm_pairs_random data infer.prm in
      let chol =
        E.to_chol (Jumps.prm_pairs n hy#prm_jump infer.prm)
      in
      let vdraw = prm_vector_draw_dep draws in
      Fit.Ram.Loop.proposal
        ~output
        (fun x -> x)
        (fun x -> x)
        len
        chol
        prior
        vdraw
        likelihood

    let prop_prm_all_ram
      hy data (infer : Infer.t) output prior likelihood =
      let draws = Draws.prm_all data infer.prm in
      let len = Draws.len_prm_all infer.prm in
      let chol =
        E.to_chol (Jumps.prm_all hy#prm_jump infer.prm)
      in
      let vdraw = prm_vector_draw_dep draws in
      Fit.Ram.Loop.proposal
        ~output
        Fun.id
        Fun.id
        len
        chol
        prior
        vdraw
        likelihood

    let prop_prm_all_th_ram
      hy data (infer : Infer.t) output prior likelihood =
      let draws_prm = Draws.prm_all data infer.prm in
      let len_prm = Draws.len_prm_all infer.prm in
      let El pars = params_th hy infer in
      let draws_th = Fit.Param.List.adapt_draws pars in
      let draws_ratio = Fit.Param.List.log_pd_ratio pars in
      let jumps =
          (Jumps.prm_all hy#prm_jump infer.prm)
        @ (Fit.Param.List.adapt_jumps pars)
      in
      let chol = E.to_chol jumps in
      let vdraw = prm_th_vector_draw_dep len_prm draws_prm draws_th in
      Fit.Ram.Loop.proposal
        ~output
        ?draws_ratio
        Fun.id
        Fun.id
        (L.length jumps)
        chol
        prior
        vdraw
        likelihood

    let proposal
      ?theta_every ?traj_every ?prm_every
      ~path (hypar : Param.hyper) infer data =
      let sim_mrcas = sim_mrcas data hypar in
      let sim_forest = sim_forest data hypar in
      let output =
        Out.all_no_chol
          ?theta_every
          ?traj_every
          ?prm_every
          ~columns
          ~extract
          ~sim_mrcas
          ~sim_forest
          ~convert_prm:E.convert_prm
          (Data.length data)
          path
      in
      let E.El pars = params_all data hypar infer in
      let prior = Fit.Param.List.prior (new Param.theta) pars in
      let likelihood = Likelihood.compute hypar data in
      (* FIXME the prop should not be in the choices,
       * when it does nothing *)
      let prop_th_mh =
        prop_th_mh hypar infer output prior likelihood
      in
      let prop_th_ram =
        prop_th_ram hypar infer output prior likelihood
      in
      let prop_prm_single_ram =
        prop_prm_single_ram hypar data infer output prior likelihood
      in
      let prop_prm_pairs_ram =
        prop_prm_pairs_ram hypar data infer output prior likelihood
      in
      let prop_prm_pairs_redraw_ram =
        prop_prm_pairs_redraw_ram hypar data infer output prior likelihood
      in
      let prop_prm_all_ram =
        prop_prm_all_ram hypar data infer output prior likelihood
      in
      let prop_prm_all_th_ram =
        prop_prm_all_th_ram hypar data infer output prior likelihood
      in
      let choices = [
        (prop_th_mh, hypar#proposal_weights.th) ;
        (prop_th_ram, hypar#proposal_weights.th) ;
        (prop_prm_single_ram, hypar#proposal_weights.prm_single) ;
        (prop_prm_pairs_ram, hypar#proposal_weights.prm_pairs) ;
        (prop_prm_pairs_redraw_ram, hypar#proposal_weights.prm_pairs_redraw) ;
        (prop_prm_all_ram, hypar#proposal_weights.prm_all) ;
        (prop_prm_all_th_ram, hypar#proposal_weights.prm_all_th) ;
      ]
      in
      let start k th =
        let smpl = Fit.Mh.Sample.weight prior likelihood th in
        let par_res = Fit.Mh.Return.accepted (fun x -> x) smpl in
        output.start k par_res ;
        smpl
      in
      let return k smplf =
        let res = Fit.Mh.Return.accepted (fun x -> x) smplf in
        output.return k res
      in
      Fit.Propose.Choice choices, start, return

    let posterior
      ?seed ?theta_every ?traj_every ?prm_every
      ~path ~n_iter hypar infer tseqs theta =
      let data = Data.read tseqs in
      let proposal, start, return = proposal
        ?theta_every
        ?traj_every
        ?prm_every
        ~path
        hypar
        infer
        data
      in
      let nu = Boost.rand_prm data hypar theta (U.rng seed) in
      let theta' = theta#with_prm_internal nu in
      let smpl0 = start 0 theta' in
      let smplf = Fit.Markov_chain.direct_simulate
        ?seed
        proposal
        smpl0
        n_iter
      in
      return n_iter smplf
  end


module Term =
  struct
    module C = Cmdliner
    module T = Cons.Term

    let hyper () =
      let f lbd_m lbd_v lbd_j mu_m mu_v mu_j t0 tf prm_j prm_nr eps_j pw config capture =
        (new Param.hyper)
        |> U.Arg.Capture.empty
        |> lbd_m ~config
        |> lbd_v ~config
        |> lbd_j ~config
        |> mu_m ~config
        |> mu_v ~config
        |> mu_j ~config
        |> t0 ~config
        |> tf ~config
        |> prm_j ~config
        |> prm_nr ~config
        |> eps_j ~config
        |> pw
        |> U.Arg.Capture.output capture
      in
      C.Term.(const f
          $ Term.Hyper.lambda_mean ()
          $ Term.Hyper.lambda_var ()
          $ Term.Hyper.lambda_jump ()
          $ Term.Hyper.mu_mean ()
          $ Term.Hyper.mu_var ()
          $ Term.Hyper.mu_jump ()
          $ Term.Hyper.t0 ()
          $ Term.Hyper.tf ()
          $ Term.Hyper.prm_jump ()
          $ Term.Hyper.prm_nredraws ()
          $ Term.Hyper.min_exp_fuel_jump ()
          $ Term.Hyper.proposal_weights ()
          $ Term.Hyper.load_from
      )

    let tags = [
      ("lambda", `Lambda) ;
      ("mu", `Mu) ;
      ("prm", `Prm) ;
      ("epsilon", `Epsilon) ;
    ]

    let theta0 () =
      let f mu lbd eps config capture =
        (new Param.theta)
        |> U.Arg.Capture.empty
        |> mu ~config
        |> lbd ~config
        |> eps ~config
        |> U.Arg.Capture.output capture
      in
      C.Term.(const f
          $ Term.Theta0.mu ()
          $ Term.Theta0.lambda ()
          $ Term.Theta0.min_expected_fuel ()
          $ Term.Theta0.load_from
      )

    let infer_adapt =
      let doc =
          "Set the specified parameter to be inferred "
        ^ "as part of the adaptive proposal."
      in
      T.infer_as ~doc ~name:"infer-adapt" (Infer.infer `Adapt) tags

    let infer_custom =
      let doc =
          "Set the specified parameter to be inferred "
        ^ "with its custom proposal."
      in
      T.infer_as ~doc ~name:"infer-custom" (Infer.infer `Custom) tags

    let infer_prior =
      let doc =
        "Set the specified parameter to be inferred "
      ^ "using its prior distribution as proposal."
      in
      T.infer_as ~doc ~name:"infer-prior" (Infer.infer `Prior) tags

    let fix =
      let doc =
        "Set the specified parameter to be fixed to the given value."
      in
      T.infer_as ~doc ~name:"fix" Infer.fix tags

    let infer =
      let f fix prior custom adapt =
        Infer.default
        |> fix
        |> prior
        |> custom
        |> adapt
      in
      C.Term.(const f
          $ fix
          $ infer_prior
          $ infer_custom
          $ infer_adapt
      )

    let run =
      let flat seed theta_every traj_every prm_every
               path n_iter read_hypar infer txseqs read_theta =
        let capt_hy = Some (Printf.sprintf "%s.par.csv" path) in
        let capt_th = Some (Printf.sprintf "%s.theta0.csv" path) in
        let hypar = read_hypar capt_hy in
        let theta = read_theta capt_th in
        let tseqs = L.map (fun ((t, _), seq) -> (t, seq)) txseqs in
        U.time_it_to
          (Printf.sprintf "%s.duration.csv" path)
          (Estimate.posterior
            ?seed ?theta_every ?traj_every ?prm_every
            ~path ~n_iter hypar infer tseqs)
          theta
      in
      C.Term.(const flat
          $ Term.seed
          $ Term.theta_every
          $ Term.traj_every
          $ Term.prm_every
          $ Term.path
          $ Term.n_iter
          $ hyper ()
          $ infer
          $ Term.sequences
          $ theta0 ()
      )

    let run_unit =
      C.Term.(const ignore $ run)

    let info_doc =
      "Estimate a phylogeny under Kingman's coalescent and JC69
       substitutions from timed DNA sequences, with distance boosting."

    let info =
      let doc = info_doc in
      C.Term.info
        "coalfit-constant-boosted"
        ~version:"%%VERSION%%"
        ~doc
        ~exits:C.Term.default_exits
        ~man:[]

    let info_sub =
      let doc = info_doc in
      C.Term.info
        "boosted"
        ~version:"%%VERSION%%"
        ~doc
        ~exits:C.Term.default_exits
        ~man:[]
  end

module A = BatArray
module L = BatList
module M = BatMap

module Intset = BatSet.Int
module Intmap = BatMap.Int

module Lac = Lacaml.D

module U = Util
module F = U.Float
module I = U.Int
module D = Fit.Dist



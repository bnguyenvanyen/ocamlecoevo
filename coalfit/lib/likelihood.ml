open Abbrevs


module type SAMPLES =
  sig
    val seqs : Seqs.t M.Int.t
  end


module Make
  (State : Coal.State.S)
  (T : Tree.Timed.S with type state = State.t) =
  struct
    let relabel n tree =
      let rec f m tree =
        let leaf (t, ks, _) =
          let k =
            let k, rem = Intset.pop ks in
            if (Intset.is_empty rem) then
              k
            else
              failwith "invalid leaf"
          in
          Tree.Lt.leaf (t, k), m
        in
        let node x stree =
          let t = T.State.to_time x in
          let stree', m = f m stree in
          Tree.Lt.node (t, m + 1) stree', (m + 1)
        in
        let binode x ltree rtree =
          let t = T.State.to_time x in
          let ltree', m = f m ltree in
          let rtree', m = f m rtree in
          Tree.Lt.binode (t, m + 1) ltree' rtree', (m + 1)
        in
        T.case ~leaf ~node ~binode tree
      in
      f n tree

    let relabel_forest n forest =
      let _, forest' = L.fold_left_map (fun n (js, x, tree) ->
        let tree', n' = relabel n tree in
        (n', (js, x, tree'))
      ) n forest
      in
      forest'

    module S = Seqsim
    module Fel = S.Tree.Make (Tree.Lt.State) (Tree.Lt)

    let loglik kseqs invs = 
      let stgs = S.Probas.Homogen in
      let nseqs = Intmap.cardinal kseqs in
      let seq_probas = S.Probas.sequences kseqs in
      let eq = S.Jc69.equilibrium_probas () in
      let mat = S.Snp.zero_mat None in
      let f theta forest =
        let par = S.Jc69.{ mu = F.to_float (Param.mu theta) } in
        let trans =
          let tp = S.Jc69.transition_probas ~mat par in
          let trans_mat dt =
            tp (F.Pos.of_float dt)
          in
          S.Probas.sym_transition stgs trans_mat
        in
        (* FIXME allocations *)
        let loglik_var =
          Fel.log_likelihood_variant stgs seq_probas eq trans
        in
        let loglik_inv =
          Fel.log_likelihood_invariant stgs invs eq trans
        in
        (* sum log-likelihood over trees *)
        forest
        |> relabel_forest nseqs
        |> L.fold_left (fun x (_, _, tree) ->
             x +. loglik_var tree +. loglik_inv tree
           ) 0.
      in f
  end


let relabel nseqs forest =
  let module S = Coal.State.Make (Coal.Constant.Trait) in
  let module T = Coal.Constant.Pop.T in
  let module M = Make (S) (T) in
  M.relabel_forest nseqs forest


let loglik_constant kseqs invs =
  let module S = Coal.State.Make (Coal.Constant.Trait) in
  let module T = Coal.Constant.Pop.T in
  let module M = Make (S) (T) in
  M.loglik kseqs invs

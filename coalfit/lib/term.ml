open Abbrevs

module C = Cmdliner
module UAC = U.Arg.Capture
module UAUC = U.Arg.Update.Capture


let int_opt ~doc ~docv ~arg =
  C.Arg.(value
     & opt (some int) None
     & info [arg] ~docv ~doc
  )


let float_opt ~doc ~docv ~arg =
  C.Arg.(value
     & opt (some float) None
     & info [arg] ~docv ~doc
  )


let positive_opt ~doc ~docv ~arg =
  C.Arg.(value
     & opt (some (U.Arg.positive ())) None
     & info [arg] ~docv ~doc
  )


let float_arg ~doc ~get ~set name =
  UAUC.opt_config get set C.Arg.float ~doc name

let int_arg ~doc ~get ~set name =
  UAUC.opt_config get set C.Arg.int ~doc name

let pos_arg ~doc ~get ~set name =
  UAUC.opt_config get set (U.Arg.positive ()) ~doc name
  

module Hyper =
  struct
    let load_from =
      let doc =
        "Load (hyper) parameter values from $(docv)."
      in
      C.Arg.(
        value
        & opt (some string) None
        & info ["load-par-from"] ~docv:"LOAD-PAR-FROM" ~doc
      )

    let lambda_mean () =
      let doc =
        "Logarithm of the mean of the lognormal prior on 'lambda'."
      in
      let get hy = hy#lambda_mean in
      let set x hy = hy#with_lambda_mean x in
      float_arg ~doc ~get ~set "lambda-mean"

    let lambda_var () =
      let doc =
        "Logarithm of the variance of the lognormal prior on 'lambda'."
      in
      let get hy = hy#lambda_var in
      let set x hy = hy#with_lambda_var x in
      float_arg ~doc ~get ~set "lambda-var"

    let lambda_jump () =
      let doc =
        "Initial standard deviation of the gaussian proposals for 'lambda'."
      in
      let get hy = hy#lambda_jump in
      let set x hy = hy#with_lambda_jump x in
      float_arg ~doc ~get ~set "lambda-jump"

    let mu_mean () =
      let doc =
        "Logarithm of the mean of the lognormal prior on 'mu'."
      in
      let get hy = hy#mu_mean in
      let set x hy = hy#with_mu_mean x in
      float_arg ~doc ~get ~set "mu-mean"

    let mu_var () =
      let doc =
        "Logarithm of the variance of the lognormal prior on 'mu'."
      in
      let get hy = hy#mu_var in
      let set x hy = hy#with_mu_var x in
      float_arg ~doc ~get ~set "mu-var"

    let mu_jump () =
      let doc =
        "Initial standard deviation of the gaussian proposals for 'mu'."
      in
      let get hy = hy#mu_jump in
      let set x hy = hy#with_mu_jump x in
      float_arg ~doc ~get ~set "mu-jump"

    let t0 () =
      let doc =
        "Estimate the system starting from time $(docv)."
      in
      let get hy = hy#t0 in
      let set x hy = hy#with_t0 x in
      float_arg ~doc ~get ~set "t0"

    let tf () =
      let doc =
        "Estimate the system up to $(docv) in time."
      in
      let get hy = hy#tf in
      let set x hy = hy#with_tf x in
      float_arg ~doc ~get ~set "tf"

    let width () =
      let doc =
        "Width of time slices for the estimated prm."
      in
      let get hy = hy#h in
      let set x hy = hy#with_h x in
      float_arg ~doc ~get ~set "width"

    let prm_jump () =
      let doc =
        "Standard deviation of prm jumps."
      in
      let get hy = hy#prm_jump in
      let set x hy = hy#with_prm_jump x in
      float_arg ~doc ~get ~set "prm-jump"

    let prm_nredraws () =
      let doc =
        "Redraw $(docv) merge pairs by lineage on prm proposal."
      in
      let get hy = hy#prm_nredraws in
      let set x hy = hy#with_prm_nredraws x in
      int_arg ~doc ~get ~set "prm-nredraws"

    let min_exp_fuel_jump () =
      let doc =
        "Initial standard deviation of minimum expected fuel epsilon."
      in
      let get hy = hy#min_exp_fuel_jump in
      let set x hy = hy#with_min_exp_fuel_jump x in
      float_arg ~doc ~get ~set "epsilon-jump"

    let proposal_weights () =
      let set x hy = hy#with_proposal_weights x in
      let f pw =
        pw
        |> UAC.empty
        |> UAUC.update_always set
      in
      C.Term.(const f $ Proposal_weights.term)
  end


module Theta0 =
  struct
    let load_from =
      let doc =
        "Load initial theta parameter values from $(docv)."
      in
      C.Arg.(
        value
        & opt (some string) None
        & info ["load-theta-from"] ~docv:"LOAD-THETA-FROM" ~doc
      )

    let mu () =
      let doc =
        "Nucleotide substitution rate."
      in
      let get th = th#mu in
      let set x th = th#with_mu x in
      float_arg ~doc ~get ~set "mu"

    let lambda () =
      let doc =
        "Lineage pair coalescence rate."
      in
      let get th = th#lambda in
      let set x th = th#with_lambda x in
      float_arg ~doc ~get ~set "lambda"

    let min_expected_fuel () =
      let doc =
        "Minimum expected fuel epsilon for boosting."
      in
      let get th = th#min_expected_fuel in
      let set x th = th#with_min_expected_fuel x in
      float_arg ~doc ~get ~set "min-expected-fuel"
  end


let path =
  let doc = "Output results to file names starting by $(docv)." in
  C.Arg.(
    required
    & pos 0 (some string) None
    & info [] ~docv:"PATH" ~doc
  )


let n_thin =
  let doc =
    "Output sample every $(docv) iterations.
     By default output at every iteration."
  in int_opt ~doc ~docv:"NTHIN" ~arg:"nthin"


let theta_every =
  let doc =
      "Output parameter samples every $(docv) iterations. "
    ^ "By default, do not output."
  in int_opt ~doc ~arg:"theta-every" ~docv:"THETA-EVERY"


let traj_every =
  let doc =
    "Output system trajectory every $(docv) iterations.
     By default do not output it."
  in int_opt ~doc ~docv:"TRAJ-EVERY" ~arg:"traj-every"


let forward_every =
  let doc =
    "Output forward system trajectory every $(docv) iterations.
     By default, do not output."
  in int_opt ~doc ~docv:"FORWARD-EVERY" ~arg:"forward-every"


let coal_every =
  let doc =
    "Output coalescent system trajectory every $(docv) iterations.
     By default, do not output."
  in int_opt ~doc ~docv:"COAL-EVERY" ~arg:"coal-every"


let prm_every =
  let doc =
    "Ouptut point process realisation every $(docv) iterations.
     By default do not output it."
  in int_opt ~doc ~docv:"PRM-EVERY" ~arg:"prm-every"


let chol_every =
  let doc =
    "Output covariance matrix every $(docv) iterations.
     By default do not output it."
  in int_opt ~doc ~docv:"CHOL-EVERY" ~arg:"chol-every"


let adapt_until =
  let doc =
    "Number of iterations during which to adapt the RAM proposals."
  in int_opt ~doc ~docv:"ADAPT-UNTIL" ~arg:"adapt-until"


let n_prior =
  let doc =
    "At the start of the MCMC run, use the prior as proposal
     during $(docv) iterations. Equal 0 by default."
  in
  C.Arg.(value
     & opt int 0
     & info ["nprior"] ~docv:"NPRIOR" ~doc
  )


let n_iter =
  let doc =
    "Run $(docv) iterations of adaptive MCMC. Equal 0 by default."
  in
  C.Arg.(value
     & opt int 0
     & info ["niter"] ~docv:"NITER" ~doc
  )


let seed =
  let doc =
    "Initialize the PRNG with the seed $(docv)."
  in int_opt ~doc ~docv:"SEED" ~arg:"seed"


let sequence_path =
  let doc =
    "Path to sequence data FASTA file."
  in
  C.Arg.(
      required
    & opt (some string) None
    & info ["sequences"] ~docv:"SEQUENCES" ~doc
  )


let parse_fasta_headers () =
  let doc =
    "Read the timestamp from FASTA headers with format string $(docv)."
  in
  let arg = U.Arg.opt C.Arg.string ~doc "parse-fasta-headers" in
  let f s =
    Scanf.format_from_string s "%s@:%f"
  in
  C.Term.(const (Option.map f) $ arg)


let sequences =
  let f fmt fname =
    let chan = open_in fname in
    let scan =
      match fmt with
      | None ->
          (fun s -> Scanf.sscanf s "%s@:%f" (fun s t -> (t, s)))
      | Some fmt ->
          (fun s -> Scanf.sscanf s fmt (fun s t -> (t, s)))
    in
    let read_stamp s =
      match scan s with
      | tx ->
          Some tx
      | exception Scanf.Scan_failure _ ->
          None
    in
    let txseqs =
      match Seqs.Io.assoc_of_any_fasta read_stamp chan with
      | None ->
          failwith
          "FASTA stamps do not respect the format ('%s@:%f' or supplied)"
      | Some txseqs ->
          txseqs
    in
    close_in chan ;
    L.map (fun ((t, x), seq) ->
        ((F.to_float (F.Pos.of_float t), x), seq)
    ) txseqs
  in
  C.Term.(
    const f
    $ parse_fasta_headers ()
    $ sequence_path
  )

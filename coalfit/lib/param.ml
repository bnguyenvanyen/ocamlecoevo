open Abbrevs


class prior = object
  val lambda_mean_v = 1.
  val lambda_var_v = 1.

  val mu_mean_v = 1e-4
  val mu_var_v = 1.

  method lambda_mean = lambda_mean_v
  method lambda_var = lambda_var_v

  method mu_mean = mu_mean_v
  method mu_var = mu_var_v

  method with_lambda_mean x = {< lambda_mean_v = x >}
  method with_lambda_var x = {< lambda_var_v = x >}

  method with_mu_mean x = {< mu_mean_v = x >}
  method with_mu_var x = {< mu_var_v = x >}
end


class prop = object
  val lambda_jump_v = 1e-3
  val mu_jump_v = 1e-6
  val prm_jump_v = 1e-4
  val min_exp_fuel_jump_v = 1e-4

  method lambda_jump = lambda_jump_v
  method mu_jump = mu_jump_v
  method prm_jump = prm_jump_v
  method min_exp_fuel_jump = min_exp_fuel_jump_v

  method with_lambda_jump x = {< lambda_jump_v = x >}
  method with_mu_jump x = {< mu_jump_v = x >}
  method with_prm_jump x = {< prm_jump_v = x >}
  method with_min_exp_fuel_jump x = {< min_exp_fuel_jump_v = x >}
end


class env = object
  val t0_v = 0.
  val tf_v = 1.
  val h_v = 0.5
  val prm_nredraws_v = 1
  val pw_v = Proposal_weights.default

  method t0 = t0_v
  method tf = tf_v
  method h = h_v
  method prm_nredraws = prm_nredraws_v
  method proposal_weights = pw_v

  method with_t0 x = {< t0_v = x >}
  method with_tf x = {< tf_v = x >}
  method with_h x = {< h_v = x >}
  method with_prm_nredraws n = {< prm_nredraws_v = n >}
  method with_proposal_weights x = {< pw_v = x >}
end


class hyper = object
  inherit prior
  inherit prop
  inherit env
end


let fpof = F.Pos.of_float

let lambda p = fpof p#lambda
let mu p = fpof p#mu
let prm p = p#prm
let prm_internal p = p#prm_internal
let with_prm_internal p nu = p#with_prm_internal nu
let min_expected_fuel p = fpof p#min_expected_fuel

let lambda_mean p = p#lambda_mean
let lambda_var p = fpof p#lambda_var

let mu_mean p = p#mu_mean
let mu_var p = fpof p#mu_var

let lambda_jump p = fpof p#lambda_jump
let mu_jump p = fpof p#mu_jump
let prm_jump p = fpof p#prm_jump
let min_exp_fuel_jump p = fpof p#min_exp_fuel_jump

let t0 p = p#t0
let tf p = p#tf
let h p = fpof p#h
let prm_nredraws p = p#prm_nredraws
let proposal_weights p = p#proposal_weights

open Abbrevs


module type COAL =
  sig
    type hyper

    type theta

    type infer

    type tag

    type sim

    val par_header : string list

    val par_line : hyper -> string list

    val to_hyper_sim : hyper -> sim

    val default_theta : theta

    val getsets : 
      infer ->
      hyper ->
        ((theta -> float) * (theta -> float -> theta)) array

    val jumps :
      infer ->
      hyper ->
        float array

    val prior :
      infer ->
      hyper ->
        (theta, float list) D.t

    val proposal :
      infer ->
      hyper ->
        (theta -> (theta, float list) D.t)

    val simulate :
      sim ->
      theta ->
        ('a U.pos * float) Jump.cadlag

    val csv_simulate :
      chan : out_channel ->
      sim ->
      theta ->
        unit

    val header : string list list

    val line :
      int ->
      theta ->
      string ->
      float ->
      float ->
      float ->
        string list
  end

module L = BatList

module U = Util
module F = U.Float


type t = {
  th : U.closed_pos ;
  (** MH and RAM proposals for theta *)
  prm_single : U.closed_pos ;
  (** uni dim RAM proposal for a single random merge pair *)
  prm_pairs : U.closed_pos ;
  (** n dim RAM proposal for all lineages, paired with a random lineage *)
  prm_pairs_redraw : U.closed_pos ;
  (** n dim RAM proposal for all lineages, paired with k random lineages *)
  prm_all : U.closed_pos ;
  (** uni dim RAM proposal for all merge pairs *)
  prm_all_th : U.closed_pos ;
  (** joint RAM proposal for theta and prm (via prm_all) *)
  scale_prm_all_th : U.closed_pos ;
  (** joint RAM scaling proposal for theta and prm (via prm_all) *)
}


let zero = {
  th = F.zero ;
  prm_single = F.zero ;
  prm_pairs = F.zero ;
  prm_pairs_redraw = F.zero ;
  prm_all = F.zero ;
  prm_all_th = F.zero ;
  scale_prm_all_th = F.zero ;
}


let default = {
  th = F.Pos.of_float (1. /. 4.) ;
  prm_single = F.one ;
  prm_pairs = F.Pos.of_float (1. /. 4.) ;
  prm_pairs_redraw = F.Pos.of_float (1. /. 4.) ;
  prm_all = F.Pos.of_float (1. /. 4.) ;
  prm_all_th = F.Pos.of_float (1. /. 4.) ;
  scale_prm_all_th = F.zero ;
}


let modify =
  function
  | `Theta ->
      (fun x y -> { y with th = x })
  | `Prm_single ->
      (fun x y -> { y with prm_single = x })
  | `Prm_pairs ->
      (fun x y -> { y with prm_pairs = x })
  | `Prm_pairs_redraw ->
      (fun x y -> { y with prm_pairs_redraw = x })
  | `Prm_all ->
      (fun x y -> { y with prm_all = x })
  | `Prm_all_theta ->
      (fun x y -> { y with prm_all_th = x })
  | `Scale_prm_all_theta ->
      (fun x y -> { y with scale_prm_all_th = x })


let to_string =
  function
  | `Theta ->
      "theta"
  | `Prm_single ->
      "prm-single"
  | `Prm_pairs ->
      "prm-pairs"
  | `Prm_pairs_redraw ->
      "prm-pairs-redraw"
  | `Prm_all ->
      "prm-all"
  | `Prm_all_theta ->
      "prm-all-theta"
  | `Scale_prm_all_theta ->
      "scale-prm-all-theta"


module Term =
  struct
    let tags =
      [
        `Theta ;
        `Prm_single ;
        `Prm_pairs ;
        `Prm_pairs_redraw ;
        `Prm_all ;
        `Prm_all_theta ;
        `Scale_prm_all_theta ;
      ]
      |> L.map (fun tag -> (to_string tag, tag))
      |> Cmdliner.Arg.enum

    let modify =
      let doc =
        "Modify the propensity to choose this proposal"
      in
      Cmdliner.Arg.(
        value
        & opt_all (pair tags (U.Arg.positive ())) []
        & info
          ["proposal-weight"]
          ~docv:"PROPOSAL-WEIGHT"
          ~doc
      )
  end


let term =
  let f modifiers =
    L.fold_right
      (fun (tag, w) -> modify tag w)
      modifiers
      default
  in
  Cmdliner.Term.(
    const f
    $ Term.modify
  )

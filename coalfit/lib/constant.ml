open Abbrevs


module Consl = Coal.Constant_lazy
module Sim = Consl.Simulate
module Pop = Consl.Pop
module Evm = Consl.Evm
module Color = Consl.Color
module Prm = Consl.Prm
module Point = Prm.Point
module Points = Prm.Points


module Param =
  struct
    class theta = object
      val lambda_v = 1.
      val mu_v = 1.
      val prm_internal_v : Prm.t option = None

      method lambda = lambda_v
      method mu = mu_v

      method prm_internal =
        match prm_internal_v with
        | None ->
            invalid_arg "unset internal prm"
        | Some nu ->
            nu

      method with_lambda x = {< lambda_v = x >}
      method with_mu x = {< mu_v = x >}
      method with_prm_internal nu = {< prm_internal_v = Some nu >}
    end

    class hyper = object
      inherit Param.hyper
    end

    let lambda = Param.lambda
    let mu = Param.mu
    let prm = Param.prm
    let prm_internal = Param.prm_internal
    let with_prm_internal = Param.with_prm_internal

    let lambda_mean = Param.lambda_mean
    let lambda_var = Param.lambda_var

    let mu_mean = Param.mu_mean
    let mu_var = Param.mu_var

    let lambda_jump = Param.lambda_jump
    let mu_jump = Param.mu_jump
    let prm_jump = Param.prm_jump

    let t0 = Param.t0
    let tf = Param.tf
    let h = Param.h
    let prm_nredraws = Param.prm_nredraws
end


module Data =
  struct
    type t = {
      length : int ;
      samples : (int * float * unit) list ;  (* sorted *)
      sequences : Seqs.t Intmap.t ;
      invariants : int Seqsim.Tree.Ntm.t ;
    }

    let read tseqs =
      let length = L.length tseqs in
      let sorted = L.sort (fun (t, _) (t', _) -> compare t' t) tseqs in
      let samples = L.mapi (fun k (t, _) -> (k + 1, t, ())) sorted in
      let sequences = L.fold_lefti (fun seqs i (_, seq) ->
        (* start from 1 *)
        let k = i + 1 in
        Intmap.add k seq seqs
      ) Intmap.empty sorted
      in
      let invariants, sequences =
        Seqsim.Base.split_invariant sequences
      in
      { length ; samples ; sequences ; invariants }

    let length { length ; _ } =
      length

    let sorted { samples ; _ } =
      L.map (fun (k, t, x) -> (k, F.Pos.of_float t, x)) samples

    let sequences { sequences ; _ } =
      sequences

    let invariants { invariants ; _ } =
      invariants
  end


module Prior =
  struct
    (* FIXME Allow a flat prior ?
       This would correspond to 1/X on popsize in BEAST ? *)
    let lambda hy =
      let m = log (Param.lambda_mean hy) /. log 10. in
      D.Lognormal (m, Param.lambda_var hy)

    let mu hy =
      let m = log (Param.mu_mean hy) /. log 10. in
      D.Lognormal (m, Param.mu_var hy)

    let prm data hy =
      let tf = F.Pos.of_float (Param.tf hy) in
      let samples = Data.sorted data in
      let draw rng =
        Sim.Backward.rand_prm ~rng ~samples ~tf
      in
      (* This function is not correct in general,
       * as in the course of simulation we merge colors etc.
       * However nu is persistent and we only ever call log_dens_proba
       * on a nu that behaves like we want it to,
       * that is, always in between simulations. *)
      let log_dens_proba nu =
        let logp = Prm.fold_points (fun c pt x ->
            match c with
            | Color.Merge _ ->
                let t = Point.time pt in
                x +. F.to_float (U.logd_exp F.one (F.to_float t))
            | Color.Mutation _ ->
                failwith "no mutations in kingman"
            | Color.Sample _ ->
                x
          ) nu 0.
        in [logp]
      in Fit.Dist.Base { draw ; log_dens_proba }
  end


module Propose =
  struct
    let lambda hy =
      Fit.Propose.endo_dist (fun x -> D.Normal (x, Param.lambda_jump hy))

    let mu hy =
      Fit.Propose.endo_dist (fun x -> D.Normal (x, Param.mu_jump hy))

    let add_to_point dx (pt : Point.t) : Point.t =
      match F.Pos.of_anyfloat (F.add (Point.time pt) (F.of_float dx)) with
      | t ->
          t
      | exception (Invalid_argument _ as e) -> (* negative *)
          Fit.raise_draw_error e

    (* affects nu in place *)
    let prm_draw_single nm1 dx rng nu =
      (* choose a random Merge point *)
      (* add a normal variable to its exponential variable *)
      let f (pt : Point.t) : Point.t =
        add_to_point dx pt
      in
      let j = I.Pos.succ (U.rand_int ~rng nm1) in
      let i = U.rand_int ~rng j in
      let color =
        Sim.Merge.color (I.to_int i + 1) (I.to_int j + 1)
      in
      Prm.modify_map color f nu

    let prm_all dx nu =
      (* most colors are merge colors, so we map *)
      let f c pt =
        match Color.kind c with
        | `Merge ->
            add_to_point dx pt
        | `Sample | `Mutation ->
            pt
      in
      Prm.map_points f nu

    (* draw physically equal *)
    let prm data hy =
      let v = Param.prm_jump hy in
      let n = Data.length data in
      let nm1 = I.Pos.of_int (n - 1) in
      let draw rng nu =
        let dx = U.rand_normal ~rng 0. v in
        prm_draw_single nm1 dx rng nu
      in
      let log_pd_ratio _ _ =
        (* assume they come from the proposal,
         * then it's 0. (by symmetry)
         * (otherwise it would be neg_infinity but it's annoying to prove it,
         * and it should never happen in the normal run of the MCMC)
         *)
        [0.]
      in
      let move_to_sample nu =
        nu
      in
      Fit.Propose.Base { draw ; log_pd_ratio ; move_to_sample }
  end


module Draws =
  struct
    (* add dx to a randomly chosen merge point *)
    let prm_single_random n infer =
      let nm1 = I.Pos.of_int (n - 1) in
      let f rng dx nu =
        Propose.prm_draw_single nm1 dx rng nu
      in
      if Fit.Infer.is_free_adapt infer then
        [f]
      else
        []

    (* list of draws where each draw k adds dx to all merge pairs with k *)
    let prm_pairs n infer =
      let draw dx k k' nu =
        if k = k' then
          nu
        else
          let color =
            Sim.Merge.color k k'
          in
          Prm.update_merge_point color dx nu
      in
      (* for a given lineage *)
      let f k _ dx nu =
        (* for all merge events on that lineage, add dx *)
        (* k' goes from 1 to n, <> k *)
        U.int_fold_right ~f:(fun k' nu ->
          draw dx k k' nu
        ) ~n nu
      in
      if Fit.Infer.is_free_adapt infer then
        U.int_fold_right ~f:(fun k draws ->
          f k :: draws
        ) ~n []
      else
        []

    (* list of draws where each draw k adds dx to a random merge pair with k *)
    let prm_pairs_random ~n ~n_redraw infer =
      (* the set of 1 to n *)
      let first_n = U.int_fold_right ~f:(fun k ->
          Intset.add k
        ) ~n Intset.empty
      in
      let draw k rng dx nu =
        let possible = Intset.remove k first_n in
        (* for n_redraw merge events on that lineage, add dx *)
        let selected =
          U.rand_subset
            (module Intset : BatSet.S with type elt = int
                                    and type t = Intset.t)
            ~rng
            possible
            n_redraw
        in
        Intset.fold (fun k' nu ->
          (* for all selected pairs (k, k') *)
          if k = k' then
            nu
          else
            let color =
              Sim.Merge.color k k'
            in
            Prm.update_merge_point color dx nu
        ) selected nu
      in
      if Fit.Infer.is_free_adapt infer then
        U.int_fold_right  ~f:(fun k draws ->
          draw k :: draws
        ) ~n []
      else
        []

    (* a draw that adds dx to all prm merge points *)
    let prm_all infer =
      let f _ dx nu =
        Propose.prm_all dx nu
      in
      if Fit.Infer.is_free_adapt infer then
        [f]
      else
        []

    module Scale =
      struct
        module D = Fit.Draw_lognormal

        let mu _ =
          D.draw
            ~get:(fun th -> th#mu)
            ~set:(fun th -> th#with_mu)

        let lambda _ =
          D.draw
            ~get:(fun th -> th#lambda)
            ~set:(fun th -> th#with_lambda)

        let prm_all _ dx nu =
          let alpha = F.exp (F.of_float dx) in
          let f c pt =
            match Color.kind c with
            | `Merge ->
                F.Pos.mul alpha pt
            | `Sample | `Mutation ->
                pt
          in
          Prm.map_points f nu

        module Ratio =
          struct
            let mu () =
              D.ratio ~get:(fun th -> th#mu)

            let lambda () =
              D.ratio ~get:(fun th -> th#lambda)

            let prm_all nu nu' =
              (* the "first" merge color *)
              let c = Color.merge () () () 0 1 in
              let get nu =
                F.to_float (Prm.find_single c nu)
              in
              D.ratio ~get nu nu'
          end
      end
  end


module Jumps =
  struct
    let prm_single_random j infer =
      if Fit.Infer.is_free_adapt infer then
        [j]
      else
        []

    let prm_pairs n j infer =
      if Fit.Infer.is_free_adapt infer then
        U.int_fold_right ~f:(fun _ js ->
          j :: js
        ) ~n []
      else
        []

    let prm_all j infer =
      if Fit.Infer.is_free_adapt infer then
        [j]
      else
        []
  end


module Likelihood =
  struct
    let sim ~t0 ~tf ~samples theta =
      let lambda = Param.lambda theta in
      let nu = Param.prm_internal theta in
      (* until copies nu for all changes *)
      let zf, _, _ = Sim.Backward.until ~lambda ~samples ~t0 ~tf nu in
      zf

    let loglik = Likelihood.loglik_constant

    let relabel = Likelihood.relabel

    let compute hy data =
      let t0 = F.Pos.of_float (Param.t0 hy) in
      let tf = F.Pos.of_float (Param.tf hy) in
      let samples = Data.sorted data in
      let sequences = Data.sequences data in
      let invs = Data.invariants data in
      let loglik = loglik sequences invs in
      let draw _ =
        failwith "Not_implemented"
      in
      let log_dens_proba th =
        match sim ~t0 ~tf ~samples th with
        | z ->
            [loglik th (Pop.to_forest z)]
        | exception (Pop.Already_merged _ as e) ->
            raise (Fit.Simulation_error e)
      in Fit.Dist.Base { draw ; log_dens_proba }
  end


module Infer =
  struct
    type t = {
      lambda : Fit.Infer.t ;
      mu : Fit.Infer.t ;
      prm : Fit.Infer.t ;
    }

    let default = {
      lambda = Fit.Infer.free `Adapt ;
      mu = Fit.Infer.free `Adapt ;
      prm = Fit.Infer.free `Adapt ;
    }

    let lambda hy infer =
      Fit.Infer.maybe_adaptive_float
        ~tag:`Lambda
        ~get:(fun th -> th#lambda)
        ~set:(fun th x -> th#with_lambda x)
        ~prior:(Prior.lambda hy)
        ~proposal:(Propose.lambda hy)
        ~jump:(F.to_float (Param.lambda_jump hy))
        infer

    let mu hy infer =
      Fit.Infer.maybe_adaptive_float
        ~tag:`Mu
        ~get:(fun th -> th#mu)
        ~set:(fun th x -> th#with_mu x)
        ~prior:(Prior.mu hy)
        ~proposal:(Propose.mu hy)
        ~jump:(F.to_float (Param.mu_jump hy))
        infer

    (* can't be used for adaptive draw, only for prior *)
    let prm data hy =
      Fit.Infer.maybe_adaptive_random
        ~tag:`Prm
        ~get:(fun th -> Prm.copy th#prm_internal)
        ~set:Param.with_prm_internal
        ~prior:(Prior.prm data hy)
        ~proposal:(Propose.prm data hy)
        ~jumps:[]
        ~log_pd_ratio:None

    let fix tag infer =
      match tag with
      | `Lambda ->
          { infer with lambda = Fit.Infer.fixed }
      | `Mu ->
          { infer with mu = Fit.Infer.fixed }
      | `Prm ->
          { infer with prm = Fit.Infer.fixed }

    let infer is_adapt tag infer =
      match tag with
      | `Lambda ->
          { infer with lambda = Fit.Infer.free is_adapt }
      | `Mu ->
          { infer with mu = Fit.Infer.free is_adapt }
      | `Prm ->
          { infer with prm = Fit.Infer.free is_adapt }
  end


module Estimate =
  struct
    type (_, _, _) elist =
      El : ('tag, 'theta, _, 'a) Fit.Param.List.t -> ('tag, 'theta, 'a) elist

    let columns =
      Fit.Csv.columns_sample_move @ [
        "mu" ;
        "lambda" ;
        "move_mu" ;
        "move_lambda" ;
      ]

    let th_extract th =
      let sof = string_of_float in
      function
      | "mu" ->
          sof th#mu
      | "lambda" ->
          sof th#lambda
      | _ ->
          failwith "unrecognized column"

    let extract k res =
      Some (Fit.Csv.extract_sample_move
        ~move_to_sample:(fun th -> th)
        ~extract:th_extract
        k res
      )

    let params_th hy (infer : Infer.t) =
      El Fit.Param.List.[
        Infer.lambda hy infer.lambda ;
        Infer.mu hy infer.mu ;
      ]

    let params_all data hy (infer : Infer.t) =
      let El pars = params_th hy infer in
      El Fit.Param.List.(
           Infer.prm data hy infer.prm
        :: pars
      )

    let sim_traj data hy th =
      let samples = Data.sorted data in
      let t0 = F.Pos.of_float (Param.t0 hy) in
      let tf = F.Pos.of_float (Param.tf hy) in
      Likelihood.sim ~t0 ~tf ~samples th

    let sim_mrcas data hy th =
      let n = Data.length data in
      let z = sim_traj data hy th in
      Pop.time_mrcas n z

    let sim_forest data hy th =
      let n = Data.length data in
      let z = sim_traj data hy th in
      let forest = Pop.to_forest z in
      Likelihood.relabel n forest

    let output_points chan nu =
      Prm.iter_points (fun color pt ->
        let t = Point.time pt in
        Printf.fprintf chan "%s,%f\n"
        (Color.to_string color) (F.to_float t)
      ) nu

    let convert_prm ?append ?(n_burn=0) ?(n_thin=1) path =
      ignore append ;
      let output k x =
        let chan = open_out (Printf.sprintf "%s.%i.prm.points.csv" path k) in
        let th = Fit.Mh.Return.sample x in
        output_points chan th#prm_internal ;
        close_out chan
      in
      let start k0 x0 =
        output k0 x0
      in
      let out k x =
        if (k >= n_burn) && (k mod n_thin = 0) then
          output k x
      in
      let return k x =
        (k, x)
      in
      U.Out.{ start ; out ; return }

    let vector_draw ~before ~after draws =
      let vd =
        Fit.Propose.vector_draw (A.of_list draws)
      in
      (fun rng v th ->
         th
         |> before
         |> vd rng v
         |> after
      )

    let prm_vector_draw draws =
      let vd =
        Fit.Propose.vector_draw (A.of_list draws)
      in
      (fun rng v th ->
         th
         |> Param.prm_internal
         |> Prm.copy
         |> vd rng v
         |> th#with_prm_internal
      )

    let prm_th_vector_draw prm_draws th_draws =
      let prm_vd = Fit.Propose.vector_draw
        (A.of_list prm_draws)
      in
      let th_vd = Fit.Propose.vector_draw
        ~start:(1 + L.length prm_draws)
        (A.of_list th_draws)
      in
      (fun rng v th ->
         th
         |> Param.prm_internal
         |> Prm.copy
         |> prm_vd rng v
         |> th#with_prm_internal
         |> th_vd rng v
      )

    let to_chol js =
      js
      |> A.of_list
      |> Lac.Vec.of_array
      |> Lac.Mat.of_diag

    let prior data hy infer =
      let El pars = params_all data hy infer in
      Fit.Param.List.prior (new Param.theta) pars

    let prop_th_mh
      hypar infer output prior likelihood =
      let El pars = params_th hypar infer in
      let proposal = Fit.Param.List.custom_proposal pars in
      Fit.Mh.Loop.proposal
        ~output
        (fun x -> x)
        prior
        proposal
        likelihood

    let prop_th_ram
      ?adapt_until
      hypar infer output prior likelihood =
      let El pars = params_th hypar infer in
      let draws = Fit.Param.List.adapt_draws pars in
      let draws_ratio = Fit.Param.List.log_pd_ratio pars in
      let chol =
        pars
        |> Fit.Param.List.adapt_jumps
        |> to_chol
      in
      let vdraw = vector_draw ~before:Fun.id ~after:Fun.id draws in
      Fit.Ram.Loop.proposal
        ~output
        ?draws_ratio
        ?kf:adapt_until
        (fun x -> x)
        (fun x -> x)
        (L.length draws)
        chol
        prior
        vdraw
        likelihood

    let prop_prm_single_ram
      ?adapt_until
      hy data (infer : Infer.t) output prior likelihood =
      let n = Data.length data in
      let draws = Draws.prm_single_random n infer.prm in
      let chol =
        to_chol (Jumps.prm_single_random hy#prm_jump infer.prm)
      in
      let vdraw = prm_vector_draw draws in
      Fit.Ram.Loop.proposal
        ~output
        ?kf:adapt_until
        (fun x -> x)
        (fun x -> x)
        (L.length draws)
        chol
        prior
        vdraw
        likelihood

    (* split it into more proposals *)
    let prop_prm_pairs_ram
      ?adapt_until
      hy data (infer : Infer.t) output prior likelihood =
      let n = Data.length data in
      let draws = Draws.prm_pairs n infer.prm in
      let chol =
        to_chol (Jumps.prm_pairs n hy#prm_jump infer.prm)
      in
      let vdraw = prm_vector_draw draws in
      Fit.Ram.Loop.proposal
        ~output
        ?kf:adapt_until
        (fun x -> x)
        (fun x -> x)
        (L.length draws)
        chol
        prior
        vdraw
        likelihood
     
    let prop_prm_pairs_redraw_ram
      ?adapt_until
      hy data (infer : Infer.t) output prior likelihood =
      let n_redraw = Param.prm_nredraws hy in
      let n = Data.length data in
      let draws = Draws.prm_pairs_random ~n_redraw ~n infer.prm in
      let chol =
        to_chol (Jumps.prm_pairs n hy#prm_jump infer.prm)
      in
      let vdraw = prm_vector_draw draws in
      Fit.Ram.Loop.proposal
        ~output
        ?kf:adapt_until
        (fun x -> x)
        (fun x -> x)
        (L.length draws)
        chol
        prior
        vdraw
        likelihood

    let prop_prm_all_ram
      ?adapt_until
      hy (infer : Infer.t) output prior likelihood =
      let draws = Draws.prm_all infer.prm in
      let chol =
        to_chol (Jumps.prm_all hy#prm_jump infer.prm)
      in
      let vdraw = prm_vector_draw draws in
      Fit.Ram.Loop.proposal
        ~output
        ?kf:adapt_until
        (fun x -> x)
        (fun x -> x)
        (L.length draws)
        chol
        prior
        vdraw
        likelihood

    let prop_prm_all_th_ram
      ?adapt_until
      hy (infer : Infer.t) output prior likelihood =
      let draws_prm = Draws.prm_all infer.prm in
      let El pars = params_th hy infer in
      let draws_th = Fit.Param.List.adapt_draws pars in
      let draws_ratio = Fit.Param.List.log_pd_ratio pars in
      (* jumps order matters *)
      let jumps =
          (Jumps.prm_all hy#prm_jump infer.prm)
        @ (Fit.Param.List.adapt_jumps pars)
      in
      let chol = to_chol jumps in
      let vdraw = prm_th_vector_draw draws_prm draws_th in
      Fit.Ram.Loop.proposal
        ~output
        ?draws_ratio
        ?kf:adapt_until
        (fun x -> x)
        (fun x -> x)
        (L.length jumps)
        chol
        prior
        vdraw
        likelihood

    let prop_scale_prm_all_th_ram
      ?adapt_until
      (infer : Infer.t) output prior likelihood =
      let module D = Fit.Draw_lognormal in
      let module DS = Draws.Scale in
      let draws_prm =
        D.if_adapt DS.prm_all infer.prm in
      let draws_th =
          (D.if_adapt DS.mu infer.mu)
        @ (D.if_adapt DS.lambda infer.lambda)
      in
      let ratio_prm =
        let r th th' =
          DS.Ratio.prm_all
            (Param.prm_internal th)
            (Param.prm_internal th')
        in
        D.if_adapt r infer.prm
      in
      let ratio_th =
          (D.if_adapt (DS.Ratio.mu ()) infer.mu)
        @ (D.if_adapt (DS.Ratio.lambda ()) infer.lambda)
      in
      let draws_ratio = D.fold_ratio (ratio_prm @ ratio_th) in
      (* jumps order matters *)
      let jumps =
          (L.map (fun _ -> 0.01) draws_prm)
        @ (L.map (fun _ -> 0.01) draws_th)
      in
      let chol = to_chol jumps in
      let vdraw = prm_th_vector_draw draws_prm draws_th in
      Fit.Ram.Loop.proposal
        ~output
        ~draws_ratio
        ?kf:adapt_until
        (fun x -> x)
        (fun x -> x)
        (L.length jumps)
        chol
        prior
        vdraw
        likelihood


    let proposal
      ?theta_every ?traj_every ?prm_every ?adapt_until
      ~path (hypar : Param.hyper) infer data =
      let sim_mrcas = sim_mrcas data hypar in
      let sim_forest = sim_forest data hypar in
      let output =
        Out.all_no_chol
          ?theta_every
          ?traj_every
          ?prm_every
          ~columns
          ~extract
          ~sim_mrcas
          ~sim_forest
          ~convert_prm
          (Data.length data)
          path
      in
      let El pars = params_all data hypar infer in
      let prior = Fit.Param.List.prior (new Param.theta) pars in
      let likelihood = Likelihood.compute hypar data in
      (* FIXME the prop should not be in the choices,
       * when it does nothing *)
      let prop_th_mh =
        prop_th_mh
          hypar
          infer
          output
          prior
          likelihood
      in
      let prop_th_ram =
        prop_th_ram
          ?adapt_until
          hypar
          infer
          output
          prior
          likelihood
      in
      let prop_prm_single_ram =
        prop_prm_single_ram
          ?adapt_until
          hypar
          data
          infer
          output
          prior
          likelihood
      in
      let prop_prm_pairs_ram =
        prop_prm_pairs_ram
          ?adapt_until
          hypar
          data
          infer
          output
          prior
          likelihood
      in
      let prop_prm_pairs_redraw_ram =
        prop_prm_pairs_redraw_ram
          ?adapt_until
          hypar
          data
          infer
          output
          prior
          likelihood
      in
      let prop_prm_all_ram =
        prop_prm_all_ram
          ?adapt_until
          hypar
          infer
          output
          prior
          likelihood
      in
      let prop_prm_all_th_ram =
        prop_prm_all_th_ram
          ?adapt_until
          hypar
          infer
          output
          prior
          likelihood
      in
      let prop_scale_prm_all_th_ram =
        prop_scale_prm_all_th_ram
          ?adapt_until
          infer
          output
          prior
          likelihood
      in
      let choices = [
        (prop_th_mh, hypar#proposal_weights.th) ;
        (prop_th_ram, hypar#proposal_weights.th) ;
        (prop_prm_single_ram, hypar#proposal_weights.prm_single) ;
        (prop_prm_pairs_ram, hypar#proposal_weights.prm_pairs) ;
        (prop_prm_pairs_redraw_ram, hypar#proposal_weights.prm_pairs_redraw) ;
        (prop_prm_all_ram, hypar#proposal_weights.prm_all) ;
        (prop_prm_all_th_ram, hypar#proposal_weights.prm_all_th) ;
        (prop_scale_prm_all_th_ram, hypar#proposal_weights.scale_prm_all_th) ;
      ]
      in
      let start k th =
        let smpl = Fit.Mh.Sample.weight prior likelihood th in
        let par_res = Fit.Mh.Return.accepted (fun x -> x) smpl in
        output.start k par_res ;
        smpl
      in
      let return k smplf =
        let res = Fit.Mh.Return.accepted (fun x -> x) smplf in
        output.return k res
      in
      Fit.Propose.Choice choices, start, return

    let posterior
      ?seed ?theta_every ?traj_every ?prm_every ?adapt_until
      ~path ~n_iter hypar infer tseqs theta =
      let data = Data.read tseqs in
      let proposal, start, return = proposal
        ?theta_every
        ?traj_every
        ?prm_every
        ?adapt_until
        ~path
        hypar
        infer
        data
      in
      let nu = Fit.Dist.draw_from (U.rng seed) (Prior.prm data hypar) in
      let theta' = theta#with_prm_internal nu in
      let smpl0 = start 0 theta' in
      let smplf = Fit.Markov_chain.direct_simulate
        ?seed
        proposal
        smpl0
        n_iter
      in
      return n_iter smplf
  end


module Term =
  struct
    module C = Cmdliner
    module UAC = U.Arg.Capture

    let theta0 () =
      let f mu lbd config capture =
        (new Param.theta)
        |> UAC.empty
        |> mu ~config
        |> lbd ~config
        |> UAC.output capture
      in
      C.Term.(const f
          $ Term.Theta0.mu ()
          $ Term.Theta0.lambda ()
          $ Term.Theta0.load_from
      )

    let hyper () =
      let f lbd_m lbd_v lbd_j mu_m mu_v mu_j t0 tf prm_j prm_nr pw config capture =
        (new Param.hyper)
        |> UAC.empty
        |> lbd_m ~config
        |> lbd_v ~config
        |> lbd_j ~config
        |> mu_m ~config
        |> mu_v ~config
        |> mu_j ~config
        |> t0 ~config
        |> tf ~config
        |> prm_j ~config
        |> prm_nr ~config
        |> pw
        |> UAC.output capture
      in
      C.Term.(const f
          $ Term.Hyper.lambda_mean ()
          $ Term.Hyper.lambda_var ()
          $ Term.Hyper.lambda_jump ()
          $ Term.Hyper.mu_mean ()
          $ Term.Hyper.mu_var ()
          $ Term.Hyper.mu_jump ()
          $ Term.Hyper.t0 ()
          $ Term.Hyper.tf ()
          $ Term.Hyper.prm_jump ()
          $ Term.Hyper.prm_nredraws ()
          $ Term.Hyper.proposal_weights ()
          $ Term.Hyper.load_from
      )

    let tags = [
      ("lambda", `Lambda) ;
      ("mu", `Mu) ;
      ("prm", `Prm) ;
    ]

    let infer_as ~doc ~name f tags =
      let docv = String.uppercase_ascii name in
      let arg = C.Arg.(value
           & opt_all (enum tags) []
           & info [name] ~docv ~doc
        )
      in
      let f tags =
        L.fold_right f tags
      in
      C.Term.(const f $ arg)

    let infer_adapt =
      let doc =
          "Set the specified parameter to be inferred "
        ^ "as part of the adaptive proposal."
      in
      infer_as ~doc ~name:"infer-adapt" (Infer.infer `Adapt) tags

    let infer_custom =
      let doc =
          "Set the specified parameter to be inferred "
        ^ "with its custom proposal."
      in
      infer_as ~doc ~name:"infer-custom" (Infer.infer `Custom) tags


    let infer_prior =
      let doc =
        "Set the specified parameter to be inferred "
      ^ "using its prior distribution as proposal."
      in
      infer_as ~doc ~name:"infer-prior" (Infer.infer `Prior) tags

    let fix =
      let doc =
        "Set the specified parameter to be fixed to the given value."
      in
      infer_as ~doc ~name:"fix" Infer.fix tags

    let infer =
      let f fix prior custom adapt =
        Infer.default
        |> fix
        |> prior
        |> custom
        |> adapt
      in
      C.Term.(const f
          $ fix
          $ infer_prior
          $ infer_custom
          $ infer_adapt
      )

    let run =
      let flat seed theta_every traj_every prm_every adapt_until
               path n_iter read_hypar infer txseqs read_theta =
        (* output niter *)
        U.Csv.output
          ~columns:["niter"]
          ~extract:(fun niter ->
            function
            | "niter" ->
                string_of_int niter
            | _ ->
                invalid_arg "unrecognized column"
          )
          ~path:(Printf.sprintf "%s.niter.csv" path)
          [n_iter]
        ;
        let capt_hy = Some (Printf.sprintf "%s.par.csv" path) in
        let capt_th = Some (Printf.sprintf "%s.theta0.csv" path) in
        let hypar = read_hypar capt_hy in
        let theta = read_theta capt_th in
        let tseqs = L.map (fun ((t, _), seq) -> (t, seq)) txseqs in
        U.time_it_to
          (Printf.sprintf "%s.duration.csv" path)
          (Estimate.posterior
            ?seed ?theta_every ?traj_every ?prm_every ?adapt_until
            ~path ~n_iter hypar infer tseqs)
          theta
      in
      C.Term.(const flat
          $ Term.seed
          $ Term.theta_every
          $ Term.traj_every
          $ Term.prm_every
          $ Term.adapt_until
          $ Term.path
          $ Term.n_iter
          $ hyper ()
          $ infer
          $ Term.sequences
          $ theta0 ()
      )

    let run_unit =
      C.Term.(const ignore $ run)

    let info_doc =
      "Estimate a phylogeny under Kingman's coalescent and JC69
       substitutions from timed DNA sequences."

    let info =
      let doc = info_doc in
      C.Term.info
        "coalfit-constant"
        ~version:"%%VERSION%%"
        ~doc
        ~exits:C.Term.default_exits
        ~man:[]

    let info_sub =
      let doc = info_doc in
      C.Term.info
        "simple"
        ~version:"%%VERSION%%"
        ~doc
        ~exits:C.Term.default_exits
        ~man:[]
  end

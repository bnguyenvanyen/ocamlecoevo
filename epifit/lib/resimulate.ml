open Sig


module Make
  (Model : Epi.Sig.MODEL_SPEC)
  (Hyper : HYPER)
  (Fit : FITSIM with type sim := Param.sim
                 and type hyper = Hyper.t) =
  struct
    module E = Episim.Make (Model)
    module M = Mcmc.Make (Hyper) (Fit)

    let run (type a b) (settings : (a, b, b, b) settings) hypar theta path =
      E.sim_accepted settings Fit.state_settings hypar theta path


    module Term =
      struct
        type e_settings = Es : (_, 'a, 'a, 'a) settings -> e_settings

        let path =
          let doc =
            "Resimulate results for '$(docv).theta.csv'."
          in
          Cmdliner.Arg.(
            required
            & pos 0 (some string) None
            & info [] ~docv:"PATH" ~doc
          )

        let settings =
          let doc =
            "Settings for simulation and inference"
          in
          let e = Cmdliner.Arg.enum [
            ("ode", Es (Ode Simple)) ;
            ("prm_exact", Es (Prm (Exact, No_intensities, Simple))) ;
            ("prm_approx", Es (Prm (Approx, No_intensities, Simple))) ;
          ]
          in
          Cmdliner.Arg.(
            value
            & opt e (Es (Ode Simple))
            & info ["settings"] ~docv:"SETTINGS" ~doc
          )

        let sim =
          let f sim_config fname =
            (new Param.sim)
            |> U.Arg.Capture.empty
            |> sim_config ~config:(Some fname) 
            |> U.Arg.Capture.output None
          in
          Cmdliner.Term.(
            const f
            $ Term.Hyper.lik ()
          )

        let theta =
          (* theta with all params with no capture *)
          let f th ~config =
            (new Episim.theta)
            |> U.Arg.Capture.empty
            |> th ~config
            |> U.Arg.Capture.output None
          in
          Cmdliner.Term.(
            const f
            $ Term.Theta.update_full ~latency:Model.spec.latency
          )

        let run =
          let flat stgs hyper_config theta_config path =
            let Es settings = stgs in
            let load_hy_path = Printf.sprintf "%s.par.csv" path in
            let hypar = hyper_config load_hy_path in
            (* FIXME theta reading *)
            (* let load_th_path = Some (Printf.sprintf "%s.theta.csv" path) in *)
            let load_th_path = None in
            let theta = theta_config ~config:load_th_path in
            run settings hypar theta path
          in
          Cmdliner.Term.(
            const flat
            $ settings
            $ sim
            $ theta
            $ path
          )

        let info =
          let doc =
            "Resimulate the " ^ Fit.model_name
            ^ " model from accepted MCMC samples."
          in
          Cmdliner.Term.info
            ("epifit-sim" ^ Fit.model_name)
            ~version:"%%VERSION%%"
            ~doc
            ~exits:Cmdliner.Term.default_exits
            ~man:[]
      end
  end

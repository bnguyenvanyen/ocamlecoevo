

type base_pars = [
  | `Beta
  | `Nu
  | `Rho
]


type state = [
  | `Sir
  | `I0
]


type base = [ base_pars | state ]


type latent = [
  | `Sigma
]


type circular = [
  | `Betavar
  | `Phase
  | `Gamma
  | `Eta
]


type stochastic = [
  | `Stoch
]

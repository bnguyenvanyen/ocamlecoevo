open Sig

let assert_synchro ?(tol=1e-5) t t' =
  match assert (abs_float (t' -. t) < tol) with
  | () ->
      ()
  | exception (Assert_failure _ as e) ->
      begin
        Printf.eprintf "|%f - %f| > tol\n" t' t ;
        raise e
      end


let bound_down ?(min=F.Neg.of_float (~-. 1000.)) x =
  F.Neg.max min x


(* Because of the way we output (Sim.Csv and Sim.Cadlag),
 * it sometimes happens that we miss one point on one side.
 * Since I'm not sure how to best fix that, I propose to ignore the problem
 * (the missing point does not participate to the likelihood) *)
let map_times ?tol f a b =
  (* we don't really need tail-rec here *)
  let rec loop txs tys =
    match txs, tys with
    | [], []
    (* it is allowed to miss a point at the end *)
    | _ :: [], []
    | [], _ :: [] ->
        []
    (* but not more than one *)
    | [], _
    | _, [] ->
        invalid_arg "map_times: lists don't correspond"
    | (t, x) :: xtl, (s, y) :: ytl ->
        assert_synchro ?tol t s ;
        (f x y) :: (loop xtl ytl)
  in loop (J.to_list a) (J.to_list b)


(* Likelihood for observed case count data.
 * Observations can be Poisson or negative binomial.
 * If [rate], the total case reporting rate cadlag starts with times
 * (t_0, t_1, t_2, ...),
 * then [cases], the case counts, should start with (t_1, t_2, ...).
 *)
let reported_case ~(obs : [`Poisson | `Negbin]) hypar cases =
  let logp =
    match obs with
    | `Poisson ->
        Pre.logp_poisson
    | `Negbin ->
        let ovd = Param.cases_over_disp hypar in
        Pre.logp_negbin_over ovd
  in
  let rep (t, binned_reportr) =
    let c = J.eval cases (F.to_float t) in
    bound_down (logp binned_reportr c)
  in rep


let reported_cases ~(obs : [`Poisson | `Negbin]) hypar cases =
  let dt = hypar#dt in
  let tol = hypar#dt /. 100. in
  let logp =
    match obs with
    | `Poisson ->
        Pre.logp_poisson
    | `Negbin ->
        let ovd = Param.cases_over_disp hypar in
        Pre.logp_negbin_over ovd
  in
  (* compute loglik from the rate trajectory from simulation *)
  let rep rate =
    (* integrate over intervals *)
    let cumul_rate =
      rate
      |> J.Regular.of_cadlag ~tol ~dt
      |> J.Regular.Pos.primitive
    in
    let _, logps = J.fold_left (fun (t, logps) t' c ->
        let c_r = J.Regular.Pos.eval cumul_rate t in
        let c_r' = J.Regular.Pos.eval cumul_rate t' in
        (* if c_r ~= c_r', the difference might be < 0 (but small),
         * which would be a problem for F.Pos.of_anyfloat,
         * so we use F.positive_part instead *)
        let bin_r = F.positive_part F.Op.(c_r' - c_r) in
        (t', F.to_float (bound_down (logp bin_r c)) :: logps)
      ) (J.Regular.t0 cumul_rate, []) cases
    in logps
  in rep


let reported_prevalence
  ~(obs : [`Prevalence | `Prevalence_negbin]) hypar prev =
  let logp =
    match obs with
    | `Prevalence ->
        (fun rho i c -> Pre.logp_binomial i rho c)
    | `Prevalence_negbin ->
        let ovd = Param.cases_over_disp hypar in
        (fun rho i c ->
          Pre.logp_negbin_over
            ovd
            (F.Pos.mul rho (I.Pos.to_float i))
            c
        )
  in
  let rep rho i_traj =
    try
      map_times (fun i c ->
        F.to_float (bound_down (logp rho i c))
      ) i_traj prev
    with Invalid_argument _ as e ->
      begin
        Printf.eprintf
        "i_traj goes from %f to %f\nprev goes from %f to %f\n"
        (i_traj |> J.hd |> J.time)
        (i_traj |> J.last |> J.time)
        (prev |> J.hd |> J.time)
        (prev |> J.last |> J.time)
        ;
        raise e
      end
  in rep


(* Likelihood for transversal non age-stratified seroprevalence data.
 * A transversal sero-epidemiology tells us that out of [n_cohort]
 * people in the cohort, [n_seropos] tested seropositive at time [t].
 * Given that in the full (infinite) population,
 * a proportion [p_t] is seropositive at time [t],
 * the likelihood of observing [n_seropos / n_cohort] is
 * a binomial [P(N = n_seropos)] where [N ~ B(n_cohort, p_t)].
 * TODO take into account false-negative and false-positive
 *)
let aggregate_sero seros =
  let obs removed_proportion =
    L.fold_left (fun logps (t, n_seropos, n_cohort) ->
      let p_t = J.eval removed_proportion t in
      let logp = U.logp_binomial n_cohort p_t n_seropos in
      F.to_float (bound_down logp) :: logps
    ) [] seros
  in obs


(* Likelihood for transversal age-stratified seroprevalence data.
 * The data is a list of [(age, n_seropos_age, n_cohort_age)] for time [t].
 * Given the forces of infection [lambdas] from [t - amax] to [t],
 * (considered constant on each subinterval)
 * the likelihood is given by integrating linear systems
 *)
(* Otherwise I can integrate with the exponential of the matrix.
 * The matrix is diagonalizable :
 * delta = lambda ** 2 - 2 * lambda * nu - 2 * lambda * gamma + nu ** 2 - 2 * nu * gamma + gamma ** 2
 * D = [
 *   [0, 0, 0],
 *   [0, ~- (lambda + nu + gamma) / 2 - sqrt delta, 0]
 *   [0, 0, ~- (lambda + nu + gamma) / 2 + sqrt delta]
 * ]
 * P = [
 *   [ gamma / lambda, 2 * gamma / (lambda - nu - gamma - sqrt delta), 2 * gamma / (lambda - nu - gamma + sqrt delta) ],
 *   [ gamma / nu, ~- 2 * lambda / (lambda + nu - gamma - sqrt delta), ~- 2 * lambda / (lambda + nu - gamma + sqrt delta) ],
 *   [ 1, 1, 1]
 * ]
 *)
let sero_p_age t_sero gamma lambdas a =
  let e = L.fold_left (fun x (t, lambda) ->
    (* TODO Or should this be '>=' ? *)
    if F.Op.(t > t_sero - a) then
      F.Op.(x - lambda - gamma)
    else
      x
  ) F.zero lambdas
  in
  let e' t' =
    L.fold_left (fun x (t, lambda) ->
      (* TODO Or should this be '>' ? *)
      if F.Op.(t > t') then 
        F.Op.(x - lambda - gamma)
      else
        x
    ) F.zero lambdas
  in
  let b = F.Op.(gamma * L.fold_left (fun x (t, _) ->
    (* TODO Or should this be '>=' ? *)
    if (t > t_sero - a) && (t < t_sero) then
      x + F.exp (e' t)
    else
      x
  ) F.zero lambdas)
  in
  let p = F.Op.(F.one - F.exp e - b) in
  F.Proba.of_anyfloat p


let stratified_sero _ (t_sero, seros) =
  let obs (gamma, lambdas) =
    (* TODO I need to check this formula from Tan et al
     * because it seems a bit suspicious *)
    L.fold_left (fun logp (a, n_seropos, n_a) ->
      let p = sero_p_age t_sero gamma lambdas a in
      F.Neg.add logp (U.logp_binomial n_a p n_seropos)
    ) F.zero seros
  in obs

module A = BatArray
module L = BatList
module String = BatString

module Lac = Lacaml.D

module U = Util
module F = U.Float
module I = U.Int
module D = Fit.Dist
module J = Jump


(** Depending on the settings of simulation and inference,
    we need different types for the population,
    the variables used to simulate the system,
    the proposals on the prm,
    and the augmented state carried through iterations.
    The [settings] GADT ensures that this is all well formed.
  
    We can proceed on a continuous population with a deterministic
    simulation, or on a discrete population with a stochastic simulation, etc.
 *)


type continuous_pop = Lac.Vec.t
type discrete_pop = Epi.Pop.Unit.t

type intensities = U._float Epi.Point.Colormap.t Sim.Prm.Fm.t

type ode_sim = unit
type exact_sim = Epi.Prm_time.t
type approx_sim = Epi.Prm_color.t
type fast_sim = Epi.Prm_cheat.t


type _ pop =
  | Continuous : continuous_pop pop
  | Discrete : discrete_pop pop


type _ prm_sim_mode =
  | Exact : exact_sim prm_sim_mode
  | Approx : approx_sim prm_sim_mode
  | Fast : fast_sim prm_sim_mode


type _ sim_mode =
  | Ode : unit sim_mode
  | Sde : Sim.Dbt.t sim_mode
  | Prm : 'a prm_sim_mode -> 'a sim_mode


type (_, _) propose_prm =
  | No_intensities : ('a, 'a) propose_prm
  | Intensities : ('a, 'a * intensities) propose_prm


type (_, _) sequential =
  | Simple : ('a, 'a) sequential
  | Sequential : ('a, 'a list) sequential
  | Seq_elt : ('a, 'a) sequential


type ('pop, 'sim, 'prop, 'seq) settings =
  | Ode : (unit, 'a) sequential ->
      (continuous_pop, unit, unit, 'a) settings
  | Sde : (Sim.Dbt.t, 'a) sequential ->
      (continuous_pop, Sim.Dbt.t, Sim.Dbt.t, 'a) settings
  | Prm : 'a prm_sim_mode * ('a, 'b) propose_prm * ('b, 'c) sequential ->
      (discrete_pop, 'a, 'b, 'c) settings


type ('sim, 'prop, 'seq) prm_settings =
  (discrete_pop, 'sim, 'prop, 'seq) settings


type ('sim, 'prop, 'seq) cont_settings =
  (continuous_pop, 'sim, 'prop, 'seq) settings


type sir = {
  s : float ;
  i : float ;
  r : float ;
  ps : float ;
  pi : float ;
  pr : float ;
}


type seir = {
  s : float ;
  e : float ;
  i : float ;
  r : float ;
}


type _ state_settings =
  | Sir : sir state_settings
  | Seir : seir state_settings


type (_, _) elist =
  El : ('tag, 'theta, _, _) Fit.Param.List.t -> ('tag, 'theta) elist


type data = {
  cases : int J.cadlag option ;
  (** number of cases on each time interval *)
  prevalence : int J.cadlag option ;
  (** prevalence counted at each time *)
  sero_agg : (float * int * U.anyposint) list option ;
}


type likelihood_settings = {
  cases : [`None | `Poisson | `Negbin | `Prevalence | `Prevalence_negbin] ;
  sero : [`None | `Aggregate | `Stratified] ;
  alpha : float option ;
}


type proposal_weights = {
  joint : U.closed_pos ;
  par_state : U.closed_pos ;
  scale_par_state : U.closed_pos ;
  stoch : U.closed_pos ;
}

(** state for sequential simulation : a time and a population state *)
type 'pop seq_state = U._float * 'pop

(** observations for sequential simulation :
    a time and a binned reporting rate *)
type seq_obs = U._float * U.closed_pos


module type HYPER =
  sig
    (** the type of the hyper parameters used for the inference *)
    type t

    (** [t0 hypar] is the time to start simulations at. *)
    val t0 : t -> U._float option

    (** [tf hypar] is the time to end simulations at. *)
    val tf : t -> U._float

    (** [dt hypar] is the time between datapoints. *)
    val dt : t -> _ U.pos

    (** [width hypar] is the maximum step size of simulations. *)
    val width : t -> _ U.pos

    (** [n_best_of hypar] is the number of times to repeat simulations
        per iteration in sequential mode. *)
    val n_best_of : t -> int

    (** [seq_p_redraw_stoch hypar] is the probability to redraw
        the stoch component for a given step of an iteration in Seqram mode *)
    val seq_p_redraw_stoch : t -> _ U.proba

    (** [proposal_weights hypar] is the weights for the proposals *)
    val proposal_weights : t -> proposal_weights

    (** [(eval term) capture] reads a [t] value,
        and optionally captures the values read to the file at [capture]. *)
    val term : (string option -> t) Cmdliner.Term.t
  end


module type FITSIM =
  sig
    (** hyper-parameters for simulation -- 
        to be replaced by Param.sim at usesite *)
    type sim

    (** hyper-parameters *)
    type hyper

    (** parameters (including those to infer) *)
    type theta

    (** parameters to simulate the system *)
    type par

    (** state (sir or seir) *)
    type state

    (** what parameters to infer and how to infer them *)
    type infer

    (** identifying parameters *)
    type tag

    (** all parameter types corresponding to tags *)
    type 'a par_enum

    (** all parameter state types corresponding to tags *)
    type 'a state_enum

    (** [model_name] identifies the model with a name. *)
    val model_name : string

    val n_events : int

    val state_settings : state state_settings

    (** how to translate between Dbt dimensions and event names for Sde *)
    val color_namer : (string -> int) * (int -> string)

    val hyper_to_sim : hyper -> sim

    (** [to_par theta] are the parameters corresponding to [theta]
        for sequential simulations. *)
    val to_par : theta -> par

    val to_state : theta -> state

    val pop_to_state : 'pop pop -> 'pop -> state

    val state_to_pop : 'pop pop -> state -> 'pop 

    (** [default_infer] are the default inference settings *)
    val default_infer : infer

    (** [infer_tags] are the tags for all possible parameters to infer. *)
    val infer_tags : tag list

    (** [infer_gather filter infer] is the list of tags of [infer],
        filtered by [filter]. *)
    val infer_gather : (Fit.Infer.t -> bool) -> infer -> tag list

    (** [tag_to_string tag] identifies the parameter [tag]
        on the command line *)
    val tag_to_string : tag -> string

    (** [tag_to_columns tag] are the columns corresponding
        to the parameter [tag] for CSV reading and writing. *)
    val tag_to_columns : tag -> string list

    (** [infer mode tag inf] is [inf] with the parameter [tag]
        set to be inferred with mode [mode]. *)
    val infer : [`Custom | `Adapt | `Prior] -> tag -> infer -> infer

    (** [fix tag inf] is [inf] with the parameter [tag] set to be fixed. *)
    val fix : tag -> infer -> infer

    (** [infer_stochastic infer] is the inference mode
        for the stochastic component *)
    val infer_stochastic : infer -> Fit.Infer.t

    (** [params infer hypar] is the inference strategy for [theta]. *)
    val params :
      infer ->
      hyper ->
        (tag, par, 'a par_enum, 'a) Fit.Param.List.t

    val params_state :
      infer ->
      hyper ->
        (tag, state, 'a state_enum, 'a) Fit.Param.List.t
       
    (** [param_stoch inf stgs hy] is the inference strategy for the
        stochastic component of the model --
        either the PRM, or Brownian motion (or nothing) *)
    val param_stoch :
      infer ->
      (_, _, _, 'payload) settings ->
      hyper ->
        tag * 'payload Fit.Param.t

    (** [par_state_scale_draws infer hypar] are rescaling draws *)
    val par_state_scale_draws :
      infer ->
      hyper ->
        (U.rng -> float -> par * state -> par * state) list

    val par_state_scale_draws_ratio :
      infer ->
      hyper ->
        (par * state -> par * state -> float list) option

    (** [simulate stgs hy th nu] is the reporting probability
        and simulated trajectory, used in simple mode. *)
    val simulate :
      (_, 'sim, _, _) settings ->
      hyper ->
      par ->
      state ->
      'sim ->
        _ U.proba * Epi.Traj.t

    (** [simulate_csv ~chan stgs hy th nu] outputs the simulated trajectory
     *  to [chan], used in simple mode. *) 
    val simulate_csv :
      chan : out_channel ->
      (_, _, _, 'payload) settings ->
      hyper ->
      par ->
      state ->
      'payload ->
        unit

    (** [simulate_seq stgs hy p nu tx] simulates from [tx] for [nu],
        used in sequential mode. *)
    val simulate_seq :
      ('pop, _, 'a, 'a list) settings ->
      hyper ->
      par ->
      'a ->
      'pop seq_state ->
        'pop seq_state

    (** [obs_seq stgs hy p tx tx'] are the observations for states [tx]
        and [tx'], used in sequential mode. *)
    val obs_seq :
      ('pop, _, _, _) settings ->
      hyper ->
      par ->
      'pop seq_state ->
      'pop seq_state ->
        seq_obs

    (** [extract theta column] is the value of [column] in [theta]. *)
    val extract :
      par ->
      string ->
        string

    (** [extract_state sir column] is the value of [column] in [sir]. *)
    val extract_state :
      state ->
      string ->
        string

    (** [(eval term) capture] reads a [theta] value,
        and optionally captures the values read to the file at [capture]. *)
    val term : (string option -> theta) Cmdliner.Term.t
  end

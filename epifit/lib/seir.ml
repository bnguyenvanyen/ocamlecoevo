open Sig

type theta = Episim.theta


class hyper_prior = object
  inherit Param.prior_base
  inherit Param.prior_stochastic
  inherit Param.prior_latent
end


class hyper_prop = object
  inherit Param.prop_base
  inherit Param.prop_stochastic
  inherit Param.prop_latent
end


class hyper = object
  inherit hyper_prior
  inherit hyper_prop
  inherit Param.sim
end


class infer_pars_ode = object
  inherit Infer.base
  inherit Infer.latent
end


class infer_pars = object
  inherit infer_pars_ode
  inherit Infer.stochastic
end


type tag = [
  | Tag.base
  | Tag.latent
  | Tag.stochastic
]


module S = Episim.Make (Epi.Seir.Model)


module Hyper =
  struct
    type t = hyper

    let t0 = Param.t0
    let tf = Param.tf
    let dt = Param.dt
    let width = Param.width
    let n_best_of = Param.n_best_of
    let seq_p_redraw_stoch = Param.seq_p_redraw_stoch
    let proposal_weights = Param.proposal_weights

    let capture_term =
      let f base latent lik stochastic load_theta ~config =
        (new hyper)
        |> U.Arg.Capture.empty
        |> base load_theta ~config
        |> latent ~config
        |> lik ~config
        |> stochastic ~config
      in Cmdliner.Term.(
          const f
        $ Term.Hyper.base ()
        $ Term.Hyper.latent ()
        $ Term.Hyper.lik ()
        $ Term.Hyper.stochastic ()
      )

    let term =
      let f read load_theta config capture =
        U.Arg.Capture.output capture (read load_theta ~config)
      in
      Cmdliner.Term.(
        const f
        $ capture_term
        $ Term.Theta.load_from
        $ Term.Hyper.load_from
      )
  end


module Fit =
  struct
    type nonrec hyper = hyper

    type nonrec theta = theta

    type par = Epi.Param.t_unit

    type state = seir

    type infer = infer_pars

    type nonrec tag = tag


    let state_settings = Seir

    let model_name = S.model_name

    let dims = S.dims

    let n_events = S.n_events

    let color_namer = S.Cont.color_namer

    let hyper_to_sim hy = (hy :> Param.sim)

    let to_state th =
      S.to_state state_settings th

    let state_to_pop x =
      S.state_to_pop state_settings x

    let pop_to_state z =
      S.pop_to_state state_settings z

    let to_par (th : theta) =
      (th :> par)

    let load_theta = S.load_theta

    let default_theta = new Episim.theta

    let default_infer = new infer_pars

    let infer_tags = Symbols.(base @ latent @ stochastic)

    let infer_gather f infer =
         (Infer.gather_base f infer)
      @@ (Infer.gather_latent f infer)
      @@ (Infer.gather_stochastic f infer)
      @@ []

    let tag_to_string = Symbols.Write.any

    let tag_to_columns = Symbols.Write.columns

    let infer mode =
      function
      | (#Tag.base | #Tag.stochastic) as tag ->
          Sir.Fit.infer mode tag
      | #Tag.latent as tag ->
          Infer.infer_latent mode tag

    let fix =
      function
      | (#Tag.base | #Tag.stochastic) as tag ->
          Sir.Fit.fix tag
      | #Tag.latent as tag ->
          Infer.fix_latent tag

    let infer_stochastic infer =
      infer#stoch

    type 'a par_enum =
      float * (
        float * (
          float * (
            float * 'a)))

    type 'a state_enum =
      state * 'a

    let params infer hy =
      Fitsim.(Fit.Param.List.[
        beta hy infer#beta ;
        sigma hy infer#sigma ;
        nu hy infer#nu ;
        rho hy infer#rho ;
      ])

    let params_state infer hy =
      Fit.Param.List.[Fitsim.seir Fun.id Fun.id hy infer#sir]

    let param_stoch
      (type a b c d)
      (infer : infer)
      (settings : (a, b, c, d) settings)
      hy =
      Fitsim.param_stoch
        settings
        n_events
        S.E.Prm.color_groups
        hy
        infer#stoch

    let par_state_scale_draws infer hyper =
      let par_draws =
          (Draw.if_adapt Draw.beta infer#beta)
        @ (Draw.if_adapt Draw.nu infer#nu)
        @ (Draw.if_adapt Draw.rho infer#rho)
      in
      let state_draws =
         Draw.seir infer#sir hyper
      in
        (L.map Draw.left par_draws)
      @ (L.map Draw.right state_draws)

    let par_state_scale_draws_ratio infer hyper =
      let par_ratios =
          (Draw.if_adapt (Draw.Ratio.beta ()) infer#beta)
        @ (Draw.if_adapt (Draw.Ratio.nu ()) infer#nu)
        @ (Draw.if_adapt (Draw.Ratio.rho ()) infer#rho)
      in
      let state_ratios =
        Draw.Ratio.seir infer#sir hyper
      in
      Some (Draw.Ratio.fold (
          (L.map Draw.Ratio.left par_ratios)
        @ (L.map Draw.Ratio.right state_ratios)
      ))

    let simulate settings =
      S.sim_simple state_settings settings

    let simulate_csv ~chan settings =
      S.csv_sim state_settings settings ~chan

    let simulate_seq = S.sim_seq

    let obs_seq = S.obs_seq

    let extract th =
      let sof = string_of_float in
      function
      | "sigma" ->
          sof th#sigma
      | s ->
          Sir.Fit.extract th s

    let extract_state seir0 =
      let sof = string_of_float in
      function
      | "s0" ->
          sof seir0.s
      | "e0" ->
          sof seir0.e
      | "i0" ->
          sof seir0.i
      | "r0" ->
          sof seir0.r
      | s ->
          failwith (Printf.sprintf "unrecognized column %s" s)

    let term =
      let f base latent sir config capture =
        (new Episim.theta)
        |> U.Arg.Capture.empty
        |> base ~config
        |> latent ~config
        |> sir ~config
        |> U.Arg.Capture.output capture
      in Cmdliner.Term.(
        const f
        $ Epi.Term.Par.base ()
        $ Epi.Term.Par.latent ()
        $ Term.Theta.init_sir ~latency:Epi.Seir.Model.spec.latency
        $ Term.Theta.load_from
      )
  end

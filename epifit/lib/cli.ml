open Sig


let likelihood_symbols =
  [
    "cases_none" ;
    "cases_poisson" ;
    "cases_negbin" ;
    "cases_prevalence" ;
    "cases_prevalence_negbin" ;
    "sero_none" ;
    "sero_aggregate" ;
  ]


let float_from row colname =
  float_of_string (Csv.Row.find row colname)


let int_from row colname =
  int_of_string (Csv.Row.find row colname)


let load_cases_data path =
  Epi.Data.load_cases path


let load_prevalence_data path =
  let f =
    function
    | "t" ->
        Some (fun s (_, c) -> (float_of_string s, c))
    | "prevalence" ->
        Some (fun s (t, _) -> (t, int_of_float (float_of_string s)))
    | _ ->
        None
  in
  match U.Csv.read_all_and_fold ~f ~path (0., 0) with
  | res ->
      Some (J.of_list res)
  | exception Sys_error _ ->
      None


let load_sero_data path =
  let f =
    function
    | "t" ->
        Some (fun s (_, n_seropos, n_cohort) ->
          (float_of_string s, n_seropos, n_cohort)
        )
    | "seropos" ->
        Some (fun s (t, _, n_cohort) ->
          (t, int_of_string s, n_cohort)
        )
    | "cohort" ->
        Some (fun s (t, n_seropos, _) ->
          (t, n_seropos, U.Option.some (I.Pos.of_string s))
        )
    | _ ->
        None
  in
  match U.Csv.read_all_and_fold ~f ~path (0., 0, I.zero) with
  | res ->
      Some res
  | exception Sys_error _ ->
      None


let load_data path_root =
  let fname_cases = path_root ^ ".data.cases.csv" in
  let fname_prev = path_root ^ ".data.prevalence.csv" in
  let fname_sero = path_root ^ ".data.sero.csv" in
  let cases = load_cases_data fname_cases in
  let prevalence = load_prevalence_data fname_prev in
  let sero_agg = load_sero_data fname_sero in
  Data.{
    cases ;
    prevalence ;
    sero_agg ;
  }


let tag_lik (lik : Likelihood.settings) =
  function
  | "cases_none" ->
      { lik with cases = `None }
  | "cases_poisson" ->
      { lik with cases = `Poisson }
  | "cases_negbin" ->
      { lik with cases = `Negbin }
  | "cases_prevalence" ->
      { lik with cases = `Prevalence }
  | "cases_prevalence_negbin" ->
      { lik with cases = `Prevalence_negbin }
  | "sero_none" ->
      { lik with sero = `None }
  | "sero_aggregate" ->
      { lik with sero = `Aggregate }
  | _ ->
      raise (Arg.Bad "invalid symbol")


let change_lik_alpha lik x =
  Likelihood.({ lik with alpha = Some x })

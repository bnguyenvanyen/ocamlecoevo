open Sig


module type EPI_PRM_FIT = (
  Simfit.Sig.PRM
    with type P.color = Epi.Color.t
     and type 's P.Point.t = (Epi.Color.t, 's) Sim.Prm.point
     and module P.Point.Color = Epi.Point.Color
     and module P.Point.Colormap = Epi.Point.Colormap
)


module Time = Simfit.Prm.Make (Epi.Prm_time)

module Color = Simfit.Prm.Make (Epi.Prm_color)

module Cheat = Simfit.Prm.Make (Epi.Prm_cheat)


let of_prm_sim_mode : type a. a prm_sim_mode ->
  (module EPI_PRM_FIT with type P.t = a) =
  function
  | Exact ->
      (module Time : EPI_PRM_FIT with type P.t = Epi.Prm_time.t)
  | Approx ->
      (module Color : EPI_PRM_FIT with type P.t = Epi.Prm_color.t)
  | Fast ->
      (module Cheat : EPI_PRM_FIT with type P.t = Epi.Prm_cheat.t)

module I = Fit.Infer

type infer = I.t

let custom = I.custom
let adapt = I.adapt
let free = I.free
let fixed = I.fixed


class base_pars = object
  val betav = adapt
  val nuv = adapt
  val rhov = adapt

  method beta = betav
  method nu   = nuv
  method rho  = rhov

  method with_beta x = {< betav = x >}
  method with_nu x   = {< nuv = x >}
  method with_rho x  = {< rhov = x >}
end


class state = object
  val sirv : [ `All of I.t | `Only_i of I.t ] = `All adapt

  method sir  = sirv

  method with_sir x  = {< sirv = x >}
end


class base = object
  inherit base_pars
  inherit state
end


class latent = object
  val sigmav = adapt
  
  method sigma = sigmav

  method with_sigma x = {< sigmav = x >}
end


class circular = object
  val betavarv = adapt
  val phasev = adapt
  val gammav = adapt
  val etav = adapt

  method betavar = betavarv
  method phase   = phasev
  method gamma   = gammav
  method eta     = etav

  method with_betavar x = {< betavarv = x >}
  method with_phase x = {< phasev = x >}
  method with_gamma x = {< gammav = x >}
  method with_eta x = {< etav = x >}
end


class stochastic = object
  val stoch_v = fixed
 
  method stoch = stoch_v

  method with_stoch m = {< stoch_v = m >}
end


let fix_base_pars =
  function
  | `Beta ->
      (fun infr -> infr#with_beta fixed)
  | `Nu ->
      (fun infr -> infr#with_nu fixed)
  | `Rho ->
      (fun infr -> infr#with_rho fixed)


let fix_state =
  function
  | `Sir ->
      (fun infr -> infr#with_sir (`All fixed))
  | `I0 ->
      (fun infr -> infr#with_sir (`Only_i fixed))


let fix_base =
  function
  | #Tag.base_pars as tag ->
      fix_base_pars tag
  | #Tag.state as tag ->
      fix_state tag


let fix_latent =
  function
  | `Sigma ->
      (fun infr -> infr#with_sigma fixed)


let fix_circular =
  function
  | `Betavar ->
      (fun infr -> infr#with_betavar fixed)
  | `Phase ->
      (fun infr -> infr#with_phase fixed)
  | `Gamma ->
      (fun infr -> infr#with_gamma fixed)
  | `Eta ->
      (fun infr -> infr#with_eta fixed)


let fix_stochastic =
  function
  | `Stoch ->
      (fun infr -> infr#with_stoch fixed)


let infer_base_pars is_adapt symb infer =
  match symb with
  | `Beta ->
      infer#with_beta (free is_adapt)
  | `Nu ->
      infer#with_nu (free is_adapt)
  | `Rho ->
      infer#with_rho (free is_adapt)


let infer_state is_adapt symb infer =
  match symb with
  | `Sir ->
      infer#with_sir (`All (free is_adapt))
  | `I0 ->
      infer#with_sir (`Only_i (free is_adapt))


let infer_base is_adapt symb infer =
  match symb with
  | #Tag.base_pars as tag ->
      infer_base_pars is_adapt tag infer
  | #Tag.state as tag ->
      infer_state is_adapt tag infer


let infer_latent is_adapt symb infer =
  match symb with
  | `Sigma ->
      infer#with_sigma (free is_adapt)


let infer_circular is_adapt symb infer =
  match symb with
  | `Betavar ->
      infer#with_betavar (free is_adapt)
  | `Phase ->
      infer#with_phase (free is_adapt)
  | `Gamma ->
      infer#with_gamma (free is_adapt)
  | `Eta ->
      infer#with_eta (free is_adapt)


let infer_stochastic is_adapt symb infer =
  match symb with
  | `Stoch ->
      infer#with_stoch (free is_adapt)


let gather_base_pars is_that_infer infers =
  let f_beta tags =
    if is_that_infer infers#beta then
      `Beta :: tags
    else
      tags
  in
  let f_nu tags =
    if is_that_infer infers#nu then
      `Nu :: tags
    else
      tags
  in
  let f_rho tags =
    if is_that_infer infers#rho then
      `Rho :: tags
    else
      tags
  in
  let f tags =
    f_beta @@ f_nu @@ f_rho @@ tags
  in f


let gather_state is_that_infer infers =
  let f_sir tags =
    let infr =
      match infers#sir with
      | `All infr ->
          infr
      | `Only_i infr ->
          infr
    in
    if is_that_infer infr then
      `Sir :: tags
    else
      tags
  in
  f_sir
  

let gather_base is_that_infer infers =
  let f tags =
       (gather_base_pars is_that_infer infers)
    @@ (gather_state is_that_infer infers)
    @@ tags
  in f


let gather_latent is_that_infer infers =
  let f_sigma tags =
    if is_that_infer infers#sigma then
      `Sigma :: tags
    else
      tags
  in f_sigma


let gather_circular is_that_infer infer =
  let f_betavar tags =
    if is_that_infer infer#betavar then
      `Betavar :: tags
    else
      tags
  in
  let f_phase tags =
    if is_that_infer infer#phase then
      `Phase :: tags
    else
      tags
  in
  let f_gamma tags =
    if is_that_infer infer#gamma then
      `Gamma :: tags
    else
      tags
  in
  let f_eta tags =
    if is_that_infer infer#eta then
      `Eta :: tags
    else
      tags
  in
  let f tags =
    f_betavar @@ f_phase @@ f_gamma @@ f_eta @@ tags
  in f


let gather_stochastic is_that_infer infer =
  let f_stoch tags =
    if is_that_infer infer#stoch then
      `Stoch :: tags
    else
      tags
  in
  let f tags =
    f_stoch @@ tags
  in f

open Sig


type theta = Episim.theta


class hyper_prior = object
  inherit Param.prior_base
  inherit Param.prior_stochastic
end


class hyper_prop = object
  inherit Param.prop_base
  inherit Param.prop_stochastic
end


class hyper = object
  inherit hyper_prior
  inherit hyper_prop
  inherit Param.sim
end


class infer_pars_ode = object
  inherit Infer.base
end


class infer_pars = object
  inherit infer_pars_ode
  inherit Infer.stochastic
end


type tag = [
  | Tag.base
  | Tag.stochastic
]


module S = Episim.Make (Epi.Sir.Model)


module Hyper =
  struct
    class t = hyper

    let t0 = Param.t0
    let tf = Param.tf
    let dt = Param.dt
    let width = Param.width
    let n_best_of = Param.n_best_of
    let seq_p_redraw_stoch = Param.seq_p_redraw_stoch
    let proposal_weights = Param.proposal_weights

    let uterm () =
      let f base lik stochastic load_theta ~config hy =
        hy
        |> base load_theta ~config
        |> lik ~config
        |> stochastic ~config
      in Cmdliner.Term.(
          const f
        $ Term.Hyper.base ()
        $ Term.Hyper.lik ()
        $ Term.Hyper.stochastic ()
      )

    let capture_term =
      let f read load_theta ~config =
        (new hyper)
        |> U.Arg.Capture.empty
        |> read load_theta ~config
      in Cmdliner.Term.(
          const f
        $ uterm ()
      )

    let term =
      let f read load_theta config capture =
        U.Arg.Capture.output capture (read load_theta ~config)
      in
      Cmdliner.Term.(
        const f
        $ capture_term
        $ Term.Theta.load_from
        $ Term.Hyper.load_from
      )
  end


module Fit =
  struct
    type nonrec hyper = hyper

    type nonrec theta = theta

    type par = Epi.Param.t_unit

    type state = sir

    type infer = infer_pars

    type nonrec tag = tag


    let state_settings = Sir

    let model_name = S.model_name

    let dims = S.dims

    let n_events = S.n_events

    let color_namer = S.Cont.color_namer

    let hyper_to_sim hy = (hy :> Param.sim)

    let to_state th =
      S.to_state state_settings th

    let state_to_pop x =
      S.state_to_pop state_settings x

    let pop_to_state z =
      S.pop_to_state state_settings z

    let to_par (th : theta) =
      (th :> par)

    let load_theta = S.load_theta

    let default_theta = new Episim.theta

    let default_infer = new infer_pars

    let infer_tags = Symbols.(base @ stochastic)

    let infer_gather f (infer : infer) =
         (Infer.gather_base f infer)
      @@ (Infer.gather_stochastic f infer)
      @@ []

    let tag_to_string = Symbols.Write.any

    let tag_to_columns =
      function
      | `Sir ->
          (* override to include 'ps0' 'pi0' 'pr0' *)
          ["s0" ; "i0" ; "r0" ; "ps0" ; "pi0" ; "pr0"]
      | tag ->
          Symbols.Write.columns tag

    let infer mode =
      function
      | #Tag.base as tag ->
          Infer.infer_base mode tag
      | #Tag.stochastic as tag ->
          Infer.infer_stochastic mode tag

    let fix =
      function
      | #Tag.base as tag ->
          Infer.fix_base tag
      | #Tag.stochastic as tag ->
          Infer.fix_stochastic tag

    let infer_stochastic infer =
      infer#stoch

    type 'a par_enum =
      float * (float * (float * 'a))

    type 'a state_enum =
      sir * 'a

    let params infer hy =
      Fitsim.(Fit.Param.List.[
        beta hy infer#beta ;
        nu hy infer#nu ;
        rho hy infer#rho ;
      ])

    let params_state infer hy =
      Fit.Param.List.[Fitsim.sir Fun.id Fun.id hy infer#sir]

    let param_stoch
      (type a b c d)
      (infer : infer)
      (settings : (a, b, c, d) settings)
      hy =
      Fitsim.param_stoch
        settings
        n_events
        S.E.Prm.color_groups
        hy
        infer#stoch

    let par_state_scale_draws infer hyper =
      let par_draws =
          (Draw.if_adapt Draw.beta infer#beta)
        @ (Draw.if_adapt Draw.nu infer#nu)
        @ (Draw.if_adapt Draw.rho infer#rho)
      in
      let state_draws =
         Draw.sir infer#sir hyper
      in
        (L.map Draw.left par_draws)
      @ (L.map Draw.right state_draws)

    let par_state_scale_draws_ratio infer hyper =
      let par_ratios =
          (Draw.if_adapt (Draw.Ratio.beta ()) infer#beta)
        @ (Draw.if_adapt (Draw.Ratio.nu ()) infer#nu)
        @ (Draw.if_adapt (Draw.Ratio.rho ()) infer#rho)
      in
      let state_ratios =
        Draw.Ratio.sir infer#sir hyper
      in
      Some (Draw.Ratio.fold (
          (L.map Draw.Ratio.left par_ratios)
        @ (L.map Draw.Ratio.right state_ratios)
      ))
      
    let simulate settings =
      S.sim_simple state_settings settings

    let simulate_csv ~chan settings =
      S.csv_sim state_settings settings ~chan

    let simulate_seq = S.sim_seq

    let obs_seq = S.obs_seq

    let extract th =
      let sof = string_of_float in
      function
      | "beta" ->
          sof th#beta
      | "nu" ->
          sof th#nu
      | "rho" ->
          sof th#rho
      | s ->
          failwith (Printf.sprintf "unrecognized column %s" s)

    let extract_state (sir0 : state) =
      let sof = string_of_float in
      function
      | "s0" ->
          sof sir0.s
      | "i0" ->
          sof sir0.i
      | "r0" ->
          sof sir0.r
      | "ps0" ->
          sof sir0.ps
      | "pi0" ->
          sof sir0.pi
      | "pr0" ->
          sof sir0.pr
      | s ->
          failwith (Printf.sprintf "unrecognized column %s" s)

    let extract_for_prm th =
      match th#prm_time, th#prm_color with
      | None, None ->
          ""
      | Some (nu, _), None ->
          F.to_string (Epi.Prm_time.density nu)
      | None, Some (nu, _) ->
          F.to_string (Epi.Prm_color.density nu)
      | Some _, Some _ ->
          invalid_arg "extract"

    let term =
      let f base sir config capture =
        (new Episim.theta)
        |> U.Arg.Capture.empty
        |> base ~config
        |> sir ~config
        |> U.Arg.Capture.output capture
      in Cmdliner.Term.(
        const f
        $ Epi.Term.Par.base ()
        $ Term.Theta.init_sir ~latency:Epi.Sir.Model.spec.latency
        $ Term.Theta.load_from
      )
  end

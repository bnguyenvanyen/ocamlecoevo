open Sig

type t = proposal_weights = {
  joint : U.closed_pos ;
  par_state : U.closed_pos ;
  scale_par_state : U.closed_pos ;
  stoch : U.closed_pos ;
}


let zero = {
  joint = F.zero ;
  par_state = F.zero ;
  scale_par_state = F.zero ;
  stoch = F.zero ;
}


let default = {
  joint = F.one ;
  par_state = F.zero ;
  scale_par_state = F.zero ;
  stoch = F.zero ;
}


let modify =
  function
  | `Joint ->
      (fun x y -> { y with joint = x })
  | `Par_state ->
      (fun x y -> { y with par_state = x })
  | `Scale_par_state ->
      (fun x y -> { y with scale_par_state = x })
  | `Stoch ->
      (fun x y -> { y with stoch = x })


let to_string =
  function
  | `Joint ->
      "joint"
  | `Par_state ->
      "par-state"
  | `Scale_par_state ->
      "scale-par-state"
  | `Stoch ->
      "stoch-pairs"


module Term =
  struct
    let tags =
      [
        `Joint ;
        `Par_state ;
        `Scale_par_state ;
        `Stoch ;
      ]
      |> L.map (fun tag -> (to_string tag, tag))
      |> Cmdliner.Arg.enum

    let modify =
      let doc =
        "Modify the propensity to choose this proposal"
      in
      Cmdliner.Arg.(
        value
        & opt_all (pair tags (U.Arg.positive ())) []
        & info
          ["epi-proposal-weight"]
          ~docv:"EPI-PROPOSAL-WEIGHT"
          ~doc
      )
  end


let term =
  let f modifiers =
    L.fold_right
      (fun (tag, w) -> modify tag w)
      modifiers
      default
  in
  Cmdliner.Term.(
    const f
    $ Term.modify
  )

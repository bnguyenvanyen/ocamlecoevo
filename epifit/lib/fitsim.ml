open Sig

module FP = Fit.Param
module FI = Fit.Infer


let beta hy = FI.maybe_adaptive_float
  ~tag:`Beta
  ~get:(fun th -> th#beta)
  ~set:(fun th x -> th#with_beta x)
  ~prior:(Prior.beta hy)
  ~proposal:(Propose.beta hy)
  ~jump:hy#beta_jump


let betavar hy = FI.maybe_adaptive_float
  ~tag:`Betavar
  ~get:(fun th -> th#betavar)
  ~set:(fun th x -> th#with_betavar x)
  ~prior:(Prior.betavar hy)
  ~proposal:(Propose.betavar hy)
  ~jump:hy#betavar_jump


let phase hy = FI.maybe_adaptive_float
  ~tag:`Phase
  ~get:(fun th -> th#phase)
  ~set:(fun th x -> th#with_phase x)
  ~prior:(Prior.phase hy)
  ~proposal:(Propose.phase hy)
  ~jump:hy#phase_jump


let sigma hy = FI.maybe_adaptive_float
  ~tag:`Sigma
  ~get:(fun th -> th#sigma)
  ~set:(fun th x -> th#with_sigma x)
  ~prior:(Prior.sigma hy)
  ~proposal:(Propose.sigma hy)
  ~jump:hy#sigma_jump


let nu hy = FI.maybe_adaptive_float
  ~tag:`Nu
  ~get:(fun th -> th#nu)
  ~set:(fun th x -> th#with_nu x)
  ~prior:(Prior.nu hy)
  ~proposal:(Propose.nu hy)
  ~jump:hy#nu_jump


let gamma hy = FI.maybe_adaptive_float
  ~tag:`Gamma
  ~get:(fun th -> th#gamma)
  ~set:(fun th x -> th#with_gamma x)
  ~prior:(Prior.gamma hy)
  ~proposal:(Propose.gamma hy)
  ~jump:hy#gamma_jump


let rho hy = FI.maybe_adaptive_float
  ~tag:`Rho
  ~get:(fun th -> th#rho)
  ~set:(fun th x -> th#with_rho x)
  ~prior:(Prior.rho hy)
  ~proposal:(Propose.rho hy)
  ~jump:hy#rho_jump


let eta hy = FI.maybe_adaptive_float
  ~tag:`Eta
  ~get:(fun th -> th#eta)
  ~set:(fun th x -> th#with_eta x)
  ~prior:(Prior.eta hy)
  ~proposal:(Propose.eta hy)
  ~jump:hy#eta_jump


let sir_fixed_size_jumps hy =
  let sj_sr =
    let set dx (sir : sir) =
      { sir with s = sir.s +. dx ;
                 r = sir.r -. dx }
    in
    let diff (sir : sir) (sir' : sir) =
      sir'.s -. sir.s +. 1. /. 2. *. (sir'.i -. sir.i)
    in
    let jump=(hy#sr_jump *. hy#popsize) in
    Fit.Param.SJ.{ set ; diff ; jump }
  in
  let sj_i =
    let set dx (sir : sir) =
     { sir with s = sir.s -. 1./.2. *. dx ;
                i = sir.i +. dx ;
                r = sir.r -. 1./.2. *. dx }
    in
    let diff (sir : sir) (sir' : sir) =
      sir'.i -. sir.i
    in
    let jump=(hy#ei_jump *. hy#popsize) in
    Fit.Param.SJ.{ set ; diff ; jump }
  in
  match hy#sir_prior with
  | `Dirichlet
  | `Multinomial ->
      [ sj_sr ; sj_i ]
  | `Dirichlet_multinomial ->
      let sj_psr =
        let set dx (sir : sir) =
         { sir with ps = sir.ps +. dx ;
                    pr = sir.pr -. dx }
        in
        let diff (sir : sir) (sir' : sir) =
          sir'.ps -. sir.ps +. 1. /. 2. *. (sir'.pi -. sir.pi)
        in
        let jump=hy#psir_jump in
        Fit.Param.SJ.{ set ; diff ; jump }
      in
      let sj_pi =
        let set dx (sir : sir) =
         { sir with ps = sir.ps -. 1./.2. *. dx ;
                    pi = sir.pi +. dx ;
                    pr = sir.pr -. 1./.2. *. dx }
        in
        let diff (sir : sir) (sir' : sir) =
          sir'.pi -. sir.pi
        in
        let jump=hy#psir_jump in
        Fit.Param.SJ.{ set ; diff ; jump }
      in
      [ sj_sr ; sj_i ; sj_psr ; sj_pi ]
      

(* don't try to conserve total population size *)
let sir_indep_jumps hy =
  let sj_s =
    let set dx (sir : sir) =
      { sir with s = sir.s +. dx }
    in
    let diff (sir : sir) (sir' : sir) =
      sir'.s -. sir.s
    in
    let jump=(hy#sr_jump *. hy#popsize) in
    Fit.Param.SJ.{ set ; diff ; jump }
  in
  let sj_i =
    let set dx (sir : sir) =
      { sir with i = sir.i +. dx }
    in
    let diff (sir : sir) (sir' : sir) =
      sir'.i -. sir.i
    in
    let jump=(hy#ei_jump *. hy#popsize) in
    Fit.Param.SJ.{ set ; diff ; jump }
  in
  let sj_r =
    let set dx (sir : sir) =
      { sir with r = sir.r +. dx }
    in
    let diff (sir : sir) (sir' : sir) =
      sir'.r -. sir.r
    in
    let jump=(hy#sr_jump *. hy#popsize) in
    Fit.Param.SJ.{ set ; diff ; jump }
  in
  match hy#sir_prior with
  | `Dirichlet
  | `Multinomial ->
      [ sj_s ; sj_i ; sj_r ]
  | `Dirichlet_multinomial ->
      let sj_psr =
        let set dx (sir : sir) =
         { sir with ps = sir.ps +. dx ;
                    pr = sir.pr -. dx }
        in
        let diff (sir : sir) (sir' : sir) =
          sir'.ps -. sir.ps +. 1. /. 2. *. (sir'.pi -. sir.pi)
        in
        let jump=hy#psir_jump in
        Fit.Param.SJ.{ set ; diff ; jump }
      in
      let sj_pi =
        let set dx (sir : sir) =
         { sir with ps = sir.ps -. 1./.2. *. dx ;
                    pi = sir.pi +. dx ;
                    pr = sir.pr -. 1./.2. *. dx }
        in
        let diff (sir : sir) (sir' : sir) =
          sir'.pi -. sir.pi
        in
        let jump=hy#psir_jump in
        Fit.Param.SJ.{ set ; diff ; jump }
      in
      [ sj_s ; sj_i ; sj_r ; sj_psr ; sj_pi ]


let seir_fixed_size_jumps hy =
  let sj_sr =
    let set dx (seir : seir) =
      { seir with s = seir.s +. dx ;
                  r = seir.r -. dx }
    in
    let diff (seir : seir) (seir' : seir) =
      seir'.s -. seir.s
      +. 1. /. 2. *. (seir'.e -. seir.e)
      +. 1. /. 2. *. (seir'.i -. seir.i)
    in
    let jump=(hy#sr_jump *. hy#popsize) in
    Fit.Param.SJ.{ set ; diff ; jump }
  in
  let sj_e =
    let set dx (seir : seir) =
      { seir with s = seir.s -. 1./.2. *. dx ;
                  e = seir.e +. dx ;
                  r = seir.r -. 1./.2. *. dx }
    in
    let diff (seir : seir) (seir' : seir) =
      seir'.e -. seir.i
    in
    let jump=(hy#ei_jump *. hy#popsize) in
    Fit.Param.SJ.{ set ; diff ; jump }
  in
  let sj_i =
    let set dx (seir : seir) =
      { seir with s = seir.s -. 1./.2. *. dx ;
                  i = seir.i +. dx ;
                  r = seir.r -. 1./.2. *. dx }
    in
    let diff (seir : seir) (seir' : seir) =
      seir'.i -. seir.i
    in
    let jump=(hy#ei_jump *. hy#popsize) in
    Fit.Param.SJ.{ set ; diff ; jump }
  in
  [ sj_sr ; sj_e ; sj_i ]


let seir_indep_jumps hy =
  let sj_s =
    let set dx seir =
      { seir with s = seir.s +. dx }
    in
    let diff seir seir' =
      seir'.s -. seir.s
    in
    let jump=(hy#sr_jump *. hy#popsize) in
    Fit.Param.SJ.{ set ; diff ; jump }
  in
  let sj_e =
    let set dx seir =
      { seir with e = seir.e +. dx }
    in
    let diff seir seir' =
      seir'.e -. seir.e
    in
    let jump=(hy#ei_jump *. hy#popsize) in
    Fit.Param.SJ.{ set ; diff ; jump }
  in
  let sj_i =
    let set dx seir =
      { seir with i = seir.i +. dx }
    in
    let diff seir seir' =
      seir'.i -. seir.i
    in
    let jump=(hy#ei_jump *. hy#popsize) in
    Fit.Param.SJ.{ set ; diff ; jump }
  in
  let sj_r =
    let set dx seir =
      { seir with r = seir.r +. dx }
    in
    let diff seir seir' =
      seir'.r -. seir.r
    in
    let jump=(hy#sr_jump *. hy#popsize) in
    Fit.Param.SJ.{ set ; diff ; jump }
  in
  [ sj_s ; sj_e ; sj_i ; sj_r ]


(* never needs to update ps pi pr *)
let i_jumps hy =
 assert (hy#sir_prior = `Dirichlet) ;
  let delta_i dx (sir : sir) =
    { sir with i = sir.i +. dx }
  in
  let diff_i (sir : sir) (sir' : sir) =
    sir'.i -. sir.i
  in
  Fit.Param.SJ.[
    {
      set=delta_i ;
      diff=diff_i ;
      jump=(hy#ei_jump *. hy#popsize) ;
    }
  ]


let ei_jumps hy =
  let delta_e dx seir =
    { seir with e = seir.e +. dx }
  in
  let delta_i dx seir =
    { seir with i = seir.i +. dx }
  in
  let diff_e seir seir' =
    seir'.e -. seir.e
  in
  let diff_i seir seir' =
    seir'.i -. seir.i
  in
  Fit.Param.SJ.[
    {
      set=delta_e ;
      diff=diff_e ;
      jump=(hy#ei_jump *. hy#popsize) ;
    } ;
    {
      set=delta_i ;
      diff=diff_i ;
      jump=(hy#ei_jump *. hy#popsize) ;
    } ;
  ]


let reraise_as_draw_error f x =
  match f x with
  | res ->
      res
  | exception ((Failure _ | Invalid_argument _) as e) ->
      Fit.raise_draw_error e


let sir_all of_tuple to_tuple hy =
  let jumps =
    if hy#fix_popsize then
      sir_fixed_size_jumps hy
    else
      sir_indep_jumps hy
  in
  FI.maybe_adaptive
  ~tag:`Sir
  ~get:to_tuple
  ~set:(fun _ x -> reraise_as_draw_error of_tuple x)
  ~prior:(Prior.sir hy)
  ~proposal:(Propose.sir hy)
  ~jumps


let i_only of_tuple to_tuple hy =
  FI.maybe_adaptive
  ~tag:`Sir
  ~get:to_tuple
  ~set:(fun _ x -> reraise_as_draw_error of_tuple x)
  ~prior:(Prior.i_only hy)
  ~proposal:(Propose.i_normal hy)
  ~jumps:(i_jumps hy)


let seir_all of_tuple to_tuple hy =
  let jumps =
    if hy#fix_popsize then
      seir_fixed_size_jumps hy
    else
      seir_indep_jumps hy
  in
  FI.maybe_adaptive
  ~tag:`Sir
  ~get:to_tuple
  ~set:(fun _ x -> reraise_as_draw_error of_tuple x)
  ~prior:(Prior.seir hy)
  ~proposal:(Propose.seir hy)
  ~jumps


let ei_only of_tuple to_tuple hy =
  FI.maybe_adaptive
  ~tag:`Sir
  ~get:to_tuple
  ~set:(fun _ x -> reraise_as_draw_error of_tuple x)
  ~prior:(Prior.ei_only hy)
  ~proposal:(Propose.ei_normal hy)
  ~jumps:(ei_jumps hy)


let sir of_tuple to_tuple hy =
  function
  | `All infr ->
      sir_all of_tuple to_tuple hy infr
  | `Only_i infr ->
      i_only of_tuple to_tuple hy infr


let seir of_tuple to_tuple hy =
  function
  | `All infr ->
      seir_all of_tuple to_tuple hy infr
  | `Only_i infr ->
      ei_only of_tuple to_tuple hy infr


type 'a prm_able = <
  prm : 'b. 'b prm_sim_mode -> 'b * intensities ;
  ..
> as 'a


let prm
  (type a b c) (settings : (a, b, c) prm_settings)
  color_groups hy infer :
  _ * c FP.t =
  let colors = L.reduce (@) color_groups in
  let prior = Prior.prm settings colors hy in
  let proposal = Propose.prm settings hy in
  let mode = Settings.prm_sim_mode settings in
  let module Prm = (
    val (Prmfit.of_prm_sim_mode mode) :
      (Prmfit.EPI_PRM_FIT with type P.t = a)
  ) in
  let par : c FP.t =
    match settings with
    | Ode _
    | Sde _ ->
        assert false
    | Prm (_, No_intensities, _) ->
        (* cannot be adaptive without intensities *)
        FI.Param.only_custom ~prior ~proposal infer
    | Prm (_, Intensities, Sequential) ->
        (* no proposal for a prm list (custom is just Constant_flat) *)
        (* FI.Param.only_custom ~prior ~proposal infer *)
        (* I cheat here so that initial rand_nu goes through
         * when infer-adapt stoch *)
        FI.Param.maybe_adaptive
          ~prior
          ~proposal
          ~jumps:[]
          infer
    | Prm (_, Intensities, Simple) ->
        (* maybe adaptive *)
        let tf = Param.duration hy in
        let width = Param.width hy in
        let jumps : c FP.DJ.t list =
          Prm.jumps
            ~color_groups
            ~ntranges:(Param.prm_ntranges hy)
            ~ntslices:(Epi.Point.ntslices ~width tf)
            hy#prm_jump
        in
        let log_pd_ratio : (b -> b -> float list) option =
          Some Prm.log_pd_ratio
        in
        FI.Param.maybe_adaptive_random
          ~prior
          ~proposal
          ~jumps
          ~log_pd_ratio
          infer
    | Prm (_, Intensities, Seq_elt) ->
        let ntslices =
          Prm.ntslices
            ~width:(Param.width hy)
            ~duration:(Param.dt hy)
        in
        let jumps : c FP.DJ.t list =
          Prm.jumps_elt
            ~colors
            ~ntslices (* how many per element ? *)
            hy#prm_jump
        in
        let log_pd_ratio : (b -> b -> float list) option =
          Some Prm.log_pd_ratio
        in
        FI.Param.maybe_adaptive_random
          ~prior
          ~proposal
          ~jumps
          ~log_pd_ratio
          infer
  in (`Stoch, par)


let dbt_jumps (type a) (seq : (Sim.Dbt.t, a) sequential) n_events hy :
  a Fit.Param.SJ.t list =
  let rtdt = sqrt (F.to_float (Param.width hy)) in
  let f () =
    L.init n_events (fun k ->
      Fit.Param.SJ.{
        set=(fun dx ->
          Jump.Regular.map (fun _ dbt ->
            dbt.{k + 1} <- dbt.{k + 1} +. rtdt *. dx ;
            dbt)
        ) ;
        diff=(fun dbts dbts' ->
          (* the results should be the same for all increments *)
          let _, dbt = Jump.Regular.nth dbts 0 in
          let _, dbt' = Jump.Regular.nth dbts' 0 in
          (dbt'.{k + 1} -. dbt.{k + 1}) /. rtdt
        ) ;
        jump=hy#dbt_jump ;
      }
    )
  in
  match seq with
  | Simple ->
      f ()
  | Seq_elt ->
      f ()
  | Sequential ->
      []


let dbts
  (type a) (settings : (_, _, _, a) settings)
  n_events hy infer : _ * a FP.t =
  let seq = Settings.sequential settings in
  let prior = Prior.dbts n_events seq hy in
  let proposal = Propose.dbts n_events seq hy in
  (* If I could directly use Lac.Vec.add it would be more efficient *)
  let jumps = dbt_jumps seq n_events hy in
  let par = FI.Param.maybe_adaptive
    ~prior
    ~proposal
    ~jumps
    infer
  in (`Stoch, par)


let param_stoch
  (type a b c d) (settings : (a, b, c, d) settings)
  n_events color_groups hy infer : _ * d FP.t =
  match settings with
  | Ode _ ->
      (`Stoch, Custom {
          prior = Prior.ode settings ;
          proposal = Fit.Propose.Constant_flat
       })
  | Sde _ as settings ->
      dbts settings n_events hy infer
  | Prm _ as settings ->
      prm settings color_groups hy infer


let set_from_prior tag tag' set prior =
  if tag = tag' then
    Some (fun rng x -> set x (D.draw_from rng prior))
  else
    None


let set_from_prior_dep tag tag' set prior =
  if tag = tag' then
    Some (fun rng x -> set x (D.draw_from rng (prior x)))
  else
    None


let draw_from_prior tag' l =
  let combine :
    type p a.
    (U.rng -> p -> p) option ->
    ('tag, p, a) FP.List.par ->
      (U.rng -> p -> p) option =
    FP.(
    fun draw (tag, { set ; elt ; _ }) ->
    match draw with
    | Some _ ->
        draw
    | None ->
        begin match elt with
        | Fixed ->
            if tag = tag' then
              Some (fun _ x -> x)
            else
              None
        | Custom { prior ; _ }
        | Adaptive { prior ; _ }
        | Random { prior ; _ } ->
            set_from_prior tag tag' set prior
        | Adaptive_float { prior ; _ } ->
            (* different type so not under or *)
            set_from_prior tag tag' set prior
        end
  )
  in
  match FP.List.fold_left { f = combine } None l with
  | None ->
      (* if tag is an open variant type this can not happen (?) *)
      raise Not_found
  | Some draw ->
      draw


(** @canonical Epifit.Sig *)
module Sig = Sig

(** @canonical Epifit.Param *)
module Param = Param

(** @canonical Epifit.Tag *)
module Tag = Tag

(** @canonical Epifit.Symbols *)
module Symbols = Symbols

(** @canonical Epifit.Term *)
module Term = Term

(** @canonical Epifit.Data *)
module Data = Data

(** @canonical Epifit.Observe *)
module Observe = Observe

(** @canonical Epifit.Likelihood *)
module Likelihood = Likelihood

(** @canonical Epifit.Infer *)
module Infer = Infer

(** @canonical Epifit.Prior *)
module Prior = Prior

(** @canonical Epifit.Propose *)
module Propose = Propose

(** @canonical Epifit.Draw *)
module Draw = Draw

(** @canonical Epifit.Fitsim *)
module Fitsim = Fitsim

(** @canonical Epifit.Episim *)
module Episim = Episim

(** @canonical Epifit.Mcmc *)
module Mcmc = Mcmc

(** @canonical Epifit.Cli *)
module Cli = Cli

(** @canonical Epifit.Resimulate *)
module Resimulate = Resimulate

(** @canonical Epifit.Sir *)
module Sir = Sir

(** @canonical Epifit.Sirs *)
module Sirs = Sirs

(** @canonical Epifit.Seir *)
module Seir = Seir

(** @canonical Epifit.Seirs *)
module Seirs = Seirs

open Sig

module P = Param


let prop_of d = Fit.Propose.endo_dist d


let normal_prop sigma =
  prop_of (fun x -> D.Normal (x, F.sq sigma))


let beta hypar =
  normal_prop (P.beta_jump hypar)


let betavar hypar =
  normal_prop (P.betavar_jump hypar)


let phase hypar =
  normal_prop (P.phase_jump hypar)


let sigma hypar =
  normal_prop (P.sigma_jump hypar)


let nu hypar =
  normal_prop (P.nu_jump hypar)


let gamma hypar =
  normal_prop (P.gamma_jump hypar)


let rho hypar =
  normal_prop (P.rho_jump hypar)


let eta hypar =
  normal_prop (P.eta_jump hypar)


let sir hypar =
  let vi = F.sq F.Op.(P.ei_jump hypar * P.popsize hypar) in
  let vsr = F.sq F.Op.(P.sr_jump hypar * P.popsize hypar) in
  let vp = F.sq (P.psir_jump hypar) in
  let try_draw rng (sir : sir) =
    let dsr = U.rand_normal ~rng 0. vsr in
    let di = U.rand_normal ~rng 0. vi in
    let s = sir.s +. dsr -. di /. 2. in
    let i = sir.i +. di in
    let r = sir.r -. dsr -. di /. 2. in
    match hypar#sir_prior with
    | `Dirichlet
    | `Multinomial ->
        { sir with s ; i ; r }
    | `Dirichlet_multinomial ->
        let dp1 = U.rand_normal ~rng 0. vp in
        let dp2 = U.rand_normal ~rng 0. vp in
        let dp3 = U.rand_normal ~rng 0. vp in
        let ps = sir.ps +. dp1 -. dp2 in
        let pi = sir.pi +. dp2 -. dp3 in
        let pr = sir.pr +. dp3 -. dp1 in
        { s ; i ; r ; ps ; pi ; pr }
  in
  let valid (sir : sir) =
       (sir.s > 0.)
    && (sir.i > 0.)
    && (sir.r > 0.)
    && (sir.ps > 0.)
    && (sir.pi > 0.)
    && (sir.pr > 0.)
  in
  let draw rng x =
    U.insist ~rng (fun ~rng ->
      let x' = try_draw rng x in
      if valid x' then
        Some x'
      else
        None
    )
  in
  let log_pd_ratio _ _ =
    [0.]
  in
  let move_to_sample sir =
    sir
  in
  Fit.Propose.Base { draw ; log_pd_ratio ; move_to_sample }


let i_normal hypar =
  let vi = F.sq F.Op.(P.ei_jump hypar * P.popsize hypar) in
  let try_draw rng (sir : sir) =
    { sir with i = U.rand_normal ~rng sir.i vi }
  in
  let valid (sir : sir) =
    sir.i > 0.
  in
  let draw rng x =
    U.insist ~rng (fun ~rng ->
      let x' = try_draw rng x in
      if valid x' then
        Some x'
      else
        None
    )
  in
  let log_pd_ratio _ _ =
    [0.]
  in
  let move_to_sample sir =
    sir
  in
  Fit.Propose.Base { draw ; log_pd_ratio ; move_to_sample }


let seir hypar =
  let vei = F.sq F.Op.(P.ei_jump hypar * P.popsize hypar) in
  let vsr = F.sq F.Op.(P.sr_jump hypar * P.popsize hypar) in
  let try_draw rng (seir : seir) =
    let dsr = U.rand_normal ~rng 0. vsr in
    let de = U.rand_normal ~rng 0. vei in
    let di = U.rand_normal ~rng 0. vei in
    {
      s = seir.s +. dsr -. (de +. di) /. 2. ;
      e = seir.e +. de ;
      i = seir.i +. di ;
      r = seir.r -. dsr -. (de +. di) /. 2. ;
    }
  in
  let valid { s ; e ; i ; r } =
       (s > 0.)
    && (e > 0.)
    && (i > 0.)
    && (r > 0.)
  in
  let draw rng x =
    U.insist ~rng (fun ~rng ->
      let x' = try_draw rng x in
      if valid x' then
        Some x'
      else
        None
    )
  in
  let log_pd_ratio _ _ =
    [0.]
  in
  let move_to_sample seir =
    seir
  in
  Fit.Propose.Base { draw ; log_pd_ratio ; move_to_sample }


let ei_normal hypar =
  let v = F.sq F.Op.(P.ei_jump hypar * P.popsize hypar) in
  let try_draw rng (seir : seir) =
    { seir with e = U.rand_normal ~rng seir.e v ;
                i = U.rand_normal ~rng seir.i v ;
    }
  in
  let valid { e ; i ; _ } =
    (e > 0.) && (i > 0.)
  in
  let draw rng x =
    U.insist ~rng (fun ~rng ->
      let x' = try_draw rng x in
      if valid x' then
        Some x'
      else
        None
    )
  in
  let log_pd_ratio _ _ =
    [0.]
  in
  let move_to_sample seir =
    seir
  in
  Fit.Propose.Base { draw ; log_pd_ratio ; move_to_sample }


let prm_poisson (type a) (mode : a prm_sim_mode) hy :
  (a option, a option) Fit.Propose.t =
  let nredraws = I.to_int (Param.prm_nredraws hy) in
  match mode with
  | Exact ->
      Fit.Propose.Option (Prmfit.Time.propose_poisson_full nredraws)
  | Approx ->
      Fit.Propose.Option (Prmfit.Color.propose_poisson_full nredraws)
  | Fast ->
      Fit.Propose.Option (Prmfit.Cheat.propose_poisson_full nredraws)


let prm (type a b c) (settings : (a, b, c) prm_settings) hy :
  (c, c) Fit.Propose.t =
  let n = I.to_int (Param.prm_nredraws hy) in
  match settings with
  | Ode _
  | Sde _ ->
      assert false
  | Prm (_, _, Sequential) ->
      (* No proposal for the prm list *)
      Fit.Propose.Constant_flat
  | Prm (Exact, Intensities, Simple) ->
      Prmfit.Time.propose_poisson_intens_dist n
  | Prm (Exact, Intensities, Seq_elt) ->
      Prmfit.Time.propose_poisson_intens_dist n
  | Prm (Exact, No_intensities, Simple) ->
      Prmfit.Time.propose_poisson_full n
  | Prm (Exact, No_intensities, Seq_elt) ->
      Prmfit.Time.propose_poisson_full n
  | Prm (Approx, Intensities, Simple) ->
      Prmfit.Color.propose_poisson_intensities n
  | Prm (Approx, Intensities, Seq_elt) ->
      Prmfit.Color.propose_poisson_intensities n
  | Prm (Approx, No_intensities, Simple) ->
      Prmfit.Color.propose_poisson_full n
  | Prm (Approx, No_intensities, Seq_elt) ->
      Prmfit.Color.propose_poisson_full n
  | Prm (Fast, Intensities, Simple) ->
      Prmfit.Cheat.propose_poisson_intensities n
  | Prm (Fast, Intensities, Seq_elt) ->
      Prmfit.Cheat.propose_poisson_intensities n
  | Prm (Fast, No_intensities, Simple) ->
      Prmfit.Cheat.propose_poisson_full n
  | Prm (Fast, No_intensities, Seq_elt) ->
      Prmfit.Cheat.propose_poisson_full n


(* The most simple custom proposal I can think of,
 * in the style of Prm.propose_poisson *)
let dbt_simple dims hy =
  (* number of increments (slices) for which to redraw *)
  let n_incr_redraw = I.to_int (Param.prm_nredraws hy) in
  let draw rng dbts =
    Sim.Dbt.redraw_dim ~rng ~n:dims n_incr_redraw dbts
  in
  let log_pd_ratio dbts dbts' =
    (* we redraw from the prior, so we cancel the prior ratio *)
    [F.to_float F.Op.(Sim.Dbt.log_dens dbts - Sim.Dbt.log_dens dbts')]
  in
  let move_to_sample dbts =
    dbts
  in
  Fit.Propose.Base { draw ; log_pd_ratio ; move_to_sample }


(* For Seqcustom, I'd like to use something like the following
 * (dbt proposed so that the trajectory moves by a gaussian variable)
 * But there is a lot of work to make that usable in practice
 *)
let dbt_seq dims drift diff hy =
  let j = hy#dbt_jump in
  let dt = F.to_float (Param.width hy) in
  let lifted_draw = Sim.Dbt.draw_sde_normal dims drift diff j dt in
  let lifted_prop t thz thz' =
    let draw rng dbt =
      lifted_draw rng t thz thz' dbt
    in
    let log_pd_ratio _ _ =
      []
    in
    let move_to_sample thz =
      thz
    in
    Fit.Propose.Base { draw ; log_pd_ratio ; move_to_sample }
  in lifted_prop


let dbts (type a) dims (seq : (Sim.Dbt.t, a) sequential) hy :
  (a, a) Fit.Propose.t =
  match seq with
  | Simple ->
      dbt_simple dims hy
  | Seq_elt ->
      (* FIXME is dbt_simple ok here ? *)
      (* failwith "Not_implemented" *)
      dbt_simple dims hy
  | Sequential ->
      Fit.Propose.Constant_flat

open Sig


type 'a mh_return = ('a, 'a, float list) Fit.Mh.return


module type S =
  sig
    type hyper
    type infer
    type tag
    type theta
    type par
    type state

    type recipe_step = {
      name : string ;
      adapt : bool ;
      infer : infer ;
      n_iter : int ;
      theta_every : int option ;
      traj_every : int option ;
      chol_every : int option ;
      stoch_every : int option ;
    }

    val all_params :
      (_, _, _, 'd) settings ->
      infer ->
      hyper ->
        (tag, (par * state) * 'd) elist

    val run :
      ?verbose: bool ->
      ?seed: int ->
      path: string ->
      lik: likelihood_settings ->
      settings: (_, _, _, _) settings ->
      recipe: recipe_step list ->
      hyper ->
      data ->
      theta ->
        par

    module Term :
      sig
        val run : (hyper * par) Cmdliner.Term.t
        val info : Cmdliner.Term.info
      end
  end


let create_chol pars =
  Fit.Param.List.init_chol pars


(* chol from step to step with new parameters to infer *)
let new_chol pars' =
  (* ' is new *)
  let mat' = create_chol pars' in
  function
  | None ->
      mat'
  | Some (El pars, mat) ->
      Fit.Param.List.copy_to pars mat pars' mat' ;
      mat'


module Make
  (Hyper : HYPER)
  (M : FITSIM with type sim := Param.sim
               and type hyper = Hyper.t) :
  (S with type hyper = Hyper.t
      and type infer = M.infer
      and type tag = M.tag
      and type theta = M.theta
      and type par = M.par) =
  struct
    (* does it make sense to mix custom and adaptive, however ?
     * Or when it's adaptive should it be all adaptive (and fixed) ?
     * Does the algorithm still work otherwise ?
     *)

    type hyper = Hyper.t
    type infer = M.infer
    type tag = M.tag
    type theta = M.theta
    type par = M.par
    type state = M.state

    type recipe_step = {
      name : string ;
      adapt : bool ;
      infer : M.infer ;
      (* not enough to allow the flexibility we would like *)
      (* prior_as_proposal : bool ; *)
      n_iter : int ;
      theta_every : int option ;
      traj_every : int option ;
      chol_every : int option ;
      stoch_every : int option ;
    }

    let left_param_dep (tag, d) =
      let d' =
        Fit.Param.Depend.map
          ~get:(fun (a, _) -> a)
          ~set:(fun (_, b) a -> (a, b))
          d
      in (tag, d')

    let right_param_dep (tag, d) =
      let d' =
        Fit.Param.Depend.map
          ~get:(fun (_, b) -> b)
          ~set:(fun (a, _) b -> (a, b))
          d
      in (tag, d')

    let append l l' =
      Fit.Param.List.(
          (map { f = left_param_dep } l)
        @ (map { f = right_param_dep } l')
      )

    let right_param g (tag, p) =
      let d =
        Fit.Param.dep
          ~get:(fun (_, nu) -> g nu)
          ~set:(fun (th, _) nu -> (th, nu))
          p
      in (tag, d)

    let put_at_end g l tp' =
      Fit.Param.List.(
          (map { f = left_param_dep } l)
        @ [right_param g tp']
      )

    let pair_params infer hy :
      (M.tag, M.par * M.state, _, _) Fit.Param.List.t =
      let pars_th = M.params infer hy in
      let pars_x = M.params_state infer hy in
      append pars_th pars_x

    (* complete params,
       as acting on [((th, x0), stoch)] *)
    let all_params
      (type a b c d) (settings : (a, b, c, d) settings) infer hy :
      (M.tag, (M.par * M.state) * d) elist =
      match settings with
      | Ode _ ->
          (* unit on the right *)
          let pars_pair = pair_params infer hy in
          El (Fit.Param.List.map { f = left_param_dep } pars_pair)
      | Sde _
      | Prm _ ->
          (* map to (theta, b) pair and add dbts / prm inference *)
          let pars_pair = pair_params infer hy in
          let par_stoch = M.param_stoch infer settings hy in
          let g = Settings.copy_stoch settings in
          El (put_at_end g pars_pair par_stoch)

    (* only (par * state) pars, but for the full ((par * state) * stoch) *)
    let par_state_params_fulldim (type d) infer hy :
      (M.tag, (M.par * M.state) * d) elist =
      let pars_pair = pair_params infer hy in
      El (Fit.Param.List.map { f = left_param_dep } pars_pair)

    (* only stoch pars, but for the full ((par * state) * stoch) *)
    let stoch_params_fulldim
      (type a b c d) (settings : (a, b, c, d) settings) infer hy :
      (M.tag, (M.par * M.state) * d) elist =
      match settings with
      | Ode _ ->
          El Fit.Param.List.[]
      | Sde _
      | Prm _ ->
          let par_stoch = M.param_stoch infer settings hy in
          let g = Settings.copy_stoch settings in
          El Fit.Param.List.[right_param g par_stoch]


    (* output stuff *)

    let recipe_columns = [
      "name" ;
      "niter" ;
      "theta_every" ;
      "traj_every" ;
      "chol_every" ;
      "stoch_every" ;
      (* for infer *)
      "fix" ;
      "infer-prior" ;
      "infer-custom" ;
      "infer-adapt" ;
    ]

    let tags_to_string tags =
      let stags = L.map M.tag_to_string tags in
      let out = BatIO.output_string () in
      L.print ~sep:";" BatString.print out stags ;
      BatIO.close_out out

    let infer_tags_to_string f infer =
      tags_to_string (M.infer_gather f infer)

    let recipe_extract rcp =
      let soi = string_of_int in
      let soio =
        function
        | None ->
            ""
        | Some n ->
            soi n
      in
      function
      | "name" ->
          rcp.name
      | "niter" ->
          soi rcp.n_iter
      | "theta_every" ->
          soio rcp.theta_every
      | "traj_every" ->
          soio rcp.traj_every
      | "chol_every" ->
          soio rcp.chol_every
      | "stoch_every" ->
          soio rcp.stoch_every
      | "fix" ->
          infer_tags_to_string Fit.Infer.is_fixed rcp.infer
      | "infer-prior" ->
          infer_tags_to_string Fit.Infer.is_free_prior rcp.infer
      | "infer-custom" ->
          infer_tags_to_string Fit.Infer.is_free_custom rcp.infer
      | "infer-adapt" ->
          infer_tags_to_string Fit.Infer.is_free_adapt rcp.infer
      | c ->
          invalid_arg (Printf.sprintf "unrecognized column : %s" c)

    let output_recipe ~path rcp =
      Util.Csv.output
        ~columns:recipe_columns
        ~extract:recipe_extract
        ~path:(Printf.sprintf "%s.recipe.csv" path)
        rcp

    let columns pars =
      let tags = Fit.Param.List.infer_tags pars in
      let th_cols = L.fold_left (fun cs tag ->
          cs @ (M.tag_to_columns tag)
        ) [] tags
      in
      let move_cols = L.fold_left (fun cs tag ->
          cs @ (L.map (fun s -> "move_" ^ s) (M.tag_to_columns tag))
        ) [] tags
      in
      Fit.Csv.columns_sample_move @ th_cols @ move_cols

    let extract k res =
      let extract ((th, z0), _) s =
        match M.extract_state z0 s with
        | v ->
            v
        | exception (Failure _ | Invalid_argument _) ->
            M.extract th s
      in
      Some (Fit.Csv.extract_sample_move
        ~move_to_sample:(fun x -> x)
        ~extract
        k res
      )

    let sim_traj (type a b c d) (settings : (a, b, c, d) settings)
      hy ~chan ((th, z0), (nu : d)) =
      try
        begin match settings, nu with
        | Ode _, nu ->
            M.simulate_csv settings hy ~chan th z0 nu
        | Sde _, nu ->
            M.simulate_csv settings hy ~chan th z0 nu
        | Prm (_, No_intensities, _), nu ->
            M.simulate_csv settings hy ~chan th z0 nu
        | Prm (_, Intensities, _), nu ->
            M.simulate_csv settings hy ~chan th z0 nu
        end
      with Fit.Simulation_error _ ->
          ()

    let convert_prm
      (type a b c) (settings : (a, b, c) prm_settings) :
      (_ * c) mh_return Simfit.Out.convert_nu =
      match settings with
      | Ode _
      | Sde _ ->
          (* can't be reached with type discrete_pop,
           * but can't be refuted *)
          assert false
      | Prm (mode, _, _) ->
          let module Prm = (
            val (Prmfit.of_prm_sim_mode mode) :
              Prmfit.EPI_PRM_FIT with type P.t = a
          )
          in
          let module Out_prm = Simfit.Out.Prm.Make (Prm.P) in
          Out_prm.convert_prm_agg
            ~to_prm:(fun (_, nu) -> Settings.simplify settings nu)
            ~output_grid:true

    let convert_dbts (type a b c) (settings : (a, b, c) cont_settings) :
      (_ * c) mh_return Simfit.Out.convert_nu =
      match settings with
      | Ode _
      | Prm _ ->
          assert false
      | Sde _ as settings -> 
          Simfit.Out.Dbt.convert
            ~namer:M.color_namer
            ~to_dbts:(fun (_, nu) -> Settings.simplify settings nu)
            ~len:M.n_events

    let convert_nu (type a b c d) (settings : (a, b, c, d) settings) :
      (_ * d) mh_return Simfit.Out.convert_nu =
      match settings with
      | Ode _ ->
          (* never useful *)
          (fun ?append ?n_burn ?n_thin _ ->
            ignore append ; ignore n_burn ; ignore n_thin ;
            U.Out.convert_null
          )
      | Sde _  as settings ->
          convert_dbts settings
      | Prm _ as settings ->
          convert_prm settings

    type 'a res = ((M.par * M.state) * 'a) mh_return
    and 'a output = (
      int,
      'a res,
      int * 'a res * Lac.mat
    ) U.Out.t

    let output (type a b c d)
      ?theta_every ?traj_every ?chol_every ?stoch_every
      (settings : (a, b, c, d) settings) columns hy chol path :
      d output =
      let sim_traj = sim_traj settings hy in
      let convert_nu = convert_nu settings in
      Simfit.Out.convert_csv_traj_ram_nu
        ?n_thin:theta_every
        ?traj_every
        ?chol_every
        ?nu_every:stoch_every
        ~append:true
        ~columns
        ~extract
        ~convert_nu
        ~sim_traj
        ~chol
        path

    (* random stochastic component from prior *)
    let rand_nu (type a b c d) ?seed infer (settings : (a, b, c, d) settings) hy =
      (* dirty grab of the tag *)
      let tag_stoch, _ = M.param_stoch infer settings hy in
      (* with stoch to be inferred, so that prior goes through *)
      let inf' = M.infer `Custom tag_stoch infer in
      let _, par_stoch = M.param_stoch inf' settings hy in
      let prior_stoch = Fit.Param.prior par_stoch in
      Fit.Dist.draw_from (U.rng seed) prior_stoch

    let augment_state pop z =
      (F.narrow F.zero, M.state_to_pop pop z)

    (* Information is lost here. It would not work to call prior,
       but it works for seqram because it's just for diff
       (so from adaptive proposals on sir) *)
    let strip_state pop (_, z) =
      M.pop_to_state pop z

    let proposal_all_plus
      ?theta_every ?traj_every ?chol_every ?stoch_every
      ~alpha
      name adapt settings infer hypar path
      likelihood theta z0 nu old_o =
      let El pars = all_params settings infer hypar in
      let prior = Fit.Param.List.prior ((theta, z0), nu) pars in
      let custom_proposal = Fit.Param.List.custom_proposal pars in
      let draws = A.of_list (Fit.Param.List.adapt_draws pars) in
      let ndraws = A.length draws in
      let vdraw = Fit.Propose.vector_draw draws in
      let draws_ratio = Fit.Param.List.log_pd_ratio pars in
      let chol = new_chol pars old_o in
      let path = Printf.sprintf "%s.%s" path name in
      let columns = columns pars in
      let output = output
        ?theta_every
        ?traj_every
        ?chol_every
        ?stoch_every
        settings
        columns
        hypar
        chol
        path
      in
      let ram_proposal = Fit.Ram.Loop.Also.proposal
        ~adapt
        ~output
        ~alpha
        ?draws_ratio
        ~custom_proposal
        Fun.id
        Fun.id
        ndraws
        chol
        prior
        vdraw
        likelihood
      in
      ram_proposal, prior, output, (El pars, chol)

    let proposal_par_state
      ~alpha
      adapt infer hypar prior likelihood output old_o =
      let El pars = par_state_params_fulldim infer hypar in
      let custom_proposal = Fit.Param.List.custom_proposal pars in
      let draws = A.of_list (Fit.Param.List.adapt_draws pars) in
      let ndraws = A.length draws in
      let vdraw = Fit.Propose.vector_draw draws in
      let draws_ratio = Fit.Param.List.log_pd_ratio pars in
      let chol = new_chol pars old_o in
      let ram_proposal = Fit.Ram.Loop.Also.proposal
        ~adapt
        ~output
        ~alpha
        ?draws_ratio
        ~custom_proposal
        Fun.id
        Fun.id
        ndraws
        chol
        prior
        vdraw
        likelihood
      in
      ram_proposal, (El pars, chol)

    let proposal_scale_par_state
      ~alpha
      adapt infer hypar prior likelihood output =
      let draws = M.par_state_scale_draws infer hypar in
      let draws_ratio =
        let r = M.par_state_scale_draws_ratio infer hypar in
        U.Option.map (fun r (thz, _) (thz', _) -> r thz thz') r
      in
      (* hardcode 1 % *)
      let jumps = L.map (fun _ -> 0.01) draws in
      let ndraws = L.length draws in
      let vdraw =
        let vd = Fit.Propose.vector_draw (A.of_list draws) in
        (fun rng v (thz, nu) ->
          (vd rng v thz, nu)
        )
      in
      (* cannot use "new_chol" as we don't have the jumps in pars,
         so we must always create a new Chol matrix
         from the jumps *)
      let chol =
        jumps
        |> A.of_list
        |> Lac.Vec.of_array
        |> Lac.Mat.of_diag
      in
      Fit.Ram.Loop.Also.proposal
        ~adapt
        ~output
        ~alpha
        ?draws_ratio
        Fun.id
        Fun.id
        ndraws
        chol
        prior
        vdraw
        likelihood

    let proposal_stoch
      ~alpha
      adapt settings infer hypar prior likelihood output old_o =
      let El pars = stoch_params_fulldim settings infer hypar in
      let custom_proposal = Fit.Param.List.custom_proposal pars in
      let draws = A.of_list (Fit.Param.List.adapt_draws pars) in
      let ndraws = A.length draws in
      let vdraw = Fit.Propose.vector_draw draws in
      let draws_ratio = Fit.Param.List.log_pd_ratio pars in
      let chol = new_chol pars old_o in
      let ram_proposal = Fit.Ram.Loop.Also.proposal
        ~adapt
        ~output
        ~alpha
        ?draws_ratio
        ~custom_proposal
        Fun.id
        Fun.id
        ndraws
        chol
        prior
        vdraw
        likelihood
      in
      ram_proposal, (El pars, chol)

    let proposal_simple
      ?theta_every ?traj_every ?chol_every ?stoch_every
      name adapt settings infer hypar path
      likelihood theta z0 nu old_o =
      let old_chol_all, old_chol_par_state, old_chol_stoch =
        match old_o with
        | None ->
            None, None, None
        | Some (a, ps, s) ->
            Some a, Some ps, Some s
      in
      let pw = Hyper.proposal_weights hypar in
      (* scale RAM adaptation weights by proposal weights *)
      let w = F.to_float F.Op.(
          pw.joint
        + pw.par_state
        + pw.scale_par_state
        + pw.stoch
      )
      in
      let a_joint = F.to_float pw.joint /. w in
      let a_par_state = F.to_float pw.par_state /. w in
      let a_scale = F.to_float pw.scale_par_state /. w in
      let a_stoch = F.to_float pw.stoch /. w in
      let ram_prop_all, prior, output, new_chol_all =
        proposal_all_plus
          ?theta_every
          ?traj_every
          ?chol_every
          ?stoch_every
          ~alpha:a_joint
          name
          adapt
          settings
          infer
          hypar
          path
          likelihood
          theta
          z0
          nu
          old_chol_all
      in
      let ram_prop_par_state, new_chol_par_state =
        proposal_par_state
          ~alpha:a_par_state
          adapt
          infer
          hypar
          prior
          likelihood
          output
          old_chol_par_state
      in
      let ram_prop_scale_par_state =
        proposal_scale_par_state
          ~alpha:a_scale
          adapt
          infer
          hypar
          prior
          likelihood
          output
      in
      let ram_prop_stoch, new_chol_stoch =
        proposal_stoch
          ~alpha:a_stoch
          adapt
          settings
          infer
          hypar
          prior
          likelihood
          output
          old_chol_stoch
      in
      let choices =
        (* don't propose on stoch if it is not inferred *)
        if Fit.Infer.is_free (M.infer_stochastic infer) then
          [
            (ram_prop_all, pw.joint) ;
            (ram_prop_par_state, pw.par_state) ;
            (ram_prop_scale_par_state, pw.scale_par_state) ;
            (ram_prop_stoch, pw.stoch) ;
          ]
        else
          [
            (ram_prop_par_state, F.one) ;
          ]
      in
      let start k tup =
        let smpl = Fit.Mh.Sample.weight prior likelihood tup in
        let res = Fit.Mh.Return.accepted Fun.id smpl in
        output.start k res ;
        smpl
      in
      let return k smplf =
        let res = Fit.Mh.Return.accepted Fun.id smplf in
        output.return k res
      in
      let new_chol =
        Some (new_chol_all, new_chol_par_state, new_chol_stoch)
      in
      Fit.Propose.Choice choices, start, return, new_chol

    (* I don't handle Sequential case here so far, hence (c, c) sequential *)
    let run_simple (type a b c) ?(verbose=true) ?seed ~path ~lik
      ~(settings : (a, b, c, c) settings) ~recipe hypar data theta =
      let likelihood = Likelihood.composite
        settings
        lik
        (M.simulate settings hypar)
        (M.hyper_to_sim hypar)
        data
      in
      let leg
        (((theta, z0), nu), old_o)
        { name ; adapt ; infer ;
          n_iter ; theta_every ; traj_every ; chol_every ; stoch_every ;
          _ } =
        if verbose then Printf.printf "MCMC step %s\n%!" name ;
        let prop, start, return, new_chol = proposal_simple
          ?theta_every
          ?traj_every
          ?chol_every
          ?stoch_every
          name
          adapt
          settings
          infer
          hypar
          path
          likelihood
          theta
          z0
          nu
          old_o
        in
        let smpl0 = start 0 ((theta, z0), nu) in
        let smplf = Fit.Markov_chain.direct_simulate
          ?seed
          prop
          smpl0
          n_iter
        in
        let _, ret, _ = return n_iter smplf in
        (Fit.Mh.Return.sample ret, new_chol)
      in
      output_recipe ~path recipe ;
      (* actually infer does not really matter,
       * as long as it's not fixed *)
      let nu : c = rand_nu ?seed (L.hd recipe).infer settings hypar in
      let bud = (
        ((M.to_par theta, M.to_state theta), nu),
        None
      ) in
      let ((thf, _), _), _ = L.fold_left leg bud recipe in
      thf

    let run_seqram
      (type a b c)
      ?(verbose=true) ?seed ~path ~lik
      ~(settings : (a, b, c, c list) settings) ~recipe
      hypar data theta =
      let pop = Settings.pop settings in
      let leg
        (((theta, x0), nus), old_o)
        { name ; infer ; n_iter ;
          theta_every ; traj_every ; stoch_every ; chol_every ;
          _ } =
        if verbose then Printf.printf "MCMC step %s\n%!" name ;
        match
          M.params infer hypar,
          M.params_state infer hypar
        with
        | pars_th, pars_state ->
        let elt_stgs = Settings.simple_element settings in
        let (_, par_stoch) as tagpar_stoch =
          M.param_stoch infer elt_stgs hypar
        in
        let pars_pair = append pars_th pars_state in
        let g = Settings.copy_stoch elt_stgs in
        let pars_all = put_at_end g pars_pair tagpar_stoch in
        let chol = new_chol pars_all old_o in
        let path = Printf.sprintf "%s.%s" path name in
        let columns = columns pars_th in
        let output =
          let f ((th, zs), nus) =
            let x = M.pop_to_state pop (snd (L.hd zs)) in
            ((th, x), nus)
          in
          let map_res = Fit.Mh.Return.map f f in
          Fit.Csv.map
            ~return:(fun k res -> (k, map_res res, chol))
            map_res
            (output
              ?theta_every
              ?traj_every
              ?chol_every
              ?stoch_every
              settings
              columns
              hypar
              chol
              path
            )
        in
        let prior = Fit.Seqram.{
          theta_state = Fit.Param.List.prior (theta, x0) pars_pair ;
          nu = Fit.Param.prior par_stoch ;
        }
        in
        let draws : _ Fit.Seqram.draws = {
          theta = Fit.Param.List.adapt_draws pars_th ;
          state = Fit.Param.List.adapt_draws pars_state ;
          nu = Fit.Param.adapt_draw par_stoch ;
        }
        in
        let ratio : _ Fit.Seqram.ratio = {
          theta = Fit.Param.List.log_pd_ratio pars_th ;
          state = Fit.Param.List.log_pd_ratio pars_state ;
          nu = Fit.Param.log_pd_ratio par_stoch ;
        }
        in
        let prop = Fit.Seqcustom.{
          theta_state = Fit.Param.List.custom_proposal pars_pair ;
          nu = Fit.Param.custom_proposal par_stoch ;
        }
        in
        let diff_state =
          Fit.Param.List.adapt_diff
            (* FIXME not length, but number of draws ?
             * -> adapt length ? *)
            ~start:(1 + L.length draws.theta)
            pars_state
        in
        let augment_state = augment_state pop in
        let strip_state = strip_state pop in
        let _, ret, chol =
          (* we know how to handle two cases :
           * either everything is adaptive, or everything is custom *)
          if (prop.theta_state = Fit.Propose.Constant_flat)
          && (prop.nu = Fit.Propose.Constant_flat)
          then
            Fit.Seqram.posterior
              ~output
              ~n_iter
              ~p_redraw_nu:(Hyper.seq_p_redraw_stoch hypar)
              ~prior
              ~draws
              ~ratio
              chol
              diff_state
              augment_state
              strip_state
              (M.simulate_seq settings hypar)
              (M.obs_seq settings hypar)
              (Likelihood.sequential lik (M.hyper_to_sim hypar) data)
              theta x0 nus
          else
          if (L.length draws.theta = 0)
          && (L.length draws.state = 0)
          && (L.length draws.nu = 0)
          then
            Fit.Seqcustom.posterior
              ~output
              ~n_iter
              ~prior
              ~prop
              augment_state
              strip_state
              (M.simulate_seq settings hypar)
              (M.obs_seq settings hypar)
              (Likelihood.sequential lik (M.hyper_to_sim hypar) data)
              theta x0 nus
          else
            begin
              Printf.eprintf "prop th z : %s\n" (Fit.Propose.to_string prop.theta_state) ;
              Printf.eprintf "len draws th = %i\n" (L.length draws.theta) ;
              Printf.eprintf "len draws z = %i\n" (L.length draws.state) ;
              Printf.eprintf "prop nu : %s\n" (Fit.Propose.to_string prop.nu) ;
              Printf.eprintf "len draws nu = %i\n" (L.length draws.nu) ;
              invalid_arg "Partly adaptive, partly not"
            end
        in (Fit.Mh.Return.sample ret, Some (El pars_all, chol))
      in
      let nus = rand_nu ?seed (L.hd recipe).infer settings hypar in
      output_recipe ~path recipe ;
      let bud = (
        (
          (M.to_par theta, M.to_state theta),
          nus
        ),
        None
      )
      in
      let ((thf, _), _), _ = L.fold_left leg bud recipe in
      thf

    let run (type a b c d) ?verbose ?seed ~path ~lik
      ~(settings : (a, b, c, d) settings) ~recipe hypar data theta =
      match Settings.sequential settings with
      | Seq_elt ->
          invalid_arg "invalid sequential mode Seq_elt"
      | Simple ->
          run_simple
            ?verbose
            ?seed
            ~path
            ~lik
            ~settings
            ~recipe
            hypar
            data
            theta
      | Sequential ->
          begin match settings with
          | Sde (Simple | Seq_elt)
          | Prm (_, _, (Simple | Seq_elt)) ->
              assert false
          | Ode Sequential ->
              (* should be possible to implement though *)
              invalid_arg "Incompatible settings"
          | Sde Sequential ->
              run_seqram
                ?verbose
                ?seed
                ~path
                ~lik
                ~settings
                ~recipe
                hypar
                data
                theta
          | Prm (_, _, Sequential) ->
              run_seqram
                ?verbose
                ?seed
                ~path
                ~lik
                ~settings
                ~recipe
                hypar
                data
                theta
          end


    module Term =
      struct
        module C = Cmdliner

        module Infer =
          struct
            let infer_tags =
              M.infer_tags
              |> L.map (fun tag -> (M.tag_to_string tag, tag))
              |> C.Arg.enum

            (* TODO for those could replace by functions
             * from Fit.Term,
             * but this would require a bit of refactoring
             *)

            let infer_custom_tags =
              let doc =
                  "Infer the specified parameter for all steps, "
                ^ "and use its custom proposal."
              in
              C.Arg.(
                value
                & opt_all infer_tags []
                & info ["infer-custom"] ~docv:"INFER-CUSTOM" ~doc
              )

            let infer_adapt_tags =
              let doc =
                  "Infer the specified parameter for all steps, "
                ^ "and use the adaptive proposal."
              in
              C.Arg.(
                value
                & opt_all infer_tags []
                & info ["infer-adapt"] ~docv:"INFER-ADAPT" ~doc
              )

            let infer_prior_tags =
              let doc =
                  "Infer the specified parameter for all steps, "
                ^ "and use its prior as proposal."
              in
              C.Arg.(
                value
                & opt_all infer_tags []
                & info ["infer-prior"] ~docv:"INFER-PRIOR" ~doc
              )

            let fix_tags =
              let doc =
                  "Fix the specified parameter to its current value "
                ^ "for all steps."
              in
              C.Arg.(
                value
                & opt_all infer_tags []
                & info ["fix"] ~docv:"FIX" ~doc
              )

            let infer_custom_step_tags =
              let doc =
                  "Infer the specified parameter for the specified step, "
                ^ "and use its custom proposal."
              in
              C.Arg.(
                value
                & opt_all (pair string infer_tags) []
                & info ["infer-custom-step"] ~docv:"INFER-CUSTOM-STEP" ~doc
              )

            let infer_adapt_step_tags =
              let doc =
                  "Infer the specified parameter for the specified step, "
                ^ "and use the adaptive proposal."
              in
              C.Arg.(
                value
                & opt_all (pair string infer_tags) []
                & info ["infer-adapt-step"] ~docv:"INFER-ADAPT-STEP" ~doc
              )

            let infer_prior_step_tags =
              let doc =
                  "Infer the specified parameter for the specified step, "
                ^ "and use its prior as proposal."
              in
              C.Arg.(
                value
                & opt_all (pair string infer_tags) []
                & info ["infer-prior-step"] ~docv:"INFER-PRIOR-STEP" ~doc
              )

            let fix_step_tags =
              let doc =
                  "Fix the specified parameter for the specified step "
                ^ "to its current value."
              in
              C.Arg.(
                value
                & opt_all (t2 string infer_tags) []
                & info ["fix-step"] ~docv:"FIX-STEP" ~doc
              )

            let infer_prior_step =
              let doc =
                  "For the specified step, use the prior as proposal for all "
                ^ "parameters being inferred."
              in
              C.Arg.(
                value
                & opt_all string []
                & info ["infer-prior-step-all"] ~docv:"INFER-PRIOR-STEP-ALL" ~doc
              )
          end

        let infer_step =
          let f fix_tags fix_step_tags
                custom_tags custom_step_tags
                adapt_tags adapt_step_tags
                prior_tags prior_step_tags
                prior_names
                name =
            let valid (name', tag) =
              if name = name' then
                Some tag
              else
                None
            in
            (* tags for this step *)
            let fix = L.filter_map valid fix_step_tags in
            let custom = L.filter_map valid custom_step_tags in
            let adapt = L.filter_map valid adapt_step_tags in
            let prior = L.filter_map valid prior_step_tags in
            M.default_infer
            (* global settings *)
            |> L.fold_right M.fix fix_tags
            |> L.fold_right (M.infer `Custom) custom_tags
            |> L.fold_right (M.infer `Adapt) adapt_tags
            |> L.fold_right (M.infer `Prior) prior_tags
            (* globally switch all params in custom+adapt tags to `Prior *)
            |> (if L.mem name prior_names then begin
                  L.iter (fun tag ->
                    Printf.printf "%s switching %s to prior\n"
                    name (M.tag_to_string tag)
                  ) (custom_tags @ adapt_tags) ;
                  L.fold_right (M.infer `Prior) (custom_tags @ adapt_tags)
                end else
                  fun x -> x
               )
            (* step specific settings *)
            |> L.fold_right M.fix fix
            |> L.fold_right (M.infer `Custom) custom
            |> L.fold_right (M.infer `Adapt) adapt
            |> L.fold_right (M.infer `Prior) prior
          in
          Infer.(
          C.Term.(
            const f
            $ fix_tags
            $ fix_step_tags
            $ infer_custom_tags
            $ infer_custom_step_tags
            $ infer_adapt_tags
            $ infer_adapt_step_tags
            $ infer_prior_tags
            $ infer_prior_step_tags
            $ infer_prior_step
          )
          )

        let recipe =
          let f theta_every traj_every chol_every stoch_every
                steps infer_steps =
            L.fold_left (fun recipe ->
              fun (name, adapt, n_iter) ->
                let infer = infer_steps name in
                {
                  name ;
                  adapt ;
                  infer ;
                  n_iter ;
                  theta_every ;
                  traj_every ;
                  chol_every ;
                  stoch_every ;
                } :: recipe
            ) [] steps
          in
          C.Term.(
            const f
            $ Term.Mcmc.theta_every
            $ Term.Mcmc.traj_every
            $ Term.Mcmc.chol_every
            $ Term.Mcmc.stoch_every
            $ Term.Mcmc.step
            $ infer_step
          )


        let data =
          let doc =
              "Path prefix to input dataset as CSV files. "
            ^ "Case data is loaded from '$(docv).data.cases.csv', "
            ^ "and should have columns 't' and 'cases'. "
            ^ "Prevalence data is loaded from '$(docv).data.prevalence.csv', "
            ^ "and should have columns 't' and 'prevalence'. "
          in
          let f path_o =
            match path_o with
            | None ->
                Data.empty
            | Some path ->
                Cli.load_data path
          in
          let arg =
            C.Arg.(
              value
              & opt (some string) None
              & info ["data"] ~docv:"DATA" ~doc
            )
          in
          C.Term.(const f $ arg)

        (* for now no infer (the hardest part) *)
        let run =
          let flat seed path lik stgs recipe read_hypar data read_theta =
            let Term.Mcmc.Es settings = stgs in
            let capt_hy = Some (Printf.sprintf "%s.par.csv" path) in
            let capt_th = Some (Printf.sprintf "%s.theta0.csv" path) in
            let hypar = read_hypar capt_hy in
            let theta = read_theta capt_th in
            let theta_f = U.time_it_to
              (Printf.sprintf "%s.duration.csv" path)
              (run
                ~verbose:true
                ?seed
                ~path
                ~lik
                ~settings
                ~recipe
                hypar
                data)
              theta
            in
            (hypar, theta_f)
          in
          C.Term.(
            const flat
            $ Term.Mcmc.seed
            $ Term.Mcmc.path
            $ Term.Mcmc.likelihood
            $ Term.Mcmc.settings
            $ recipe
            $ Hyper.term
            $ data
            $ M.term
          )

        let info =
          let doc =
            "Estimate the parameters and the events of a "
          ^ M.model_name ^ " model with Robust Adaptive Metropolis "
          ^ "MCMC runs. "
          ^ "The parameters can be estimated with deterministic simulations "
          ^ "or with stochastic simulations via "
          ^ "Poisson Random Measure (PRM) augmentation. "
          ^ "In that case, 'sequential MCMC' can be used. "
          ^ "Parameters can be set to be fixed or inferred independently, "
          ^ "in multiple different steps."
          in
          Cmdliner.Term.info
            ("epifit-" ^ M.model_name)
            ~version:"%%VERSION%%"
            ~doc
            ~exits:Cmdliner.Term.default_exits
            ~man:[]
      end
  end

open Sig


class init_sepir_base = object(self)
  val s0_v = float_of_int Epi.Default_init.s0
  val ei0_v : float option = None
  val r0_v = float_of_int Epi.Default_init.r0

  method s0 = s0_v
  method r0 = r0_v

  method with_s0 x = {< s0_v = x >}
  method with_r0 x = {< r0_v = x >}

  method ei0 ~latency =
    match ei0_v with
    | Some x ->
        x
    | None ->
        if not latency then
          float_of_int Epi.Default_init.i0
        else
          float_of_int (Epi.Default_init.e0 +  Epi.Default_init.i0)

  method with_ei0 x = {< ei0_v = Some x >}

  method sepir ~latency = (s0_v, self#ei0 ~latency, r0_v)
  method with_sepir (s0, ei0, r0) =
    {< s0_v = s0 ; ei0_v = Some ei0 ; r0_v = r0 >}
end


class init_sepir = object(self)
  inherit init_sepir_base

  method with_delta_s0_dx dx =
    {< s0_v = s0_v +. dx ;
       r0_v = r0_v -. dx >}

  method with_delta_s0 x =
    let dx = x -. s0_v in
    self#with_delta_s0_dx dx

  method with_delta_ei0_dx dx =
    {< s0_v = s0_v -. 1. /. 2. *. dx ;
       ei0_v = Some (U.Option.some ei0_v +. dx) ;
       r0_v = r0_v -. 1. /. 2. *. dx >}

  method with_delta_ei0 x =
    let dx = x -. U.Option.some ei0_v in
    self#with_delta_ei0_dx dx

  method with_delta_r0_dx dx =
    {< s0_v = s0_v -. dx ;
       r0_v = r0_v +. dx >}

  method with_delta_r0 x =
    let dx = x -. r0_v in
    self#with_delta_r0_dx dx
end


class lik = object
  val t0_v : float option = None
  val tf_v = 1.
  val dt_v = 2e-2
  val csv_dt_v : float option = None
  val coal_var_v = 0.1
  val n_best_of_v = 10
  (* When those are 0. we get nans in the likelihood *)
  val cases_over_disp_v = 1e-6
  val coal_over_disp_v = 1e-6

  method t0 = t0_v
  method tf = tf_v
  method dt = dt_v
  method csv_dt = csv_dt_v
  method coal_var = coal_var_v
  method n_best_of = n_best_of_v
  method cases_over_disp = cases_over_disp_v
  method coal_over_disp = coal_over_disp_v

  method with_t0 x = {< t0_v = x >}
  method with_tf x = {< tf_v = x >}
  method with_dt x = {< dt_v = x >}
  method with_csv_dt x = {< csv_dt_v = x >}
  method with_coal_var x = {< coal_var_v = x >}
  method with_n_best_of x = {< n_best_of_v = x >}
  method with_cases_over_disp x = {< cases_over_disp_v = x >}
  method with_coal_over_disp x = {< coal_over_disp_v = x >}
end


class odeable = object
  val ode_apar_v = Sim.Ode.Lsoda.default

  method ode_apar = ode_apar_v

  method with_ode_apar x = {< ode_apar_v = x >}
end


class sim = object
  inherit lik
  inherit odeable
end


class prior_base = object
  val beta_prior_v : [`Uniform | `Lognormal | `Gamma] = `Lognormal
  val beta_mean_v = 70.
  val beta_var_v = 0.1
  val beta_max_v = 100.
  val beta_shape_v = 1.
  val nu_prior_v : [`Uniform | `Lognormal] = `Lognormal
  val nu_mean_v = 52.
  val nu_var_v = 0.1
  val nu_max_v = 100.
  val rho_range_v = 1.
  val sir_prior_v :
    [`Dirichlet | `Multinomial | `Dirichlet_multinomial] =
    `Dirichlet
  val sir_conc_v = 1.
  val popsize_v = 1e6
  val popsize_var_rel_v = 0.01
  val prevalence_v = 1e-4
  val sero_prevalence_v = 0.5

  (* get *)
  method beta_prior = beta_prior_v
  method beta_mean = beta_mean_v
  method beta_var = beta_var_v
  method beta_max = beta_max_v
  method beta_shape = beta_shape_v

  method nu_prior = nu_prior_v
  method nu_mean = nu_mean_v
  method nu_var = nu_var_v
  method nu_max = nu_max_v

  method rho_range = rho_range_v

  method sir_prior = sir_prior_v
  method sir_conc = sir_conc_v

  method popsize = popsize_v
  method popsize_var_rel = popsize_var_rel_v
  method prevalence = prevalence_v
  method sero_prevalence = sero_prevalence_v


  (* set *)
  method with_beta_prior x = {< beta_prior_v = x >}
  method with_beta_mean x = {< beta_mean_v = x >}
  method with_beta_var x = {< beta_var_v = x >}
  method with_beta_max x = {< beta_max_v = x >}
  method with_beta_shape x = {< beta_shape_v = x >}

  method with_nu_prior x = {< nu_prior_v = x >}
  method with_nu_mean x = {< nu_mean_v = x >}
  method with_nu_var x = {< nu_var_v = x >}
  method with_nu_max x = {< nu_max_v = x >}

  method with_rho_range x = {< rho_range_v = x >}

  method with_sir_prior x = {< sir_prior_v = x >}
  method with_sir_conc x = {< sir_conc_v = x >}

  method with_popsize x = {< popsize_v = x >}
  method with_popsize_var_rel x = {< popsize_var_rel_v = x >}
  method with_prevalence x = {< prevalence_v = x >}
  method with_sero_prevalence x = {< sero_prevalence_v = x >}
end


class prop_base = object
  val fix_popsize_v = false
  val beta_jump_v = 5e-2
  val nu_jump_v = 5e-2
  val rho_jump_v = 5e-3
  val sr_jump_v = 1e-3
  val ei_jump_v = 1e-6
  val psir_jump_v = 1e-6

  method fix_popsize = fix_popsize_v
  method beta_jump = beta_jump_v
  method nu_jump = nu_jump_v
  method rho_jump = rho_jump_v
  method sr_jump = sr_jump_v
  method ei_jump = ei_jump_v
  method psir_jump = psir_jump_v

  method with_fix_popsize x = {< fix_popsize_v = x >}
  method with_beta_jump x = {< beta_jump_v = x >}
  method with_nu_jump x = {< nu_jump_v = x >}
  method with_rho_jump x = {< rho_jump_v = x >}
  method with_sr_jump x = {< sr_jump_v = x >}
  method with_ei_jump x = {< ei_jump_v = x >}
  method with_psir_jump x = {< psir_jump_v = x >}
end


class prior_latent = object
  val sigma_prior_v : [`Uniform | `Lognormal] = `Lognormal
  val sigma_mean_v = 30.
  val sigma_var_v = 0.1
  val sigma_max_v = 100.
  (* val exp_prevalence_v = 1e-4 *)

  method sigma_prior = sigma_prior_v
  method sigma_mean = sigma_mean_v
  method sigma_var = sigma_var_v
  method sigma_max = sigma_max_v
  (* method exp_prevalence = exp_prevalence_v *)

  method with_sigma_prior x = {< sigma_prior_v = x >}
  method with_sigma_mean x = {< sigma_mean_v = x >}
  method with_sigma_var x = {< sigma_var_v = x >}
  method with_sigma_max x = {< sigma_max_v = x >}
  (* method with_exp_prevalence x = {< exp_prevalence_v = x >} *)
end


class prop_latent = object
  val sigma_jump_v = 5e-2

  method sigma_jump = sigma_jump_v

  method with_sigma_jump x = {< sigma_jump_v = x >}
end


class prior_circular = object
  val gamma_prior_v : [`Uniform | `Lognormal] = `Lognormal
  val gamma_mean_v = 0.2
  val gamma_var_v = 1e-2
  val gamma_max_v = 1.
  val eta_mean_v = 1.

  method gamma_prior = gamma_prior_v
  method gamma_mean = gamma_mean_v
  method gamma_var = gamma_var_v
  method gamma_max = gamma_max_v
  method eta_mean = eta_mean_v

  method with_gamma_prior x = {< gamma_prior_v = x >}
  method with_gamma_mean x = {< gamma_mean_v = x >}
  method with_gamma_var x = {< gamma_var_v = x >}
  method with_gamma_max x = {< gamma_max_v = x >}
  method with_eta_mean x = {< eta_mean_v = x >}
end


class prop_circular = object
  val betavar_jump_v = 1e-2
  val phase_jump_v = 1e-2
  val gamma_jump_v = 1e-3
  val eta_jump_v = 1e-3

  method betavar_jump = betavar_jump_v
  method phase_jump = phase_jump_v
  method gamma_jump = gamma_jump_v
  method eta_jump = eta_jump_v

  method with_betavar_jump x = {< betavar_jump_v = x >}
  method with_phase_jump x = {< phase_jump_v = x >}
  method with_gamma_jump x = {< gamma_jump_v = x >}
  method with_eta_jump x = {< eta_jump_v = x >}
end


class prior_stochastic = object
  val prm_max_height_v : float option = None
  val prm_min_width_v : float option = None

  method prm_max_height = prm_max_height_v
  method prm_min_width = prm_min_width_v

  method with_prm_max_height x = {< prm_max_height_v = x >}
  method with_prm_min_width x = {< prm_min_width_v = x >}
end


class prop_stochastic = object
  val prm_nredraws_v = 1
  val prm_ntranges_v = 1
  val prm_jump_v = 0.01
  val dbt_jump_v = 0.01
  val seq_p_redraw_stoch_v = 1.
  val pw_v = Proposal_weights.default

  method prm_nredraws = prm_nredraws_v
  method prm_ntranges = prm_ntranges_v
  method prm_jump = prm_jump_v
  method dbt_jump = dbt_jump_v
  method seq_p_redraw_stoch = seq_p_redraw_stoch_v
  method proposal_weights = pw_v

  method with_prm_nredraws n = {< prm_nredraws_v = n >}
  method with_prm_ntranges n = {< prm_ntranges_v = n >}
  method with_prm_jump x = {< prm_jump_v = x >}
  method with_dbt_jump x = {< dbt_jump_v = x >}
  method with_seq_p_redraw_stoch x = {< seq_p_redraw_stoch_v = x >}
  method with_proposal_weights x = {< pw_v = x >}
end




let fpof = F.Pos.of_float
let ipoi = I.Pos.of_int

let sepir ~latency p =
  let s, ei, r = p#sepir ~latency in
  (fpof s, fpof ei, fpof r)

let t0 p = Option.map F.of_float p#t0
let tf p = F.of_float p#tf
let dt p = fpof p#dt

let csv_dt p =
  match p#csv_dt with
  | None ->
      dt p
  | Some dt ->
      fpof dt

let ode_apar p = p#ode_apar
let width p =
  F.Pos.narrow p#ode_apar.Sim.Ode.Lsoda.mimic_h

let prm_max_height p =
  U.Option.map fpof p#prm_max_height

let prm_min_width p =
  U.Option.map fpof p#prm_min_width

let n_best_of p = p#n_best_of

let t0_or_0 p =
  Option.value ~default:F.zero (t0 p)

let duration p =
  F.Pos.of_anyfloat F.Op.(tf p - t0_or_0 p)

let with_h x p =
  p#with_ode_apar Sim.Ode.Lsoda.{ p#ode_apar with mimic_h = x }

let prm_ntslices p = ipoi p#prm_ntslices
let prm_nuslices p = ipoi p#prm_nuslices

let seed_range p = p#seed_range
let beta_mean p = p#beta_mean
let beta_var p = fpof p#beta_var
let beta_max p = p#beta_max
let beta_shape p = fpof p#beta_shape
let nu_mean p = p#nu_mean
let nu_var p = fpof p#nu_var
let nu_max p = p#nu_max
let rho_range p = F.Proba.of_float p#rho_range
let popsize p = fpof p#popsize

let popsize_var p =
  let rel = fpof p#popsize_var_rel in
  F.Pos.mul rel (F.sq (popsize p))

let prevalence p = F.Proba.of_float p#prevalence
let sero_prevalence p = F.Proba.of_float p#sero_prevalence
let sir_concentration p = F.Pos.of_float p#sir_conc

let beta_jump p = fpof p#beta_jump
let nu_jump p = fpof p#nu_jump
let rho_jump p = fpof p#rho_jump
let sr_jump p = fpof p#sr_jump
let ei_jump p = fpof p#ei_jump
let psir_jump p = fpof p#psir_jump

let sigma_mean p = p#sigma_mean
let sigma_var p = fpof p#sigma_var
let sigma_max p = p#sigma_max
(* let exp_prevalence p = fpof p#exp_prevalence *)

let sigma_jump p = fpof p#sigma_jump

let gamma_mean p = p#gamma_mean
let gamma_var p = fpof p#gamma_var
let gamma_max p = p#gamma_max
let eta_mean p = fpof p#eta_mean

let betavar_jump p = fpof p#betavar_jump
let phase_jump p = fpof p#phase_jump
let gamma_jump p = fpof p#gamma_jump
let eta_jump p = fpof p#eta_jump

let prm_nredraws p = ipoi p#prm_nredraws
let prm_ntranges p = ipoi p#prm_ntranges

let prm_jump p = fpof p#prm_jump
let dbt_jump p = fpof p#dbt_jump

let seq_p_redraw_stoch p = F.Proba.of_float p#seq_p_redraw_stoch
let proposal_weights p = p#proposal_weights

let coal_var p = F.Pos.of_float p#coal_var
let cases_over_disp p = F.Pos.of_float p#cases_over_disp
let coal_over_disp p = F.Pos.of_float p#coal_over_disp

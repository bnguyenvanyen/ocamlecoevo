open Sig


let base_pars = [
  `Beta ;
  `Nu ;
  `Rho ;
]


let state = [
  `Sir ;
  `I0 ;
]


let base = base_pars @ state


let latent = [
  `Sigma ;
]

let circular = [
  `Betavar ;
  `Phase ;
  `Gamma ;
  `Eta ;
]

let stochastic = [
  `Stoch ;
]


let kill_none f x =
  match f x with
  | Some y ->
      y
  | None ->
      assert false


module Read =
  struct
    let beta =
      function
      | "beta" ->
          Some `Beta
      | _ ->
          None

    let betavar =
      function
      | "betavar" ->
          Some `Betavar
      | _ ->
          None

    let phase =
      function
      | "phase" ->
          Some `Phase
      | _ ->
          None

    let sigma =
      function
      | "sigma" ->
          Some `Sigma
      | _ ->
          None

    let nu =
      function
      | "nu" ->
          Some `Nu
      | _ ->
          None

    let gamma =
      function
      | "gamma" ->
          Some `Gamma
      | _ ->
          None

    let rho =
      function
      | "rho" ->
          Some `Rho
      | _ ->
          None

    let eta =
      function
      | "eta" ->
          Some `Eta
      | _ ->
          None

    let sir =
      function
      | "sir" ->
          Some `Sir
      | _ ->
          None

    let i0 =
      function
      | "i0" ->
          Some `I0
      | _ ->
          None

    let stoch =
      function
      | "stoch" ->
          Some `Stoch
      | _ ->
          None
         
    let combine l =
      L.fold_left (fun g f ->
        fun s ->
        match g s with
        | None ->
            f s
        | (Some _) as some_tag ->
            some_tag
      ) (fun _ -> None) l
  end


module Write =
  struct
    let any =
      function
      | `Beta ->
          "beta"
      | `Betavar ->
          "betavar"
      | `Phase ->
          "phase"
      | `Sigma ->
          "sigma"
      | `Nu ->
          "nu"
      | `Gamma ->
          "gamma"
      | `Rho ->
          "rho"
      | `Eta ->
          "eta"
      | `Sir ->
          "sir"
      | `I0 ->
          "i0"
      | `Stoch ->
          "stoch"

    (* for 'theta' output *)
    let columns =
      function
      | (`Beta | `Betavar | `Phase | `Sigma
        | `Nu | `Gamma | `Rho | `Eta) as symb ->
          [any symb]
      | `Sir ->
          ["s0" ; "i0" ; "r0"]
      | `I0 | `Stoch ->
          []
  end

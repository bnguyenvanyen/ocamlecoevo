open Sig


type settings = likelihood_settings = {
  cases : [`None | `Poisson | `Negbin | `Prevalence | `Prevalence_negbin] ;
  sero : [`None | `Aggregate | `Stratified] ;
  alpha : float option ;
}


let none = {
  cases = `None ;
  sero = `None ;
  alpha = None
}


let traj_log_dens_proba ?scale loglikfs hypar data =
  let t0 = F.to_float (Param.t0_or_0 hypar) in
  let data = Data.restrict ~t0 ~dt:hypar#dt ~tf:hypar#tf data in
  let hdloglikfs = L.map (fun f -> f hypar data) loglikfs in
  match hdloglikfs with
  | [] ->
      (fun _ -> [0.])
  | _ ->
      begin match scale with
      | None ->
          (fun rhotraj ->
            L.fold_left (fun l f ->
              l @ (f rhotraj)
            ) [] hdloglikfs
          )
      | Some alphas ->
          (fun rhotraj ->
            L.fold_left2 (fun l f alph ->
              l @ (L.map (fun x -> alph *. x) (f rhotraj))
            ) [] hdloglikfs alphas
          )
      end


(* might have to think about making this a theta dist for efficiency *)
let as_dist
  (type a b c d) ?scale (settings : (a, b, c, d) Sig.settings)
  loglikfs (sim : 'th -> 'st -> b -> _) hypar data :
  (('th * 'st) * c, float list) D.t =
  let draw _ =
    failwith "Not_implemented"
  in
  let traj_ldp = traj_log_dens_proba ?scale loglikfs hypar data in
  let log_dens_proba : ('th * 'st) * c -> float list =
    match settings with
    | Ode _ ->
        (fun ((theta, z0), ()) ->
          match traj_ldp (sim theta z0 ()) with
          | logps ->
              logps
          | exception Fit.Simulation_error _ ->
              [neg_infinity]
        )
    | Sde _ ->
        (fun ((theta, z0), dbts) ->
          match traj_ldp (sim theta z0 dbts) with
          | logps ->
              logps
          | exception Fit.Simulation_error _ ->
              [neg_infinity]
        )
    | Prm (_, No_intensities, _) ->
        (fun ((theta, z0), nu) ->
          match traj_ldp (sim theta z0 nu) with
          | logps ->
              logps
          | exception Fit.Simulation_error _ ->
              [neg_infinity]
        )
    | Prm (_, Intensities, _) ->
        (fun ((theta, z0), (nu, _)) ->
          match traj_ldp (sim theta z0 nu) with
          | logps ->
              logps
          | exception Fit.Simulation_error _ ->
              [neg_infinity]
        )
  in
  D.Base { draw ; log_dens_proba }


let flat _ _ _ =
  D.Constant_flat ([], [], [], [])


let case ~obs hy Data.{ cases ; _ } =
  let draw _ =
    failwith "Not_implemented"
  in
  let logp =
    match cases with
    | None ->
        invalid_arg "No case count data"
    | Some cs ->
        Observe.reported_case ~obs hy cs
  in
  let log_dens_proba tr =
    [F.to_float (logp tr)]
  in
  Fit.Dist.Base { draw ; log_dens_proba }


(* This is float *)
let cases_loglik =
  function
  | `None ->
      None
  | (`Poisson | `Negbin) as obs ->
      Some (fun hypar Data.({ cases ; _ }) ->
        match cases with
        | None ->
            invalid_arg "Epifit.Likelihood.cases_loglik : no cases data"
        | Some c ->
            let f =
              Observe.reported_cases ~obs hypar c
            in
            (fun (_, traj) ->
              let rates = Epi.Traj.reporting_rate traj in
              f rates
            )
      )
  | (`Prevalence | `Prevalence_negbin) as obs ->
      Some (fun hypar Data.({ prevalence ; _ }) ->
        match prevalence with
        | None ->
            invalid_arg "cases_loglik with `Prevalence needs prevalence data"
        | Some prev ->
            let f = Observe.reported_prevalence ~obs hypar prev in
            (fun (rho, traj) ->
              let i_traj =
                traj
                |> Epi.Traj.infected
                |> Jump.map I.Pos.of_float
              in f rho i_traj
            )
      )


let sero_loglik =
  function
  | `None ->
      None
  | `Aggregate ->
      Some (fun _ Data.({ sero_agg ; _ }) ->
        match sero_agg with
        | None ->
            invalid_arg "Epifit.Likelihood.sero_loglik : no sero data"
        | Some sero ->
            let f = Observe.aggregate_sero sero in
            (fun (_, traj) ->
              f (Epi.Traj.removed_proportion traj)
            )
      )
  | `Stratified ->
      failwith "Not_implemented"


let composite
  (type a b c d) (settings : (a, b, c, d) Sig.settings)
  { cases ; sero ; alpha } =
  let casesf = cases_loglik cases in
  let serof = sero_loglik sero in
  (* keep only the loglik components that are [Some] *)
  let loglikfs = L.filter_map (fun x -> x) [ casesf ; serof ] in
  let scale =
    match alpha with
    | None ->
        None
    | Some alph ->
        Some [1. ; alph]
  in
  as_dist ?scale settings loglikfs


let sequential { cases ; sero ; alpha } hy data =
  begin match sero with
  | `None ->
      ()
  | `Aggregate | `Stratified ->
      failwith "Not_implemented : sero data in sequential"
  end
  ;
  begin match alpha with
  | None ->
      ()
  | Some _ ->
      failwith "Not_implemented : multiple sources of data in sequential"
  end
  ;
  match cases with
  | (`Prevalence | `Prevalence_negbin) ->
      failwith "Not_implemented : prevalence data in sequential"
  | `None ->
      Fit.Dist.Flat
  | (`Poisson | `Negbin) as obs ->
      case ~obs hy data

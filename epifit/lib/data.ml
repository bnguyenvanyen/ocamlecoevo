(** Data to compute the likelihood on.
 *
 *  The data is divided into several independant sources,
 *  each with its own log-likelihood function :
 *
 *  - [cases] corresponds to the periodic count of observed cases
 *    for the epidemic.
 *    The likelihood depends on the trajectory of total cases accumulated
 *    during the study period.
 *
 *  - [sequences] corresponds to the pathogen sequences sampled in the
 *    population, with the time of sampling, and already aligned.
 *
 *  - [trees] corresponds to the (timed) phylogenies reconstructed
 *    from the sequences.
 *
 *  - [coal] corresponds to the inferred pair lineage coalescence rate
 *    trajectory in the population of interest,
 *    inferred for instance by Bayesian skyline from the sequences.
 *
 *  Not all of the sources of data might be required together for inference.
 *  Actually it doesn't make too much sense to use more than one of
 *  [coal], [sequences] and [trees] as they are clearly not independent :
 *  they correspond to different stages of information extraction
 *  from the sequence data.
 *
 *)

open Sig


type t = data = {
  cases : int J.cadlag option ;
  (** number of cases on each time interval *)
  prevalence : int J.cadlag option ;
  (** prevalence counted at each time *)
  sero_agg : (float * int * U.anyposint) list option ;
  (** aggregated sero data (t, n_seropos, n_cohort) *)
}


let empty = {
  cases = None ;
  prevalence = None ;
  sero_agg = None ;
  (* sero_strat = None ; *)
}


let translate t0 data =
  let cases = U.Option.map (J.translate (~-. t0)) data.cases in
  let prevalence = U.Option.map (J.translate (~-. t0)) data.prevalence in
  let sero_agg = U.Option.map (L.map (fun (t, n_pos, n_tot) ->
    (t -. t0, n_pos, n_tot)
  )) data.sero_agg
  in
  {
    cases ;
    prevalence ;
    sero_agg ;
  }


let restrict ~t0 ~dt ~tf data =
  let tol = dt /. 100. in
  let cases =
    match data.cases with
    | None ->
        None
    | Some c ->
        let c' =
          match J.cut_left (t0 +. dt) c with
          | c' ->
              c'
          | exception Invalid_argument _ ->
              (* t0 +. dt is smaller than the start of cases,
               * we just return cases *)
              c
        in
        let c'' = J.cut_right_after ~tol tf c' in
        Some c''
  in
  let prevalence =
    match data.prevalence with
    | None ->
        None
    | Some p ->
        let p' =
          match J.cut_left t0 p with
          | p' ->
              p'
          | exception Invalid_argument _ ->
              (* same *)
              p
        in
        let p'' = J.cut_right_after ~tol tf p' in
        Some p''
  in
  let sero_agg =
    match data.sero_agg with
    | None ->
        None
    | Some sa ->
        Some (L.filter (fun (t, _, _) -> (t0 <= t) && (t <= tf)) sa)
  in { cases ; prevalence ; sero_agg }

open Sig


type theta = Episim.theta


class hyper_prior = object
  inherit Param.prior_base
  inherit Param.prior_stochastic
  inherit Param.prior_circular
end


class hyper_prop = object
  inherit Param.prop_base
  inherit Param.prop_stochastic
  inherit Param.prop_circular
end


class hyper = object
  inherit hyper_prior
  inherit hyper_prop
  inherit Param.sim
end


class infer_pars_ode = object
  inherit Infer.base
  inherit Infer.circular
end


class infer_pars = object
  inherit infer_pars_ode
  inherit Infer.stochastic
end


type tag = [
  | Tag.base
  | Tag.circular
  | Tag.stochastic
]


module S = Episim.Make (Epi.Sirs.Model)


module Hyper =
  struct
    class t = hyper

    let t0 = Param.t0
    let tf = Param.tf
    let dt = Param.dt
    let width = Param.width
    let n_best_of = Param.n_best_of
    let seq_p_redraw_stoch = Param.seq_p_redraw_stoch
    let proposal_weights = Param.proposal_weights

    let uterm () =
      let f base circular lik stochastic load_theta ~config hy =
        hy
        |> base load_theta ~config
        |> circular ~config
        |> lik ~config
        |> stochastic ~config
      in Cmdliner.Term.(
          const f
        $ Term.Hyper.base ()
        $ Term.Hyper.circular ()
        $ Term.Hyper.lik ()
        $ Term.Hyper.stochastic ()
      )

    let capture_term =
      let f read load_theta ~config =
        (new hyper)
        |> U.Arg.Capture.empty
        |> read load_theta ~config
      in Cmdliner.Term.(
          const f
        $ uterm ()
      )

    let term =
      let f read load_theta config capture =
        U.Arg.Capture.output capture (read load_theta ~config)
      in
      Cmdliner.Term.(
        const f
        $ capture_term
        $ Term.Theta.load_from
        $ Term.Hyper.load_from
      )
  end


module Fit =
  struct
    type nonrec hyper = hyper

    type nonrec theta = theta

    type par = Epi.Param.t_unit

    type state = sir

    type infer = infer_pars

    type nonrec tag = tag

    let state_settings = Sir.Fit.state_settings

    let model_name = S.model_name

    let dims = S.dims

    let n_events = S.n_events

    let color_namer = S.Cont.color_namer

    let hyper_to_sim hy = (hy :> Param.sim)

    let to_state th =
      S.to_state state_settings th

    let state_to_pop x =
      S.state_to_pop state_settings x

    let pop_to_state z =
      S.pop_to_state state_settings z

    let to_par (th : theta) =
      (th :> par)

    let load_theta th = S.load_theta th

    let default_theta = new Episim.theta

    let default_infer = new infer_pars

    let infer_tags = Symbols.(base @ circular @ stochastic)

    let infer_gather f infer =
         (Infer.gather_base f infer)
      @@ (Infer.gather_circular f infer)
      @@ (Infer.gather_stochastic f infer)
      @@ []

    let tag_to_string = Symbols.Write.any

    let tag_to_columns =
      (* function from Sir to include 'ps0' 'pi0' 'pr0' *)
      Sir.Fit.tag_to_columns

    let infer mode =
      function
      | (#Tag.base | #Tag.stochastic) as tag ->
          Sir.Fit.infer mode tag
      | #Tag.circular as tag ->
          Infer.infer_circular mode tag

    let fix =
      function
      | (#Tag.base | #Tag.stochastic) as tag ->
          Sir.Fit.fix tag
      | #Tag.circular as tag ->
          Infer.fix_circular tag

    let infer_stochastic infer =
      infer#stoch

    type 'a par_enum =
      float * (
        float * (
          float * (
            float * (
              float * (
                float * (
                  float * 'a))))))

    type 'a state_enum =
      state * 'a

    let params infer hy =
      Fitsim.(Fit.Param.List.[
        beta hy infer#beta ;
        betavar hy infer#betavar ;
        phase hy infer#phase ;
        nu hy infer#nu ;
        gamma hy infer#gamma ;
        rho hy infer#rho ;
        eta hy infer#eta ;
      ])

    let params_state infer hy =
      Fit.Param.List.[Fitsim.sir Fun.id Fun.id hy infer#sir]

    let param_stoch
      (type a b c d)
      (infer : infer)
      (settings : (a, b, c, d) settings)
      hy =
      Fitsim.param_stoch
        settings
        n_events
        S.E.Prm.color_groups
        hy
        infer#stoch

    let par_state_scale_draws infer hyper =
      Sir.Fit.par_state_scale_draws infer hyper

    let par_state_scale_draws_ratio infer hyper =
      Sir.Fit.par_state_scale_draws_ratio infer hyper

    let simulate settings =
      S.sim_simple state_settings settings

    let simulate_csv ~chan settings =
      S.csv_sim state_settings settings ~chan

    let simulate_seq = S.sim_seq

    let obs_seq = S.obs_seq

    let extract th =
      let sof = string_of_float in
      function
      | "betavar" ->
          sof th#betavar
      | "phase" ->
          sof th#phase
      | "gamma" ->
          sof th#gamma
      | "eta" ->
          sof th#eta
      | s ->
          Sir.Fit.extract th s

    let extract_state = Sir.Fit.extract_state

    let term =
      let f base circular sir config capture =
        (new Episim.theta)
        |> U.Arg.Capture.empty
        |> base ~config
        |> circular ~config
        |> sir ~config
        |> U.Arg.Capture.output capture
      in Cmdliner.Term.(
        const f
        $ Epi.Term.Par.base ()
        $ Epi.Term.Par.circular ()
        $ Term.Theta.init_sir ~latency:Epi.Sirs.Model.spec.latency
        $ Term.Theta.load_from
      )
  end

open Sig


type ('a, 'b, 'c, 'd) t = ('a, 'b, 'c, 'd) settings


let pop : type a b c d. (a, b, c, d) t -> a pop =
  function
  | Ode _ ->
      Continuous
  | Sde _ ->
      Continuous
  | Prm _ ->
      Discrete


let sim_mode : type a b c d. (a, b, c, d) t -> b sim_mode =
  function
  | Ode _ ->
      Ode
  | Sde _ ->
      Sde
  | Prm (sim, _, _) ->
      Prm sim


let sequential :
  type a b c d. (a, b, c, d) settings -> (c, d) sequential =
  function
  | Ode seq ->
      seq
  | Sde seq ->
      seq
  | Prm (_, _, seq) ->
      seq


let prm_sim_mode :
  type a b c. (a, b, c) prm_settings -> a prm_sim_mode =
  function
  | Prm (sim, _, _) ->
      sim
  | Ode _
  | Sde _ ->
      assert false


let propose_prm :
  type a b c. (a, b, c) prm_settings -> (a, b) propose_prm =
  function
  | Ode _
  | Sde _ ->
      assert false
  | Prm (_, prop, _) ->
      prop


let propose :
  type a b c d. (a, b, c, d) settings -> (b, c) propose_prm =
  function
  | Ode _ ->
      No_intensities
  | Sde _ ->
      No_intensities
  | Prm (_, prop, _) ->
      prop


let simple :
  type a b c d. (a, b, c, d) settings -> (a, b, c, c) settings =
  function
  | Ode _ ->
      Ode Simple
  | Sde _ ->
      Sde Simple
  | Prm (sim, prop, _) ->
      Prm (sim, prop, Simple)


let simple_element :
  type a b c. (a, b, c, c list) settings -> (a, b, c, c) settings =
  function
  | Ode _ ->
      Ode Seq_elt
  | Sde _ ->
      Sde Seq_elt
  | Prm (sim, prop, _) ->
      Prm (sim, prop, Seq_elt)


let without_intensities :
  type a b. (a, b) propose_prm -> (b -> a) =
  function
  | No_intensities ->
      (fun nu -> nu)
  | Intensities ->
      (fun (nu, _) -> nu)


let simplify : type a b c d. (a, b, c, d) settings -> d -> b =
  function
  | Ode Simple ->
      (fun () -> ())
  | Ode Seq_elt ->
      (fun () -> ())
  | Ode Sequential ->
      (fun _ -> ())
  | Sde Simple ->
      (fun nu -> nu)
  | Sde Seq_elt ->
      (fun nu -> nu)
  | Sde Sequential ->
      Sim.Dbt.concat
  | Prm (_, intens, Simple) ->
      without_intensities intens
  | Prm (_, intens, Seq_elt) ->
      without_intensities intens
  | Prm (Exact, No_intensities, Sequential) ->
      Epi.Prm_time.concat
  | Prm (Approx, No_intensities, Sequential) ->
      Epi.Prm_color.concat
  | Prm (Fast, No_intensities, Sequential) ->
      Epi.Prm_cheat.concat
  | Prm (Exact, (Intensities as intens), Sequential) ->
      (fun nus ->
        nus
        |> L.map (without_intensities intens)
        |> Epi.Prm_time.concat
      )
  | Prm (Approx, (Intensities as intens), Sequential) ->
      (fun nus ->
        nus
        |> L.map (without_intensities intens)
        |> Epi.Prm_color.concat
      )
  | Prm (Fast, (Intensities as intens), Sequential) ->
      (fun nus ->
        nus
        |> L.map (without_intensities intens)
        |> Epi.Prm_cheat.concat
      )


let copy_sim : type b. b sim_mode -> (b -> b) =
  function
  | Ode ->
      (fun () -> ())
  | Sde ->
      Sim.Dbt.copy
  | Prm Exact ->
      Epi.Prm_time.copy
  | Prm Approx ->
      Epi.Prm_color.copy
  | Prm Fast ->
      Epi.Prm_cheat.copy


let copy_prop : type b c. (b -> b) -> (b, c) propose_prm -> (c -> c) =
  fun copy ->
  function
  | No_intensities ->
      copy
  | Intensities ->
      (fun (prm, intens) -> (copy prm, intens))


let copy_seq : type c d. (c -> c) -> (c, d) sequential -> (d -> d) =
  fun copy ->
  function
  | Simple ->
      copy
  | Seq_elt ->
      copy
  | Sequential ->
      L.map copy


let copy_stoch : type a b c d. (a, b, c, d) settings -> (d -> d) =
  fun stgs ->
  copy_seq (
    copy_prop (
      copy_sim (sim_mode stgs)
    ) (propose stgs)
  ) (sequential stgs)

open Sig

module C = Cmdliner


let float_arg ~doc ~get ~set name =
  U.Arg.Update.Capture.opt_config get set C.Arg.float ~doc name

let int_arg ~doc ~get ~set name =
  U.Arg.Update.Capture.opt_config get set C.Arg.int ~doc name

let float_opt_arg ~doc ~get ~set name =
  U.Arg.Update.Capture.opt_config get set C.Arg.(some float) ~doc name


module Theta =
  struct
    let load_from =
      let doc =
        "Load theta parameter values from $(docv)."
      in
      C.Arg.(
        value
        & opt (some string) None
        & info ["load-theta-from"] ~docv:"LOAD-THETA-FROM" ~doc
      )

    let s0 () =
      let doc =
        "Initial number of susceptible hosts."
      in
      let get th = th#s0 in
      let set x th = th#with_s0 x in
      float_arg ~get ~set ~doc "s0"

    (* the initialization for ei0 is a bit complex,
     * as we need to consider in order of priority :
     * - '--ei0' passed on the command line
     * - '--e0' or '--i0' passed on the command line,
     *   in which case the other value for e0 or i0 if necessary
     *   and not given on the command line,
     *   should be taken from the file, or from the default value
     * - 'ei0' in the file
     * - 'e0' and 'i0' in the file (both if necessary)
     *)

    let e0_opt =
      let doc =
        "Initial number of exposed hosts. Overridden by '--ei0'."
      in
      U.Arg.opt C.Arg.float ~doc "e0"

    let i0_opt =
      let doc =
        "Initial number of infectious hosts. Overridden by '--ei0'."
      in
      U.Arg.opt C.Arg.float ~doc "i0"

    let ei0_opt =
      let doc =
        "Initial number of infected hosts (E + I). Overrides '--e0' and '--i0'."
      in
      U.Arg.opt C.Arg.float ~doc "ei0"

    let read_opt name fname =
      U.Arg.read_csv_opt name (C.Arg.conv_parser C.Arg.float) fname

    let read_req name fname =
      U.Arg.read_csv name (C.Arg.conv_parser C.Arg.float) fname

    (* priorities :
     * - ei0 from command line
     * - e0 and/or i0 on command line,
     *   the other from file then default value if needed
     * - ei0 from file
     * - e0 and i0 from file
     * - e0 and i0 at default value
     *)
    let ei0_latency =
      let f ei0 e0 i0 ~config =
        match ei0 with
        | Some x ->
            x
        | None ->
            (* ei0 is not given on the command line,
             * see if e0 and/or i0 have been given on the command line *)
            begin match e0, i0 with
            | Some e0, Some i0 ->
                e0 +. i0
            | Some e0, None ->
                (* e0 is given on the command line,
                 * but not i0 :
                 * get i0 from the file or use its default value *)
                let i0 =
                  Option.value
                    ~default:(float_of_int Epi.Default_init.i0)
                    (Option.bind config (read_opt "i0"))
                in e0 +. i0
            | None, Some i0 ->
                (* get a value for missing e0 *)
                let e0 =
                  Option.value
                    ~default:(float_of_int Epi.Default_init.e0)
                    (Option.bind config (read_opt "e0"))
                in e0 +. i0
            | None, None ->
                (* neither e0 nor i0 are on the command line,
                 * so we look for ei0 in the file,
                 * but authorize it to be absent *)
                Option.value
                  ~default:(Option.value
                    ~default:(float_of_int (
                       Epi.Default_init.e0
                     + Epi.Default_init.i0
                    ))
                    (Option.bind config (fun f ->
                       U.Option.map2
                        (+.)
                        (read_opt "e0" f)
                        (read_opt "i0" f)
                    ))
                  )
                  (Option.bind config (read_opt "ei0"))
            end
        in
        C.Term.(const f $ ei0_opt $ e0_opt $ i0_opt)

    (* priorities :
     * - ei0 on the command line
     * - i0 on the command line
     * - ei0 in the file
     * - i0 in the file
     * - i0 default value
     *)
    let ei0_no_latency =
      let f ei0 i0 ~config =
        match ei0 with
        | Some x ->
            x
        | None ->
            (* ei0 is not given on the command line,
             * see if i0 has been given on the command line *)
            begin match i0 with
            | Some i0 ->
                i0
            | None ->
                (* i0 not on the command line,
                 * so we look for ei0 in the file,
                 * but authorize it to be absent *)
                Option.value
                  ~default:(Option.value
                    ~default:(float_of_int Epi.Default_init.i0)
                    (Option.bind config (read_opt "i0"))
                  )
                  (Option.bind config (read_opt "ei0"))
            end
        in
        C.Term.(const f $ ei0_opt $ i0_opt)

    let ei0 ~latency =
      let some term =
        C.Term.(const (fun f ~config -> Some (f ~config)) $ term)
      in
      let term =
        if latency then
          some ei0_latency
        else
          some ei0_no_latency
      in
      let get th = th#ei0 ~latency in
      let set x th = th#with_ei0 x in
      let f ei0 ~config =
        U.Arg.Update.Capture.update_capture_default
          get
          set
          Cmdliner.Arg.float
          "ei0"
          (Option.map U.Arg.Capture.empty (ei0 ~config))
      in
      Cmdliner.Term.(
        const f
        $ term
      )

    let r0 () =
      let doc =
        "Initial number of resistant hosts."
      in
      let get th = th#r0 in
      let set x th = th#with_r0 x in
      float_arg ~get ~set ~doc "r0"

    let init_sir ~latency =
      let f s0 ei0 r0 ~config th =
        th
        |> s0 ~config
        |> ei0 ~config
        |> r0 ~config
      in Cmdliner.Term.(
        const f
        $ s0 ()
        $ ei0 ~latency
        $ r0 ()
      )

    let update_full ~latency =
      let f base latent circular init_sir ~config th =
        th
        |> base ~config
        |> latent ~config
        |> circular ~config
        |> init_sir ~config
      in Cmdliner.Term.(
        const f
        $ Epi.Term.Par.base ()
        $ Epi.Term.Par.latent ()
        $ Epi.Term.Par.circular ()
        $ init_sir ~latency
      )
  end


module Hyper =
  struct
    let load_from =
      let doc =
        "Load (hyper) parameter values from $(docv)."
      in
      C.Arg.(
        value
        & opt (some string) None
        & info ["load-par-from"] ~docv:"LOAD-PAR-FROM" ~doc
      )

    (* prior_base *)

    let prior_choice_arg ~doc ~get ~set name =
      U.Arg.Update.Capture.opt_config
        get
        set
        (C.Arg.enum [
          ("uniform", `Uniform) ;
          ("lognormal", `Lognormal) ;
        ])
        ~doc
        name


    let beta_prior () =
      let doc =
        "Choose between lognormal, uniform, and gamma prior for beta."
      in
      let get hy = hy#beta_prior in
      let set x hy = hy#with_beta_prior x in
      U.Arg.Update.Capture.opt_config
        get
        set
        (C.Arg.enum [
          ("uniform", `Uniform) ;
          ("lognormal", `Lognormal) ;
          ("gamma", `Gamma) ;
        ])
        ~doc
        "beta-prior"

    let beta_mean () =
      let doc =
        "Mean of the beta prior if lognormal."
      in
      let get hy = hy#beta_mean in
      let set x hy = hy#with_beta_mean x in
      float_arg ~get ~set ~doc "beta-mean"

    let beta_var () =
      let doc =
        "10-log-variance of the beta prior if lognormal."
      in
      let get hy = hy#beta_var in
      let set x hy = hy#with_beta_var x in
      float_arg ~get ~set ~doc "beta-var"

    let beta_max () =
      let doc =
        "Upper bound of the beta prior if uniform."
      in
      let get hy = hy#beta_max in
      let set x hy = hy#with_beta_max x in
      float_arg ~get ~set ~doc "beta-max"

    let beta_shape () =
      let doc =
        "Shape of the beta prior if gamma."
      in
      let get hy = hy#beta_shape in
      let set x hy = hy#with_beta_shape x in
      float_arg ~get ~set ~doc "beta-shape"

    let nu_prior () =
      let doc =
        "Choose between lognormal and uniform prior for nu."
      in
      let get hy = hy#nu_prior in
      let set x hy = hy#with_nu_prior x in
      prior_choice_arg ~get ~set ~doc "nu-prior"

    let nu_mean () =
      let doc =
        "Mean of the nu prior if lognormal."
      in
      let get hy = hy#nu_mean in
      let set x hy = hy#with_nu_mean x in
      float_arg ~get ~set ~doc "nu-mean"

    let nu_var () =
      let doc =
        "10-log-variance of the nu prior if lognormal."
      in
      let get hy = hy#nu_var in
      let set x hy = hy#with_nu_var x in
      float_arg ~get ~set ~doc "nu-var"

    let nu_max () =
      let doc =
        "Upper bound of the nu prior if uniform."
      in
      let get hy = hy#nu_max in
      let set x hy = hy#with_nu_max x in
      float_arg ~get ~set ~doc "nu-max"

    let rho_range () =
      let doc =
        "Max possible value for the reporting probability rho. <= 1."
      in
      let get hy = hy#rho_range in
      let set x hy = hy#with_rho_range x in
      float_arg ~get ~set ~doc "rho-range"

    let sir_prior () =
      let doc =
        "Choose between Dirichlet, Multinomial, "
      ^ "and Dirichlet-Multinomial prior for state. "
      ^ "The Multinomial and Dirichlet-Multinomial prior "
      ^ "are usable only with SIR or SIRS."
      in
      let get hy = hy#sir_prior in
      let set x hy = hy#with_sir_prior x in
      U.Arg.Update.Capture.opt_config
        get
        set
        (C.Arg.enum [
          ("dirichlet", `Dirichlet) ;
          ("multinomial", `Multinomial) ;
          ("dirichlet-multinomial", `Dirichlet_multinomial) ;
        ])
        ~doc
        "sir-prior"

    let sir_concentration () =
      let doc =
        "Concentration of the SIR Dirichlet-Multinomial prior."
      in
      let get hy = hy#sir_conc in
      let set x hy = hy#with_sir_conc x in
      float_arg ~get ~set ~doc "sir-concentration"

    let popsize () =
      let set x hy = hy#with_popsize x in
      let f pop ~config =
        U.Arg.Update.Capture.update set (pop ~config)
      in
      Cmdliner.Term.(
        const f
        $ Epi.Term.Par.popsize
      )

    let popsize_var () =
      let doc =
        "Relative variance of the initial host population size."
      in
      let get hy = hy#popsize_var_rel in
      let set x hy = hy#with_popsize_var_rel x in
      float_arg ~get ~set ~doc "popsize-var"

    let prevalence () =
      let doc =
        "Initial expected proportion of infectious hosts."
      in
      let get hy = hy#prevalence in
      let set x hy = hy#with_prevalence x in
      float_arg ~get ~set ~doc "prevalence"

    let sero_prevalence () =
      let doc =
        "Initial expected proportion of immune hosts."
      in
      let get hy = hy#sero_prevalence in
      let set x hy = hy#with_sero_prevalence x in
      float_arg ~get ~set ~doc "sero-prevalence"

    (* prop_base *)

    let fix_popsize () =
      let doc =
        "Keep initial popsize constant."
      in
      let get hy = hy#fix_popsize in
      let set x hy = hy#with_fix_popsize x in
      U.Arg.Update.Capture.flag_config get set ~doc "fix-popsize"

    let beta_jump () =
      let doc =
        "Standard deviation of beta proposal."
      in
      let get hy = hy#beta_jump in
      let set x hy = hy#with_beta_jump x in
      float_arg ~get ~set ~doc "beta-jump"

    let nu_jump () =
      let doc =
        "Standard deviation of nu proposal."
      in
      let get hy = hy#nu_jump in
      let set x hy = hy#with_nu_jump x in
      float_arg ~get ~set ~doc "nu-jump"

    let rho_jump () =
      let doc =
        "Standard deviation of rho proposal."
      in
      let get hy = hy#rho_jump in
      let set x hy = hy#with_rho_jump x in
      float_arg ~get ~set ~doc "rho-jump"

    let sr_jump () =
      let doc =
        "Standard deviation of S0 and R0 initial proportion."
      in
      let get hy = hy#sr_jump in
      let set x hy = hy#with_sr_jump x in
      float_arg ~get ~set ~doc "sr-jump"

    let ei_jump () =
      let doc =
        "Standard deviation of E0 / I0 initial proportion."
      in
      let get hy = hy#ei_jump in
      let set x hy = hy#with_ei_jump x in
      float_arg ~get ~set ~doc "ei-jump"


    (* lik *)

    let t0 () =
      let doc =
        "Start simulations at $(docv)."
      in
      let get hy = hy#t0 in
      let set x hy = hy#with_t0 x in
      float_opt_arg ~get ~set ~doc "t0"

    let tf () =
      let doc =
        "End simulations at $(docv)."
      in
      let get hy = hy#tf in
      let set x hy = hy#with_tf x in
      float_arg ~get ~set ~doc "tf"

    let dt () =
      let doc =
        "Time interval between datapoints."
      in
      let get hy = hy#dt in
      let set x hy = hy#with_dt x in
      float_arg ~get ~set ~doc "dt"

    let csv_dt () =
      let doc =
        "Write a point every $(docv) in traj output. Default 'dt'."
      in
      let get hy = hy#csv_dt in
      let set x hy = hy#with_csv_dt x in
      float_opt_arg ~get ~set ~doc "csv-dt"

    let coal_var () =
      let doc =
        "Expected error variance on coalescence rate estimates."
      in
      let get hy = hy#coal_var in
      let set x hy = hy#with_coal_var x in
      float_arg ~get ~set ~doc "coal-var"

    let n_best_of () =
      let doc =
        "Repeat sequential simulations $(docv) times and keep the max."
      in
      let get hy = hy#n_best_of in
      let set x hy = hy#with_n_best_of x in
      int_arg ~get ~set ~doc "nbest-of"

    let cases_over_disp () =
      let doc =
        "Overdispersion of case count data."
      in
      let get hy = hy#cases_over_disp in
      let set x hy = hy#with_cases_over_disp x in
      float_arg ~get ~set ~doc "cases-over-disp"
    
    let coal_over_disp () =
      let doc =
        "Overdispersion of coalescent effective population size data."
      in
      let get hy = hy#coal_over_disp in
      let set x hy = hy#with_coal_over_disp x in
      float_arg ~get ~set ~doc "coal-over-disp"

    let ode_apar () =
      let f update_apar ~config hy =
        let upd' =
          U.Arg.Update.Capture.map
            (fun hy -> hy#ode_apar)
            (fun apar hy -> hy#with_ode_apar apar)
            (update_apar ~config)
        in
        upd' hy
      in
      C.Term.(
        const f
        $ Sim.Ode.Lsoda.Term.apar
      )


    (* prior_latent *)

    let sigma_prior () =
      let doc =
        "Choose between uniform and lognormal prior for sigma."
      in
      let get hy = hy#sigma_prior in
      let set x hy = hy#with_sigma_prior x in
      prior_choice_arg ~get ~set ~doc "sigma-prior"

    let sigma_mean () =
      let doc =
        "Mean of the sigma prior if lognormal."
      in
      let get hy = hy#sigma_mean in
      let set x hy = hy#with_sigma_mean x in
      float_arg ~get ~set ~doc "sigma-mean"
         
    let sigma_var () =
      let doc =
        "10-log-variance of the sigma prior if lognormal."
      in
      let get hy = hy#sigma_var in
      let set x hy = hy#with_sigma_var x in
      float_arg ~get ~set ~doc "sigma-var"

    let sigma_max () =
      let doc =
        "Upper bound of the sigma prior if uniform."
      in
      let get hy = hy#sigma_max in
      let set x hy = hy#with_sigma_max x in
      float_arg ~get ~set ~doc "sigma-max"

    (* prop_latent *)

    let sigma_jump () =
      let doc =
        "Standard deviation of sigma proposal."
      in
      let get hy = hy#sigma_jump in
      let set x hy = hy#with_sigma_jump x in
      float_arg ~get ~set ~doc "sigma-jump"


    (* prior_circular *)

    let gamma_prior () =
      let doc =
        "Choose between uniform and lognormal prior for gamma."
      in
      let get hy = hy#gamma_prior in
      let set x hy = hy#with_gamma_prior x in
      prior_choice_arg ~get ~set ~doc "gamma-prior"

    let gamma_mean () =
      let doc =
        "Mean of the gamma prior."
      in
      let get hy = hy#gamma_mean in
      let set x hy = hy#with_gamma_mean x in
      float_arg ~get ~set ~doc "gamma-mean"

    let gamma_var () =
      let doc =
        "Variance of the 10-lognormal gamma prior."
      in
      let get hy = hy#gamma_var in
      let set x hy = hy#with_gamma_var x in
      float_arg ~get ~set ~doc "gamma-var"

    let gamma_max () =
      let doc =
        "Upper bound of the gamma prior if uniform."
      in
      let get hy = hy#gamma_max in
      let set x hy = hy#with_gamma_max x in
      float_arg ~get ~set ~doc "gamma-max"

    let eta_mean () =
      let doc =
        "Mean of the exponential eta prior."
      in
      let get hy = hy#eta_mean in
      let set x hy = hy#with_eta_mean x in
      float_arg ~get ~set ~doc "eta-mean"


    (* prop_circular *)

    let betavar_jump () =
      let doc =
        "Standard deviation of betavar proposal."
      in
      let get hy = hy#betavar_jump in
      let set x hy = hy#with_betavar_jump x in
      float_arg ~get ~set ~doc "betavar-jump"

    let phase_jump () =
      let doc =
        "Standard deviation of phase proposal."
      in
      let get hy = hy#phase_jump in
      let set x hy = hy#with_phase_jump x in
      float_arg ~get ~set ~doc "phase-jump"

    let gamma_jump () =
      let doc =
        "Standard deviation of gamma proposal."
      in
      let get hy = hy#gamma_jump in
      let set x hy = hy#with_gamma_jump x in
      float_arg ~get ~set ~doc "gamma-jump"

    let eta_jump () =
      let doc =
        "Standard deviation of eta proposal."
      in
      let get hy = hy#eta_jump in
      let set x hy = hy#with_eta_jump x in
      float_arg ~get ~set ~doc "eta-jump"

    (* prior_stochastic *)

    let float_opt_arg ~doc ~get ~set name =
      U.Arg.Update.Capture.opt_config
        get
        set
        (C.Arg.some C.Arg.float)
        ~doc
        name

    let prm_max_height () =
      let doc =
        "Maximum height of the base prm uk slice."
      in
      let get hy = hy#prm_max_height in
      let set x hy = hy#with_prm_max_height x in
      float_opt_arg ~get ~set ~doc "prm-max-height"

    let prm_min_width () =
      let doc =
        "Width for the minimum volume of the base prm uk slice."
      in
      let get hy = hy#prm_min_width in
      let set x hy = hy#with_prm_min_width x in
      float_opt_arg ~get ~set ~doc "prm-min-width"

    (* prop_stochastic *)

    let prm_nredraws () =
      let doc =
        "Number of color slices to redraw on proposals for prm."
      in
      let get hy = hy#prm_nredraws in
      let set x hy = hy#with_prm_nredraws x in
      int_arg ~get ~set ~doc "prm-nredraws"

    let prm_ntranges () =
      let doc =
        "Number of time ranges to consider for adaptive proposals for prm."
      in
      let get hy = hy#prm_ntranges in
      let set x hy = hy#with_prm_ntranges x in
      int_arg ~get ~set ~doc "prm-ntranges"

    let prm_jump () =
      let doc =
        "Standard deviation of prm proposal."
      in
      let get hy = hy#prm_jump in
      let set x hy = hy#with_prm_jump x in
      float_arg ~get ~set ~doc "prm-jump"

    let dbt_jump () =
      let doc =
        "Standard deviation of dbt proposal."
      in
      let get hy = hy#dbt_jump in
      let set x hy = hy#with_dbt_jump x in
      float_arg ~get ~set ~doc "dbt-jump"

    let seq_p_redraw_stoch () =
      let doc =
          "Probability to redraw the stoch component for a given step "
        ^ "of an iteration in Seqram mode."
      in
      let get hy = hy#seq_p_redraw_stoch in
      let set x hy = hy#with_seq_p_redraw_stoch x in
      float_arg ~get ~set ~doc "seq-p-redraw-stoch"

    let proposal_weights () =
      let set x hy = hy#with_proposal_weights x in
      let f pw =
        U.Arg.(
          pw
          |> Capture.empty
          |> Update.Capture.update_always set
        )
      in
      C.Term.(const f $ Proposal_weights.term)

    (* assemble *)

    let base () =
      let f beta_p beta_m beta_v beta_x beta_s beta_j
            nu_p nu_m nu_v nu_x nu_j
            rho_range rho_j sir_p sir_c
            psz psz_var fix_psz prev sero_prev sr_j i_j
            load_theta ~config hy =
        hy
        |> beta_p ~config
        |> beta_m ~config
        |> beta_v ~config
        |> beta_x ~config
        |> beta_s ~config
        |> beta_j ~config
        |> nu_p ~config
        |> nu_m ~config
        |> nu_v ~config
        |> nu_x ~config
        |> nu_j ~config
        |> rho_range ~config
        |> rho_j ~config
        |> sir_p ~config
        |> sir_c ~config
        (* read from theta *)
        |> psz ~config:load_theta
        |> psz_var ~config
        |> fix_psz ~config
        |> prev ~config
        |> sero_prev ~config
        |> sr_j ~config
        |> i_j ~config
      in
      C.Term.(
        const f
        $ beta_prior ()
        $ beta_mean ()
        $ beta_var ()
        $ beta_max ()
        $ beta_shape ()
        $ beta_jump ()
        $ nu_prior ()
        $ nu_mean ()
        $ nu_var ()
        $ nu_max ()
        $ nu_jump ()
        $ rho_range ()
        $ rho_jump ()
        $ sir_prior ()
        $ sir_concentration ()
        $ popsize ()
        $ popsize_var ()
        $ fix_popsize ()
        $ prevalence ()
        $ sero_prevalence ()
        $ sr_jump ()
        $ ei_jump ()
      )

    let latent () =
      let f sigma_p sigma_m sigma_v sigma_x sigma_j ~config hy =
        hy
        |> sigma_p ~config
        |> sigma_m ~config
        |> sigma_v ~config
        |> sigma_x ~config
        |> sigma_j ~config
      in
      C.Term.(
        const f
        $ sigma_prior ()
        $ sigma_mean ()
        $ sigma_var ()
        $ sigma_max ()
        $ sigma_jump ()
      )

    let circular () =
      let f bv_j gam_p gam_m gam_v gam_x gam_j eta_m eta_j ~config hy =
        hy
        |> bv_j ~config
        |> gam_p ~config
        |> gam_m ~config
        |> gam_v ~config
        |> gam_x ~config
        |> gam_j ~config
        |> eta_m ~config
        |> eta_j ~config
      in
      C.Term.(
        const f
        $ betavar_jump ()
        $ gamma_prior ()
        $ gamma_mean ()
        $ gamma_var ()
        $ gamma_max ()
        $ gamma_jump ()
        $ eta_mean ()
        $ eta_jump ()
      )

    let stochastic () =
      let f maxu minh ndraws nranges prm_j dbt_j p_redraw pw ~config hy =
        hy
        |> maxu ~config
        |> minh ~config
        |> ndraws ~config
        |> nranges ~config
        |> prm_j ~config
        |> dbt_j ~config
        |> p_redraw ~config
        |> pw
      in
      C.Term.(
        const f
        $ prm_max_height ()
        $ prm_min_width ()
        $ prm_nredraws ()
        $ prm_ntranges ()
        $ prm_jump ()
        $ dbt_jump ()
        $ seq_p_redraw_stoch ()
        $ proposal_weights ()
      )

    let lik () =
      let f t0 tf dt csv_dt coal_v n_best_of cases_ovd coal_ovd ode_apar ~config hy =
        hy
        |> t0 ~config
        |> tf ~config
        |> dt ~config
        |> csv_dt ~config
        |> coal_v ~config
        |> n_best_of ~config
        |> cases_ovd ~config
        |> coal_ovd ~config
        |> ode_apar ~config
      in
      C.Term.(
        const f
        $ t0 ()
        $ tf ()
        $ dt ()
        $ csv_dt ()
        $ coal_var ()
        $ n_best_of ()
        $ cases_over_disp ()
        $ coal_over_disp ()
        $ ode_apar ()
      )
  end


module Mcmc =
  struct
    module C = Cmdliner
    module Di = Default_init

    type e_settings =
      Es : (_, _, _, _) settings -> e_settings

    let seed =
      let doc = "Seed the PRNG with $(docv)." in
      C.Arg.(value
         & opt (some int) None
         & info ["seed"] ~docv:"SEED" ~doc
      )

    let path =
      let doc =
        "Output results to paths starting by $(docv)."
      in
      C.Arg.(required
         & pos 0 (some string) None
         & info [] ~docv:"PATH" ~doc
      )

    let likelihood_components =
      let doc =
        "Add the component $(docv) to the likelihood function."
      in
      let tag =
        C.Arg.enum (L.map (fun s -> (s, s)) Cli.likelihood_symbols)
      in
      let arg =
        C.Arg.(value
           & opt_all tag []
           & info ["likelihood"] ~docv:"LIKELIHOOD" ~doc
        )
      in
      let f lik_comps lik =
        L.fold_left Cli.tag_lik lik lik_comps
      in
      C.Term.(const f $ arg)

    let likelihood_scale =
      let doc =
        "Scale the coalescent likelihood by $(docv)."
      in
      let arg =
        C.Arg.(value
           & opt (some float) None
           & info ["lik-scale"] ~docv:"LIK-SCALE" ~doc
        )
      in
      let f alpha lik =
        Likelihood.{ lik with alpha }
      in
      C.Term.(const f $ arg)

    let likelihood =
      let f lik_comps lik_scale =
        Di.lik
        |> lik_comps
        |> lik_scale
      in
      C.Term.(const f
          $ likelihood_components
          $ likelihood_scale
      )

    let theta_every =
      let doc =
        "Output the estimated parameter values every $(docv) iterations."
      in
      C.Arg.(value
         & opt (some int) None
         & info ["theta-every"] ~docv:"THETA-EVERY" ~doc
      )

    let traj_every =
      let doc =
          "Output the estimated trajectory every $(docv) iterations. "
        ^ "By default do not output."
      in
      C.Arg.(value
         & opt (some int) None
         & info ["traj-every"] ~docv:"TRAJ-EVERY" ~doc
      )

    let chol_every =
      let doc =
          "Output the estimated covariance matrix Cholesky decomposition "
        ^ "every $(docv) iterations. By default do not output."
      in
      C.Arg.(value
         & opt (some int) None
         & info ["chol-every"] ~docv:"CHOL-EVERY" ~doc
      )

    let stoch_every =
      let doc =
          "Output the estimated Poisson random measure "
        ^ "every $(docv) iterations. By default do not output."
      in
      C.Arg.(value
         & opt (some int) None
         & info ["stoch-every"] ~docv:"STOCH-EVERY" ~doc
      )

    let settings =
      let doc =
        "Settings for simulation and inference"
      in
      let e = C.Arg.enum [
        ("ode", Es (Ode Simple)) ;
        ("sde", Es (Sde Simple)) ;
        ("sde_sequential", Es (Sde Sequential)) ;
        ("prm_exact", Es (Prm (Exact, Intensities, Simple))) ;
        ("prm_approx", Es (Prm (Approx, Intensities, Simple))) ;
        ("prm_fast", Es (Prm (Fast, Intensities, Simple))) ;
        ("ode_sequential", Es (Ode Sequential)) ;
        ("prm_exact_sequential", Es (Prm (Exact, Intensities, Sequential))) ;
        ("prm_approx_sequential", Es (Prm (Approx, Intensities, Sequential))) ;
        ("prm_fast_sequential", Es (Prm (Fast, Intensities, Sequential))) ;
      ] in
      C.Arg.(value
         & opt e (Es (Ode Simple))
         & info ["settings"] ~docv:"SETTINGS" ~doc
      )

    let step =
      let doc =
        "Add a step to the recipe, specified by 'name,adapt,n_iter'."
      in
      let arg =
        C.Arg.(
          value
          & opt_all (t3 string bool int) []
          & info ["step"] ~docv:"STEP" ~doc
        )
      in
      C.Term.(const L.rev $ arg)
  end

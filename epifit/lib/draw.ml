(** Most of the draws are already handled automatically
    through Fit.Param, but here we put additional scaling draws *)

open Sig


module D = Fit.Draw_lognormal

let scale = D.scale

let diff = D.diff

let draw = D.draw

let ratio = D.ratio

let if_adapt = D.if_adapt

let left = D.left_draw

let right = D.right_draw


let beta _ =
  draw
    ~get:(fun th -> th#beta)
    ~set:(fun th -> th#with_beta)


let nu _ =
  draw
    ~get:(fun th -> th#nu)
    ~set:(fun th -> th#with_nu)


let rho _ =
  draw
     ~get:(fun th -> th#rho)
     ~set:(fun th -> th#with_rho)


let sir_indep =
  let ds _ = draw
    ~get:(fun (sir : sir) -> sir.s)
    ~set:(fun (sir : sir) s -> { sir with s })
  in
  let di _ = draw
    ~get:(fun (sir : sir) -> sir.i)
    ~set:(fun (sir : sir) i -> { sir with i })
  in
  let dr _ = draw
    ~get:(fun (sir : sir) -> sir.r)
    ~set:(fun (sir : sir) r -> { sir with r })
  in
  [ds ; di ; dr]


let psir =
  let dpsr _ dx sir =
    let ps = sir.ps in
    let ps' = scale dx ps in
    let pr' = sir.pr +. (ps -. ps') in
    { sir with ps = ps' ;
               pr = pr' }
  in
  let dpi _ dx sir =
    let pi = sir.pi in
    let pi' = scale dx pi in
    let dpi = pi' -. pi in
    let pr' = sir.pr -. dpi in
    { sir with pi = pi' ;
               pr = pr' }
  in
  [dpsr ; dpi]


let sir_fixed =
  let dsr _ dx (sir : sir) =
    let s = sir.s in
    let s' = scale dx s in
    let r' = sir.r +. (s -. s') in
    { sir with s = s' ;
               r = r' }
  in
  let di _ dx (sir : sir) =
    let i = sir.i in
    let i' = scale dx i in
    let di = i' -. i in
    let r' = sir.r -. di in
    { sir with i = i' ;
               r = r' }
  in
  [dsr ; di]


let i_only =
  let di _ =
    draw
      ~get:(fun (sir : sir) -> sir.i)
      ~set:(fun (sir : sir) i -> { sir with i })
  in
  [di]
 

let sir infer hy =
  match infer with
  | `All infr ->
      if Fit.Infer.is_free_adapt infr then begin
        let sir =
          if hy#fix_popsize then
            sir_fixed
          else
            sir_indep
        in
        match hy#sir_prior with
        | `Dirichlet
        | `Multinomial ->
            sir
        | `Dirichlet_multinomial ->
            sir @ psir
      end else
        []
  | `Only_i infr ->
      if Fit.Infer.is_free_adapt infr then begin
        assert (hy#sir_prior = `Dirichlet) ;
        assert (not hy#fix_popsize) ;
        i_only
      end else
        []
        


let seir_indep =
  let ds _ = draw
    ~get:(fun seir -> seir.s)
    ~set:(fun seir s -> { seir with s })
  in
  let de _ = draw
    ~get:(fun seir -> seir.e)
    ~set:(fun seir e -> { seir with e })
  in
  let di _ = draw
    ~get:(fun seir -> seir.i)
    ~set:(fun seir i -> { seir with i })
  in
  let dr _ = draw
    ~get:(fun seir -> seir.r)
    ~set:(fun seir r -> { seir with r })
  in
  [ds ; de ; di ; dr]


let seir_fixed =
  let dsr _ dx seir =
    let s = seir.s in
    let s' = scale dx s in
    let r' = seir.r +. (s -. s') in
    { seir with s = s' ;
                r = r' }
  in
  let de _ dx seir =
    let e = seir.e in
    let e' = scale dx e in
    let de = e' -. e in
    let r' = seir.r -. de in
    { seir with e = e' ;
                r = r' }
  in
  let di _ dx seir =
    let i = seir.i in
    let i' = scale dx i in
    let di = i' -. i in
    let r' = seir.r -. di in
    { seir with i = i' ;
                r = r' }
  in
  [dsr ; de ; di]

 
let ei_only =
  let de _ =
    draw
      ~get:(fun seir -> seir.e)
      ~set:(fun seir e -> { seir with e })
  in
  let di _ =
    draw
      ~get:(fun seir -> seir.i)
      ~set:(fun seir i -> { seir with i })
  in
  [de ; di]


let seir infer hy =
  match infer with
  | `All infr ->
      if Fit.Infer.is_free_adapt infr then begin
        if hy#fix_popsize then
          seir_fixed
        else
          seir_indep
      end else
        []
  | `Only_i infr ->
      if Fit.Infer.is_free_adapt infr then begin
        assert (not hy#fix_popsize) ;
        ei_only
      end else
        []


module Ratio =
  struct
    let fold = D.fold_ratio

    let left = D.left_ratio

    let right = D.right_ratio

    let beta () =
      ratio ~get:(fun th -> th#beta)

    let nu () =
      ratio ~get:(fun th -> th#nu)

    let rho () =
      ratio ~get:(fun th -> th#rho)

    let sir_indep =
      let rs = ratio ~get:(fun (sir : sir) -> sir.s) in
      let ri = ratio ~get:(fun (sir : sir) -> sir.i) in
      let rr = ratio ~get:(fun (sir : sir) -> sir.r) in
      [ rs ; ri ; rr ]

    let psir =
      let rpsr = ratio ~get:(fun sir -> sir.ps) in
      let rpi = ratio ~get:(fun sir -> sir.pi) in
      [ rpsr ; rpi ]

    let sir_fixed =
      let rsr = ratio ~get:(fun (sir : sir) -> sir.s) in
      let ri = ratio ~get:(fun (sir : sir) -> sir.i) in
      [ rsr ; ri ]

    let i_only =
      let r = ratio ~get:(fun (sir : sir) -> sir.i) in
      [ r ]

    let sir infer hy =
      match infer with
      | `All infr ->
          if Fit.Infer.is_free_adapt infr then begin
            let sir =
              if hy#fix_popsize then
                sir_fixed
              else
                sir_indep
            in
            match hy#sir_prior with
            | `Dirichlet
            | `Multinomial ->
                sir
            | `Dirichlet_multinomial ->
                sir @ psir
          end else
            []
      | `Only_i infr ->
          if Fit.Infer.is_free_adapt infr then begin
            assert (hy#sir_prior = `Dirichlet) ;
            assert (not hy#fix_popsize) ;
            i_only
          end else
            []

    let seir_indep =
      let rs = ratio ~get:(fun seir -> seir.s) in
      let re = ratio ~get:(fun seir -> seir.e) in
      let ri = ratio ~get:(fun seir -> seir.i) in
      let rr = ratio ~get:(fun seir -> seir.r) in
      [ rs ; re ; ri ; rr ]

    let seir_fixed =
      let rsr = ratio ~get:(fun seir -> seir.s) in
      let re = ratio ~get:(fun seir -> seir.e) in
      let ri = ratio ~get:(fun seir -> seir.i) in
      [ rsr ; re ; ri ]

    let ei_only =
      let re = ratio ~get:(fun seir -> seir.e) in
      let ri = ratio ~get:(fun seir -> seir.i) in
      [ re ; ri ]

    let seir infer hy =
      match infer with
      | `All infr ->
          if Fit.Infer.is_free_adapt infr then begin
            if hy#fix_popsize then
              seir_fixed
            else
             seir_indep
          end else
            []
      | `Only_i infr ->
          if Fit.Infer.is_free_adapt infr then begin
            assert (not hy#fix_popsize) ;
            ei_only
          end else
            []
  end

open Sig


(* here we lose some type distinctions between S(E)IR(S)s *)
class theta = object
  inherit Epi.Param.t_unit
  inherit Param.init_sepir
end


module Make (M : Epi.Sig.MODEL_SPEC) =
  struct
    module E = Epi.Process.Unit.Make (M)
    module Cont = Epi.Process.Continuous.Make (M)

    type tmp = Lac.Vec.t * Lac.Vec.t * Lac.Vec.t

    let model_name = Epi.Seirco.model_name M.spec

    let dims = Cont.dims

    let n_events = Cont.n_events

    let ode_init () =
      let d = dims in
      Lac.Vec.((make0 d, make0 d, make0 d), make0 d)

    let copy : type a. a pop -> (a -> a) =
      function
      | Continuous ->
          (fun z -> Lac.copy z)
      | Discrete ->
          Epi.Pop.Unit.copy

    let to_state : type a. a state_settings -> (_ -> a) =
      function
      | Sir ->
          assert (not M.spec.latency) ;
          (fun th ->
            th
            |> Param.sepir ~latency:M.spec.latency
            |> Seirco.float_triplet
            |> Seirco.triplet_to_sir
          )
      | Seir ->
          assert M.spec.latency ;
          (fun th ->
            th
            |> Param.sepir ~latency:M.spec.latency
            |> Cont.sepir_to_seir th
            |> Seirco.float_tuple
            |> Seirco.tuple_to_seir
          )

    let state_to_pop :
      type a b. a state_settings -> b pop -> (a -> b) =
      function
      | Sir ->
          begin function
          | Continuous ->
              let _, z = ode_init () in
              (fun x ->
                x
                |> Seirco.sir_to_triplet
                |> Cont.of_sir ~z
              )
          | Discrete ->
              (fun x ->
                x
                |> Seirco.sir_round
                |> E.of_sir
              )
          end
      | Seir ->
          begin function
          | Continuous ->
              let _, z = ode_init () in
              (fun x ->
                x
                |> Seirco.seir_to_tuple
                |> Cont.of_seir ~z
              )
          | Discrete ->
              (fun x ->
                x
                |> Seirco.seir_round
                |> E.of_seir
              )
          end

    let to_pop state pop th =
      th
      |> to_state state
      |> state_to_pop state pop

    let pop_to_state :
      type a b. a state_settings -> b pop -> (b -> a) =
      function
      | Sir ->
          begin function
          | Continuous ->
              (fun z ->
                z
                |> Cont.to_sir
                |> Seirco.triplet_to_sir
              )
          | Discrete ->
              (fun z ->
                z
                |> E.to_sir
                |> Seirco.triplet_to_sir
              )
          end
      | Seir ->
          begin function
          | Continuous ->
              (fun z ->
                z
                |> Cont.to_seir
                |> Seirco.tuple_to_seir
              )
          | Discrete ->
              (fun z ->
                z
                |> E.to_seir
                |> Seirco.tuple_to_seir
              )
          end

    let ode_simulate ~output hy par () z0 =
      let t0 = Param.t0 hy in
      let tf = Param.tf hy in
      let apar = Param.ode_apar hy in
      let output = output par in
      Cont.Ode.sim ~output ~apar ?t0 par z0 tf

    let sde_simulate ~output par dbts z0 =
      let output = output par in
      Cont.Sde_limit.sim ~output par z0 dbts

    let prm_simulate (type a)
      ~(sim : a prm_sim_mode)
      ~output
      (par : Epi.Param.t_unit)
      (nu : a)
      z0 =
      let output = output par in
      try
        match sim with
        | Fast ->
            E.Prm_fast.sim par ~output z0 nu
        | Approx ->
            E.Prm_approx.sim par ~output z0 nu
        | Exact ->
            E.Prm_exact.sim par ~output z0 nu
      with Not_found ->  (* example Pop.remove *)
          raise Sim.Simulation_error

    let output (type a) (pop : a pop) out hy :
      (Epi.Param.t_unit -> (a, _, _) Sim.Sig.output) =
      let conv : (Epi.Param.t_unit -> U._float -> a -> _) =
        match pop with
        | Continuous ->
            Cont.conv
        | Discrete ->
            E.conv
      in
      let line : (Epi.Param.t_unit -> U._float -> a -> _) =
        match pop with
        | Continuous ->
            Cont.line ?k:None
        | Discrete ->
            E.line ?k:None
      in
      match out with
      | `Cadlag ->
          let dt = Param.dt hy in
          (fun par ->
            let conv = conv par in
            Sim.Cadlag.convert ~dt ~conv
          )
      | `Csv chan ->
          let dt = Param.csv_dt hy in
          let header = E.Csv.header ~append:false in
          (fun par ->
            let line = line par in
            Sim.Csv.convert_compat ~dt ~chan ~header ~line
          )

    let reraise_as_sim_error e =
      let bt = Printexc.get_raw_backtrace () in
      Printexc.raise_with_backtrace (Fit.Simulation_error e) bt

    let simulate
      (type a b c d e)
      ~out
      (state : e state_settings)
      (settings : (a, b, c, d) settings)
      hy =
      let pop = Settings.pop settings in
      let output = output pop out hy in
      let sim : _ -> b -> a -> _ =
        match settings with
        | Ode _ ->
            ode_simulate ~output hy
        | Sde _ ->
            sde_simulate ~output
        | Prm (Exact, _, _) ->
            prm_simulate ~sim:Exact ~output
        | Prm (Approx, _, _) ->
            prm_simulate ~sim:Approx ~output
        | Prm (Fast, _, _) ->
            prm_simulate ~sim:Fast ~output
      in
      (fun th z nu ->
        match sim th nu (state_to_pop state pop z) with
        | res ->
            res
        | exception (
            Invalid_argument _
          | Failure _
          | Not_found
          | Sim.Simulation_error
           as e) ->
            (* for instance bad param value *)
            reraise_as_sim_error e
      )

    let sim_simple state settings hy =
      let f = simulate ~out:`Cadlag state settings hy in
      (fun th z nu ->
        let res = f th z nu in
        (Epi.Param.rho th, res)
      )

    let sim_seq
      (type a b c) (settings : (a, b, c, c list) settings) _ :
      (_ -> c -> _ * a -> _ * a) =
      let output _ = U.Out.convert_null in
      let catch_and_reraise f par nu tz =
        match f par nu tz with
        | res ->
            res
        | exception (
            Invalid_argument _
          | Failure _
          | Not_found
          | Sim.Simulation_error
           as e) ->
            (* for instance bad param value *)
            reraise_as_sim_error e
      in
      match settings with
      | Sde (Simple | Seq_elt)
      | Prm (_, _, (Simple | Seq_elt)) ->
          assert false
      | Ode Sequential ->
          failwith "Not_implemented"
          (* FIXME implement with the right t0 and tf
          catch_and_reraise (fun par () (_, z) ->
            ode_simulate ~output hy par () (Lac.copy z)
          )
          *)
      | Sde Sequential ->
          catch_and_reraise (fun par nu (_, z) ->
            sde_simulate ~output par nu z
          )
      | Prm (sim, prop, Sequential) ->
          let no_intens = Settings.without_intensities prop in
          catch_and_reraise (fun par nu (_, z) ->
            prm_simulate ~sim ~output par (no_intens nu) (Epi.Pop.Unit.copy z)
          )

    let csv_sim ~chan state settings hy th z nu =
      ignore (
        simulate
          ~out:(`Csv chan)
          state
          settings
          hy
          th
          z
          (Settings.simplify settings nu)
      )

    let obs_seq (type a b c d) (settings : (a,b,c,d) settings)
      _ par (t, (x : a)) (t', x') =
      (* cases observations : we want the cumulative reporting rate,
       * which we approximate by the middle reporting rate between x and x',
       * times dt *)
      let conv : _ -> _ -> a -> _ = 
        match Settings.pop settings with
        | Discrete ->
            E.conv
        | Continuous ->
            Cont.conv
      in
      match conv par t x, conv par t' x' with
      | pt, pt' ->
          let obs_t = t' in
          let obs_binned_reportr = F.Pos.of_anyfloat F.Op.(
            (t' - t) * (pt.reportr + pt'.reportr) / F.two
          )
          in
          (obs_t, obs_binned_reportr)
      | exception (Invalid_argument _ | Failure _ as e) ->
          reraise_as_sim_error e

    (* We rewrite it because we need it to be different *)
    let load_par fname hyper =
      let hy' = Epi.Cli.load_par_from [
        Read.tf () ;
        Read.dt ()
      ] hyper fname
      in Read.size_from_seir0 fname hy'

    let theta_reads () = Epi.Read.Reader.(
      let fos = float_of_string in
        [s0 fos]
      @ [ei0 fos ; r0 fos]
      @ (if M.spec.circular then circular () else [])
      @ (if M.spec.latency then latent () else [])
      @ [beta () ; nu () ; rho ()]
    )

    let load_theta_row row theta =
      Epi.Cli.load_par_row (theta_reads ()) row theta

    let load_theta =
      function
      | None ->
          (fun th -> th)
      | Some s ->
          (fun th -> Epi.Cli.load_par_from (theta_reads ()) th s)

    let load_prm (type a) (sim : a prm_sim_mode) hy fname : a option =
      let t0 = Param.t0_or_0 hy in
      let duration = Param.duration hy in
      let width = Param.width hy in
      let time_range = (t0, duration) in
      let ntslices = Epi.Point.ntslices ~width duration in
      try
        match sim with
        | Fast ->
            Epi.Prm_cheat.read
              ~time_range
              ~ntslices
              Epi.Point.u0
              fname
        | Approx ->
            Epi.Prm_color.read
              ~time_range
              ~ntslices
              Epi.Point.u0
              fname
        | Exact ->
            Epi.Prm_time.read
              ~time_range
              ~ntslices
              Epi.Point.u0
              fname
      with Sys_error _ ->
        (* could Sys_error be other than 'no such file' ? *)
        None


    let sim_accepted
      (type a b c d)
      (settings : (a, b, c, c) settings)
      (state : d state_settings)
      hy th path =
      (* read 'accepted' lines from '.theta.csv' file *)
      let in_fname = Printf.sprintf "%s.theta.csv" path in
      let out_fname =
        match Settings.sim_mode settings with
        | Ode ->
            (fun k -> Printf.sprintf "%s.%i.traj.ode.csv" path k)
        | Sde ->
            (fun k -> Printf.sprintf "%s.%i.traj.sde.csv" path k)
        | Prm Exact ->
            (fun k -> Printf.sprintf "%s.%i.traj.prm_exact.csv" path k)
        | Prm Approx ->
            (fun k -> Printf.sprintf "%s.%i.traj.prm_approx.csv" path k)
        | Prm Fast ->
            (fun k -> Printf.sprintf "%s.%i.traj.prm_fast.csv" path k)
      in
      let in_chan = Csv.of_channel ~has_header:true (open_in in_fname) in
      let sim k th' (z' : d) (nu' : b) =
        let chan = open_out (out_fname k) in
        ignore (simulate ~out:(`Csv chan) state settings hy th' z' nu') ;
        close_out chan
      in
      (* simulate and output to out_fname file *)
      Csv.Rows.iter ~f:(fun row ->
        if Csv.Row.find row "rejected" = "accept" then
          let k = int_of_string (Csv.Row.find row "k") in
          (* Printf.eprintf "load_theta\n" ; *)
          let th' = load_theta_row row th in
          let z' = to_state state th' in
          let par' = (th' :> Epi.Param.t_unit) in
          match settings with
          | Ode _ ->
              sim k par' z' ()
          | Sde _ ->
              failwith "Not_implemented"
          | Prm (from_prm, _, _) ->
              let fname = Printf.sprintf "%s.%i.prm.points.csv" path k in
              Printf.eprintf "load_prm at %s\n%!" fname ;
              begin match load_prm from_prm hy fname with
              | Some nu' ->
                  Printf.eprintf "found prm file\n%!" ;
                  sim k par' z' nu'
              | None ->
                  ()
              end
      ) in_chan
  end

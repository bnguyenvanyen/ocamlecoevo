open Sig

module P = Param


let beta hypar =
  match hypar#beta_prior with
  | `Lognormal ->
      (* beta_mean is the actual mean, we need the log *)
      let m = log (P.beta_mean hypar) /. log 10. in
      D.Lognormal (m, P.beta_var hypar)
  | `Uniform ->
      D.Uniform (0., P.beta_max hypar)
  | `Gamma ->
      let alpha = P.beta_shape hypar in
      let m = F.Pos.of_float (P.beta_mean hypar) in
      let beta = F.Pos.div alpha m in
      D.Gamma (alpha, beta)


let betavar _ =
  D.Uniform (0., 1.)


let phase _ =
  D.Uniform (0., 1.)


let sigma hypar =
  match hypar#sigma_prior with
  | `Lognormal ->
      let m = log (P.sigma_mean hypar) /. log 10. in
      D.Lognormal (m, P.sigma_var hypar)
  | `Uniform ->
      D.Uniform (0., P.sigma_max hypar)


let nu hypar =
  match hypar#nu_prior with
  | `Lognormal ->
      let m = log (P.nu_mean hypar) /. log 10. in
      D.Lognormal (m, P.nu_var hypar)
  | `Uniform -> 
      D.Uniform (0., P.nu_max hypar)


let gamma hypar =
  match hypar#gamma_prior with
  | `Lognormal ->
      let m = log (P.gamma_mean hypar) /. log 10. in
      D.Lognormal (m, P.gamma_var hypar)
  | `Uniform ->
      D.Uniform (0., P.gamma_max hypar)


let rho hypar =
  D.Uniform (0., hypar#rho_range)


let eta hypar =
  let eta_mean = P.eta_mean hypar in
  D.Exponential F.Pos.Op.(F.one / eta_mean)


let draw_dirichlet_sir hypar =
  let open F.Pos.Op in
  let a = P.sir_concentration hypar in
  let a_i = a * P.prevalence hypar in
  let a_r = a * P.sero_prevalence hypar in
  let a_s = F.Pos.of_anyfloat F.Op.(a - a_i - a_r) in
  let alphas = [ a_s ; a_i ; a_r ] in
  let draw rng =
    match U.rand_dirichlet ~rng alphas with
    | [ ps ; pi ; pr ] ->
        let n =
          if hypar#fix_popsize then
            hypar#popsize
          else
            U.rand_normal ~rng hypar#popsize (P.popsize_var hypar)
        in
        let ps, pi, pr = F.to_float ps, F.to_float pi, F.to_float pr in
        {
          s = n *. ps ;
          i = n *. pi ;
          r = n *. pr ;
          ps ;
          pi ;
          pr ;
        }
    | _ ->
        (* cannot be reached *)
        assert false
  in draw


let draw_multinomial_sir hypar =
  let prev = P.prevalence hypar in
  let sero_prev = P.sero_prevalence hypar in
  let draw rng =
    let pi = prev in
    let pr = sero_prev in
    let ps = F.Proba.of_anyfloat F.Op.(F.one - pi - pr) in
    let n =
      if hypar#fix_popsize then
        I.Pos.of_int (int_of_float hypar#popsize)
      else
        I.Pos.of_int (int_of_float (
          U.rand_normal ~rng hypar#popsize (P.popsize_var hypar)
        ))
    in
    match
      U.rand_multinomial ~rng n [((), ps) ; ((), pi) ; ((), pr)]
    with
    | [(_, s) ; (_, i) ; (_, r)] ->
        {
          s = float_of_int (I.to_int s) ;
          i = float_of_int (I.to_int i) ;
          r = float_of_int (I.to_int r) ;
          ps = F.to_float ps ;
          pi = F.to_float pi ;
          pr = F.to_float pr ;
        }
    | _ ->
        (* cannot be reached *)
        assert false
  in draw


let draw_dirichlet_multinomial_sir hypar =
  let open F.Pos.Op in
  let a = P.sir_concentration hypar in
  let a_i = a * P.prevalence hypar in
  let a_r = a * P.sero_prevalence hypar in
  let a_s = F.Pos.of_anyfloat F.Op.(a - a_i - a_r) in
  let alphas = [ a_s ; a_i ; a_r ] in
  let draw rng =
    match U.rand_dirichlet ~rng alphas with
    | [ ps ; pi ; pr ] ->
        let n =
          if hypar#fix_popsize then
            I.Pos.of_int (int_of_float hypar#popsize)
          else
            I.Pos.of_int (int_of_float (
              U.rand_normal ~rng hypar#popsize (P.popsize_var hypar)
            ))
        in
        begin match
          U.rand_multinomial ~rng n [((), ps) ; ((), pi) ; ((), pr)] 
        with
        | [(_, s) ; (_, i) ; (_, r)] ->
            {
              s = float_of_int (I.to_int s) ;
              i = float_of_int (I.to_int i) ;
              r = float_of_int (I.to_int r) ;
              ps = F.to_float ps ;
              pi = F.to_float pi ;
              pr = F.to_float pr ;
            }
        | _ ->
            (* cannot be reached *)
            assert false
        end
    | _ ->
        (* cannot be reached *)
        assert false
  in draw


let log_dens_proba_dirichlet_sir hypar =
  let open F.Pos.Op in
  let a = P.sir_concentration hypar in
  let a_i = a * P.prevalence hypar in
  let a_r = a * P.sero_prevalence hypar in
  let a_s = F.Pos.of_anyfloat F.Op.(a - a_i - a_r) in
  let alphas = [ a_s ; a_i ; a_r ] in
  let log_dens_proba ({ s ; i ; r ; _ } : sir) =
    let n = s +. i +. r in
    if (i < 1e-30)
    || (s < 1e-30)
    || (r < 1e-30) then
      [neg_infinity]
    else
      let ps, pi, pr = s /. n, i /. n, r /. n in
      let logd = U.logd_dirichlet alphas [ ps ; pi ; pr ] in
      let logd' =
        if hypar#fix_popsize then
          F.zero
        else
          U.logd_normal
            hypar#popsize
            (P.popsize_var hypar)
            n
      in
      [F.to_float logd +. F.to_float logd']
  in log_dens_proba


let log_dens_proba_multinomial_sir hypar =
  (* we get ps, pi, pr from hypar instead of from state,
     because we are sure those are the correct values *)
  let prev = P.prevalence hypar in
  let sero_prev = P.sero_prevalence hypar in
  let log_dens_proba ({ s ; i ; r ; _ } : sir) =
    if (i < 0.)
    || (s < 0.)
    || (r < 0.)
    then
      [neg_infinity]
    else
      let pi = prev in
      let pr = sero_prev in
      let ps = F.Proba.of_anyfloat F.Op.(F.one - pi - pr) in
      let p = [ ps ; pi ; pr ] in
      let x = Seirco.simplex_round [ s ; i ; r ] in
      (* we consider that s i r is a multinomial draw
         out of their total *)
      let n = I.Pos.of_int (L.fold_left (+) 0 x) in
      let loglik = Pre.logp_multinomial n p x in
      (* additionally the total varies following a gaussian w.r.t popsize *)
      let loglik' =
        if hypar#fix_popsize then
          F.zero
        else
          U.logd_normal
            hypar#popsize
            (P.popsize_var hypar)
            (float (I.to_int n))
      in
      [F.to_float loglik +. F.to_float loglik']
  in log_dens_proba


let log_dens_proba_dirichlet_multinomial_sir hypar =
  let open F.Pos.Op in
  let a = P.sir_concentration hypar in
  let a_i = a * P.prevalence hypar in
  let a_r = a * P.sero_prevalence hypar in
  let a_s = F.Pos.of_anyfloat F.Op.(a - a_i - a_r) in
  let alphas = [ a_s ; a_i ; a_r ] in
  let log_dens_proba { s ; i ; r ; ps ; pi ; pr } =
    if (i < 0.)
    || (pi < 0.)
    || (s < 0.)
    || (ps < 0.)
    || (r < 0.)
    || (pr < 0.)
    then
      [neg_infinity]
    else
      (* dirichlet component *)
      let loglik = U.logd_dirichlet alphas [ ps ; pi ; pr ] in
      (* multinomial component *)
      let p = [
        F.Proba.of_float ps ;
        F.Proba.of_float pi ;
        F.Proba.of_float pr ;
      ]
      in
      let x = Seirco.simplex_round [ s ; i ; r ] in
      (* we consider that s i r is a multinomial draw
         out of their total *)
      let n = I.Pos.of_int (L.fold_left Int.add 0 x) in
      let loglik' = Pre.logp_multinomial n p x in
      (* additionally the total varies following a gaussian w.r.t popsize *)
      let loglik'' =
        if hypar#fix_popsize then
          F.zero
        else
          U.logd_normal
            hypar#popsize
            (P.popsize_var hypar)
            (float (I.to_int n))
      in
      [F.to_float loglik +. F.to_float loglik' +. F.to_float loglik'']
  in log_dens_proba



(* Dirichlet prior for (S, I, R) initial value. *)
let sir_dirichlet hypar =
  let draw = draw_dirichlet_sir hypar in
  let log_dens_proba = log_dens_proba_dirichlet_sir hypar in
  D.Base { draw ; log_dens_proba }


let sir_multinomial hypar =
  let draw = draw_multinomial_sir hypar in
  let log_dens_proba = log_dens_proba_multinomial_sir hypar in
  D.Base { draw ; log_dens_proba }


(* Dirichlet-Multinomial prior for (S, I, R) initial values. *)
let sir_dirichlet_multinomial hypar =
  let draw = draw_dirichlet_multinomial_sir hypar in
  let log_dens_proba = log_dens_proba_dirichlet_multinomial_sir hypar in
  D.Base { draw ; log_dens_proba }


let sir hypar =
  match hypar#sir_prior with
  | `Dirichlet ->
      sir_dirichlet hypar
  | `Multinomial ->
      sir_multinomial hypar
  | `Dirichlet_multinomial ->
      sir_dirichlet_multinomial hypar


(* draw s/r binomial sero_prev, i exponential,
 * log_dens_proba i exponential *)
let i_only hypar =
  (* proportion of the population that is I *)
  let prev = P.prevalence hypar in
  (* proportion of the population that is R *)
  let sero_prev = P.sero_prevalence hypar in
  let n = P.popsize hypar in
  let lbd_i = F.Pos.Op.(F.one / (prev * n)) in
  let draw rng =
    let i = U.rand_exp ~rng lbd_i in
    (* total to share between s and r *)
    let sr = F.Pos.of_anyfloat F.Op.(n - i) in
    (* approx binomial via normal *)
    let mr = F.to_float F.Op.(sero_prev * sr) in
    let vr = F.Pos.Op.(sero_prev * (F.Proba.compl sero_prev) * sr) in
    let r = F.Pos.of_float (U.rand_normal ~rng mr vr) in
    let s = F.Pos.of_anyfloat F.Op.(sr - r) in
    let ps, pi, pr = F.Pos.Op.(s / n, i / n, r / n) in
    {
      s = F.to_float s ;
      i = F.to_float i ;
      r = F.to_float r ;
      ps = F.to_float ps ;
      pi = F.to_float pi ;
      pr = F.to_float pr ;
    }
  in
  let log_dens_proba ({ s ; i ; r ; _ } : sir) =
    if (i < 0.)
    || (s < 0.)
    || (r < 0.) then
      [neg_infinity]
    else
      [F.to_float (U.logd_exp lbd_i i)]
  in D.Base { draw ; log_dens_proba }


(* Dirichlet prior for (S, E, I, R) initial value.
 * uniform in S and R, but biased towards
 * smaller values for E and I, depending on 'prevalence' hyper param *)
let seir hypar =
  let prev = P.prevalence hypar in
  let draw rng =
    (* total population size *)
    let n =
      F.Pos.of_float (
        U.rand_normal ~rng hypar#popsize (P.popsize_var hypar)
      )
    in
    let se = U.rand_exp ~rng F.one in
    let ee = U.rand_exp ~rng F.Pos.Op.(F.one / prev) in
    let ie = U.rand_exp ~rng F.Pos.Op.(F.one / prev) in
    let re = U.rand_exp ~rng F.one in
    let ne = F.Pos.Op.(se + ee + ie + re) in
    let a = F.Pos.Op.(n / ne) in
    let s, e, i, r = F.Pos.Op.((se * a, ee * a, ie * a, re * a)) in
    {
      s = F.to_float s ;
      e = F.to_float e ;
      i = F.to_float i ;
      r = F.to_float r ;
    }
  in
  (* 2 * prev *)
  let biprev = F.Pos.mul F.two prev in
  let cst = F.Op.(
      F.Pos.log biprev + F.Pos.log1p biprev 
    + F.Pos.loggamma biprev - F.two * F.Pos.loggamma prev
  )
  in
  let log_dens_proba { s ; e ; i ; r } =
    let n = s +. e +. i +. r in
    if (e < 0.)
    || (i < 0.)
    || (s < 0.)
    || (r < 0.)
    then
      [neg_infinity]
    else
      let le = if e = 0. then F.zero else F.of_float (log (e /. n)) in
      let li = if i = 0. then F.zero else F.of_float (log (i /. n)) in
      let loglik = F.to_float F.Op.(
        (prev - F.one) * le + (prev - F.one) * li + cst
      )
      in
      (* from difference on total population *)
      let loglik' =
        U.logd_normal hypar#popsize (P.popsize_var hypar) n
      in
      [loglik +. F.to_float loglik']
  in D.Base { draw ; log_dens_proba }


let ei_only hypar =
  let prev = P.prevalence hypar in
  let sero_prev = P.sero_prevalence hypar in
  let n = P.popsize hypar in
  let lbd = F.Pos.Op.(F.one / (prev * n)) in
  let draw rng =
    let e = U.rand_exp ~rng lbd in
    let i = U.rand_exp ~rng lbd in
    let sr = F.Pos.of_anyfloat F.Op.(n - e - i) in
    (* approx binomial via normal *)
    let mr = F.to_float F.Op.(sero_prev * sr) in
    let vr = F.Pos.Op.(sero_prev * (F.Proba.compl sero_prev) * sr) in
    let r = F.Pos.of_float (U.rand_normal ~rng mr vr) in
    let s = F.Pos.of_anyfloat F.Op.(sr - r) in
    {
      s = F.to_float s ;
      e = F.to_float e ;
      i = F.to_float i ;
      r = F.to_float r ;
    }
  in
  let log_dens_proba { s ; e ; i ; r } =
    if (i < 0.)
    || (e < 0.)
    || (s < 0.)
    || (r < 0.) then
      [neg_infinity]
    else
      [F.to_float (U.logd_exp lbd i) ; F.to_float (U.logd_exp lbd e)]
  in D.Base { draw ; log_dens_proba }


let prm_simple (type a b) (settings : (a, b, b) prm_settings) colors hypar :
  (b, float list) D.t =
  let t0 = P.t0 hypar in
  let tf = P.tf hypar in
  let max_height = P.prm_max_height hypar in
  let min_width = P.prm_min_width hypar in
  let width = P.width hypar in
  let vectors = Epi.Point.vectors ?max_height ?min_width ~width colors in
  match settings with
  | Ode _
  | Sde _
  | Prm (_, _, Sequential) ->
      assert false
  | Prm (_, _, Seq_elt) ->
      failwith "Not_implemented"
  | Prm (Exact, Intensities, Simple) ->
      Prmfit.Time.prior_intensities ~width ?t0 ~tf ~vectors colors
  | Prm (Exact, No_intensities, Simple) ->
      Prmfit.Time.prior ~width ?t0 ~tf ~vectors colors
  | Prm (Approx, Intensities, Simple) ->
      Prmfit.Color.prior_intensities ~width ?t0 ~tf ~vectors colors
  | Prm (Approx, No_intensities, Simple) ->
      Prmfit.Color.prior ~width ?t0 ~tf ~vectors colors
  | Prm (Fast, Intensities, Simple) ->
      Prmfit.Cheat.prior_intensities ~width ?t0 ~tf ~vectors colors
  | Prm (Fast, No_intensities, Simple) ->
      Prmfit.Cheat.prior ~width ?t0 ~tf ~vectors colors


let prm_seq
  (type a b) (settings : (a, b, b list) prm_settings)
  colors hypar : (b list, float list) D.t =
  let t0 = P.t0 hypar in
  let tf = P.tf hypar in
  let width = P.width hypar in
  let max_height = P.prm_max_height hypar in
  let min_width = P.prm_min_width hypar in
  let dt = P.dt hypar in
  let vectors = Epi.Point.vectors ?max_height ?min_width ~width colors in
  match settings with
  | Ode _
  | Sde _
  | Prm (_, _, Simple)
  | Prm (_, _, Seq_elt) ->
      assert false
  | Prm (Exact, Intensities, Sequential) ->
      Prmfit.Time.prior_intens_seq ~dt ~width ?t0 ~tf ~vectors colors
  | Prm (Exact, No_intensities, Sequential) ->
      Prmfit.Time.prior_seq ~dt ~width ?t0 ~tf ~vectors colors
  | Prm (Approx, Intensities, Sequential) ->
      Prmfit.Color.prior_intens_seq ~dt ~width ?t0 ~tf ~vectors colors
  | Prm (Approx, No_intensities, Sequential) ->
      Prmfit.Color.prior_seq ~dt ~width ?t0 ~tf ~vectors colors
  | Prm (Fast, Intensities, Sequential) ->
      Prmfit.Cheat.prior_intens_seq ~dt ~width ?t0 ~tf ~vectors colors
  | Prm (Fast, No_intensities, Sequential) ->
      Prmfit.Cheat.prior_seq ~dt ~width ?t0 ~tf ~vectors colors


let prm (type a b c) (settings : (a, b, c) prm_settings) colors hypar :
  (c, float list) D.t =
  match settings with
  | Ode _
  | Sde _ ->
      assert false
  | Prm (_, _, Simple) ->
      prm_simple settings colors hypar
  | Prm (_, _, Sequential) ->
      prm_seq settings colors hypar
  | Prm (_, _, Seq_elt) ->
      (* not very clean *)
      Fit.Dist.Base {
        draw = (fun _ -> failwith "Not_implemented") ;
        log_dens_proba = (fun _ -> []) ;
      }


let dbts_simple dims hy =
  let t0 = P.t0_or_0 hy in
  let width = P.width hy in
  let tf = P.tf hy in
  let draw rng =
    Sim.Dbt.rand_draw ~rng ~t0 ~dt:width ~tf ~n:dims
  in
  let log_dens_proba dbts =
    [F.to_float (Sim.Dbt.log_dens dbts)]
  in
  Fit.Dist.Base { draw ; log_dens_proba }


let dbts_seq dims hy =
  (* cover duration and stop every width *)
  let n_slices ~width ~duration =
    I.Pos.of_anyint (I.of_float (F.div duration width))
  in
  let width = P.width hy in
  let dt = P.dt hy in
  let duration = P.duration hy in
  (* total number of elements : cover duration, and stop every dt *)
  let n_dbts = n_slices ~width:dt ~duration in
  let draw rng =
    I.Pos.fold ~f:(fun dbtss k ->
      let t0 = F.Pos.mul (I.Pos.to_float (I.Pos.pred k)) dt in
      let tf = F.Pos.add t0 dt in
      let dbts = Sim.Dbt.rand_draw ~rng ~t0 ~dt:width ~tf ~n:dims in
      dbts :: dbtss
    ) ~x:[] ~n:n_dbts
  in
  let log_dens_proba dbtss =
    L.map (fun dbts -> F.to_float (Sim.Dbt.log_dens dbts)) dbtss
  in
  Fit.Dist.Base { draw ; log_dens_proba }


let dbts (type a) dims (seq : (Sim.Dbt.t, a) sequential) hy :
  (a, float list) Fit.Dist.t =
  match seq with
  | Simple ->
      dbts_simple dims hy
  | Sequential ->
      dbts_seq dims hy
  | Seq_elt ->
      (* not very clean *)
      Fit.Dist.Base {
        draw = (fun _ -> failwith "Not_implemented") ;
        log_dens_proba = (fun dbts -> [F.to_float (Sim.Dbt.log_dens dbts)]) ;
      }


let ode_simple =
  Fit.Dist.Constant ()


let ode_seq hy =
  let n_slices ~width ~duration =
    I.to_int (I.of_float (F.div duration width))
  in
  let dt = P.dt hy in
  let duration = P.duration hy in
  let n = n_slices ~width:dt ~duration in
  Fit.Dist.Constant (L.init n (fun _ -> ()))


let ode (type a) (settings : (unit, unit, a) cont_settings) :
  (a, float list) Fit.Dist.t =
  match settings with
  | Ode Simple ->
      ode_simple
  | Ode Seq_elt ->
      ode_simple
  | Ode Sequential ->
      (* can't know what is in hypar in epifit/lib/mcmc.ml *)
      failwith "Not_implemented"
  | Sde _
  | Prm _ ->
      assert false

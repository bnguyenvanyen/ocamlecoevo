open Epifit
open Seir

module M = Mcmc.Make (Hyper) (Fit)

let () = Cmdliner.Term.exit @@ Cmdliner.Term.eval (M.Term.run, M.Term.info)

open Epifit.Seirs

module R = Epifit.Resimulate.Make (Epi.Seirs.Model) (Hyper) (Fit)

let () =
  Cmdliner.Term.exit @@ Cmdliner.Term.eval (R.Term.run, R.Term.info)

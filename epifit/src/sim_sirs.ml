open Epifit.Sir

module R = Epifit.Resimulate.Make (Epi.Sir.Model) (Hyper) (Fit)

let () =
  Cmdliner.Term.exit @@ Cmdliner.Term.eval (R.Term.run, R.Term.info)

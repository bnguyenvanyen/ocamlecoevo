open Epifit

let header = [ "beta" ; "nu" ; "rho" ; "s0" ; "i0" ; "r0" ; "seed" ]

let output_line chan th =
  let sof = string_of_float in
  let soi = string_of_int in
  let l = [
    sof th#beta ; sof th#nu ; sof th#rho ;
    sof th#s0 ; sof th#i0 ; sof th#r0 ;
    soi th#seed
  ] in Csv.output_record chan l


let chan_r = ref stdout

let seed_r = ref None

let n_r = ref 100

let hypar_r = ref (new Param.prior_base)


let specl = [
  ("-chan",
   Arg.String (fun s -> chan_r := Lift_arg.handle_chan s),
   "Channel or file to output to.") ;
  ("-seed",
   Arg.Int (fun n -> seed_r := Some n ),
   "Seed of the PRNG.") ;
  ("-n",
   Arg.Set_int n_r,
   "Sample size.") ;
] @ (Lift_arg.specl_of hypar_r Specs.prior_base)
   

let anon_fun s = Printf.printf "Ignored anonymous argument : %s" s

let usage_msg = " Sample from the SIR prior distribution."

let main () =
  begin
    Arg.parse specl anon_fun usage_msg ;
    let rng =
      match !seed_r with
      | None -> Random.State.make_self_init ()
      | Some n -> Random.State.make (Array.init 1 (fun _ -> n))
    in
    let n = !n_r in
    (* draw the sample *)
    let thetas = Fit.Dist.sample rng ~n (F.prior !hypar_r) in
    (* output as CSV *)
    let csv_chan = Csv.to_channel !chan_r in
    Csv.output_record csv_chan header ;
    List.iter (output_line csv_chan) thetas ;
    Csv.close_out csv_chan
  end ;;

main ()

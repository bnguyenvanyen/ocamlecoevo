open Epifit
open Sir

module M = Mcmc.Make (Hyper) (Fit)

let () =
  Printexc.record_backtrace true ;
  Cmdliner.Term.exit @@ Cmdliner.Term.eval (M.Term.run, M.Term.info)

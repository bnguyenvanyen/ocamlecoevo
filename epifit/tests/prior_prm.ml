open Epifit
open Sig


type hyper = Sir.hyper

let hy = new Sir.hyper


let draw () =
  let rng = Util.rng (Some 0) in
  let prior =
   Prior.prm (Prm (Approx, Intensities, Simple)) [`I_infection] hy
  in
  Fit.Dist.draw_from rng prior


let%test _ =
 let nu, _ = draw () in
 Epi.Prm_color.colors nu = [`I_infection]


let%expect_test _ =
  let nu, _ = draw () in
  Printf.printf "%f" (F.to_float (Epi.Prm_color.logp nu)) ;
  [%expect{| -60.609003 |}]

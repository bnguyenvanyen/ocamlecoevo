open Epifit
open Sig


let hy = Prior_prm.hy

let prop = Propose.prm (Prm (Approx, Intensities, Simple)) hy


(* going from itself to itself with the proposal *)
let%test _ =
  let prm = Prior_prm.draw () in
  let logps = Fit.Propose.log_dens_proba_ratio prop prm prm in
  abs_float (L.fold_left (+.) 0. logps) < 1e-6


(* not true anymore as the ratio is empty (so this raises) *)
(*
let%expect_test _ =
  let prm = Prior_prm.draw () in
  let rng = Util.rng (Some 0) in
  let prm' = Fit.Propose.draw_from rng prm prop in
  let logps = Fit.Propose.log_dens_proba_ratio prop prm prm' in
  Printf.printf "%f" (L.hd logps) ;
  [%expect{|
    -0.087011 |}]
*)

let%test _ =
  let prm = Prior_prm.draw () in
  let rng = Util.rng (Some 0) in
  let prm' = Fit.Propose.draw_from rng prm prop in
  let logps = Fit.Propose.log_dens_proba_ratio prop prm prm' in
  L.is_empty logps


(* not true anymore as the proposal log_pd_ratio simply compensates
 * the prior values *)
(*
let%test _ =
  let prm = Prior_prm.draw () in
  let rng = Util.rng (Some 0) in
  let prm' = Fit.Propose.draw_from rng prm prop in
  let prm'' = Fit.Propose.draw_from rng prm' prop in
  let logps = Fit.Propose.log_dens_proba_ratio prop prm prm'' in
  logps = [ neg_infinity ]
*)

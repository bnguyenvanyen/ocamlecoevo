(* test the command line for epifit-sir *)

open Epifit


module M = Mcmc.Make (Sir.Hyper) (Sir.Fit)


(* the term runs with a minimal command line *)
let%test _ =
  let path = "/tmp/ocaml_ecoevo.epifit.test" in
  let step = "iter,false,1" in
  let argv = [| "sh" ; path ; "--step" ; step |] in
  match Cmdliner.Term.eval ~argv (M.Term.run, M.Term.info) with
  | `Ok _ ->
      true
  | _ ->
      false


(* the popsize in hyper gets set from theta 0 *)
let%test _ =
  let path = "/tmp/ocaml_ecoevo.epifit.test" in
  let step = "iter,false,1" in
  let popsize = 10. in
  let argv = [|
    "sh" ;
    path ;
    "--step" ; step ;
    "--popsize" ; string_of_float popsize ;
  |]
  in
  match Cmdliner.Term.eval ~argv (M.Term.run, M.Term.info) with
  | `Ok (hy, _) ->
      (* check hy popsize *)
      Util.nearly_equal popsize hy#popsize
  | _ ->
      assert false


open Epifit

module Lac = Lacaml.D

module M = Mcmc.Make (Sir.Hyper) (Sir.Fit)



let write_adapt_tags out pars =
  pars
  |> Fit.Param.List.adapt_tags
  |> List.map Sir.Fit.tag_to_string
  |> BatList.print ~first:"" ~sep:"\t" ~last:"\n" BatString.print out


(* with only one jump per tag (no sir) *)
let infer_simple =
  (new Sir.infer_pars)#with_sir (`All Fit.Infer.fixed)


(* also with sir, and prm after it *)
let infer_full =
  (new Sir.infer_pars)#with_stoch Fit.Infer.adapt


let simple_vec n =
  let v = Lac.Vec.make0 n in
  v.{1} <- ~-. 0.5 ;
  v.{2} <- 0.4 ;
  v


let full_vec n =
  let v = Lac.Vec.make0 n in
  v.{1} <- ~-. 0.5 ;
  v.{2} <- 0.4 ;
  v.{3} <- ~-. 0.6 ;
  v.{4} <- 0.3 ;
  v
  


(* create *)

let%expect_test _ =
  let pars = Sir.Fit.params infer_simple (new Sir.hyper) in
  let c = Mcmc.create_chol pars in
  write_adapt_tags BatIO.stdout pars ;
  Lacaml.Io.pp_fmat Format.std_formatter c ;
  [%expect{|
    beta	nu	rho
    0.05    0     0
       0 0.05     0
       0    0 0.005 |}]


(* go from pars to pars with an adjust *)

let%expect_test _ =
  let pars = Sir.Fit.params infer_simple (new Sir.hyper) in
  let c = Mcmc.create_chol pars in
  (* Fit.Ram.adjust once *)
  let n = Lac.Mat.dim1 c in
  let v = simple_vec n in
  Fit.Ram.adjust ~gamma:(2./.3.) n 0 c v 0.5 ;
  write_adapt_tags BatIO.stdout pars ;
  Lacaml.Io.pp_fmat Format.std_formatter c ;
  [%expect{|
    beta	nu	rho
     0.0571156         0     0
    -0.0106758 0.0536081     0
             0         0 0.005 |}] ;
  let c' = Mcmc.new_chol pars (Some (El pars, c)) in
  Lacaml.Io.pp_fmat Format.std_formatter (Lac.Mat.sub c' c) ;
  [%expect{|
    0 0 0
    0 0 0
    0 0 0 |}]


(* add a par to infer *)

let%expect_test _ =
  let hy = new Sir.hyper in
  (* first fix beta *)
  let infer = infer_simple#with_beta Fit.Infer.fixed in
  let pars = Sir.Fit.params infer hy in
  let c = Mcmc.create_chol pars in
  (* Fit.Ram.adjust once *)
  let n = Lac.Mat.dim1 c in
  let v = simple_vec n in
  Fit.Ram.adjust ~gamma:(2./.3.) n 0 c v 0.5 ;
  write_adapt_tags BatIO.stdout pars ;
  Lacaml.Io.pp_fmat Format.std_formatter c ;
  [%expect{|
    nu	rho
      0.0571156          0
    -0.00106758 0.00536081 |}] ;
  (* then let beta vary *)
  let infer' = infer#with_beta Fit.Infer.adapt in
  let pars' = Sir.Fit.params infer' hy in
  (* and copy *)
  let c' = Mcmc.new_chol pars' (Some (El pars, c)) in
  write_adapt_tags BatIO.stdout pars' ;
  Lacaml.Io.pp_fmat Format.std_formatter c' ;
  [%expect{|
    beta	nu	rho
    0.05           0          0
       0   0.0571156          0
       0 -0.00106758 0.00536081 |}] ;
  Lacaml.Io.pp_fmat Format.std_formatter (Lac.Mat.sub ~ar:2 ~ac:2 c' c) ;
  [%expect{|
    0 0
    0 0 |}]


(* test new_chol with sir varying *)

let%expect_test _ =
  let hy = new Sir.hyper in
  (* first fix beta *)
  let settings = Epifit.Sig.Prm (Approx, Intensities, Simple) in
  let infer = infer_full#with_sir (`All Fit.Infer.fixed) in
  let El pars = M.all_params settings infer hy in
  let c = Mcmc.create_chol pars in
  (* before anything *)
  write_adapt_tags BatIO.stdout pars ;
  Lacaml.Io.pp_fmat Format.std_formatter c ;
  [%expect{|
    beta	nu	rho	stoch
    0.05    0     0    0    0    0
       0 0.05     0    0    0    0
       0    0 0.005    0    0    0
       0    0     0 0.01    0    0
       0    0     0    0 0.01    0
       0    0     0    0    0 0.01 |}] ;
  (* adjust *)
  let n = Lac.Mat.dim1 c in
  let v = full_vec n in
  Fit.Ram.adjust ~gamma:(2./.3.) n 0 c v 0.5 ;
  write_adapt_tags BatIO.stdout pars ;
  Lacaml.Io.pp_fmat Format.std_formatter c ;
  [%expect{|
    beta	nu	rho	stoch
      0.0535105            0            0         0    0    0
    -0.00543254    0.0519908            0         0    0    0
     0.00081488 -0.000585812   0.00540605         0    0    0
    -0.00081488  0.000585812 -0.000781597 0.0101791    0    0
              0            0            0         0 0.01    0
              0            0            0         0    0 0.01 |}] ;
  (* then let beta vary *)
  let infer' = infer#with_sir (`All Fit.Infer.adapt) in
  let El pars' = M.all_params settings infer' hy in
  (* and copy *)
  let c' = Mcmc.new_chol pars' (Some (El pars, c)) in
  write_adapt_tags BatIO.stdout pars' ;
  Lacaml.Io.pp_fmat Format.std_formatter c' ;
  [%expect{|
    beta	nu	rho	sir	stoch
      0.0535105            0            0    0 0    0         0    0    0
    -0.00543254    0.0519908            0    0 0    0         0    0    0
     0.00081488 -0.000585812   0.00540605    0 0    0         0    0    0
              0            0            0 1000 0    0         0    0    0
              0            0            0    0 1    0         0    0    0
              0            0            0    0 0 1000         0    0    0
    -0.00081488  0.000585812 -0.000781597    0 0    0 0.0101791    0    0
              0            0            0    0 0    0         0 0.01    0
              0            0            0    0 0    0         0    0 0.01 |}]

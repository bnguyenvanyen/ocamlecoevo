open Epifit
open Sig


module M = Mcmc.Make (Sir.Hyper) (Sir.Fit)


(* test reporting loglik on itself *)
let%expect_test _ =
  let hy = (new Sir.hyper)#with_tf 0.1 in
  let th = (new Episim.theta) in
  let par = (th :> Epi.Param.t_unit) in
  let z0 = Sir.Fit.to_state th in
  let traj = Sir.S.simulate ~out:`Cadlag Sir (Ode Simple) hy par z0 () in
  let cases =
    traj
    |> Epi.Data.binned_reportr_data
    |> J.map F.to_float
    |> J.map int_of_float
  in
  let rate = Epi.Traj.reporting_rate traj in
  let loglik =
    Observe.reported_cases ~obs:`Poisson hy cases rate
  in
  (* print what's in cases *)
  J.print BatInt.print BatIO.stdout cases ;
  [%expect{|
    0.02: 1
    0.04: 1
    0.06: 1
    0.08: 2
    0.1: 3 |}] ;
  (* print what's in rate *)
  J.print F.print BatIO.stdout rate ;
  [%expect{|
    0.: 62.9992299923
    0.02: 78.4726795755
    0.04: 97.7405571915
    0.06: 121.731175725
    0.08: 151.598870195
    0.1: 188.778496111 |}] ;
  (* this is reversed compared with the previous one *)
  L.print
    ~first:"" ~sep:"\n" ~last:""
    BatFloat.print BatIO.stdout loglik
  ;
  [%expect{|
    -1.52087898118
    -1.41542796294
    -1.40866407035
    -1.19560771944
    -1.06778810325 |}]
  ;
  Printf.printf "%f" (L.fold_left (+.) 0. loglik) ;
  [%expect{| -6.608367 |}]


(* test M.run doesn't raise *)


(* with no likelihood, and so no simulations *)

let%test _ =
  let seed = 543 in
  let path = "/tmp/ocamlecoevo.epifit.test" in
  (* nothing much will actually be output *)
  let lik = {
    cases = `None ;
    sero = `None ;
    alpha = None ;
  }
  in
  let settings = Ode Simple in
  let recipe = [ M.{
    name = "iter" ;
    adapt = false ;
    infer = new Sir.infer_pars ;
    n_iter = 1 ;
    theta_every = None ;
    traj_every = None ;
    chol_every = None ;
    stoch_every = None ;
  } ]
  in
  let hy = new Sir.hyper in
  let data = {
    cases = None ;
    prevalence = None ;
    sero_agg = None ;
  }
  in
  let th = (new Episim.theta) in
  match M.run ~verbose:false ~seed ~path ~lik ~settings ~recipe hy data th with
  | _ ->
      true
  | exception _ ->
      false


(* with some dummy case data -- so simulation happens *)

(* for different settings *)

let path = "/tmp/ocamlecoevo.epifit.test"


let run (type a b c d) ~out (settings : (a, b, c, d) settings) =
  let seed = 543 in
  (* nothing much will actually be output *)
  let lik = {
    cases = `Poisson ;
    sero = `None ;
    alpha = None ;
  }
  in
  let infer =
    (new Sir.infer_pars)
    |> (fun i -> i#with_sir (`All Fit.Infer.Fixed))
    |> (fun i -> 
        match settings with
        | Ode _ ->
            i
        | Sde Sequential
        | Prm (_, _, Sequential) ->
            i#with_stoch (Fit.Infer.Free `Adapt)
        | _ ->
            i#with_stoch (Fit.Infer.Free `Custom)
        )
  in
  let theta_every = if out then Some 1 else None in
  let recipe = [ M.{
    name = "iter" ;
    adapt = false ;
    infer ;
    n_iter = 1 ;
    theta_every ;
    traj_every = None ;
    chol_every = None ;
    stoch_every = None ;
  } ]
  in
  let hy =
    (new Sir.hyper)
    |> (fun hy -> hy#with_tf 0.1)
    |> (fun hy -> hy#with_dt 0.1)
  in
  let data = {
    cases = Some (J.of_list [(0.1, 0)]) ;
    prevalence = None ;
    sero_agg = None ;
  }
  in
  let th = (new Episim.theta) in
  ignore (
    M.run
      ~verbose:false
      ~seed
      ~path
      ~lik
      ~settings
      ~recipe
      hy
      data
      th
  )


let%expect_test _ =
  run ~out:true (Ode Simple) ;
  [%expect] ;
  let s = BatFile.with_file_in
    ~perm:BatFile.user_read
    (path ^ ".iter.theta.csv")
    BatIO.read_all
  in
  Printf.printf "%s" s ;
  [%expect{|
    k,outcome,logprior,loglik,logu,p'p,qq',l'l,logv,beta,nu,rho,move_beta,move_nu,move_rho
    0,accept,-7.73503093405,-12.5888863052,0.,inf,inf,inf,inf,70.,52.,0.1,70.,52.,0.1
    0,accept,-7.73411002829,-12.7112318491,-0.726905205158,0.000920905757317,0.,-0.122345543918,-0.121424638161,70.0109773569,51.9439309006,0.100457763332,70.0109773569,51.9439309006,0.100457763332 |}]


let%test _ =
  match run ~out:false (Ode Simple) with
  | _ ->
      true
  | exception _ ->
      false


let%test _ =
  match run ~out:false (Sde Simple) with
  | _ ->
      true
  | exception _ ->
      false


let%test _ =
  match run ~out:false (Sde Sequential) with
  | _ ->
      true
  | exception _ ->
      false


let%test _ =
  match run ~out:false (Prm (Approx, Intensities, Simple)) with
  | _ ->
      true
  | exception _ ->
      false


let%test _ =
  match run ~out:false (Prm (Exact, Intensities, Simple)) with
  | _ ->
      true
  | exception _ ->
      false


let%test _ =
  match run ~out:false (Prm (Approx, Intensities, Sequential)) with
  | _ ->
      true
  | exception _ ->
      false


let%test _ =
  match run ~out:false (Prm (Exact, Intensities, Sequential)) with
  | _ ->
      true
  | exception _ ->
      false


(* Not implemented *)

let%test _ =
  match run ~out:false (Ode Sequential) with
  | _ ->
      false
  | exception Invalid_argument _ ->
      true

#!/usr/bin/bash

NAME="vietnam_hcmc_dengue_like.78901.cut"
RUN="mcmc.phylo"
BASEDEST="_data/mcmc/sirs/"$NAME
BASEOUT="_data/plot/mcmc/sirs/"$NAME

# FIXME BURN should be different for cases and sequences
BURN="500000"
MULT="75000 0"
S0_RANGE="s0 0 6000000"
I0_RANGE="i0 0 15000"
ALPHA="0.05"
DPI="200."
WIDTH="300."
WIDTH2="180."
CTXT="paper_cramped"

PARS="phase rho s0 i0"
MU="mu"
PI="pia pit pic"
GI="p-inv gamma-shape"
R="rct rat rgt rac rcg rag"

# Plot the data
## cases
python3 plot/cases.py\
  --focus "_data/"$NAME".data.cases.csv"\
  --range 0. 3500.\
  --dpi $DPI --width $WIDTH --latex --context $CTXT\
  --out "_data/plot/"$NAME".data."$CTXT".png"

## phylogeny
python3 plot/newick.py\
  --times "_data/"$NAME".data.sequences.fasta"\
  --target "_data/"$NAME".beast.tree.newick"\
  --fasta-header "(?P<label>\w+)_(?P<time>[+-]?(\d+(\.\d*)?|\.\d+)([eE][+-]?\d+)?)\w*"\
  --dont-relabel-target\
  --dont-print-labels\
  --margin -1955\
  --size 3 9\
  --out "_data/plot/"$NAME".tree.png"



# for INFER in "fix_nu"; do
#   if [ "$INFER" = "fix_nu_betavar" ]; then
#     PARS_MORE="beta"
#   elif [ "$INFER" = "fix_nu" ]; then
#     PARS_MORE="beta betavar"
#   elif [ "$INFER" = "infer" ]; then
#     PARS_MORE="beta betavar nu"
#   fi

#   # for SEED in "111" "222"; do
#   for SEED in "111"; do
#     echo $BASEDEST"."$RUN"."$INFER".{}."$SEED

#     # # params
#     # ## cases
#     # python3 plot/params.py\
#     #   --read-template $BASEDEST"."$RUN"."$INFER".{}."$SEED"/run.theta.csv"\
#     #   --read-label "Source"\
#     #   --read "Cases" "cases"\
#     #   --parameters $PARS_MORE $PARS\
#     #   --triangular\
#     #   --burn $BURN\
#     #   --range $S0_RANGE\
#     #   --range $I0_RANGE\
#     #   --rename "beta" "$\beta_m$"\
#     #   --rename "betavar" "$\beta_v$"\
#     #   --rename "phase" "$\phi$"\
#     #   --rename "nu" "$\gamma$"\
#     #   --rename "rho" "$\rho$"\
#     #   --rename "s0" "\$S(0)\$"\
#     #   --rename "i0" "\$I(0)\$"\
#     #   --alpha $ALPHA\
#     #   --dpi $DPI\
#     #   --width $WIDTH\
#     #   --latex\
#     #   --context $CTXT\
#     #   --out $BASEOUT"."$RUN"."$INFER".cases."$SEED".theta.{}."$CTXT".png"

#     # ## tree
#     # python3 plot/params.py\
#     #   --read-template $BASEDEST"."$RUN"."$INFER".{}."$SEED"/run.theta.csv"\
#     #   --read-label "Source"\
#     #   --read "Tree" "tree"\
#     #   --parameters $PARS_MORE $PARS\
#     #   --triangular\
#     #   --burn $BURN\
#     #   --range $S0_RANGE\
#     #   --range $I0_RANGE\
#     #   --rename "beta" "$\beta_m$"\
#     #   --rename "betavar" "$\beta_v$"\
#     #   --rename "phase" "$\phi$"\
#     #   --rename "nu" "$\gamma$"\
#     #   --rename "rho" "$\rho$"\
#     #   --rename "s0" "\$S(0)\$"\
#     #   --rename "i0" "\$I(0)\$"\
#     #   --alpha $ALPHA\
#     #   --dpi $DPI\
#     #   --width $WIDTH\
#     #   --latex\
#     #   --context $CTXT\
#     #   --out $BASEOUT"."$RUN"."$INFER".tree."$SEED".theta.{}."$CTXT".png"

#     # ## tree focused on phase
#     # python3 plot/params.py\
#     #   --read-template $BASEDEST"."$RUN"."$INFER".{}."$SEED"/run.theta.csv"\
#     #   --read-label "Source"\
#     #   --read "Tree" "tree"\
#     #   --parameters beta phase i0\
#     #   --triangular\
#     #   --burn $BURN\
#     #   --range $S0_RANGE\
#     #   --range $I0_RANGE\
#     #   --rename "beta" "$\beta_m$"\
#     #   --rename "phase" "$\phi$"\
#     #   --rename "i0" "\$I(0)\$"\
#     #   --alpha $ALPHA\
#     #   --dpi $DPI\
#     #   --width $WIDTH\
#     #   --latex\
#     #   --context $CTXT\
#     #   --out $BASEOUT"."$RUN"."$INFER".tree."$SEED".around_phase.{}."$CTXT".png"

#     # ## sequences
#     # python3 plot/params.py\
#     #   --read-template $BASEDEST"."$RUN"."$INFER".{}."$SEED"/run.theta.csv"\
#     #   --read-label "Source"\
#     #   --read "Sequences" "sequences"\
#     #   --parameters $PARS_MORE $PARS\
#     #   --triangular\
#     #   --burn $BURN\
#     #   --range $S0_RANGE\
#     #   --range $I0_RANGE\
#     #   --rename "beta" "$\beta_m$"\
#     #   --rename "betavar" "$\beta_v$"\
#     #   --rename "phase" "$\phi$"\
#     #   --rename "nu" "$\gamma$"\
#     #   --rename "rho" "$\rho$"\
#     #   --rename "s0" "\$S(0)\$"\
#     #   --rename "i0" "\$I(0)\$"\
#     #   --alpha $ALPHA\
#     #   --dpi $DPI\
#     #   --width $WIDTH\
#     #   --latex\
#     #   --context $CTXT\
#     #   --out $BASEOUT"."$RUN"."$INFER".sequences."$SEED".theta.{}."$CTXT".png"

#     # ## sequences evo
#     # python3 plot/params.py\
#     #   --read-template $BASEDEST"."$RUN"."$INFER".{}."$SEED"/run.theta.csv"\
#     #   --read-label "Source"\
#     #   --read "Sequences" "sequences"\
#     #   --parameters $MU $PI $GI $R\
#     #   --triangular\
#     #   --burn $BURN\
#     #   --rename "mu" "$\mu$"\
#     #   --rename "pia" "\$\pi_A\$"\
#     #   --rename "pit" "\$\pi_T\$"\
#     #   --rename "pic" "\$\pi_C\$"\
#     #   --rename "p-inv" "\$p_{inv}\$"\
#     #   --rename "gamma-shape" "\$\alpha_\Gamma\$"\
#     #   --rename "rct" "\$r_{CT}\$"\
#     #   --rename "rat" "\$r_{AT}\$"\
#     #   --rename "rgt" "\$r_{GT}\$"\
#     #   --rename "rac" "\$r_{AC}\$"\
#     #   --rename "rcg" "\$r_{CG}\$"\
#     #   --rename "rag" "\$r_{AG}\$"\
#     #   --alpha $ALPHA\
#     #   --dpi $DPI\
#     #   --width $WIDTH\
#     #   --latex\
#     #   --context $CTXT\
#     #   --out $BASEOUT"."$RUN"."$INFER".sequences."$SEED".evo.{}."$CTXT".png"

#     # ## sequences eco evo
#     # python3 plot/params.py\
#     #   --read-template $BASEDEST"."$RUN"."$INFER".{}."$SEED"/run.theta.csv"\
#     #   --read-label "Source"\
#     #   --read "Sequences" "sequences"\
#     #   --parameters $PARS_MORE $MU $GI\
#     #   --triangular\
#     #   --burn $BURN\
#     #   --rename "beta" "$\beta_m$"\
#     #   --rename "betavar" "$\beta_v$"\
#     #   --rename "phase" "$\phi$"\
#     #   --rename "nu" "$\gamma$"\
#     #   --rename "mu" "$\mu$"\
#     #   --rename "p-inv" "\$p_{inv}\$"\
#     #   --rename "gamma-shape" "\$\alpha_\Gamma\$"\
#     #   --alpha $ALPHA\
#     #   --dpi $DPI\
#     #   --width $WIDTH\
#     #   --latex\
#     #   --context $CTXT\
#     #   --out $BASEOUT"."$RUN"."$INFER".sequences."$SEED".theta_evo.{}."$CTXT".png"

#     # ## cases + tree
#     # python3 plot/params.py\
#     #   --read-template $BASEDEST"."$RUN"."$INFER".{}."$SEED"/run.theta.csv"\
#     #   --read-label "Source"\
#     #   --read "Tree" "tree"\
#     #   --read "Cases" "cases"\
#     #   --read "Both" "cases_tree"\
#     #   --parameters $PARS_MORE $PARS\
#     #   --triangular\
#     #   --burn $BURN\
#     #   --range $S0_RANGE\
#     #   --range $I0_RANGE\
#     #   --rename "beta" "$\beta_m$"\
#     #   --rename "betavar" "$\beta_v$"\
#     #   --rename "phase" "$\phi$"\
#     #   --rename "nu" "$\gamma$"\
#     #   --rename "rho" "$\rho$"\
#     #   --rename "s0" "\$S(0)\$"\
#     #   --rename "i0" "\$I(0)\$"\
#     #   --alpha $ALPHA\
#     #   --dpi $DPI\
#     #   --width $WIDTH\
#     #   --latex\
#     #   --context $CTXT\
#     #   --out $BASEOUT"."$RUN"."$INFER".cases_tree."$SEED".theta.{}."$CTXT".png"

#     # ## cases + sequences
#     # python3 plot/params.py\
#     #   --read-template $BASEDEST"."$RUN"."$INFER".{}."$SEED"/run.theta.csv"\
#     #   --read-label "Source"\
#     #   --read "Sequences" "sequences"\
#     #   --read "Cases" "cases"\
#     #   --read "Both" "cases_sequences"\
#     #   --parameters $PARS_MORE $PARS\
#     #   --triangular\
#     #   --burn $BURN\
#     #   --range $S0_RANGE\
#     #   --range $I0_RANGE\
#     #   --rename "beta" "$\beta_m$"\
#     #   --rename "betavar" "$\beta_v$"\
#     #   --rename "phase" "$\phi$"\
#     #   --rename "nu" "$\gamma$"\
#     #   --rename "rho" "$\rho$"\
#     #   --rename "s0" "\$S(0)\$"\
#     #   --rename "i0" "\$I(0)\$"\
#     #   --alpha $ALPHA\
#     #   --dpi $DPI\
#     #   --width $WIDTH\
#     #   --latex\
#     #   --context $CTXT\
#     #   --out $BASEOUT"."$RUN"."$INFER".cases_sequences."$SEED".theta.{}."$CTXT".png"

#     # # estimates
#     # python3 plot/estimates.py\
#     #   --read-template $BASEDEST"."$RUN"."$INFER".{}."$SEED"/run.theta.csv"\
#     #   --read "Cases" "cases"\
#     #   --read "Tree" "tree"\
#     #   --read "Sequences" "sequences"\
#     #   --read "Cases+Tree" "cases_tree"\
#     #   --read "Cases+Sequences" "cases_sequences"\
#     #   --parameters beta phase rho s0 i0 r0\
#     #   --burn $BURN

#     # trajs
#     I_RANGE="i 0. 30000."
#     ASP="1.618"

#     # # compartments for cases+tree alone
#     # python3 plot/trajs.py\
#     #   --glob-template $BASEDEST"."$RUN"."$INFER".{}."$SEED"/run.traj.csv"\
#     #   --glob-label "Source"\
#     #   --glob "Cases+Tree" "cases_tree"\
#     #   --compartment s i\
#     #   --range $I_RANGE\
#     #   --concat\
#     #   --burn $BURN\
#     #   --alpha 0.1\
#     #   --latex\
#     #   --context $CTXT\
#     #   --width $WIDTH\
#     #   --aspect $ASP\
#     #   --dpi $DPI\
#     #   --out $BASEOUT"."$RUN"."$INFER".cases_tree."$SEED".trajs."$CTXT".png"

#     ## cases / tree / cases + tree
#     python3 plot/trajs.py\
#       --glob-template $BASEDEST"."$RUN"."$INFER".{}."$SEED"/run.traj.csv"\
#       --glob-label "Source"\
#       --glob "Tree" "tree"\
#       --glob "Cases" "cases"\
#       --glob "Both" "cases_tree"\
#       --compartment i\
#       --range $I_RANGE\
#       --concat\
#       --burn $BURN\
#       --plot-globs rows\
#       --alpha 0.1\
#       --latex\
#       --context $CTXT\
#       --width $WIDTH2\
#       --aspect $ASP\
#       --dpi $DPI\
#       --out $BASEOUT"."$RUN"."$INFER".cases_tree."$SEED".trajs.rows."$CTXT".png"

#     # ## cases + sequences
#     # python3 plot/trajs.py\
#     #   --glob-template $BASEDEST"."$RUN"."$INFER".{}."$SEED"/run.traj.csv"\
#     #   --glob-label "Source"\
#     #   --glob "Sequences" "sequences"\
#     #   --glob "Cases" "cases"\
#     #   --glob "Cases+Sequences" "cases_sequences"\
#     #   --compartment s i\
#     #   --concat\
#     #   --burn $BURN\
#     #   --alpha 0.1\
#     #   --latex\
#     #   --context $CTXT\
#     #   --width $WIDTH\
#     #   --dpi $DPI\
#     #   --out $BASEOUT"."$RUN"."$INFER".cases_sequences."$SEED".trajs."$CTXT".png"

#     # cases
#     for SRC in "cases" "tree" "sequences" "cases_tree" "cases_sequences" ; do
#       dune exec epi-gen-data --\
#         --from reportr\
#         --concat\
#         $BASEDEST"."$RUN"."$INFER"."$SRC"."$SEED"/run.traj.csv"
#     done

#     ## cases + tree
#     python3 plot/cases.py\
#       --glob-template $BASEDEST"."$RUN"."$INFER".{}."$SEED"/run.data.cases.csv"\
#       --glob-label "Source"\
#       --glob "Tree" "tree"\
#       --glob "Cases" "cases"\
#       --glob "Both" "cases_tree"\
#       --focus "_data/"$NAME".data.cases.csv"\
#       --concat\
#       --burn $BURN\
#       --alpha 0.1\
#       --interval false\
#       --rows\
#       --aspect $ASP\
#       --latex\
#       --context $CTXT\
#       --width $WIDTH2\
#       --dpi $DPI\
#       --out $BASEOUT"."$RUN"."$INFER".cases_tree."$SEED".cases.rows."$CTXT".png"
#   done
# done


# Results on sequence data
INFER="fix_nu"
SEED="555"
BURN="500000"
PARS_MORE="beta betavar"
I_RANGE="i 0. 30000."
C_RANGE="0 10000"
ASP="1.618"


## sequences evo
python3 plot/params.py\
  --read-template $BASEDEST"."$RUN"."$INFER".{}."$SEED"/run.theta.csv"\
  --read-label "Source"\
  --read "Cases+Sequences" "cases_sequences"\
  --parameters $MU $PI $GI $R\
  --triangular\
  --burn $BURN\
  --rename "mu" "$\mu$"\
  --rename "pia" "\$\pi_A\$"\
  --rename "pit" "\$\pi_T\$"\
  --rename "pic" "\$\pi_C\$"\
  --rename "p-inv" "\$p_{inv}\$"\
  --rename "gamma-shape" "\$\alpha_\Gamma\$"\
  --rename "rct" "\$r_{CT}\$"\
  --rename "rat" "\$r_{AT}\$"\
  --rename "rgt" "\$r_{GT}\$"\
  --rename "rac" "\$r_{AC}\$"\
  --rename "rcg" "\$r_{CG}\$"\
  --rename "rag" "\$r_{AG}\$"\
  --violin-ncol 4\
  --alpha $ALPHA\
  --dpi $DPI\
  --width $WIDTH\
  --latex\
  --context $CTXT\
  --out $BASEOUT"."$RUN"."$INFER".cases_sequences."$SEED".evo.{}."$CTXT".png"


for SRC in "cases" "tree" "cases_tree"; do
  python3 scripts/thin.py\
    --burn 200000\
    --in $BASEDEST"."$RUN"."$INFER"."$SRC"."$SEED"/run.theta.csv"\
    --out $BASEDEST"."$RUN"."$INFER"."$SRC"."$SEED"/run.burn.theta.csv"
done

for SRC in "sequences" "cases_sequences"; do
  python3 scripts/thin.py\
    --burn 1000000\
    --in $BASEDEST"."$RUN"."$INFER"."$SRC"."$SEED"/run.theta.csv"\
    --out $BASEDEST"."$RUN"."$INFER"."$SRC"."$SEED"/run.burn.theta.csv"
done


python3 plot/params.py\
  --read-template $BASEDEST"."$RUN"."$INFER".{}."$SEED"/run.burn.theta.csv"\
  --read-label "Source"\
  --read "Cases+Sequences" "cases_sequences"\
  --parameters betavar phase rho i0\
  --triangular\
  --range $S0_RANGE\
  --range $I0_RANGE\
  --rename "beta" "$\beta_m$"\
  --rename "betavar" "$\beta_v$"\
  --rename "phase" "$\phi$"\
  --rename "nu" "$\gamma$"\
  --rename "rho" "$\rho$"\
  --rename "s0" "\$S(0)\$"\
  --rename "i0" "\$I(0)\$"\
  --violin-ncol 3\
  --no-pair\
  --alpha $ALPHA\
  --dpi $DPI\
  --width $WIDTH\
  --latex\
  --context $CTXT\
  --out $BASEOUT"."$RUN"."$INFER".cases_sequences."$SEED".theta.{}."$CTXT".png"

python3 plot/params.py\
  --read-template $BASEDEST"."$RUN"."$INFER".{}."$SEED"/run.burn.theta.csv"\
  --read-label "Source"\
  --read "Sequences" "sequences"\
  --read "Cases" "cases"\
  --read "Both" "cases_sequences"\
  --parameters $PARS_MORE $PARS\
  --triangular\
  --range $S0_RANGE\
  --range $I0_RANGE\
  --rename "beta" "$\beta_m$"\
  --rename "betavar" "$\beta_v$"\
  --rename "phase" "$\phi$"\
  --rename "nu" "$\gamma$"\
  --rename "rho" "$\rho$"\
  --rename "s0" "\$S(0)\$"\
  --rename "i0" "\$I(0)\$"\
  --no-share-k\
  --alpha $ALPHA\
  --dpi $DPI\
  --width $WIDTH\
  --latex\
  --context $CTXT\
  --out $BASEOUT"."$RUN"."$INFER".compare_cases_sequences."$SEED".theta.{}."$CTXT".png"

python3 plot/params.py\
  --read-template $BASEDEST"."$RUN"."$INFER".{}."$SEED"/run.burn.theta.csv"\
  --read-label "Source"\
  --read "Cases+Sequences" "cases_sequences"\
  --read "Cases+Tree" "cases_tree"\
  --parameters $PARS_MORE $PARS\
  --triangular\
  --range $S0_RANGE\
  --range $I0_RANGE\
  --rename "beta" "$\beta_m$"\
  --rename "betavar" "$\beta_v$"\
  --rename "phase" "$\phi$"\
  --rename "nu" "$\gamma$"\
  --rename "rho" "$\rho$"\
  --rename "s0" "\$S(0)\$"\
  --rename "i0" "\$I(0)\$"\
  --no-share-k\
  --alpha $ALPHA\
  --dpi $DPI\
  --width $WIDTH\
  --latex\
  --context $CTXT\
  --out $BASEOUT"."$RUN"."$INFER".cases_compare_tree_sequences."$SEED".theta.{}."$CTXT".png"

python3 plot/params.py\
  --read-template $BASEDEST"."$RUN"."$INFER".{}."$SEED"/run.burn.theta.csv"\
  --read-label "Source"\
  --read "Cases+Sequences" "cases_sequences"\
  --read "Cases+Tree" "cases_tree"\
  --parameters "loglik-eco" "loglik-tree" "loglik-evo" "beta" "i0"\
  --triangular\
  --range $S0_RANGE\
  --range $I0_RANGE\
  --rename "beta" "$\beta_m$"\
  --rename "betavar" "$\beta_v$"\
  --rename "phase" "$\phi$"\
  --rename "nu" "$\gamma$"\
  --rename "rho" "$\rho$"\
  --rename "s0" "\$S(0)\$"\
  --rename "i0" "\$I(0)\$"\
  --no-share-k\
  --alpha $ALPHA\
  --dpi $DPI\
  --width $WIDTH\
  --latex\
  --context $CTXT\
  --out $BASEOUT"."$RUN"."$INFER".cases_compare_tree_sequences."$SEED".loglik.{}."$CTXT".png"


for SRC in "cases" "tree" "sequences" "cases_tree" "cases_sequences"; do
  python3 scripts/rnaught_sir.py\
    --in $BASEDEST"."$RUN"."$INFER"."$SRC"."$SEED"/run.burn.theta.csv"\
    --recovery-rate 8.\
    --out $BASEDEST"."$RUN"."$INFER"."$SRC"."$SEED"/run.burn.complete.theta.csv"
done

python3 plot/estimates.py\
  --read-template $BASEDEST"."$RUN"."$INFER".{}."$SEED"/run.burn.complete.theta.csv"\
  --read "Cases" "cases"\
  --read "Tree" "tree"\
  --read "Sequences" "sequences"\
  --read "Cases+Tree" "cases_tree"\
  --read "Cases+Sequences" "cases_sequences"\
  --parameters beta betavar phase rho s0 i0 r0 rnaught

for SRC in "cases" "tree" "cases_tree"; do
  python3 scripts/thin.py\
    --burn 200000\
    --in $BASEDEST"."$RUN"."$INFER"."$SRC"."$SEED"/run.traj.csv"\
    --out $BASEDEST"."$RUN"."$INFER"."$SRC"."$SEED"/run.burn.traj.csv"
done

for SRC in "sequences" "cases_sequences"; do
  python3 scripts/thin.py\
    --burn 1000000\
    --in $BASEDEST"."$RUN"."$INFER"."$SRC"."$SEED"/run.traj.csv"\
    --out $BASEDEST"."$RUN"."$INFER"."$SRC"."$SEED"/run.burn.traj.csv"
done

python3 plot/trajs.py\
  --glob-template $BASEDEST"."$RUN"."$INFER".{}."$SEED"/run.burn.traj.csv"\
  --glob-label "Source"\
  --glob "Sequences" "sequences"\
  --glob "Cases+Sequences" "cases_sequences"\
  --compartments i coal\
  --concat\
  --alpha 0.1\
  --latex\
  --context $CTXT\
  --width $WIDTH\
  --dpi $DPI\
  --aspect 1.618\
  --out $BASEOUT"."$RUN"."$INFER".compare_cases_sequences."$SEED".trajs."$CTXT".png"

python3 plot/trajs.py\
  --glob-template $BASEDEST"."$RUN"."$INFER".{}."$SEED"/run.burn.traj.csv"\
  --glob-label "Source"\
  --glob "Sequences" "sequences"\
  --glob "Cases" "cases"\
  --glob "Both" "cases_sequences"\
  --compartments i\
  --range $I_RANGE\
  --concat\
  --plot-globs rows\
  --alpha 0.1\
  --latex\
  --context $CTXT\
  --width $WIDTH2\
  --dpi $DPI\
  --aspect 1.618\
  --out $BASEOUT"."$RUN"."$INFER".compare_cases_sequences."$SEED".trajs.rows."$CTXT".png"

python3 plot/trajs.py\
  --glob-template $BASEDEST"."$RUN"."$INFER".{}."$SEED"/run.burn.traj.csv"\
  --glob-label "Source"\
  --glob "Sequences" "sequences"\
  --glob "Cases" "cases"\
  --glob "Both" "cases_sequences"\
  --compartments i\
  --range $I_RANGE\
  --concat\
  --plot-globs columns\
  --alpha 0.1\
  --latex\
  --context $CTXT\
  --width $WIDTH\
  --dpi $DPI\
  --aspect 1.618\
  --decoration-width 0.1\
  --decoration-height 0.3\
  --out $BASEOUT"."$RUN"."$INFER".compare_cases_sequences."$SEED".trajs.columns."$CTXT".png"

python3 plot/trajs.py\
  --glob-template $BASEDEST"."$RUN"."$INFER".{}."$SEED"/run.burn.traj.csv"\
  --glob-label "Source"\
  --glob "Sequences" "sequences"\
  --glob "Tree" "tree"\
  --compartments i coal\
  --concat\
  --alpha 0.1\
  --latex\
  --context $CTXT\
  --width $WIDTH\
  --dpi $DPI\
  --aspect 1.618\
  --out $BASEOUT"."$RUN"."$INFER".compare_tree_sequences."$SEED".trajs."$CTXT".png"

python3 plot/trajs.py\
  --glob-template $BASEDEST"."$RUN"."$INFER".{}."$SEED"/run.burn.traj.csv"\
  --glob-label "Source"\
  --glob "Cases+Sequences" "cases_sequences"\
  --glob "Cases+Tree" "cases_tree"\
  --compartments i coal\
  --concat\
  --alpha 0.1\
  --latex\
  --context $CTXT\
  --width $WIDTH2\
  --dpi $DPI\
  --aspect 1.618\
  --out $BASEOUT"."$RUN"."$INFER".cases_compare_tree_sequences."$SEED".trajs."$CTXT".png"


for SRC in "cases" "tree" "cases_tree" "sequences" "cases_sequences"; do
  echo $SRC
  dune exec epi-gen-data --\
    --from reportr\
    --concat\
    $BASEDEST"."$RUN"."$INFER"."$SRC"."$SEED"/run.traj.csv"
done

for SRC in "cases" "tree" "cases_tree"; do
  python3 scripts/thin.py\
    --burn 200000\
    --in $BASEDEST"."$RUN"."$INFER"."$SRC"."$SEED"/run.data.cases.csv"\
    --out $BASEDEST"."$RUN"."$INFER"."$SRC"."$SEED"/run.burn.data.cases.csv"
done

for SRC in "sequences" "cases_sequences"; do
  python3 scripts/thin.py\
    --burn 1000000\
    --in $BASEDEST"."$RUN"."$INFER"."$SRC"."$SEED"/run.data.cases.csv"\
    --out $BASEDEST"."$RUN"."$INFER"."$SRC"."$SEED"/run.burn.data.cases.csv"
done

python3 plot/cases.py\
  --glob-template $BASEDEST"."$RUN"."$INFER".{}."$SEED"/run.burn.data.cases.csv"\
  --glob-label "Source"\
  --glob "Sequences" "sequences"\
  --glob "Cases" "cases"\
  --glob "Both" "cases_sequences"\
  --focus "_data/"$NAME".data.cases.csv"\
  --concat\
  --interval false\
  --rows\
  --alpha 0.1\
  --latex\
  --context $CTXT\
  --width $WIDTH2\
  --dpi $DPI\
  --aspect 1.618\
  --out $BASEOUT"."$RUN"."$INFER".compare_cases_sequences."$SEED".cases.rows."$CTXT".png"

python3 plot/cases.py\
  --glob-template $BASEDEST"."$RUN"."$INFER".{}."$SEED"/run.burn.data.cases.csv"\
  --glob-label "Source"\
  --glob "Sequences" "sequences"\
  --glob "Cases" "cases"\
  --glob "Both" "cases_sequences"\
  --focus "_data/"$NAME".data.cases.csv"\
  --concat\
  --interval false\
  --range $C_RANGE\
  --columns\
  --alpha 0.1\
  --latex\
  --context $CTXT\
  --width $WIDTH\
  --dpi $DPI\
  --aspect 1.618\
  --decoration-width 0.1\
  --decoration-height 0.4\
  --out $BASEOUT"."$RUN"."$INFER".compare_cases_sequences."$SEED".cases.columns."$CTXT".png"

python3 plot/cases.py\
  --glob-template $BASEDEST"."$RUN"."$INFER".{}."$SEED"/run.burn.data.cases.csv"\
  --glob-label "Source"\
  --glob "Cases+Sequences" "cases_sequences"\
  --glob "Cases+Tree" "cases_tree"\
  --focus "_data/"$NAME".data.cases.csv"\
  --concat\
  --interval false\
  --rows\
  --alpha 0.1\
  --latex\
  --context $CTXT\
  --width $WIDTH2\
  --dpi $DPI\
  --aspect 1.618\
  --out $BASEOUT"."$RUN"."$INFER".cases_compare_tree_sequences."$SEED".cases.rows."$CTXT".png"

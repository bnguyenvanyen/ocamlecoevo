(* Logistic growth (linear birth and competitive death) with mutations *)

open Sig


type param = {
  b : float ;  (* individual base birth rate *)
  c : float ; (* competitive death rate *)
  d : float ;  (* natural death rate *)
  mu : float ;  (* mutation probability on birth *)
  v : float ;  (* variance of mutations *)
}


let output_par chan par =
  Printf.fprintf chan
  "Logistic birth-death process with a real-valued selective trait :
   b=%f ; d=%f ; mu=%f ; v=%f\n"
   par.b par.d par.mu par.v

let specl = [
  ("-b", Lift_arg.Float (fun par x -> { par with b = x }),
   "Base individual birth rate.") ;
  ("-d", Lift_arg.Float (fun par x -> { par with d = x }),
   "Competitive death rate.") ;
  ("-mu", Lift_arg.Float (fun par x -> { par with mu = x }),
   "Mutation probability on birth.") ;
  ("-v", Lift_arg.Float (fun par x -> { par with v = x }),
   "Variance of mutations.") ;
]

let default = {
  b = 1. ;
  c = 1e-3 ;
  d = 0. ;
  mu = 1e-2 ;
  v = 1. ;
}

module T : 
  sig
    open Pop.Sig
    include TRAIT
    val gid : id group
    val scores : (id t -> float) list
    val id : float -> id t
    val to_float : id t -> float
  end =
  struct
    open Pop.Sig

    type _ t =
      | F : float -> id t

    type _ group =
      | Id : id group

    let copy x = x

    let group_of : type a. a t -> a group =
      function
      | F _ -> Id
      | _ -> .

    let of_group _ = failwith "never nonid"

    let isid : type a. a group -> a isid =
      function
      | Id -> Isid Eq
      | _ -> .

    let gid = Id

    let scores = [
      (fun (F x) -> ~-. x) ;
      (fun (F x) -> x) ;
    ]

    let id x = F x

    let to_float : type a. a t -> float =
      function
      | F x -> x
      | _ -> .
  end


module P = Pop.With_events.Augment_max (Pop.With_max.Make (T))


let birth par = F.Pos.of_float par.b

let comp par = F.Pos.of_float par.c

let death par = F.Pos.of_float par.d

let variance par = F.Pos.of_float par.v

let mutation par = F.Proba.of_float par.mu


let upper_birth_rate par _ z =
  F.Pos.mul (birth par) (I.Pos.to_float (P.count ~group:T.gid z))


let no_birth_proba _ ind _ _ : 'a F.proba F.t =
  let x = T.to_float (P.I.trait_of ind) in
  (* we don't easily have a proof that this is a proba (but it is) *)
  F.of_float_unsafe (1. /. 2. -. atan x /. U.pi)


let comp_death_rate par _ z =
  let c = comp par in
  let n = P.count ~group:T.gid z in
  let nm1 = I.Pos.pred n in
  let fn = I.Pos.to_float n in
  let fnm1 = I.Pos.to_float nm1 in
  F.Pos.Op.(c * fn * fnm1)


let nat_death_rate par _ z =
  let n = P.count ~group:T.gid z in
  F.Pos.mul (death par) (I.Pos.to_float n)


let mutate par rng _ x =
  T.id (U.rand_normal ~rng (T.to_float x) (variance par))

let no_mutate_proba par _ _ =
  F.Proba.compl (mutation par)

let maybe_mutate par =
  Sim.Ctmjp.Util.erase (no_mutate_proba par) (mutate par)

let birth_modif par ind =
  Sim.Ctmjp.Util.erase
    (no_birth_proba par ind)
    (P.birth_modif ~mut:(maybe_mutate par) ind)
  

let of_indiv_modif m rng t z =
  let ind = P.choose ~rng ~group:T.gid z in
  m ind rng t z


let event par = Sim.Ctmjp.Util.(add_many [
  combine (upper_birth_rate par) (of_indiv_modif (birth_modif par)) ;
  combine (comp_death_rate par) (of_indiv_modif P.death_modif) ;
  combine (nat_death_rate par) (of_indiv_modif P.death_modif) ;
])


let create ~nindivs =
  P.create ~mem:3 ~ngroups:1 ~scores:T.scores ~nindivs

module Csv =
  struct
    module Floatmap = Map.Make (struct
      type t = float
      let compare = compare
    end)

    let header =
      [ ["Selective LBD simulation"] ;
        ["t" ; "rest:list-of-{trait-value}:{count}"] ]

    let line (_ : param) t z =
      let sof = string_of_float in
      let soi = string_of_int in
      (* traits are not counted during the simulation so we count them here *)
      let count ind m' =
        let x = T.to_float (P.I.trait_of ind) in
        let n =
          try
            Floatmap.find x m'
          with Not_found ->
            0
        in Floatmap.add x (n + 1) m'
      in
      let m = P.fold_indivs count z T.gid Floatmap.empty in
      let add_to_list x n l =
        ((sof x) ^ ":" ^ (soi n)) :: l
      in
      (sof (F.to_float t)) :: (Floatmap.fold add_to_list m [])


    let simulate_until ~chan ~dt ?seed par t z0 =
      let next = Sim.Ctmjp.Gill.integrate (event par) (U.rng seed) in
      let line = line par in
      output_par stdout par ;
      let output = Sim.Csv.convert ~dt ~header ~line ~chan in
      let sim = Sim.Loop.simulate_until ~output ~next in
      let tf, zf = sim z0 t in
      (tf, P.count ~group:T.gid zf, P.max ~i:0 zf, P.max ~i:1 zf, zf)

  end

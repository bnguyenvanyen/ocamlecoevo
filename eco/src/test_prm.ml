open Eco

module U = Util
module F = U.Float
module I = U.Int


let main () =
  begin
    let z0 = Prm.create ~nindivs:100 in
    let seed = 1234 in
    let dt = F.Pos.of_float 0.2 in
    let b = 5. in
    let c = 10. in
    let tf = F.Pos.of_float 1. in
    let nrep = 5 in
    Printf.printf "Before Prm : %f\n%!" (Sys.time ()) ;
    Prm.simulate_until ~k:nrep ~seed ~dt { b ; c } tf z0 ;
    Printf.printf "After Prm : %f\n%!" (Sys.time ())
  end;;

main ()

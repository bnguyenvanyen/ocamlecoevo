module Lns = Eco.Logistic_neutral_seq
module T = Lns.Term

let term =
  Cmdliner.Term.(
      const (fun seed dt out par tf z0 ->
        Lns.simulate_until ?seed ?dt out par tf z0
      )
    $ T.seed
    $ T.dt
    $ T.out
    $ T.par
    $ T.tf
    $ T.z0
  )


let info =
  let doc =
    "Simulate a logistic birth death process
     with neutral sequence evolution"
  in
  Cmdliner.Term.info
    "eco-logistic-seq"
    ~version:"%%VERSION%%"
    ~doc
    ~exits:Cmdliner.Term.default_exits
    ~man:[]


let () = Cmdliner.Term.exit @@ Cmdliner.Term.eval (term, info)

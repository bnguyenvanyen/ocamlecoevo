open Cmdliner


let () =
  Term.exit @@ Term.eval (
    Eco.Intra.Term.simulate_until,
    Eco.Intra.Term.info
  )

open Cmdliner
module CC = Coal.Constant_lazy


let () =
  Printexc.record_backtrace true ;
  Term.exit @@ Term.eval (
    CC.Term.many_rand_until_out_mrca,
    CC.Term.info
  )

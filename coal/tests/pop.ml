module C = Coal.Constant


let merge = C.Pop.merge ~source:() ~source':() ~dest:()


(* test sample *)
let%expect_test _ =
  C.Pop.empty ()
  |> C.Pop.sample ~dest:() 1 0.99
  |> C.Pop.output ~chan:stdout
  ;
  [%expect{|
    #tree {1}
    {1}|_:0.990; |}]


(* test merge *)
let%expect_test _ =
  C.Pop.empty ()
  |> C.Pop.sample ~dest:() 1 0.99
  |> C.Pop.sample ~dest:() 2 0.93
  |> merge
      1
      2
      0.918
  |> C.Pop.output ~chan:stdout
  ;
  [%expect{|
    #tree {1;2}
    ({1}|_:0.990,{2}|_:0.930){1;2}|_:0.918; |}]


let%test _ =
  match C.Pop.find ~k:1 (C.Pop.empty ()) with
  | Some _ ->
      false
  | None ->
      true


(* test find after sample *)
let%test _ =
  let z = C.Pop.sample ~dest:() 1 0.99 (C.Pop.empty ()) in
  match C.Pop.find ~k:1 z with
  | Some _ ->
      true
  | None ->
      false


(* test find after sample + merge *)
let%test _ =
  let z =
    C.Pop.empty ()
    |> C.Pop.sample ~dest:() 1 0.99
    |> C.Pop.sample ~dest:() 2 0.93
    |> merge
        1
        2
        0.918
  in
  match C.Pop.find ~k:1 z with
  | Some _ ->
      true
  | None ->
      false


(* test find after more complex history *)
let%test _ =
  let z =
    C.Pop.empty ()
    |> C.Pop.sample ~dest:() 1 0.9
    |> C.Pop.sample ~dest:() 2 0.8
    |> C.Pop.sample ~dest:() 3 0.7
    |> C.Pop.sample ~dest:() 4 0.6
    |> merge
        1
        2
        0.5
    |> merge
        1
        3
        0.4
  in
  match C.Pop.find ~k:2 z with
  | Some _ ->
      true
  | None ->
      false

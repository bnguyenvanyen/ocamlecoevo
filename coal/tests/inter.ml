module I = Coal.Inter


(* {1} <> {2} *)
let%test _ =
  let ks = I.singleton 1 in
  let ks' = I.singleton 2 in
  not (I.equal ks ks')


(* {1;2} = {2;3} *)
let%test _ =
  let ks = I.of_list [1 ; 2] in
  let ks' = I.of_list [2 ; 3] in
  (I.equal ks ks')


(* {1} < {2} *)
let%test _ =
  let ks = I.singleton 1 in
  let ks' = I.singleton 2 in
  (I.compare ks ks' < 0)


(* {1,3} < {2,4} *)
let%test _ =
  let ks = I.of_list [1 ; 3] in
  let ks' = I.of_list [2 ; 4] in
  (I.compare ks ks' < 0)


let%expect_test _ =
  let ks = I.of_list [1 ; 3 ; 5] in
  Printf.printf "%s" (I.to_string ks) ;
  [%expect{| {1;3;5} |}]


let%test _ =
  let ks = I.of_list [1 ; 3 ; 5] in
  let s = I.to_string ks in
  match I.of_string s with
  | None ->
      false
  | Some ks' ->
      let s' = I.to_string ks' in
      (s = s')

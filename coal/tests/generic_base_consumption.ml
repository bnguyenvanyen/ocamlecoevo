module C = Coal.Constant.Gen

module F = Util.Float

let tf = F.two

(* test constant *)
let%expect_test _ =
  let rate = `Constant 3. in
  let prim, inv_prim, _ = C.Backward.base_consumption ~tf rate in
  (* in backward time, we start at -2 *)
  let s0 = prim (F.of_float ~-.2.) in
  let t0 = inv_prim s0 in
  let s1 = prim (F.of_float ~-.1.) in
  let t1 = inv_prim s1 in
  let sf = prim F.zero in
  let tf = inv_prim tf in
  Printf.printf "%f" (F.to_float s0) ;
  [%expect{| 0.000000 |}] ;
  Printf.printf "%f" (F.to_float t0) ;
  [%expect{| -2.000000 |}] ;
  Printf.printf "%f" (F.to_float s1) ;
  [%expect{| 3.000000 |}] ;
  Printf.printf "%f" (F.to_float t1) ;
  [%expect{| -1.000000 |}] ;
  Printf.printf "%f" (F.to_float sf) ;
  [%expect{| 6.000000 |}] ;
  Printf.printf "%f" (F.to_float tf) ;
  [%expect{| -1.333333 |}]



(* test traj *)
let%expect_test _ =
  let rate = `Traj (
    Jump.Regular.of_cadlag (
      Jump.of_list [
        (0., 0.5) ;
        (0.5, 0.75) ;
        (1., 0.35) ;
        (1.5, 0.65) ;
        (2., 0.8) ;
      ]
    )
  )
  in
  let prim, inv_prim, _ = C.Backward.base_consumption ~tf rate in
  (* in backward time, we go from -2. to 0. *)
  let s0 = prim (F.of_float ~-.2.) in
  let t0 = inv_prim s0 in
  let s1 = prim (F.of_float ~-.1.) in
  let t1 = inv_prim s1 in
  let sf = prim F.zero in
  let tf = inv_prim sf in
  Printf.printf "%f" (F.to_float s0) ;
  [%expect{| 0.000000 |}] ;
  Printf.printf "%f" (F.to_float t0) ;
  [%expect{| -2.000000 |}] ;
  Printf.printf "%f" (F.to_float s1) ;
  [%expect{| 0.612500 |}] ;
  Printf.printf "%f" (F.to_float t1) ;
  [%expect{| -1.000000 |}] ;
  Printf.printf "%f" (F.to_float sf) ;
  [%expect{| 1.200000 |}] ;
  Printf.printf "%f" (F.to_float tf) ;
  [%expect{| 0.000000 |}]

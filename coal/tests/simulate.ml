open Coal
open Sig

module C = Constant

let fpof = F.Pos.of_float

let samples : (_ * U.closed_pos * _) list = [
  (1, fpof 0.99, ()) ;
  (2, fpof 0.93, ()) ;
  (3, fpof 0.62, ()) ;
  (4, fpof 0.47, ()) ;
]


let lambda = 10.

let tf = F.one


let c_prm () =
  let rng = U.rng (Some 0) in
  C.Simulate.rand_prm ~rng ~samples ~lambda ~tf


(* test simulation *)
let%expect_test _ =
  let prm = c_prm () in
  let t0 = F.zero in
  let forest, _ =
    C.Simulate.until
      ~lambda
      ~n:(L.length samples)
      ~t0
      ~tf
      prm
  in
  C.Pop.output ~chan:stdout forest ;
  [%expect{|
    #tree {1;2;3;4}
    (((({1}|_:0.990,{2}|_:0.930){1;2}|_:0.918,{3}|_:0.620){1;2;3}|_:0.528,{4}|_:0.470){1;2;3;4}|_:0.460){1;2;3;4}|_:0.000; |}]


(* test with t0 > 0 *)
let%expect_test _ =
  let prm = c_prm () in
  let t0 = fpof 0.3 in
  let forest, _ =
    C.Simulate.until
      ~lambda
      ~n:(L.length samples)
      ~t0
      ~tf
      prm
  in
  C.Pop.output ~chan:stdout forest ;
  [%expect{|
    #tree {1;2;3;4}
    (((({1}|_:0.990,{2}|_:0.930){1;2}|_:0.918,{3}|_:0.620){1;2;3}|_:0.528,{4}|_:0.470){1;2;3;4}|_:0.460){1;2;3;4}|_:0.300; |}]


module Cl = Constant_lazy


(* this needs to give the same points as c_prm *)
let cl_prm () =
  let rng = U.rng (Some 0) in
  Cl.Simulate.Backward.rand_prm ~rng ~samples ~tf


(* test simulation *)
let%expect_test _ =
  let prm = cl_prm () in
  let lambda = fpof lambda in
  let t0 = F.zero in
  let forest, _, _ =
    Cl.Simulate.Backward.until
      ~t0
      ~tf
      ~lambda
      ~samples
      prm
  in
  Cl.Pop.output ~chan:stdout forest ;
  [%expect{|
    #tree {1;2;3;4}
    (((({1}|_:0.990,{2}|_:0.930){1;2}|_:0.918,{3}|_:0.620){1;2;3}|_:0.528,{4}|_:0.470){1;2;3;4}|_:0.460){1;2;3;4}|_:0.000; |}]


(* test t0 > 0 *)
let%expect_test _ =
  let prm = cl_prm () in
  let lambda = fpof lambda in
  let t0 = fpof 0.3 in
  let forest, _, _ =
    Cl.Simulate.Backward.until
      ~t0
      ~tf
      ~lambda
      ~samples
      prm
  in
  Cl.Pop.output ~chan:stdout forest ;
  [%expect{|
    #tree {1;2;3;4}
    (((({1}|_:0.990,{2}|_:0.930){1;2}|_:0.918,{3}|_:0.620){1;2;3}|_:0.528,{4}|_:0.470){1;2;3;4}|_:0.460){1;2;3;4}|_:0.300; |}]

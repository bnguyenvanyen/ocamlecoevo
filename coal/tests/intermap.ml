module I = Coal.Inter
module Im = Coal.Intermap


(* simplest find *)
let%test _ =
  let ks = I.singleton 1 in
  let m = Im.singleton ks 0 in
  match Im.find ks m with
  | 0 ->
      true
  | _ ->
      false
  | exception Not_found ->
      false


let%test _ =
  let ks = I.of_list [1 ; 3 ; 5] in
  let m = Im.singleton ks 0 in
  match Im.find ks m with
  | 0 ->
      true
  | _ ->
      false
  | exception Not_found ->
      false


let%test _ =
  let ks = I.of_list [1 ; 3 ; 5] in
  let m = Im.singleton ks 0 in
  match Im.find (I.singleton 1) m with
  | 0 ->
      true
  | _ ->
      false
  | exception Not_found ->
      false


(* show that possible uses of Intermap are pretty limited *)
let%test _ =
  let k1 = I.of_list [1 ; 9] in
  let k2 = I.of_list [2 ; 5 ; 11] in
  let k3 = I.of_list [3 ; 7 ; 14] in
  let k4 = I.of_list [4 ; 8 ; 15] in
  let k5 = I.of_list [5 ; 10] in
  let m =
    Im.empty
    |> Im.add k1 1
    |> Im.add k2 2
    |> Im.add k3 3
    |> Im.add k4 4
    |> Im.add k5 5
  in
  match Im.find (I.singleton 9) m with
  | 1 ->
      false
  | _ ->
      true
  | exception Not_found ->
      true

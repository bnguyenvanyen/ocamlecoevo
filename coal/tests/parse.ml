

module T = Coal.Constant.Pop.T
module E = Coal.Constant.Pop


let%test _ =
  E.of_string "" = Some (E.empty ())


let%test _ =
  E.of_string "#tree {1}\n{1}|bad_trait:0.;\n\n" = None


let%expect_test _ =
  let ks = Coal.Inter.of_list [1 ; 2] in
  let tree = T.(
    node
      (0., ks, ())
      (binode
        (1.001e-1, ks, ())
        (leaf (1.5, Coal.Inter.singleton 1, ()))
        (leaf (2., Coal.Inter.singleton 2, ()))
      )
  )
  in
  let forest = E.of_forest [(ks, (), tree)] in
  let s = E.to_string forest in
  Printf.printf "%s" s ;
  [%expect{|
    #tree {1;2}
    (({1}|_:1.500,{2}|_:2.000){1;2}|_:0.100){1;2}|_:0.000; |}] ;
  let s' =
    match E.of_string s with
    | None ->
        ""
    | Some forest' ->
        E.to_string forest'
  in
  Printf.printf "%s" s' ;
  [%expect{|
    #tree {1;2}
    (({1}|_:1.500,{2}|_:2.000){1;2}|_:0.100){1;2}|_:0.000; |}]


(* same but on the strings *)
let%expect_test _ =
  let s =
      "#tree {1;2}\n"
    ^ "(({1}|_:1.5,{2}|_:2.){1;2}|_:1.001e-1){1;2}|_:0.;\n\n"
  in
  let s' =
    match E.of_string s with
    | None ->
        ""
    | Some forest ->
        E.to_string forest
  in
  Printf.printf "%s" s' ;
  [%expect{|
    #tree {1;2}
    (({1}|_:1.500,{2}|_:2.000){1;2}|_:0.100){1;2}|_:0.000; |}]

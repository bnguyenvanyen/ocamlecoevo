open Sig

module Make (Trait : TRAIT) =
  Inter_tree.Make (State.Make (Trait))

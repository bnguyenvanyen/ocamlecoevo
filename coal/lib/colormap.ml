(** A map but with the different types of events separated 
 *
 *  See Generic
 *)


open Sig


module Make
  (Trait : TRAIT)
  (C : COLOR_ANY with type trait = Trait.t) :
  (BatMap.S with type key = C.t) =
  struct
    module Cm = BatMap.Make (C)

    type key = C.t

    type 'a t = {
      samples : 'a Cm.t ;
      mutations : 'a Cm.t ;
      merges : 'a Cm.t ;
    }

    let empty = {
      samples = Cm.empty ;
      mutations = Cm.empty ;
      merges = Cm.empty ;
    }

    let apply f m c =
      match C.kind c with
      | `Sample ->
          f c m.samples
      | `Mutation ->
          f c m.mutations
      | `Merge ->
          f c m.merges

    let cmap f m c =
      match C.kind c with
      | `Sample ->
          { m with samples = f c m.samples }
      | `Mutation ->
          { m with mutations = f c m.mutations }
      | `Merge ->
          { m with merges = f c m.merges }

    let is_empty m =
         (Cm.is_empty m.samples)
      && (Cm.is_empty m.mutations)
      && (Cm.is_empty m.merges)

    let cardinal m =
        Cm.cardinal m.samples
      + Cm.cardinal m.mutations
      + Cm.cardinal m.merges

    let add c z m =
      cmap (fun c m -> Cm.add c z m) m c

    let update c c' z m =
      match C.kind c, C.kind c' with
      | `Sample,
        `Sample ->
          { m with samples = Cm.update c c' z m.samples }
      | `Mutation,
        `Mutation ->
          { m with mutations = Cm.update c c' z m.mutations }
      | `Merge,
        `Merge ->
          { m with merges = Cm.update c c' z m.merges }
      | `Sample, `Mutation
      | `Sample, `Merge
      | `Mutation, `Sample
      | `Mutation, `Merge
      | `Merge, `Sample
      | `Merge, `Mutation ->
          invalid_arg "different events"

    let find c m =
      apply Cm.find m c

    let find_default d c m =
      apply (Cm.find_default d) m c
    
    let find_opt c m =
      apply Cm.find_opt m c

    let remove c m =
      cmap Cm.remove m c

    let modify c f m =
      cmap (fun c m -> Cm.modify c f m) m c

    let modify_def d c f m =
      cmap (fun c m -> Cm.modify_def d c f m) m c

    let modify_opt c f m =
      cmap (fun c m -> Cm.modify_opt c f m) m c

    let extract c m =
      match C.kind c with
      | `Sample ->
          let z, samples = Cm.extract c m.samples in
          z, { m with samples }
      | `Mutation ->
          let z, mutations = Cm.extract c m.mutations in
          z, { m with mutations }
      | `Merge ->
          let z, merges = Cm.extract c m.merges in
          z, { m with merges }

    let pop m =
      let cv, samples = Cm.pop m.samples in
      cv, { m with samples }

    let mem c m =
      apply Cm.mem m c

    let iter f m =
      Cm.iter f m.samples ;
      Cm.iter f m.mutations ;
      Cm.iter f m.merges

    let map f m =
      let samples = Cm.map f m.samples in
      let mutations = Cm.map f m.mutations in
      let merges = Cm.map f m.merges in
      { samples ; mutations ; merges }

    let mapi f m =
      let samples = Cm.mapi f m.samples in
      let mutations = Cm.mapi f m.mutations in
      let merges = Cm.mapi f m.merges in
      { samples ; mutations ; merges }

    let fold f m a =
      a
      |> Cm.fold f m.samples
      |> Cm.fold f m.mutations
      |> Cm.fold f m.merges

    let filterv f m =
      let samples = Cm.filterv f m.samples in
      let mutations = Cm.filterv f m.mutations in
      let merges = Cm.filterv f m.merges in
      { samples ; mutations ; merges }

    let filter f m =
      let samples = Cm.filter f m.samples in
      let mutations = Cm.filter f m.mutations in
      let merges = Cm.filter f m.merges in
      { samples ; mutations ; merges }

    let filter_map f m =
      let samples = Cm.filter_map f m.samples in
      let mutations = Cm.filter_map f m.mutations in
      let merges = Cm.filter_map f m.merges in
      { samples ; mutations ; merges }

    let compare cmp m m' =
      let c = Cm.compare cmp m.samples m'.samples in
      if c = 0 then
        let c' = Cm.compare cmp m.mutations m'.mutations in
        if c' = 0 then
          Cm.compare cmp m.merges m'.merges
        else
          c'
      else
        c

    let equal eq m m' =
         (Cm.equal eq m.samples m'.samples)
      && (Cm.equal eq m.mutations m'.mutations)
      && (Cm.equal eq m.merges m'.merges)

    let keys m =
      BatEnum.append
        (Cm.keys m.samples)
        (BatEnum.append (Cm.keys m.mutations) (Cm.keys m.merges))

    let values m =
      BatEnum.append
        (Cm.values m.samples)
        (BatEnum.append (Cm.values m.mutations) (Cm.values m.merges))

    let min_binding m =
      (* follows compare order ? *)
      match Cm.min_binding m.samples with
      | kv ->
          kv
      | exception Not_found ->
          begin match Cm.min_binding m.mutations with
          | kv ->
              kv
          | exception Not_found ->
              Cm.min_binding m.merges
          end

    let pop_min_binding m =
      match Cm.pop_min_binding m.samples with
      | kv, samples ->
          kv, { m with samples }
      | exception Not_found ->
          begin match Cm.pop_min_binding m.mutations with
          | kv, mutations ->
              kv, { m with mutations }
          | exception Not_found ->
              let kv, merges = Cm.pop_min_binding m.merges in
              kv, { m with merges }
          end

    let max_binding m =
      (* follows compare order ? *)
      match Cm.max_binding m.merges with
      | kv ->
          kv
      | exception Not_found ->
          begin match Cm.max_binding m.mutations with
          | kv ->
              kv
          | exception Not_found ->
              Cm.max_binding m.samples
          end

    let pop_max_binding m =
      match Cm.pop_max_binding m.merges with
      | kv, merges ->
          kv, { m with merges }
      | exception Not_found ->
          begin match Cm.pop_max_binding m.mutations with
          | kv, mutations ->
              kv, { m with mutations }
          | exception Not_found ->
              let kv, samples = Cm.pop_max_binding m.samples in
              kv, { m with samples }
          end

    let choose m =
      Cm.choose m.samples

    let any m =
      Cm.any m.samples

    let split c m =
      match C.kind c with
      | `Sample ->
          let l, data, r = Cm.split c m.samples in
          { m with samples = l }, data, { m with samples = r }
      | `Mutation ->
          let l, data, r = Cm.split c m.mutations in
          { m with mutations = l }, data, { m with mutations = r }
      | `Merge ->
          let l, data, r = Cm.split c m.merges in
          { m with merges = l }, data, { m with merges = r }

    let partition f m =
      let samples_pass, samples_dont = Cm.partition f m.samples in
      let mutations_pass, mutations_dont = Cm.partition f m.mutations in
      let merges_pass, merges_dont = Cm.partition f m.merges in
      let m_pass = {
          samples = samples_pass ;
          mutations = mutations_pass ;
          merges = merges_pass ;
        }
      in
      let m_dont = {
          samples = samples_dont ;
          mutations = mutations_dont ;
          merges = merges_dont ;
        }
      in (m_pass, m_dont)

    let singleton c v =
      let m = Cm.singleton c v in
      match C.kind c with
      | `Sample ->
          { samples = m ; mutations = Cm.empty ; merges = Cm.empty }
      | `Mutation ->
          { samples = Cm.empty ; mutations = m ; merges = Cm.empty }
      | `Merge ->
          { samples = Cm.empty ; mutations = Cm.empty ; merges = m }

    let bindings m =
        (Cm.bindings m.samples)
      @ (Cm.bindings m.mutations)
      @ (Cm.bindings m.merges)

    let enum m =
      BatEnum.append
        (Cm.enum m.samples)
        (BatEnum.append
          (Cm.enum m.mutations)
          (Cm.enum m.merges)
        )

    let backwards m =
      BatEnum.append
        (Cm.backwards m.merges)
        (BatEnum.append
          (Cm.backwards m.mutations)
          (Cm.backwards m.samples)
        )

    let of_enum e =
      let samples = Cm.of_enum (BatEnum.filter (fun (c, _) ->
          match C.kind c with
          | `Sample ->
              true
          | `Mutation | `Merge ->
              false
        ) e)
      in
      let mutations = Cm.of_enum (BatEnum.filter (fun (c, _) ->
          match C.kind c with
          | `Mutation ->
              true
          | `Sample | `Merge ->
              false
        ) e)
      in
      let merges = Cm.of_enum (BatEnum.filter (fun (c, _) ->
          match C.kind c with
          | `Merge ->
              true
          | `Sample | `Mutation ->
              false
        ) e)
      in
      { samples ; mutations ; merges }

    let for_all f m =
         (Cm.for_all f m.samples)
      && (Cm.for_all f m.mutations)
      && (Cm.for_all f m.merges)

    let exists f m =
         (Cm.exists f m.samples)
      || (Cm.exists f m.mutations)
      || (Cm.exists f m.merges)

    let merge f m m' =
      let samples = Cm.merge f m.samples m'.samples in
      let mutations = Cm.merge f m.mutations m'.mutations in
      let merges = Cm.merge f m.merges m'.merges in
      { samples ; mutations ; merges }

    let print ?first ?last ?sep ?kvsep print_key print_val out m =
      Cm.print ?first ?last ?sep ?kvsep print_key print_val out m.samples ;
      Cm.print ?first ?last ?sep ?kvsep print_key print_val out m.mutations ;
      Cm.print ?first ?last ?sep ?kvsep print_key print_val out m.merges ;

    module Exceptionless =
      struct
        let find c m =
          match find c m with
          | x ->
              Some x
          | exception Not_found ->
              None

        let choose m =
          match choose m with
          | x ->
              Some x
          | exception Not_found ->
              None

        let any m =
          match any m with
          | x ->
              Some x
          | exception Not_found ->
              None
      end

    module Infix =
      struct
        let (-->) m c =
          find c m

        let (<--) m (c, v) =
          add c v m
      end

    module Labels =
      struct
        let add ~key ~data m =
          add key data m

        let iter ~f m =
          iter (fun key data -> f ~key ~data) m

        let map ~f m =
          map f m

        let mapi ~f m =
          mapi (fun key data -> f ~key ~data) m

        let filterv ~f m =
          filterv f m

        let filter ~f m =
          filter f m

        let fold ~f m ~init =
          fold (fun key data x -> f ~key ~data x) m init

        let compare ~cmp m m' =
          compare cmp m m'

        let equal ~cmp m m' =
          equal cmp m m'
      end
  end

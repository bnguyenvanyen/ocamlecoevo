open Sig

module C = Cmdliner


let positive_opt ~doc ~docv ~arg =
  C.Arg.(value
     & opt (some (U.Arg.positive ())) None
     & info [arg] ~docv ~doc
  )


let positive_req ~doc ~docv ~arg =
  C.Arg.(required
     & opt (some (U.Arg.positive ())) None
     & info [arg] ~docv ~doc
  )


let out =
  let doc = "Output the results to $(docv)." in
  C.Arg.(value
     & pos 0 (some string) None
     & info [] ~docv:"OUT" ~doc
  )


let dt () =
  let doc = "Print the state at least every $(docv)." in
  positive_opt ~doc ~docv:"DT" ~arg:"dt"


(* I make it a required 'optional' argument (so a named argument),
 * because I'm bad. *)
let width () =
  let doc = "Use a prm time slice width of $(docv)." in
  positive_req ~doc ~docv:"WIDTH" ~arg:"width"


let t0 () =
  let doc =
    "Simulate the system starting at time $(docv)."
  in
  positive_req ~doc ~docv:"T0" ~arg:"t0"


let tf () =
  let doc =
    "Simulate the system until time $(docv)."
  in
  positive_req ~doc ~docv:"TF" ~arg:"tf"


let niter =
  let doc = "Number of iterations to run. Default 1" in
  C.Arg.(value
     & opt int 1
     & info ["niter"] ~docv:"NITER" ~doc
  )


let seed =
  let doc = "Initialize the PRNG with the seed $(docv)." in
  C.Arg.(
    value
    & opt (some int) None
    & info ["seed"] ~docv:"SEED" ~doc
  )

module La = Lift_arg

module F = Util.Float

let fpof = F.Pos.of_float


let out_string = (
  "-out",
  La.String (fun _ s -> Some s),
  "Path prefix to output to."
)

let dt = (
  "-dt",
  La.Float (fun _ x -> Some (fpof x)),
  "Minimum interval between prints."
)

let tf = (
  "-tf",
  La.Float (fun _ x -> Some (fpof x)),
  "Simulate until."
)

let seed = (
  "-seed",
  La.Int (fun _ n -> Some n),
  "Seed for the PRNG."
)

let lambda = (
  "-lambda",
  La.Float (fun _ x -> Some (fpof x)),
  "Pair coalescence rate."
)

let samples = (
  "-read-samples",
  La.String (fun _ s -> Some s),
  "Path to file to read samples from."
)

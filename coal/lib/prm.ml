(** An internal Prm for coalescent processes
 *
 *  See Generic
 *)


open Sig


module Make
  (Color : COLOR_ANY)
  (Colormap : BatMap.S with type key = Color.t) =
  struct
    module Prm = Sim.Prm_internal.Make (Color) (Colormap)

    (* only correct on separated merges (init nu) *)
    let logd_prm nu =
      let point_log_dens pt (t, logd) =
        let t' = F.to_float (Prm.Point.time pt) in
        let dt = t' -. t in
        let logd' =
          logd +. F.to_float (U.logd_exp F.one dt)
        in
        (t', logd')
      in
      Prm.fold_points (fun c pt x ->
        match Color.kind c with
        | `Merge
        | `Mutation ->
            let _, x' = point_log_dens pt (0., x) in
            x'
        | `Sample ->
            x
      ) nu 0.

    let update_merge_point color dx nu =
      let f (point : Prm.Point.t) =
        (* change its internal time *)
        let t = Prm.Point.time point in
        let t' =
          (* if t' below 0, we can't just take the positive part,
           * because if we do that for several points,
           * then they all happen right at the start of the simulation.
           * Instead just keep t...
           * FIXME that breaks the symmetry !
           * This introduces a bias (up or down ?) *)
          match F.identify (F.add t (F.of_float dx)) with
          | Zero _ ->
              t
          | Neg _ ->
              t
          | Proba t' ->
              F.Pos.narrow t'
          | Pos t' ->
              F.Pos.narrow t'
        in t'
      in
      assert (Color.kind color = `Merge) ;
      Prm.modify_map color f nu

    (* [f] is a positive function that we use to transform time *)
    let update_mutation_points color (f : (_ U.anypos -> _ U.pos)) nu =
      assert (Color.kind color = `Mutation) ;
      let update (pt : Prm.Point.t) : Prm.Point.t =
        f (Prm.Point.time pt)
      in
      Prm.modify_map color update nu

    include Prm
  end

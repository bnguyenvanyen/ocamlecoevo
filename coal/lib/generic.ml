(** Generic coalescent process simulation
  
    The class of processes we model are the following :
    
    The lineages have trait values described by the module type TRAIT.
  
    There are 3 types of colors of events :
    - Sample, when a new lineage with the given index and trait value
      is introduced
    - Mutation, when a bundle of lineages changes from a source
      trait value to a destination trait value
    - Merge, when two bundles of lineages merge and go
      from sources trait values to a destination trait value.
      All lineages ks and ks' are then merged to form
      a new group of lineages is = union ks ks'
  
    Event rates do not depend on lineage indices.
    Merge events are symmetric : what is left or right,
    or the association between trait value and group of lineages
    doesn't matter for the coalescent population events.
 *)

open Sig


(** Maps of positive values *)
module Posmap = BatMap.Make (struct
  type t = U.closed_pos
  let compare = F.compare
end)


(** Simulate the coalescent process from internal PRMs
  
    We simulate the system using Sim.Ctmjp.Prm_internal.
    To do that, we need to have events to make a
    Sim.Evm_internal.Make(...).t.
  
    For each event, we must be able to determine the amount of fuel
    consumed in a given amount of time,
    to project the next occurrence time,
    to update the state,
    and to determine the affected events on focal event occurrence.
    (see {!Sim.Evm_internal})
  
    To determine all of this, we ask the user to pass for each event :
    - The starting and ending traits
    - The amount of fuel consumed by the event up to [t]
      for a single solitary lineage, as a {!Jump.Regular.t}.
      This corresponds to the primitive of the rate of the event
      for a single solitary lineage.
    - The duration after which [fuel] is consumed for a single
      solitary lineage.
      This corresponds to the inverse of the previous function.
  
    Then for each of these, the actual fuel consumption,
    and projected occurrence times need to be adjusted for redundant
    events due to already coalesced lineages.
 *)
module Make (Trait : TRAIT) =
  struct
    module Color = Color.Make (Trait)
    module Colormap = Colormap.Make (Trait) (Color)
    module Prm = Prm.Make (Color) (Colormap)
    module Evm = Sim.Evm_internal.Make (Color) (Colormap) (Prm)
    module Pop = Pop.Make (Trait)

    module Ctmjp = Sim.Ctmjp.Prm_internal.Make (Evm)

    (* conversion forward/backward time *)
    let reverse t =
      F.neg t

    (* starting fuel is 0 at tf (the event should happen at once) *)
    let forward_to_fuel ~(tf : _ U.anyfloat) t =
      F.Pos.of_anyfloat F.Op.(tf - t)

    (* For each event we need to know how to do two things :
     * - given a real duration, compute how much internal time (fuel)
     *   has been consumed
     * - given a remaining internal time, project the real time
     *   when it will have been consumed.
     *
     * [prim] is the primitive of the mass one rate,
     * which maps real time to internal time,
     * and [inv] is the inverse, mapping internal time to real time.
     *)

    module Config =
      struct
        module Mut =
          struct
            (** user specification for a mutation event *)
            (* the type variables are useless but cannot be made existential *)
            type ('a, 'b, 'c, 'd, 'e) t = {
              x : Trait.t ;
              (** source *)
              y : Trait.t ;
              (** destination *)
              prim_base : ('a U.anyfloat -> 'b U.pos) ;
              (** primitive of the base rate *)
              inv_prim_base : ('c U.anypos -> U._float) ;
              (** inverse of the primitive of the base rate *)
              rescale : (Pop.t -> 'd U.pos) option ;
              (** factor to additionally divide the base rate by *)
              alloc_fuel : 'e U.pos ;
              (** allocated fuel *)
            }

            let color { x ; y ; _ } k =
              Color.mutation x y (Intset.singleton k)
          end

        module Merge =
          struct
            (** user specification for a merge event *)
            type ('a, 'b, 'c, 'd) t = {
              x : Trait.t ;
              (** first source *)
              x' : Trait.t ;
              (** second source *)
              y : Trait.t ;
              (** destination *)
              prim_base : ('a U.anyfloat -> 'b U.pos) ;
              (** primitive of the base rate *)
              inv_prim_base : ('c U.anypos -> U._float) ;
              (** inverse of the primitive of the base rate *)
              rescale : (Pop.t -> 'd U.pos) option ;
              (** factor to additionally divide the base rate by *)
            }

            let color { x ; x' ; y ; _ } k k' =
              Color.merge x x' y (Intset.singleton k) (Intset.singleton k')
          end
      end

    module Mutation =
      struct
        open Config.Mut

        (* Amount of internal time (fuel) consumed
         * from time [from_t] to time [to_t],
         * for the mutation of [ks] with rate primitive [prim],
         * given that no event affecting [ks] happened between [t] and [t'] *)
        let fuel_consumed { prim_base ; rescale ; _ } k ~from_t ~to_t z =
          match Pop.card ~k z with
          | Some n ->
              let pn = F.Pos.of_float (float n) in
              (* [ds] is the internal time that would be consumed
               * between [t] and [t'] if [ks] contained one lineage *)
              let ds = F.Pos.of_anyfloat F.Op.(
                prim_base to_t - prim_base from_t
              )
              in
              let alpha =
                match rescale with
                | None ->
                    pn
                | Some resc ->
                    F.Pos.mul pn (resc z)
              in
              F.Pos.Op.(ds / alpha)
          | None ->
              F.zero

        let update_fuel spec k ~from_t ~to_t z s =
          let ds = fuel_consumed spec k ~from_t ~to_t z in
          F.Op.(s - ds)

        (* The time [t'] when [fuel] units of internal time
         * will have been consumed, starting from time [t].
         * To compute it, [prim] of [t] gives us a starting point
         * in terms of "internal time that would have passed if
         * all lineages were of mass 1 since time 0".
         * Add to that the remaining mass 1 internal time,
         * and then [inv] gives us the projected occurrence time.
         * (it's quite clear on a drawing)
         *)
        let project_time { prim_base ; inv_prim_base ; rescale ; _ } k ~fuel z t =
          match Pop.card ~k z with
          | Some n ->
              let pn = F.Pos.of_float (float n) in
              let start = prim_base t in
              let alpha =
                match rescale with
                | None ->
                    pn
                | Some resc ->
                    F.Pos.mul pn (resc z)
              in
              let target = F.Pos.Op.(start + alpha * fuel) in
              inv_prim_base target
          | None ->
              F.infinity

        let affected muts merges k (z : Pop.t) =
          (* the events affected when the bundle of k mutates are :
           * - every other mutation acting on the bundle,
           * - every merge event acting on the bundle *)
          let _, ks, _ = Pop.bundle k z in
          (* mutations that touch any of ks *)
          Intset.fold (fun k l ->
            let mut_cs = L.map (fun spec ->
                color spec k
              ) muts
            in
            (* merge that touch ks *)
            let mrg_cs = Intmap.fold (fun _ (ks', _, _) l ->
              Intset.fold (fun k' l ->
                  let l' = L.map (fun spec ->
                      Config.Merge.color spec k k'
                    ) merges
                  in l' @ l
                ) ks' l
              ) z.forest []
            in
            mut_cs @ mrg_cs @ l
          ) ks []

        let modif_state { x ; y ; _ } k t z =
          (* we need to convert to forward time,
           * so that time increases when going down branches *)
          let fwd_t = reverse t in
          Pop.mutate
            ~source:x
            ~dest:y
            k
            (F.to_float fwd_t)
            z

        let add_event muts merges spec k t z evsnu =
          Evm.add
            (update_fuel spec k)
            (project_time spec k)
            {
              state = modif_state spec k ;
              affected = affected muts merges k ;
            }
            false
            (color spec k)
            t
            z
            evsnu
      end

    module Merge =
      struct
        open Config.Merge

        let fuel_consumed { prim_base ; rescale ; _ } k k' ~from_t ~to_t z =
          match Pop.card ~k z, Pop.card ~k:k' z with
          | Some n, Some n' ->
              (* if k and k' are in the same bundle,
               * then no fuel is consumed *)
              if Pop.coalesced k k' z then
                F.zero
              else
                let pn = F.Pos.of_float (float n) in
                let pn' = F.Pos.of_float (float n') in
                let alpha =
                  match rescale with
                  | None ->
                      F.Pos.Op.(pn * pn')
                  | Some resc ->
                      F.Pos.Op.(pn * pn' * (resc z))
                in
                let ds = F.Pos.of_anyfloat F.Op.(
                  prim_base to_t - prim_base from_t
                )
                in
                F.Pos.Op.(ds / alpha)
          | Some _, None
          | None, Some _
          | None, None ->
              F.zero

        let update_fuel spec k k' ~from_t ~to_t z s =
          let ds = fuel_consumed spec k k' ~from_t ~to_t z in
          F.Op.(s - ds)

        let project_time
          { prim_base ; inv_prim_base ; rescale ; _ } k k' ~fuel z t =
          let proj =
            match Pop.card ~k z, Pop.card ~k:k' z with
            | Some n, Some n' ->
                (* if k and k' are in the same bundle,
                 * then the rate is 0 *)
                if Pop.coalesced k k' z then
                  F.infinity
                else
                  let pn = F.Pos.of_float (float n) in
                  let pn' = F.Pos.of_float (float n') in
                  let alpha =
                    match rescale with
                    | None ->
                        F.Pos.Op.(pn * pn')
                    | Some resc ->
                        F.Pos.Op.(pn * pn' * (resc z))
                  in
                  let start = prim_base t in
                  let target = F.Pos.Op.(start + alpha * fuel) in
                  inv_prim_base target
            | Some _, None
            | None, Some _
            | None, None ->
                F.infinity
          in
          proj

        let affected muts merges color k k' (z : Pop.t) =
          (* the events affected when the bundle of k
           * and the bundle of k' merge are :
           * - every mutation event in the new bundle
           * - every other merge event between the two bundles
           * - every merge event between the new bundle / other bundles
           *)
          let _, ks, _ = Pop.bundle k z in
          let _, ks', _ = Pop.bundle k' z in
          (* the new bundle *)
          let union = Intset.union ks ks' in
          []
          (* other merge events between the same bundles *)
          |> Intset.fold (fun j ->
              Intset.fold (fun j' aff ->
                let l = L.filter_map (fun spec ->
                  let c = Config.Merge.color spec j j' in
                  if Color.equal color c then
                    None
                  else
                    Some c
                ) merges
                in
                l @ aff
              ) ks'
          ) ks
          (* every mutation in the new bundle *)
          |> Intset.fold (fun j aff ->
              let l = L.map (fun spec ->
                Config.Mut.color spec j
              ) muts
              in
              l @ aff
          ) union
          (* every merge event between new bundle / other bundles *)
          |> Intmap.fold (fun _ (js', _, _) aff ->
              if Inter.equal union js' then
                aff
              else
                Intset.fold (fun j ->
                  Intset.fold (fun j' aff ->
                    let l = L.map (fun spec ->
                      Config.Merge.color spec j j'
                    ) merges
                    in
                    l @ aff
                  ) js'
                ) union aff
          ) z.forest

        let modif_state { x ; x' ; y ; _ } k k' t z =
          assert (k <> k') ;
          let fwd_t = reverse t in
          Pop.merge
            ~source:x
            ~source':x'
            ~dest:y
            k
            k'
            (F.to_float fwd_t)
            z

        let add_event muts merges spec k k' t z evsnu =
          let c = color spec k k' in
          Evm.add
            (update_fuel spec k k')
            (project_time spec k k')
            {
              state = modif_state spec k k' ;
              affected = affected muts merges c k k' ;
            }
            true
            c
            t
            z
            evsnu
      end

    module Sample =
      struct
        let update_fuel ~from_t ~to_t _ fuel =
          F.Pos.of_anyfloat F.Op.(fuel + to_t - from_t)

        let project_time ~(fuel : _ U.anypos) _ t =
          F.Op.(t + fuel)

        let affected muts merges k (z : Pop.t) =
          (* mutation that thouch k *)
          let mut_cs = L.map (fun spec ->
              Config.Mut.color spec k
            ) muts
          in
          let mrg_cs = Intmap.fold (fun _ (js, _, _) l ->
              (* merges that touch k *)
              Intset.fold (fun j l ->
                let l' = L.map (fun spec ->
                    Config.Merge.color spec k j
                  ) merges
                in
                l' @ l
              ) js l
          ) z.forest []
          in
          mut_cs @ mrg_cs

        let modif_state x k t z =
          let fwd_t = reverse t in
          Pop.sample ~dest:x k (F.to_float fwd_t) z

        let add_event muts merges x k t z evsnu =
          Evm.add
            update_fuel
            project_time
            {
              state = modif_state x k ;
              affected = affected muts merges k ;
            }
            true
            (Color.sample x k)
            t
            z
            evsnu
      end

    let events ~mutations ~merges ~samples prm t z =
      let n = L.length samples in
      (Evm.empty, prm)
      (* add sample events *)
      |> L.fold_right (fun (k, _, x) ->
          (* t is time of creation, not sampling time *)
          Sample.add_event mutations merges x k t z
      ) samples
      (* add mutation events *)
      |> L.fold_right (fun spec evsnu ->
          U.int_fold_right ~f:(fun k evsnu ->
            Mutation.add_event
              mutations
              merges
              spec
              k
              t
              z
              evsnu
          ) ~n evsnu
      ) mutations
      (* add merge events *)
      |> L.fold_right (fun spec evsnu ->
          U.int_fold_right ~f:(fun k' evsnu ->
            U.int_fold_right ~f:(fun k evsnu ->
              assert (k < k') ;
              Merge.add_event
                mutations
                merges
                spec
                k
                k'
                t
                z
                evsnu
            ) ~n:(k' - 1) evsnu
          ) ~n evsnu
      ) merges

    (* mutations : mutation event specifications
     * n : number of sequences
     * tf : final time
     * (TODO something easier to specify than uf ? should vary by mutation)
     * nu : discrete measure to add to *)
    let add_rand_mutations ~rng mutations n nu =
      U.int_fold_right ~f:(fun k ->
        L.fold_right (fun spec ->
          let c = Config.Mut.color spec k in
          let times = U.rand_poisson_process ~rng F.one spec.alloc_fuel in
          let points = L.fold_left (fun pts t ->
            Prm.Points.add t pts
          ) Prm.Points.empty times
          in
          Prm.add_graftable ~rng c points
        ) mutations
      ) ~n nu

    (* merges : merge event specifications
     * n : number of sequences
     * nu : discrete measure to add to *)
    let add_rand_merges ~rng merges n nu =
      let f k k' =
        L.fold_right (fun spec ->
          let c = Config.Merge.color spec k k' in
          let t = U.rand_exp ~rng F.one in
          let point = t in
          Prm.add_single c point
        ) merges
      in
      U.int_fold_right ~f:(fun k ->
        U.int_fold_right ~f:(fun k' ->
          f k k'
        ) ~n:(k - 1)
      ) ~n nu


    let reg_base_consumption reg =
      let prim = Jump.Regular.primitive reg in
      let inv = Jump.Regular.inverse prim in
      (fun t ->
        t
        |> F.to_float
        |> Jump.Regular.eval prim
        |> F.Pos.of_float
      ),
      (fun s ->
        s
        |> F.to_float
        |> Jump.Piecelin_map.eval inv
        |> F.of_float
      )


    (* coalescence happens backward with respect to time,
     * as they are passed through samples, or rates etc *)
    module Backward =
      struct
        let base_consumption ~tf =
          function
          | `Constant a ->
              (* same as Forward *)
              let pos_a = F.Pos.of_float a in
              (* t goes from -tf to -t0 *)
              (fun t -> F.Pos.of_anyfloat F.Op.(pos_a * (t + tf))),
              (fun s -> F.Op.(s / pos_a - tf)),
              None
          | `Traj reg ->
              (* reverse time *)
              let back_reg = Jump.Regular.neg reg in
              (* then same as Forward *)
              let prim, inv = reg_base_consumption back_reg in
              prim, inv, None
          | `Traj_rescale (reg, rescale) ->
              (* reverse time *)
              let back_reg = Jump.Regular.neg reg in
              (* then same as Forward *)
              let prim, inv = reg_base_consumption back_reg in
              prim, inv, Some rescale

        let mutation ~tf x y rate =
          let prim_base, inv_prim_base, rescale =
            base_consumption ~tf (rate x)
          in
          (* FIXME use something better *)
          let alloc_fuel = F.one in
          Config.Mut.{
            x ;
            y ;
            prim_base ;
            inv_prim_base ;
            rescale ;
            alloc_fuel ;
          }
          
        let merge ~tf x x' y rate =
          let prim_base, inv_prim_base, rescale =
            base_consumption ~tf (rate x x')
          in
          Config.Merge.{
            x ;
            x' ;
            y ;
            prim_base ;
            inv_prim_base ;
            rescale ;
          }

        let add_samples ~tf samples nu =
          L.fold_right (fun (k, fwd_t, x) ->
            let c = Color.sample x k in
            let back_t = forward_to_fuel ~tf fwd_t in
            let point = back_t in
            Prm.add_single c point
          ) samples nu

        (* FIXME t0 *)
        let until ~t0 ~tf ~mutations ~merges ~samples nu =
          let output = `Pair Util.Out.convert_null in
          let mutations = L.map (fun (x, y, rate) ->
              mutation ~tf x y rate
            ) mutations
          in
          let merges = L.map (fun (x, x', y, rate) ->
              merge ~tf x x' y rate
            ) merges
          in
          let back_t0 = reverse tf in
          let back_tf = reverse t0 in
          (* here z0 actually corresponds to time tf *)
          let z0 = Pop.empty () in
          (* everything is done on a copy of nu *)
          (* FIXME t0 *)
          let evm, nu' = events
            ~mutations
            ~merges
            ~samples
            (Prm.copy nu)
            back_t0
            z0
          in
          let _, (zf, nuf) = Ctmjp.simulate_until
            ~output
            ~t0:back_t0
            evm
            z0
            nu'
            back_tf
          in
          let rooted_zf = Pop.root_at (F.to_float t0) zf in
          (rooted_zf, nuf)

        let until_merged ~tf ~mutations ~merges ~samples nu =
          let output = `Pair Util.Out.convert_null in
          let mutations = L.map (fun (x, y, rate) ->
              mutation ~tf x y rate
            ) mutations
          in
          let merges = L.map (fun (x, x', y, rate) ->
              merge ~tf x x' y rate
            ) merges
          in
          let back_t0 = reverse tf in
          let z0 = Pop.empty () in
          (* everything is done on a copy of nu *)
          (* FIXME t0 *)
          let evm, nu' = events
            ~mutations
            ~merges
            ~samples
            (Prm.copy nu)
            back_t0
            z0
          in
          let nlin = L.length samples in
          let stop t z =
            match Pop.card ~k:1 z with
            | None ->
                None
            | Some n ->
                if n = nlin then
                  Some t
                else
                  None
          in
          let back_tf, (zf, nuf) = Ctmjp.simulate
            ~output
            ~t0:back_t0
            stop
            evm
            z0
            nu'
          in
          let t0 = reverse back_tf in
          let rooted_zf = Pop.root_at (F.to_float t0) zf in
          (rooted_zf, nuf)

        let rand_prm ~rng ~mutations ~merges ~samples ~tf =
          let mutations = L.map (fun (x, y, rate) ->
              mutation ~tf x y rate
            ) mutations
          in
          let merges = L.map (fun (x, x', y, rate) ->
              merge ~tf x x' y rate
            ) merges
          in
          let n = L.length samples in
          Prm.empty ()
          |> add_rand_mutations ~rng mutations n
          |> add_rand_merges ~rng merges n
          |> add_samples ~tf samples

        let rand_until ~rng ~mutations ~merges ~samples ~t0 ~tf =
          let nu =
            rand_prm ~rng ~mutations ~merges ~samples ~tf
          in
          let pop, _ = until ~t0 ~tf ~mutations ~merges ~samples nu in
          pop

        let many_rand_until_out_mrca ?seed ?path ~mutations ~merges ~samples ~n ~t0 ~tf =
          let rng = U.rng seed in
          let chan_mrca = Option.map (fun p -> 
            U.Csv.open_out (p ^ ".mrca.csv")
          ) path
          in
          Option.iter (fun c ->
            Csv.output_record c (Pop.header_all_mrca (L.length samples))
          ) chan_mrca
          ;
          for k = 1 to n do
            let rng' = U.rand_rng rng in
            let zf = rand_until ~rng:rng' ~mutations ~merges ~samples ~t0 ~tf in
            match chan_mrca with
            | Some c ->
                let mrcas = Pop.time_mrcas (L.length samples) zf in
                Pop.out_all_mrca c k mrcas
            | None ->
                ()
          done ;
          Option.iter U.Csv.close_out chan_mrca
      end
  end

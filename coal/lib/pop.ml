(** Coalescent population of bundle of lineages *)

open Sig

module JC = Jump.Cadlag_map


let string_of_float_option =
  function
  | None ->
      ""
  | Some x ->
      string_of_float x


let mrca_columns n =
  let res = ref [] in
  for i = 1 to n do
    for j = 1 to (i - 1) do
      res := (Printf.sprintf "%i-%i" i j) :: !res
    done
  done ;
  "k" :: (L.rev !res)


module Make (Trait : TRAIT) =
  struct
    exception Already_sampled of int
    exception Already_merged of int * int * Intset.t * Intset.t

    module Traitmap = BatMap.Make (Trait)

    module S = State.Make (Trait)
    module T = Inter_tree.Make (S)
    module P = Parse.Make (Trait) (S) (T)

    type trait = Trait.t

    type tree = T.t

    type t = {
      forest : (Intset.t * Trait.t * tree) Intmap.t ;
      (** the atoms of the coalescent population,
          given unique ids *)
      bundles : (int * Intset.t * int) Intmap.t ;
      (** for each sample, the id of the bundle,
          its samples, and their number *)
      counts : U._int Traitmap.t ;
      (** the number of atoms of each trait value *)
      maxid : int ;
      (** maximum id used (starts from 1) *)
    }

    let empty () = {
      forest = Intmap.empty ;
      bundles = Intmap.empty ;
      counts = Traitmap.empty ;
      maxid = 0 ;
    }

    let of_forest forest' =
      let maxid, forest = L.fold_right (fun ksxtree (id, m) ->
        let id' = id + 1 in
        (id', Intmap.add id' ksxtree m)
      ) forest' (0, Intmap.empty)
      in
      let bundles = Intmap.fold (fun id (ks, _, _) ->
        Intset.fold (fun k -> Intmap.add k (id, ks, Intset.cardinal ks)) ks
      ) forest Intmap.empty
      in
      let counts = L.fold_right (fun (_, x, _) ->
        Traitmap.modify_def I.zero x I.succ
      ) forest' Traitmap.empty
      in
      { forest ; bundles ; counts ; maxid }

    let to_forest z =
      L.of_enum (Intmap.values z.forest)

    let count x z =
      match Traitmap.find x z.counts with
      | c ->
          c
      | exception Not_found ->
          I.zero

    let root_at t z =
      let forest = Intmap.map (fun (_, x, tree) ->
          let _, ks, y = T.root_value tree in
          (ks, x, T.node (t, ks, y) tree)
        ) z.forest
      in
      { z with forest }

    let output ~chan z =
      Intmap.iter (fun _ (ks, _, tree) ->
        Printf.fprintf chan "#tree %s\n" (Inter.to_string ks) ;
        T.output_newick chan tree ;
        output_string chan "\n\n"
      ) z.forest

    let to_string z =
      Intmap.fold (fun _ (ks, _, tree) s ->
        Printf.sprintf "%s#tree %s\n%s\n\n"
          s (Inter.to_string ks) (T.to_newick tree)
      ) z.forest ""

    let read ~chan =
      let lxbf = Lexing.from_channel chan in
      U.Option.map of_forest (P.forest Lex.read lxbf)

    let of_string s =
      let lxbf = Lexing.from_string s in
      U.Option.map of_forest (P.forest Lex.read lxbf)

    let bundle k (z : t) =
      Intmap.find k z.bundles

    let bundle_set ks z =
      bundle (Intset.any ks) z

    let coalesced k k' z =
      let id, _, _ = bundle k z in
      let id', _, _ = bundle k' z in
      (id = id')

    let find_set ks z =
      match bundle_set ks z with
      | id, _, n ->
          let js, x, tree = Intmap.find id z.forest in
          Some (id, n, js, x, tree)
      | exception Not_found ->
          None

    let card ~k z =
      match bundle k z with
      | _, _, n ->
          Some n
      | exception Not_found ->
          None

    let find ~k z =
      match bundle k z with
      | id, _, n ->
          let js, x, tree = Intmap.find id z.forest in
          Some (id, n, js, x, tree)
      | exception Not_found ->
          None

    let state_set ks z =
      U.Option.map (fun (_, _, _, x, _) ->
        x
      ) (find_set ks z)

    let card_state ~k (z : t) =
      U.Option.map (fun (_, n, _, x, _) ->
        (n, x)
      ) (find ~k z)

    let card_state_set ks z =
      U.Option.map (fun (_, n, _, x, _) ->
        (n, x)
      ) (find_set ks z)

    let sample ~dest k t (z : t) =
      (* get next id to use *)
      let id = z.maxid + 1 in
      let ks = Intset.singleton k in
      let tree = T.leaf (t, ks, dest) in
      if Intmap.mem k z.bundles then
        raise (Already_sampled k)
      else
        let forest = Intmap.add id (ks, dest, tree) z.forest in
        let bundles = Intmap.add k (id, ks, 1) z.bundles in
        let counts = Traitmap.modify_def I.zero dest I.succ z.counts in
        { forest ; bundles ; counts ; maxid = id }

    let mutate ~source ~dest k t z =
      let forest =
        match find ~k z with
        | Some (id, _, ks, x, tree) ->
            assert (Trait.equal source x) ;
            let tree' = T.node (t, ks, dest) tree in
            (* replace *)
            Intmap.add id (ks, dest, tree') z.forest
        | None ->
            raise Not_found
      in
      (* + dest - source *)
      let counts =
        z.counts
        |> Traitmap.modify_def I.zero dest I.succ
        |> Traitmap.modify source I.pred
      in
      { z with forest ; counts }


    let merge ~source ~source' ~dest k k' t z =
      let fo = U.Option.map2
        (fun (id, _, ks, x, tree) (id', _, ks', x', tree') ->
          assert (
             ((Trait.equal source x) && (Trait.equal source' x'))
          || ((Trait.equal source' x) && (Trait.equal source x'))
          ) ;
          if id = id' then
            raise (Already_merged (id, id', ks, ks'))
          else
            let js = Intset.union ks ks' in
            let tree'' = T.binode (t, js, dest) tree tree' in
            (* take id as new key *)
            let forest' =
              z.forest
              |> Intmap.remove id
              |> Intmap.remove id'
              |> Intmap.add id (js, dest, tree'')
            in
            (forest', id, js)
        ) (find ~k z) (find ~k:k' z)
      in
      let forest, id, js = U.Option.some fo in
      let n = Intset.cardinal js in
      let bundles = Intset.fold (fun j ->
        (* for each lineage in js, replace by (id, js) *)
        Intmap.add j (id, js, n)
      ) js z.bundles
      in
      (* + dest - source - source' *)
      let counts =
        z.counts
        |> Traitmap.modify_def I.zero dest I.succ
        |> Traitmap.modify source I.pred
        |> Traitmap.modify source' I.pred
      in
      { z with forest ; bundles ; counts }

    let time_mrca z k k' =
      let is_label_single i =
        let single_i = Intset.singleton i in
        (fun x -> Intset.equal (T.State.to_label x) single_i)
      in
      match find ~k:k z, find ~k:k' z with
      | None, None
      | Some _, None
      | None, Some _ ->
          (* if one or both label is absent, then None *)
          None
      | Some (id, _, _, _, tree), Some (id', _, _, _, _) ->
          if id = id' then begin
            (* in the same tree : coalesced already *)
            (* cannot use T.label_mrca, which uses Inter.equal,
             * which would match everywhere *)
            tree
            |> (fun t -> T.mrca t (is_label_single k) (is_label_single k'))
            |> U.Option.map T.State.to_time
          end else
            None

    let time_mrcas n z =
      let a = A.make_matrix n n None in
      for i = 0 to (n - 1) do
        for j = 0 to (i - 1) do
          a.(i).(j) <- time_mrca z (i + 1) (j + 1)
        done
      done ;
      a

    let output_mrcas ~chan n z =
      let a = A.to_list (A.map A.to_list (time_mrcas n z)) in
      let csv = L.map (L.map
        (function
         | None ->
             ""
         | Some x ->
             string_of_float x
        )
      ) a
      in
      Csv.output_all chan csv

    let triang_fold f x a =
      let n = A.length a in
      let res = ref x in
      for i = 0 to (n - 1) do
        for j = 0 to (i - 1) do
          res := f !res a.(i).(j)
        done
      done ;
      !res

    let header_all_mrca n =
      mrca_columns n

    let out_all_mrca chan k mrcas =
      let line = L.rev (
        triang_fold (fun l xo ->
          string_of_float_option xo :: l
        ) [] mrcas
      )
      in Csv.output_record chan ((string_of_int k) :: line)


    let out_min_max_mrca ~tf chan k mrcas =
      let sofo = string_of_float_option in
      let mino (x : float option) (y : float option) =
        match x, y with
        | None, None
        | None, Some _
        | Some _, None ->
            None
        | Some a, Some b ->
            Some (min a b)
      in
      let maxo (x : float option) (y : float option) =
        match x, y with
        | None, None ->
            None
        | None, Some a
        | Some a, None ->
            Some a
        | Some a, Some b ->
            Some (max a b)
      in
      (* What is the order None / Some ? *)
      let min_mrca = triang_fold mino (Some tf) mrcas in
      let max_mrca = triang_fold maxo None mrcas in
      let line = [
        string_of_int k ;
        sofo (min_mrca) ;
        sofo (mrcas.(1).(0)) ;
        sofo (max_mrca) ;
      ]
      in Csv.output_record chan line
  end

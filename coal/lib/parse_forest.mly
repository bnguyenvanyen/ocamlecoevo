%start <(BatSet.Int.t * Trait.t * T.t) list option> forest

%%

forest:
  | EOF
      { Some [] }
  | DIESE ; WTREE ; ks = set ; NEWLINE ; t = tree ; NEWLINE ; NEWLINE ; f = forest
      { Util.Option.map2 (fun tree f ->
          let _, _, x = T.root_value tree in
          (ks, x, tree) :: f
        ) t f }
  ;


tree:
  | t = state_tree(binode, snode, leaf, state)
      { t }
  ;


binode(node, state):
  | LPAREN ; lt = node ; COMMA ; rt = node ; RPAREN ; z = state
      { Util.Option.map3 T.binode z lt rt }
  ;


snode(node, state):
  | LPAREN ; t = node ; RPAREN ; z = state
      { Util.Option.map2 T.node z t }
  ;


leaf(state):
  | z = state
      { Util.Option.map T.leaf z }
  ;

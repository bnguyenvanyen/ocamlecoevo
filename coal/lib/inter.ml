include Sig.Intset


let root_label = empty


let to_string set =
  match pop_min set with
  | n0, set' ->
      let s0 = Printf.sprintf "{%i" n0 in
      let s = fold (fun n s -> Printf.sprintf "%s;%i" s n) set' s0 in
      Printf.sprintf "%s}" s
  | exception Not_found ->
      "{}"


let of_string s =
  let lxbf = Lexing.from_string s in
  Parse_inter.set_option Lex.read lxbf


(* apparently slower *)
let rec not_disjoint set set' =
  let f l x r s' =
    match split x s' with
    | _, true, _ ->
        (* x is in s' *)
        true
    | l', false, r' ->
        (* x is not in s' *)
        (not_disjoint l l') || (not_disjoint r r')
  in
  match any set with
  | exception Not_found ->
      false
  | x ->
      let l, _, r = split x set in
      f l x r set'


(* two sets are equal if they have elements in common *)
let equal set set' =
  not (disjoint set set')


(* we don't really care about the precise ordering here.
 * since the sets must be disjoint to be different,
 * we can just compare the smallest elements *)
let compare set set' =
  if equal set set' then
    0
  else match min_elt set with
  | n ->
      begin match min_elt set' with
      | n' ->
          BatInt.compare n n'
      | exception Not_found ->
          (* set' is the smallest *)
          BatInt.one
      end
  | exception Not_found ->
      (* empty set is the smallest set *)
      BatInt.minus_one


(* How can we do this so that it makes sense ?
 * We would need a canonical representation of the equivalence class *)
let hash _ =
  failwith "Not_implemented"



(** Lazy constant coalescent process simulation
  
    Like Generic, but for systems where we are able to give an upper bound
    on event rate (fuel consumption), whatever other events happen,
    which gives us a lower bound on event occurrence time.

 *)

open Sig


module Trait = Constant.Trait

module Color = Color_single.Make (Trait)
(* split color map *)
module Colormap = Colormap.Make (Trait) (Color)
(* faster find map for colors *)
module Cmaps = Colormap_single.Make (Trait) (Color)
module Prm = Prm.Make (Color) (Colormap)
module Evm = Sim.Evm_internal_lazy.Make (Color) (Cmaps) (Prm)
module Pop = Constant.Pop

module Ctmjp = Sim.Ctmjp.Prm_internal_lazy.Make (Color) (Prm) (Evm)

module Simulate =
  struct
    (* conversion forward/backward time *)
    let reverse t =
      F.neg t

    (* starting fuel is 0 at tf (the event should happen at once) *)
    let forward_to_fuel ~(tf : _ U.anyfloat) t =
      F.Pos.of_anyfloat F.Op.(tf - t)

    module JC = Jump.Cadlag_map

    module Traj =
      struct

        type t = int JC.t M.Int.t

        let empty = M.Int.empty

        let add _ (_, traj) (_ : t) : t =
          traj

        let merge t js n traj =
          Intset.fold (fun j ->
            Intmap.modify j (JC.add t n)
          ) js traj

        let sample t k traj =
          let c = JC.add t 1 JC.undefined in
          Intmap.add k c traj

        let cardinal ~from_t ~to_t k traj =
          M.Int.find k traj
          |> JC.cut from_t to_t
      end


    module Merge =
      struct
        let fuel_consumed lbd k k' ~from_t ~to_t ~traj =
          let c = Traj.cardinal ~from_t ~to_t k traj in
          let c' = Traj.cardinal ~from_t ~to_t k' traj in
          let rate = Jump.Cadlag_map.map2 (fun no no' ->
            match no, no' with
            | Some n, Some n' ->
                let pn = F.Pos.of_float (float n) in
                let pn' = F.Pos.of_float (float n') in
                F.Pos.Op.(lbd / (pn * pn'))
            | Some _, None
            | None, Some _
            | None, None ->
                F.zero
          ) c c'
          in
          Jump.Cadlag_map.integrate ~from_t ~to_t Fun.id rate

        let update_fuel lbd k k' ~from_t ~to_t ~traj fuel =
          let ds = fuel_consumed lbd k k' ~from_t ~to_t ~traj in
          F.Op.(fuel - ds)

        (* earliest time the event can happen, given fuel at t *)
        (* t_k is the time at which k is sampled
         * (in the backward sim referential) *)
        let project_time lbd k t_k k' t_k' ~(fuel : Prm.point) (z, _) t =
          match Pop.card_state ~k z, Pop.card_state ~k:k' z with
          | Some (n, _), Some (n', _) ->
              (* if k and k' in the same bundle, rate is zero *)
              if Pop.coalesced k k' z then
                F.infinity
              else
                let pn = F.Pos.of_float (float n) in
                let pn' = F.Pos.of_float (float n') in
                F.Op.(t + fuel * pn * pn' / lbd)
          (* for the remaining cases,
           * at least one of the lineages has not yet been sampled.
           * we take mass 1 for the missing lineages. *)
          | Some (n, _), None ->
              let pn = F.Pos.of_float (float n) in
              F.Op.(t_k' + fuel * pn / lbd)
          | None, Some (n, _) ->
              let pn = F.Pos.of_float (float n) in
              F.Op.(t_k + fuel * pn / lbd)
          | None, None ->
              F.Op.(F.max t_k t_k' + fuel / lbd)

        let update_state k k' t (z, traj) =
          assert (k <> k') ;
          let fwd_t = F.to_float (reverse t) in
          match Pop.merge
            ~source:()
            ~source':()
            ~dest:()
            k
            k'
            fwd_t
            z
          with
          | z' ->
              let _, js, n = Pop.bundle k z' in
              let traj' = Traj.merge t js n traj in
              (z', traj')
          | exception (Pop.Already_merged (id, id', ks, ks') as e) ->
              (* This should never happen with no bugs *)
              Printf.eprintf
              "Already merged:\n%i and %i,\n%i and %i,\n%s and %s\n"
              k k' id id' (Inter.to_string ks) (Inter.to_string ks') ;
              raise e

        let color k k' =
          Color.merge () () ()
            k
            k'

        let add_event lbd k t_k k' t_k' t z evsnu =
          let c = color k k' in
          Evm.add
            (update_fuel lbd k k')
            (project_time lbd k t_k k' t_k')
            (update_state k k')
            true
            c
            t
            z
            evsnu
      end

    module Sample =
      struct
        let update_fuel ~from_t ~to_t ~traj:_ fuel =
          F.Op.(fuel - (to_t - from_t))

        let project_time ~fuel _ t =
          F.Op.(t + fuel)
        
        let update_state k t (z, traj) =
          let fwd_t = F.to_float (reverse t) in
          let z' = Pop.sample ~dest:() k fwd_t z in
          let traj' = Traj.sample t k traj in
          (z', traj')

        let add_event k t z evsnu =
          Evm.add
            update_fuel
            project_time
            (update_state k)
            true
            (Color.sample () k)
            t
            z
            evsnu
      end

    let time ~samples k =
      let _, t, () = L.find (fun (j, _, ()) -> j = k) samples in
      reverse t

    let events ~samples lbd prm t z =
      let n = L.length samples in
      (Evm.empty, prm)
      (* add sample events *)
      |> L.fold_right (fun (k, _, ()) ->
          Sample.add_event k t z
      ) samples
      (* add merge events *)
      |> U.int_fold_right ~f:(fun k' ->
           U.int_fold_right ~f:(fun k ->
             assert (k < k') ;
             (* sample time for k in the backward sim referential *)
             let t_k = time ~samples k in
             let t_k' = time ~samples k' in
             Merge.add_event lbd k t_k k' t_k' t z
           ) ~n:(k' - 1)
         ) ~n

    (* n : number of sequences
     * nu : discrete measure to add to *)
    let add_rand_merges ~rng n nu =
      let f k k' =
        let c = Merge.color k k' in
        let t = U.rand_exp ~rng F.one in
        let point = t in
        Prm.add_single c point
      in
      U.int_fold_right ~f:(fun k ->
        U.int_fold_right ~f:(fun k' ->
          f k k'
        ) ~n:(k - 1)
      ) ~n nu
    
    module Backward =
      struct
        let add_samples ~tf samples nu =
          L.fold_right (fun (k, fwd_t, ()) ->
            let c = Color.sample () k in
            let back_t = forward_to_fuel ~tf fwd_t in
            let point = back_t in
            Prm.add_single c point
          ) samples nu

        let until ~t0 ~tf ~lambda ~samples nu =
          let output = `Trio Util.Out.convert_null in
          let back_t0 = reverse tf in
          let back_tf = reverse t0 in
          (* z0 actually corresponds to tf *)
          let z0 = Pop.empty () in
          let traj0 = Traj.empty in
          let evm, nu' =
            events ~samples lambda (Prm.copy nu) back_t0 (z0, traj0)
          in
          let _, ((zf, _), nuf, evmf) = Ctmjp.simulate_until
            ~output
            Traj.empty
            Traj.add
            ~t0:back_t0
            evm
            (z0, traj0)
            nu'
            back_tf
          in
          let rooted_zf = Pop.root_at (F.to_float t0) zf in
          (rooted_zf, nuf, evmf)

        let until_merged ~tf ~lambda ~samples nu =
          let output = `Pair Util.Out.convert_null in
          let back_t0 = reverse tf in
          let z0 = Pop.empty () in
          let traj0 = Traj.empty in
          let evm, nu' =
            events ~samples lambda (Prm.copy nu) back_t0 (z0, traj0)
          in
          let nlin = L.length samples in
          let stop t (z, _) =
            match Pop.card ~k:1 z with
            | None ->
                None
            | Some n ->
                if n = nlin then
                  Some t
                else
                  None
          in
          let back_tf, ((zf, _), nuf) = Ctmjp.simulate
            ~output
            Traj.empty
            Traj.add
            ~t0:back_t0
            stop
            evm
            (z0, traj0)
            nu'
          in 
          let t0 = reverse back_tf in
          let rooted_zf = Pop.root_at (F.to_float t0) zf in
          (rooted_zf, nuf)

        let rand_prm ~rng ~samples ~tf =
          let n = L.length samples in
          Prm.empty ()
          |> add_rand_merges ~rng n
          |> add_samples ~tf samples

        let rand_until ~rng ~lambda ~samples ~t0 ~tf =
          let nu =
            rand_prm ~rng ~samples ~tf
          in
          let pop, _, _ = until ~t0 ~tf ~lambda ~samples nu in
          pop

        let many_rand_until_out_mrca ?seed ?path ~lambda ~samples ~n ~t0 ~tf =
          let rng = U.rng seed in
          let chan_mrca = Option.map (fun p ->
            U.Csv.open_out (p ^ ".mrca.csv")
          ) path
          in
          Option.iter (fun c ->
            Csv.output_record c (Pop.header_all_mrca (L.length samples))
          ) chan_mrca
          ;
          for k = 1 to n do
            let rng' = U.rand_rng rng in
            let zf = rand_until ~rng:rng' ~lambda ~samples ~t0 ~tf in
            match chan_mrca with
            | Some c ->
                let mrcas = Pop.time_mrcas (L.length samples) zf in
                Pop.out_all_mrca c k mrcas
            | None ->
                ()
          done ;
          Option.iter U.Csv.close_out chan_mrca
      end
  end


module Term =
  struct
    module C = Cmdliner

    let many_rand_until_out_mrca =
      let flat seed n samples lambda t0 tf path =
        let t = Sys.time () in
        Simulate.Backward.many_rand_until_out_mrca
          ?seed
          ?path
          ~n
          ~samples
          ~lambda
          ~t0
          ~tf
        ;
        Printf.printf "Execution CPU time: %f\n" (Sys.time () -. t)
      in
      C.Term.(const flat
          $ Term.seed
          $ Term.niter
          $ Constant.Term.samples ()
          $ Constant.Term.lambda ()
          $ Term.t0 ()
          $ Term.tf ()
          $ Term.out
      )
    
    let info =
      let doc =
          "Simulate Kingman's coalescent with internal times, "
        ^ "with the 'lazy' algorithm."
      in
      Cmdliner.Term.info
        "coal-constant-lazy"
        ~version:"%%VERSION%%"
        ~doc
        ~exits:Cmdliner.Term.default_exits
        ~man:[]
  end

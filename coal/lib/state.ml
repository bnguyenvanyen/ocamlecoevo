open Sig


type 'a t = float * Inter.t * 'a


module type S =
  sig
    type trait
    type label = Inter.t
    include Tree.Sig.TIMED with type t = trait t
    include Tree.Sig.LABELED with type t := t and type label := label
    include Util.Interfaces.REPRABLE with type t := t
  end


module Make (Trait : TRAIT) =
  (* (S with type trait = Trait.t) = *)
  struct
    type trait = Trait.t
    type label = Inter.t
    type nonrec t = trait t

    module Label = Inter

    let to_label (_, k, _) = k

    let to_time (t, _, _) = t

    let at_time t' (_, k, x) = (t', k, x)

    let to_string (t, k, x) =
      Printf.sprintf "%s|%s:%.3f"
      (Label.to_string k)
      (Trait.to_string x)
      t

    let of_string s =
      match Scanf.sscanf s "%s@|%s@:%f" (fun ks xs t -> (ks, xs, t)) with
      | ks, xs, t ->
          U.Option.map2 (fun k x ->
              (t, k, x)
            ) (Label.of_string ks) (Trait.of_string xs)
      | exception Scanf.Scan_failure _ ->
          None
  end

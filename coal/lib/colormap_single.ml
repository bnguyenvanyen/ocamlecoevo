(** A map but with the different types of events separated 
 *
 *  See Generic
 *)


open Sig

module type S =
  sig
    type key
    type +'a t

    val empty : 'a t
    val find : key -> 'a t -> 'a
    val add : key -> 'a -> 'a t -> 'a t
    val remove : key -> 'a t -> 'a t
    val extract : key -> 'a t -> 'a * 'a t
    val modify : key -> ('a -> 'a) -> 'a t -> 'a t
  end


module M = BatMap


module Make
  (Trait : TRAIT)
  (C : COLOR_SINGLE with type trait = Trait.t) :
  (S with type key = C.t) =
  struct
    module Cm = M.Make (C)
    module Tm = M.Make (Trait)
    module T2m = M.Make (struct
      type t = Trait.t * Trait.t
      let compare (x1, x2) (x1', x2') =
        let c = Trait.compare x1 x1' in
        if c = 0 then
          Trait.compare x2 x2'
        else
          c
    end)
    module T3m = M.Make (struct
      type t = Trait.t * Trait.t * Trait.t
      let compare (x1, x2, x3) (x1', x2', x3') =
        let c1 = Trait.compare x1 x1' in
        if c1 = 0 then
          let c2 = Trait.compare x2 x2' in
          if c2 = 0 then
            Trait.compare x3 x3'
          else
            c2
        else
          c1
    end)
    module ITm = M.Make (struct
      type t = int * Trait.t
      let compare ((k : int), x) (k', x') =
        let c = compare k k' in
        if c = 0 then
          Trait.compare x x'
        else
          c
    end)

    type key = C.t

    type 'a t = {
      samples : 'a ITm.t ;
      mutations : 'a M.Int.t T2m.t ;
      merges : 'a M.Int.t M.Int.t T3m.t ;
    }

    let empty = {
      samples = ITm.empty ;
      mutations = T2m.empty ;
      merges = T3m.empty ;
    }

    let add (c : C.t) z m =
      match c with
      | Sample (x, k) ->
          { m with samples = ITm.add (k, x) z m.samples }
      | Mutation (x, y, k) ->
          let muts = m.mutations in
          let muts' = T2m.modify_def
            M.Int.empty
            (x, y)
            (M.Int.add k z)
            muts
          in
          { m with mutations = muts' }
      | Merge (x, x', y, k, k') ->
          let mrg = m.merges in
          let mrg' = T3m.modify_def
            M.Int.empty
            (x, x', y)
            (M.Int.modify_def
              M.Int.empty
              k
              (M.Int.add k' z)
            )
            mrg
          in
          { m with merges = mrg' }

    let remove (c : C.t) m =
      match c with
      | Sample (x, k) ->
          { m with samples = ITm.remove (k, x) m.samples }
      | Mutation (x, y, k) ->
          let muts = m.mutations in
          let muts' = T2m.modify
            (x, y)
            (M.Int.remove k)
            muts
          in
          { m with mutations = muts' }
      | Merge (x, x', y, k, k') ->
          let mrg = m.merges in
          let mrg' = T3m.modify
            (x, x', y)
            (M.Int.modify
              k
              (M.Int.remove k')
            )
            mrg
          in
          { m with merges = mrg' }

    let find (c : C.t) m =
      match c with
      | Sample (x, k) ->
          ITm.find (k, x) m.samples
      | Mutation (x, y, k) ->
          m.mutations
          |> T2m.find (x, y)
          |> M.Int.find k
      | Merge (x, x', y, k, k') ->
          m.merges
          |> T3m.find (x, x', y)
          |> M.Int.find k
          |> M.Int.find k'

    let modify (c : C.t) f m =
      match c with
      | Sample (x, k) ->
          { m with samples = ITm.modify (k, x) f m.samples }
      | Mutation (x, y, k) ->
          let muts = m.mutations in
          let muts' = T2m.modify
            (x, y)
            (M.Int.modify k f)
            muts
          in
          { m with mutations = muts' }
      | Merge (x, x', y, k, k') ->
          let mrg = m.merges in
          let mrg' = T3m.modify
            (x, x', y)
            (M.Int.modify
              k
              (M.Int.modify k' f)
            )
            mrg
          in
          { m with merges = mrg' }

    let extract (c : C.t) m =
      match c with
      | Sample (x, k) ->
          let v, samples = ITm.extract (k, x) m.samples in
          v, { m with samples }
      | Mutation (x, y, k) ->
          let muts = m.mutations in
          let mxy = T2m.find (x, y) muts in
          let v, mxy' = M.Int.extract k mxy in
          let muts' = T2m.modify (x, y) (fun _ -> mxy') muts in
          v, { m with mutations = muts' }
      | Merge (x, x', y, k, k') ->
          let mrg = m.merges in
          let mxxy = T3m.find (x, x', y) mrg in
          let mxxyk = M.Int.find k mxxy in
          let v, mxxyk' = M.Int.extract k' mxxyk in
          let mxxy' = M.Int.modify k (fun _ -> mxxyk') mxxy in
          let mrg' = T3m.modify (x, x', y) (fun _ -> mxxy') mrg in
          v, { m with merges = mrg' }
  end

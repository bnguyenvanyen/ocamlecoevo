{
  module T = Tokens
}


let white = [' ' '\t']+
let newline = '\r' | '\n' | "\r\n"

let lower = ['a'-'z']
let upper = ['A'-'Z']
let digit = ['0'-'9']
let letter = lower | upper
let sign = [':' '.' '-']
let singular = [^'s']
let unused_sign = ['<' '>' '[' ']' '-' '_' '@' '/' '+' '*']

let number = digit+
let frac = '.' digit*
let exp = ['e' 'E'] ['+' '-']? digit+
let float = '-'? digit frac? exp?
let word = (upper? lower+) | (upper+)
let other = digit | float | letter | unused_sign | white
let trait = other*

let wtree = "tree" singular


rule read =
  parse
  | white   { read lexbuf }
  | newline { Lexing.new_line lexbuf ; T.NEWLINE }
  | wtree   { T.WTREE }
  (* | trait   { T.TRAIT (Lexing.lexeme lexbuf) } *)
  | number  { T.NUMBER (Lexing.lexeme lexbuf) }
  | float   { T.FLOAT (Lexing.lexeme lexbuf) }
  | word    { T.WORD  (Lexing.lexeme lexbuf) }
  | '_'     { T.UNDER }
  | '/'     { T.SLASH }
  | ';'     { T.SEMIC }
  | ']'     { T.RSQUARE }
  | ')'     { T.RPAREN }
  | '}'     { T.RCURLY }
  | '['     { T.LSQUARE }
  | '('     { T.LPAREN }
  | '{'     { T.LCURLY }
  | eof     { T.EOF }
  | '='     { T.EQUAL }
  | '#'     { T.DIESE }
  | ','     { T.COMMA }
  | ':'     { T.COLON }
  | '|'     { T.BAR }


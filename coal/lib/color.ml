(** See Generic *)


open Sig

(** Coalescent event colors (Sample, Mutation, and Merge) *)
module Make (Trait : TRAIT) :
  (COLOR with type trait = Trait.t) =
  struct
    type trait = Trait.t

    type t =
      | Sample of Trait.t * int
        (** (dest, index) *)
      | Mutation of Trait.t * Trait.t * Intset.t
        (** (source, dest, indices) *)
      | Merge of Trait.t * Trait.t * Trait.t * Intset.t * Intset.t
        (** (source, source', dest, indices, indices'),
            with source <= source', and indices <= indices' *)

    let sample x k = Sample (x, k)

    let mutation src dst ks = Mutation (src, dst, ks)

    (* canonical merge color :
       symmetric : no distinction depending on the order of ks, is,
       or the association with x y
     *)
    let merge x x' y ks ks' =
      let x1, x2 =
        if Trait.compare x x' < 0 then
          x, x'
        else
          x', x
      in
      let ks1, ks2 =
        let c = Inter.compare ks ks' in
        if c < 0 then
          ks, ks'
        else if c > 0 then
          ks', ks
        else (* ks = ks', intersection not empty *)
          invalid_arg "no such merge color"
      in
      Merge (x1, x2, y, ks1, ks2)

    let kind =
      function
      | Sample _ ->
          `Sample
      | Mutation _ ->
          `Mutation
      | Merge _ ->
          `Merge

    let compare (col : t) (col' : t) =
      compare col col'

    let equal c c' = (compare c c' = 0)

    let to_string =
      function
      | Sample (x, n) ->
          Printf.sprintf "sample(%s,%i)" (Trait.to_string x) n
      | Mutation (x, y, ns) ->
          Printf.sprintf "mutation(%s,%s,%s)"
          (Trait.to_string x)
          (Trait.to_string y)
          (Inter.to_string ns)
      | Merge (x, y, z, is, js) ->
          Printf.sprintf "merge(%s,%s,%s,%s,%s)"
          (Trait.to_string x)
          (Trait.to_string y)
          (Trait.to_string z)
          (Inter.to_string is)
          (Inter.to_string js)

    let of_string s =
      let scan = Scanf.sscanf in
      let trait s =
        Util.Option.some (Trait.of_string s)
      in
      let set s =
        Util.Option.some (Inter.of_string s)
      in
      let to_sample s n =
        Sample (trait s, n)
      in
      let to_mut s1 s2 s3 =
        Mutation (trait s1, trait s2, set s3)
      in
      let to_merge s1 s2 s3 s4 s5 =
        Merge (trait s1, trait s2, trait s3, set s4, set s5)
      in
      match scan s "sample(%s@,%i)" to_sample with
      | col ->
          Some col
      | exception (Invalid_argument _ | Scanf.Scan_failure _) ->
          begin match scan s "mutation(%s@,%s@,%s@)" to_mut with
          | col ->
              Some col
          | exception (Invalid_argument _ | Scanf.Scan_failure _) ->
              begin match scan s "merge(%s@,%s@,%s@,%s@,%s@)" to_merge with
              | col ->
                  Some col
              | exception (Invalid_argument _ | Scanf.Scan_failure _) ->
                  None
              end
          end
  end

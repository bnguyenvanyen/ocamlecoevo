%parameter<Trait : Sig.TRAIT>

(* It would be more natural to put these in parse_forest,
 * but then the order is not guaranteed *)

%parameter<S : State.S with type trait = Trait.t>

%parameter<T : Tree.Labeltimed.S with type state = S.t>

%%

%public
state:
  | ks = set ; BAR ; xo = trait ; COLON ; t = time
      { Util.Option.map (fun x -> (t, ks, x)) xo }
  ;


time:
  | k = FLOAT
      { float_of_string k }
  ;


trait:
  | s = any
      { Trait.of_string s }
  ;


any:
  | l = nonempty_list(any_token)
      { BatList.reduce (^) l }
;


any_token:
  | s = WORD
      { s }
  | UNDER
      { "_" }
  | SLASH
      { "/" }
  | SEMIC
      { ";" }
  | RSQUARE
      { "]" }
  | RCURLY
      { "}" }
  | k = NUMBER
      { k }
  | LSQUARE
      { "[" }
  | LCURLY
      { "{" }
  | EQUAL
      { "=" }
  | DIESE
      { "#" }
  ;

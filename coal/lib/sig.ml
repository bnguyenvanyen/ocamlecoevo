module L = BatList
module A = BatArray
module M = BatMap
module H = BatHashtbl

module Intset = BatSet.Int
module Intmap = BatMap.Int

module U = Util
module F = U.Float
module I = U.Int



module type TRAIT =
  sig
    open Util.Interfaces

    include Tree.Sig.STATE

    include EQUALABLE with type t := t
    include COMPARABLE with type t := t
  end


module type COLOR =
  sig
    type trait

    type t = private
      | Sample of trait * int
      | Mutation of trait * trait * Intset.t
      | Merge of trait * trait * trait * Intset.t * Intset.t

    val sample : trait -> int -> t

    val mutation : trait -> trait -> Intset.t -> t

    val merge : trait -> trait -> trait -> Intset.t -> Intset.t -> t

    val kind : t -> [`Sample | `Mutation | `Merge]

    include Sim.Sig.COLOR with type t := t
  end


module type COLOR_SINGLE =
  sig
    type trait

    type t = private
      | Sample of trait * int
      | Mutation of trait * trait * int
      | Merge of trait * trait * trait * int * int

    val sample : trait -> int -> t

    val mutation : trait -> trait -> int -> t

    val merge : trait -> trait -> trait -> int -> int -> t

    val kind : t -> [`Sample | `Mutation | `Merge]

    include Sim.Sig.COLOR with type t := t
  end


module type COLOR_ANY =
  sig
    type trait

    type t

    val kind : t -> [`Sample | `Mutation | `Merge]

    include Sim.Sig.COLOR with type t := t
  end

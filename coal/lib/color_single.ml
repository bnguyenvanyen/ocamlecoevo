(** Coalescent event colors,
  
    In the case where events are associated to single samples,
    and not to lineage bundles.
 *)


open Sig


module Make (Trait : TRAIT) :
  (COLOR_SINGLE with type trait = Trait.t) =
  struct
    type trait = Trait.t

    type t =
      | Sample of Trait.t * int
        (** (dest, index) *)
      | Mutation of Trait.t * Trait.t * int
        (** (source, dest, sample index) *)
      | Merge of Trait.t * Trait.t * Trait.t * int * int
        (** (source, source', dest, index, index'),
            source <= source', index <= index' *)

    let sample x k = Sample (x, k)

    let mutation src dst k = Mutation (src, dst, k)

    (* canonical merge color -> symmetry equivalence class *)
    let merge x x' y k k' =
      let x1, x2 =
        if Trait.compare x x' < 0 then
          x, x'
        else
          x', x
      in
      let k1, k2 =
        if k < k' then
          k, k'
        else if k > k' then
          k', k
        else (* k = k' *)
          invalid_arg "no merge color with k = k'"
      in
      Merge (x1, x2, y, k1, k2)

    let kind =
      function
      | Sample _ ->
          `Sample
      | Mutation _ ->
          `Mutation
      | Merge _ ->
          `Merge

    let compare (col : t) (col' : t) =
      match col, col' with
      | Sample _, Mutation _
      | Sample _, Merge _
      | Mutation _, Merge _ ->
          ~-1
      | Mutation _, Sample _
      | Merge _, Sample _
      | Merge _, Mutation _ ->
          1
      | Sample (x, k), Sample (x', k') ->
          let c = compare k k' in
          if c = 0 then
            Trait.compare x x'
          else
            c
      | Mutation (src, dst, k), Mutation (src', dst', k') ->
          let c = compare k k' in
          if c = 0 then
            let c' = Trait.compare src src' in
            if c' = 0 then
              Trait.compare dst dst'
            else
              c'
          else
            c
      | Merge (src1, src2, dst, k1, k2),
        Merge (src1', src2', dst', k1', k2') ->
          let c1 = compare k1 k1' in
          if c1 = 0 then
            let c2 = compare k2 k2' in
            if c2 = 0 then
              let c3 = Trait.compare src1 src1' in
              if c3 = 0 then
                let c4 = Trait.compare src2 src2' in
                if c4 = 0 then
                  Trait.compare dst dst'
                else
                  c4
              else
                c3
            else
              c2
          else
            c1

    let equal c c' = (compare c c' = 0)

    let to_string =
      function
      | Sample (x, n) ->
          Printf.sprintf "sample(%s,%i)" (Trait.to_string x) n
      | Mutation (x, y, n) ->
          Printf.sprintf "mutation(%s,%s,%i)"
          (Trait.to_string x)
          (Trait.to_string y)
          n
      | Merge (x, y, z, i, j) ->
          Printf.sprintf "merge(%s,%s,%s,%i,%i)"
          (Trait.to_string x)
          (Trait.to_string y)
          (Trait.to_string z)
          i
          j

    let of_string s =
      let scan = Scanf.sscanf in
      let trait s =
        Util.Option.some (Trait.of_string s)
      in
      let to_sample s n =
        sample (trait s) n
      in
      let to_mut s1 s2 k3 =
        mutation (trait s1) (trait s2) k3
      in
      let to_merge s1 s2 s3 k4 k5 =
        merge (trait s1) (trait s2) (trait s3) k4 k5
      in
      match scan s "sample(%s@,%i)" to_sample with
      | col ->
          Some col
      | exception (Invalid_argument _ | Scanf.Scan_failure _) ->
          begin match scan s "mutation(%s@,%s@,%i)" to_mut with
          | col ->
              Some col
          | exception (Invalid_argument _ | Scanf.Scan_failure _) ->
              begin match scan s "merge(%s@,%s@,%s@,%i,%i)" to_merge with
              | col ->
                  Some col
              | exception (Invalid_argument _ | Scanf.Scan_failure _) ->
                  None
              end
          end
  end

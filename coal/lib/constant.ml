open Sig


module Trait =
  struct
    type t = unit

    let compare _ _ = 0
    let equal _ _ = true

    let to_string () = "_"

    let of_string =
      function
      | "_" ->
          Some ()
      | _ ->
          None
  end


module Gen = Generic.Make (Trait)

module Pop = Gen.Pop


let mutations = []

let merges lambda =
  [
    ((), (), (), (fun () () -> `Constant lambda))
  ]


module Simulate =
  struct
    open Gen

    let color_merge ks is =
      Color.merge () () () ks is

    let until ~lambda ~n ~t0 ~tf nu =
      (* we can create a dummy samples list for evm :
       * the trait value is always (),
       * and the time doesn't matter for events *)
      let samples = U.int_fold ~f:(fun l k ->
          (k, F.zero, ()) :: l
        ) ~n ~x:[]
      in
      let merges = merges lambda in
      Backward.until ~t0 ~tf ~mutations ~merges ~samples nu

    let rand_prm ~rng ~samples ~lambda ~tf =
      let merges = merges lambda in
      Backward.rand_prm ~rng ~mutations ~merges ~samples ~tf

    let rand_until ~rng ~samples ~lambda ~t0 ~tf =
      let merges = merges lambda in
      Backward.rand_until ~rng ~mutations ~merges ~samples ~t0 ~tf

    let many_rand_until_out_mrca ?seed ?path ~n ~samples ~lambda ~t0 ~tf =
      let merges = merges lambda in
      Backward.many_rand_until_out_mrca
        ?seed
        ~mutations
        ~merges
        ~samples
        ~n
        ~t0
        ~tf
        ?path
  end


module Term =
  struct
    open Term
    open Cmdliner

    let samples () =
      let doc = "Sampling times of the coalescent." in
      let times = Arg.(
          non_empty
        & opt (list float) []
        & info ["samples"] ~docv:"SAMPLES" ~doc
      )
      in
      let f times =
        (* start from the most recent *)
        let sorted = L.sort (fun t t' -> compare t' t) times in
        L.mapi (fun k t -> (k + 1, F.Pos.of_float t, ())) sorted
      in
      Term.(const f $ times)

    let lambda () =
      let doc = "Pair coalescence rate." in
      positive_req ~doc ~docv:"LAMBDA" ~arg:"lambda"

    let many_rand_until_out_mrca =
      let flat seed n samples lambda t0 tf path =
        let t = Sys.time () in
        Simulate.many_rand_until_out_mrca
          ?seed
          ?path
          ~n
          ~samples
          ~lambda:(F.to_float lambda)
          ~t0
          ~tf
        ;
        Printf.printf "Execution CPU time: %f\n" (Sys.time () -. t)
      in
      Term.(const flat
          $ seed
          $ niter
          $ samples ()
          $ lambda ()
          $ t0 ()
          $ tf ()
          $ out
      )

    let info =
      let doc =
        "Simulate Kingman's coalescent with internal times."
      in
      Cmdliner.Term.info
        "coal-constant"
        ~version:"%%VERSION%%"
        ~doc
        ~exits:Cmdliner.Term.default_exits
        ~man:[]
  end

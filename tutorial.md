
Tutorial
========

In this tutorial,
we will see how the different sublibraries can be used in conjunction
to define increasingly complex systems,
simulate them in various ways,
and do bayesian inference on them.

We will assume the reader has a basic knowledge of OCaml,
and in particular of its type system and module system.
Some good resources to get acquainted with it are :
- The tutorials at https://ocaml.org/learn/tutorials/
- Real World OCaml, https://realworldocaml.org/v1/en/html/index.html 
- The language reference, https://caml.inria.fr/pub/docs/manual-ocaml/language.html
- The standard library, https://caml.inria.fr/pub/docs/manual-ocaml/stdlib.html

We advise the reader to follow along using `utop` (https://opam.ocaml.org/blog/about-utop/).

We will walk the reader through defining and simulating a stochastic SIR
system,
fitting some data with this model,
and complexifying the model to track viral ancestry 
and simulate neutral evolution of viral sequences.



1 - Define a simple system
--------------------------

The code for this section is present in [Sir](src/epi/sir.ml).

The library that is directly concerned with simulations is ocamlsim,
accessible through the `Sim` module.

To get access to it in utop
```ocaml
utop # #require "ocamlsim";;
```

You can then use the utop directive `#show` to show the types and signatures.
```ocaml
utop # #show Sim;;
module Sim :
  sig
    module Arg = Sim_arg
    module Sig = Sim_sig
    module Out = Sim_out
    module Dopri5 : sig  end
    module Euler_multinomial : sig  end
    module Gill : sig  end
    module Poisson : sig  end
    module Stopped_poisson : sig  end
    module Concat :
      functor
        (S : Sig.SYSTEM) (S' : sig
                                 type t = S.t
                                 type param
                                 val evl : (param, t) Sig.event list
                               end) ->
        sig  end
  end
```

`Sim.Sig` defines `SYSTEM` for stochastic systems
and is used by our stochastic integrators.

```ocaml
type ('a, 'b) rate = ('a -> float -> 'b -> float)
type ('a, 'b) modif = ('a -> Random.State.t -> float -> 'b -> 'b)
type ('a, 'b) event = ('a, 'b) rate * ('a, 'b) modif

(** module type for stochastic systems *)
module type SYSTEM =
  sig
    (** state space of the process *)
    type t
    (** parameters of the system *)
    type param
    (** list of events *)
    val evl : (param, t) event list
  end
```

Those stochastic integrators are basically functors
following the `ALGO` signature
```ocaml
(** module type for most algorithms *)
module type ALGO =
  sig
    (** parameters of the algorithm *)
    type algparam

    (** arguments for CLIs *)
    val specl : (string * algparam Sim_arg.spec * string) list

    (** default algparam *)
    val default : algparam

    (** functor to obtain a simulate function to integrate with *)
    module Make :
      functor (S : SYSTEM) ->
      functor (O : OUTSPEC with type t = S.t
                            and type param = S.param) ->
      (INTEGR with type t = S.t
               and type param = S.param
               and type outparam = O.outparam
               and type algparam = algparam)
  end
```
And this tutorial will try to explain what this means.
To do so, as an example, we will try to simulate the SIR system.

First, we will define a system `S` that follows this `SYSTEM` signature.
The state of the system is the triplet of number of susceptibles,
infected, and removed, which we can represent by `int * int * int`.
But we also want to know the number of cases reported
-- those that we are aware of -- for later inference.
Thus we use

```ocaml
type t = int * int * int * int  (* (S, I, R, C) *)
```

The parameters of the system are beta, the infectivity,
nu, the recovery rate, and rho, the reporting-on-infection probability.
So we simply define

```ocaml
type param = {
  beta : float ;  (* infectivity *)
  nu : float ;  (* recovery rate *)
  rho : float ;  (* reporting proba *)
};;
```

Now for the interesting part, the dynamics.
The events affecting the system are infection and recovery.

Rate functions have type
```ocaml
(param -> float -> t -> float)
```
From the parameters of the system, the time and the state of the system,
they should give us the rate of the event.

For infection, we can write
```ocaml
let inf_rate par _ (s, i, r, _) =
  let n = s + i + r in
  par.beta *. float i *. float s /. (float n);;
```
And for recovery
```ocaml
let recov_rate par _ (_, i, _, _) =
  par.nu *. float i;;
```

For modifications, the type is
```ocaml
(param -> Random.State.t -> float -> t -> t)
```
Where `Random.State.t` is a random generator, which can be obtained by
`Random.get_state ()` -- the default generator --
or `Random.State.make_self_init () -- a new self-initialized generator --
for instance.

For infection, we will use it,
as we need to decide whether the case will be reported or not.
Another (equivalent) possibility would be to consider two events,
infection-without-report and infection-with-report.
(With Gillespie, that actually means less random draws).

```ocaml
let inf_modif par rng _ (s, i, r, c) =
  if Util.rand_bernoulli rng par.rho then  (* report *)
    (s - 1, i + 1, r, c + 1)
  else
    (s - 1, i + 1, r, c);;
```

For recovery,
```ocaml
let recov_modif _ rng _ (s, i, r, c) =
  (s, i - 1, r + 1, c);;
```
We can now define the list of events
```ocaml
let evl = [ (inf_rate, inf_modif) ; (recov_rate, recov_modif) ];;
```

This is enough to simulate the system,
but we also want to specify how information will be output back from
the simulation.
For that, we need to give a module following the `Sim.Sig.OUTSPEC` signature,
which is :
```ocaml
(** module type for output specification *)
module type OUTSPEC =
  sig
    (** state space of the process *)
    type t
    (** parameters of the system *)
    type param
    (** parameters for output *)
    type outparam
    (** command line arguments *)
    val specl : (string * outparam Sim_arg.spec * string) list
    (* FIXME problem if t mutable : can be affected by output *)
    val default : outparam
    (** begin output *)
    val output_begin : outparam -> param -> unit
    (** output at each timestep *)
    val output : outparam -> param -> float -> t -> unit
    (** end output *)
    val output_end : outparam -> param -> float -> t -> unit
  end
```
and which should be compatible with the defined system.
`output_begin` is called at the beginning of the simulation,
`output` is called at each step (each event) of the simulation,
and `output_end` at the end.

Since we have a simple 3-dimensional system,
we would like to specify a csv output.
To help us, `Sim.Out` defines a `Csv` functor that we can use.
It expects a `CSVSPEC` input reproduced here :
```ocaml
(** Input for Csv *)
module type CSVSPEC =
  sig
    type t
    type param
    (** lines output at the start.
      * Should typically name the columns last *)
    val header : string list list
    (** line output at each step (>= dt_print).
      * Should typically give the column values. *)
    val line : param -> float -> t -> string list
  end
```
with
```ocaml
let sof = string_of_float
let soi = string_of_int
let header = [["t" ; "S" ; "I" ; "R" ; "C"]];;
let line _ t (s, i, r, c) = [sof t ; soi i ; soi r ; soi c];;
```

We then want to integrate that system with the Gillespie method.
The `Sim.Gill` module follows the `ALGO` signature,
so putting it all together, we do
```ocaml
type t = int * int * int * int
type param = {
  beta : float ;  (* infectivity *)
  nu : float ;  (* recovery rate *)
  rho : float ;  (* reporting proba *)
}

let inf_rate par _ (s, i, r, _) =
  let n = s + i + r in
  par.beta *. float i *. float s /. (float n)

let recov_rate par _ (_, i, _, _) =
  par.nu *. float i

let inf_modif par rng _ (s, i, r, c) =
  if Util.rand_bernoulli rng par.rho then  (* report *)
    (s - 1, i + 1, r, c + 1)
  else
    (s - 1, i + 1, r, c)

let recov_modif _ rng _ (s, i, r, c) =
  (s, i - 1, r + 1, c)

module S =
  struct
    type nonrec t = t
    type nonrec param = param

    let evl = [ (inf_rate, inf_modif) ; (recov_rate, recov_modif) ];;
  end

module O = Sim.Out.Csv(struct
  type nonrec t = t
  type nonrec param = param
  let header = [["t" ; "S" ; "I" ; "R" ; "C"]]
  let line _ t (s, i, r, c) =
    let sof = string_of_float in
    let soi = string_of_int in
    [sof t ; soi s ; soi i ; soi r ; soi c]
end)

module I = Sim.Gill.Make (S) (O);;
```
The resulting module `I` gives us a `simulate` function to simulate the system.
Before we can use it however, we need to decide on the stopping condition.
To keep it simple, we will just simulate until a final time `tf`.

Using the `auxf` argument, we could also count the number of `I->R` transitions,
and stop when that number reaches a certain threshold.

With our simpler version, we don't need `auxf`,
and we can simulate the system with
```ocaml
let tf = 100.
let par = { beta = 1. ; nu = 0.5 ; rho = 0.1 }
let auxf ?(auxi=()) _ _ _ = ()
let stop t _ _ = (t >= tf)
let x0 = (999, 1, 0, 0);;
I.simulate par auxf stop x0;;
```
We kept the defaults for the csv output and the algorithm,
which is to output to `stdout` at every event,
and to use a self-initialized random generator.

Changing the method of integration, to use an approximate one,
for example Euler-poisson, is as simple as
```ocaml
module I' = Sim.Euler_poisson.Make (S) (O);;
I'.simulate par auxf stop x0;;
```


We would now like to fit this model to some (simulated) data.


2 - Infer the parameters of the model
-------------------------------------

The library for fitting is ocamlfit, and it is accessible through `Fit`.
```ocaml
utop # #require "ocamlfit"
open Fit;;
```

We will use MCMC and check that we can get back what we put in.
In the codebase, the relevant modules are
[Sir](src/epi/sir.ml), [Sir_mcmc](src/epi/sir_mcmc.ml), and [Test_sir_mcmc](src/test/test_sir_mcmc.ml).

We will assume we only know the total number of observed cases C,
at regular interval in times.

We don't want to have to write then read a Csv file,
so we will change our output module, and use `Sim.Out.Cadlag`,
which simply accumulates values into a `(float * 'a) list` association list.

```ocaml
module O' = Sim.Out.Cadlag (struct
  type nonrec t = t
  type nonrec param = param
  type aux = int
  let aux_of _ _ (_, _, _, c) = c
end)

module I' = Sim.Gill.Make (S) (O');;
```

We will also need the reporting rate in the system
```ocaml
module O'' = Sim.Out.Cadlag (struct
  type nonrec t = t
  type nonrec param = param
  type aux = float
  let aux_of par t x =
    par.rho *. inf_rate par t x
end)

module I'' = Sim.Gill.Make (S) (O'');;
```

The modules in Fit follow the same principle as those in Sim.
They define a `Make` functor, that expects a definition of a system,
and an output specification.
Sadly they each expect close-but-different signatures, so check what you need.

They make no assumption over the way we simulate data,
so ocamlfit does not depend on ocamlsim.

As we often need to both be able to draw values from our distributions,
and compute (log)likelihoods on them, `Dist` defines a (GADT-powered) `dist`
type,
which defines classical distributions and ways to combine them.

The different modules then expect a `dist` in places.


`Mcmc` expects a prior distribution, a proposal distribution,
and a likelihood distribution.
Initial param values are either given in or drawn from the prior,
the subsequent values are drawn from the proposal,
and accepted or not depending on the Hastings ratio and the prior * likelihood
value.
It also accepts a `hyperparam` type, which can parameterize the prior,
proposal, and likelihood.
In our case, it will be
```ocaml
type hyperparam = {
  tf : float ;  (** simulation time *)
  vbeta : [`V of float] ;  (** variance of beta proposals *)
  vnu : [`V of float] ;  (** variance of nu proposals *)
};;
```

We want to infer the value of the parameters.
```ocaml

let target_par = {
  beta = 1. ;
  nu = 0.5 ;
  rho = 0.1 ;
} * (999, 1, 0)
```
We will fix `rho` and the initial state, and infer `beta` and `nu`.

If you look at the code in [Sir_mcmc](src/epi/sir_mcmc.ml),
there is quite a bit of it written to draw uniformly from the simplex
of initial values, but we won't need it as we will keep those constant.

For `beta`, we will take a uniform prior between `0.` and `10.`.
```ocaml
let prior_beta = Dist.Uniform (0., 10.);;
```
For `nu`, we will take a lognormal prior.
```ocaml
let prior_nu = Dist.lognormal (0., `V 1.)
```
For `rho`, and the initial (S,I,R) values, a constant.
```ocaml
let prior_rho = Dist.Constant (target_par.rho)
let prior_simplex =
  let _, sir = target_par in
  Dist.Constant sir;;
```

For the proposals, it will be normal proposals on the previous value,
for `beta` and `nu`, and a constant value (no change) for the rest.
```ocaml
let proposal_beta v beta =
  Dist.Normal (beta, v)
let proposal_nu v nu =
  Dist.Normal (nu, v)
let proposal_rho rho =
  Dist.Constant rho
let proposal_simplex sir0 =
  Dist.Constant sir0
```

It's a bit convoluted to combine them, as we need to go from our record
param type, to the nested pairs created by `Dist.Both`.
We can do that using the `Dist.Map` constructor.

```ocaml
let to_par (beta, (nu, (rho, sir0))) =
  { beta ; nu ; rho } *  sir0

let of_par (par,sir0) =
  (par.beta,
    (par.nu,
      (par.rho,
        sir0
      )
    )
  )

let prior hypar =
  Dist.Map (
    to_par,
    of_par,
    Dist.Both (
      prior_beta,
      Dist.Both (
        prior_nu,
        Dist.Both (
          prior_rho,
          prior_simplex
        )
      )
    )
  )

let proposal hypar (par, sir0) =
  Dist.Map (
    to_par,
    of_par,
    Dist.Both (
      proposal_beta hypar.vbeta par.beta,
      Dist.Both(
        proposal_nu hypar.vnu par.nu,
        Dist.Both(
          proposal_rho par.rho,
          proposal_simplex sir0
        )
      )
    )
  )
```

Now for the difficult part, the likelihood.
We will simulate a deterministic SIR trajectory,
and compute along the way the reporting rate.
We then consider that this rate drives an (inhomogeneous) Poisson process,
and compute the (log)likelihood of the data based on that.
This is done in `comp_loglik` in [Sir_mcmc](src/epi/sir_mcmc.ml#L67),
which is pretty straightforward but I won't detail it here.
```ocaml
let comp_loglik rc cc tf =
  let cmp_rho src t t' =
    let tsrc = Jump.find_right src t in
    let srho = Jump.piecelin_eval tsrc t in
    let tsrc' = Jump.find_right tsrc t' in
    let srho' = Jump.piecelin_eval tsrc' t' in
    let lbd = srho' -. srho in
    (tsrc', lbd)
  in
  let rec f acc src cc =
    match cc with
    | (t, n) :: (t', n') :: tcc when t' <= tf ->
      let src', lbd = cmp_rho src t t' in
      let cc' = (t', n') :: tcc in
      let loglik = Util.logp_poisson lbd (n' - n) in
      f (loglik :: acc) src' cc'
    | (t, n) :: (t', n') :: tcc ->  (* t' > tf *)
      let _, lbd = cmp_rho src t tf in
      let loglik = Util.logp_poisson lbd 0 in
      (loglik :: acc)
    | (t, n) :: [] ->  (* t <= tf *)
      let _, lbd = cmp_rho src t tf in
      let loglik = Util.logp_poisson lbd 0 in
      (loglik :: acc)
    | [] ->
      (* reached only if initial rc is [] *)
      acc
  in 
  let src = Jump.primitive rc in
  f [] src cc
```

`likelihood` then simply contains some code to run the simulation
then compute the likelihood.
Note that we use the `Dist.Base` constructor to create the dist by hand.
Here we give a version that fails on draw, since we won't use it.
It is implemented in [Sir_mcmc](src/epi/sir_mcmc.ml#L399) if you're interested.

```ocaml
let likelihood hypar (par, sir0) =
  let rng = Random.get_state () in
  let opar = { O''.default with Sim.Out.rng = rng } in
  let reporting_rate_c =
    opar.Sim.Out.res <- [] ;
    let s0, i0, r0 = sir0 in
    let x0 = (s0, i0, r0, 0) in
    let auxf ?(auxi=()) = () in
    let p t _ _ = (t >= hypar.tf) in
    ignore begin
      try
        I''.simulate ~opar par auxf p x0
      with Sim.Sig.Simulation_error ->
        raise Fit.Simulation_error
    end ;
    (* need to regulate it ? *)
    opar.Sim.Out.res
  in
  let draw () : int Jump.cadlag =
    failwith "not implemented."
  in
  let loglik cc =
    comp_loglik reporting_rate_c cc hypar.tf
  in Dist.Base (draw, loglik) (* data len 1 *);;
```

Putting it all together, we have
```ocaml
module S =
  struct
    type nonrec hyperparam = hyperparam
    type nonrec param = param * (int * int * int)
    type data = int Jump.cadlag

    let string_of_param (par, (s0, i0, r0)) =
      Printf.sprintf "beta=%f ; nu=%f ; rho=%f ; s0=%f ; i0=%f ; r0=%f"
      par.beta par.nu par.rho s0 i0 r0

    let prior = prior
    let proposal = proposal
    let likelihood = likelihood

  end
```

And for the output, here is a simplified version that simply outputs
the iteration number, number of accepted proposals so far,
drawn parameter values, and acceptance probability.
For a more informative output, see [Sir_mcmc](src/epi/sir_mcmc.ml#L478).

```ocaml
module O =
  struct
    type nonrec hyperparam = hyperparam
    type nonrec param = param * (int * int * int)
    type outparam = out_channel
    type data = int Jump.cadlag

    let default = stdout

    let output_begin opar _ _ _ =
      Printf.fprintf opar "n;k;beta;nu;v\n"

    let output opar _ n k _ (par', _) _ _ _ v =
      Printf.fprintf opar "%i;%i;%f;%f;%f\n" n k par'.beta par'.nu v

    let output_end _ = ()
  end
```

We can finally do
```ocaml
module F = Mcmc.Make (S) (O)
```
And we have our `posterior` function, with type
```ocaml
#show F.posterior
```
We will keep the defaults, so we need a `hyperparam` and some data.

```ocaml
let hypar = {
  tf ;
  vbeta = `V 0.1 ;
  vnu = `V 0.1 ;
}
let opar = { O'.default with rng = Random.get_state () }
let par, (s0, i0, r0) = target_par
let x0 = (s0, i0, r0, 0)
let auxf ?(auxi=()) _ _ _ = ()
let pred t _ _ (t >= tf);;
ignore (I'.simulate ~opar par auxf pred x0);;
let data = opar.Sim.Out.res;;
```

And finally,
```ocaml
F.posterior hypar data;;
```
which should take about a minute to complete.


For systems where the likelihood is computed progressively during simulations,
like this one, `ProgMcmc` gives a way to speed up the process,
by stopping simulations as soon as their likelihood is too low.
An implementation with this method is also present in [Sir_mcmc](src/epi/sir_mcmc.ml).


3 - Simulate a genealogy
------------------------

To be able to generate a genealogy, we need to consider the individuals,
and the individual events as different.
The state of the system needs to take into account each alive individual,
and its genealogy.
The ocamlpop library, packed under the `Pop` module, can help us do that.
It defines the `POP` signature which describes how the state of the system
should change when we add a descendant to an individual, remove an individual,
or add an individual without ancestor.
Individuals are associated with a trait value, a phenotype value,
the part that selection can act on, and a unique identifier.

```ocaml
#require "ocamlpop"
#show Pop.Sig.POP;;
```

`Bdmi` can then be used to define systems where the events are birth,
death, mutation or immigration.
If other types of events are needed -- for example those that affect
more than one individual -- they can be added by hand afterwards,
using the functions provided by the POP P.

Some functors/modules following the POP signature are already provided for use :
- `Pop.Traitable`, in [pop](src/pop/traitable.ml) is an example of a basic `POP`,
  that doesn't keep track of genealogy.
- `Treepopbis`, in [treesim](src/treesim/treepopbis.ml),
  is an example that tracks genealogy,
  using the type defined in `Tree.Genealogy`.
- `Seqpop`, in [seqsim](src/seqsim/seqpop.ml),
  is an example that tracks neutral mutations,
  with some notion of genealogy (sequences of mutation events)

`Treepopbis` and `Seqpop` actually use `Traitable`
and only add additional functionality for what's needed.
So the user can also define their own POP for their use-case.


4 - Simulate neutral sequences
------------------------------

To add sequences into the mix, we will use a slightly enriched `POP`,
defined in `Seq.Pop` as `SEQPOP`.

```ocaml
#require "ocamlseq"
#show Seq.Pop.SEQPOP;;
```

This simply adds functions to handle the seq in the trait,
and to count sequences.

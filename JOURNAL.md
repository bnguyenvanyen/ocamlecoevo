
JOURNAL
=======


This aims to keep some record of the ongoing work and its logic,
with less of a focus on the code and the libraries,
and more on the scientific aims of the work,
with the code seen as a (hopefully reusable and reused) tool to reach
those aims.

The general goal is to work on simulation and inference of ecological
and evolutionary models.

We chose to do that in OCaml, because of the type-safety it offers,
its modularity, as well as its speed.
We'll see how we solve the different problems we encounter
using OCaml's features.

The current goal is to achieve MCMC inference
for different epidemiological models,
with multiple sources of data, observation models, etc.

The strong nonlinearity of the models and codependence of their parameters,
force us to use adaptive MCMC with multi-dimensional proposals.

Since we compare a number of different-but-not-so-much models,
we don't want their implementations to be independent,
so that a change on one model is automatically reflected for all
other models for which it makes sense.
For instance, if we change the default value of some parameter,
or the function for the rate of some event,
all other models that use the same parameter or the same event
should also have the change.
Also (and it is related), we want the different functions to have compatible
types to be used in the different models.
One way to achieve that is to use immutable objects,
which allow the different parameters to combine in a natural way.

We want to be able to simulate both a deterministic and a stochastic
version for each model,
and the stochastic can be with sequence evolution, or without.
The deterministic and stochastic simulation algorithms use
a very different description of the model, and use a different state.
To use them without writing everything twice
(and with more guarantee that we are indeed simulating the same model),
we use OCaml functors.
Each module defines a `Rates` functor that defines rate functions
to be used both in the `Events` module and the `Ode` module.

There is still then a redundant description, that we need to be careful with.

The simulations are done with the `sim` library, which does both ODEs,
in `Sim.Ode` and Markov processes in `Sim.Ctmjp`.

`Sim.Ode` implements both the midpoint method and the Dormand-Prince method.


2018-??-?? Vile square
----------------------

For a difficult-to-sample-from distribution to adapt to
and sample from using MCMC.

We will imagine a square with corners `(0,0)`, `(0,a)`, `(a,0)`, and `(a,a)`.
To each side of the square is associated a normal distribution.
Let `N_v(x, y) = exp (- (x - y) ** 2 / (2 v)) / sqrt (2 pi v)`
the normal distribution with variance `v` and mean `y`.
For the side `(0,0)-(0,a)` for instance,
`d_00a(x) = N_v (x e', 0) if x e \in [0, a]
          = 0             else`
For `(0,0)-(a,0)`,
`d_0a0(x) = N_v (x e, 0) if x e' \in [0, a]
          = 0            else`
For `(0,a)-(a,a)`,
`d_aaa(x) = N_v (x e, a) if x e' \in [0, a]
          = 0            else`
For `(a,0)-(a,a)`,
`d_0aa(x) = N_v (x e', a) if x e \in [0, a]
          = 0             else`

The density to sample from is then
`f = (d_00a + d_0a0 + d_aaa + d_0aa) / 4`
(where the renormalisation is unimportant for MCMC inference).


2018-10-11 A Sim.Sig.SYMBOLIC that makes more sense
---------------------------------------------------

An event changes the state of the system at a certain rate.

One might rescale the rate of the event
(to something bigger or smaller : it's different),
or might combine two events.

We also want to be able to define some transformation on an event,
for example to go from an individual event to a population event.

There's also the special thing we do when we combine two events,
but rescale the rates to still being 1.


So one can
- increase the rate (parameter something / some function > 1)
- lower the rate (parameter something / some function in 0-1)
- add two events (give two events)
- add many events ? (do we need to treat this differently than two ?)
- map an event to another space :
  compute the old rate from the new state,
  and what does the modification become.



About map
---------

Is there a canonical new modification from the previous one that we can then
change to become the modification that we want ?
We basically have (a -> a) and we want to define (b -> b).
If we could define (a -> b) unequivocally it would be easy
(basically if there's a bijection)
but in our typical case that's not the case.
The way we've gone about it until now is to ask for (_ -> b -> a) and (a -> b -> b)
Where there's typically only an injection (for one b, many a).

Maybe the bijective case should ask for (a -> b) and (b -> a) and be called Map,
and the other case be called Submap ?
I don't really know

Can we just change the type a little to get this ?
Or should we not present it with 'state' appearing inside it ?

I think we don't use Bernoulli, but directly the two rescales,
Upscale and Downscale, which would make more sense.
Otherwise do we keep the full type ?
(with possibly state and time appearing or not in the type parameters)


So if everything always goes with a rate, we have six base cases :
- ODE auto
  { rate : 'a -> 'b pos ;
    modif : 'a -> 'a }
- RNG auto
  { rate : 'a -> 'b pos ;
    modif : rng -> 'a -> 'a }
- PRM auto
  { rate : 'a -> 'b pos ;
    modif : point -> 'a -> 'a }
- ODE time
  { rate : time -> 'a -> 'b pos ;
    modif : time -> 'a -> 'a }
- RNG time
  { rate : time -> 'a -> 'b pos ;
    modif : rng -> time -> 'a -> 'a }
- PRM time
  { rate : time -> 'a -> 'b pos ;
    modif : point -> 'a -> 'a }

If we wanted to make the cases a bit simpler (but a bit uglier),
we could say that (auto -> time) is always just putting time
in front of the functions.
To normalize it even more we could say that modif for ODE is `(unit -> 'a -> 'a)`.


Otherwise we should be able to manage with our expressions.


We could still build arguments `_ t` with the full type like

`(('a -> 'b pos) * (rng -> 'a -> 'a)) t`

or with a mutually recursive type def, we can have a record type for the full thing,
and still work with its members where we need to.
Looks like it will be better for the u changes in PRM, that we won't need
to do anymore (and that were weird)

We pull as much as we can into this type,
and leave a type parameter (from a functor as `type expr`) that describes
what we can do with the state, hopefully in a ODE/RNG/PRM agnostic way.

To make it agnostic we need to provide functions/constructs available
for the three cases that could then be used.
otherwise it seems difficult.

But is it even possible ?
rng > point > unit (ode)
in terms of what we can do.

We put constructors that will accept a `('a -> 'b pos) expr`
or a `('b -> 'a -> 'a) expr`
or etc I don't know... Or should we just have one constructor for a `'a expr` ?
In which case we need to be able to handle any case which might be annoying.
(but maybe not).


Or the type could also be more like (auto case)
`('a -> 'b pos * (state -> 'a)) t`
or (time dependent)
`(time -> 'a -> 'b pos * (state -> 'a)) t`

For ODE would `(vec -> 'b pos * vec) t` always be enough ?
We only do `rate * vec` at the end so we never need `?z` ?
(in that we can add it ourselves once we 'compile')

But the problem is that we will want to be able to manipulate
`('a -> 'a) t` values and `('a -> 'b pos) t` values separately.

But still it should be possible to combine them the right way.

Also what do we do when we have an upscaling / downscaling
that depends on more than the global system state ?
(like a chosen indiv for instance)
How does that fit in the picture ?

We can't end up with the right type ?

Or it could work through a kind of map ?
That would take the rate through what is needed.
In which case it would be useful to try to separate out a constant part of the rate.

But that doesn't really tell us how to do it in general,
like we do it in Pop with `P.max` on the right function...
But we can't really make that general ?
Or can we ask for an implementation that would always work ?
As an expr.


So the two problems we need to address if we do this are
- What about 'lower level' rates 


Main problem : rates that depend on states
------------------------------------------

Let's say we start with an event at the level of the indiv.
For example sequence mutation by substitution.
We have a certain rate.

We know want to build from that an event at the level of the population

How do we do that ?

We need to get an upper bound on the rate at the level of the population
that doesn't depend on the indivs (and state).
In Sim I think we let the user do it through (expr) functions.
Basically they tell us how to obtain the new event.

In popsim we can put in a 'Maximize' constructor that will make sense of it
(either register a 'score' function and use it,
find a constant upper bound to the rate, or error out).

But so we need to allow the type that we end up with to contain interior rates / probas
(a rate that goes through a Downscale)

You have the indiv rate.
You can rewrite it as a 
```
Downscale (something', Lift_left (Maximize something))
```

And then we exchange the order somehow to
Upscale (Lift_left (Maximize something), something')

But that's not really what it is because 'something' might be '< 1',
we don't really care,
but we do care that 'something' is '< 1'.


So then one might write, for the event of indiv birth in the pop,
with birth rate depending on the trait
(but can we make it so we can have a t inside an expr ?
 = recursive modules. Not sure if it would work)
```
(* how many lift ? *)
let as_event m = Tuple (Lift_left One_rate, m)

let indiv_birth_rate : (indiv -> pos) = ...
let modif : (indiv -> pop -> pop) = Copy
let indiv_upper_rate : (pop -> pos) = Maximize indiv_birth_rate
let upper_rate : (pop -> pos) = Times (Count, indiv_upper_rate)


Upscale upper_rate (Choose (Downscale (indiv_birth_rate / indiv_upper_rate) (as_event modif)))
```
or something like that.
Does it make sense ? Seems like it.
But we'd like to not compute the rates twice -> We could make all of that go
through memoization ?
Here, we just look up the max indiv_birth_rate twice, which might be ok.



2018-10-25
----------

We need to make the situation around `_float` types more coherent.
Make some opinionated choice around it
(use `float` everywhere or use `_float` etc everywhere (for some lib)).

But some problems with `_float` types :
- Too many abstract type parameters
- The types can be annoying
  (unification, recursive functions, function arguments, ...)

But if we want them somewhere (to have pos / proba), we should rather
have them a lot, to avoid too many conversions
(and the 'is it a `float` or a `_float` here question).


2018-11-02
----------

In Epi, we would like to be able to write the same code
for
- when there are seqs (id indiv) (for Epi.Events_seq) or not
- when we simulate with Gill or with Prm

This is a bit challenging because so far we use different functions
with different types
- `id` / `nonid` argument
- taking `?parent` or not as an argument
- taking time or not as an argument
- calling `P.choose`

This last one is especially troublesome.
`P.choose` can only work with `rng`.
It has a fundamentally different logic with Prm,
as it might return None (and then we drop the event).

Should we change the Gill algorithm to be the same as for Prm ?
Or is it too slow ?
Could we instead change the Prm algorithm to be the same as for Gill ?
No I don't think so.

So the rates would need to be scaled by the length of the dyn array,
which is an implementation detail and should not leak to that extent.
So what to do ?
Can we make the Argrouper have its own rng ?
And fix its seed so that stochasticity does not leak ?
Would that be ok ?
But then there's no randomness left at all ?
Or should we change the implementation ?

Could we just give it *some* integer as input, and then it uses it as
a seed ? I think that works but is probably too expensive.

We could also come back to a Hashtbl, would that work ?
But then we're back to redefining the indices all the time,
which is not great...
Or a set of idents ? Is there a mutable set implementation ?
That might be best. Then we can ask for the n-th indiv.
But it's not O(1)

Or maybe :
- we know there are *n* indivs
- draw a random int between *0* and *n-1*
- if there is an indiv there, we've won
- otherwise try the indiv at *n*
- etc...
Is that uniform ? No, the indiv at *n* is chosen too much.

Or with a BatISet (DIET) ? But becomes less efficient as time goes on
and we poke holes in the set. Also each group would need to have its own idents.

But then making the idents more local would mostly help.
Then knowing about the ident is not really a leak of the implementation details,
and we can allow a function like `val get : 'a group * int -> 'a indiv`
(which could raise `Not_found` or return `'a indiv option`).


If we forget this problem for now and rather think about `id` / `nonid`
.......................................................................

It's also different when `nonid` as there's basically nothing to do :
We can know the trait from the group.

Can this save us ?

```
val add :
  ?parent:'a isid indiv ->
  time ->
  t ->
  'b isid indiv ->
    unit

val remove :
  time ->
  t ->
  'a isid indiv ->
    unit

val choose :
 'a stoch ->
 'b isid group ->
   'c isid indiv
```

And then in the case of Epi I stick that into a functor,
and then have implementations for the case where 
```
type 'a isid = 'a constraint 'a = nonid
```
(`Events_unit`) and
```
type 'a isid = 'a
```
(`Events_seq`).

The problem is that we go backwards in terms of informativity of the signatures,
as some of those `add` functions will actually ignore the given `parent`,
and/or the given `time`, etc...

Is that a problem ?
We can make it a bit more explicit with a base implementation,
and then a `dummy functor` transforming it to have the right signature
(and ignoring the arguments). That might be best.



2018-11-05 -- Multi-level population
-----------------------------------

We'd like to be able to have more levels than just groups in pops.
We'll consider that each higher level regroups elements of the level just
below it.

So basically we could have
`'a trait -> 'a group`, `'a group -> 'a meta_group`, `'a meta_group -> pop`.
So when we have a value at some level, we want to be able to apply some function,
and get the value at the next level, until we've reached the last level,
and we can't apply the function anymore.

How can we type this ?
A GADT where 'a corresponds to the levels from trait (max) to pop (0 = unit).
But it would be more explicit with different names = open variant constructors,
for the different levels.
So maybe

```
type _ level =
  | Pop : pop -> ('a, [`Pop]) level
  | Group : 'a group -> ('a, [`Group] * [`Pop]) level
  | Trait : 'a trait -> ('a, [`Trait] * ([`Group] * [`Pop])) level

val higher_level : ('a, 'b * 'c) level -> ('a, 'c) level
```
And we only ask for
```
sig
  type ('a, 'b) level
  val higher_level : ('a, 'b * 'c) level -> ('a, 'c) level
end
```
(need a type for the top level ? And how to obtain it from a trait ?)

But if the level is like the island the individual is in,
do we store that information in the trait value ?
Yes I guess so.
(like we 'store' what compartment the individuals is in in the trait value in Epi)

So then the question is what do we count.
If we have `(trait_x_species_x_island -> species_x_island -> island)`,
then we can know the number of indivs of one species in one island,
but never the total number of indivs one species.
Could we need to ? I guess so, but most of the time we don't,
so it should be ok.

The first type parameter that is propagated around by trait is a bit annoying...
Maybe a single type parameter and the id / nonid can be carried in the other one ?


Some examples
.............

Traits only :
```
type _ level =
  | Trait : 'a trait -> ('a, unit) level

let to_higher_level = function | . -> _
```

Traits and species and island (and we never count the total) :
```
type _ level =
  | Island : island -> ('a, [`Island]) level
  | Species : 'a species -> ('a, [`Species] * [`Island]) level
  | Trait : 'a trait -> ('a, [`Trait] * ([`Species] * [`Island])) level
```
or possibly, with a different type for trait and species,
(this is less restrictive in what we can write
(product type, so all combinations of trait value and species are a priori possible)
```
type _ level =
  | Island : island ->
      ('a, [`Island]) level
  | Species : 'a species * island ->
      ('a, [`Species] * [`Island]) level
  | Trait : 'a trait * 'a species * island ->
      ('a, [`Trait] * ([`Species] * [`Island])) level
```
or with only one type parameter
```
type _ level =
  | Island : island ->
      [`Island] level
  | Species : 'a species * island ->
      ([`Species of 'a] * [`Island]) level
  | Trait : 'a trait * 'a species * island -> 
      ([`Trait of 'a] * ([`Species of 'a] * [`Island])) level
```

Then how can pop implement this ?
.................................

We want to count the various levels : when you add / remove a trait,
just go through the various levels with higher_level ?
Or is there a type problem ?

Can I write a function that iterates through the levels until it can't ?
Seems difficult...
With exceptions to circumvent the type it would be easy.
Or can we get an equality witness ?
via `val is_not_top_level_exn : 'a level -> ('a, 'b * 'c) eq`

```
let increment_counts z x =
  let rec f :
    type a. a level -> unit =
    fun lv ->
      (* increment the count for level lv *)

      (* then *)
      match is_not_top_level_exn with
      | Eq ->
          (* continue *)
          f (to_higher_level lv)
      | exception ... ->
          (* finish *)
          ()
  in f (level_of_trait x)
```


2018-11-05 -- Better music
--------------------------

Making the music better :
- Try to make birth selective (only sufficiently harmonious notes get played)
  (more annoying to compute ?)
- Or use a Moran model with selective birth-death
  (a more harmonious note tends to replace a less harmonious note)
- Smoothen the volume : not linear in the number of individuals, but saturating
  (hard to do ?)
- Plot the result in trait space (had already started ?)
- Try a spatial model with two locations that don't need to be in harmony
  with each other : do they diverge ?
- (maybe this is the first thing to do ->)
  Compute/Update the spectrum as time goes forward
  and compute harmony against it (convolution).
  What happens if we start with a certain melody,
  can the population follow it ? (equilibrium biomass depending on melody ?)
  We would need the equilibrium to not be based on sustained mutation ?


2018-11-15 -- Prm_approx
------------------------

It would be nice to have modifications of type :
```
type modif = (int -> 'a -> 'a)
```
Where `modif k x` modifies `x` `k` times.

But this is different from the `rate_modif` type we've been using.
And it's annoying to (re)introduce a divergence.
But in any case the modification cannot be expecting a `point`,
because we don't have them.


2018-11-15 -- Csv.simulate_until
--------------------------------

Invert control flow between Csv and integrator ?
Just make them call ~start_out ~out ~end_out,
And in Csv/Stack just a function that will do like
```
val csv_simulate :
  (start_out:_ -> out:_ -> end_out:_ -> 'a) ->
    (header:_ -> line:_ -> ?chan:_ -> ?dt:_ -> 'a)
```
(also order well to preserve erasure in all cases and avoid the annoying warning)
Slight problem : in the type of `start_out` we find types
that must also appear in `'a`, so we need to know what `'a` is,
which depends on the actual simulator ?
Maybe we're saved if all arguments are named
so we can get rid of them at the start...
I think only `x0` needs to get named (or be the next argument),
and the rest is left free.

So then the simulation modules that can, can just reuse
the simulate function from `Loop` but it doesn't need to leak out
to `Csv` and `Stack`.

Looks to me like the best option.


2018-11-22 -- Restructure Epifit 
--------------------------------

Epifit is unwieldy and repetitive.
How can we improve this ?

Things to take into account :
- We might want to supply different (combinations of) sources of data,
- On which we might want to use different likelihood functions.
- We might also want to simulate the model in different ways.
- We might also want to output in different ways
  (parameter values, or also trajectories, but less often ?)

So far, prior and proposal are set up in the individual S(e)ir(s) modules,
with an opt-out possibility at runtime through "infer".
But we might want to do it more like how we set up events in Epi,
so that we might do it only once for all models.

For the simulation method, it is set through the `tech` parameter
in simulate,
which is fairly ugly.
`tech` should maybe be a parameter of the functor so that it's more pervasive.

Data is I think the most problematic.
So far it exists as a record type where some of the data may remain
unused, and combinations are limited.

As a starting point at least, the point of view we want to adopt
is that the different sources of data are independent,
and they might be available or not,
and that every simulation method will return all simulated data.


About having a functor :
Where would the type information come from,
so that every module has a different type.
A richer model_spec ? associated with the parameters that are present ?
corresponding to the necessary tags.
Or is infer enough ?

We could at least avoid repeating 'simulate' and 'header'.
But could we get everything from a list of tags ?


2018-11-24 -- Prm in Epifit
---------------------------

About prm parameter, prior and proposal.
How do we put it in prior when we need evm and max_of to make it work ?

should we move prior and proposal to inside the functor ?

After we can also remove seed from prior, proposal etc,

And use Ctmjp.Prm_approx instead of Ctmjp.Prm.


Problem : need to redraw evm every time theta changes,
even though it stays basically the same.
Can we do it some other way ?
Maybe by computing the worst case ?
But we have parameters with an unbounded prior (beta for instance).
So what to do ?
Do we say "probably it won't be bigger than this much" ?
Or do we authorize graft ?
Each c_slice could have its rng set at creation time,
and then we use that.
Problem a priori : then stochasticity depends on integration again
(the actual trajectory of the system).
But if each rng is only used by its c_slice then it's ok ?
-> if more points are needed it will necessarily be the same ?

Another thing we could do, when we graft a lot, is to replace the u_slices
by bigger and fewer slices.
We leave this to a later improvement

When we graft the prior logp for prm changes.
How can we track that ?

If we propagate the logp info up from our u slices,
then it might work.
We don't need to change it too often.
But we make it more and more tailored at estimation,
which I guess is ok.


2018-11-25 -- Complex likelihoods (Felsenstein etc)
---------------------------------------------------

If we start computing Felsenstein likelihoods and such,
how can we be sort of fast about it ?
A complete overhaul would use JS's Incremental.
But otherwise can we memoize something ? = the part of the computation that's discrete
We will end up many times with the same edges
(but different parameter values + lengths)
Is there something to memoize in there ?


2018-11-25 -- More intricate proposals
--------------------------------------

We would like to have a proposal that combines RAM for most parameters,
and our custom PRM proposal in addition.
But to do that we need more than simply 'Free' or 'Fixed' :
When the parameter is free, the proposal might be '`Custom' or 'something else'.
Something else could be just '`Adaptive',
because we need them to be all coherent together.

Donc peut-être :
```
type 'a t = Free of [`Custom | `Adaptive] | Fixed of 'a
```
?
Mais ça va changer toutes les fonctions avec Free dedans
et il y en a beaucoup, donc c'est chiant.

Une solution moins flexible serait la suivante :
On ne met de toute façon pas prm dans jumps.
(du coup pas dans getsets non plus je crois ?)
Mais si prm est Free alors on construit l'autre proposal comme il faut,
et on le passe aux méthodes adaptatives comme base.

L'autre problème est de savoir si l'algorithme marche quand même
avec un autre proposal qui perturbe.
Mais on peut penser que oui (?).


2018-11-25 -- Fit output
------------------------

The output in fit looks like the previous version for sim.
But we should be able to factorize more by making
it look like the new version in sim.
But it's pretty boring.


2018-11-25 -- Epifit arguments
------------------------------

Maybe we should change the order of arguments especially in Propose.Also
(like pars#beta before th)

On another note, for prm prior, it would be a good example where a more
flexible `Also` would be useful,
for example with ('a -> 'b dist) (or ('a -> 'b) dist ?),
so that the prm we draw might depend on the param value we've drawn.
It should make for an ok guess.
We could also use what's in hypar -> maybe the simplest,
but then we need to be able to write what it should be.

If we change Also, it could basically be a ('a -> 'a) dist

Something where for each tag we say what we need
```
`tag, is_free, getset option, jump option, prior, proposal
```

Or more explicit (`tag` might not need to be in it)
```
type fitparam =
  | Adaptive of { tag ; prior ; getset ; jump }
  | Custom of { tag ; prior ; proposal }
  | Fixed of { tag ; value }
```


Ok this interface doesn't work, it might be worse than the previous one...
Problems :
- Can have several getsets/jumps to one prior
  (example 'sir' 'sir_sr' etc)
- We need to mix different parameter types
  (float, Prm.t, float triplet, etc)
- We need to be able to get 'get' and 'set' back out


2018-11-28 -- Fit x Prm output
------------------------------

How to handle the prm output thing ?
We probably want the main chan to be for par_samples.csv into a folder,
with additional periodic files for prm.
Where would we put this 'prm_convert' though ?
Also it should not be the same n_thin for both ?


2018-12-02 -- Epifit - prm
--------------------------

With '-from', Prm is not drawn (-> problem).
Make '-from' more complex : callable multiple times, like '-fix', '-infer',
and maybe split into '-from-prior', '-from-file', '-from-default' (can we ?),
which could apply to only one parameter or all possible ones
(some never get read though).


2018-12-04 -- Better typing in Epi
----------------------------------

To have different param types for the different models.
Possibly with an interface to the Param module, with a phantom type.

So what we want is to be able to cast from "circular" to "base" for instance,
so base should have more tags than circular.
So we could have
```
type sir = [`Epi | `No_incubation | `Not_circular]
type sirs = [`Epi | `No_incubation]
type seir = [`Epi | `Not_circular]
type seirs = [`Epi]

type 'a model = 'a (* where 'a will be sir, sirs, seir, or seirs *)

type theta = ???
```

Or the other way around, could the parameter of model be the object type
for theta ?
```
type theta_sir
type theta_sirs
type theta_seir
type theta_seirs

type _ model =
  | Sir : theta_sir model
  | Sirs : theta_sirs model
  | Seir : theta_seir model
  | Seirs : theta_seirs model
```
And then we can match on model, and have the right type for theta ?


2018-12-06 -- Towards Epi Prm_approx sim
----------------------------------------

To use Prm_approx we can't have "maybe" events,
so in Events_unit we can't have "maybe_case",
we need to split the event between the event without the case (always),
and the event with the case (always).

So far, cases are observed upon entering the I class.
But it might make more sense to have them upon leaving the I class.
(even if for dengue observation doesn't really cause recovery,
we suspect that observations happen after most of the transmission
has already happened.
Doing this change first could be best because it will make the events simpler
(less different kinds, because it's always I -> R (+ C), there's no E 
that might be involved anymore).
But then we won't be able as much to see if it changes anything.

With Prm_approx we can have errors : for example I got too many recoveries
(and there were no Is left).
Can we salvage the system in that case ?
We can slice thinner, but it's never really safe.
When it's an error of that magnitude, the rate actually became 0,
so it's "pretty safe" to just not do the 'bad' events ?

To mitigate this we could interleave events of different kinds.
Or we could also order the events in a clever (system-dependent) way,
for example all infections, then all 'leave_exposed', then all recoveries,
then all others (because we should never empty S) (or R)
Maybe immunity losses first is better ?
Or we go around the events.

We could also have a more specialized pop implementation where we can just
bump all the counts, then check that the final state is allowed.
(or undo the least number of forbidden events so that it is)

For a specialized implementation, maybe adding a add_k / remove_k function
is possible,
with a list of events, but in Nonid we just look at the length of the list.
It's annoying that we then need to build the lists but well.
But then we need alternative versions for all events which is crappy.
We would need an automatic rewrite when possible...


2018-12-06 -- Epifit stoch sim
------------------------------


Once we have stochastic simulations for Epifit
(with Prm_approx, and hoping it is fast enough),
we have explicit cases in the simulation.
Can we compute a likelihood on this ?
But then it would seem that it is an exact variablen
and either we have the right number of cases in an interval, or we don't.
But most likely that is much too discontinuous (abrupt) to estimate correctly...

We can also have a version of the system without cases,
and keep doing the same thing (or have the cases but ignore them).

no cases actually sounds best
this makes it easier to change the system,
but how do we type it correctly ?
(so that stoch = 'a)
Can we just pass `no_case` as `maybe_case` ?

So it does run, but it's too slow !
With a lot of time spent on compare, and in set operations.
-> so on redraw ?


2018-12-11
----------

Host vitality : need to fix in epifit !!!


2018-12-12
----------

Prm inference : we want prm to be fixed until we actually do prm inference.
How can we do that ?
We set prm to Fixed at the start in infer,
then change it to free after.


2018-12-12
----------

So changing the k-th color slice of the j-th time slice has probability
1 / ncolors * tf / dt
(so in log, - log ncolor + log tf - log dt)
Then for the new points, we have P(new_points) (or logP(new_points))

So logp' - logp is 
logP(new_points) - logP(points)

And logq - logq' is logP(points) - logP(new_points)
(logq is par' to par and logq' is par to par')

So I think it just cancels out.
Which is weird because then nothing tells us wether we're a priori "extreme".
But it's the same as if we draw from the prior, it just cancels out.

Problem :
Grafting can change logp without being reflected in the stored
logprior value, and then make proposal be rejected.

How can we fix this ?


2018-12-20
----------

Can I remove the Csv and Cadlag output functions,
and just keep the convert ?

They're redundant and they make stuff more complex

(yes it's better now)


2019-01-06
----------

How to load prm from file ? -->
Can we use the grid file ?
For now let's just keep using tf and h,
and then we can check we do get the same grid in the end at least.


2019-01-24
----------

Reducing the dimension of the initial condition for inference :
use the equilibrium value for e0,
so that it is uniquely determined by theta, s0, i0, r0,
instead of having a conditional distribution on it.

For SEIR (which we don't really use but well) :

dS = - D S - beta S / N I + B N
dE = - D E + beta S / N I - sigma E
dI = - D I + sigma E - nu I
dR = - D R + nu I

At equilibrium

(D + nu) I = sigma E

So that E = (D + nu) / sigma I

So now we replace the prior on S, I, R by a prior on S, I + E, R
And from I + E we can determine both I and E from the equilibrium equation.

(E + I) = (D + nu) / sigma I + I
(E + I) = (D + nu + sigma) / sigma I

I = sigma (E + I) / (D + nu + sigma)

For SEIRS :

dS = - D S - beta S / N (I + eta) + B N + gamma R
dE = - D E + beta S / N (I + eta) - sigma E
dI = - D I + sigma E - nu I
dR = - D R + nu I - gamma R

So it's the same ! yay


2019-01-26 -- Problem with seirs + stochastic + neff data for i0
----------------------------------------------------------------

The estimate seems to go to 0 in this case. Why ?
Is there something different in the simulated data that goes through
in this case, that's used only for that ?
(s0 inference doesn't seem crazy, but maybe because it's not very sensitive,
and it doesn't change very much ?)


2019-01-26 -- coalescent simulation
-----------------------------------

We don't ask for samples :
they should be a part of the prm.

For that to be manageable, we need a way to
- rand_draw / create color by color
- redraw only for some colors

Since our PRM implementation only has k slices
(but does not have a second integer),
what we plan is to have all the other integers for the other slices


2019-01-29 -- Adaptive PRM proposals
------------------------------------

Taking a lot of inspiration from RAM (Matti Vihola),
we consider that each square of the PRM corresponds to a dimension,
and that a multi-dimensional normal variable will give us either :
- the number of points to add / remove for that square
- the probability of Poisson-redrawing for that square

The advantage of the first possibility is that it makes more sense.
The disadvantage is that we don't draw into the Poisson process anymore,
so we need to compute the likelihood exactly.

The advantage of the second possibility is that we still draw from Poisson.
The disadvantage is that it doesn't make much sense :
we'd like the proposal to be able to express that
if we add points to some square, we should remove points from some other square,
but it will only be able to say that if we redraw for one square we should redraw for another.


2019-01-30 -- Spatial Poisson process likelihood
------------------------------------------------

Well I'm confused...
Any exact realisation of the process has the same likelihood
for a volume for which we already know the number of points,
since they're just uniformly distributed.
Which seems weird ?


2019-02-16 -- The direction of time
-----------------------------------

We assume things about the direction of time in a number of places,
and now problems appear :
- in Tree.Timed, deeper nodes should come later
- in sim, time goes forward in time
- in coal, we simulate a tree backwards in time,
  which is where the problems spring up.


Maybe the solution would be to allow Prm and Prm_approx
to go backwards in time.
This would fit with how we want things to look like.

It's not hard to reverse the order of points,
but it means we should also make it possible to reverse the order
of t_slices.

How could we do that though ?

The points should say how they want to be compared.
val compare_time
val compare_color

So that the order can be reversed.

But then we need to change Fm...

Too complex to do all this in a coherent way.

So instead we reverse the times when going from simulations to data.



2019-02-19 -- Other Ctmjp.Prm algorithm
---------------------------------------

For sequence estimation, I'd like to estimate a new exact Ctmjp.Prm
simulation algorithm.
The idea is to use the prm time for each color as internal time,
that we compute at the relevant rate.

When the number of events is small, this should work better
(the tree will be automatically rescaled when the rates change)

It can't work with exactly the same prm implementation however.
each color should correspond to one rate.
Do I still have the right to forget the points
when they're eventually considered however ?

I'm pretty sure that yes, so that still works.

We don't have time slices.
We keep a map of color -> (s, points)
And then from the time t at which we are, we want to find the next
point.


For coalescence, we have the choice between separating the k/i
as different colors, or keeping them together.
For the k/i possibility, the advantage is that changes in the topology
will propagate more smoothly along the tree :
For instance if one coalescence more happens early in the tree,
with all k/i together, a coalescence later can only stay at the same time,
or disappear.
If they are separated (different internal times),
then the time of the next coalescence will be reduced (-> smoother).

Of course the problem is that if we keep a different slice of points
for every possible pair, that's a LOT of slices
(1000000 for 1000 sequences).


2019-04-23 -- Weird PRM MCMC
----------------------------

The problem is :
Even with 'none.none', the (custom) MCMC gives a prm density line plot
around 0.5 density, with seasonal peaks at 1 or 2.
Moreover, the corresponding trajectories don't seem very 'prior'
(not around the deterministic trajectory)

Both features are weird and worrying.

Possible explanations :
- the plot is wrong (not actually the right density average ?)
- the prior log_density we use on discrete measures is wrong.
- something weird happens with the simulations for the prms
- the events might be wrong


How can we test :
- plot more things (some of it should be right)
- get a lot of random prm samples not by MCMC, without any simulation,
and plot
- get a lot of random prm samples not by MCMC, with simulations,
and plot
- get a lot of random prm samples by MCMC ('none.none'), without any simulation,
and plot


Conclusions :
- not-MCMC samples without and with simulations mostly fit with
what we expect (average 1.), with more variance around peaks
(does that make sense ?)

- lot of 'none.none' with no simulation :
is it enough to remove 'output-traj' ?

So it looks like the problem comes from the proposal.
Need to confirm with longer prior run (50_000 ? 100_000 ? check acceptance ratio)

Then if we find a (the ?) problem, we can move on to checking the simulations,
and the adaptive proposal.

For the adaptive proposal, we don't seem to get good samples.
Possible explanations :
- has not converged
- qq' is wrong

Diagnose :
- run longer on something easier (on 'none.none' once it's fixed ?)


2019-05-02 -- Better recipe
---------------------------

I'd like to be able to parameterise the recipe more :
no necessarily infer everything the same way at evere round,
mix up proposals, etc.
For example, some time with custom prm proposal, but prior proposals
for other parameters might be useful.

For more flexible version on the command line,
here is the interface we would like :
one could specify general infer option,
and then change it under a '--step' argument.
Or we just put a '--step=NAME,TECH,NITER' argument,
and then in 


2019-05-21 -- Coal internal
---------------------------

Problème (classique) de contraintes.
Soit je peux mettre les contraintes sur le type abstrait,
Ou les rendre existentielles ?


2019-06-13 -- need to check 'rand_poisson'
------------------------------------------

Seeing as we don't get the same result in simfit/src/test.ml
in the prior case and with the pdf (in python)

Normally it has been fixed by using log1p,
but the point density is still not centered around 1 in PRM MCMC.


2019-07-06 -- revisit the early stopping MCMC ?
-------------------------------------------------

Relevant : Korattikara 2014

I think it can only become interesting if we ask for more aggressive proposals.
For example:
- with the atan rule,
- and/or with Cauchy samples,
- and/or with lower acceptance probability

With an acceptance proba around 0.25 we would still be able to win
3/4 of the total cost I guess ?

So the idea is that at the time of computing the new likelihood,
we already know what the target is, and as soon as that target is passed,
we can stop.

Note that on rejection we don't know what the new likelihood value is,
so we can't do the adaptation in the same way !

Can we have a different, less precise rule, based on the inequality ?
Since we know that the acceptance proba is below some value.
We could take a guess though ? from how much of the whole computation was done.
I fear that in the atan case that might not be good enough.

But we can keep it for the non-adaptive phase, if it is not too long.

Since the likelihood computation might take many different shapes,
and we don't want to make too many assumptions about it,
I guess it would be better to leave the control to the likelihood computation,
and simply pass it the target value.
Then the computation must either `Accept and give the likelihood value,
or `Reject. Do we ask for some guess about the likelihood value then ?

But if we have different computations to do, then a guess would be hard to get.

So I guess no guesswork,
or the choice is left to the user : `Reject of 'a option

It still means we need to run the likelihood computation at the same time
as the simulation, which is a pretty big change
(but should work through changing the 'output').


2019-07-06 -- Position aware proposal ?
---------------------------------------

Don't simply move along the right direction,
but move (mostly) *in* the right direction.
That would be more like (quasi) HMC or (quasi) MALA.

So from gradient approximations of the posterior surface.
This is a more differetn problem.
But can we do something slightly simpler ? Less opinionated.

Or would it be possible to implement those reasonably cheaply for our case ?
Read the SGLD paper at least (Welling and Teh, 2011).

Also look at Zig-Zag.

In the centered DA case, it might be doable,
but there's a problem of change of dimensions (add/remove events).


2019-07-15 -- Cmdliner for Epi
------------------------------

The current Arg code is ugly.
It looks difficult to move to Cmdliner though, as there's quite a lot of code
in Arg style to be changed still...



2019-07-15 -- More general 'from_log'
-------------------------------------

The executable 'epi/src/sim_sirs_from_log.ml' is the only thing that
depends on 'epi/lib/map_csv.ml', and contains specific not pretty code
that otherwise I've managed to factor into 'epi/lib/cli_unit.ml' and related.

So it would be good to do the same thing for it too.


2019-08-26 -- Ctmjp.Prm vs Ctmjp.Prm_approx
-------------------------------------------

Even with small 'h' the two trajectories are very different.
Even more problematic is that there seems to be a bias :
S doesn't vary the same way on average when gamma = 0
(equilibrium not even at the same point ?)
It also doesn't look like there's a notable convergence as h goes down.
So it would seem that there is a bug somewhere...
Can we count events to see if there is somewhere in particular
that the bias happens ?
I can put a dirty debug in the sim code to count them I believe
(a color -> count map)


2019-09-09 -- Auto blocking
---------------------------

In Turek, De Valpine et al. 2015, it is shown that it is possible
to automatically form block of parameters to update jointly,
and greatly increase efficiency that way.
I should look at a way to implement a variant of that in my case,
as I am starting to have a lot of latent variables.

To do this, a reorganization of the fit code will be needed,
which I'm not super eager to do.


2019-11-07 -- Simplify Fit.Param
--------------------------------

I have a number of problems with Fit.Param :
- too many partially redundant constructors -> unyieldy
- forced to pass 'Fixed' value at construction time
- forced to pass a starting value to draw from prior
- that's also a reason why the 'dep' constructors are annoying

Instead :
- get / set can come in later as needed with a map
- fixed can be left without a value (to be passed at use site)
- ideally I would like to not decide yet between Fixed / Custom / Adaptive,
but that might be too hard
- the 'dep' cases can come as functions

Things to be careful of :
- for one param, I might not know how to draw custom or adaptive
- the jumps might not agree on get / set with the prior / proposal
  do I have an example ?


2019-11-09 -- Make coal simulations faster
------------------------------------------

coal simulations scale in the order of O(n_seqs ^ 3) :
- the total number of events during a simulation is of order O(n_seqs)
- for each event, we need to look a each possible event,
  compute all the rates and find the minimum,
  and there are of the order O(n_seqs ^ 2) events

which is too much. I can try to bring it to O(n_seqs ^ 2) by using
the following algorithm, more similar to the one in Coal.Generic,
than to the one in Coal.Simple :
At every event that happens, for every event whose rate has changed,
the user is responsible for updating the predicted real time at which
the event will happen, if the coalescent population would not change state again.
For each event, there are of the order of O(n) events affected,
so this should be O(n).
Now, to choose the next event that happens,
just take the one with the smallest predicted time.
If those are stored in a map, I think this is O(log(n ^ 2))
So actually, I think this is O(n ^ 2 log(n ^ 2)),
which is still not great but well.

A further optimization, but I don't know how general it can be,
is to only compute a lower bound on the time at which the event would happen,
and then to update that time only once it is reached,
instead of every time it changes due to some other event.
This requires two things :
- being able to easily compute a tight lower bound on the time
- being able to 'retrospectively' compute that bound :
  when we recompute the time, the event might have changed rate
  several times, and we must still be able to compute the new bound
  correctly.

For now, I will try to implement the simpler optimization.
- The evm for Ctmjp.Prm_internal is indexed by projected event times
- The user updates it as he wants on each event (like now)
- in Ctmjp.Prm_internal, we just take the smallest time to get
  the next event and the next time.

There is a difficulty. We need to be able to both find the min time easily,
and to find the events to update easily.
So I think I need two linked datastructures

The events could be in a hashtbl.
The keys don't need to be the colors, I can choose whatever,
and I must simply gather the points at creation time.
I think that reduces the amount of work.
This needs to also point to the projected times, so that I can
easily update them, and then I need to have the sorted list of times,
pointing to the keys of the hashtbl,
so that I can keep it sorted, and find the next event quickly.
On merge, when some events become all the same event,
I keep only the first that will happen, and I can remove all the others.
That does mean I need to be able to retrieve a precise time from the list of times,
to update it, or remove it.
With floats that's not super easy. But if it's the exact same float
as before (and I don't recompute anything),
then it should still work.

So it goes like this :
I have some data structure with colors mapping to points,
with their initial internal times.
I then have a Hashtbl, where I decide on int keys as needed,
associated to a color, an event, its points, and the projected occurrence time.
Lastly I have a map of projected times, mapping to the hashtbl keys.
I update those times as needed,
and at each iteration, I get the min to proceed to the next event.

I would like the user to not need to know about the details of the datastructure.
We don't really need to update the points anymore.
I can probably cheat a little and avoid to update colors explicitly.

I don't really need explicit rates (as they are not enough).
I will need a function to compute the projected time of the event,
to which I can also pass the previous context,
in case that makes it faster.
And that should be enough.

Then to handle the addition and removal of events,
and connections between events.
The user should not know about hashtbl keys,
but the user can know about colors.
Maybe I can build up a map mapping colors to hashtbl keys.
But I don't really want to go through it every time...
Maybe if I give to the user a way to go through it himself,
and memoize th calls a bit further down the line,
that could work out ?
The main advantage would be that instead of updating colors,
when I merge lineages for instance,
I could just have the different colors map to the same id.
But actually I want to keep only once, though.
So the 'other colors' should not occur anymore...
I don't know :(


2019-11-21 -- Util.rand_rng
---------------------------

Turns out that Util.rand_rng is very slow and gets called a lot
when drawing from a prm prior.
(weirdly invisible in prior seq case when it looks like it should also matter)
They should be ways to reuse rng state more to avoid this,
by replacing some uses with Random.State.copy

I still need to preserve stochasticity, but it should be ok to reuse
from iteration to iteration ? (maybe ?)


2019-12-12 -- Unify simple and sequential/seqram
------------------------------------------------

I use the prior and proposal differently in seq mode for nu :
The prior should give me a 'nu list, whereas the proposal should act
on a single 'nu. I'd like to reuse the same Fit.Param.t for both cases.
Would that be possible ?
An idea is to manipulate settings by calling with both Simple and Sequential,
to get the proposal with Simple,
and the prior with Sequential.
The problem is for the "simple in sequential" case,
we'd need the right t0, tf etc (not the gobal ones),
if we every called the prior.
Or I could also add an additional parameter to the function call ?


2020-01-18 -- Seqram fails
--------------------------

The solution explodes with Sde : bug or instability ?
-> Test with smaller h : better or worse ?

With Prm, the proposal ends up failing because "remove too many points",
which means it fails every iteration.
Need a way to recover :
- Do I have the right to insist while keeping the same posterior ?
  (I think not)
- Do I have the right to do nothing or keep 0 points,
  when I try to remove too much ?
  Yes, but then I need to correct the Poisson part of the log_pd_ratio,
  and I'm not sure how
- Do the proposal only with a certain probability (like 0.1)
  Then how do I need to correct the log_pd_ratio ?
  When I look at two nus, they might :
  - be different, in which only the jump case remains, and the factors
    cancel out
  - be the same, in which case both are possible,
    and the computation is totally symmetrical,
    so everything cancels out I think
  So I think nothing changes for the log_pd_ratio.
  So this is the simplest thing to try.


2020-01-21 -- Seqram adapt
--------------------------

The adaptations seem to not be correct for Seqram Prm in particular.
A first factor is that log_pd_ratio is missing (so it can't be correct).
But it looks more serious than that, as the chol matrix becomes
much too big, especially for state.

It would be good to have a way to adapt only the nu part stepwise,
and the theta + state part once globally per iteration.

Is there such a thing as a partial Cholesky update ?
The Cholesky update corresponds to :


2020-01-21 -- Sde stability
---------------------------

We can hit stability problems with Sde that are not detected,
and which crash the program (on an assert).

How should I detect and catch instability ?
I should not recover from the assert failure,
I should check that the assumptions of the function are verified.
I can at least check that there are no nans.


2020-02-11 -- Continue
----------------------

Coming back to this after a few months, it's difficult to really
understand where I was.

As I understand, there is already a new version for
Evm_internal and Prm_internal,
that tracks "remaining internal fuel" and "projected occurrence time",
and makes updates to "affected" events.

Additionnally, I was in the process of converting Coal.Generic over
to that new system.
There is already stuff for mutation and merge about remaining internal fuel
and projected occurrence time,
but not the rest.

The remaining work is to add affected events,
add stuff for sampling events, and then form the initial evm.

-->
The remaining work is to fix the plumbing for the exposed simulation functions.


2020-02-12 -- Forward / Backward time
-------------------------------------

Coal.Generic has nearly been converted, but some problems remain.
One is time. Where should time be "forward", where should it be backward,
and where should it get reversed ?

Most places should use only one referential.
Places that use both and convert between them should maybe use
two different types, so that we can be more safe ?

For Coal.Generic, the simplest is that time flows backward everywhere.
So t0 <= tf and simulations start at t0,
and prim functions get evaluated directly at t.
Then Coal.Pop should also be in only backward time.

However, that is not possible, as time needs to increase along branches
of the trees in the coalescent population.
So time must be reversed in modif_state functions.

So we provide a Backward module, that takes everything in the forward time
referential,
simulates backward,
then returns results in the forward time referential.
(as before)


2020-02-13 -- epicoalfit
------------------------

epicoalfit must still be fixed (fix rates etc).


2020-02-27 -- After rebase
--------------------------

Rebased faster_coal_sim on top of devel-john.
And then fixed stuff so that everything compiles.
However, that is not the end of it, as the correction
for unobserved merges in epicoalfit must be reintroduced.

A long list of TODOs on the code :

- test infer-adapt stoch a bit more for epifit with prm_approx.
If only the prm is inferred, and the adaptation is stopped,
is the discrepancy still there for the prior ?
Try to write the thing done to see if I can find the right ratio.
Is Prm(lambda) dominated by Prm(1) ?

- test infer-adapt prm for coalfit a bit more. Where is it going wrong ?
Why does the adaptation make things progressively worse ?
Can I plot an animation of the (Cholesky factor of the) covariance matrix,
to get an idea of what is going on ?

- for coalfit, does it make sense that often the move does not 
change the likelihood ? 
That means only 'unimportant' merge times are affected,
those that come after the actual merge.
There are many of those, so it makes sense in the 'custom' case.


2020-05-22 -- Versatile proposals combination
---------------------------------------------

The change to proposals so that the sample and the move don't have
the same type is not finished.
The next problem is with Also, which I use pervasively.
The (main) problem is the following :
how do I get the move from the full thing ?

Do I need to totally change and not use Also anymore,
or can I change it so that it's good enough ?

The alternative is to not have a base type that I build on.
That would mean going back to tuples for instance.
Or at some point I tried having applications I think,
which would mean being able to get a new value from all previous values.
(so a "theta" function which would build it from all parameter values).
The problem is that this would not let me get back the individual
sample or move values either.

If I want to keep nearly the same mechanism,
is the object system permissive enough that it would let me build
arbitrary theta_move types that would have the same structure as theta,
but hold the move ?

It would mean Also can't be ('a, 'b) t -> ('a, 'b) t.
It needs to be a new type, ('a, 'c) t ?
which in practice would still be a fairly free object type.

So when I draw :
- draw from base : given a 'a, get a 'b
- get back a 'a with move_to_sample (this is suspicious)
- get a 'c with 'get_sample'
- given the 'c, draw a 'd with 'sub'
- put the 'd into the 'b with 'set' (this is suspicious)
- return the 'b

And when I compute the ratio :
- given a 'a and 'b
- get a 'c with 'get_sample' and a 'd with 'get_move'
- compute the 'base' ratio with the 'a and 'b,
  and the 'sub' ratio with the 'c and 'd
- return the sum

The suspicion with move_to_sample is :
- can I write a generic ('b -> 'a) for Also base,
without knowing the sub moves and their specific ('d -> 'c) ?
Can I do without somehow ?

The suspicion with set is sort of the same,
are object types generic enough so that I can write the function ?

Otherwise it means I need to always be at a new type for each new sub move
and then a combination of chain and map would be enough.

Thinking along those lines, I could also have a kind hybrid as a more
general symmetric Also :
I combine two proposals ('fst, 'fst_mv) and ('snd, 'snd_mv),
to get a new proposal ('smpl, 'mv).

Do we have a problem for moves that touch the same thing ?
Will draw and log_pd_ratio still be correct ?
For example let us say that I have several proposals that affect a tree.
No it would not necessarily work.
But then, if I do that, it messes up the ratio anyway, no ?
I have no way to compute the ratio correctly.
So we forbid that.

The next problem is that Choice must have the same move return type
for all proposals, when instead we would need it to specialize
to the specific one we need to compute the ratio.
Is there any way we could have that ?
If I take in a typed list instead ?

A much simpler possibility is to keep Choice as is,
and make Metropolis_hastings into a (Base) proposal.
Then one would use Choice over that,
and 'Mcmc.proposal' would be turned into a simple loop function,
that just keeps calling the proposal,
with no guarantee by itself about the distribution sampled
by this Markov chain.
So then just add a Markov_chain module or something ?


2020-06-08 -- "Dumb Independence Sampler"
-----------------------------------------

Is it possible to have a very dumb adaptive independence sampler
based on accumulating uniform/gaussian balls ?

It could go like that :
- We keep a tree of uniform distributions to draw from on the leaves,
  each with a weight.
  At each node of the tree, you've got the sum of all the weights under it.

- To draw from the proposal, draw a U[0,1] variable u
  At each node of the tree, decide if you go left or right
  depending on the two sub weights, and recursively until you reach
  a distribution.
  Then draw from the distribution

- To adapt the proposal, we add a new distribution centered around
  the last (accepted?) draw x, with volume v, and weight v * d(x),
  where d(x) is the posterior density in x.
  To add it in the tree, just descend the branches and add the weight
  to the nodes, descending on the side of the smaller weight.

- To help at the start, draw from `p * prior + (1 - p) * proposal`,
  and progressively decrease `p`.

- When the tree gets too big, drawing and updating will get slow,
  so we need a way to compact the tree / simplify the proposal.
  This can be a rare operation (like 1e-4), so it can be costly.
  The goal is to remove redundant information, so we want to combine
  distributions that are similar.

 
The goal is for the proposal to approximate the posterior.
Is that the case ? Do I draw from a small volume `v` around `x`
with probability `v * d(x)` ?
No, so the idea is too dumb...


2020-06-09 -- Better SPR proposals
----------------------------------

With the "divergence SPR", the inference with JC69 and 32 sequences
seems to work ok, but it's not super fast.

An idea to make the SPR moves better is to choose prune points
and graft points more carefully, thanks to the probabilities
computed at the nodes.

The idea is that a good prune point is a point where subtrees
disagree with each other.
At each node i, we have the matrix of probabilities for the left subtree,
`P^i_l`, and for the right subtree, `P^i_r`
We can compute the ssqr_diff between them, `d^i`,
and then pick node `j` with probability `d^j / (sum_i d^i)`

Then the idea is similar for the graft point.
A good graft point is a point where the grafted subtree would agree
well with the graft point. I can't compute at every point along the tree,
but I can compute at every node, and use that as an indication
for points that are nearby.

So one could pick a graft point uniformly under nodes `j`
with probability `e^(- d^j) / sum_i e^(- d^i)`.
It could be a good idea to try to control this a bit better,
depending on the distance in time between the the subtree and the node,
to adjust the distance, etc.


Then we need to make the move reversible.
The difficulty is that for a move from tree `T` to tree `T'`,
there are three different trees, with three different probability
flows, `T`, `T'`, and the pruned tree `T_p` between them.
In particular I don't really want to recompute the probabilities
for the pruned tree `T_p`, as I won't need them otherwise.
So if everything depending on `T_p` could be made symmetric,
that would be nice.
That makes sense because `T_p` is the same in both directions.
So on `T_p`, the probability of moving from A to B must be the same
as that of moving from B to A.
The simple way to obtain that is to use something that looks like
z_A * z_B / (sum z * sum z) ?
But then do I need the z do be the same in both directions ?
(in which case I need to compute the probabilities for T_p)
Yes I think so.

Or can I avoid the renormalizations somehow ?
No I don't think so.

So I would need to compute the probabilities for the three trees.
To not pay too much, I need incremental computations.

Probably the first step is to plot these ssqr_diff on the tree,
see whether there are nodes that stand out, and whether they
actually look like good prune nodes.


2020-06-09 -- Tree rescaling
----------------------------

Currently, as part of the RAM proposal for parameters, I also
rescale the tree branch lengths.
On the one hand, it makes sense : if I change lambda or mu,
I should adapt the tree to reflect that.

Can I check that this improves mixing compared with doing them
in separate proposals ?


2020-06-09 -- Incremental sequence likelihood
---------------------------------------------

I want to avoid recomputing the subtree probabilities when not needed.
The goal is to try to implement that with Current_incr
(because it's small).

The probabilities at one subtree is a function of the probabilities
of both children, and of who are the children.
When the children change, or the children probabilities change,
then the subtree probabilities should be recomputed.
It feels like we want to avoid direct checks on the probabilities,
as that feels fragile.
Can the check on the subtrees be enough ?

Could it work with something like that ?
`let proba_subtree left_probas right_probas =`
 

2020-06-09 -- Phylodynamics
---------------------------

All the previous items are optimizations.
The most urgent is to obtain something that even-only-sort-of works
in the phylodynamics setting, with the SIR and SEIRS model.

The first (and main) step is to implement the "prior" probabilities
for these models.
I can put the basis for it in epi Process I think.

The remaining work is mostly boring glue work (probably with some difficulties).

Then I can already do the first tests on synthetic SIR data,
and compare the data settings.

Then test on the actual data.

And while doing that I can optimize further.


2020-07-23 -- Tree proposals
----------------------------

So one month and a half later, there has not been much progress...

I've got inference under Kingman's coalescent,
and under the Volz SIR coalescent.
The problem is that my proposals are apparently not valid,
since they don't all give the same prior and posterior distributions
on simple datasets.
But this could also be due to non-convergence...
For the prior I also need to compare to the actual prior
(which I can do).

I corrected and reversed some ratios where needed,
but this doesn't fix it.

I added directly drawing from the prior for comparison,
the problem is that it's a bit slow.
Even for the example without sequences, the result is weird.
We get unif and div on one side, but not exactly identical,
then prior draw and guided draw on the other,
with similar peaks, but the guided proposal is way too narrow
and asymmetrical.
Why ? It should mean that three out of four of the proposals are wrong,
even either the direct draw or the uniform SPR moves...
Should uniform SPR really have a null ratio ?
There is a problem in that I allow the first binode to be pruned,
but then I can only graft under the first (remaining) binode.
This is weird and biases the moves.
I should either forbid the first binode to be pruned, or graft up to the root.
Both decisions seem valid, but I should be consistent between proposals:
- the prior proposal can move the first binode (and the root)
- the uniform proposal can move the first binode,
  but only to under the second (first remaining) binode.
  (easy to change)
- the divergence proposal can move the first binode,
  but only to under the second (first remaining) binode.
  To allow it to move it up to the root, it would mean valid_up
  would also need to take d into account.
- the guided proposal can pick the first binode,
  and move it up to the root.

The specific thing about the first binode is that a prune there
cannot change the topology of the tree, but only branch lengths,
which can also changed otherwise.

I also halve the starting density for Div Graft moves,
which should not change anything but seems more correct...

For Div Graft moves it's not clear to me that I am doing the right thing
with t_max. Is t_max actually what I want,
in particular when I reverse the move ?


2020-07-30 -- Tree proposals bis
--------------------------------

After a number of fixes,
the tree proposals still don't yield the same prior distribution.
Most perplexingly, uniform and divergence are still not equivalent,
when I feel they should be.

Also, slide_node seems to collapse coalescence events together,
which is bad...

For guided, we need to be able to graft to over the leaves,
which means changing valid_graft_labels.

But there are still necessarily other problems remaining :(
- for the prior draw, it might be wrong if lambda doesn't correspond
  between draw and the log density
- for the uniform, I don't really see how it could be wrong now

Ok, after fixing the root thing, it's all better !
The only remaining problem is with guided.
The ratio looks ok for now, since it's always 0.
We then expect the actual draw to be exactly equivalent to the uniform draw.
But it seems like it's not. Why ?
It was because of qs_prune = 0.

Now it's fixed and it looks like everything is sort of ok.
The distributions are still not identical,
but it might be due to differences in convergence.


2020-08-03 -- Next items
------------------------

- update probas correctly for SIR
- look at long SIR inference results with guided
- retry divSPR under RAM
- do less work for probas
- implement a generic phylo functor
- implement GTR+Gamma+I (or rather, X+Gamma+I ?)
- look at SIRS on the vietnamese data ?
  I need to look at real data to calibrate stuff a little.
  Also, how slow is it ?
- PANIC preliminary work on sequences
- PANIC model and inference, with SEIRS and serology.
  What do we see ?


2020-08-06 -- Improve util Arg again
------------------------------------

util Arg has poor flexibility and composability.
I need a more generic config and capture mechanism, to do what I want
in phylo Generic and freely combine terms from different modules.


2020-08-07 -- Improve Phylo
---------------------------

Quite a lot of improvement for one day !


2020-08-08 -- Implement GTR+Gamma+Inv
-------------------------------------

For GTR, it's the same dumb work as for JC69.

For Gamma + Inv, first I need to have an implementation of gamma quantiles.
(For that I need internet).

I also need to generalize the seqsim Tree interface for rate variation.
I think I should add types to represent that.
- For a nucleotide distribution over a single site, like for eq_probas.
type site_distribution =
  | Simple / Certain / Dirac / ... of Lac.vec
  | Distributed / Rate_variation / ... of (Lac.vec * _ anyproba) list
    where the probas should sum to 1

I think for eq_probas it will never vary with rate_variation,
since it does not depend on mu, so maybe we don't need that type.

Then we need the same for transition_probas,
type transition_probas =
  | Simple of Lac.mat
  | ... of (Lac.mat * _ anyproba) list
    (the probas should sum to 1)

And then for the full sequence,
type sequence_distribution =
  | Simple of Lac.mat
  | ... of (Lac.mat * _ anyproba) list

So same as before but it's a bigger matrix.

For the invariant class there should be a way to optimize though,
but maybe not worth it since there are stil the four other classes
to compute anyway.


2020-08-19 -- +Gamma+Inv implementation
---------------------------------------

Introduced a settings GADT in Seqsim.Tree, which does do most of the work
as I would like.
For computing the log-likelihood from the root probabilities,
what should I now do ?
Before, in the homogeneous case, I was doing :
sum (log (map (max -1000) (eq . p)))
Now I have a distribution over probability matrices, as a list.

Ok done.


2020-08-25 -- Buggy likelihood
------------------------------

Comparing a phylo-constant with a BEAST2 run on the same data makes it
pretty clear that there is a serious bug somewhere.
The BEAST2 version actually infers mu rather correctly (1,7e-4 instead of 2e-4),
where phylo-constant gives a value of 1e-3 which is very very wrong.
I would say there are two possibilities ;
either a ratio somewhere is wrong, or the likelihood computation is wrong.
For the ratio, if some ratios are correct, I can try by removing one of them,
and see if the inference changes.
For the likelihood, it doesn't seem to be *totally* wrong at least.
To check the correctness of the implementation does not seem very easy...


2020-08-25 -- Optimization
--------------------------

The comparison with BEAST2 shows that on the logistic example with 32 sequences,
BEAST is at least 30 times faster (for 1e5 iterations).
That's way too big of a gap.
perf shows that there's still too many bigarray allocations,
and that the coalescent prior is pretty expensive too.
Seqsim.Probas functions that allocate too much :
- syps
- sypv
- mul for vectors (from where ?)
- mul for matrices
- lacpy


The other important thing to do is to reduce computations in the tree
likelihood. This is a bit involved, but it should help a lot.


2020-08-27 -- Wrong seqsim transition rates and probas
------------------------------------------------------

Checking directly with Biocaml shows that in my implementation,
mu does not have the same meaning (or even a consistent meaning),
compared with Biocaml, or BEAST2 or phangorn.
In these (inference) programs, mu is the expected number of substitutions
per site per unit time, whatever the model.
It makes sense to align our implementation on the others for comparison.

For JC69 that meant multiplying the rate by 4./.3, so that 
For HKY85 it seems instant_rates and transition_probas were in disagreement.
For GTR and SNP, it's not totally clear what I should do with the relative rates.

For HKY85, the fix seems to work.
It should also work ok for SNP, but the next problem is that
rates (subst_rate, erase_proba) for simulations also need to change.

Also I need to check whether I'm doing the matrix multiplications correctly
in Seqsim.Tree, for transition matrices which are not symmetric.


Also this all doesn't fix our more general inference problem.
The next items are :
- check the coalescent prior
- check the proposals (again...)


2020-09-02 -- Phylo sequence input
----------------------------------

What to do if there are indels in the alignment ?
I have avoided the problem until now, but it needs to be addressed
for the real life.

- simplest, but bad:
  just trim the alignment from all sites with indels.

- also simple, but also bad:
  replace indels by equilibrium frequencies.
  That means it's handled as missing data, but it's not the same.

- better, but a bit complex:
  when there is an indel somewhere, we want to do as if the leaf
  was not there on the tree.
  We don't want to handle this site by site,
  but we cant cut the alignment into sections with the same indel
  pattern, and fold over these sections.
  In the likelihood function, we return None for a subtree with indels.
  Meaning :
  - need to find the patterns correctly
  - make the likelihood functions more complex
  - will be slower because we cut the BLAS stuff into more bits


2020-09-04 -- Improve epi-gen-data
----------------------------------

I want to be able to generate the data for the concatenated traj files
from MCMC. To make this simpler, I need to make it more composable.
The file got longer, but it's ok, it works.


2020-09-05 -- Improve coalfit
-----------------------------

Our problem is with proposals that don't actually change the trajectory,
but still adapt the matrix.
This will tend to make the proposal more aggressive.
Then when randomly the proposal actually changes something, it's too agressive,
and it's rejected.
Either we need the adaptation to find what changes the trajectory and what
doesn't.
Or at a minimum, we need to not adapt when nothing happens.
How could we do that ?
Is there a situation where I don't adapt ?


2020-09-23 -- Optimize Coal.Constant_lazy
-----------------------------------------

- Use simpler colors
- Use more clever colormaps where possible
- Use cardinal trajectories instead of a coal population trajectory


2020-09-23 -- Fix Coalfit.Boosted
---------------------------------

I broke the proposals when I introduced Draws and choices.
Now I fixed it again.
I changed when the boosting happens, so I need to check
that the results are good.


2020-09-24 -- Slow RAM adaptation
---------------------------------

Does the rate of RAM adaptation decrease too fast ?
- compare with a smaller gamma
- add a kmul parameter to rescale adaptation rate
  (if it's 1./.2., it decreases twice slower)

Since I'm not looking for convergence, I don't feel like there is a good reason
to decrease adaptation, especially fast.
As a first rule, it should at least scale with the choice probability !


2021-01-05 -- Loglik output
---------------------------

After a long hiatus, I come back to the Journal.
It would be nice to have access to all the component values
of the different log-likelihoods to better understand what is going on
in the phylo inference.
A full-fledged logging system would obviously be better for this,
but that's a too big change.
More resonably, we need to be able to pass output functions through
the loglik computation functions.
How ?

Can it directly happen inside the likelihood calls ?
Then who is responsible for opening/closing the channel(s) ?
I guess it could happen either individually inside the different
`log_likelihood` calls, or in the `Likelihood.dist` call.

Problems is I also need to be able to pass `k`
through the likelihood functions to print correctly...

If I do the printing through this through Mh, I won't have the details
of the computations, only the component list.
Or a more general approach, which could reuse the current output
that goes through Mh would be to allow a polymorphic likelihood
instead of a float list.
I don't really want to functorize over that, so I could introduce
a `'a likelihood` type with a sum function and a print function.

Would that be easy and lightweight ?

While I'm at it, should I do the same for logprior ?
Let us see if it works for loglik.
The problem is that the Fit functions need to become polymorphic too.
And I also need to know what to do about the Dist constructors...


2021-10-20 -- Bad peak for SIR sequence data
--------------------------------------------

For the inference on the SIR simulated dataset with sequence data alone,
the I peak is inferred too early and too big.
Looking at the estimates of peak time and peak magnitude,
they are strongly negatively correlated, and are driven by loglik-tree.
So it seems that the tree is inferred differently from its actual true value.

What do the phylogeny estimates look like ? Are they weird ?
What drives this effect in the loglik-eco: look at the components.
Is there an error ?

Those are the next steps.

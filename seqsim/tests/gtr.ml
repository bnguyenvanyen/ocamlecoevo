module S = Seqsim
open S.Sig


let par = S.Gtr.{
  mu = 1. ;
  rct = 1. /. 6. ;
  rat = 1.1 /. 6. ;
  rgt = 0.9 /. 6. ;
  rac = 1.2 /. 6. ;
  rcg = 0.8 /. 6. ;
  pia = 0.22 ;
  pit = 0.26 ;
  pic = 0.27 ;
}


(* check that if dt is 0., then P is Id *)
let%test _ =
  let p = S.Gtr.transition_probas par F.zero in
  Base.check_id p


(* check generic instant rates *)
let%expect_test _ =
  let q = S.Gtr.instant_rates par in
  Lacaml.Io.pp_fmat Format.std_formatter q ;
  [%expect{|
    -1.15622  0.384512 0.435601 0.336112
    0.325356 -0.990858 0.363001 0.302501
    0.354934  0.349556 -0.97338 0.268889
    0.295778  0.314601 0.290401 -0.90078 |}]


(* check generic transition probabilities *)
let%expect_test _ =
  let p = S.Gtr.transition_probas par F.one in
  Lacaml.Io.pp_fmat Format.std_formatter p ;
  [%expect{|
    0.397399 0.200896 0.217668 0.184036
    0.169989 0.454249 0.200218 0.175543
    0.177359 0.192803 0.463618  0.16622
    0.161952 0.182565 0.179518 0.475965 |}]


(* compare instant rates with Jc69 *)
let%test _ =
  let par = S.Jc69.{ mu = 1. } in
  let par' = S.Jc69.to_gtr par in
  let q = S.Jc69.instant_rates par in
  let q' = S.Gtr.instant_rates par' in
  Base.check_equal q q'


(* compare transition probabilities with Jc69 *)
let%test _ =
  let par = S.Jc69.{ mu = 1. } in
  let par' = S.Jc69.to_gtr par in
  let p = S.Jc69.transition_probas par F.one in
  let p' = S.Gtr.transition_probas par' F.one in
  Base.check_equal_up p p'


(* compare instant rates with Hky85 *)
let%test _ =
  let par = S.Hky85.{
    default with mu = 1. ;
                 kappa = 1.5 ;
                 pia = 0.1 ;
  }
  in
  let par' = S.Hky85.to_gtr par in
  let q = S.Hky85.instant_rates par in
  let q' = S.Gtr.instant_rates par' in
  Base.check_equal q q'


(* compare transition probabilities with Hky85 *)
let%test _ =
  let par = S.Hky85.{
    default with mu = 1. ;
                 kappa = 1.5 ;
                 pia = 0.1 ;
  }
  in
  let par' = S.Hky85.to_gtr par in
  let p = S.Hky85.transition_probas par F.one in
  let p' = S.Gtr.transition_probas par' F.one in
  Base.check_equal p p'

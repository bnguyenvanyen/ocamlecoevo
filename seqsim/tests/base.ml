open Seqsim.Sig

let check_zero mat =
  U.int_fold ~f:(fun b i ->
    U.int_fold ~f:(fun b' j ->
      b' && (U.nearly_equal mat.{i,j} 0.)
    ) ~x:b ~n:4
  ) ~x:true ~n:4


let check_zero_up mat =
  U.int_fold ~f:(fun b j ->
    U.int_fold ~f:(fun b' i ->
      b' && (U.nearly_equal mat.{i,j} 0.)
    ) ~x:b ~n:j
  ) ~x:true ~n:4


let check_equal m m' =
  check_zero (Lac.Mat.sub m m')


let check_equal_up m m' =
  check_zero_up (Lac.Mat.sub m m')


let check_zero_col j mat =
  U.int_fold ~f:(fun b i ->
    b && (U.nearly_equal mat.{i,j} 0.)
  ) ~x:true ~n:4


let check_id mat =
  U.int_fold ~f:(fun b i ->
    U.int_fold ~f:(fun b' j ->
      b' && (
        if i = j then
          U.nearly_equal mat.{i,j} 1.
        else
          U.nearly_equal mat.{i,j} 0.
      )
    ) ~x:b ~n:4
  ) ~x:true ~n:4

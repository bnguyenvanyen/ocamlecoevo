module M = BatMap
module F = Util.Float
module S = Seqsim

module Fel = S.Tree.Make (Tree.Lt.State) (Tree.Lt)


module X = S.With_gamma.Make (S.Jc69)


let seq = Seqs.of_string


let leaf t k =
  Tree.Lt.leaf (t, k)


let node t k stree =
  Tree.Lt.node (t, k) stree


let binode t k ltree rtree =
  Tree.Lt.binode (t, k) ltree rtree


let par =
  let sub = S.Jc69.{ mu = 1. } in
  S.With_gamma.{ sub ; shape = 1. ; n_cat = 4 }


let loglik =
  let eq = S.Jc69.equilibrium_probas par.sub in
  let tp = X.transition_probas par in
  let trans_mat dt =
    tp (F.Pos.of_float dt)
  in
  let trans =
    S.Probas.sym_transition Heterogen trans_mat
  in
  let loglik kseqs tree =
    let seq_probas = S.Probas.sequences kseqs in
    Fel.log_likelihood_variant Heterogen seq_probas eq trans tree
  in loglik


(* Compare with phangorn
   In R:
   > library(phangorn)
   > seqs = phyDat(read.FASTA("triplet.fasta"))
   > tree = read.tree("triplet_good.newick")
   > pml(tree, seqs, model="JC69", k=4)
   loglikelihood: -78.3102 
 *)
let%expect_test _ =
  let kseqs =
    M.Int.empty
    |> M.Int.add 1 (seq "TTCAATGAATACATCGATCG")
    |> M.Int.add 2 (seq "ATCGATCGATCGGACGTTCG")
    |> M.Int.add 3 (seq "ACCGAGAGATCGGACGTTCG")
  in
  let a = leaf 9.01 1 in
  let b = leaf 9.02 2 in
  let c = leaf 9.03 3 in
  let tree = node 0. 6 (binode 3. 5 a (binode 6. 4 b c)) in
  (* loglikelihood: -78.3102 *)
  Printf.printf "%f" (loglik kseqs tree) ;
  [%expect{| -78.703914 |}]

module S = Seqsim
open S.Sig


(* if mu is 0., then Q is 0. *)
let%test _ =
  let par = S.Jc69.{ mu = 0. } in
  let q = S.Jc69.instant_rates par in
  Base.check_zero q


(* if mu is 0., then P is Id *)
let%test _ =
  let par = S.Jc69.{ mu = 0. } in
  let p = S.Jc69.transition_probas par F.one in
  Base.check_id p


(* if dt is 0., then P is Id *)
let%test _ =
  let par = S.Jc69.{ mu = 1. } in
  let p = S.Jc69.transition_probas par F.zero in
  Base.check_id p


(* check that instant_rates and transition_probas match *)
let%expect_test _ =
  let par = S.Jc69.{ mu = 1. } in
  let trans = S.Jc69.transition_probas par F.one in
  let trans' = U.Mat.sym_linear_flow
    ~up:true
    (S.Jc69.instant_rates par)
    F.one
  in
  S.Probas.Mat.print Homogen BatIO.stdout trans ;
  [%expect{|
    0.447698 0.184101 0.184101 0.184101
           0 0.447698 0.184101 0.184101
           0        0 0.447698 0.184101
           0        0        0 0.447698 |}] ;
  S.Probas.Mat.print Homogen BatIO.stdout trans' ;
  [%expect{|
    0.447698 0.184101 0.184101 0.184101
    0.184101 0.447698 0.184101 0.184101
    0.184101 0.184101 0.447698 0.184101
    0.184101 0.184101 0.184101 0.447698 |}]

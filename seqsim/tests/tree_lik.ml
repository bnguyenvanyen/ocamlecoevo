open Seqsim.Sig

module S = Seqsim
module M = BatMap



(* module T = Tree.Timed.Make (S) *)
module T = Tree.Lt


(* Felsenstein *)
module Fel = S.Tree.Make (Tree.Lt.State) (T)


let seq = Seqs.of_string


let leaf t k =
  T.leaf (t, k)


let node t k stree =
  T.node (t, k) stree


let binode t k ltree rtree =
  T.binode (t, k) ltree rtree


(* first, test with JC69 *)


let equilibrium () =
  S.Jc69.equilibrium_probas ()


let par =
  S.Jc69.{ mu = 1. }

let trans_mat dt =
  (* needs a fresh matrix at every dt *)
  S.Jc69.transition_probas par (F.Pos.of_float dt)


let transition () =
  let tp = S.Jc69.transition_probas par in
  let trans_mat dt =
    tp (F.Pos.of_float dt)
  in
  S.Probas.sym_transition Homogen trans_mat


let jc_loglik =
  let eq = equilibrium () in
  let loglik kseqs tree =
    Fel.log_likelihood_sites eq kseqs trans_mat tree
  in loglik


let jc_loglik_mat =
  let eq = equilibrium () in
  let trans = transition () in
  let loglik kseqs tree =
    let seq_probas = S.Probas.sequences kseqs in
    Fel.log_likelihood_variant Homogen seq_probas eq trans tree
  in loglik


let jc_loglik_inv =
  let eq = equilibrium () in
  let trans = transition () in
  let loglik invs t =
    Fel.log_likelihood_invariant Homogen invs eq trans t
  in loglik


let%expect_test _ =
  let kseqs =
    M.Int.empty
    |> M.Int.add 1 (seq "A")
    |> M.Int.add 2 (seq "A")
  in
  let lleaf = leaf 2. 1 in
  let rleaf = leaf 2.5 2 in
  let tree = node 0. 4 (binode 1. 3 lleaf rleaf) in
  Printf.printf "%f" (jc_loglik kseqs tree) ;
  [%expect{| -2.670915 |}]


let%expect_test _ =
  let kseqs =
    M.Int.empty
    |> M.Int.add 1 (seq "ACCGATAT")
    |> M.Int.add 2 (seq "GTCTCACG")
    |> M.Int.add 3 (seq "GTGTCCAG")
  in
  let a = leaf 5. 1 in
  let b = leaf 6. 2 in
  let c = leaf 9. 3 in
  let good_tree = node 0. 6 (binode 2. 5 a (binode 5. 4 b c)) in
  let bad_tree = node 0. 6 (binode 2.5 5 c (binode 4. 4 a b)) in
  Printf.printf "%f < %f"
  (jc_loglik kseqs bad_tree) (jc_loglik kseqs good_tree) ;
  [%expect{| -33.346982 < -33.256195 |}]


(* Phangorn results similar but not equal
   To compare in R :
   > library(phangorn)
   > seqs = phyDat(read.FASTA("triplet.fasta"))
   > tree_good = read.tree("triplet_good.newick")
   > tree_bad = read.tree("triplet_bad.newick")
   > pml(tree_good, seqs)
   loglikelihood: -83.1626
   > pml(tree_bad, seqs)
   loglikelihood: -83.17257
 *)
let%expect_test _ =
  let kseqs =
    M.Int.empty
    |> M.Int.add 1 (seq "TTCAATGAATACATCGATCG")
    |> M.Int.add 2 (seq "ATCGATCGATCGGACGTTCG")
    |> M.Int.add 3 (seq "ACCGAGAGATCGGACGTTCG")
  in
  let a = leaf 9.01 1 in
  let b = leaf 9.02 2 in
  let c = leaf 9.03 3 in
  let good_tree = node 0. 6 (binode 3. 5 a (binode 6. 4 b c)) in
  let bad_tree = node 0. 6 (binode 3. 5 b (binode 6. 4 a c)) in
  Printf.printf "%f < %f"
  (jc_loglik kseqs bad_tree) (jc_loglik kseqs good_tree) ;
  [%expect{| -83.172570 < -83.162601 |}]


(* compare small invariant trees *)
let%expect_test _ =
  let kseqs =
    M.Int.empty
    |> M.Int.add 1 (seq "A")
    |> M.Int.add 2 (seq "A")
  in
  let a = leaf 5. 1 in
  let b = leaf 6. 2 in
  let tree1 = node 0. 4 (binode 1. 3 a b) in
  let tree2 = node 0. 4 (binode 2. 3 a b) in
  let tree3 = node 0. 4 (binode 3. 3 a b) in
  let tree4 = node 0. 4 (binode 4. 3 a b) in
  Printf.printf "%f" (jc_loglik kseqs tree1) ;
  [%expect{| -2.772570 |}] ;
  Printf.printf "%f" (jc_loglik kseqs tree2) ;
  [%expect{| -2.772323 |}] ;
  Printf.printf "%f" (jc_loglik kseqs tree3) ;
  [%expect{| -2.768778 |}] ;
  Printf.printf "%f" (jc_loglik kseqs tree4) ;
  [%expect{| -2.719098 |}]


(* test matrix implementation *) 
let%expect_test _ =
  let kseqs =
    M.Int.empty
    |> M.Int.add 1 (seq "TTCAATGAATACATCGATCG")
    |> M.Int.add 2 (seq "ATCGATCGATCGGACGTTCG")
    |> M.Int.add 3 (seq "ACCGAGAGATCGGACGTTCG")
  in
  let a = leaf 9.01 1 in
  let b = leaf 9.02 2 in
  let c = leaf 9.03 3 in
  let tree = node 0. 6 (binode 3. 5 a (binode 6. 4 b c)) in
  Printf.printf "%f = %f"
  (jc_loglik kseqs tree)
  (jc_loglik_mat kseqs tree) ;
  [%expect{| -83.162601 = -83.162601 |}]


(* test invariant implementation *)
let%expect_test _ =
  let kseqs =
    M.Int.empty
    |> M.Int.add 1 (seq "ATTCCCGGGG")
    |> M.Int.add 2 (seq "ATTCCCGGGG")
    |> M.Int.add 3 (seq "ATTCCCGGGG")
  in
  let a = leaf 9.01 1 in
  let b = leaf 9.02 2 in
  let c = leaf 9.03 3 in
  let tree = node 0. 6 (binode 3. 5 a (binode 6. 4 b c)) in
  (* make it by hand *)
  let invs = S.Base.Ntm.(
    empty
    |> add Seqs.Dna.A 1
    |> add Seqs.Dna.T 2
    |> add Seqs.Dna.C 3
    |> add Seqs.Dna.G 4
  )
  in
  Printf.printf "%f = %f"
  (jc_loglik_mat kseqs tree) (jc_loglik_inv invs tree) ;
  [%expect{| -41.579414 = -41.579414 |}]


(* test split invariant *)
let%expect_test _ =
  let module Ntm = S.Base.Ntm in
  let kseqs =
    M.Int.empty
    |> M.Int.add 1 (seq "TTCAATGAATACATCGATCG")
    |> M.Int.add 2 (seq "ATCGATCGATCGGACGTTCG")
    |> M.Int.add 3 (seq "ACCGAGAGATCGGACGTTCG")
  in
  let invs, kseqs' = S.Base.split_invariant kseqs in
  M.Int.print BatInt.print Seqs.Io.print BatIO.stdout kseqs' ;
  [%expect{|
    {
    1: TTATGAACATA,
    2: ATGTCGCGGAT,
    3: ACGGAGCGGAT
    } |}] ;
  Ntm.print Seqs.Dna.print BatInt.print BatIO.stdout invs ;
  [%expect{|
    {
    A: 2,
    T: 2,
    C: 3,
    G: 2
    } |}]


let to_letter =
  function
  | 1 ->
      "A"
  | 2 ->
      "B"
  | 3 ->
      "C"
  | 4 ->
      "D"
  | 5 ->
      "E"
  | 6 ->
      "F"
  | _ ->
      failwith "Not_implemented"


let ecoevo_loglik seqs tree =
  let seqs = L.fold_left (fun m (k, s) ->
    M.Int.add k (seq s) m
  ) M.Int.empty seqs
  in
  jc_loglik_mat seqs tree


let biocaml_loglik seqs tree =
  let module Phy = Phylogenetics in
  let module Al = Phy.Alignment.Make (Phy.Seq.DNA) in
  let module F = Phy.Felsenstein.Make
    (Phy.Nucleotide)
    (Al)
    (Phy.Site_evolution_model.JC69)
  in
  let tree =
    tree
    (* transform times to lengths *)
    |> T.untime_tree
    |> T.Len.basic
    (* transform ints to letters *)
    |> Tree.Base.map (fun (len, k) -> (len, to_letter k))
    (* drop the root *)
    |> Tree.Base.case
       ~leaf:(fun _ -> failwith "leaf")
       ~binode:(fun _ -> failwith "binode")
       ~node:(fun _ stree -> stree)
    (* to newick *)
    |> Tree.Base.to_newick (fun (len, lab) ->
        Printf.sprintf "%s:%f" lab len
       )
    (* read the newick with biocaml *)
    |> Phy.Phylogenetic_tree.of_newick
  in
  let seq s =
    Phy.Seq.DNA.of_string_exn s
  in
  let align =
    seqs
    |> L.map (fun (k, s) -> (to_letter k, seq s))
    |> Al.of_assoc_list
  in
  F.felsenstein () tree align


(* compare with biocaml implementation *)
let%expect_test _ =
  let seqs = [
    (1, "TTCAATGAATACATCGATCG") ;
  ]
  in
  let a = leaf 4. 1 in
  let tree = node 3. 6 a in
  let elogd = ecoevo_loglik seqs tree in
  let blogd = biocaml_loglik seqs tree in
  Printf.printf "%f ?= %f" elogd blogd ;
  [%expect{| -27.725887 ?= -27.725887 |}]


let%expect_test _ =
  let seqs = [
    (1, "TTCAATGAATACATCGATCG") ;
    (2, "ATCGATCGATCGGACGTTCG") ;
  ]
  in
  let a = leaf 4. 1 in
  let b = leaf 7. 2 in
  let tree = node 3. 6 (binode 3. 5 a b) in
  let elogd = ecoevo_loglik seqs tree in
  let blogd = biocaml_loglik seqs tree in
  Printf.printf "%f ?= %f" elogd blogd ;
  [%expect{| -55.421318 ?= -55.421318 |}]


let%expect_test _ =
  let seqs = [
    (1, "TTCAATGAATACATCGATCG") ;
    (2, "ATCGATCGATCGGACGTTCG") ;
    (3, "ACCGAGAGATCGGACGTTCG") ;
  ]
  in
  let a = leaf 9.01 1 in
  let b = leaf 9.02 2 in
  let c = leaf 9.03 3 in
  let tree = node 0. 6 (binode 3. 5 a (binode 6. 4 b c)) in
  let elogd = ecoevo_loglik seqs tree in
  let blogd = biocaml_loglik seqs tree in
  Printf.printf "%f ?= %f" elogd blogd ;
  [%expect{| -83.162601 ?= -83.162601 |}]


(* then, test with non-symmetric HKY *)

let hky_loglik =
  let par = S.Hky85.{
    default with
    mu = 1. ;
    kappa = 1.5 ;
    pia = 0.1 ;
  }
  in
  let eq = S.Hky85.equilibrium_probas par in
  let trans =
    let tp = S.Hky85.transition_probas par in
    let trans_mat dt =
      tp (F.Pos.of_float dt)
    in
    S.Probas.gen_transition Homogen trans_mat
  in
  let loglik seqs tree =
    let kseqs = L.fold_left (fun m (k, s) ->
      M.Int.add k (seq s) m
    ) M.Int.empty seqs
    in
    let seq_probas = S.Probas.sequences kseqs in
    Fel.log_likelihood_variant Homogen seq_probas eq trans tree
  in loglik



let%expect_test _ =
  let seqs = [
    (1, "TTCAATGAATACATCGATCG") ;
    (2, "ATCGATCGATCGGACGTTCG") ;
    (3, "ACCGAGAGATCGGACGTTCG") ;
  ]
  in
  let a = leaf 9.01 1 in
  let b = leaf 9.02 2 in
  let c = leaf 9.03 3 in
  let tree = node 0. 6 (binode 3. 5 a (binode 6. 4 b c)) in
  let logd = hky_loglik seqs tree in
  Printf.printf "%f" logd ;
  [%expect{| -90.305313 |}]

module S = Seqsim

open S.Sig

(* if mu is 0., then Q is 0. *)
let%test _ =
  let par = S.Hky85.{ default with mu = 0. } in
  let q = S.Hky85.instant_rates par in
  Base.check_zero q


(* if mu is 0., then P is Id *)
let%test _ =
  let par = S.Hky85.{ default with mu = 0. } in
  let p = S.Hky85.transition_probas par F.one in
  Base.check_id p


(* if dt is 0., then P is Id *)
let%test _ =
  let p = S.Hky85.transition_probas S.Hky85.default F.zero in
  Base.check_id p


(* if pia is 0., we can't get any As *)
let%test _ =
  let par = S.Hky85.{ default with pia = 0. } in
  let q = S.Hky85.instant_rates par in
  let p = S.Hky85.transition_probas par F.one in
     (q.{2,1} = 0.) && (q.{3,1} = 0.) && (q.{4,1} = 0.)
  && (p.{2,1} = 0.) && (p.{3,1} = 0.) && (p.{4,1} = 0.)


(* look at generic instant rates *)
let%expect_test _ =
  let par = S.Hky85.{
    default with mu = 1. ;
                 kappa = 1.5 ;
                 pia = 0.1 ;
  }
  in
  let q = S.Hky85.instant_rates par in
  S.Probas.Mat.print Homogen BatIO.stdout q ;
  [%expect{|
    -1.36223 0.309598 0.309598  0.743034
    0.123839 -1.08359 0.464396  0.495356
    0.123839 0.464396 -1.08359  0.495356
    0.185759 0.309598 0.309598 -0.804954 |}]


(* check that instant_rates and transition_probas match *)
let%test _ =
  let par = S.Hky85.{ default with mu = 1. ; kappa = 1.5 } in
  let trans = S.Hky85.transition_probas par F.one in
  let trans' = U.Mat.gen_linear_flow
    (S.Hky85.instant_rates par)
    F.one
  in
  Base.check_equal_up trans trans'


(* check that instant_rates correspond with Jc69 *)
let%test _ =
  let par = S.Jc69.{ mu = 1. } in
  let par' = S.Jc69.to_hky85 par in
  let q = S.Jc69.instant_rates par in
  let q' = S.Hky85.instant_rates par' in
  Base.check_equal q q'


(* check that transition_probas correspond with Jc69 *)
let%test _ =
  let par = S.Jc69.{ mu = 1. } in
  let par' = S.Jc69.to_hky85 par in
  let p = S.Jc69.transition_probas par F.one in
  let p' = S.Hky85.transition_probas par' F.one in
  Base.check_equal_up p p'

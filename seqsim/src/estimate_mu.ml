

module State =
  struct
    type t = float * Seqs.Diff.t
    let to_time (t, _) = t
    let at_time t (_, sd) = (t, sd)
    let to_string (t, sd) =
      Printf.sprintf "%s:%.4f" (Seqs.Io.string_of_seqdiff sd) t
    let of_string s =
      match Scanf.sscanf s "%s@:%f" (fun ss t -> (ss, t)) with
      | ss, t ->
          Some (t, Seqs.Io.seqdiff_of_string ss)
      | exception Scanf.Scan_failure _ ->
          None
  end

module T = Tree.Timed.Make (State)

let is_mutation = function
  | Seqs.Sig.Not -> Printf.eprintf "." ; false
  | _ -> true


let chan_in_r = ref stdin
let chan_out_r = ref stdout

let specl = [("-in", Arg.String (fun s -> chan_in_r := open_in s),
              "Input file -- default stdin.") ;
             ("-out", Arg.String (fun s -> chan_out_r := open_out s),
              "Output file -- default stdout.")]

let anon_fun s = Printf.printf "Ignored anonymous argument %s" s
let usage_msg = "  Read the seqdiff binary tree at 'in',
  and output the total number of mutations, total branch length,
  and ratio of those, as a raw estimate of the mutation rate, to 'out'."


let main () =
  begin
    Arg.parse specl anon_fun usage_msg ;
    let tree = T.read_lengthed_newick !chan_in_r in
    close_in !chan_in_r ;
    let blen = T.branch_length tree in
    let nmu = T.left_fold (fun (_, sd) n ->
        if is_mutation sd then
          1
        else
          n
      ) tree 0
    in
    let mur = (float nmu) /. blen in
    let ch = !chan_out_r in
    Printf.fprintf ch "branch_length,n_mutations,mu_rate\n" ;
    Printf.fprintf ch "%f,%i,%f\n" blen nmu mur ;
    close_out ch
  end ;;

main ()

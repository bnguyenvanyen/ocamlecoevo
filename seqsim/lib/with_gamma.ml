open Sig


module type INPUT = With_heterogen.INPUT


module type S = With_heterogen.S


type 'a t = {
  sub : 'a ;
  shape : float ;
  n_cat : int ;
}


let discretized_gamma n shape =
  let ps = L.init n (fun i ->
    (2. *. float i +. 1.) /. (2. *. float n)
  )
  in
  let rs = L.map (fun p ->
    Owl_stats_dist.gamma_ppf ~shape ~scale:(1. /. shape) p
  ) ps
  in
  let m = L.fold_left (+.) 0. rs in
  L.map (fun r -> (r *. float n /. m, 1. /. float n)) rs


module Term =
  struct
    let shape () =
      let doc =
        "Shape parameter of the gamma distribution."
      in
      let get th =
        th.shape
      in
      let set shape th =
        { th with shape }
      in
      U.Arg.Update.Capture.opt_config
        get
        set
        Cmdliner.Arg.float
        ~doc
        "gamma-shape"

    let n_cat =
      let doc =
        "Number of categories of the gamma distribution."
      in
      U.Arg.Capture.opt_config Cmdliner.Arg.int ~doc "n-gamma-cat"

    let n_gamma_cat () =
      let get th =
        th.n_cat
      in
      let set n_cat th =
        { th with n_cat }
      in
      let f n_cat ~config =
        U.Arg.Update.Capture.update_capture_default
          get
          set
          Cmdliner.Arg.int
          "n-gamma-cat"
          (n_cat ~config)
      in
      Cmdliner.Term.(
        const f
        $ n_cat
      )
  end


module Make (X : INPUT) :
  (S with type sub = X.t
      and type t = X.t t) =
  struct
    type sub = X.t

    type nonrec t = sub t

    let default = {
      sub = X.default ;
      shape = 1. ;
      n_cat = 4 ;
    }

    let mu th =
      X.mu th.sub

    let with_mu mu th =
      { th with sub = X.with_mu mu th.sub }

    module Term =
      struct
        include Term

        let sub =
          let set sub p = { p with sub } in
          let f read ~config =
            U.Arg.Update.Capture.update_always
              set
              (read ~config)
          in
          Cmdliner.Term.(const f $ X.capture_term)

      end

    let capture_term =
      let f sub shape n_cat ~config =
        default
        |> U.Arg.Capture.empty
        |> sub ~config
        |> shape ~config
        |> n_cat ~config
      in
      Cmdliner.Term.(
        const f
        $ Term.sub
        $ Term.shape ()
        $ Term.n_gamma_cat ()
      )

    let extract th =
      function
      | "gamma-shape" ->
          Some (string_of_float th.shape)
      | "n-gamma-cat" ->
          Some (string_of_int th.n_cat)
      | s ->
          X.extract th.sub s

    let dist th =
      L.map (fun (r, p) ->
        let mu = F.Pos.Op.(F.Pos.of_float r * X.mu th.sub) in
        (X.with_mu mu th.sub, p)
      ) (discretized_gamma th.n_cat th.shape)
      
    module H = With_heterogen.Make (X)

    type vec = H.vec
    type mat = H.mat

    let settings = H.settings

    let transition_probas ?mat th =
      H.transition_probas dist ?mat th
  end

(* About BLAS-like functions names :
 * - 'v' stands for a simple vector
 * - 'm' stands for a simple matrix
 * - 's' stands for a distributed vector
 * - 'p' stands for a distributed matrix
 *)

open Sig

module S = BatSet
module M = BatMap


type vec = Lac.vec
type mat = Lac.mat

(* discrete distribution *)
type 'a dist = ('a * float) list

type (_, _) settings =
  | Homogen : (vec, mat) settings
  | Heterogen : (vec dist, mat dist) settings


type ('u, 'v) transition = {
  v :
    ?mem: 'u ->
    dt: float ->
    vec ->
      'u
  ;
  s :
    ?mem: 'u ->
    dt: float ->
    'u ->
      'u
  ;
  m :
    ?mem: 'v ->
    dt: float ->
    mat ->
      'v
  ;
  p :
    ?mem: 'v ->
    dt: float ->
    'v ->
      'v
}


let map3 f a b c =
  L.map2 (fun a (b, c) -> f a b c) a (L.combine b c)


(* as a first approximation, we need a fresh vector for each site *)
let of_nucleotide nt =
  let i = Seqs.Dna.to_int nt in
  let v = Lac.Vec.make0 4 in
  v.{i + 1} <- 1. ;
  v


let mat_print fmt a =
  Lacaml.Io.pp_fmat (BatFormat.formatter_of_out_channel fmt) a


module Vec =
  struct
    let copy
      (type u v) (stgs : (u, v) settings)
      ?(y : u option) (x : u) : u =
      match stgs with
      | Homogen ->
          Lac.copy ?y x
      | Heterogen ->
          begin match y with
          | None ->
              L.map (fun (x, p) -> (Lac.copy x, p)) x
          | Some y ->
              L.map2 (fun (x, p) (y, _) -> (Lac.copy ~y x, p)) x y
          end

    let mul
      (type u v) (stgs : (u, v) settings)
      ?(z : u option) (x : u) (y : u) : u =
      match stgs with
      | Homogen ->
          Lac.Vec.mul ?z x y
      | Heterogen ->
          begin match z with
          | None ->
              L.map2 (fun (x, p) (y, _) ->
                (Lac.Vec.mul x y, p)
              ) x y
          | Some z ->
              map3 (fun (x, p) (y, _) (z, _) ->
                (Lac.Vec.mul ~z x y, p)
              ) x y z
          end
  end


module Mat =
  struct
    let make0_like
      (type u v) (stgs : (u, v) settings)
      (m : v) : v =
      let homo_make0_like a =
        let m = Lac.Mat.dim1 a in
        let n = Lac.Mat.dim2 a in
        Lac.Mat.make0 m n
      in
      match stgs with
      | Homogen ->
          homo_make0_like m
      | Heterogen ->
          L.map (fun (m, p) -> (homo_make0_like m, p)) m

    let fill
      (type u v) (stgs : (u, v) settings)
      (m : v) x =
      match stgs with
      | Homogen ->
          Lac.Mat.fill m x
      | Heterogen ->
          L.iter (fun (m, _) -> Lac.Mat.fill m x) m

    let zero
      (type u v) (stgs : (u, v) settings)
      ?(store : (v * S.Int.t) M.Int.t option)
      k : v option =
      Option.map (fun kmats ->
        let mat, _ = M.Int.find k kmats in
        fill stgs mat 0. ;
        mat
      ) store

    let iter
      (type u v) (stgs : (u, v) settings)
      f (a : v) =
      let iter a =
        let m = Lac.Mat.dim1 a in
        let n = Lac.Mat.dim2 a in
        for i = 1 to m do
          for j = 1 to n do
            f a.{i,j}
          done
        done
      in 
      match stgs with
      | Homogen ->
          iter a
      | Heterogen ->
          L.iter (fun (a, _) ->
             iter a
          ) a

    let sub
      (type u v) (stgs : (u, v) settings)
      ?(c : v option) (a : v) (b : v) : v =
      match stgs with
      | Homogen ->
          Lac.Mat.sub ?c a b
      | Heterogen ->
          (* only gets called inside the tree likelihood functions,
           * so that both sides will have the same distributions
           * over transition matrices, and we can ignore them *)
          begin match c with
          | None ->
              L.map2 (fun (a, p) (b, _) ->
                (Lac.Mat.sub a b, p)
              ) a b
          | Some c ->
              map3 (fun (a, p) (b, _) (c, _) ->
                (Lac.Mat.sub ~c a b, p)
              ) a b c
          end

    let mul
      (type u v) (stgs : (u, v) settings)
      ?(c : v option) (a : v) (b : v) : v =
      match stgs with
      | Homogen ->
          Lac.Mat.mul ?c a b
      | Heterogen ->
          (* only gets called inside the tree likelihood functions,
           * so that both sides will have the same distributions
           * over transition matrices, and we can ignore them *)
          begin match c with
          | None ->
              L.map2 (fun (a, p) (b, _) ->
                (Lac.Mat.mul a b, p)
              ) a b
          | Some c ->
              map3 (fun (a, p) (b, _) (c, _) ->
                (Lac.Mat.mul ~c a b, p)
              ) a b c
          end

    let ssqr_diff
      (type u v) (stgs : (u, v) settings)
      (a : v) (b : v) =
      match stgs with
      | Homogen ->
          Lac.Mat.ssqr_diff a b
      | Heterogen ->
          let xps = L.map2 (fun (a, p) (b, _) ->
            (Lac.Mat.ssqr_diff a b, p)
          ) a b
          in
          L.fold_left (fun sum (x, p) -> sum +. p *. x) 0. xps

    let print
      (type u v) (stgs : (u, v) settings)
      fmt (a : v) =
      let mat_print fmt a =
        Lacaml.Io.pp_fmat (BatFormat.formatter_of_out_channel fmt) a
      in
      match stgs with
      | Homogen ->
          mat_print fmt a
      | Heterogen ->
          L.print
            (BatTuple.Tuple2.print mat_print BatFloat.print)
            fmt
            a
  end


let lacpy
  (type u v) (stgs : (u, v) settings)
  ?m ?n ?ar ?ac ?br ?bc ?(b : v option) (a : v) : v =
  match stgs with
  | Homogen ->
      Lac.lacpy ?m ?n ?ar ?ac ?br ?bc ?b a
  | Heterogen ->
      begin match b with
      | None ->
          L.map (fun (a, p) ->
            (Lac.lacpy ?m ?n ?ar ?ac ?br ?bc a, p)
          ) a
      | Some b ->
          L.map2 (fun (a, p) (b, _) ->
            (Lac.lacpy ?m ?n ?ar ?ac ?br ?bc ~b a, p)
          ) a b
      end


let dotvs
  (type u v) (stgs : (u, v) settings)
  (x : vec) (x' : u) : float =
  match stgs with
  | Homogen ->
      Lac.dot x x'
  | Heterogen ->
      let dotted = L.map (fun (x', p) -> (Lac.dot x x', p)) x' in
      L.fold_left (fun sum (y, p) -> sum +. p *. y) 0. dotted


let sypv
  (type u v) (stgs : (u, v) settings)
  ?(y : u option) (a : v) x : u =
  match stgs with
  | Homogen ->
      Lac.symv ?y a x
  | Heterogen ->
      begin match y with
      | None ->
          L.map (fun (a, p) -> (Lac.symv a x, p)) a
      | Some y ->
          L.map2 (fun (a, p) (y, _) -> (Lac.symv ~y a x, p)) a y
      end


let syps
  (type u v) (stgs : (u, v) settings)
  ?(y : u option) (a : v) (x : u) : u =
  match stgs with
  | Homogen ->
      Lac.symv ?y a x
  | Heterogen ->
      begin match y with
      | None ->
          L.map2 (fun (a, p) (x, _) -> (Lac.symv a x, p)) a x
      | Some y ->
          map3 (fun (a, p) (x, _) (y, _) -> (Lac.symv ~y a x, p)) a x y
      end


let sypm
  (type u v) (stgs : (u, v) settings)
  ?side ?(c : v option) (a : v) (b : Lac.mat) : v =
  match stgs with
  | Homogen ->
      Lac.symm ?side ?c a b
  | Heterogen ->
      begin match c with
      | None ->
          L.map (fun (a, p) ->
            (Lac.symm ?side a b, p)
          ) a
      | Some c ->
          L.map2 (fun (a, p) (c, _) ->
            (Lac.symm ?side ~c a b, p)
          ) a c
      end


(* symm between probas *)
let sypp
  (type u v) (stgs : (u, v) settings)
  ?side ?(c : v option) (a : v) (b : v) : v =
  match stgs with
  | Homogen ->
      Lac.symm ?side ?c a b
  | Heterogen ->
      (* only gets called inside the tree likelihood functions,
       * so that both sides will have the same distributions
       * over transition matrices, and we can ignore them *)
      begin match c with
      | None ->
          L.map2 (fun (a, p) (b, _) ->
            (Lac.symm ?side a b, p)
          ) a b
      | Some c ->
          map3 (fun (a, p) (b, _) (c, _) ->
            (Lac.symm ?side ~c a b, p)
          ) a b c
      end


let gepm
  (type u v) (stgs : (u, v) settings)
  ?transa ?transb ?(c : v option) (a : v) (b : Lac.mat) : v =
  match stgs with
  | Homogen ->
      Lac.gemm ?transa ?transb ?c a b
  | Heterogen ->
      begin match c with
      | None ->
          L.map (fun (a, p) ->
            (Lac.gemm ?transa ?transb a b, p)
          ) a
      | Some c ->
          L.map2 (fun (a, p) (c, _) ->
            (Lac.gemm ?transa ?transb ~c a b, p)
          ) a c
      end


let gepp
  (type u v) (stgs : (u, v) settings)
  ?transa ?transb ?(c : v option) (a : v) (b : v) : v =
  match stgs with
  | Homogen ->
      Lac.gemm ?transa ?transb ?c a b
  | Heterogen ->
      (* only gets called inside the tree likelihood functions,
       * so that both sides will have the same distributions
       * over transition matrices, and we can ignore them *)
      begin match c with
      | None ->
          L.map2 (fun (a, p) (b, _) ->
            (Lac.gemm ?transa ?transb a b, p)
          ) a b
      | Some c ->
          map3 (fun (a, p) (b, _) (c, _) ->
            (Lac.gemm ?transa ?transb ~c a b, p)
          ) a b c
      end


let gemp
  (type u v) (stgs : (u, v) settings)
  ?transa ?transb ?(c : v option) (m : Lac.mat) (a : v) : v =
  match stgs with
  | Homogen ->
      Lac.gemm ?transa ?transb ?c m a
  | Heterogen ->
      begin match c with
      | None ->
          L.map (fun (a, p) ->
            (Lac.gemm ?transa ?transb m a, p)
          ) a
      | Some c ->
          L.map2 (fun (a, p) (c, _) ->
            (Lac.gemm ?transa ?transb ~c m a, p)
          ) a c
      end


let gepv
  (type u v) (stgs : (u, v) settings)
  ?trans ?(y : u option) (a : v) x : u =
  match stgs with
  | Homogen ->
      Lac.gemv ?trans ?y a x
  | Heterogen ->
      begin match y with
      | None ->
          L.map (fun (a, p) -> (Lac.gemv ?trans a x, p)) a
      | Some y ->
          L.map2 (fun (a, p) (y, _) -> (Lac.gemv ?trans ~y a x, p)) a y
      end


let geps
  (type u v) (stgs : (u, v) settings)
  ?trans ?(y : u option) (a : v) (x : u) : u =
  match stgs with
  | Homogen ->
      Lac.gemv ?trans ?y a x
  | Heterogen ->
      begin match y with
      | None ->
          L.map2 (fun (a, p) (x, _) ->
            (Lac.gemv ?trans a x, p)
          ) a x
      | Some y ->
          map3 (fun (a, p) (x, _) (y, _) ->
            (Lac.gemv ?trans ~y a x, p)
          ) a x y
      end


let gevp
  (type u v) (stgs : (u, v) settings)
  ?(c : v option) (x : Lac.vec) (a : v) : v =
  gemp stgs ?c (Lac.Mat.from_row_vec x) a


let homogeneize
  (type u v) (stgs : (u, v) settings)
  ?b (a : v) : Lac.mat =
  match stgs with
  | Homogen ->
      a
  | Heterogen ->
      let sum =
        match b with
        | None ->
            let hd, _ = L.hd a in
            let m = Lac.Mat.dim1 hd in
            let n = Lac.Mat.dim2 hd in
            Lac.Mat.make0 m n
        | Some b ->
            Lac.Mat.fill b 0. ;
            b
      in
      L.iter (fun (a, p) ->
        Lac.Mat.axpy ~alpha:p a sum ;
      ) a ;
      sum


let init_nodes_homo ~nsamples ~nsites =
  U.int_fold_right ~f:(fun k ->
    (* for each node k *)
    let m = Lac.Mat.make0 4 nsites in
    M.Int.add k (m, S.Int.empty)
  ) ~n:(2 * nsamples) M.Int.empty


let init_nodes_hetero ~nsamples ~ncat ~nsites =
  U.int_fold_right ~f:(fun k ->
    (* for each node k *)
    let m = L.init ncat (fun _ -> (Lac.Mat.make0 4 nsites, 0.)) in
    M.Int.add k (m, S.Int.empty)
  ) ~n:(2 * nsamples) M.Int.empty


let zero_vectors_homo ~nsamples =
  U.int_fold_right ~f:(fun k ->
    let v = Lac.Vec.make0 4 in
    M.Int.add k v
  ) ~n:(2 * nsamples) M.Int.empty


let zero_vectors_hetero ~nsamples ~ncat =
  U.int_fold_right ~f:(fun k ->
    let v = L.init ncat (fun _ -> (Lac.Vec.make0 4, 0.)) in
    M.Int.add k v
  ) ~n:(2 * nsamples) M.Int.empty


let sequences =
  M.Int.map (fun seq ->
    let n = I.to_int (Seqs.length seq) in
    let ps = Lac.Mat.make0 4 n in
    (* set of indel indices *)
    let is = ref S.Int.empty in
    Seqs.iteri (fun k uc ->
      match Seqs.Dna.of_uchar uc with
      | nt ->
          let i = Seqs.Dna.to_int nt in
          ps.{i + 1, k + 1} <- 1.
      | exception (Invalid_argument _ as e) ->
          (* indels are vectors of 0,
             so that the probability does not change
             under transitions *)
          if BatUChar.char_of uc = '-' then
            is := S.Int.add (k + 1) !is
          else
            raise e
    ) seq
    ;
    (ps, !is)
  )


let sym_transition
  (type u v) (stgs : (u, v) settings)
  (trans_mat : float -> v) =
  let v ?mem ~dt x =
    let trans = trans_mat dt in
    sypv stgs ?y:mem trans x
  in
  let s ?mem ~dt (x : u) =
    let trans = trans_mat dt in
    syps stgs ?y:mem trans x
  in
  let m ?mem ~dt a =
    let trans = trans_mat dt in
    sypm stgs ?c:mem trans a
  in
  let p ?mem ~dt (a : v) =
    let trans = trans_mat dt in
    sypp stgs ?c:mem trans a
  in
  { v ; s ; m ; p }


let gen_transition
  (type u v) (stgs : (u, v) settings)
  (trans_mat : float -> v) =
  let v ?mem ~dt x =
    let trans = trans_mat dt in
    gepv stgs ?y:mem trans x
  in
  let s ?mem ~dt (x : u) =
    let trans = trans_mat dt in
    geps stgs ?y:mem trans x
  in
  let m ?mem ~dt a =
    let trans = trans_mat dt in
    gepm stgs ?c:mem trans a
  in
  let p ?mem ~dt (a : v) =
    let trans = trans_mat dt in
    gepp stgs ?c:mem trans a
  in
  { v ; s ; m ; p }

module C = Cmdliner


let load_from =
  let doc =
    "Load parameter values from $(docv)."
  in
  C.Arg.(
    value
    & opt (some string) None
    & info ["load-par-from"] ~docv:"LOAD-PAR-FROM" ~doc
  )


let capture_to =
  let doc =
    "Save parameter values read to $(docv)."
  in
  C.Arg.(
    value
    & opt (some string) None
    & info ["save-par-to"] ~docv:"SAVE-PAR-TO" ~doc
  )

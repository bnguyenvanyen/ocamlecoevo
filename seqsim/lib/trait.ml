open Seqs.Sig

type t = seq * (float * seqdiff) list

type x = unit

type comparison = t -> t -> int

let comp ((seq1, _):t) ((seq2, _):t) =
  (* only compare lengths *)
  compare (Seqs.length seq1) (Seqs.length seq2)

let compare = Array.make 1 comp

let equal = (=)

let hash = Hashtbl.hash

let copy (seq, sdl) =
  (Seqs.copy seq,
   List.map (fun (t, sd) -> (t, Seqs.Diff.copy sd)) sdl)

let group_of _ = ()

let length (seq, _) = Seqs.length seq

let seq_of (seq, _) = seq
let with_seq (_, sdl) seq = (seq, sdl)
let diff_of (_, sdl) = sdl
let with_diff (seq, _) sdl = (seq, sdl)

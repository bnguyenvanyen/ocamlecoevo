open Sig


type t = {
  mu : float ;
}

let default = {
  mu = 1e-4
}


let to_hky85 (par : t) : Hky85.t = {
  mu = par.mu ;
  kappa = 1. ;
  pia = 0.25 ;
  pit = 0.25 ;
  pic = 0.25 ;
}


let to_gtr (par : t) : Gtr.t =
  Hky85.to_gtr (to_hky85 par)


module Term =
  struct
    let mu =
      let doc =
        "Nucleotide substitution rate (JC69)."
      in
      U.Arg.Capture.opt_config Cmdliner.Arg.float ~doc "mu"

    include Term
  end


let capture_term =
  let get { mu } = mu in
  let set mu _ = { mu } in
  let f read ~config =
    let upd = U.Arg.Update.Capture.update_capture_default
      get
      set
      Cmdliner.Arg.float
      "mu"
      (read ~config)
    in
    upd (U.Arg.Capture.empty default)
  in Cmdliner.Term.(
    const f
    $ Term.mu
  )


let term =
  let f read config capture =
    U.Arg.Capture.output capture (read ~config)
  in Cmdliner.Term.(
    const f
    $ capture_term
    $ Term.load_from
    $ Term.capture_to
  )


let to_snp par = Gtr.to_snp @@ to_gtr @@ par

let mu par =
  F.Pos.of_float par.mu

let with_mu mu _ =
  { mu = F.to_float mu }

let columns = ["mu"]

let extract p =
  function
  | "mu" ->
      Some (string_of_float p.mu)
  | _ ->
      None


type vec = Lac.vec
type mat = Lac.mat


let settings = Probas.Homogen


let equilibrium_probas _ =
  Lac.Vec.make 4 0.25


(* This formula corresponds to mu as the expected number of substitutions
 * per unit time.
 * alpha = 4. /. 3. *. mu *)
let instant_rates ?mat par =
  let mat = Snp.zero_mat mat in
  let r = F.to_float (F.Pos.div (mu par) (F.Pos.of_float 3.)) in
  let q = ~-. (F.to_float (mu par)) in
  L.iteri (fun i _ ->
    let i' = i + 1 in
    L.iteri (fun j _ ->
      let j' = j + 1 in
      if i' = j' then
        mat.{i',j'} <- q
      else
        mat.{i',j'} <- r
    ) Seqs.Dna.nucleotides
  ) Seqs.Dna.nucleotides ;
  mat
 

let transition_probas ?mat par =
  let mat = Snp.zero_mat mat in
  let mu = F.to_float (mu par) in
  let f (dt : _ U.anypos) =
    (* multiply by [4. /. 3.] to follow the convention that
     * mu = 1. means one expected substitution per unit time *)
    let e = exp (~-. 4. /. 3. *. (F.to_float dt) *. mu) in
    let a = 1. /. 4. +. 3. /. 4. *. e in
    let b = 1. /. 4. -. 1. /. 4. *. e in
    (* as an upper sym matrix *)
    for i = 1 to 4 do
      mat.{i,i} <- a ;
      for j = i + 1 to 4 do
        mat.{i,j} <- b
      done
    done ;
    mat
  in f

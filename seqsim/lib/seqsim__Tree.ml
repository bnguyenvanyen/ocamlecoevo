(*
 * Framework :
 * - a timed binary tree with aligned sequences at the tips
 * - a model of evolution
 *
 * We want to compute Felsenstein's likelihood on that tree.
 * First let's consider the case with no indels.
 * For each site :
 * - For each node from the latest one :
 *   - compute the probas that the ancestor site is in the four states
 * - When the root has been reached, compute against prior
 *   of site probabilities
 * Then multiply the probas for each site.
 *)


(* To open the possibility of reducing allocations,
 * optional arguments are present on many functions to pass memory
 * to reuse.
 * Since it's a bit hard to follow, here are codenames to follow
 * what is expected.
 * - a vmem is a vector
 * - a mmem is a matrix
 * - a smem is a vector Probas.dist
 * - a pmem is a matrix Probas.dist
 * - a vstore is a vector M.Int.t
 * - a pstore is a matrix Probas.dist M.Int.t
 * Of course the types are easy to find out anyway.
 * The sizes come after, with n the number of samples
 * - a pstore_4n is a matrix Probas.dist M.Int.t where each matrix
 *   element has dimensions 4 x n.
 * - etc...
 *)


open Sig

module S = BatSet
module M = BatMap


(** A four element vector giving the probabilities
 *  of the site being in states A T C G *)
type site = Lac.Vec.t


module Ntm = Base.Ntm


module type STATE = (Tree.Labeltimed.INPUT with type label = int)


module Make (St : STATE) (T : Tree.Timed.S with type state = St.t) =
  struct
    (* TODO what do we do about indels -> gaps in the sequences ?
     * We'd like to ignore those sites most likely.
     * How are they identified ? *)
  

    (* where k starts at 1 *)
    let nucleotide seqs k x =
      x
      |> St.to_label
      |> (fun i -> M.Int.find i seqs)
      |> Seqs.get ~n:(P (I.Pos.pred k))
      |> Seqs.Dna.of_uchar

    (* When all sites have the same transition rates,
     * then we can compute the transition probabilities only once
     * for each branch,
     * then reuse them for every site *)

    (* transition_matrix should return upper symmetric matrices *)
    let homogen_trans_mat transition_matrix tree =
      let rec f prev_t =
        function
        | Tree.Leaf x ->
            let t = St.to_time x in
            let mat = transition_matrix (t -. prev_t) in
            Tree.Base.leaf (mat, x)
        | Tree.Node (x, stree) ->
            Tree.Base.node (Lac.Mat.empty, x) (f prev_t stree)
        | Tree.Binode (x, ltree, rtree) ->
            let t = St.to_time x in
            let mat = transition_matrix (t -. prev_t) in
            Tree.Base.binode (mat, x) (f t ltree) (f t rtree)
      in
      match tree with
      | Tree.Leaf _ | Tree.Binode _ ->
          invalid_arg "unrooted tree"
      | Tree.Node (x, stree) ->
          Tree.Base.node (Lac.Mat.empty, x) (f (St.to_time x) stree)
   
    (* compute the likelihood for a tree with homogen site likelihood,
     * from the trans mat tree built by homogen_trans_mat.
     * to_probas gives the vector of probability for a fixed site *)
    let homogen_site_likelihood seqs k tree =
      let nuc = nucleotide seqs k in
      let rec f =
        function
        | Tree.Leaf (trans, x) ->
            let p = Probas.of_nucleotide (nuc x) in
            Lac.symv trans p
        | Tree.Node (_, stree) ->
            f stree
        | Tree.Binode ((trans, _), ltree, rtree) ->
            let lprobas = f ltree in
            let rprobas = f rtree in
            let v = Lac.Vec.mul lprobas rprobas in
            Lac.symv trans v
      in
      match tree with
      | Tree.Leaf _ | Tree.Binode _ ->
          invalid_arg "unrooted tree"
      | Tree.Node (_, stree) ->
          f stree

    (* copy ps into qs for columns is *)
    let ignore_indels stgs is qs ps =
      S.Int.iter (fun j ->
        ignore (Probas.lacpy stgs ~ac:j ~bc:j ~n:1 ~b:ps qs)
      ) is

    let correct_indels stgs lis ris lps rps ps =
      let is = S.Int.inter lis ris in
      let lis_only = S.Int.diff lis is in
      let ris_only = S.Int.diff ris is in
      ignore_indels stgs lis_only rps ps ;
      ignore_indels stgs ris_only lps ps ;
      is

    let alignment_likelihood
      (type u v) (stgs : (u, v) Probas.settings)
      ?pstore_4n ?pmem_4n
      seq_probas
      (transition : (u, v) Probas.transition) tree : (v * _) =
      let rec f prev_t store tree =
        let leaf x =
          let t = St.to_time x in
          let k = St.to_label x in
          let mem = Probas.Mat.zero stgs ?store k in
          let ps, indels = M.Int.find k seq_probas in
          let ps = transition.m ?mem ~dt:(t -. prev_t) ps in
          let store =
            Option.map (M.Int.update k k (ps, indels)) store
          in
          (ps, indels, store)
        in
        (* skip over node *)
        let node _ stree =
          f prev_t store stree
        in
        let binode x ltree rtree =
          let t = St.to_time x in
          let lprobas, lindels, store = f t store ltree in
          let rprobas, rindels, store = f t store rtree in
          let ps = Probas.Mat.mul stgs ?c:pmem_4n lprobas rprobas in
          let indels = correct_indels
            stgs
            lindels rindels
            lprobas rprobas
            ps
          in
          let k = St.to_label x in
          let mem = Probas.Mat.zero stgs ?store k in
          let ps = transition.p ?mem ~dt:(t -. prev_t) ps in
          let store =
            Option.map (M.Int.update k k (ps, indels)) store
          in
          (ps, indels, store)
        in
        T.case ~leaf ~node ~binode tree
      in
      T.case
        ~leaf:(fun _ -> invalid_arg "unrooted tree")
        ~binode:(fun _ _ _ -> invalid_arg "unrooted tree")
        ~node:(fun x stree ->
          let ps, is, store = f (St.to_time x) pstore_4n stree in
          (* no site is invariant indel *)
          assert (is = S.Int.empty) ;
          (* don't copy to avoid allocations : be careful *)
          (ps, store)
        )
        tree

    let alignment_likelihood_least_work
      (type u v) (stgs : (u, v) Probas.settings)
      ?pmem_4n
      ~pstore_4n
      ~changed_nodes
      seq_probas
      (transition : (u, v) Probas.transition) tree : (v * _) =
      (* [parent_changed] says whether the parent has changed position,
       * since the previous taversal
       * [parent_t] is the time of the parent
       * [store] stores sequence probability distributions for nodes
       *)
      let rec f parent_changed parent_t store tree =
        let leaf x =
          if parent_changed then
            let t = St.to_time x in
            let k = St.to_label x in
            (* if need to recompute, fresh matrix *)
            let a, _ = M.Int.find k store in
            let mem = Probas.Mat.make0_like stgs a in
            let ps, indels = M.Int.find k seq_probas in
            let ps = transition.m ~mem ~dt:(t -. parent_t) ps in
            let store = M.Int.update k k (ps, indels) store in
            (`Changed, ps, indels, store)
          else
            (* the only way probas change over a leaf,
             * is if the parent position changes *)
            let k = St.to_label x in
            let ps, indels = M.Int.find k store in
            (`Unchanged, ps, indels, store)
        in
        (* skip over node *)
        let node _ stree =
          f parent_changed parent_t store stree
        in
        let binode x ltree rtree =
          let t = St.to_time x in
          let k = St.to_label x in
          let changed = S.Int.mem k changed_nodes in
          let lchanged, lprobas, lindels, store =
            f changed t store ltree
          in
          let rchanged, rprobas, rindels, store =
            f changed t store rtree
          in
          (* TODO Can I recompute less ? *)
          match parent_changed, changed, lchanged, rchanged with
          | false, false, `Unchanged, `Unchanged ->
              let ps, indels = M.Int.find k store in
              (`Unchanged, ps, indels, store)
          | true, _, _, _
          | _, true, _, _
          | _, _, `Changed, _
          | _, _, _, `Changed ->
              (* something has changed *)
              let ps = Probas.Mat.mul stgs ?c:pmem_4n lprobas rprobas in
              let indels = correct_indels
                stgs
                lindels rindels
                lprobas rprobas
                ps
              in
              (* fresh matrix *)
              let a, _ = M.Int.find k store in
              let mem = Probas.Mat.make0_like stgs a in
              let ps = transition.p ~mem ~dt:(t -. parent_t) ps in
              let store = M.Int.update k k (ps, indels) store in
              (`Changed, ps, indels, store)
        in
        T.case ~leaf ~node ~binode tree
      in
      T.case
        ~leaf:(fun _ -> invalid_arg "unrooted tree")
        ~binode:(fun _ _ _ -> invalid_arg "unrooted tree")
        ~node:(fun x stree ->
          let _, ps, is, store = f true (St.to_time x) pstore_4n stree in
          (* no invariant indel *)
          assert (is = S.Int.empty) ;
          (* don't copy to avoid allocations : be careful *)
          (ps, store)
        )
        tree

    (* kept for testing purposes *)
    let site_likelihood k seqs trans_mat tree =
      let nuc = nucleotide seqs k in
      let rec f prev_t =
        function
        | Tree.Leaf x ->
            (* a dim 4 vec with three 0 and one 1 *)
            let p = Probas.of_nucleotide (nuc x) in
            let t = St.to_time x in
            Lac.symv (trans_mat  (t -. prev_t)) p
        | Tree.Node (_, stree) ->
            (* ignore node *)
            f prev_t stree
        | Tree.Binode (x, ltree, rtree) ->
            let t = St.to_time x in
            let lprobas = f t ltree in
            let rprobas = f t rtree in
            let m = trans_mat (t -. prev_t) in
            (* breaks if indels *)
            let p = Lac.Vec.mul lprobas rprobas in
            Lac.symv m p
      in
      match tree with
      | Tree.Leaf _ | Tree.Binode _ ->
          invalid_arg "unrooted tree"
      | Tree.Node (x, stree) ->
          f (St.to_time x) stree

    let invariant_site_likelihood
      (type u v) (stgs : (u, v) Probas.settings)
      ?sstore_4 ?smem_4
      nt (transition : (u, v) Probas.transition) tree : u =
      let p = Probas.of_nucleotide nt in
      let rec f prev_t tree =
        let leaf x =
          let t = St.to_time x in
          let k = St.to_label x in
          let mem = Option.map (M.Int.find k) sstore_4 in
          transition.v ?mem ~dt:(t -. prev_t) p
        in
        let node _ stree =
          (* ignore node *)
          f prev_t stree
        in
        let binode x ltree rtree =
          let t = St.to_time x in
          let k = St.to_label x in
          let lprobas = f t ltree in
          let rprobas = f t rtree in
          let p = Probas.Vec.mul stgs ?z:smem_4 lprobas rprobas in
          let mem = Option.map (M.Int.find k) sstore_4 in
          transition.s ?mem ~dt:(t -. prev_t) p
        in
        T.case ~leaf ~node ~binode tree
      in
      T.case
        ~leaf:(fun _ -> invalid_arg "unrooted tree")
        ~binode:(fun _ _ _ -> invalid_arg "unrooted tree")
        ~node:(fun x stree ->
          f (St.to_time x) stree
        )
        tree

    let nsites kseqs =
      Option.value ~default:I.zero (
       M.Int.fold (fun _ seq ->
        function
        | None ->
            Some (Seqs.length seq)
        | (Some n) as len ->
            let n' = Seqs.length seq in
            if not I.Op.(n = n') then
              failwith "unaligned sequences"
            else
              len
       ) kseqs None
      )

    (* kept for testing purposes *)
    let log_likelihood_sites eq kseqs trans_mat tree =
      (* check same lengths *)
      let tree = T.basic tree in
      let len = nsites kseqs in
      let tree' = homogen_trans_mat trans_mat tree in
      (* fold over sites *)
      I.Pos.fold ~f:(fun loglik k ->
        let lik = homogen_site_likelihood kseqs k tree' in
        let post = Lac.dot eq lik in
        loglik +. log post
      ) ~x:0. ~n:len

    let root_probas
      (type u v) (stgs : (u, v) Probas.settings)
      ?pmem_4n (store : (v * _) M.Int.t) tree : v =
      let get_probas x =
        let ps, _ = M.Int.find (St.to_label x) store in
        ps
      in
      let probas : v = T.case
        ~leaf:(fun _ -> invalid_arg "unrooted tree")
        ~binode:(fun _ _ _ -> invalid_arg "unrooted tree")
        ~node:(fun _ stree ->
          (* look under the root *)
          T.case
            ~leaf:get_probas
            ~node:(fun x _ -> get_probas x)
            ~binode:(fun x _ _ -> get_probas x)
            stree
        )
        tree
      in
      Probas.lacpy ?b:pmem_4n stgs probas

    let log_likelihood_from_root
      (type u v) (stgs : (u, v) Probas.settings)
      ?pmem_1n ?mmem_1n eq (probas : v) =
      probas
      (* multiply by equilibrium frequency *)
      |> Probas.gevp ?c:pmem_1n stgs eq
      (* homogeneize before summing over sites for better precision *)
      |> Probas.homogeneize stgs ?b:mmem_1n
      (* a 1 x n matrix of likelihood by site *)
      |> Lac.Mat.log ?b:mmem_1n
      (* sites that have loglik neg_infinity count for -1000 instead *)
      |> Lac.Mat.map ?b:mmem_1n (fun x -> max (~-.1000.) x)
      (* sum over sites *)
      |> Lac.Mat.sum

    (* the function to use in practice, combined with the invariant one *)
    let log_likelihood_variant
      (type u v) (stgs : (u, v) Probas.settings)
      ?pstore_4n ?pmem_4n ?pmem_1n ?mmem_1n
      seq_probas eq trans_mat tree =
      let probas, _ = alignment_likelihood
        stgs
        ?pstore_4n
        ?pmem_4n
        seq_probas
        trans_mat
        tree
      in
      log_likelihood_from_root ?pmem_1n ?mmem_1n stgs eq probas

    (* Here the tree does not carry sequence information *)
    let log_likelihood_invariant
      (type u v) (stgs : (u, v) Probas.settings)
      ?sstore_4 ?smem_4
      invs eq trans_mat tree =
      Ntm.fold (fun nt count loglik ->
        let lik = invariant_site_likelihood
          stgs
          ?sstore_4
          ?smem_4
          nt
          trans_mat
          tree
        in
        let p = Probas.dotvs stgs eq lik in
        loglik +. (float_of_int count) *. log p
      ) invs 0.

    let log_likelihood_full
      (type u v) (stgs : (u, v) Probas.settings)
      ?pstore_4n ?sstore_4 ?pmem_4n ?smem_4 ~nvsites
      seq_probas invs eq trans_mat tree =
      let logd =
        if nvsites = 0 then
          0.
        else
          log_likelihood_variant
            stgs
            ?pstore_4n
            ?pmem_4n
            seq_probas
            eq
            trans_mat
            tree
      in
      let logd' = log_likelihood_invariant
        stgs
        ?sstore_4
        ?smem_4
        invs
        eq
        trans_mat
        tree
      in
      logd +. logd'
  end

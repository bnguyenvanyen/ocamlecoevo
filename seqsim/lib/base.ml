open Sig


module M = BatMap.Int

module Ntm = BatMap.Make (struct
  type t = Seqs.Dna.t
  let compare = compare
end)


let split_invariant kseqs =
  let ks, seqs =
    kseqs
    |> M.enum
    |> BatEnum.uncombine
  in
  let sites =
    seqs
    |> BatEnum.map BatText.explode
    |> L.of_enum
    |> L.transpose
  in
  let invariants, variants = L.partition (fun samples ->
    let exception Break in
    match 
      L.reduce (fun site site' ->
        if site = site' then
          site
        else
          raise Break
      ) samples
    with
    | _ ->
        true
    | exception Break ->
        false
  ) sites
  in
  let invs = L.fold_left (fun m samples ->
    match samples with
    | [] ->
        m
    | uc :: _ ->
        Ntm.modify_def 0 (Seqs.Dna.of_uchar uc) (fun n -> n + 1) m
  ) Ntm.empty invariants
  in
  let kseqs' =
    variants
    |> L.transpose
    |> L.enum
    |> BatEnum.map BatText.implode
    |> BatEnum.combine ks
    |> M.of_enum
  in
  (invs, kseqs')

open Sig


module Auto =
  struct
    let record _ snap (z, snaps) = (z, snap :: snaps)
  end


module Timedep =
  struct
    let record _ = Auto.record
  end


type channels = {
  par : out_channel ;
  traj : out_channel ;
  seq : out_channel ;
  newick : out_channel ;
  tnewick : out_channel ;
}


module Out =
  struct
    module TGI = Tree.Genealogy.Intbl

    let open_out path = {
      par = open_out (Printf.sprintf "%s.par.csv" path) ;
      traj = open_out (Printf.sprintf "%s.traj.csv" path) ;
      seq = open_out (Printf.sprintf "%s.data.sequences.fasta" path) ;
      newick = open_out (Printf.sprintf "%s.data.trees.newick" path) ;
      tnewick = open_out (Printf.sprintf "%s.data.trees.tnewick" path) ;
    }

    let specl = [
      ("-out", La.String (fun _ s -> open_out s),
       "File basename to output to.") ;
    ]

    let default = {
      par = stdout ;
      traj = stdout ;
      seq = stdout ;
      newick = stdout ;
      tnewick = stdout ;
    }

    let output_seqs chans record =
      (* sequences output *)
      let itseq (i, (seq, sdl)) =
        match sdl with
        | (t, Seqs.Sig.Not) :: _ ->
            (i, t, seq)
        | [] ->
            invalid_arg "Seqpop.output_seqs : empty"
        | _ :: _ ->
            invalid_arg "Seqpop.output_seqs : invalid record"
      in
      let iseqs = L.map itseq (TGI.to_list record) in
      Seqs.Io.stamped_fasta_of_assoc chans.seq iseqs

    let output_trees_from_genealogy chans genealogy =
      let forest = Tree.Genealogy.to_forest genealogy in
      let f_tree i tree =
        (* newick *)
        Printf.fprintf chans.newick "#tree %i\n" i ;
        tree
        |> Tree.Lt.drop_unary
        |> Tree.Lt.output_lengthed_newick chans.newick
        ;
        output_string chans.newick "\n\n" ;
        (* tnewick *)
        Printf.fprintf chans.tnewick "#tree %i\n" i ;
        tree
        |> Tree.Lt.drop_unary
        |> Tree.Lt.output_newick chans.tnewick
        ;
        output_string chans.tnewick "\n\n"
      in
      L.iteri f_tree forest

    let close_par chans =
      close_out chans.par

    let close_csv chans =
      close_out chans.traj

    let close_seq chans =
      close_out chans.seq

    let close_tree chans =
      close_out chans.newick ;
      close_out chans.tnewick

    let close chans =
      close_par chans ;
      close_csv chans ;
      close_seq chans ;
      close_tree chans
  end

open Sig


type t = {
  mu : float ; (* max SNP rate *)
  relmu_a : float ; (* mu_snp * relmu_a is mutation rate of As *)
  relmu_t : float ; (* mu_snp * relmu_t is mutation rate of Ts *)
  relmu_c : float ; (* mu_snp * relmu_c is mutation rate of Cs *)
  relmu_g : float ; (* mu_snp * relmu_g is mutation rate of Gs *)
  snproba : float array array ;
  (* [| [| 0. ; A->T ; A->C ; A->G |] ;
        [| T->A ; 0. ; T->C ; T->G |] ;
        [| C->A ; C->T ; 0. ; C->G |] ;
        [| G->A ; G->T ; G->C ; 0. |] |] *)
}

let load_snproba_csv s =
  let csv = Csv.load s in
  let sa = Csv.to_array csv in
  A.map (fun col -> A.map (fun s -> float_of_string s) col) sa


let with_mu par x = { par with mu = x }
let with_relmu_a par x = { par with relmu_a = x }
let with_relmu_t par x = { par with relmu_t = x }
let with_relmu_c par x = { par with relmu_c = x }
let with_relmu_g par x = { par with relmu_g = x }


let get_proba snp snp' par =
  par.snproba.(Seqs.Dna.to_int snp).(Seqs.Dna.to_int snp')


let set_proba snp snp' x par =
  par.snproba.(Seqs.Dna.to_int snp).(Seqs.Dna.to_int snp') <- x ;
  par
  

(* can we mutate those values or is that a problem ? *)
let with_p_at par x = (par.snproba.(0).(1) <- x ; par)
let with_p_ac par x = (par.snproba.(0).(2) <- x ; par)
let with_p_ag par x = (par.snproba.(0).(3) <- x ; par)
let with_p_ta par x = (par.snproba.(1).(0) <- x ; par)
let with_p_tc par x = (par.snproba.(1).(2) <- x ; par)
let with_p_tg par x = (par.snproba.(1).(3) <- x ; par)
let with_p_ca par x = (par.snproba.(2).(0) <- x ; par)
let with_p_ct par x = (par.snproba.(2).(1) <- x ; par)
let with_p_cg par x = (par.snproba.(2).(3) <- x ; par)
let with_p_ga par x = (par.snproba.(3).(0) <- x ; par)
let with_p_gt par x = (par.snproba.(3).(1) <- x ; par)
let with_p_gc par x = (par.snproba.(3).(2) <- x ; par)


module Term =
  struct
    let opt_float get set doc name =
      U.Arg.Update.Capture.opt_config get set Cmdliner.Arg.float ~doc name

    let mu_subst =
      let doc =
        "Nucleotide base substitution rate (SNP)."
      in
      let get par = par.mu in
      let set x par = { par with mu = x } in
      opt_float get set doc "mu"

    let mu_rel_a =
      let doc =
        "A relative mutation rate."
      in
      let get par = par.relmu_a in
      let set x par = { par with relmu_a = x } in
      opt_float get set doc "rA"

    let mu_rel_t =
      let doc =
        "T relative mutation rate."
      in
      let get par = par.relmu_t in
      let set x par = { par with relmu_t = x } in
      opt_float get set doc "rT"
   
    let mu_rel_c =
      let doc =
        "C relative mutation rate."
      in
      let get par = par.relmu_c in
      let set x par = { par with relmu_c = x } in
      opt_float get set doc "rC"

    let mu_rel_g =
      let doc =
        "Relative rate of substitutions for G."
      in
      let get par = par.relmu_g in
      let set x par = { par with relmu_g = x } in
      opt_float get set doc "rG"

    let mu_proba_at =
      let doc =
        "Probability of A->T."
      in
      let get par = get_proba A T par in
      let set x par = set_proba A T x par in
      opt_float get set doc "pAT"

    let mu_proba_ac =
      let doc =
        "Probability of A->C."
      in
      let get par = get_proba A C par in
      let set x par = set_proba A C x par in
      opt_float get set doc "pAC"

    let mu_proba_ag =
      let doc =
        "Probability of A->G."
      in
      let get par = get_proba A G par in
      let set x par = set_proba A G x par in
      opt_float get set doc "pAG"

    let mu_proba_ta =
      let doc =
        "Probability of T->A."
      in
      let get par = get_proba T A par in
      let set x par = set_proba T A x par in
      opt_float get set doc "pTA"

    let mu_proba_tc =
      let doc =
        "Probability of T->C."
      in
      let get par = get_proba T C par in
      let set x par = set_proba T C x par in
      opt_float get set doc "pTC"

    let mu_proba_tg =
      let doc =
        "Probability of T->G."
      in
      let get par = get_proba T G par in
      let set x par = set_proba T G x par in
      opt_float get set doc "pTG"

    let mu_proba_ca =
      let doc =
        "Probability of C->A."
      in
      let get par = get_proba C A par in
      let set x par = set_proba C A x par in
      opt_float get set doc "pCA"

    let mu_proba_ct =
      let doc =
        "Probability of C->T."
      in
      let get par = get_proba C T par in
      let set x par = set_proba C T x par in
      opt_float get set doc "pCT"

    let mu_proba_cg =
      let doc =
        "Probability of C->G."
      in
      let get par = get_proba C G par in
      let set x par = set_proba C G x par in
      opt_float get set doc "pCG"

    let mu_proba_ga =
      let doc =
        "Probability of G->A."
      in
      let get par = get_proba G A par in
      let set x par = set_proba G A x par in
      opt_float get set doc "pGA"

    let mu_proba_gt =
      let doc =
        "Probability of G->T."
      in
      let get par = get_proba G T par in
      let set x par = set_proba G T x par in
      opt_float get set doc "pGT"

    let mu_proba_gc =
      let doc =
        "Probability of G->C."
      in
      let get par = get_proba G C par in
      let set x par = set_proba G C x par in
      opt_float get set doc "pGC"
  end


let update_term =
  let f
    mu
    rel_a rel_t rel_c rel_g
    p_at p_ac p_ag
    p_ta p_tc p_tg
    p_ca p_ct p_cg
    p_ga p_gt p_gc
    ~config par =
    par
    |> mu ~config
    |> rel_a ~config
    |> rel_t ~config
    |> rel_c ~config
    |> rel_g ~config
    |> p_at ~config
    |> p_ac ~config
    |> p_ag ~config
    |> p_ta ~config
    |> p_tc ~config
    |> p_tg ~config
    |> p_ca ~config
    |> p_ct ~config
    |> p_cg ~config
    |> p_ga ~config
    |> p_gt ~config
    |> p_gc ~config
  in Cmdliner.Term.(
    const f
    $ Term.mu_subst
    $ Term.mu_rel_a
    $ Term.mu_rel_t
    $ Term.mu_rel_c
    $ Term.mu_rel_g
    $ Term.mu_proba_at
    $ Term.mu_proba_ac
    $ Term.mu_proba_ag
    $ Term.mu_proba_ta
    $ Term.mu_proba_tc
    $ Term.mu_proba_tg
    $ Term.mu_proba_ca
    $ Term.mu_proba_ct
    $ Term.mu_proba_cg
    $ Term.mu_proba_ga
    $ Term.mu_proba_gt
    $ Term.mu_proba_gc
  )


let mu par =
  F.Pos.of_float par.mu


let relmu par nt = Seqs.Sig.(
  let r = match nt with
    | A -> par.relmu_a
    | T -> par.relmu_t
    | C -> par.relmu_c
    | G -> par.relmu_g
  in F.Pos.of_float r
)


let proba par nt nt' =
  F.Proba.of_float (par.snproba.(Seqs.Dna.to_int nt).(Seqs.Dna.to_int nt'))


let rate par nt nt' =
  F.Pos.Op.(mu par * relmu par nt * proba par nt nt')


(* FIXME all the simulation things are now wrong *)
let max_relmu par =
  L.fold_left (fun x nt ->
    F.Pos.max x (relmu par nt)
  ) F.zero Seqs.Dna.nucleotides


(* FIXME all the simulation things are now wrong *)
let max_rate par =
  F.Pos.Op.(mu par * max_relmu par)


(* FIXME all the simulation things are now wrong *)
(* repetitive in max_relmu computations *)
let erase_proba par =
  let relmax = max_relmu par in
  (fun nt ->
    F.Proba.compl (F.Proba.of_pos F.Pos.Op.(
      relmu par nt / relmax
    ))
  )
    

(* FIXME all the simulation things are now wrong *)
let mutate par rng nt =
  let i = Seqs.Dna.to_int nt in
  let probas = L.map (fun p -> F.Proba.of_float p) (A.to_list par.snproba.(i)) in
  let choices = L.combine Seqs.Dna.nucleotides probas in
  Util.rand_choose ~rng choices


(* FIXME all the simulation things are now wrong *)
let modif par =
  let ep = erase_proba par in
  Sim.Ctmjp.Util.erase_auto ep (mutate par)


let output_par chan par =
  Printf.fprintf chan
  "Point mutations' parameter values :\n
   mu=%f ; relmu_a=%f ; relmu_t=%f ; relmu_c=%f ; relmu_g=%f ;\n"
  par.mu par.relmu_a par.relmu_t par.relmu_c par.relmu_g ;
  Printf.fprintf chan
  "snproba=[|\n
     [| 0. ; %.2f ; %.2f ; %.2f |] ;\n
     [| %.2f ; 0. ; %.2f ; %.2f |] ;\n
     [| %.2f ; %.2f ; 0. ; %.2f |] ;\n
     [| %.2f ; %.2f ; %.2f ; 0. |] ;\n
   |]\n"
  par.snproba.(0).(1) par.snproba.(0).(2) par.snproba.(0).(3)
  par.snproba.(1).(0) par.snproba.(1).(2) par.snproba.(1).(3)
  par.snproba.(2).(0) par.snproba.(2).(1) par.snproba.(2).(3)
  par.snproba.(3).(0) par.snproba.(3).(1) par.snproba.(3).(2)


let out_par_head chan =
  Printf.fprintf chan
  "mu_snp,relmu_a,relmu_t,relmu_c,relmu_g," ;
  Printf.fprintf chan
  "p_at,p_ac,p_ag,p_ta,p_tc,p_tg,p_ca,p_ct,p_cg,p_ga,p_gt,p_gc"

let out_par_values chan par =
  Printf.fprintf chan
  "%f,%f,%f,%f,%f,"
  par.mu par.relmu_a par.relmu_t par.relmu_c par.relmu_g ;
  let a = par.snproba in
  Printf.fprintf chan
  "%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f"
  a.(0).(1) a.(0).(2) a.(0).(3)
  a.(1).(0) a.(1).(2) a.(1).(3)
  a.(2).(0) a.(2).(1) a.(2).(3)
  a.(3).(0) a.(3).(1) a.(3).(2)


let zero_mat =
  function
  | None ->
      Lac.Mat.make0 4 4
  | Some mat ->
      Lac.Mat.fill mat 0. ;
      mat


(* fill in relative rates off the diagonal *)
let set_offdiag rel_rate par mat =
  L.iteri (fun i nt ->
    let i' = i + 1 in
    L.iteri (fun j nt' ->
      let j' = j + 1 in
      if i' <> j' then
        mat.{i',j'} <- F.to_float (rel_rate par nt nt')
    ) Seqs.Dna.nucleotides
  ) Seqs.Dna.nucleotides

(* normalize the off diagonal rates so that the total expected
 * number of substitutions per site per unit time is mu *)
let normalize mu pi par mat =
  let tot = L.fold_lefti (fun x i nt ->
      (* the term on the diagonal is the total relative transition rate
       * from that nucleotide, but negative *)
      let q = F.Neg.neg (F.Neg.of_float mat.{i+1,i+1}) in
      F.Pos.Op.(x + pi par nt * q)
  ) F.zero Seqs.Dna.nucleotides
  in
  let a = F.to_float F.Pos.Op.(mu par / tot) in
  Lac.Mat.scal a mat
  

let compensate_diagonal mat =
  L.iteri (fun i _ ->
    let i' = i + 1 in
    let q = Lac.Mat.sum ~m:1 ~ar:i' mat in
    mat.{i',i'} <- ~-. q
  ) Seqs.Dna.nucleotides


let instant_rates ?mat:_ _par =
  failwith "Not_implemented"

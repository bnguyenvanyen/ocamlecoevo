open Sig

module type INPUT = With_heterogen.INPUT

module type S = With_heterogen.S


(* for now I expose the type *)
type 'a t = {
  sub : 'a ;
  p_inv : U.anyproba ;
}


module Make (X : INPUT) :
  (S with type sub = X.t
      and type t = X.t t) =
  struct
    type sub = X.t

    type nonrec t = sub t

    let default = {
      sub = X.default ;
      p_inv = F.zero ;
    }

    let mu th =
      X.mu th.sub

    let with_mu mu th =
      { th with sub = X.with_mu mu th.sub }

    module Term =
      struct
        let sub =
          let set sub p = { p with sub } in
          let f read ~config =
            U.Arg.Update.Capture.update_always
              set
              (read ~config)
          in
          Cmdliner.Term.(const f $ X.capture_term)

        let p_inv =
          let doc =
            "Proportion of invariant sites."
          in
          let get p =
            F.to_float p.p_inv
          in
          let set p_inv p =
            { p with p_inv = F.Proba.of_float p_inv }
          in
          U.Arg.Update.Capture.opt_config
            get
            set
            Cmdliner.Arg.float
            ~doc
            "p-inv"
      end

    let capture_term =
      let f sub p_inv ~config =
        default
        |> U.Arg.Capture.empty
        |> sub ~config
        |> p_inv ~config
      in
      Cmdliner.Term.(
        const f
        $ Term.sub
        $ Term.p_inv
      )

    let extract th =
      function
      | "p-inv" ->
          Some (F.to_string th.p_inv)
      | s ->
          X.extract th.sub s

    let dist th =
      [
       (X.with_mu F.zero th.sub, F.to_float th.p_inv) ;
       (th.sub, F.to_float (F.Proba.compl th.p_inv)) ;
      ]

    (* not suitable 
     * module H = With_heterogen.Make (X) *)

    type vec = Lac.vec Probas.dist
    type mat = Lac.mat Probas.dist

    let settings = Probas.Heterogen

    let transition_invariant mat =
      let mat = Snp.zero_mat mat in
      for i = 1 to 4 do
        mat.{i,i} <- 1.
      done ;
      mat

    let transition_probas ?(mat : mat option) th :
      (_ U.anypos -> mat) =
      let p = F.to_float th.p_inv in
      let q = F.to_float (F.Proba.compl th.p_inv) in
      let reweigh =
        L.map (fun (m, p') -> (m, p' *. q))
      in
      (* we need to rescale mu to take into account p_inv,
         so that the expected rate of substitutions is still mu *)
      let sub_transition_probas ?mat th =
        let mu' = F.Pos.div (X.mu th.sub) (F.Proba.compl th.p_inv) in
        let sub' = X.with_mu mu' th.sub in
        X.transition_probas ?mat sub'
      in
      match X.settings with
      | Homogen ->
          begin match mat with
          | None ->
              let trans_inv = transition_invariant None in
              let trans_x = sub_transition_probas th in
              (fun dt -> [
                (trans_inv, p) ;
                (trans_x dt, q) ;
              ])
          | Some [ (mat1, _) ; (mat2, _) ] ->
              let trans_inv = transition_invariant (Some mat1) in
              let trans_x = sub_transition_probas ~mat:mat2 th in
              (fun dt -> [
                (trans_inv, p) ;
                (trans_x dt, q) ;
              ])
          | Some _ ->
              failwith "mat not of length 2"
          end
      | Heterogen ->
          begin match mat with
          | None ->
              let trans_inv = transition_invariant None in
              let trans_x = sub_transition_probas th in
              (fun dt ->
                   (trans_inv, p)
                :: (reweigh (trans_x dt))
              )
          | Some ((m, _) :: mat) ->
              let trans_inv = transition_invariant (Some m) in
              let trans_x = sub_transition_probas ~mat th in
              (fun dt ->
                   (trans_inv, p)
                :: (reweigh (trans_x dt))
              )
          | Some _ ->
              failwith "mat of wrong length"
          end
  end

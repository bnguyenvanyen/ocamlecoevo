open Sig


type 'a capture_term =
  'a U.Arg.Capture.t U.Arg.configurable Cmdliner.Term.t


module type INPUT =
  sig
    type t


    val default : t
    val mu : t -> _ U.pos
    val with_mu : _ U.anypos -> t -> t

    val capture_term : t capture_term

    val extract : t -> string -> string option

    type vec
    type mat

    val settings : (vec, mat) Probas.settings

    val transition_probas :
      ?mat:mat ->
      t ->
      _ U.anypos ->
        mat
  end


module type S =
  sig
    include INPUT
      with type vec = Lac.vec Probas.dist
       and type mat = Lac.mat Probas.dist

    type sub  (* better name ? *)

    val dist : t -> sub Probas.dist
  end


module Make (X : INPUT) =
  struct
    type vec = Lac.vec Probas.dist
    type mat = Lac.mat Probas.dist

    let settings = Probas.Heterogen
    
    let transition_probas dist ?(mat : mat option) th :
      (_ U.anypos -> mat) =
      let d = dist th in
      match X.settings with
      | Homogen ->
          let clos = 
            match mat with
            | None ->
                L.map (fun (sub, p) ->
                  (X.transition_probas sub, p)
                ) d
            | Some mat ->
                L.map2 (fun (sub, p) (mat, _) ->
                   (X.transition_probas ~mat sub, p)
                ) d mat
          in
          (fun dt -> L.map (fun (c, p) -> (c dt, p)) clos)
      | Heterogen ->
          (fun dt ->
            match mat with
            | None ->
                L.fold_left (fun ts (sub, p) ->
                   let trans = X.transition_probas sub dt in
                   ts @ (L.map (fun (m, p') -> (m, p *. p')) trans)
                ) [] d
            | Some mat ->
                let n = L.length mat / L.length d in
                let trans, rem = L.fold_left (fun (ts, ms) (sub, p) ->
                  let mat, ms = L.split_at n ms in
                  let trans = X.transition_probas ~mat sub dt in
                  ts @ (L.map (fun (m, p') -> (m, p *. p')) trans),
                  ms
                ) ([], mat) d
                in
                assert (rem = []) ;
                trans
          )
  end



open Sig


type t = {
  mu : float ;
  (** expected number of substitutions per site per unit time *)
  kappa : float ;
  (** relative rate of A <-> G, C <-> T *)
  pia : float ;
  (** equilibrium frequency of A *)
  pit : float ;
  (** equilibrium frequency of T *)
  pic : float ;
  (** equilibrium frequency of C *)
}


(* corresponds to Jc69 *)
let default = {
  mu = 1e-4 ;
  kappa = 1. ;
  pia = 0.25 ;
  pit = 0.25 ;
  pic = 0.25 ;
}


let mu par =
  F.Pos.of_float par.mu


let kappa par =
  F.Pos.of_float par.kappa


let pia par =
  F.Proba.of_float par.pia


let pit par =
  F.Proba.of_float par.pit


let pic par =
  F.Proba.of_float par.pic


let pig par =
  F.Proba.of_anyfloat F.Op.(F.one - pia par - pit par - pic par)


let ftf = F.to_float


let to_gtr par : Gtr.t =
  (* to convert to Gtr, where all rates sum to 1.,
     we say that if
     [rv] is relative rate of transversions,
     [rz] is relative rate of transitions,
     we want to have [8 rv + 4 rz = 2],
     which gives us
     [rv = 1 / (4 + 2 kappa)]
     [rz = kappa / (4 + 2 kappa)]
   *)
  let kap = ftf (kappa par) in
  let rv = 1. /. (4. +. 2. *. kap) in
  let rz = kap *. rv in
  {
    mu = ftf (mu par) ;
    rct = rz ;
    rat = rv ;
    rgt = rv ;
    rac = rv ;
    rcg = rv ;
    pia = ftf (pia par) ;
    pit = ftf (pit par) ;
    pic = ftf (pic par) ;
  }


let to_snp par = Gtr.to_snp @@ to_gtr @@ par


let pi par =
  function
  | A ->
      pia par
  | T ->
      pit par
  | C ->
      pic par
  | G ->
      pig par


let rel_rate par nt nt' =
  let lbd =
    match nt, nt' with
    | A, G | G, A | C, T | T, C ->
        kappa par
    | A, C | C, A | G, T | T, G | A, T | T, A | C, G | G, C ->
        F.one
    | A, A | T, T | C, C | G, G ->
        F.zero
  in
  let pi = pi par nt' in
  F.Pos.Op.(lbd * pi)


(* fill in relative rates off the diagonal *)
let set_rel_rates_offdiag par mat =
  Snp.set_offdiag rel_rate par mat


(* normalize the off diagonal rates so that the total expected
 * number of substitutions per site per unit time is mu *)
let normalize_mu par mat =
  Snp.normalize mu pi par mat


type vec = Lac.vec
type mat = Lac.mat


let settings = Probas.Homogen


let equilibrium_probas par =
  Seqs.Dna.nucleotides
  |> L.map (pi par)
  |> L.map F.to_float
  |> Lac.Vec.of_list


let instant_rates ?mat par =
  let mat = Snp.zero_mat mat in
  (* non diagonal *)
  set_rel_rates_offdiag par mat ;
  (* set up the diagonal *)
  Snp.compensate_diagonal mat ;
  (* normalize for mu *)
  normalize_mu par mat ; 
  mat


let transition_probas ?mat par dt =
  let mu = mu par in
  let kappa = kappa par in
  let pia = pia par in
  let pit = pit par in
  let pic = pic par in
  let pig = pig par in
  (* purines *)
  let pir = F.Pos.Op.(pia + pig) in
  (* pyrimidines *)
  let piy = F.Pos.Op.(pic + pit) in
  (* from wikipedia *)
  let bnu = F.Pos.Op.(mu * dt /
      (F.two * pir * piy
     + F.two * kappa * (pia * pig + pic * pit))
  )
  in
  (* base exponent *)
  let e = F.(exp (neg bnu)) in
  (* minus e plus 1 *)
  let mep1 = F.Op.(F.one - e) in
  (* kappa minus 1 *)
  let km1 = F.Op.(kappa - F.one) in
  let ekr = F.exp F.(Op.(neg (F.one + pir * km1) * bnu)) in
  let eky = F.exp F.(Op.(neg (F.one + piy * km1) * bnu)) in
  let x1 = F.Pos.Op.(piy / pir * e) in
  let x2 = F.Pos.Op.(ekr / pir) in
  let x3 = F.Pos.Op.(pir / piy * e) in
  let x4 = F.Pos.Op.(eky / piy) in
  let proba nt nt' = F.Op.(
    match nt, nt' with
    | A, A ->
        pia + pia * x1 + pig * x2
    | A, G ->
        pig + pig * x1 - pig * x2
    | G, A ->
        pia + pia * x1 - pia * x2
    | G, G ->
        pig + pig * x1 + pia * x2
    | C, C ->
        pic + pic * x3 + pit * x4
    | C, T ->
        pit + pit * x3 - pit * x4
    | T, C ->
        pic + pic * x3 - pic * x4
    | T, T ->
        pit + pit * x3 + pic * x4
    | A, C | G, C ->
        pic * mep1
    | A, T | G, T ->
        pit * mep1
    | C, A | T, A ->
        pia * mep1
    | C, G | T, G ->
        pig * mep1
  )
  in
  let mat = Snp.zero_mat mat in
  for i = 1 to 4 do
    let nt = Seqs.Dna.of_int (i - 1) in
    for j = 1 to 4 do
      let nt' = Seqs.Dna.of_int (j - 1) in
      mat.{i,j} <- F.to_float (proba nt nt')
    done
  done ;
  mat

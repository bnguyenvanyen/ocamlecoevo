open Sig


module La = Lift_arg

(* The GTR parameters are the substitution rate, the relative rates,
   and the equilibrium proportions.
   The substitution rate [mu] should be positive.
   The relative rates [rct rat rgt rac rcg] should be
   between [0.] and [1.].
   The equilibrium proportions [pia pit pic] should be
   between [0.] and [1.].
   [pig = 1. -. pia -. pit -. pic]
   [rag = 1. -. rct -. rat -. rgt -. rac -. rcg]
 *)
type t = {
  mu : float ;
  rct : float ;
  rat : float ;
  rgt : float ;
  rac : float ;
  rcg : float ;
  pia : float ;
  pit : float ;
  pic : float ;
}



(* default corresponds to JC69 with mu = 1e-4 *)
let default = {
  mu = 1e-4 ;
  rct = 1./.6. ;
  rat = 1./.6. ;
  rgt = 1./.6. ;
  rac = 1./.6. ;
  rcg = 1./.6. ;
  pia = 0.25 ;
  pit = 0.25 ;
  pic = 0.25 ;
}


module Term =
  struct
    module C = Cmdliner

    let float_update_config get set ~doc name =
      U.Arg.Update.Capture.opt_config get set C.Arg.float ~doc name

    let mu =
      let doc =
        "Nucleotide base substitution rate (GTR)."
      in
      let get p = p.mu in
      let set mu p = { p with mu } in
      float_update_config get set ~doc "mu"

    let rct =
      let doc =
        "C <-> T relative mutation rate."
      in
      let get p = p.rct in
      let set rct p = { p with rct } in
      float_update_config get set ~doc "rCT"

    let rat =
      let doc =
        "A <-> T relative mutation rate."
      in
      let get p = p.rat in
      let set rat p = { p with rat } in
      float_update_config get set ~doc "rAT"

    let rgt =
      let doc =
        "G <-> T relative mutation rate."
      in
      let get p = p.rgt in
      let set rgt p = { p with rgt } in
      float_update_config get set ~doc "rGT"

    let rac =
      let doc =
        "A <-> C relative mutation rate."
      in
      let get p = p.rac in
      let set rac p = { p with rac } in
      float_update_config get set ~doc "rAC"

    let rcg =
      let doc =
        "C <-> G relative mutation rate."
      in
      let get p = p.rcg in
      let set rcg p = { p with rcg } in
      float_update_config get set ~doc "rCG"

    let pia =
      let doc =
        "A equilibrium frequency. $(docv) <= 1."
      in
      let get p = p.pia in
      let set pia p = { p with pia } in
      float_update_config get set ~doc "piA"

    let pit =
      let doc =
        "T equilibrium frequency. $(docv) <= 1."
      in
      let get p = p.pit in
      let set pit p = { p with pit } in
      float_update_config get set ~doc "piT"

    let pic =
      let doc =
        "C equilibrium frequency. $(docv) <= 1."
      in
      let get p = p.pic in
      let set pic p = { p with pic } in
      float_update_config get set ~doc "piC"

    include Term
  end


let capture_term =
  let f mu rct rat rgt rac rcg pia pit pic ~config =
    default
    |> U.Arg.Capture.empty
    |> mu ~config
    |> rct ~config
    |> rat ~config
    |> rgt ~config
    |> rac ~config
    |> rcg ~config
    |> pia ~config
    |> pit ~config
    |> pic ~config
  in
  Cmdliner.Term.(
    const f
    $ Term.mu
    $ Term.rct
    $ Term.rat
    $ Term.rgt
    $ Term.rac
    $ Term.rcg
    $ Term.pia
    $ Term.pit
    $ Term.pic
  )


let term =
  let f read config capture =
    U.Arg.Capture.output capture (read ~config)
  in
  Cmdliner.Term.(
    const f
    $ capture_term
    $ Term.load_from
    $ Term.capture_to
  )


let specl = [
  ("-mu", La.Float (fun (par : t) x -> { par with mu = x }),
   "SNP base mutation rate.") ;
  ("-rCT", La.Float (fun par x -> { par with rct = x }),
   "C<->T relative mutation rate.") ;
  ("-rAT", La.Float (fun par x -> { par with rat = x }),
   "A<->T relative mutation rate.") ;
  ("-rGT", La.Float (fun par x -> { par with rgt = x }),
   "G<->T relative mutation rate.") ;
  ("-rAC", La.Float (fun par x -> { par with rac = x }),
   "A<->C relative mutation rate.") ;
  ("-rCG", La.Float (fun par x -> { par with rcg = x }),
   "C<->G relative mutation rate.") ;
  ("-piA", La.Float (fun par x -> { par with pia = x }),
   "A equilibrium frequency.") ;
  ("-piT", La.Float (fun par x -> { par with pit = x }),
   "T equilibrium frequency.") ;
  ("-piC", La.Float (fun par x -> { par with pic = x }),
   "C equilibrium frequency.") ;
]


let mu p =
  F.Pos.of_float p.mu


let with_mu mu p =
  { p with mu = F.to_float mu }


let rct p =
  F.Proba.of_float p.rct


let rat p =
  F.Proba.of_float p.rat


let rgt p =
  F.Proba.of_float p.rgt


let rac p =
  F.Proba.of_float p.rac


let rcg p =
  F.Proba.of_float p.rcg


let rag p =
  F.Pos.Op.(rct p + rat p + rgt p + rac p + rcg p)
  |> F.Proba.of_pos
  |> F.Proba.compl


let pia p =
  F.Proba.of_float p.pia


let pit p =
  F.Proba.of_float p.pit


let pic p =
  F.Proba.of_float p.pic


let pig p =
  F.Pos.Op.(pia p + pit p + pic p)
  |> F.Proba.of_pos
  |> F.Proba.compl


let pi p =
  function
  | A ->
      pia p
  | T ->
      pit p
  | C ->
      pic p
  | G ->
      pig p


let rel_rate par nt nt' =
  let lbd =
    match nt, nt' with
    | A, T | T, A ->
        rat par
    | A, C | C, A ->
        rac par
    | A, G | G, A ->
        rag par
    | T, C | C, T ->
        rct par
    | T, G | G, T ->
        rgt par
    | C, G | G, C ->
        rcg par
    | A, A | T, T | C, C | G, G ->
        F.zero
  in
  let pi = pi par nt' in
  F.Pos.Op.(lbd * pi)


let extract p =
  let conv x =
    Some (string_of_float x)
  in
  function
  | "mu" ->
      conv p.mu
  | "rct" ->
      conv p.rct
  | "rat" ->
      conv p.rat
  | "rgt" ->
      conv p.rgt
  | "rac" ->
      conv p.rac
  | "rcg" ->
      conv p.rcg
  | "rag" ->
      conv (1. -. p.rct -. p.rat -. p.rgt -. p.rac -. p.rcg)
  | "pia" ->
      conv p.pia
  | "pit" ->
      conv p.pit
  | "pic" ->
      conv p.pic
  | "pig" ->
      conv (1. -. p.pia -. p.pit -. p.pic)
  | _ ->
      None


let to_snp p =
  let relmu_a = F.to_float F.Pos.Op.(
    rag p * pig p + rat p * pit p + rac p * pic p
  )
  in
  let relmu_c = F.to_float F.Pos.Op.(
    rac p * pia p + rcg p * pig p + rct p * pit p
  )
  in
  let relmu_g = F.to_float F.Pos.Op.(
    rag p * pia p + rcg p * pic p + rgt p * pit p
  )
  in
  let relmu_t = F.to_float F.Pos.Op.(
    rat p * pia p + rct p * pic p + rgt p * pig p
  )
  in
  let rag = F.to_float (rag p) in
  let pig = F.to_float (pig p) in
  let snproba = Array.make_matrix 4 4 0. in
  (* fill in values for A : 0 *)
  snproba.(0).(1) <- p.rat /. relmu_a *. p.pit ; (* T *)
  snproba.(0).(2) <- p.rac /. relmu_a *. p.pic ; (* C *)
  snproba.(0).(3) <- rag /. relmu_a *. pig ; (* G *)
  (* fill in values for T : 1 *)
  snproba.(1).(0) <- p.rat /. relmu_t *. p.pia ; (* A *)
  snproba.(1).(2) <- p.rct /. relmu_t *. p.pic ; (* C *)
  snproba.(1).(3) <- p.rgt /. relmu_t *. pig ; (* G *)
  (* fill in values for C : 2 *)
  snproba.(2).(0) <- p.rac /. relmu_c *. p.pia ; (* A *)
  snproba.(2).(1) <- p.rct /. relmu_c *. p.pit ; (* T *)
  snproba.(2).(3) <- p.rcg /. relmu_c *. pig ; (* G *)
  (* fill in values for G *)
  snproba.(3).(0) <- rag /. relmu_g *. p.pia ; (* A *)
  snproba.(3).(1) <- p.rgt /. relmu_g *. p.pit ; (* T *)
  snproba.(3).(2) <- p.rcg /. relmu_g *. p.pic ; (* C *)
  { Snp.mu = p.mu ; relmu_a ; relmu_t ; relmu_c ; relmu_g ; snproba }


type vec = Lac.vec
type mat = Lac.mat


let settings = Probas.Homogen


let equilibrium_probas p =
  Lac.Vec.of_array [|
    F.to_float (pia p) ;
    F.to_float (pit p) ;
    F.to_float (pic p) ;
    F.to_float (pig p) ;
  |]


let instant_rates ?mat par =
  (* don't use Snp.instant_rates because they seem broken
   * with the new definition of mu *)
  let mat = Snp.zero_mat mat in
  (* non diagonal *)
  Snp.set_offdiag rel_rate par mat ; 
  (* diagonal *)
  Snp.compensate_diagonal mat ;
  (* normalize *)
  Snp.normalize mu pi par mat ;
  mat


let transition_probas ?mat par =
  let q = instant_rates ?mat par in
  U.Mat.gen_linear_flow q

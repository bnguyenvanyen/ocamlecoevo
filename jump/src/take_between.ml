(* Load a CSV file for timeseries data,
 * but only keep the data between time [t0] and time [tf]. *)

let cut_csv s =
  (* drops s's separator, and replaces the end by '.cut.csv' *)
  let n = String.length s in
  let suffix = String.sub s (n - 4) 4 in
  assert (suffix = ".csv") ;
  let s' = String.sub s 0 (n - 4) in
  s' ^ ".cut.csv"

let load_traj chan =
  let from conv row colname =
    conv (Csv.Row.find row colname)
  in
  let read_row row =
    let t = from float_of_string row "t" in
    let rest = List.tl @@ Csv.Row.to_list @@ row in
    (t, rest)
  in
  let csv_chan = Csv.of_channel ~separator:',' ~has_header:true chan in
  let csv = Csv.Rows.input_all csv_chan in
  Csv.Rows.header csv_chan, List.map read_row csv


let output_data chan header traj =
  let sof = string_of_float in
  let csv_chan = Csv.to_channel chan in
  let csv = header :: List.map (fun (t, rest) -> (sof t) :: rest) traj in
  Csv.output_all csv_chan csv


let input_r = ref None
let output_r = ref None
let t0_r = ref 0.
let tf_r = ref infinity

let specl = [
  ("-in", Arg.String (fun s -> input_r := Some s),
   "Path to input. Default stdin.") ;
  ("-out", Arg.String (fun s -> output_r := Some s),
   "Path to output. Default stdout, then if an input filename is given,
    same filename, suffixed by '.cut.csv'.") ;
  ("-t0", Arg.Set_float t0_r,
   "Time to start from.") ;
  ("-tf", Arg.Set_float tf_r,
   "Time to stop at.") ;
]

let anon_fun s = Printf.printf "Ignored anonymous argument : %s" s

let usage_msg = " Take the data from a timeseries between [t0] and [tf]."


let main () =
  begin
    Arg.parse specl anon_fun usage_msg ;
    let chan_in, chan_out =
      match !input_r, !output_r with
      | None, None ->
        stdin, stdout
      | Some s, Some s' ->
        (open_in s), (open_out s')
      | Some s, None ->
        (open_in s), (open_out (cut_csv s))
      | None, Some s' ->
        stdin, (open_out s')
    in
    let t0 = !t0_r in
    let tf = !tf_r in
    let header, traj = load_traj chan_in in
    let cut_traj = Jump.cut_left t0 traj in
    let recut_traj = Jump.cut_right tf cut_traj in
    let gen_traj = List.map (fun (t, rest) ->
      (t -. t0, rest)
    ) recut_traj
    in
    output_data chan_out header gen_traj ;
    close_out chan_out
  end;;

main ()

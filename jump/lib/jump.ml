include Cadlag


module Piecelin = Piecelin

module Cadlag_map = Cadlag_map

module Piecelin_map = Piecelin_map

module Regular = Regular


type 'a cadlag = 'a t


(* swap arguments to get the primitive, pretty much ? *)
let integr c tmax =
  let p = Piecelin.primitive c in
  Piecelin.eval p tmax


let l2_norm c tmax =
  sqrt (integr (Cadlag.Float.sq c) tmax)


let wmean lbd1 c1 lbd2 c2 =
  let module CF = Cadlag.Float in
  CF.scale
    (1. /. (lbd1 +. lbd2))
    (CF.add (CF.scale lbd1 c1) (CF.scale lbd2 c2))


let cwmean c1 c1' c2 c2' =
  let module CF = Cadlag.Float in
  let specdiv x y =
    if y = 0. then 
      (assert (x = 0.) ; 0.)
    else
      x /. y
  in Cadlag.map2
    specdiv
    (CF.add (CF.mul c1 c1') (CF.mul c2 c2'))
    (CF.add c1 c2)

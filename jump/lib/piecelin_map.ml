

module M = BatMap.Float


type t = float M.t


let eval p t =
  let under, y_o, over = M.split t p in
  (* If t_o is Some then we have our value *)
  match y_o with
  | Some y ->
      y
  | None ->
      if (M.is_empty under) || (M.is_empty over) then
        raise Not_found
      else
        let t_pred, y_pred = M.max_binding under in
        let t_succ, y_succ = M.min_binding over in
        let a = (y_succ -. y_pred) /. (t_succ -. t_pred) in
        y_pred +. a *. (t -. t_pred)

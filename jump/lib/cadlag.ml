module L = BatList


(** an association list with ordered jump time keys *)
type 'a t = (float * 'a) list


let cons ((t1 : float), x1) c =
  match c with
  | (t2, _) :: _ ->
      if t1 < t2 then
        (t1, x1) :: c
      else
        invalid_arg (Printf.sprintf "t1 = %f >= t2 = %f\n" t1 t2)
  | [] ->
      (t1, x1) :: []

let empty = []

let of_list l = L.fold_right cons l []
let to_list c = c

let hd = L.hd

let last = L.last

let njumps = L.length

let time (t, _) = t

let value (_, x) = x

let times c =
  let t, _ = L.split c in
  t

let values c =
  let _, v = L.split c in
  v

let t0 c =
  L.hd (times c)

let tf c =
  L.last (times c)

let min c =
  snd (L.hd (L.sort (fun (_, x1) -> fun (_, x2) -> compare x1 x2) c))

let argmin c =
  fst (L.hd (L.sort (fun (_, x1) -> fun (_, x2) -> compare x1 x2) c))

let max c =
  snd (L.hd (L.sort (fun (_, x1) -> fun (_, x2) -> ~- (compare x1 x2)) c))

let argmax c =
  fst (L.hd (L.sort (fun (_, x1) -> fun (_, x2) -> ~- (compare x1 x2)) c))


let split c =
  L.split @@ (L.map (fun (t, (a, b)) -> ((t, a), (t, b)))) @@ c


let translate t = L.map (fun (s, x) -> (s +. t, x))


let base ((t : float), x) c =
  match c with
  | (t', _) :: _ ->
      if (t < t') then
        (t, x) :: c
      else
        c
  | [] ->
      (t, x) :: c


let rebase ((t : float), x) c =
  match c with
  | (t', _) :: _ when t < t' ->
      (t, x) :: c
  | (t', _) :: tc when t = t' ->
      (t, x) :: tc
  | _ :: _ ->
      c
  | [] ->
      (t, x) :: []


(* FIXME I'm not convinced *)
let find_right c t =
  let rec f c =
    match c with
    | [] ->
        []
    | (t1, _) :: _ when t < t1 ->
        c
    | (t1, x1) :: [] when t >= t1 ->
        (t1, x1) :: []
    | (t1, x1) :: (t2, x2) :: tc when (t1 <= t && t < t2) ->
        (t1, x1) :: (t2, x2) :: tc
    | (_, _) :: tc ->  (* t1 < t *)
        f tc
  in f c


let cut_left t c =
  let rec f =
    function
    | [] ->
        invalid_arg "empty cadlag"
    | (t', _) :: _ when t < t' ->
        invalid_arg "cadlag starts after cutting time"
    | (_, x) :: [] -> (* t >= t' *)
        (t, x) :: []
    | (t1, x1) :: (((t2, _) :: _) as tc) ->
        if (t1 <= t) && (t < t2) then
          (t, x1) :: tc
        else (* t2 <= t *)
          f tc
  in f c


let cut_at t c =
  let rec f acc c =
    match c, acc with
    | [], [] ->
        ([], [])
    | [], (_, x) :: _ ->
        (acc, [(t, x)])
    | (t', _) :: _, (_, x) :: _ when t <= t' ->
        (acc, (t, x) :: c)
    | (t', _) :: _, [] when t <= t' ->
        ([], c)
    | (t', x') :: tc, _ -> (* t > t' *)
        f ((t', x') :: acc) tc
  in
  let rev_cut_c, tx = f [] c in
  L.rev rev_cut_c, tx


let cut_right t c =
  let cutl, _ = cut_at t c in
  cutl


let cut_right_after ?(tol=1e-9) t c =
  let rec f acc c =
    match c with
    | [] ->
        acc
    | ((t', _) as head) :: tc ->
        if t' <= t +. tol then  (* the = is the difference with cut_right *)
          f (head :: acc) tc
        else (* t < t' *)
          acc
  in
  let rev_cut_c = f [] c in
  L.rev rev_cut_c


let eval_cut_left t c =
  let cc = cut_left t c in
  match cc with
  | [] ->
      invalid_arg "Jump.eval : not defined at t"
  | (_, x) :: _ ->
      x, cc


let eval c t =
  let x, _ = eval_cut_left t c in
  x


let output to_str chan c =
  L.iter (fun (t, x) ->
    Printf.fprintf chan "(%.3f, %s) " t (to_str x)
  ) c ;
  Printf.fprintf chan "\n"


let sprint_jump_times (c : _ t) =
  match c with
  | [] -> ""
  | (t, _) :: tc ->
    let init = Printf.sprintf "%.2f" t in
    let out s (t', _) = Printf.sprintf "%s;%.2f" s t' in
    L.fold_left out init tc


let sprint_float (c : float t) =
  match c with
  | [] -> ""
  | (t, x) :: tc ->
    let init = Printf.sprintf "(%.2f;%.2f)" t x in
    let out s (t', x') = Printf.sprintf "%s;(%.2f,%.2f)" s t' x' in
    L.fold_left out init tc


let cumul_dist_of_sample xl =
  let m = L.length xl in
  let rec f n l =
    match l with
    | [] -> []
    | x :: tl -> (x, (n + 1)) :: (f (n + 1) tl)
  in
  let c = (0., 0) :: (f 0 (L.sort compare xl)) in
  L.map (fun (t, n) -> (t, float_of_int n /. float_of_int m)) c


let map f c =
  L.map (fun (t, x) -> (t, f x)) c


let for_all f c =
  L.for_all (fun (_, x) -> f x) c


let fold_left f x0 c =
  L.fold_left (fun x (t, y) -> f x t y) x0 c

 
let fold_succ_left f x0 c =
  let rec g x =
    function
     | []
     | _ :: [] ->
         x0
     | (t, y) :: (t', y') :: [] ->
         f x t y t' y'
     | (t, y) :: (((t', y') :: _) as tl) ->
         g (f x t y t' y') tl
  in g x0 c


let reverse ?tf c =
  let yf, rev_c = fold_succ_left (fun (_, rev_c) _ y t' y' ->
      y', ((t', y) :: rev_c)
    ) (value (hd c), []) c
  in
  match tf with
  | Some tf ->
      (tf, yf) :: rev_c
  | None ->
      rev_c


(* FIXME : make it tail recursive *)
let map2 f (c1 : 'a t) (c2 : 'b t) =
  let rec g c3 px1 px2 c1 c2 =
    match c1, c2 with
    | [], [] ->
        c3
    | ((t1, x1) :: tc1), [] ->
        let c3 = (t1, f x1 px2) :: c3 in
        g c3 x1 px2 tc1 c2
    | [], ((t2, x2) :: tc2) ->
        let c3 = (t2, f px1 x2) :: c3 in
        g c3 px1 x2 c1 tc2
    | ((t1, x1) :: tc1), ((t2, x2) :: tc2) when t2 = t1 ->
        let c3 = (t1, f x1 x2) :: c3 in
        g c3 x1 x2 tc1 tc2
    | ((t1, x1) :: tc1), ((t2, _) :: _) when t2 > t1 ->
        let c3 = (t1, f x1 px2) :: c3 in
        g c3 x1 px2 tc1 c2
    | ((_, _) :: _), ((t2, x2) :: tc2) ->  (* t2 < t1 *)
        let c3 = (t2, f px1 x2) :: c3 in
        g c3 px1 x2 c1 tc2
  in
  match c1, c2 with
  | (t1, x1) :: tc1, (t2, x2) :: tc2 when t1 = t2 ->
      L.rev (g [(t1, f x1 x2)] x1 x2 tc1 tc2)
  | [], [] ->
      []
  | (_ :: _, _ :: _) | ([], _ :: _) | (_ :: _, []) ->
      failwith "Jump.map2 : Should start at the same point"


let merge f c1 c2 =
  let rec g px1 px2 c1 c2 =
    match c1, c2 with
    | [], [] ->
        []
    | ((t1, x1) :: tc1), [] ->
        (t1, f false x1 true px2) :: (g x1 px2 tc1 c2)
    | [], ((t2, x2) :: tc2) ->
        (t2, f true px1 false x2) :: (g px1 x2 c1 tc2)
    | ((t1, x1) :: tc1), ((t2, x2) :: tc2) when t2 = t1 ->
        (t1, f false x1 false x2) :: (g x1 x2 tc1 tc2)
    | ((t1, x1) :: tc1), ((t2, _) :: _) when t2 > t1 ->
        (t1, f false x1 true px2) :: (g x1 px2 tc1 c2)
    | ((_, _) :: _), ((t2, x2) :: tc2) ->  (* t2 < t1 *)
        (t2, f true px1 false x2) :: (g px1 x2 c1 tc2)
  in
  match c1, c2 with
  | (t1, x1) :: tc1, (t2, x2) :: tc2 when t1 = t2 ->
      (t1, f false x1 false x2) :: (g x1 x2 tc1 tc2)
  | [], [] ->
      []
  | (_ :: _, _ :: _) | ([], _ :: _) | (_ :: _, []) ->
      Printf.eprintf "fail :\nc1 : %s\nc2 : %s\n"
      (sprint_jump_times c1)
      (sprint_jump_times c2) ;
      failwith "Jump.merge : Should start at the same point"
 

let rec prune cc =
  match cc with
  | (t, n) :: (_, n') :: tcc when n = n' ->
      prune ((t, n) :: tcc)
  | (t, n) :: tcc ->  (* n <> n' or tcc = [] *)
      (t, n) :: (prune tcc)
  | [] ->
      []


let rec bissect ?(tol=epsilon_float) c p t t' =
  (* we start from knowing that at t' p has become true,
     but that at t < t', it is false.
     We want to find the earliest time at which it becomes true *)
  (* we need a stopping condition on t' - t, if too close should stop *)
  if t' -. t < tol then
    (t, eval c t)
  else
    begin
      (* if not done already, we discard everything before t,
         since we won't need it *)
      let c = cut_left t c in
      let t'' = t +. (t' -. t) /. 2. in
      let x = eval c t'' in
      let b = p t'' x in
      if b then  (* look left *)
        bissect c p t t''
      else  (* look right *)
        bissect c p t'' t'
    end


let inf_bissect c p t =
  (* find last not-infinity jump time *)
  let rc = L.rev c in
  let rec find_max_non_inf rc =
    match rc with
    | (t, x) :: _ when t < infinity ->
        (t, x)
    | _ :: trc ->  (* infinity *)
        find_max_non_inf trc
    | [] ->
        invalid_arg "empty cadlag"
  in
  let max_t, max_x = find_max_non_inf rc in
  let rec find_valid t =
    if p t max_x then t else find_valid (2. *. t)
  in
  let t' = find_valid max_t in
  bissect c p t t'


let regulate c period =
  (* evaluate c every period until p is true then bissect *)
  (* FIXME infinity in c *)
  let rec f acc c t =
    let nt = t +. period in
    match c with
    | (t', _) :: _ when nt < t' ->
        failwith "oops"
    | (_, x') :: (t'', _) :: _ when nt < t'' ->
        let nacc = (nt, x') :: acc in
        f nacc c nt
    | (_, _) :: (t'', x'') :: tc ->  (* nt > t'' *)
        let nc = (t'', x'') :: tc in
        f acc nc t
    | (_, x') :: [] ->
        let nacc = (nt, x') :: acc in
        (nt, nacc)
    | [] ->
        (* should only be reached on init c=[] already matched *)
        failwith "should not be reached (matched already on init)"
  in match c with
  | [] ->
      []
  | (t, x) :: _ ->
      let _, rreg = f [(t, x)] c t in
      L.rev rreg
     

let print p out c =
  L.print
    ~first:"" ~sep:"\n" ~last:""
    (BatTuple.Tuple2.print
      ~first:"" ~sep:": " ~last:""
      BatFloat.print p)
    out
    c



module Int =
  struct
    type nonrec t = int t

    let to_float c = map float_of_int c
    let add c c' = map2 (+) c c'

    let rec longer_leaves margin (c : t) =
      match c with
      | (t1, n1) :: (t2, n2) :: tc when n2 = n1 - 1 ->
          (t1, n1) :: longer_leaves margin ((t2 +. margin, n2) :: tc)
      | (t1, n1) :: tc ->
          (t1, n1) :: longer_leaves margin tc
      | [] ->
          []

    let diff c =
      let rec f =
        function
        | [] ->
            []
        | _ :: [] ->
            []
        | (_, x) :: (t', x') :: tl ->
            (t', (x' - x)) :: f ((t', x') :: tl)
      in f c

    let print out c = print BatInt.print out c
  end


module Float =
  struct
    type nonrec t = float t

    let add c c' = map2 (+.) c c'
    let sub c c' = map2 (-.) c c'
    let mul c c' = map2 ( *. ) c c'
    let div c c' = map2 (/.) c c'
    let scale lbd c = map (fun x -> lbd *. x) c
    let sq c = map (fun x -> x ** 2.) c
    let abs c = map (fun x -> abs_float x) c
    let inferior c1 c2 = for_all (fun x -> x >= 0.) (sub c2 c1)
    let print out c = print BatFloat.print out c
  end


module Pos =
  struct
    module U = Util
    module F = U.Float
    module P = F.Pos

    let areas ?tf c =
      let area x t t' =
        let dt = P.of_float (t' -. t) in
        P.mul dt x
      in
      let rec f =
        function
        | [] ->
            []
        | (t, x) :: [] ->
            begin match tf with
            | None ->
                []
            | Some tf ->
                (tf, area x t tf) :: []
            end
        | (t, x) :: ((t', _) :: _ as tc) ->
            (t', area x t t') :: f tc
      in f c

    let primitive c =
      let rec f (t, (y, a)) =
        function
        | [] ->
            []
        | (t', x') :: tc ->
            let dt = P.of_float (t' -. t) in
            let y' = P.narrow P.Op.(y + dt * a) in
            let tya' = (t', (y', x')) in
            tya' :: f tya' tc
      in
      match c with
      | [] ->
          []
      | (t0, x0) :: tc ->
          let tya0 = (t0, (F.zero, P.narrow x0)) in
          tya0 :: (f tya0 tc)
  end

(** A set implementation for cadlags *)

module L = BatList

module F = Util.Float

module M = BatMap.Make (struct
  type t = Util._float
  let compare = F.compare
end)


type 'a t = 'a M.t


let undefined = M.empty


let map = M.map


let of_map m = m


let of_list l =
  L.fold_left (fun c (t, x) ->
    M.add (F.narrow t) x c
  ) M.empty l


let of_cadlag c =
  c
  |> L.map (fun (t, x) -> (F.of_float t, x))
  |> of_list


let to_cadlag c =
  c
  |> M.bindings
  |> L.map (fun (t, x) -> (F.to_float t, x))
  |> Cadlag.of_list


let to_list c =
  M.bindings c


let add t y c =
  M.add (F.narrow t) y c


let eval c t =
  let under, t_o, _ = M.split (F.narrow t) c in
  match t_o with
  | Some y ->
      y
  | None ->
      if M.is_empty under then
        raise Not_found
      else
        let _, y_pred = M.max_binding under in
        y_pred


(* translate time as y = a * x + b *)
let translate ~a ~b c =
  M.fold (fun t v ->
    M.add F.Op.(a * t + b) v
  ) c M.empty


let reverse ?tf c =
  (* the first time t0 is dropped *)
  let (_t0, v0), c = M.pop_min_binding c in
  let vf, c' = M.fold (fun t v (prev_v, c') ->
    (* each negated time gets the previous value *)
    let c' = M.add (F.neg t) prev_v c' in
    (v, c')
  ) c (v0, M.empty)
  in
  match tf with
  | None ->
      c'
  | Some tf ->
      (* if tf is given, it is associated with vf *)
      M.add (F.neg tf) vf c'


let cut a b c =
  let a = F.narrow a in
  let b = F.narrow b in
  assert F.Op.(a <= b) ;
  let under, ya_o, upper = M.split a c in
  let cut_a =
    match ya_o with
    | None ->
        (* if [c] is not defined in [a], don't cut *)
        begin match M.max_binding under with
        | exception Invalid_argument _ ->
            upper
        | _, ya ->
          M.add a ya upper
        end
    | Some ya ->
        M.add a ya upper
  in
  let cut_b, _, _ = M.split b cut_a in
  cut_b


let fold =
  M.fold


let fold_left f x0 c =
  M.fold (fun t y x -> f x t y) c x0


let fold_succ_left f x0 c =
  match M.pop_min_binding c with
  | (t0, y0), c' ->
      let _, _, xf = M.fold (fun t' y' (t, y, x) ->
          (t', y', f x t y t' y')
        ) c' (t0, y0, x0)
      in
      xf
  | exception Not_found ->
      x0


let map2 f (c1 : 'a t) (c2 : 'b t) =
  (* put all times together *)
  let c = M.merge (fun _ xo yo -> Some (xo, yo)) c1 c2 in
  (* basically map but more annoying (fold_map) *)
  let _, _, c = M.fold (fun t (xo, yo) (x, y, m) ->
    let x =
      match xo with
      | None ->
          x
      | (Some _) as some ->
          some
    in
    let y =
      match yo with
      | None ->
          y
      | (Some _) as some ->
          some
    in
    let z = f x y in
    (x, y, M.add t z m)
  ) c (None, None, M.empty)
  in c


(* integrate [f] against [c] on [[from_t, to_t]] *)
(* TODO it would be nice if [f] was allowed to depend on time *)
let integrate ~from_t ~to_t f c =
  let from_t = F.narrow from_t in
  let to_t = F.narrow to_t in
  assert F.Op.(from_t <= to_t) ;
  let from_y =
    match eval c from_t with
    | exception Not_found ->
        (* if [c] starts after [from_t], we can consider that [f] is zero
         * until [c] is defined *)
        F.zero
    | from_x ->
        f from_x
  in
  let _, _, cut_left = M.split from_t c in
  let range, _, _ = M.split to_t cut_left in
  let tot, last_t, last_y = M.fold (fun t x (tot, t_pred, y_pred) ->
      let dt = F.Pos.of_anyfloat F.Op.(t - t_pred) in
      let tot' = F.Op.(tot + y_pred * dt) in
      (tot', t, f x)
    ) range (F.zero, from_t, from_y)
  in
  let dt = F.Pos.of_anyfloat F.Op.(to_t - last_t) in
  F.Op.(tot + last_y * dt)


let print p out c =
  M.print
    ~first:"" ~sep:"\n" ~kvsep:":" ~last:""
    F.print p
    out
    c

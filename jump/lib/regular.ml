(** Approximate a continuous function
  * by its values recorded at regular intervals
  * (linear interpolation)
  *)

module A = BatArray
module L = BatList


type 'a t = {
  t0 : float ;
  incr : float ;
  values : 'a array ;
}


let of_cadlag ?tol ?dt c =
  (* compute incr *)
  let n = Cadlag.njumps c in
  let times = Cadlag.times c in
  let t0 = L.hd times in
  let incr =
    match dt with
    | None ->
        (* can't guess dt with only one point *)
        assert (n > 1) ;
        let tf = L.last times in
        (tf -. t0) /. float (n - 1)
    | Some dt ->
        dt
  in
  (* check that c is regular (enough) *)
  ignore (L.reduce (fun t t' ->
    assert (Util.nearly_equal ?tol (t' -. t) incr) ;
    t'
  ) times)
  ;
  let values = A.of_list (Cadlag.values c) in
  { t0 ; incr ; values }


(* for k from 0 to n - 1 *)
let time r k =
  r.t0 +. float k *. r.incr


let t0 r = r.t0


let tf r =
  let n = A.length r.values in
  time r (n - 1)


let dt r = r.incr


let length r =
  A.length r.values


let times r =
  let n = length r in
  Util.int_fold ~f:(fun ts k ->
    (time r (k - 1)) :: ts
  ) ~n ~x:[]


let values r =
  A.to_list r.values


let to_list r =
  L.combine (times r) (values r)


let copy r =
  { r with values = A.copy r.values }


let translate t r =
  { r with t0 = r.t0 +. t }


(* k from 0 to n - 1 *)
let nth r k =
  (time r k, r.values.(k))
 

let sub ?(start=0) ?len r =
  let t0 = time r start in
  let len =
    match len with
    | None ->
        length r - start
    | Some n ->
        n
  in
  let values = A.sub r.values start len in
  { r with t0 ; values }


let cut ?(tol=1e-6) ~t0 ~tf r =
  (* t0 and tf should coincide with jump times,
   * otherwise might not be regular anymore.
   * so this is just a convenience wrapper to sub *)
  let of_float x =
    let fr, fk = modf x in
    if 1. -. fr <= tol then
      int_of_float fk + 1
    else if fr <= tol then
      int_of_float fk
    else
      invalid_arg "too far from an integer"
  in
  let k0 = of_float ((t0 -. r.t0) /. r.incr) in
  let kf = of_float ((tf -. r.t0) /. r.incr) in
  sub ~start:k0 ~len:(kf - k0 + 1) r


let append r r' =
  assert (Util.nearly_equal (dt r) (dt r')) ;
  assert (Util.nearly_equal (tf r +. dt r) (t0 r')) ;
  let t0 = t0 r in
  let incr = dt r in
  let values = A.append r.values r'.values in
  { t0 ; incr ; values }


let reverse r =
  { r with values = A.rev r.values }


let neg r =
  let t0 = t0 r in
  let tf = tf r in
  translate (~-. (t0 +. tf)) (reverse r)


let eval r t =
  assert ((t0 r <= t) && (t <= tf r)) ;
  let k = int_of_float ((t -. r.t0) /. r.incr) in
  assert (k >= 0) ;
  let t_pred = time r k in
  if t = t_pred then
    r.values.(k)
  else
    let t_succ = time r (k + 1) in
    let a = (r.values.(k + 1) -. r.values.(k)) /. (t_succ -. t_pred) in
    r.values.(k) +. a *. (t -. t_pred)


let modify_nth k f r =
  let x = r.values.(k) in
  r.values.(k) <- f x


let map f r =
  let values = A.mapi (fun k v ->
      let t = time r k in
      f t v
    ) r.values
  in { r with values }


let fold_left f x0 r =
  A.fold_lefti (fun x k v ->
    let t = time r k in
    f x t v
  ) x0 r.values


let fold_left2 f x0 r r' =
  assert (Util.nearly_equal r.t0 r'.t0) ;
  assert (Util.nearly_equal r.incr r'.incr) ;
  A.fold_lefti (fun x k v ->
    let t = r.t0 +. float k *. r.incr in
    f x t v r'.values.(k)
  ) x0 r.values


(* primitive but with linear interpolation still *)
let primitive r =
  let n = A.length r.values in
  let values = A.make (A.length r.values) 0. in
  for k = 1 to n - 1 do
    let a = r.incr *. (r.values.(k) +. r.values.(k - 1)) /. 2. in
    values.(k) <- values.(k - 1) +. a
  done ;
  { r with values }


let print p out r =
  L.print
    ~first:"" ~sep:"\n" ~last:""
    (BatTuple.Tuple2.print
      ~first:"" ~sep:": " ~last:""
      BatFloat.print p)
    out
    (to_list r)


module Pos =
  struct
    module F = Util.Float

    let eval (r : _ Util.anypos t) t =
      let k = int_of_float ((t -. r.t0) /. r.incr) in
      let kp1 = k + 1 in
      assert (k >= 0) ;
      let n = A.length r.values in
      (* FIXME This is not great but it handles common errors *)
      if k = (n - 1) then begin
        F.Pos.narrow r.values.(n - 1)
      end else
      let t_pred = F.of_float (r.t0 +. float k *. r.incr) in
      if t = F.to_float t_pred then
        F.Pos.narrow r.values.(k)
      else
        let t_succ = F.of_float (r.t0 +. float kp1 *. r.incr) in
        let a = F.Op.(
          (r.values.(kp1) - r.values.(k)) / (t_succ - t_pred)
        )
        in
        F.Pos.of_anyfloat F.Op.(
          r.values.(k) + a * (F.of_float t - t_pred)
        )

    (* TODO make it return true _ Util.pos t *)
    let primitive (r : _ Util.anypos t) =
      let n = A.length r.values in
      let incr = F.Pos.of_float r.incr in
      let values = A.make (A.length r.values) (F.Pos.narrow F.zero) in
      for k = 1 to n - 1 do
        let km1 = k - 1 in
        let a = F.Pos.of_anyfloat F.Op.(
          incr * (r.values.(k) + r.values.(km1)) / F.two
        )
        in
        values.(k) <- F.Pos.add values.(km1) a
      done ;
      { r with values }
  end


(* for a strictly increasing float t, we can take the inverse *)
let inverse (r : float t) =
  (* not very economical *)
  let is_increasing, _ = BatList.fold_left (fun (increasing, s) s' ->
      (increasing && (s < s'), s')
    ) (true, neg_infinity) (values r)
  in
  assert is_increasing ;
  fold_left (fun inv t s ->
    Piecelin_map.M.add s t inv
  ) Piecelin_map.M.empty r

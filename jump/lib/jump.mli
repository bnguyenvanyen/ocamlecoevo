(** Càdlàg processes used to represent statistics of time-embedded trees,
    and of trajectories of Ctmjps in general.
  
    Càdlàg = Continuous on the right and limit on the left,
    that is, X(t+) = X(t) but X(t-) <> X(t).
  
    We represent both piecewise-constant and piecewise linear functions. *)

(** ordered times and values associated *)
type 'a cadlag


(** {1:basics General purpose construction and destruction} *)

(** constructor *)
val cons : (float * 'a) -> 'a cadlag -> 'a cadlag

val empty : 'a cadlag

(** [of_list vals] is the cadlag of the increasing times and values of [vals].
    @raise Assert_failure if not in increasing order
 *)
val of_list : (float * 'a) list -> 'a cadlag

val to_list : 'a cadlag -> (float * 'a) list

(** [hd c] is the first element of [c]. *)
val hd : 'a cadlag -> (float * 'a)

(** [last c] is the last element of [c]. *)
val last : 'a cadlag -> (float * 'a)

(** [reverse ?tf c] is the cadlag reverse of [c] starting at [tf],
    ordered by decreasing times.
    When [tf] is not given, starts at the last point of [c]. *)
val reverse : ?tf:float -> 'a cadlag -> 'a cadlag

val njumps : 'a cadlag -> int

(** [translate t c] is [c] with times translated by [t],
    (new times are old times [+ t]). *)
val translate : float -> 'a cadlag -> 'a cadlag

(** [base (t, x) c] is [c] with [(t, x)] prepended,
    if it comes strictly before [c], otherwise it is [c]. *)
val base : (float * 'a) -> 'a cadlag -> 'a cadlag

(** [rebase (t, x) c] is [c] with [(t, x)] at the beginning,
    unless it comes strictly after the start of [c],
    in which case it is [c].
    The difference with base is on the equal case. *)
val rebase : (float * 'a) -> 'a cadlag -> 'a cadlag

(** [find_right c t] is [c] starting at its last jump before [t]. *)
val find_right : 'a cadlag -> float -> 'a cadlag

(** [cut_left t c] is [c] starting in [t+]. *)
val cut_left : float -> 'a cadlag -> 'a cadlag

(** [cut_at t c] is the couple of the cadlag finishing at [t-]
    and the cadlag starting at [t]. *)
val cut_at : float -> 'a cadlag -> 'a cadlag * 'a cadlag

(** [cut_right t c] is [c] finishing in [t-]. *)
val cut_right : float -> 'a cadlag -> 'a cadlag

(** [cut_right_after ?tol t c] is [c] finishing in [t+]. *)
val cut_right_after : ?tol:float -> float -> 'a cadlag -> 'a cadlag

(** [eval c t] is the value of [c] in [t].
    @raise Invalid_argument if [c] is not defined at [t]. *)
val eval : 'a cadlag -> float -> 'a

val eval_cut_left : float -> 'a cadlag -> 'a * 'a cadlag

(** [times c] are the jump times of [c]. *)
val times : 'a cadlag -> float list

(** [values c] are the values of [c]. *)
val values : 'a cadlag -> 'a list

(** [t0 c] is the first time point of [c]. *)
val t0 : 'a cadlag -> float

(** [tf c] is the last time point of [c]. *)
val tf : 'a cadlag -> float

(** [time] is {!Stdlib.fst} for [float]s. *)
val time : (float * 'a) -> float

(** [value] is {!Stdlib.snd}. *)
val value : ('a * 'b) -> 'b

(** [output to_str chan c] outputs [c] to chan
    with its values converted by [to_str]. *)
val output : ('a -> string) -> out_channel -> 'a cadlag -> unit

(** string representation *)
val sprint_jump_times : 'a cadlag -> string
val sprint_float : float cadlag -> string

(** [cumul_dist_of_sample xl] is the empirical cumulative distribution
    for the sample [xl] (should be positive numbers). *)
val cumul_dist_of_sample : float list -> float cadlag

(** [map f c] is [c] with [f] applied to its values *)
val map : ('a -> 'b) -> 'a cadlag -> 'b cadlag

(** [split c] is like List.split for cadlags. *)
val split : ('a * 'b) cadlag -> 'a cadlag * 'b cadlag

(** [for_all p c] is [true] if [p] is [true] all along [c]. *)
val for_all : ('a -> bool) -> 'a cadlag -> bool

(** [fold_left f x0 c] is [f] folded over values of [c]. *)
val fold_left : ('a -> float -> 'b -> 'a) -> 'a -> 'b cadlag -> 'a

(** [fold_succ_left f x0 c] is [f] folded over successive values of [c]. *)
val fold_succ_left :
  ('a -> float -> 'b -> float -> 'b -> 'a) ->
  'a ->
  'b cadlag ->
    'a

(** [map2 f c1 c2] is the cadlag where [f] has been applied to [c1]
    and [c2] at each time point. *)
val map2 : ('a -> 'b -> 'c) -> 'a cadlag -> 'b cadlag -> 'c cadlag

(** [merge f c1 c2] reproduces the behaviour of [Map.S.merge] for cadlags. *)
val merge : (bool -> 'a -> bool -> 'b -> 'c) -> 'a cadlag -> 'b cadlag -> 'c cadlag

(** [prune c] is [c] with subsequent identical values removed. *)
val prune : 'a cadlag -> 'a cadlag

(** [bissect ~tol c p t t'] is the time [t''] and value [x] at which
    [p t'' x] first becomes [true], with a tolerance [tol],
    if [p t xt] is [false] and [p t' xt'] is [true]. *)
val bissect :
  ?tol : float ->
  'a cadlag ->
  (float -> 'a -> bool) ->
  float ->
  float ->
  float * 'a

(** [inf_bissect c p t] is appropriate when the last point of [c] is at infinity.
    It will look for a smaller [t'] for which [p] is [true],
    then [bissect] from there. *)
val inf_bissect :
  'a cadlag ->
  (float -> 'a -> bool) ->
  float ->
  float * 'a

(** [regulate c dt] is [c] evaluated every [dt]
    until after its last jump. It should not have an infinity time. *)
val regulate : 'a cadlag -> float -> 'a cadlag

(** [min c] is the minimum value of [c]. *)
val min : 'a cadlag -> 'a

(** [argmin c] is the first time when [c] reaches its minimum. *)
val argmin : 'a cadlag -> float

(** [max c] is the maximum value of [c]. *)
val max : 'a cadlag -> 'a

(** [argmax c] is the first time when [c] reaches its maximum. *)
val argmax : 'a cadlag -> float


(** print a cadlag *)
val print :
  ('a BatIO.output -> 'b -> unit) ->
  'a BatIO.output ->
  'b cadlag ->
    unit


(** {1:ints int cadlag functions} *)
module Int :
  sig
    type t = int cadlag

    val to_float : t -> float cadlag

    val add : t -> t -> t

    val longer_leaves : float -> int cadlag -> int cadlag

    (** [diff c] is a cadlag such that,
        if [c] has points [(t(i), n(i))],
        then [int_diff c] has points [(t(i), n(i) - n(i-1))],
        where the first time of [c] is lost. *)
    val diff : int cadlag -> int cadlag

    val print : 'a BatIO.output -> int cadlag -> unit
  end



(** {1:floats float cadlag functions} *)
module Float :
  sig
    type t = float cadlag

    (** [add c1 c2] is the sum of [c1] and [c2]. *)
    val add : float cadlag -> float cadlag -> float cadlag

    (** [sub c1 c2] is [c1 - c2] pointwise. *)
    val sub : float cadlag -> float cadlag -> float cadlag

    (** [mul c1 c2] is [c1 * c2] pointwise. *)
    val mul : float cadlag -> float cadlag -> float cadlag

    (** [div c1 c2] is [c1 / c2] pointwise. *)
    val div : float cadlag -> float cadlag -> float cadlag

    (** [scale lbd c] is [c] scaled by [lbd]. *)
    val scale : float -> float cadlag -> float cadlag

    (** [sq c] is [c] squared. *)
    val sq : float cadlag -> float cadlag

    (** [abs c] is the absolute value of [c]. *)
    val abs : float cadlag -> float cadlag

    (** [inferior c1 c2] is [true] if [c1] is less than [c2] everywhere. *)
    val inferior : float cadlag -> float cadlag -> bool

    val print : 'a BatIO.output -> float cadlag -> unit
  end


(** {1:piecelin Piecewise linear functions} *)
module Piecelin :
  sig
    type t

    (** [eval p t] is the value of the piecewise linear function [p] at [t-]. *)
    val eval : t -> float -> float

    (** [of_cadlag c] is the piecewise linear representation of
        a stair function. So, every slope coefficient is [0.] *)
    val of_cadlag : float cadlag -> t

    (** [primitive c] is the primitive of [c]
        as a piecewise linear function *)
    val primitive : float cadlag -> t

    (** piecewise linear function difference *)
    val sub : t -> t -> t

    (** L2 norm for piecewise linear functions *)
    val l2_norm : t -> float -> float

    (** [regulate pl pred pi] is the time-series of values of [pl]
        every [pi] stopped when [pred] becomes [true]. *)
    val regulate : t -> (float -> float -> bool) -> float -> float cadlag

    (** [diff pl] is the cadlag where the value at [t_i]
        is the value of [pl] at [t_i] minus the value of [pl] at [t_(i-1)] *)
    val diff : t -> float cadlag
  end


module Cadlag_map :
  sig
    module M : BatMap.S with type key = Util._float

    type 'a t

    (** without values *)
    val undefined : 'a t

    val map : ('a -> 'b) -> 'a t -> 'b t

    val map2 : ('a option -> 'b option -> 'c) -> 'a t -> 'b t -> 'c t

    val of_map : 'a M.t -> 'a t

    val of_list : (_ Util.anyfloat * 'a) list -> 'a t

    val of_cadlag : 'a cadlag -> 'a t

    val to_cadlag : 'a t -> 'a cadlag

    val to_list : 'a t -> (Util._float * 'a) list

    val fold : (Util._float -> 'a -> 'b -> 'b) -> 'a t -> 'b -> 'b

    val fold_left : ('b -> Util._float -> 'a -> 'b) -> 'b -> 'a t -> 'b

    val fold_succ_left :
      ('a -> Util._float -> 'b -> Util._float -> 'b -> 'a) ->
      'a ->
      'b t ->
        'a

    val eval : 'a t -> _ Util.anyfloat -> 'a

    val add : _ Util.anyfloat -> 'a -> 'a t -> 'a t

    (** [translate ~a ~b c] is [c] where time becomes [t' = a * t + b] *)
    val translate :
      a:_ Util.anypos ->
      b:_ Util.anyfloat ->
      'a t ->
        'a t

    (** [reverse c] is [c] with the time reversed *)
    val reverse : ?tf:_ Util.anyfloat -> 'a t -> 'a t

    (** [cut a b c] is [c] with bounds [a] and [b].
        [a] should be smaller than [b]. *)
    val cut :
      _ Util.anyfloat ->
      _ Util.anyfloat ->
      'a t ->
        'a t

    val integrate :
      from_t:_ Util.anyfloat ->
      to_t:_ Util.anyfloat ->
      ('a -> Util._float) ->
      'a t ->
        Util._float

    val print :
      ('a BatIO.output -> 'b -> unit) ->
      'a BatIO.output ->
      'b t ->
        unit
  end


module Piecelin_map :
  sig
    type t

    (** [eval p t] is the value of the piecewise linear function [p] at [t]. *)
    val eval : t -> float -> float
  end


(** {1:pos Positive càdlàg functions} *)
module Pos :
  sig
    (** [areas ?tf c] is the cadlag of areas under the segments of [c].
        if [c] has points [(t(i), x(i))],
        then [areas c] has points [(t(i), (t(i) - t(i-1)) * x(i-1))],
        where the first time of [c] is lost.
        If [tf] is given, then a last point [tf] is present. *)
    val areas :
      ?tf:float ->
      'a Util.anypos cadlag ->
        'b Util.pos cadlag

    (** [primitive c] is {!primitive} [c]
        but for [c] a positive cadlag, so that the result is increasing. *)
    val primitive :
      Util.closed_pos cadlag ->
        (Util.closed_pos * Util.closed_pos) cadlag
  end


(** {1:more More elaborate float càdlàg functions} *)

(** [integr tmax c] is the integral from [0.] to [tmax] of [c]. *)
val integr : float cadlag -> float -> float

(** [l2_norm c tmax] is the L2 norm of [c] stopped at [tmax]. *)
val l2_norm : float cadlag -> float -> float

(** [wmean lbd1 c1 lbd2 c2] is the mean of [c1] and [c2]
    weighted respectively by [lbd1] and [lb2]. *)
val wmean : float -> float cadlag -> float -> float cadlag -> float cadlag

(** [cwmean c1 c1' c2 c2'] is the mean of [c1'] and [c2']
    weighted respectively by [c1] and [c2], cadlags themselves. *)
val cwmean : float cadlag -> float cadlag -> float cadlag -> float cadlag -> float cadlag


(** {1:reg Linear interpolation of functions (regular intervals)} *)
module Regular :
  sig
    type 'a t

    val of_cadlag : ?tol:float -> ?dt:float -> 'a cadlag -> 'a t
    val t0 : 'a t -> float
    val tf : 'a t -> float
    val dt : 'a t -> float
    val length : 'a t -> int
    val times : 'a t -> float list
    val values : 'a t -> 'a list
    val to_list : 'a t -> (float * 'a) list
    val copy : 'a t -> 'a t
    val nth : 'a t -> int -> float * 'a
    val sub : ?start:int -> ?len:int -> 'a t -> 'a t
    val cut : ?tol:float -> t0:float -> tf:float -> 'a t -> 'a t
    val append : 'a t -> 'a t -> 'a t
    val translate : float -> 'a t -> 'a t
    val reverse : 'a t -> 'a t
    val neg : 'a t -> 'a t
    val eval : float t -> float -> float
    val modify_nth : int -> ('a -> 'a) -> 'a t -> unit
    val map : (float -> 'a -> 'b) -> 'a t -> 'b t
    val fold_left : ('b -> float -> 'a -> 'b) -> 'b -> 'a t -> 'b
    val fold_left2 :
      ('c -> float -> 'a -> 'b -> 'c) ->
      'c ->
      'a t ->
      'b t ->
        'c
    val primitive : float t -> float t

    module Pos :
      sig
        val eval : _ Util.anypos t -> float -> _ Util.pos

        val primitive : _ Util.anypos t -> Util.closed_pos t
      end

    val inverse : float t -> Piecelin_map.t

    val print :
      ('a BatIO.output -> 'b -> unit) ->
      'a BatIO.output ->
      'b t ->
        unit
  end

(** Piecewise linear functions *)

module L = BatList


type t = (float * (float * float)) list


let eval c t' =
  (* FIXME don't we have t = t' sadly ? *)
  let tc = Cadlag.find_right c t' in
  match tc with
  | [] ->
      invalid_arg "Jump.Piecelin.eval"
  | (t, (y, a)) :: _ ->
      y +. (t' -. t) *. a


let of_cadlag c = Cadlag.map (fun x -> (x, 0.)) c


let primitive (c : float Cadlag.t) =
  let rec f (t, (y, a)) =
    function
    | [] ->
        []
    | (t', x') :: tc ->
        let y' = y +. (t' -. t) *. a in
        let tya' = (t', (y', x')) in
        tya' :: f tya' tc
  in match c with
  | [] ->
      []
  | (t0, x0) :: tc ->
      let tya0 = (t0, (0., x0)) in
      tya0 :: (f tya0 tc)


let sub = Cadlag.map2 (fun (x, a) (x', a') -> (x -. x', a -. a'))


let l2_norm p tmax =
  (* with fun (t + s) -> (x +. a *. s) ** 2., s >= 0. *)
  let fsq x a s = x ** 2. *. s
               +. x *. a *. s ** 2.
               +. 1. /. 3. *. a ** 2. *. s ** 3. in
  let rec f tot c =
    (* the 0. cases should only be reached is c matches them first *)
    match c with
    | [] ->
        tot
    | (t, (x, a)) :: [] when t < tmax ->
        tot +. fsq x a (tmax -. t)
    | (_, _) :: [] -> (* t >= tmax *)
        tot
    | (t, (x, a)) :: (t', xa') :: tc when t' < tmax ->
        let ntot = tot +. fsq x a (t' -. t) in
        f ntot ((t', xa') :: tc)
    | (t, (x, a)) :: (_, _) :: _ when t < tmax -> (* t < tmax <= t' *)
        tot +. fsq x a (tmax -. t)
    | _ :: _ :: _ -> (* t1 >= tmax *)
      tot
  in sqrt (f 0. p)


let regulate (pl : t) pred period =
  let rec f acc c t =
    let nt = t +. period in
    match c with
    | (t', _) :: _ when nt < t' ->
        failwith "oops"
    | (t', (x', a')) :: (t'', _) :: _ when nt < t'' ->
        let y' = x' +. a' *. (nt -. t') in
        let nacc = (nt, y') :: acc in
        if (pred nt y') then
          (nt, nacc)
        else
          f nacc c nt
    | (_, _) :: (t'', xa'') :: tc ->  (* nt > t'' *)
        let nc = (t'', xa'') :: tc in
        f acc nc t
    | (t', (x', a')) :: [] ->
        let y' = x' +. a' *. (nt -. t') in
        let nacc = (nt, y') :: acc in
        if (pred nt y') then
          (nt, nacc)
        else
          f nacc c nt
    | [] ->
        (* should only be reached on init c=[] already matched *)
        failwith "should not be reached (matched already on init)"
  in
  match pl with
  | [] ->
      []
  | (t, (x, _)) :: _ ->
      let _, rreg = f [(t, x)] pl t in
      L.rev rreg


let diff pl =
  let rec f =
    function
    | [] ->
        []
    | _ :: [] ->
        []
    | (_, (x, _)) :: (t', (x', a')) :: tl ->
        let dx = x' -. x in
        (* Printf.eprintf "dx=%f ?> 0.\n%!" dx ; *)
        (t', dx) :: f ((t', (x', a')) :: tl)
  in (0., 0.) :: f pl

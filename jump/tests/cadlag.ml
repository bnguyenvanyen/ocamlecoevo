

(* test eval *)
let%test _ =
  let l = [
    (0., 1) ;
    (1., 2) ;
    (2., 3) ;
  ]
  in
  let c = Jump.of_list l in
  (Jump.eval c 1.5 = 2)


(* test find_right *)
let%expect_test _ =
  let l = [
    (0., 1) ;
    (1., 2) ;
    (2., 3) ;
  ]
  in
  let c = Jump.of_list l in
  let c' = Jump.find_right c 1.5 in
  Jump.print BatInt.print BatIO.stdout c' ;
  [%expect{|
    1.: 2
    2.: 3 |}]


(* test cut_left *)
let%expect_test _ =
  let c = Jump.of_list [
    (0., 1) ;
    (1., 2) ;
    (2., 3) ;
  ]
  in
  let c' = Jump.cut_left 1.5 c in
  Jump.print BatInt.print BatIO.stdout c' ;
  [%expect{|
    1.5: 2
    2.: 3 |}]


(* test cut_right *)
let%expect_test _ =
  let c = Jump.of_list [
    (0., 1) ;
    (1., 2) ;
    (2., 3) ;
  ]
  in
  let c' = Jump.cut_right 1.5 c in
  Jump.print BatInt.print BatIO.stdout c' ;
  [%expect{|
    0.: 1
    1.: 2 |}]


(* test cut_right on point *)
let%expect_test _ =
  let c = Jump.of_list [
    (0., 1) ;
    (1., 2) ;
    (2., 3) ;
  ]
  in
  let c' = Jump.cut_right 2. c in
  Jump.print BatInt.print BatIO.stdout c' ;
  [%expect{|
    0.: 1
    1.: 2 |}]


(* test cut_right_after *)
let%expect_test _ =
  let c = Jump.of_list [
    (0., 1) ;
    (1., 2) ;
    (2., 3) ;
  ]
  in
  let c' = Jump.cut_right_after 1.5 c in
  Jump.print BatInt.print BatIO.stdout c' ;
  [%expect{|
    0.: 1
    1.: 2 |}]


(* test cut_right_after on point *)
let%expect_test _ =
  let c = Jump.of_list [
    (0., 1) ;
    (1., 2) ;
    (2., 3) ;
  ]
  in
  let c' = Jump.cut_right_after 2. c in
  Jump.print BatInt.print BatIO.stdout c' ;
  [%expect{|
    0.: 1
    1.: 2
    2.: 3 |}]


let%expect_test _ =
  let c = Jump.of_list [
    (0., 1) ;
    (1., 2) ;
    (2., 3) ;
  ]
  in
  let c' = Jump.reverse ~tf:3. c in
  Jump.print BatInt.print BatIO.stdout c' ;
  [%expect{|
    3.: 3
    2.: 2
    1.: 1 |}]


let%expect_test _ =
  let c1 = Jump.of_list [
    (0., 1) ;
    (1., 2) ;
    (2., 3) ;
  ]
  in
  let c2 = Jump.of_list [
    (0., 1) ;
    (1.5, 2) ;
    (2.5, 3) ;
  ]
  in
  let c = Jump.Int.add c1 c2 in
  Jump.Int.print BatIO.stdout c ;
  [%expect{|
    0.: 2
    1.: 3
    1.5: 4
    2.: 5
    2.5: 6 |}]

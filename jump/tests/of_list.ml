

let%test _ =
  let l = [
    (0., 1) ;
    (1., 2) ;
    (2., 3) ;
  ]
  in
  (Jump.to_list (Jump.of_list l) = l)

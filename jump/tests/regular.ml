

(* test of_cadlag *)
let%expect_test _ =
  let c = Jump.of_list [(0., 1) ; (1., 2) ; (2., 3)] in
  let r = Jump.Regular.of_cadlag c in
  Printf.printf "%f" (Jump.t0 c) ;
  [%expect{| 0.000000 |}] ;
  Printf.printf "%f" (Jump.tf c) ;
  [%expect{| 2.000000 |}] ;
  Printf.printf "%f" (Jump.Regular.t0 r) ;
  [%expect{| 0.000000 |}] ;
  Printf.printf "%f" (Jump.Regular.tf r) ;
  [%expect{| 2.000000 |}]


(* test translate *)
let%expect_test _ =
  let r =
    Jump.Regular.of_cadlag (Jump.of_list [(0., 1.) ; (1., 2.) ; (2., 3.)])
  in
  let r' = Jump.Regular.translate 1. r in
  Printf.printf "%f" (Jump.Regular.eval r 1.5) ;
  [%expect{| 2.500000 |}] ;
  Printf.printf "%f" (Jump.Regular.eval r' (1. +. 1.5)) ;
  [%expect{| 2.500000 |}]


(* test bad cut *)
let%test _ =
  let r =
    Jump.Regular.of_cadlag (Jump.of_list [
      (0., 1.) ;
      (1., 2.) ;
      (2., 3.) ;
      (3., 4.) ;
    ])
  in
  match Jump.Regular.cut ~t0:1.5 ~tf:3. r with
  | exception Invalid_argument _ ->
      true
  | _ ->
      false


(* test cut *)
let%expect_test _ =
  let r =
    Jump.Regular.of_cadlag (Jump.of_list [
      (0., 1.) ;
      (1., 2.) ;
      (2., 3.) ;
      (3., 4.) ;
    ])
  in
  let r' = Jump.Regular.cut ~t0:1. ~tf:2. r in
  Printf.printf "%f" (Jump.Regular.t0 r') ;
  [%expect{| 1.000000 |}] ;
  Printf.printf "%f" (Jump.Regular.tf r') ;
  [%expect{| 2.000000 |}]


(* test reverse *)
let%expect_test _ =
  let r =
    Jump.Regular.of_cadlag (Jump.of_list [
      (0., 1.) ;
      (1., 2.) ;
      (2., 3.)
    ])
  in
  let r' = Jump.Regular.reverse r in
  Jump.Regular.print BatFloat.print BatIO.stdout r' ;
  [%expect{|
    0.: 3.
    1.: 2.
    2.: 1. |}]


(* test neg *)
let%expect_test _ =
  let r =
    Jump.Regular.of_cadlag (Jump.of_list [
      (0., 1.) ;
      (1., 2.) ;
      (2., 3.)
    ])
  in
  let r' = Jump.Regular.neg r in
  Jump.Regular.print BatFloat.print BatIO.stdout r' ;
  [%expect{|
    -2.: 3.
    -1.: 2.
    0.: 1. |}]
  

(* test primitive *)
let%expect_test _ =
  let r =
    Jump.Regular.of_cadlag (Jump.of_list [
      (0., 1.) ;
      (1., 2.) ;
      (2., 3.)
    ])
  in
  let p = Jump.Regular.primitive r in
  Jump.Regular.print BatFloat.print BatIO.stdout p ;
  [%expect{|
    0.: 0.
    1.: 1.5
    2.: 4. |}]


(* test inverse *)
let%expect_test _ =
  let r =
    Jump.Regular.of_cadlag (Jump.of_list [
      (0., 1.) ;
      (1., 2.) ;
      (2., 3.)
    ])
  in
  let i = Jump.Regular.inverse r in
  let t = Jump.Piecelin_map.eval i 2. in
  Printf.printf "%f" t ;
  [%expect{| 1.000000 |}]


(* test neg + primitive *)
let%expect_test _ =
  let r =
    Jump.Regular.of_cadlag (Jump.of_list [
      (0., 1.) ;
      (1., 2.) ;
      (2., 3.)
    ])
  in
  let r' = Jump.Regular.neg r in
  let p = Jump.Regular.primitive r' in
  Jump.Regular.print BatFloat.print BatIO.stdout p ;
  [%expect{|
    -2.: 0.
    -1.: 2.5
    0.: 4. |}]

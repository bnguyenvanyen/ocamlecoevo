


let%test _ =
  let l = [
    (0., 1) ;
    (1., 2) ;
    (2., 3) ;
  ]
  in
  let c = Jump.of_list l in
  let c' = Jump.cut_left 1.5 c in
  (Jump.to_list c' = [(1.5, 2) ; (2., 3)])



module J = Jump.Cadlag_map
module F = Util.Float


let example () =
  J.undefined
  |> J.add F.zero 1
  |> J.add F.one 2
  |> J.add F.two 3


(* test eval *)

let%expect_test _ =
  let c = example () in
  let n = J.eval c (F.of_float 0.5) in
  Printf.printf "%i" n ;
  [%expect{| 1 |}]


let%test _ =
  let c = example () in
  match J.eval c (F.of_float (~-.0.5)) with
  | exception Not_found ->
      true
  | _ ->
      false


(* test integrate *)

let%expect_test _ =
  let c = example () in
  let tot =
    J.integrate
      ~from_t:F.zero
      ~to_t:(F.of_float 3.)
      (fun x -> F.of_float (float_of_int x))
      c
  in
  Printf.printf "%g" (F.to_float tot) ;
  [%expect {|
    6 |}]


let%expect_test _ =
  let c = example () in
  let tot =
    J.integrate
      ~from_t:F.zero
      ~to_t:(F.of_float 1.5)
      (fun x -> F.of_float (float_of_int x ** 2.))
      c
  in
  Printf.printf "%g" (F.to_float tot) ;
  [%expect{|
    3 |}]


(* test map2 *)
let%expect_test _ =
  let c1 = example () in
  let c2 = J.of_list [
    (F.of_float 0., 1) ;
    (F.of_float 1.5, 2) ;
    (F.of_float 2.5, 3) ;
  ]
  in
  let add x y =
    match x, y with
    | Some x, Some y ->
        x + y
    | Some x, None
    | None, Some x ->
        x
    | None, None ->
        failwith "none"
  in
  let c = J.map2 add c1 c2 in
  J.print BatInt.print BatIO.stdout c ;
  [%expect{|
    0.:2
    1.:3
    1.5:4
    2.:5
    2.5:6 |}]


(* test cut *)
let%expect_test _ =
  let c = example () in
  let c' = J.cut (F.of_float 0.5) F.two c in
  J.print BatInt.print BatIO.stdout c' ;
  [%expect{|
    0.5:1
    1.:2 |}]

#!/usr/bin/python3
# -*- coding: utf-8 -*-

import argparse
from functools import partial

import numpy as np
import pandas as pd

from date_year import date_to_year, year_to_date


parser = argparse.ArgumentParser(
  description=
  "Convert a years cases csv file to a date cases csv file."
)

parser.add_argument(
  "--in",
  type=argparse.FileType('r'),
  default='-',
  help="Input csv file path. Should have a 't' column. Default stdin."
)

parser.add_argument(
  "--out",
  type=argparse.FileType('w'),
  default='-',
  help="Output csv file path. Default stdout."
)


args = parser.parse_args()


df = pd.read_csv(vars(args)["in"])

assert("t" in df.columns)

cols = [ c for c in df.columns if c != "t" ]

df = df.assign(date = df["t"].apply(year_to_date))

df.to_csv(args.out, index=False, columns = ["date"] + cols)

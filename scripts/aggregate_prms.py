#!/usr/bin/python3
# -*- coding: utf-8 -*-


import argparse
import re
import glob

import numpy as np
import pandas as pd


parser = argparse.ArgumentParser(
  description=
    "Aggregate prm files."
)

parser.add_argument(
  "--fnames",
  help="glob string to find prm grid files.",
)

parser.add_argument(
  "--multiples",
  type=int,
  nargs=2,
  default=[1, 0],
  help="with multiples '(mult, rem)', ignore files with "
       "'k mod mult != rem'.",
)

parser.add_argument(
  "--burn",
  type=int,
  default=0,
  help="ignore grid files with 'k < {burn}'.",
)

parser.add_argument(
  "--out",
  type=argparse.FileType('w'),
  default='-',
  help="Output csv file path. Default stdout.",
)


def volume(row) :
  return (row["vector_t"] * row["vector_u"] * row["vector_k"])


def density(row) :
  return (row["count"] / row["volume"])


def single(ser) :
  return ser.values[0]


# FIXME this is slow
def aggregate_csl(df) :
  def augment(group) :
    csl_vector_u = group["vector_u"].sum()
    csl_count = group["count"].sum()
    csl_volume = group["volume"].sum()
    csl_density = csl_count / csl_volume

    return pd.Series({
      "fname"    : single(group["fname"]),
      "k"        : single(group["k"]),
      "color"    : single(group["origin_c"]),
      "origin_t" : single(group["origin_t"]),
      "count"    : csl_count,
      "volume"   : csl_volume,
      "density"  : csl_density,
    })

  return df.groupby(
    ["origin_c", "origin_t", "fname"],
    as_index=False,
    sort=False,
  ).apply(
    augment
  ).reset_index(
    drop=True
  )


k_pat = re.compile(r".(\d+).prm.grid.csv")
def extract_k(fname) :
  match = k_pat.search(fname)
  if match :
    k = int(match.group(1))
    return k
  else :
    raise ValueError("Can't extract k from fname")


def keep_k(k, burn=-1, mult=1, rem=0) :
  return (k > burn) and (k % mult == rem)


def read(fname, k) :
  print(k)
  df = pd.read_csv(fname)
  df = df.assign(fname=fname)
  df = df.assign(k=k)

  return df


def read_all(fnames, burn, mult, rem) :
  fname_ks = [ (fname, extract_k(fname)) for fname in glob.glob(fnames) ]
  reads = [
    read(fname, k)
    for (fname, k) in fname_ks
    if keep_k(k, burn, mult, rem)
  ]

  if reads == [] :
    raise ValueError("bad glob")
  else :
    df = pd.concat(
      reads,
      ignore_index=True,
      sort=False,
    )

  return df


args = parser.parse_args()

mult, rem = args.multiples


df = read_all(
  fnames=args.fnames,
  burn=args.burn,
  mult=mult,
  rem=rem
)


df = df.assign(volume=volume)
df = df.assign(density=density)

df_agg = aggregate_csl(
  df
)

print(df_agg.head())

df_agg.to_csv(
  args.out,
  index=False
)

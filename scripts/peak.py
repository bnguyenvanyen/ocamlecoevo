#!/usr/bin/python3
# -*- coding: utf-8 -*-

import argparse

import numpy as np
import pandas as pd


parser = argparse.ArgumentParser(
  description="Return the peak time and magnitudes for the column."
)

parser.add_argument(
  "--column",
  default="i",
  help="Column to process."
)

parser.add_argument(
  "--in",
  help="Path to input CSV dataframe."
)

parser.add_argument(
  "--out",
  help="Path to output CSV dataframe."
)

args = parser.parse_args()


# The dataframe should have columns k, t, and 'column'
df = pd.read_csv(vars(args)["in"], index_col="k")


peak_magn = df.groupby("k")[args.column].max()
peak_where = df.groupby("k")[args.column].transform(max) == df[args.column]
peak_time = df[peak_where]["t"]

peaks = pd.DataFrame({"peak_time":peak_time, "peak_magn":peak_magn})

peaks.to_csv(args.out)


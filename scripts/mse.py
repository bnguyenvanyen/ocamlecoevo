#!/usr/bin/python3

import numpy as np
import pandas as pd


def sqdiff(df, target) :
  diff = df - target
  sq = diff.map(lambda x : x ** 2)

  return sq


def error(df_estim, df_prior, df_target) :
    # assume each line of df_estim is a sample
    # assume df_target has only one line
    # compute on all columns common to df_estim and df_target

    columns = list(
      set(df_estim.columns).intersection(
        set(df_prior.columns).intersection(
          set(df_target.columns)
        )
      )
    )

    c_errors = dict()
    for c in columns :
      tg = df_target[c].values[0]
      sq_estim = sqdiff(df_estim.loc[:, c], tg)
      sq_prior = sqdiff(df_prior.loc[:, c], tg)

      c_errors[c] = np.sum(sq_estim[c].values) \
                  / np.sum(sq_prior[c].values)

    errors = np.array(list(c_errors.values()))

    return np.mean(errors)

#!/usr/bin/python3
# -*- coding: utf-8 -*-

import argparse

import pandas as pd


parser = argparse.ArgumentParser(
  description="Return the row value 'k' with maximum logprior + loglik or other."
)

parser.add_argument(
  "--column",
  default="logpost",
  help="Column to take the maximum from."
)

parser.add_argument(
  "path",
  help="Path to input CSV dataframe."
)

args = parser.parse_args()


# The dataframe should have columns k, logprior, and loglik
df = pd.read_csv(args.path, index_col="k")

df = df.assign(logpost=df["logprior"] + df["loglik"])

id = df[args.column].idxmax()

print(id)

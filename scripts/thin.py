#!/usr/bin/python3
# -*- coding: utf-8 -*-

import argparse

import pandas as pd


parser = argparse.ArgumentParser(
  description="Thin dataframes with with column k."
)

parser.add_argument(
  "--burn",
  type=int,
  default=0,
  help="Keep sample if 'k > burn'.",
)

parser.add_argument(
  "--multiples",
  nargs=2,
  type=int,
  default=(1, 0),
  metavar=("mult", "rem"),
  help="Keep sample if 'k mod mult == rem'.",
)

parser.add_argument(
  "--in",
  help="Path to input CSV dataframe."
)

parser.add_argument(
  "--out",
  help="Path to output CSV dataframe."
)


args = parser.parse_args()


# The dataframe should have a column k
df = pd.read_csv(vars(args)["in"])

mult, rem = args.multiples

thin = df[(df["k"] > args.burn) & (df["k"] % mult == rem)]

thin.to_csv(
  args.out,
  index=False
)



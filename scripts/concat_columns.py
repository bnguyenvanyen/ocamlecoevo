#!/usr/bin/python3
# -*- coding: utf-8 -*-

import argparse

import pandas as pd


parser = argparse.ArgumentParser(
  description="Merge dataframes with same rows and different columns."
)

parser.add_argument(
  "--in",
  action="append",
  help="Path to input CSV dataframe."
)

parser.add_argument(
  "--out",
  help="Path to output CSV dataframe."
)


# It is expected that the dataframes all have a column k,
# with the same values. The join of columns is made with k as key.

args = parser.parse_args()


dfs = [ pd.read_csv(path, index_col="k") for path in vars(args)["in"] ]

df = dfs[0].join(
  other=dfs[1:],
)


df.to_csv(
  args.out
)



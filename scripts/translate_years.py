#!/usr/bin/python3
# -*- coding: utf-8 -*-

import argparse

import numpy as np
import pandas as pd


parser = argparse.ArgumentParser(
  description=
  "Add constant to 't' column."
)

parser.add_argument(
  "--add",
  type=float,
  default=0.,
  help="Number of years to add to 't' column."
)

parser.add_argument(
  "--in",
  type=argparse.FileType('r'),
  default='-',
  help="Input csv file path. Should have a 't' column. Default stdin."
)

parser.add_argument(
  "--out",
  type=argparse.FileType('w'),
  default='-',
  help="Output csv file path. Default stdout."
)


args = parser.parse_args()


df = pd.read_csv(vars(args)["in"])

assert("t" in df.columns)

df = df.assign(t = df["t"].apply(lambda t : t + args.add))

df.to_csv(args.out, index=False)

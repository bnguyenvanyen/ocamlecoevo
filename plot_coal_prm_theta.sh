#!/usr/bin/bash

NAME="random.1e4.logistic.234567.new.new.cut"
TARGET="_data/constant/"$NAME".par.csv"
BASESRC="_data/mcmc/constant/"$NAME
BASEDEST="_data/plot/mcmc/constant/"$NAME
BURN="200000"
MULT="2000 0"
CTXT="paper_cramped"


# for LEN in "4" "8" "16" "32"; do
for LEN in "32"; do
  INFER="infer_mu_lambda"
  if [ "$INFER" = "fix_mu_lambda" ]; then
    PARS=""
  elif [ "$INFER" = "fix_mu_infer_lambda" ]; then
    PARS="lambda"
  elif [ "$INFER" = "infer_mu_lambda" ]; then
    PARS="mu lambda"
  fi

  echo $BASESRC".sub"$LEN".mcmc.{}."$INFER".111"

  # # Plot tree with max posterior value
  # K=$(python3 scripts/kmax.py $BASESRC".sub"$LEN".mcmc.phylo."$INFER".111/run.theta.csv")
  # python3 plot/newick.py\
  #   --times "_data/constant/"$NAME".sub"$LEN".data.sequences.fasta"\
  #   --target "_data/constant/"$NAME".sub"$LEN".data.trees.newick"\
  #   --tree-sample $BASESRC".sub"$LEN".mcmc.phylo."$INFER".111/run.tree.newick" $K\
  #   --out $BASEDEST".sub"$LEN".mcmc.phylo."$INFER".111.tree.png"

  # K=$(python3 scripts/kmax.py $BASESRC".sub"$LEN".mcmc.simple."$INFER".111/run.theta.csv")
  # python3 plot/newick.py\
  #   --times "_data/constant/"$NAME".sub"$LEN".data.sequences.fasta"\
  #   --target "_data/constant/"$NAME".sub"$LEN".data.trees.newick"\
  #   --forest-sample $BASESRC".sub"$LEN".mcmc.simple."$INFER".111/run.forest.newick" $K\
  #   --out $BASEDEST".sub"$LEN".mcmc.simple."$INFER".111.tree.png"

  # # Compute KC distance
  # ## for phylo
  # dune exec phylo-trace-kc --\
  #   --path-target "_data/constant/"$NAME".sub"$LEN".data.trees.tnewick"\
  #   --path-sample $BASESRC".sub"$LEN".mcmc.phylo."$INFER".111/run.tree.tnewick"\
  #   $BASESRC".sub"$LEN".mcmc.phylo."$INFER".111/run.kc.csv"

  # ## for prm
  # dune exec coalfit-trace-kc --\
  #   --path-target "_data/constant/"$NAME".sub"$LEN".data.trees.tnewick"\
  #   --path-sample $BASESRC".sub"$LEN".mcmc.simple."$INFER".111/run.forest.tnewick"\
  #   $BASESRC".sub"$LEN".mcmc.simple."$INFER".111/run.kc.csv"

  # # Merge KC with theta
  # ## for phylo
  # python3 scripts/concat_columns.py\
  #   --in $BASESRC".sub"$LEN".mcmc.phylo."$INFER".111/run.theta.csv"\
  #   --in $BASESRC".sub"$LEN".mcmc.phylo."$INFER".111/run.kc.csv"\
  #   --out $BASESRC".sub"$LEN".mcmc.phylo."$INFER".111/run.theta_kc.csv"

  # ## for prm
  # python3 scripts/concat_columns.py\
  #   --in $BASESRC".sub"$LEN".mcmc.simple."$INFER".111/run.theta.csv"\
  #   --in $BASESRC".sub"$LEN".mcmc.simple."$INFER".111/run.kc.csv"\
  #   --out $BASESRC".sub"$LEN".mcmc.simple."$INFER".111/run.theta_kc.csv"

  # # Plot full chains
  # python3 plot/params.py\
  #   --target $TARGET\
  #   --read-template $BASESRC".sub"$LEN".mcmc.{}."$INFER".111/run.theta.csv"\
  #   --read-label "Method"\
  #   --read "PRM augmentation" "simple"\
  #   --read "Classical" "phylo"\
  #   --parameters loglik $PARS\
  #   --rename lambda "$\lambda$"\
  #   --rename mu "$\mu$"\
  #   --replace-parameter "Classical" "loglik" "loglik-evo"\
  #   --alpha 0.1 --dpi 200 --width 320 --latex --context $CTXT\
  #   --out $BASEDEST".sub"$LEN".mcmc.compare_coalfit_phylo."$INFER".111.theta.{}."$CTXT".png"

  # Plot without initial iterations
  # python3 plot/params.py\
  #   --target $TARGET\
  #   --read-template $BASESRC".sub"$LEN".mcmc.{}."$INFER".111/run.theta.csv"\
  #   --read-label "Method"\
  #   --read "PRM augmentation" "simple"\
  #   --read "Classical" "phylo"\
  #   --parameters loglik $PARS\
  #   --rename lambda "$\lambda$"\
  #   --rename mu "$\mu$"\
  #   --replace-parameter "Classical" "loglik" "loglik-evo"\
  #   --burn 2000000\
  #   --alpha 0.1 --dpi 200 --width 320 --latex --context $CTXT\
  #   --out $BASEDEST".sub"$LEN".mcmc.compare_coalfit_phylo."$INFER".111.burn.theta.{}."$CTXT".png"
done


# Benchmark stuff on 333

INFER="infer_mu_lambda"
PARS="mu lambda"

# # for LEN in "4" "8" "16" "32" "64"; do
# for LEN in "16" "32"; do
#   echo $BASESRC".sub"$LEN".mcmc.{}."$INFER".333"

#   # Compute KC distance
#   ## for phylo
#   dune exec phylo-trace-kc --\
#     --path-target "_data/constant/"$NAME".sub"$LEN".data.trees.tnewick"\
#     --path-sample $BASESRC".sub"$LEN".mcmc.phylo."$INFER".333/run.tree.tnewick"\
#     $BASESRC".sub"$LEN".mcmc.phylo."$INFER".333/run.kc.csv"

#   ## for prm
#   dune exec coalfit-trace-kc --\
#     --path-target "_data/constant/"$NAME".sub"$LEN".data.trees.tnewick"\
#     --path-sample $BASESRC".sub"$LEN".mcmc.simple."$INFER".333/run.forest.tnewick"\
#     $BASESRC".sub"$LEN".mcmc.simple."$INFER".333/run.kc.csv"

#   # Merge KC with theta
#   ## for phylo
#   python3 scripts/concat_columns.py\
#     --in $BASESRC".sub"$LEN".mcmc.phylo."$INFER".333/run.theta.csv"\
#     --in $BASESRC".sub"$LEN".mcmc.phylo."$INFER".333/run.kc.csv"\
#     --out $BASESRC".sub"$LEN".mcmc.phylo."$INFER".333/run.theta_kc.csv"

#   ## for prm
#   python3 scripts/concat_columns.py\
#     --in $BASESRC".sub"$LEN".mcmc.simple."$INFER".333/run.theta.csv"\
#     --in $BASESRC".sub"$LEN".mcmc.simple."$INFER".333/run.kc.csv"\
#     --out $BASESRC".sub"$LEN".mcmc.simple."$INFER".333/run.theta_kc.csv"

#   python3 plot/params.py\
#     --target $TARGET\
#     --read-template $BASESRC".sub"$LEN".mcmc.{}."$INFER".333/run.theta_kc.csv"\
#     --read-label "Method"\
#     --read "PRM augmentation" "simple"\
#     --read "Classical" "phylo"\
#     --parameters loglik distance $PARS\
#     --rename lambda "$\lambda$"\
#     --rename mu "$\mu$"\
#     --rotate-parameter "loglik" "30"\
#     --rotate-parameter "mu" "30"\
#     --replace-parameter "Classical" "loglik" "loglik-evo"\
#     --no-violin --no-pair\
#     --multiples 200 0\
#     --alpha 0.05 --dpi 200 --width 320 --latex --context $CTXT\
#     --out $BASEDEST".sub"$LEN".mcmc.compare_coalfit_phylo."$INFER".333.theta.{}."$CTXT".png"

#   python3 plot/params.py\
#     --target $TARGET\
#     --read-template $BASESRC".sub"$LEN".mcmc.{}."$INFER".333/run.theta_kc.csv"\
#     --read-label "Method"\
#     --read "PRM augmentation" "simple"\
#     --read "Classical" "phylo"\
#     --parameters loglik distance $PARS\
#     --rename lambda "$\lambda$"\
#     --rename mu "$\mu$"\
#     --rotate-parameter "loglik" "30"\
#     --rotate-parameter "mu" "30"\
#     --replace-parameter "Classical" "loglik" "loglik-evo"\
#     --burn $BURN\
#     --multiples 200 0\
#     --alpha 0.05 --dpi 200 --width 320 --latex --context $CTXT\
#     --out $BASEDEST".sub"$LEN".mcmc.compare_coalfit_phylo."$INFER".333.burn.theta.{}."$CTXT".png"

#   # Compute and output ESS
#   python3 plot/ess.py\
#     --read-template $BASESRC".sub"$LEN".mcmc.{}."$INFER".333/run.theta_kc.csv"\
#     --read "PRM augmentation" "simple"\
#     --read "Classical" "phylo"\
#     --parameters logprior loglik distance $PARS\
#     --burn $BURN
# done


# Plot initial KC traces
# python3 plot/params.py\
#   --read-template $BASESRC".sub"{}".mcmc.simple."$INFER".333/run.kc.csv"\
#   --read-label "Sequence length"\
#   --read "n = 4" "4"\
#   --read "n = 8" "8"\
#   --read "n = 16" "16"\
#   --parameters distance\
#   --until 100000\
#   --no-pair --no-violin\
#   --trace-superpose\
#   --dpi 200 --width 320 --latex --context $CTXT\
#   --out $BASEDEST".subs.mcmc.simple.compare_lengths."$INFER".333.kc.{}."$CTXT".png"


# Plot benchmark
python3 plot/bench.py\
  --read-template $BASESRC".sub{x}.mcmc.{label}.infer_mu_lambda.333/run.{measure}.csv"\
  --read-legend-title "Method"\
  --objective-column multi\
  --xaxis-label "Number of sequences"\
  --yaxis-label "\$s/{mESS}\$"\
  --xaxis-values 4 8 16 32 64\
  --xaxis-log-base 2 --xaxis-plain-format\
  --read "PRM augmentation" "simple"\
  --read "Classical" "phylo"\
  --power-factor 0.05\
  --dpi 200 --width 320 --latex --context $CTXT\
  --out $BASEDEST".subs.mcmc.compare_coalfit_phylo.infer_mu_lambda.333.bench.duration_mess."$CTXT".png"


python3 plot/bench.py\
  --read-template $BASESRC".sub{x}.mcmc.{label}.infer_mu_lambda.333/run.{measure}.csv"\
  --read-legend-title "Method"\
  --objective niter --objective-column niter\
  --xaxis-label "Number of sequences"\
  --yaxis-label "\$s/{iter}\$"\
  --xaxis-values 4 8 16 32 64\
  --xaxis-log-base 2 --xaxis-plain-format\
  --read "PRM augmentation" "simple"\
  --read "Classical" "phylo"\
  --power-factor 1e-4\
  --dpi 200 --width 320 --latex --context $CTXT\
  --out $BASEDEST".subs.mcmc.compare_coalfit_phylo.infer_mu_lambda.333.bench.duration_niter."$CTXT".png"


python3 plot/bench.py\
  --read-template $BASESRC".sub{x}.mcmc.{label}.infer_mu_lambda.333/run.{measure}.csv"\
  --read-legend-title "Method"\
  --cost niter --cost-column niter\
  --objective-column multi\
  --xaxis-label "Number of sequences"\
  --yaxis-label "\${iter}/{mESS}\$"\
  --xaxis-values 4 8 16 32 64\
  --xaxis-log-base 2 --xaxis-plain-format\
  --read "PRM augmentation" "simple"\
  --read "Classical" "phylo"\
  --power-factor 300\
  --dpi 200 --width 320 --latex --context $CTXT\
  --out $BASEDEST".subs.mcmc.compare_coalfit_phylo.infer_mu_lambda.333.bench.niter_mess."$CTXT".png"

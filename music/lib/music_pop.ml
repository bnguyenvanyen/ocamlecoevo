
(*
open Tree
*)

type param = {
  b : float ;
  d : float ;
  c : float ;
  mu : float ;
  v : float ;
  peaks : int
}

let specl = [
  ("-b", Sim.Arg.Float (fun par x -> { par with b = x }),
   "Individual (neutral) birth rate") ;
  ("-d", Sim.Arg.Float (fun par x -> { par with d = x }),
   "Individual (neutral) death rate by competition") ;
  ("-c", Sim.Arg.Float (fun par x -> { par with c = x }),
   "Coefficient of infighting (importance of harmony)") ;
  ("-mu", Sim.Arg.Float (fun par x -> { par with mu = x }),
   "Individual mutation rate") ;
  ("-v", Sim.Arg.Float (fun par x -> { par with v = x }),
   "Width of peaks for harmony and (1/4) mutation variance") ;
  ("-peaks", Sim.Arg.Int (fun par x -> { par with peaks = x }),
   "Number of tone + overtones (precision)") ;
]

type trait = float

let min_freq = log 20. /. log 2.

let max_freq = log 20000. /. log 2.

let sq_normal_hellinger m1 v1 m2 v2 =
  let s1 = sqrt v1 in
  let s2 = sqrt v2 in
  let rt = sqrt (2. *. s1 *. s2 /. (v1 +. v2)) in
  let e = exp (~-. 1. /. 4. *. ((m1 -. m2) ** 2. /. (v1 +. v2))) in
  1. -. rt *. e

let disharmony m v x x' =
  let rec f n n' =
    match n, n' with
    | 0, 0 -> 0.
    | _, 0 -> f (n - 1) m
    | _, _ ->
      let fnm = float_of_int (n - 1) in
      let fnm' = float_of_int (n' - 1) in
      let d = sq_normal_hellinger (x +. fnm) v (x' +. fnm') v in
      1. /. 2. ** (float_of_int (n + n' + 1)) *. d +. (f n (n' - 1))
  in sqrt (f m m)


module Trait =
  struct
    type t = trait
    type x = trait
    type comparison = trait -> trait -> int
    let phenotype_of x = x
    let equal = (=)
    let hash = Hashtbl.hash
    let cmp = compare
    let compare = Array.make 1 cmp
    let string_of = string_of_float
    let parse s = Scanf.sscanf s "%f" (fun x -> x)
    let copy x = x
    let group_of x = x
    let identified x = true
  end

module S :
  (Pop.Bdmi.SYSTEM with type param = param
                    and type trait = trait
                    and type group = trait) =
  struct
    type nonrec param = param
    type nonrec trait = trait
    type group = trait

    module Make (P : Pop.Sig.POP with type trait = trait
                                  and type group = group) =
      struct
        include Pop.Bdmi.Eventypes (struct
          include P
          type nonrec param = param
        end)

        let birth_maxrate par _ _ =
          par.b

        let neutral_death_rate par _ z =
          let n = P.count_all z in
          par.d *. float (n - 1)

        let mutation_maxrate par _ z =
          par.mu

        let death_relproba par x z =
          (* FIXME should be only in z - x *)
          (* FIXME ugly to get the rng like this but for conforming to interface *)
          let rng = Random.get_state () in
          let i' = P.choose_any rng z in
          let x' = P.traitval z i' in
          let d = disharmony par.peaks par.v x x' in
          let p = (par.d +. par.c *. d) /. (par.d +. par.c) in
          p

        let mutate par rng t x =
          let d () = Util.rand_normal rng x (4. *. par.v) in
          let x' = Util.bound min_freq max_freq d in
          (* Printf.printf "%f -> %f\n%!" (2. ** x) (2. ** x') ; *)
          x'

        let draw _ rng _ z = P.choose_any rng z

        let rel1 _ _ _ _ = 1.

        let birth = Birth (birth_maxrate, P.count_all, draw, rel1)
        let neutral_death = Death (neutral_death_rate, P.count_all, draw, rel1)
        let mutation = Mutation (mutation_maxrate, P.count_all, draw, rel1, mutate)

        let event_l = [birth ; neutral_death ; mutation]
     end 
  end


(* FIXME probably need own custom pop that records all frequencies *)
(* module P = Treepopbis.Make (Trait) *)
module P = Pop.Traitable.Make (Trait)


let sync_death_rate par t z =
  let n = P.count_all z in
  par.c *. float n *. float (n - 1)

let sync_death_modif par rng t z =
  (* choose an individual *)
  let i = P.choose_any rng z in
  (* choose another different individual *)
  let f () = P.choose_any rng z in
  let i' = Util.valid (fun i' -> i <> i') f in
  (* look at the disharmony between their trait values *)
  let x = P.traitval z i in
  let x' = P.traitval z i' in
  let d = disharmony par.peaks par.v x x' in
  if Util.rand_bernoulli rng d then
    (* kill the first individual *)
    P.remove t z (Some i) x
  else
    z


let sync_death = (sync_death_rate, sync_death_modif)


module S' =
  struct
    include Pop.Bdmi.Make (S) (P)
    let evl = sync_death :: evl
  end


type audioutparam = {
  blen : int ;  (* number of samples *)
  smplr : int ;  (* sampling rate *)
  volume : float ;
  fname : string ; (* destination for .wav file *)
}

module AudiOut :
  (Sim.Sig.OUTSPEC with type t = S'.t
                    and type param = param
                    and type outparam = audioutparam) =
  struct
    type t = S'.t
    type nonrec param = param
    type outparam = audioutparam

    let specl = [
      ("-smplr", Sim.Arg.Int (fun opar x -> { opar with smplr = x }),
       "Sample rate for the audio produced") ;
      ("-volume", Sim.Arg.Float (fun opar x -> { opar with volume = x }),
       "Individual volume for the population") ;
      ("-fname", Sim.Arg.String (fun opar x -> { opar with fname = x }),
       "Destination path for the audio file produced") ;
    ]

    let default = {
      blen = 441000 ;  (* tf = 10 *)
      smplr = 44100 ;
      volume = 0.01 ;
      fname = "audiout" ;
    }

    let buf_r = ref (Audio.Mono.create default.blen) 
    let n_r = ref 0

    (*
    let fill smplr vol buf z =
      (* for now we assume buf covers the full range of time *)
      let fsmplr = float_of_int smplr in
      let tree_l = Tree.Genealogy.forest_of z in
      (* for each indiv we add its voice to the buffer *)
      let subfill ti tf x =
        (* FIXME overtones ? *)
        let nu = 2. ** x in
        let omega = 2. *. Util.pi *. nu /. fsmplr in
        (* FIXME does that ensure that they stay in phase ? *)
        let ni = int_of_float (ti *. fsmplr) in
        let nf = int_of_float (tf *. fsmplr) in
        for i = ni to nf do
          buf.(i) <- buf.(i) +. vol *. sin (float i *. omega)
        done
      in
      let rec add_subtree (ti, xi) tr =
        match tr with
        | Base.Leaf (tf, xf) ->
          subfill ti tf xf
        | Base.Node ((tf, xf), subtr) ->
          subfill ti tf xf ;
          add_subtree (tf, xf) subtr
        | Base.Binode ((tf, xf), ltr, rtr) ->
          subfill ti tf xf ;
          add_subtree (tf, xf) ltr ;
          add_subtree (tf, xf) rtr
      in
      let add_tree tree =
        match Tree.T.basic tree with
        | Base.Node ((t0, x0), subtree) ->
          add_subtree (t0, x0) subtree
        | (Base.Leaf _ | Base.Binode _) ->
          invalid_arg "unrooted tree"
      in List.iter add_tree tree_l
    *)

    let output_begin opar par =
      Printf.printf "sample rate : %i ; volume : %f ; fname : %s"
      opar.smplr opar.volume opar.fname ;
      buf_r := Audio.Mono.create opar.blen

    let output opar par t z =
      (* FIXME note that births and deaths are not necessarily
       * very precise in the wav, depending on the sampling rate *)
      let fsmplr = float opar.smplr in
      (* we silently stop recording after the end of the buffer *)
      let nt = min (int_of_float (t *. fsmplr)) (opar.blen - 1) in
      Printf.printf "On [%i,%i], %i alive <?> %f \n" !n_r nt (P.count_all z) (1. /. opar.volume) ;
      for i = !n_r to nt do
        let y = 2. *. Util.pi *. float i /. fsmplr in
        let f x k v = v +. float k *. sin (y *. 2. ** x) in
        let v = Hashtbl.fold f z.Pop.Traitable.groups.Hashgroup.count 0. in
        !buf_r.(i) <- opar.volume *. v
      done ;
      n_r := nt

    let output_end opar par tf zf =
      (* for now we ignore opar *)
      let channels = 1 in
      let fname = Printf.sprintf "%s.wav" opar.fname in
      let wav = new Audio.IO.Writer.to_wav_file channels opar.smplr fname in
      wav#write (Audio.of_mono !buf_r) 0 opar.blen ;
      wav#close
  end


module CsvSpec :
  (Sim.Out.CSVSPEC with type t = S'.t
                    and type param = param) =
  struct
    type t = S'.t
    type nonrec param = param
    (* let aux_fun par t z = () *)
    let header = [[]]
    let line par t z =
      (* FIXME could we also output the mean disharmony in the csv ? *)
      (* actually we'd like the disharmonies indiv by indiv to analyze *)
      let f x k sl =
        (Printf.sprintf "%.2f:%i" (2. ** x) k) :: sl
      in
      let l = Hashtbl.fold f  z.Pop.Traitable.groups.Hashgroup.count [] in
      (Printf.sprintf "%.5f" t) :: l
  end

module CsvOut = Sim.Out.Csv (CsvSpec)

module CoOut = Sim.Out.Concat (AudiOut) (CsvOut)

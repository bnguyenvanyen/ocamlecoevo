(* What should we do again here ? *)

open Sig


module With_int (S : U.Interfaces.SEQ) =
  struct
    type elt = int * S.elt
    type t = int * S.t

    let fold f (k, nu) x =
      S.fold (fun elt x' -> f (k, elt) x') nu x

    let unfold x f =
      let nu = S.unfold x (fun x' ->
          match f x' with
          | None ->
              None
          | Some ((_, elt), x'') ->
              Some (elt, x'')
        )
      in
      match f x with
      | None ->
          (0, nu)
      | Some ((k, _), _) ->
          (k, nu)
  end


module Prm =
  struct
    module Make (S : Sim.Sig.PRM_MINI) =
      struct
        module Grid = U.Csv.Make (With_int (S.Slices))

        (* aggregated statistics on color slices *)
        module Agg = U.Csv.Make (With_int (S.Cslices))

        let columns_grid ~append =
          if append then
            "k" :: S.columns_grid
          else
            S.columns_grid

        let out_grid ?(append=false) =
          let columns = columns_grid ~append in
          let extract (k, uksl) =
            function
            | "k" ->
                string_of_int k
            | s ->
                S.extract_grid uksl s
          in
          let f = U.Csv.Row.unfold ~columns extract in
          (fun chan k nu -> Grid.write ~f ~chan (k, nu))

        let convert_prm_separate
            ?(n_burn=0) ?(n_thin=1)
            ~output_grid ~to_prm path =
          let root k =
            Printf.sprintf "%s.%i.prm" path k
          in
          let columns = columns_grid ~append:false in
          let output k x =
            let nu = to_prm (Fit.Mh.Return.sample x) in
            if output_grid then begin
              let chan =
                U.Csv.open_out_with ~columns (root k ^ ".grid.csv")
              in
              out_grid ~append:false chan k nu ;
              U.Csv.close_out chan
            end
          in
          let start k0 x0 =
            output k0 x0
          in
          let out k x =
            if (k >= n_burn) && (k mod n_thin = 0) then
            output k x
          in
          let return k x =
            (k, x)
          in
          U.Out.{ start ; out ; return }

        let convert_prm_append
            ?(n_burn=0) ?(n_thin=1) ~output_grid ~to_prm path =
          let root =
            Printf.sprintf "%s.prm" path
          in
          let output ~chan_grid k x =
            let nu = to_prm (Fit.Mh.Return.sample x) in
            if output_grid then begin
              out_grid ~append:true chan_grid k nu
            end
          in
          let columns = columns_grid ~append:true in
          let chan_r = ref None in
          let start k0 x0 =
            let chan_grid =
              U.Csv.open_out_with ~columns (root ^ ".grid.csv")
            in
            chan_r := Some chan_grid ;
            output ~chan_grid k0 x0
          in
          let out k x =
            if (k >= n_burn) && (k mod n_thin = 0) then begin
              match !chan_r with
              | None ->
                  failwith "Output not opened"
              | Some chan_grid ->
                  output ~chan_grid k x
            end
          in
          let return k x =
            match !chan_r with
            | None ->
                failwith "Output not opened"
            | Some chan_grid ->
                begin
                  U.Csv.close_out chan_grid ;
                  chan_r := None
                end ;
            (k, x)
          in
          U.Out.{ start ; out ; return }

        let convert_prm_agg_append
            ?(n_burn=0) ?(n_thin=1) ~output_grid ~to_prm path =
          let root =
            Printf.sprintf "%s.prm" path
          in
          let columns = "k" :: S.columns_csl in
          let out_grid =
            let extract (k, csl) =
              function
              | "k" ->
                  string_of_int k
              | s ->
                  S.extract_csl csl s
            in
            let f = U.Csv.Row.unfold ~columns extract in
            (fun chan k nu -> Agg.write ~f ~chan (k, nu))
          in
          let output ~chan k x =
            let nu = to_prm (Fit.Mh.Return.sample x) in
            if output_grid then begin
              out_grid chan k nu
            end
          in
          let chan_r = ref None in
          let start k0 x0 =
            let chan =
              U.Csv.open_out_with ~columns (root ^ ".aggregate.csv")
            in
            chan_r := Some chan ;
            output ~chan k0 x0
          in
          let out k x =
            if (k >= n_burn) && (k mod n_thin = 0) then begin
              match !chan_r with
              | None ->
                  failwith "Output not opened"
              | Some chan ->
                  output ~chan k x
            end
          in
          let return k x =
            match !chan_r with
            | None ->
                failwith "Output not opened"
            | Some chan ->
                begin
                  U.Csv.close_out chan ;
                  chan_r := None
                end ;
            (k, x)
          in
          U.Out.{ start ; out ; return }

        let convert_prm ?(append=false) =
          if append then
            convert_prm_append
          else
            convert_prm_separate

        let convert_prm_agg ?(append=false) =
          if append then
            convert_prm_agg_append
          else
            failwith "Not_implemented"
      end
  end


module Dbt =
  struct
    module Csv = U.Csv.Make (With_int (Sim.Dbt))

    let convert_append ?(n_burn=0) ?(n_thin=1) ?namer ~to_dbts ~len path =
      let name_to_int, int_to_name =
        match namer with
        | None ->
            None, None
        | Some (f, g) ->
            Some f, Some g
      in
      let root =
        Printf.sprintf "%s.dbt" path
      in
      let columns = "k" :: Sim.Dbt.columns ?int_to_name len in
      let out =
        let extract (k, (t, dbt)) =
          function
          | "k" ->
              string_of_int k
          | s ->
              Sim.Dbt.extract ?name_to_int (t, dbt) s
        in
        let f = U.Csv.Row.unfold ~columns extract in
        (fun chan k nu -> Csv.write ~f ~chan (k, nu))
      in
      let output ~chan k x =
        let nu = to_dbts (Fit.Mh.Return.sample x) in
        out chan k nu
      in
      let chan_r = ref None in
      let start k0 x0 =
        let chan =
          U.Csv.open_out_with ~columns (root ^ ".aggregate.csv")
        in
        chan_r := Some chan ;
        output ~chan k0 x0
      in
      let out k x =
        if (k >= n_burn) && (k mod n_thin = 0) then begin
          match !chan_r with
          | None ->
              failwith "Output not opened"
          | Some chan ->
              output ~chan k x
        end
      in
      let return k x =
        match !chan_r with
        | None ->
            failwith "Output not opened"
        | Some chan ->
            begin
              U.Csv.close_out chan ;
              chan_r := None
            end ;
        (k, x)
      in
      U.Out.{ start ; out ; return }

    let convert ?(append=false) =
      if append then
        convert_append
      else
        failwith "Not_implemented"
  end



(* We will also have to functorize this part ? *)
let convert_traj ?(n_burn=0) ?(n_thin=1) ~sim_traj path =
  let output k x =
    let theta = Fit.Mh.Return.sample x in
    let chan = open_out (Printf.sprintf "%s.%i.traj.csv" path k) in
    sim_traj ~chan theta ;
    close_out chan
  in
  let start k0 x0 =
    output k0 x0
  in
  let out k x =
    if (k >= n_burn) && (k mod n_thin = 0) then
      output k x
  in
  let return k x =
    (k, x)
  in
  Util.Out.{ start ; out ; return }


type 'a convert_nu = (
  ?append:bool ->
  ?n_burn:int ->
  ?n_thin:int ->
  string ->
    (int, 'a, int * 'a) U.Out.t
)


let convert_nu_traj ?traj_every ?nu_every ?n_burn ?append
    ~(convert_nu : 'a convert_nu) ~sim_traj path =
  let conv_nu =
    match nu_every with
    | Some n_thin ->
        convert_nu ?append ?n_burn ~n_thin path
    | None ->
        Util.Out.convert_null
  in
  let conv_traj =
    match traj_every with
    | Some n_thin ->
        convert_traj ?n_burn ~n_thin ~sim_traj path
    | None ->
        Util.Out.convert_null
  in Util.Out.combine (fun kres _ -> kres) conv_nu conv_traj
  

let convert_csv ?n_burn ?n_thin ~columns ~extract path =
  match n_thin with
  | Some n_thin ->
      let chan = Util.Csv.open_out (Printf.sprintf "%s.theta.csv" path) in
      let conv = Fit.Csv.convert ?n_burn ~n_thin ~chan ~columns ~extract in
      let return k res =
        U.Csv.close_out chan ;
        (k, res)
      in
      Fit.Csv.map ~return (fun x -> x) conv
  | None ->
      U.Out.convert_null


let convert_chol ?n_burn ?chol_every path =
  match chol_every with
  | Some n_thin ->
      let chan = open_out (Printf.sprintf "%s.chol.csv" path) in
      let conv = Fit.Csv.convert_lower_triang ?n_burn ~n_thin chan in
      let return k chol =
        close_out chan ;
        (k, chol)
      in
      Fit.Csv.map ~return (fun x -> x) conv
  | None ->
      Util.Out.convert_null


let convert_csv_traj
    ?n_burn ?n_thin
    ?traj_every
    ~columns ~extract
    ~sim_traj
    path =
  let conv_csv = convert_csv
    ?n_burn
    ?n_thin
    ~columns ~extract
    path
  in
  let conv_traj =
    match traj_every with
    | Some n_thin ->
        convert_traj ?n_burn ~n_thin ~sim_traj path
    | None ->
        Util.Out.convert_null
  in
  Util.Out.combine (fun kres _ -> kres) conv_csv conv_traj


let convert_csv_nu_traj
    ?n_burn ?n_thin
    ?nu_every ?traj_every
    ?append
    ~columns ~extract
    ~convert_nu ~sim_traj
    path =
  let output_main =
    convert_csv ?n_burn ?n_thin ~columns ~extract path
  in
  let output_aux = convert_nu_traj
      ?n_burn 
      ?nu_every ?traj_every ?append
      ~convert_nu ~sim_traj
      path
  in
  Util.Out.combine (fun kres _ -> kres) output_main output_aux


let convert_csv_ram
    ?n_burn ?n_thin
    ?chol_every
    ~columns ~extract
    ~chol
    path =
  let output_main =
    convert_csv ?n_burn ?n_thin ~columns ~extract path
  in
  let output_aux =
    convert_chol ?n_burn ?chol_every path
  in
  let conv = Util.Out.combine_tuple (fun (k, res) (_, chol) ->
      (k, res, chol)
    ) output_main output_aux
  in
  Fit.Csv.map
    ~return:(fun k res -> (k, res, chol))
    (fun res -> (res, chol))
    conv


let convert_csv_traj_ram
    ?n_burn ?n_thin
    ?traj_every ?chol_every
    ~columns ~extract
    ~sim_traj
    ~chol
    path =
  let conv_csv_traj = convert_csv_traj
    ?n_burn ?n_thin
    ?traj_every
    ~columns ~extract
    ~sim_traj
    path
  in
  let conv_chol = convert_chol ?n_burn ?chol_every path in
  let conv = Util.Out.combine_tuple (fun (k, res) (_, chol) ->
      (k, res, chol)
    ) conv_csv_traj conv_chol
  in
  Fit.Csv.map
    ~return:(fun k res -> (k, res, chol))
    (fun res -> (res, chol))
    conv


let convert_csv_traj_ram_nu
    ?n_burn ?n_thin
    ?chol_every ?nu_every ?traj_every
    ?append
    ~columns ~extract
    ~convert_nu ~sim_traj ~chol path =
  let output_main = convert_csv_nu_traj
      ?n_burn ?n_thin
      ?nu_every ?traj_every
      ?append
      ~columns ~extract
      ~convert_nu ~sim_traj
      path
  in
  let output_aux =
    convert_chol ?n_burn ?chol_every path
  in
  let cb (k, res) (_, chol) =
    (k, res, chol)
  in
  let conv = Util.Out.combine_tuple cb output_main output_aux in
  Fit.Csv.map
    ~return:(fun k res -> (k, res, chol))
    (fun res -> (res, chol))
    conv

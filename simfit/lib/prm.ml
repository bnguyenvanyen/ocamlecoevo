open Sig


module Make (P : Sim.Sig.PRM_MINI) :
  (PRM with module P = P
        and type t = P.t * U._float P.Point.Colormap.t Fm.t) =
  struct
    module P = P

    module Cm = P.Point.Colormap

    module Pre = U.Precompute ( )

    type intensities = U._float Cm.t Fm.t

    type t = P.t * intensities


    let ntslices ~width ~duration =
      I.Pos.of_float (F.Pos.div duration width)

    let prior ~vectors ?(t0=F.zero) ~tf ~width colors =
      let duration = F.Pos.of_anyfloat F.Op.(tf - t0) in
      let time_range = (t0, duration) in
      let ntslices = ntslices ~width ~duration in
      let rngmo = ref None in
      let draw rng =
        let rngm =
          match !rngmo with
          | None ->
              let rngm = L.fold_left (fun m c ->
                Cm.add c (U.rand_rng rng) m
              ) Cm.empty colors
              in
              rngmo := Some rngm ;
              rngm
          | Some rngm ->
              rngm
        in
        P.rand_draw ~rngm ~time_range ~ntslices ~vectors
      in
      let log_dens_proba _nu =
        (* [F.to_float (P.logp nu)] *)
        []
      in
      Fit.Dist.Base { draw ; log_dens_proba }

    let one_intensities nu : intensities =
      P.fold_slices (fun tsl intensities ->
        let t = P.Tsl.key tsl in
        let lbds = P.Tsl.fold_slices (fun csl lbds ->
            Cm.add (P.Csl.color csl) (F.narrow F.one) lbds
          ) tsl Cm.empty
        in
        Fm.add t lbds intensities
      ) nu Fm.empty

    let prior_intensities ~vectors ?(t0=F.zero) ~tf ~width colors =
      let duration = F.Pos.of_anyfloat F.Op.(tf - t0) in
      let time_range = (t0, duration) in
      let ntslices = ntslices ~width ~duration in
      let rngmo = ref None in
      let draw rng =
        let rngm =
          match !rngmo with
          | None ->
              let rngm = L.fold_left (fun m c ->
                Cm.add c (U.rand_rng rng) m
              ) Cm.empty colors
              in
              rngmo := Some rngm ;
              rngm
          | Some rngm ->
              rngm
        in
        let nu = P.rand_draw ~rngm ~time_range ~ntslices ~vectors in
        let intensities = one_intensities nu in
        (nu, intensities)
      in
      let log_dens_proba _ =
        (* [F.to_float (P.logp nu)] *)
        []
      in
      Fit.Dist.Base { draw ; log_dens_proba }

    let prior_seq ~vectors ?(t0=F.zero) ~tf ~width ~dt colors =
      let duration = F.Pos.of_anyfloat F.Op.(tf - t0) in
      let n_nus = ntslices ~width:dt ~duration in
      let ntslices = ntslices ~width ~duration:dt in
      let draw rng =
        (* can I reuse the same rngm for every nu ? *)
        let rngm = L.fold_left (fun m c ->
          Cm.add c (U.rand_rng rng) m
        ) Cm.empty colors
        in
        I.Pos.fold ~f:(fun nus k ->
          let t0 = F.Pos.mul (I.Pos.to_float (I.Pos.pred k)) dt in
          let time_range = (t0, dt) in
          let nu = P.rand_draw ~rngm ~time_range ~ntslices ~vectors in
          nu :: nus
        ) ~x:[] ~n:n_nus
      in
      let log_dens_proba _ =
        []
      in
      Fit.Dist.Base { draw ; log_dens_proba }

    (* ugly pasta *)
    let prior_intens_seq ~vectors ?(t0=F.zero) ~tf ~width ~dt colors =
      let duration = F.Pos.of_anyfloat F.Op.(tf - t0) in
      let n_nus = ntslices ~width:dt ~duration in
      let ntslices = ntslices ~width ~duration:dt in
      let draw rng =
        (* can I reuse the same rngm for every nu ? *)
        let rngm = L.fold_left (fun m c ->
          Cm.add c (U.rand_rng rng) m
        ) Cm.empty colors
        in
        I.Pos.fold ~f:(fun nus k ->
          let t0 = F.Pos.mul (I.Pos.to_float (I.Pos.pred k)) dt in
          let time_range = (t0, dt) in
          let nu = P.rand_draw ~rngm ~time_range ~ntslices ~vectors in
          let intens = one_intensities nu in
          (nu, intens) :: nus
        ) ~x:[] ~n:n_nus
      in
      let log_dens_proba _ =
        []
      in
      Fit.Dist.Base { draw ; log_dens_proba }

    (* proposal *)
   
    let uksl_redraw_full ~rng uksl = P.(
      UKsl.rand_draw ~rng (UKsl.color uksl) (UKsl.bounds uksl)
    )
   
    let propose_poisson_full ?(filter=(fun _ -> true)) nredraws =
      let draw rng nu =
        P.redraw_distinct ~rng uksl_redraw_full nredraws filter nu
      in
      let log_pd_ratio _nu _nu' =
        (* to compensate with prior(_intensities) *)
        (* [F.to_float (P.logp nu) -. F.to_float (P.logp nu')] *)
        []
      in
      Fit.Propose.endomorphism draw log_pd_ratio

    (* same but as a dist for 'sequential MCMC' *)
    let propose_poisson_dist ?(filter=(fun _ -> true)) nredraws =
      let dist nu =
        let draw rng =
          P.redraw_distinct ~rng uksl_redraw_full nredraws filter nu
        in
        let log_dens_proba _nu' =
          []
        in
        Fit.Dist.Base { draw ; log_dens_proba }
      in
      Fit.Propose.endo_dist dist

    let propose_poisson_intensities ?(filter=(fun _ -> true)) nredraws =
      let draw rng (nu, intensities) =
        let nu' =
          P.redraw_distinct ~rng uksl_redraw_full nredraws filter nu
        in
        (nu', intensities)
      in
      let log_pd_ratio _ _ =
        (* to compensate with prior(_intensities) *)
        (* [F.to_float (P.logp nu) -. F.to_float (P.logp nu')] *)
        []
      in
      Fit.Propose.endomorphism draw log_pd_ratio

    (* same but as dist *)
    let propose_poisson_intens_dist ?(filter=(fun _ -> true)) nredraws =
      let dist (nu, intensities) =
        let draw rng =
          let nu' =
            P.redraw_distinct ~rng uksl_redraw_full nredraws filter nu
          in
          (nu', intensities)
        in
        let log_dens_proba _ =
          []
        in
        Fit.Dist.Base { draw ; log_dens_proba }
      in
      Fit.Propose.endo_dist dist

    let adjust_density_delta_slice ~rng dlambda uksl =
      match P.UKsl.adjust_density ~rng dlambda uksl with
      | res ->
          res
      | exception ((Invalid_argument _ | Failure _) as e) ->
          Fit.raise_draw_error e

    let adjust_intensity ~rng ~slices dlambda (nu, intensities) =
      let color, jt = U.rand_unif_choose ~rng slices in
      let t = P.time_of_nth nu jt in
      let tsl = P.t_slice_at nu t in
      let csl = P.Tsl.find tsl color in
      let tins = Fm.find t intensities in
      let lambda = Cm.find color tins in
      match F.Pos.of_anyfloat F.Op.(lambda + dlambda) with
      | exception (Invalid_argument _ as e) ->
          Fit.raise_draw_error e
      | lambda' ->
      let csl' = P.Csl.map_slices (P.UKsl.adjust_intensity ~rng lambda') csl in
      let tsl' = P.Tsl.with_slice csl' tsl in
      let nu' = P.with_slice tsl' nu in
      let tins' = Cm.add color (F.narrow lambda') tins in
      let intensities' = Fm.add t tins' intensities in
      (nu', intensities')

    let jump ~time_range j colors =
      let jt0, djt = time_range in
      let slices = L.fold_left (fun sls color ->
          let csls = I.Pos.list_init ~n:djt ~f:(fun i ->
            (color, I.Pos.add jt0 i)
          ) in
          sls @ csls
        ) [] colors
      in
      let draw rng dx nui =
        adjust_intensity ~rng ~slices (F.of_float dx) nui
      in
      let diff (nu, ins) (_, ins') =
        (* find dlambda again by looking in intensities *)
        let color, jt = L.hd slices in
        let t = P.time_of_nth nu jt in
        let tins = Fm.find t ins in
        let lambda = Cm.find color tins in
        let tins' = Fm.find t ins' in
        let lambda' = Cm.find color tins' in
        F.to_float F.Op.(lambda' - lambda)
      in
      Fit.Param.DJ.{ draw ; diff ; jump = j }

    let jumps ~color_groups ~ntranges ~ntslices j =
      (* number of slices by sub range *)
      let djt = I.Pos.Op.(ntslices / ntranges) in
      (* remaining slices to put in the last range *)
      let rem = I.Pos.Op.(ntslices mod ntranges) in
      (* from the first time range to the penultimate time range *)
      let jumps = U.Int.Pos.fold ~f:(fun js k ->
          let jt0 = I.Pos.mul djt (I.Pos.of_anyint (I.pred k)) in
          let time_range = (jt0, djt) in
          let color_jumps = L.map (jump ~time_range j) color_groups in
          color_jumps @ js
        ) ~x:[] ~n:ntranges
      in
      (* put the last jumps in their place *)
      if I.Op.(rem <> I.zero) then
        let jt0_last = I.Pos.of_anyint I.Op.(ntslices - rem) in
        let time_range = (jt0_last, rem) in
        let last_jumps = L.map (jump ~time_range j) color_groups in
        jumps @ last_jumps
      else
        jumps

    (* for a sequential element,
     * one jump per time and color slice *)
    (* ntslices number of slices per seq element *)
    let jumps_elt ~colors ~ntslices j =
      U.Int.Pos.fold ~f:(fun js k ->
        let jt = I.Pos.of_anyint (I.pred k) in
          (L.map (fun c -> jump ~time_range:(jt, I.one) j [c]) colors)
        @ js
      ) ~x:[] ~n:ntslices

    let log_pd_ratio_delta_slice (uksl, _) (uksl', _) =
      P.UKsl.adjust_log_pd_ratio uksl uksl'

    let log_pd_ratio ((nu, ins) : t) (nu', ins') =
      let logp = Fm.fold (fun t tins logp ->
          let tins' = Fm.find t ins' in
          Cm.fold (fun col lambda logp ->
            let lambda' = Cm.find col tins' in
            if F.Op.(lambda <> lambda') then
              (* should never raise *)
              let lbd = F.Pos.of_anyfloat lambda in
              (* find the relevant slices *)
              let csl = P.Tsl.find (P.t_slice_at nu t) col in
              let csl' = P.Tsl.find (P.t_slice_at nu' t) col in
              (* fold over uksls *)
              P.Csl.fold_rvl_slices (fun uksl logp ->
                let uksl' = P.Csl.find csl' (P.UKsl.key uksl) in
                let sum lbd' =
                  let logr_slice =
                    P.UKsl.log_pd_ratio_intensity (uksl, lbd) (uksl', lbd')
                  in
                  if F.Op.(logr_slice > F.of_float 100.) then
                    Printf.eprintf "ratio %f with lbd = %f ; lbd' = %f\n"
                    (F.to_float logr_slice)
                    (F.to_float lbd)
                    (F.to_float lbd')
                  ;
                  F.add logp logr_slice
                in
                match F.identify lambda' with
                | Neg _ ->
                    F.neg_infinity
                | Zero _ ->
                    sum F.zero
                | Proba lbd' ->
                    sum lbd'
                | Pos lbd' ->
                    sum lbd'
              ) csl logp
            else
              logp
          ) tins logp
        ) ins F.zero
      in
      (* [P.logp nu' - P.logp nu] to compensate with prior(_intensities) *)
      (* [F.to_float F.Op.(P.logp nu' - P.logp nu + logp)] *)
      [F.to_float logp]
  end

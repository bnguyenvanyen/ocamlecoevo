module L = BatList

module U = Util
module F = U.Float
module I = U.Int

module Fm = Sim.Prm.Fm
module PFIm = Sim.Prm.PFIm


module type PRM =
  sig
    module P : Sim.Sig.PRM_MINI

    type t = P.t * U._float P.Point.Colormap.t Fm.t

    val one_intensities : P.t -> U._float P.Point.Colormap.t Fm.t

    val ntslices :
      width: _ U.anypos ->
      duration: _ U.anypos ->
        _ U.posint

    val prior :
      vectors: (U.anyposint * P.vector) list ->
      ?t0: _ U.anyfloat ->
      tf: _ U.anyfloat ->
      width: _ U.anypos ->
      P.color list ->
        (P.t, float list) Fit.Dist.t

    val prior_intensities :
      vectors: (U.anyposint * P.vector) list ->
      ?t0: _ U.anyfloat ->
      tf: _ U.anyfloat ->
      width: _ U.anypos ->
      P.color list ->
        (t, float list) Fit.Dist.t

    val prior_seq :
      vectors: (U.anyposint * P.vector) list ->
      ?t0: _ U.anyfloat ->
      tf: _ U.anyfloat ->
      width: _ U.anypos ->
      dt: _ U.anypos ->
      P.color list ->
        (P.t list, float list) Fit.Dist.t

    val prior_intens_seq :
      vectors: (U.anyposint * P.vector) list ->
      ?t0: _ U.anyfloat ->
      tf: _ U.anyfloat ->
      width: _ U.anypos ->
      dt: _ U.anypos ->
      P.color list ->
        (t list, float list) Fit.Dist.t

    (** [propose_poisson_full ?filter nredraws]
     *  proposes [prm] by redrawing [nredraws] distinct color slices,
     *  that pass the optional [filter]. *)
    val propose_poisson_full :
      ?filter:(P.color -> bool) ->
      int ->
        (P.t, P.t) Fit.Propose.t

    (** [propose_poisson_dist] is like {!propose_poisson_full} but with
     *  the [Fit.Propose.Dist] constructor for sequential MCMC. *)
    val propose_poisson_dist :
      ?filter:(P.color -> bool) ->
      int ->
        (P.t, P.t) Fit.Propose.t

    (** [propose_poisson_intensities] is like {!propose_poisson_full}
     *  with intensities left untouched. *)
    val propose_poisson_intensities :
      ?filter:(P.color -> bool) ->
      int ->
        (t, t) Fit.Propose.t

    (** [propose_poisson_intens_dist] is like {!propose_poisson_intensities}
     *  but with the [Fit.Propose.Dist] constructor for sequential MCMC. *)
    val propose_poisson_intens_dist :
      ?filter:(P.color -> bool) ->
      int ->
        (t, t) Fit.Propose.t


    val jumps :
      color_groups: P.color list list ->
      ntranges: U.anyposint ->
      ntslices: U.anyposint ->
      float ->
        t Fit.Param.DJ.t list

    val jumps_elt :
      colors: P.color list ->
      ntslices: U.anyposint ->
      float ->
        t Fit.Param.DJ.t list

    val log_pd_ratio : t -> t -> float list

    val adjust_density_delta_slice :
      rng:U.rng ->
      U._float ->
      P.uk_slice ->
        P.uk_slice

    val log_pd_ratio_delta_slice :
      P.uk_slice * 'a ->
      P.uk_slice * 'a ->
        U._float
  end

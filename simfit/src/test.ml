module U = Util
module F = U.Float

module Color =
  struct
    type t = unit

    let compare () () = 0
    let equal () () = true
    let to_string () = "()"
    let of_string =
      function
      | "()" ->
          Some ()
      | _ ->
          None
  end


module Point = Sim.Prm.Compare_time (Sim.Prm.Point (Color))


module P = Sim.Prm.Make (Point)


module SF = Simfit.Prm.Make (P)


let point ~k ~t ~u =
  Sim.Prm.{ c = () ; k ; t ; u }


let bounds = P.{
  origin = point ~k:0 ~t:F.zero ~u:F.zero ;
  vector = point ~k:1 ~t:F.one ~u:(F.Pos.of_float 5.) ;
}


let prior =
  let draw rng =
    let uksl = P.UKsl.rand_draw ~rng () bounds in
    let lambda = F.narrow F.one in
    (uksl, lambda)
  in
  let log_dens_proba _ =
    [0.]
  in
  Fit.Dist.Base { draw ; log_dens_proba }


let likelihood = Fit.Dist.Flat


let propose_prior =
  let draw rng (_, lambda) =
    let uksl' = P.UKsl.rand_draw ~rng () bounds in
    (uksl', lambda)
  in
  let log_pd_ratio _ _ =
    [0.]
  in
  Fit.Propose.Base { draw ; log_pd_ratio }


let propose_gaussian =
  let draw rng (uksl, lambda) =
    let lambda' =
      lambda
      |> F.to_float
      |> (fun x -> U.rand_normal ~rng x (F.Pos.of_float 0.01))
      |> F.of_float
      |> F.positive_part  (* FIXME not symmetrical ? *)
    in
    let uksl' =
      SF.adjust_density_var_slice ~rng F.Op.(lambda' - lambda) uksl
    in
    (uksl', lambda')
  in
  let log_pd_ratio x x' =
    [F.to_float (SF.log_pd_ratio_var_slice x x')]
  in
  Fit.Propose.Base { draw ; log_pd_ratio }


let draw rng dx (uksl, lambda) =
  let dlambda = F.of_float dx in
  let lambda' = F.Op.(lambda + dlambda) in
  let uksl' = SF.adjust_density_delta_slice ~rng dlambda uksl in
  (uksl', lambda')


let columns =
  Fit.Csv.columns_sample_move @ [
    "lambda" ;
    "nu" ;
    "move_lambda" ;
    "move_nu"
  ]


let extract =
  let f (uksl, lambda) =
    function
    | "lambda" ->
        F.to_string lambda
    | "nu" ->
        F.to_string (P.UKsl.density uksl)
    | _ ->
        invalid_arg "unrecognized column"
  in Fit.Csv.extract_sample_move ~extract:f


let run_fixed seed mode n_iter path =
  let prop, n_thin, n_iter =
    match mode with
    | `Prior ->
        propose_prior, 1, n_iter
    | `Gauss ->
        propose_gaussian, 100, n_iter * 100
  in
  let init, sample = Fit.Mcmc.integrate prior prop likelihood in
  let n_burn = 0 in
  let chan = Util.Csv.open_out (path ^ ".theta.csv") in
  let output = Fit.Csv.convert ~n_burn ~n_thin ~chan ~columns ~extract in
  let x = Fit.Dist.draw_from (U.rng seed) prior in
  ignore (Fit.Mcmc.posterior ~output ?seed ~init ~sample ~n_iter x) ;
  Util.Csv.close_out chan


let run_adapt seed n_iter path =
  let n_iter = n_iter * 100 in
  let output = Simfit.Out.convert_csv_ram
    ~n_thin:100
    ~chol_every:100
    ~columns
    ~extract
    path
  in
  let log_pd_ratio x x' =
    [F.to_float (SF.log_pd_ratio_delta_slice x x')]
  in
  let draws = [| draw |] in
  let chol = Lacaml.D.Mat.of_array [| [| 0.01 |] |] in
  let x = Fit.Dist.draw_from (U.rng seed) prior in
  ignore (Fit.Ram.posterior
    ~output
    ?seed
    ~prior
    ~log_pd_ratio
    ~draws
    ~chol
    ~likelihood
    ~n_iter
    x
  )


let run_dumb seed n_iter path =
  let n_iter = n_iter * 100 in
  let columns = Fit.Csv.columns_sample_move @ ["lambda" ; "move_lambda"] in
  let extract =
    let f lambda =
      function
      | "lambda" ->
          string_of_float lambda
      | _ ->
          invalid_arg "unrecognized column"
    in Fit.Csv.extract_sample_move ~extract:f
  in
  let output = Simfit.Out.convert_csv_ram
    ~n_thin:100
    ~chol_every:100
    ~columns
    ~extract
    path
  in
  (* no log_pd_ratio *)
  let draws = [| (fun _ dx lambda -> lambda +. dx) |] in
  let prior = Fit.Dist.Normal (0., F.Pos.of_float 0.01) in
  let x = Fit.Dist.draw_from (U.rng seed) prior in
  ignore (Fit.Ram.posterior
    ~output
    ?seed
    ~prior
    ~draws
    ~likelihood
    ~n_iter
    x
  )


let run seed mode n_iter path =
  match mode with
  | (`Prior | `Gauss) as mode ->
      run_fixed seed mode n_iter path
  | `Ram ->
      run_adapt seed n_iter path
  | `Dumb ->
      run_dumb seed n_iter path


module Term =
  struct
    open Cmdliner

    let seed =
      let doc = "Seed the PRNG with $(docv)." in
      Arg.(value
         & opt (some int) None
         & info ["seed"] ~docv:"SEED" ~doc
      )

    let n_iter =
      let doc = "Number of iterations $(docv) of the chain." in
      Arg.(value
         & pos 0 int 0
         & info [] ~docv:"N-ITER" ~doc
      )

    let out =
      let doc = "Path to output (without '.theta.csv')." in
      Arg.(required
         & pos 1 (some string) None
         & info [] ~docv:"OUT" ~doc
      )

    let mode =
      let doc = "Proposal mode." in
      Arg.(value
         & opt (enum [
           ("prior", `Prior) ;
           ("gauss", `Gauss) ;
           ("ram", `Ram) ;
           ("dumb", `Dumb) ;
         ]) `Prior
         & info ["mode"] ~docv:"MODE" ~doc
      )

    let run = Term.(const run $ seed $ mode $ n_iter $ out)

    let info =
      let doc =
        "Check whether the gaussian prm proposal draws from the PRM prior."
      in Term.info
        "simfit-test"
        ~version:"%%VERSION%%"
        ~doc
        ~exits:Term.default_exits
        ~man:[]
  end


let () =
     Cmdliner.Term.exit
  @@ Cmdliner.Term.eval
  (Term.run, Term.info)

(* in this module we should be able to keep every type abstract,
 * and make them manifest only in Unsymb ? *)

module type IMPL =
  sig
    type rng

    type positive
    type count

    type id
    type nonid

    type vec
    type mat

    type 'a group
    type 'a trait

    type dim

    type trait_comp

    type 'a indiv

    type pop

    type 'a value
  end


module type S =
  sig
    include IMPL

    (* need one / more other constructors to make it easier
     * to build a trait,
     * rather than Map Nonid Constant,
     * or worse, Map Id Product Constant Multinormal,
     * what would be nice is something to have a constant id trait,
     * with only one of the comps following some kind of dist *)
    type _ dist =
      | Map :
          ('a -> 'b) * 'a dist -> 'b dist
      | Product :
          'a dist * 'b dist -> ('a * 'b) dist
      | Constant :
          'a value -> 'a dist
      | Uniform :
          float value * float value -> float dist
      | Exponential :
          positive value -> positive dist
      | Normal :
          positive value -> (float -> float) dist
      | Multinormal :
          mat value -> (vec -> vec) dist
      | Normal_comp :
          dim * positive value -> (id trait -> id trait) dist


    type _ choose =
      | Nil : unit choose
      | Group : id group * 'a choose -> (id indiv * 'a) choose


    module Modif :
      sig
        type _ symb =
          | Nil :
              unit symb
          | Ignore :
              'b symb -> ('a indiv * 'b) symb
          | Remove :
              'b symb -> ('a indiv * 'b) symb
          | Copy :
              'b symb -> (id indiv * 'b) symb
          | Copy_mutate :
              (id trait -> id trait) dist * 'b symb -> (id indiv * 'b) symb
          | New_nonid :
              nonid group * 'a symb -> 'a symb
          | New_id :
              id trait dist * 'a symb -> 'a symb
      end


    module Trait :
      sig
        type _ symb =
          (* function application *)
          | Eval :
              ('a -> 'b) symb * 'a symb -> 'b symb
          (* Add unused argument after first argument of function *)
          | Lift_in :
              ('a -> 'b) symb -> ('a -> 'c -> 'b) symb
          (* Add unused argument as first argument *)
          | Lift_out :
              'a symb -> ('b -> 'a) symb
          | Lift_fun :
              ('a -> 'b) symb ->
                (('c -> 'a) -> 'c -> 'b) symb
          | Lift_double :
              ('a -> 'b -> 'c) symb ->
                (('d -> 'a) -> ('d -> 'b) -> 'd -> 'c) symb
          | Tuple :
              ('a -> 'b -> 'c) symb -> ('b * 'a -> 'c) symb
          | Unit :
              ('a -> 'b) symb -> ('a * unit -> 'b) symb
          | Add :
              (positive -> positive -> positive) symb
          | Times :
              (positive -> positive -> positive) symb
          (* this constructor might be too permissive...
           * one for positive values only ? *)
          | Constant :
              positive value -> positive symb
          | Trait :
              string -> ('a indiv -> 'a trait) symb
          | Dim :
              dim -> (id trait -> trait_comp) symb
          | Abs :
              (trait_comp -> positive) symb
          | Comp_normal :
              trait_comp value * positive value ->
                (trait_comp -> positive) symb
          | Multi_normal :
              id trait value * mat value -> (id trait -> positive) symb
          (* Those constructors are dangerous (in terms of bounds)
           * (for example Abs or Dim Abs should not be used on them)
           * To protect us, we might make them work only for Multi_normal
           * and Comp_normal.
           * Would be slightly ugly since Multi_normal and Comp_normal
           * also need to stay to be used by themselves but well *)
          | Trait_diff :
              (id trait -> id trait -> id trait) symb
          | Trait_comp_diff :
              (trait_comp -> trait_comp -> trait_comp) symb


        type 'a rate = ('a -> positive) symb
      end
    

    module Bound :
      sig
        type symb =
          | Add of symb * symb
          | Times of symb * symb
          | Constant of positive value
          | Max_comp of (id trait -> positive) Trait.symb

        type rate = symb
      end

    module Atom :
      sig
        type _ symb =
          | Add :
              positive symb * positive symb -> positive symb
          | Times :
              positive symb * positive symb -> positive symb
          | Div :
              positive symb * positive symb -> positive symb
          | Constant :
              'a value -> 'a symb
          | Count_id :
              id group -> count symb
          | Count_nonid :
              nonid group -> count symb
          | As_pos :
              count symb -> positive symb
          | Holling_I :
            { a : positive value } -> positive symb
          | Holling_II :
            { a : positive value ;
              h : positive value ;
              count : count symb } ->
                positive symb
          | Holling_III :
            { a : positive value ;
              h : positive value ;
              count : count symb } ->
                positive symb

        type rate = positive symb
      end

    type wrap_choose_modif =
      Exim : 'a choose * 'a Modif.symb -> wrap_choose_modif

    type 'a event = {
      name : string ;
      indivs : 'a choose ;
      trait_rate : 'a Trait.rate ;
      bound_rate : Bound.rate ;
      (* rather atom_rate *)
      atom_rate : Atom.rate ;
      modif : 'a Modif.symb ;
    }


    (* so we sort of have to keep the whole type ?
     * even if we don't care that much at that point *)

    type events =
      | Nil_event : events
      | And_event : 'a event * events -> events


    type model = {
      groups : (id group list) * (nonid group list) ;
      events : events ;
    }
  end


module Make (I : IMPL) :
  (S with type rng = I.rng
      and type positive = I.positive
      and type count = I.count
      and type id = I.id
      and type nonid = I.nonid
      and type 'a group = 'a I.group
      and type 'a trait = 'a I.trait
      and type vec = I.vec
      and type mat = I.mat
      and type dim = I.dim
      and type trait_comp = I.trait_comp
      and type 'a group = 'a I.group
      and type 'a indiv = 'a I.indiv
      and type pop = I.pop
      and type 'a value = 'a I.value) =
  struct
    include I


    type _ dist =
      | Map :
          ('a -> 'b) * 'a dist -> 'b dist
      | Product :
          'a dist * 'b dist -> ('a * 'b) dist
      | Constant :
          'a value -> 'a dist
      | Uniform :
          float value * float value -> float dist
      | Exponential :
          positive value -> positive dist
      | Normal :
          positive value -> (float -> float) dist
      | Multinormal :
          mat value -> (vec -> vec) dist
      | Normal_comp :
          dim * positive value -> (id trait -> id trait) dist


    type _ choose =
      | Nil : unit choose
      | Group : id group * 'a choose -> (id indiv * 'a) choose


    module Modif =
      struct
        type _ symb =
          | Nil :
              unit symb
          | Ignore :
              'b symb -> ('a indiv * 'b) symb
          | Remove :
              'b symb -> ('a indiv * 'b) symb
          | Copy :
              'b symb -> (id indiv * 'b) symb
          | Copy_mutate :
              (id trait -> id trait) dist * 'b symb -> (id indiv * 'b) symb
          | New_nonid :
              nonid group * 'a symb -> 'a symb
          | New_id :
              id trait dist * 'a symb -> 'a symb
        end


    module Trait =
      struct
        type _ symb =
          | Eval :
              ('a -> 'b) symb * 'a symb -> 'b symb
          | Lift_in :
              ('a -> 'b) symb -> ('a -> 'c -> 'b) symb
          | Lift_out :
              'a symb -> ('b -> 'a) symb
          | Lift_fun :
              ('a -> 'b) symb -> (('c -> 'a) -> 'c -> 'b) symb
          | Lift_double :
              ('a -> 'b -> 'c) symb -> (('d -> 'a) -> ('d -> 'b) -> 'd -> 'c) symb
          | Tuple :
              ('a -> 'b -> 'c) symb -> ('b * 'a -> 'c) symb
          | Unit :
              ('a -> 'b) symb -> ('a * unit -> 'b) symb
          | Add :
              (positive -> positive -> positive) symb
          | Times :
              (positive -> positive -> positive) symb
          | Constant :
              positive value -> positive symb
          | Trait :
              string -> ('a indiv -> 'a trait) symb
          | Dim :
              dim -> (id trait -> trait_comp) symb
          | Abs :
              (trait_comp -> positive) symb
          (* Do we need it ? would directly be a (trait -> pop)
           * | Trait_max : compare -> trait symb *)
          | Comp_normal :
              trait_comp value * positive value -> (trait_comp -> positive) symb
          | Multi_normal :
              id trait value * mat value -> (id trait -> positive) symb
          | Trait_diff :
              (id trait -> id trait -> id trait) symb
          | Trait_comp_diff :
              (trait_comp -> trait_comp -> trait_comp) symb


        type 'a rate = ('a -> positive) symb
      end

    module Bound =
      struct
        type symb =
          | Add of symb * symb
          | Times of symb * symb
          | Constant of positive value
          | Max_comp of (id trait -> positive) Trait.symb

        type rate = symb
      end

    module Atom =
      struct
        type _ symb =
          | Add :
              positive symb * positive symb -> positive symb
          | Times :
              positive symb * positive symb -> positive symb
          | Div :
              positive symb * positive symb -> positive symb
          | Constant :
              'a value -> 'a symb
          | Count_id :
              id group -> count symb
          | Count_nonid :
              nonid group -> count symb
          | As_pos :
              count symb -> positive symb
          | Holling_I :
            { a : positive value } -> positive symb
          | Holling_II :
            { a : positive value ;
              h : positive value ;
              count : count symb } ->
                positive symb
          | Holling_III :
            { a : positive value ;
              h : positive value ;
              count : count symb } ->
                positive symb

        type rate = positive symb
      end

    type wrap_choose_modif =
      Exim : 'a choose * 'a Modif.symb -> wrap_choose_modif

    type 'a event = {
      name : string ;
      indivs : 'a choose ;
      trait_rate : 'a Trait.rate ;
      bound_rate : Bound.rate ;
      (* rather atom_rate *)
      atom_rate : Atom.rate ;
      modif : 'a Modif.symb ;
    }


    (* so we sort of have to keep the whole type ?
     * even if we don't care that much at that point *)

    type events =
      | Nil_event : events
      | And_event : 'a event * events -> events


    type model = {
      groups : (id group list) * (nonid group list) ;
      events : events ;
    }
  end

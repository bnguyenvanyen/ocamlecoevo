%parameter<S : (Symbolic.S with type 'a value = 'a Value.t
                            and type positive = Pos.t
                            and type count = Posint.t
                            and type dim = int
                            and type 'a group = 'a Zen__Pop.Trait.group)>

%parameter<H : sig
  val params : (string, float) BatHashtbl.t
end>

%{
  open S;;
  module Zp = Zen__Pop
%}

%start <S.wrap_choose_modif> modif

%type <S.wrap_choose_modif> inlist
%type <(S.wrap_choose_modif -> S.wrap_choose_modif)> tuple
%type <('a S.Modif.symb -> (S.id S.indiv * 'a) S.Modif.symb)> longmodif
%type <('a S.Modif.symb -> 'a S.Modif.symb)> shortmodif

%%

modif:
  | LSQUARE ; e = inlist ; EOF
    { e }
  ;

inlist:
  | e = tuple ; RSQUARE
    { e (Exim (Nil, Modif.Nil)) }
  | e = tuple ; SEMIC ; l = inlist
    { e l }
  ;

tuple:
  | LPAREN ; g = VAR ; COMMA ; m = longmodif ; RPAREN
    { (fun (Exim (is, ms)) -> Exim (Group (g, is), m ms)) }
  | LPAREN ; UNDER ; COMMA ; m = shortmodif ; RPAREN
    { (fun (Exim (is, ms)) -> Exim (is, m ms)) }
  ;

longmodif:
  | IGNORE
    { (fun ms -> Modif.Ignore ms) }
  | REMOVE
    { (fun ms -> Modif.Remove ms) }
  | COPY
    { (fun ms -> Modif.Copy ms) }
  | MUTATE ; LPAREN ; d = ttdist ; RPAREN
    { (fun ms -> Modif.Copy_mutate (d, ms)) }
  ;

shortmodif:
  | NEW_ID ; LPAREN ; d = tdist ; RPAREN
    { (fun ms -> Modif.New_id (d, ms)) }
  | NEW_NONID ; LPAREN ; g = VAR ; RPAREN
    { (fun ms -> Modif.New_nonid (Zp.Trait.Nonid g, ms)) }
  ;

ttdist:
  | NORMAL_COMP ; LPAREN ; n = INT ; COMMA ; v = valexpr ; RPAREN
    { Normal_comp (int_of_string n, v) }
  ;

tdist:
  |
    { failwith "Not_implemented" }
  ;


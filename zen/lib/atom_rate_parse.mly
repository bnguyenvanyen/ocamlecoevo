%parameter<S : (Symbolic.S with type 'a value = 'a Value.t
                            and type positive = Pos.t
                            and type count = Posint.t
                            and type 'a group = 'a Zen__Pop.Trait.group)>

%start <S.Atom.rate> rate

%%

rate:
  | e = expr ; EOF
    { e }
  ;

expr:
  | LPAREN; e = expr ; RPAREN
    { e }
  | e1 = expr ; PLUS ; e2 = expr
    { S.Atom.Add (e1, e2) }
  | e1 = expr ; TIMES ; e2 = expr
    { S.Atom.Times (e1, e2) }
  | e1 = expr ; DIV ; e2 = expr
    { S.Atom.Div (e1, e2) }
  | a = atom
    { a }
  ;

atom:
  | c = count
    { S.Atom.As_pos c }
  | HOLLING_I ; LPAREN ; a = valexpr ; RPAREN
    { S.Atom.Holling_I { a } }
  | HOLLING_II ; LPAREN ; a = valexpr ; COMMA ; h = valexpr ; COMMA ; c = count ; RPAREN
    { S.Atom.Holling_II { a ; h ; count = c } }
  | HOLLING_III ; LPAREN ; a = valexpr ; COMMA ; h = valexpr ; COMMA ; c = count ; RPAREN
    { S.Atom.Holling_III { a ; h ; count = c } }
  | VAL ; v = valexpr
    { S.Atom.Constant v }
  ;

count:
  | COUNT_ID ; v = VAR
    { S.Atom.Count_id v }
  | COUNT_NONID ; v = VAR
    { S.Atom.Count_nonid v }
  ;

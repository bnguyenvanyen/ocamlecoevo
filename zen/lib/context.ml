module H = BatHashtbl

type 'a key = string

type 'a value = 'a

type t = (string, Obj.t) H.t

let key _ s = s

let create () = H.create 98

let register (h : t) (k : 'a key) (v : 'a value) =
  H.add h k (Obj.repr v)

let lookup (h : t) (k : 'a key) =
  Obj.obj (H.find h k)

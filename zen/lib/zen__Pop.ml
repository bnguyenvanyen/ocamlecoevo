
module Trait =
  struct
    type 'a group = string

    type 'a t =
      | Id : Pop.Sig.id group * float array -> Pop.Sig.id t
      | Nonid : Pop.Sig.nonid group -> Pop.Sig.nonid t

    let equal = (=)

    let hash = Hashtbl.hash

    let copy :
      type isid. isid t -> isid t =
      function
      | Id (g, y)      -> Id (g, Array.copy y)
      | (Nonid _) as y -> y

    let group_of :
      type isid. isid t -> isid group =
      function
      | Id (g, _) -> g
      | Nonid g   -> g

    let id (g, y) : Pop.Sig.id t = Id (g, y)
  end


module type COMPLEX = (
  Pop.Sig.POP_EVENTS with type 'a trait = 'a Trait.t
                      and type 'a group = 'a Trait.group
)


module Demography = Pop.Events.Make (Pop.With_dummy_genealogy.Make (Trait))

module Genealogy = Pop.Events.Make (Pop.With_genealogy.Make (Trait))

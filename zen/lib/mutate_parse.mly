%parameter<S : (Symbolic.S with type 'a value = 'a Value.t
                            and type positive = Pos.t
                            and type count = Posint.t
                            and type dim = int
                            and type 'a group = 'a Zen__Pop.Trait.group)>

%start <('a S.Modif.symb -> (S.id S.indiv * 'a) S.Modif.symb)> mutate

%%

mutate:
  | MUTATE ; LPAREN ; d = ttdist ; RPAREN
    { (fun ms -> S.Modif.Copy_mutate (d, ms)) }
  ;

ttdist:
  | NORMAL_COMP ; LPAREN ; n = INT ; COMMA ; v = valexpr ; RPAREN
    { S.Normal_comp (n, v) }
  ;

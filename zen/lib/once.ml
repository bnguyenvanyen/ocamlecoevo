
module type S =
  sig
    type value
    type ('a, 'b) t

    val draw : ('a, 'b) t

    val reset :
      unit ->
        unit
  end


module Make
  (S :
    sig
      type t
      val draw : (Random.State.t -> 'a -> 'b -> t)
    end)
  ( ) :
  (S with type value = S.t) =
  struct
    type value = S.t
    type ('a, 'b) t = (Random.State.t -> 'a -> 'b -> S.t)

    let store = ref None

    let draw rng a b =
      match !store with
      | Some x ->
          x
      | None ->
          let x = S.draw rng a b in
          store := Some x ;
          x

    let reset () =
      store := None
  end

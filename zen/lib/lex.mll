{
  module T = Tokens
}


let white = [' ' '\t']+
let newline = '\r' | '\n' | "\r\n"

let unused_sign = ['<' '>' '|' '{' '}' '#' '@' ':']

let digit = ['0'-'9']
let exp = 'E' ('+' | '-') digit+
let letter = ['A'-'Z' 'a'-'z']

let integer = digit+
let pos = digit+ '.'? digit* exp*
let var = letter+

rule read =
  parse
  | white         { read lexbuf }
  | newline       { read lexbuf }
  | "count_id"    { T.COUNT_ID }
  | "count_nonid" { T.COUNT_NONID }
  | "holling_iii" { T.HOLLING_III }
  | "holling_ii"  { T.HOLLING_II }
  | "holling_i"   { T.HOLLING_I }
  | "remove"      { T.REMOVE }
  | "mutate"      { T.MUTATE }
  | "ignore"      { T.IGNORE }
  | "copy"        { T.COPY }
  | "new_id"      { T.NEW_ID }
  | "new_nonid"   { T.NEW_NONID }
  | "normal_comp" { T.NORMAL_COMP }
  | "val"         { T.VAL }
  | integer       { T.INT (lexbuf |> Lexing.lexeme |> int_of_string) }
  | pos           { T.POS (lexbuf |> Lexing.lexeme |> float_of_string |> Pos.of_float) }
  | var           { T.VAR (Lexing.lexeme lexbuf) }
  | '+'           { T.PLUS }
  | '-'           { T.MINUS }
  | '*'           { T.TIMES }
  | '/'           { T.DIV }
  | ']'           { T.RSQUARE }
  | '['           { T.LSQUARE }
  | ')'           { T.RPAREN }
  | '('           { T.LPAREN }
  | ';'           { T.SEMIC }
  | ','           { T.COMMA }
  | '_'           { T.UNDER }
  | eof           { T.EOF }


{
  module T = Tokens
}


let white = [' ' '\t']+
let newline = '\r' | '\n' | "\r\n"

let unused_sign = ['<' '>' '|' '[' ']' '{' '}' '_' '#' '@' ':' ',']

let digit = ['0'-'9']
let letter = ['A'-'Z' 'a'-'z']

let pos = digit+ '.'? digit* exp*
let var = letter+

rule read =
  parse
  | white   { read lexbuf }
  | newline { read lexbuf }
  | pos     { T.POS (Lexing.lexeme lexbuf) }
  | letter  { T.VAR (Lexing.lexeme lexbuf) }
  | '+'     { T.PLUS }
  | '-'     { T.MINUS }
  | '*'     { T.TIMES }
  | '/'     { T.DIV }
  | ')'     { T.RPAREN }
  | '('     { T.LPAREN }
  | eof     { T.EOF }


module Vec = Lacaml.D.Vec
module Mat = Lacaml.D.Mat

module U = Util

module V = Value


module Make (P : Zen__Pop.COMPLEX) =
  struct
    module S = Symbolic.Make (Impl.Make (P))

    open S

    let ev e e' =
      Trait.Eval (e, e')

    let chain e e' =
      Trait.Eval (Trait.Lift_fun e', e)

    (* FIXME if we want to use this to combine several times,
     * we need to get rid of Tuple Unit and put it in later *)
    (* FIXME here we might want to be careful about the inversion
     * of arguments at tupleification ? *)
    let combine e e' e'' = Trait.(
      let lift_e = Lift_double (Lift_double e) in
      let lift_e' = Lift_in e' in
      let lift_e'' = Lift_out e'' in
      Tuple (Unit (Eval (Eval (lift_e, lift_e'), lift_e'')))
    )

    let (||>) = chain

    let (<+>) e e' = combine Trait.Add e e'

    let (<->) = combine Trait.Trait_diff

    let (<-->) = combine Trait.Trait_comp_diff


    module None =
      struct
        let one = Trait.(Lift_out (Constant (Literal Pos.one)))
      end


    module Single =
      struct
        let dim_abs name d = Trait.(
          Unit (Trait name ||> Dim d ||> Abs)
        )

        let dim_normal name d m v = Trait.(
          Unit (Trait name ||> Dim d ||> Comp_normal (m, v))
        )
      end


    module Double =
      struct
        let dim_normal name name' d m v = Trait.(
          ((Trait name ||> Dim d) <--> (Trait name' ||> Dim d))
          ||> Comp_normal (m, v)
        )

        let multi_normal name name' m cov = Trait.(
          (Trait name) <-> (Trait name')
          ||> Multi_normal (m, cov)
        )
      end


    module Bound =
      struct
        module None =
          struct
            let one = Bound.Constant (Literal Pos.one)
          end

        module Single =
          struct
            let dim_abs d =
              Bound.Max_comp (Trait.Dim d ||> Trait.Abs)

            (* FIXME v needs to be a value not a literal,
             * how ? *)
            let dim_normal v =
              let bound = Value.map (fun v' ->
                v'
                |> Pos.to_float
                |> (fun x -> U.d_normal 0. x 0.)
                |> Pos.of_float
              ) v
              in
              Bound.Constant bound
          end

        module Double =
          struct
            let dim_normal v = Single.dim_normal v

            let multi_normal cov =
              let bound = Value.map (fun cov' ->
                let m = Vec.make0 (Mat.dim1 cov') in
                cov'
                |> U.mat_to_cov
                |> (fun x -> U.d_multi_normal m x m)
                |> Pos.of_float
              ) cov
              in
              Bound.Constant bound
          end
      end
  end

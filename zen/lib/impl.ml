module Base =
  struct
    type rng = Random.State.t
    type positive = Util.Pos.t
    type count = Util.Posint.t
    type id = Pop.Sig.id
    type nonid = Pop.Sig.nonid
    type vec = float array
    type mat = float array array
    type 'a group = 'a Zen__Pop.Trait.group
    type 'a trait = 'a Zen__Pop.Trait.t
    (* maybe type dim = Posint.t ? *)
    type dim = int
    type trait_comp = float
    type 'a name = (unit -> 'a)
    type 'a value = 'a Value.t
  end


module Make (P : Zen__Pop.COMPLEX) =
  struct
    include Base

    type 'a indiv = 'a P.indiv
    type pop = P.t
  end

%token UNDER
%token <string> VAR
%token VAL
%token TIMES
%token SEMIC
%token RPAREN
%token RSQUARE
%token REMOVE
%token <Pos.t> POS
%token PLUS
%token NORMAL_COMP
%token NEW_NONID
%token NEW_ID
%token MUTATE
%token MINUS
%token LPAREN
%token LSQUARE
%token <int> INT
%token IGNORE
%token HOLLING_III
%token HOLLING_II
%token HOLLING_I
%token EOF
%token DIV
%token COUNT_NONID
%token COUNT_ID
%token COPY
%token COMMA

%left PLUS
%left MINUS
%left TIMES
%left DIV
%left VAL

%%

module H = BatHashtbl

type 'a t =
  | Bound of (unit -> 'a)
  | Literal of 'a

let literal x = Literal x
let bind h k = Bound (fun () -> H.find h k)

let eval =
  function
  | Bound f ->
      f ()
  | Literal x ->
      x

let map f =
  function
  | Literal x ->
      Literal (f x)
  | Bound g ->
      Bound (fun () -> f (g ()))

(* comment mélanger Literal et Bound ? *)
let map2 f a b =
  match a, b with
  | Literal x, Literal x' ->
      Literal (f x x')
  | Bound g, Bound g' ->
      Bound (fun () -> f (g ()) (g' ()))
  | Literal x, Bound g ->
      Bound (fun () -> f x (g ()))
  | Bound g, Literal x ->
      Bound (fun () -> f (g ()) x)


let bind_pos h k = Bound (fun () -> Pos.of_float (H.find h k))

module L = BatList
module A = BatArray

module Vec = Lacaml.D.Vec
module Mat = Lacaml.D.Mat

module U = Util
module Evn = Sim.Ctmjp.Event.Non_auto
module Zp = Zen__Pop


module Right_tuple =
  struct
    type (_, _) t =
      | Cons : 'a * ('a, 'b) t -> ('a, 'b * 'a) t
      | Single : 'a -> ('a, 'a) t
  end


exception Not_boundable

module Make (P : Zp.COMPLEX) =
  struct
    module S = Symbolic.Make (Impl.Make (P))
              
    open S

    let rec eval_dist :
      type a. a dist -> (rng -> a) =
      function
      | Map (f, e) ->
          let d = eval_dist e in
          (fun rng -> f (d rng))
      | Product (e, e') ->
          let d  = eval_dist e in
          let d' = eval_dist e' in
          (fun rng -> (d rng, d' rng))
      | Constant x ->
          let x' = Value.eval x in
          (fun _ -> x')
      | Uniform (a, b) ->
          let a' = Value.eval a in
          let b' = Value.eval b in
          (fun rng -> a' +. U.rand_float ~rng (b' -. a'))
      | Exponential lbd ->
          let lbd' = Value.eval lbd in
          (fun rng -> U.Pos.of_float (U.rand_exp ~rng (U.Pos.to_float lbd')))
      | Normal v ->
          let v' = U.Pos.to_float (Value.eval v) in
          (fun rng m -> U.rand_normal ~rng m v')
      | Multinormal cov ->
          let cov' = U.mat_to_cov (Mat.of_array (Value.eval cov)) in
          (fun rng m -> Vec.to_array (U.rand_multi_normal ~rng (Vec.of_array m) cov'))
      | Normal_comp (d, v) ->
          (* WARNING this doesn't copy the trait *)
          (* is it a case where we need to copy or not ?
           * typically will be used in Modif,
           * where it already gets copied. *)
          let v' = U.Pos.to_float (Value.eval v) in
          (fun rng (Zp.Trait.Id (g, a)) ->
            let m = a.(d) in
            let a' = A.copy a in
            a'.(d) <- U.rand_normal ~rng m v' ;
            Zp.Trait.Id (g, a'))
          

      
    let rec eval_choose :
      type a. a choose -> (rng -> pop -> a) =
      let fgroup g rng z = P.choose ~rng z g in
      function
      | Nil ->
          (fun _ _ -> ())
      | Group (g, Nil) ->
          (* special case to avoid a dumb function call *)
          (fun rng z -> fgroup g rng z, ())
      | Group (g, e) ->
          let f = eval_choose e in
          (fun rng z -> fgroup g rng z, f rng z)


    (* FIXME might want to subtract individuals n (n - 1) instead of n ** 2 *)
    let rec eval_linear_rate :
      type a. a choose -> (pop -> positive) =
      let fgroup g z =
        g |> P.count_id z |> U.Posint.of_int |> U.Pos.of_posint
      in
      function
      | Nil ->
          (fun _ -> U.Pos.one)
      | Group (g, Nil) ->
          fgroup g
      | Group (g, e) ->
          let f = eval_linear_rate e in
          (fun z -> U.Pos.mul (fgroup g z) (f z))


    let rec eval_modif :
      type a. a Modif.symb -> (a -> rng -> float -> pop -> pop) = Modif.(
      let fignore _ _ _ _ =
        ()
      in
      let fremove ind _ t z =
        P.remove t z ind
      in
      let fcopy ind _ t z =
        ind
        |> P.trait_of
        |> P.copy_trait
        |> (P.new_indiv_id z)
        |> (P.add_id ~parent:ind t z)
      in
      let fmutate d ind rng t z =
        ind
        |> P.trait_of
        (* |> P.copy_trait -- d should copy if needed *)
        |> (d rng)
        |> (P.new_indiv_id z)
        |> (P.add_id ~parent:ind t z)
      in
      let fnew_nonid g _ t z =
        P.add_nonid z (P.new_indiv_nonid (Zp.Trait.Nonid g))
      in
      let fnew_id d rng t z =
        P.add_id t z (P.new_indiv_id z (d rng))
      in
      let chain f f' (ind, inds) rng t z =
        f ind rng t z ;
        f' inds rng t z
      in
      let fnil f (ind, ()) rng t z =
        f ind rng t z ; z
      in
      let chain_new f f' inds rng t z =
        f rng t z ;
        f' inds rng t z
      in
      let fnil_new f () rng t z =
        f rng t z ;
        z
      in
      function
      | Nil ->
          (fun () _ _ z -> z)
      | Ignore Nil ->
          (fun _ _ _ z -> z)
      | Remove Nil ->
          fnil fremove
      | Copy Nil ->
          fnil fcopy
      | Copy_mutate (d, Nil) ->
          let f = eval_dist d in
          fnil (fmutate f)
      | New_nonid (g, Nil) ->
          fnil_new (fnew_nonid g)
      | New_id (d, Nil) ->
          let f = eval_dist d in
          fnil_new (fnew_id f)
      | Ignore e ->
          let m = eval_modif e in
          chain fignore m
      | Remove e ->
          let m = eval_modif e in
          chain fremove m
      | Copy e ->
          let m = eval_modif e in
          chain fcopy m
      | Copy_mutate (d, e) ->
          let m = eval_modif e in
          let f = eval_dist d in
          chain (fmutate f) m
      | New_nonid (g, e) ->
          let m = eval_modif e in
          chain_new (fnew_nonid g) m
      | New_id (d, e) ->
          let m = eval_modif e in
          let f = eval_dist d in
          chain_new (fnew_id f) m
    )
             

    (* should we get the score / compare functions as we go ? *)
    let rec eval_trait : type a. a Trait.symb -> a = Trait.(
      function
      | Eval (e, e') ->
          let f = eval_trait e  in
          let a = eval_trait e' in
          (* warning 20 ? *)
          f a
      | Lift_in e ->
          let f = eval_trait e in
          (fun a _ -> f a)
      | Lift_out e ->
          let f = eval_trait e in
          (fun b -> f)
      | Lift_fun e ->
          let f = eval_trait e in
          (fun g c -> f (g c))
      | Lift_double e ->
          let f = eval_trait e in
          (fun g h d -> let a = g d in let b = h d in f a b)
      | Tuple e ->
          let f = eval_trait e in
          (fun (a, b) -> f b a)
      | Unit e ->
          let f = eval_trait e in
          (fun (a, ()) -> f a)
      | Add ->
          U.Pos.add
      | Times ->
          U.Pos.mul
      | Constant x ->
          Value.eval x
      | Trait _ ->
          P.trait_of
      | Dim d ->
          (fun (Zp.Trait.Id (_, v)) -> v.(d))
      | Abs ->
          (fun x -> x |> abs_float |> U.Pos.of_float)
      | Comp_normal (m, v) ->
          let m' = Value.eval m in
          let v' = U.Pos.to_float (Value.eval v) in
          (fun x -> x |> (U.d_normal m' v') |> U.Pos.of_float)
      | Multi_normal (m, cov) ->
         (match Value.eval m with Zp.Trait.Id (g, m'') ->
          let m' = Vec.of_array m'' in
          let cov' = U.mat_to_cov (Mat.of_array (Value.eval cov)) in
          (fun (Zp.Trait.Id (g', v)) ->
            (* is there a custom equal for groups ? *)
            assert (g = g') ;
            v |> Vec.of_array |> (U.d_multi_normal m' cov') |> U.Pos.of_float
          ))
      | Trait_diff ->
          (fun (Zp.Trait.Id (g, v)) (Zp.Trait.Id (g', v')) ->
            assert (g = g') ;
            Zp.Trait.id (g, A.map2 (-.) v v'))
      | Trait_comp_diff ->
          (-.)
    )

    
    type posop = (positive -> positive -> positive)

    let rec trait_to_bound : type a. a Trait.symb -> Bound.symb = Trait.(
      function
      (* can't restrict to posop : need to split Add / Times *)
      | Eval (Eval (Lift_double Add, e), e') ->
          (* both are on the same indiv *)
          let bnd = trait_to_bound e in
          let bnd' = trait_to_bound e' in
          (* FIXME result depends on bnd bnd' : if Max_comp we can combine. *)
          Bound.Add (bnd, bnd')
      | Eval (Eval (Lift_double Times, e), e') ->
          (* both are on the same indiv *)
          let bnd = trait_to_bound e in
          let bnd' = trait_to_bound e' in
          (* FIXME result depends on bnd bnd' : if Max_comp we can combine. *)
          Bound.Times (bnd, bnd')
      | Eval (
          Eval (
            Lift_double (Lift_double Add),
            Lift_in e
          ),
          Lift_out e'
        ) ->
          (* both are on different indivs *)
          (* here it's straightforward *)
          let bnd = trait_to_bound e in
          let bnd' = trait_to_bound e' in
          Bound.Add (bnd, bnd')
      | Eval (
          Eval (
            Lift_double (Lift_double Times),
            Lift_in e
          ),
          Lift_out e'
        ) ->
          (* both are on different indivs *)
          (* here it's straightforward *)
          let bnd = trait_to_bound e in
          let bnd' = trait_to_bound e' in
          Bound.Times (bnd, bnd')
      (* symmetric of previous (pair of) case *)
      | Eval (
          Eval (
            Lift_double (Lift_double Add),
            Lift_out e
          ),
          Lift_in e'
        ) ->
          let bnd = trait_to_bound e in
          let bnd' = trait_to_bound e' in
          Bound.Add (bnd, bnd')
      | Eval (
          Eval (
            Lift_double (Lift_double Times),
            Lift_out e
          ),
          Lift_in e'
        ) ->
          let bnd = trait_to_bound e in
          let bnd' = trait_to_bound e' in
          Bound.Times (bnd, bnd')
      (* we need to protect ourselves against bad uses of
       * Trait_diff and Trait_comp_diff *)
      | Eval (Lift_fun (Lift_fun Abs), Eval (
          Eval (
            Lift_double (Lift_double Trait_comp_diff),
            Lift_in _
          ),
          Lift_out _
        )) ->
          raise Not_boundable
      | Eval (Lift_fun (Lift_fun Abs), Eval (
          Eval (
            Lift_double (Lift_double Trait_comp_diff),
            Lift_out _
          ),
          Lift_in _
        )) ->
          raise Not_boundable
      | Eval (Lift_fun (Lift_fun Abs), Eval (Lift_fun (Lift_fun (Dim _)), Eval (
          Eval (
            Lift_double (Lift_double Trait_diff),
            Lift_in _
          ),
          Lift_out _
        ))) ->
          raise Not_boundable
      (* then we handle the correct uses with Comp_normal / Multi_normal *)
      | Eval (Comp_normal (m, v), _) ->
          Bound.Constant (Value.map (fun v' ->
            v'
            |> U.Pos.to_float
            |> (fun x -> U.d_normal 0. x 0.)
            |> U.Pos.of_float
          ) v)
      | Eval (Multi_normal (m, cov), _) ->
          Bound.Constant (Value.map (fun cov' ->
            let cov'' = Mat.of_array cov' in
            let m = Vec.make0 (Mat.dim1 cov'') in
            cov''
            |> U.mat_to_cov
            |> (fun x -> U.d_multi_normal m x m)
            |> U.Pos.of_float
          ) cov)
      (* a simple (but important) case *)
      | Constant x ->
          Bound.Constant x
      (* go through Tuple and Unit *)
      | Tuple e ->
          trait_to_bound e
      | Unit e ->
          trait_to_bound e
      (* then we need to build up the Max_comp cases up from Trait
       * it works in all "single Lift_fun" / "single Lift_double" cases
       * -- actually more generally we want to catch all (id indiv -> positive)
       * cases. how do we do that ?
       * for now let's enumerate then we'll try to be more general *)
      | Eval (Lift_fun Abs, Eval (Lift_fun (Dim d), _)) ->
          (* FIXME this is weird why do we need d ? *)
          (* need stronger guarantee about the type of e 
           * simplest case : it is Trait _
           * but it could also come from Trait_diff etc *)
          Bound.Max_comp (Eval (Lift_fun Abs, Dim d))
      | Eval (Lift_fun Abs, Eval (
          Eval (
            Lift_double Trait_comp_diff,
            Eval (Lift_fun (Dim d), _)
          ),
          Eval (Lift_fun (Dim d'), _)
        )) ->
          Bound.Max_comp (Eval (Lift_fun Abs, Eval (
            Eval (Lift_double Trait_comp_diff, Dim d),
            Dim d')))
      (* catch-all clause *)
      | _ ->
        failwith "Not_implemented"
  )


    let rec eval_bound n = Bound.(
      function
      | Add (e, e') ->
          let scores, f = eval_bound n e in
          let scores', f' = eval_bound (n + L.length scores) e' in
          (scores @ scores'),
          (fun z -> U.Pos.add (f z) (f' z))
      | Times (e, e') ->
          let scores, f = eval_bound n e in
          let scores', f' = eval_bound (n + L.length scores) e' in
          (scores @ scores'),
          (fun z -> U.Pos.mul (f z) (f' z))
      | Constant x ->
          let x' = Value.eval x in
          [],
          (fun _ -> x')
      | Max_comp score ->
          let f = eval_trait score in
          [f],
          (fun z -> f (P.max z n))
    )


    let holling_i a _ = a

    let holling_ii count a h z =
      let pn = U.Pos.of_posint (count z) in
      U.Pos.(a / ((U.Pos.of_float 1.) + (a * h * pn)))

    let holling_iii count a h z =
      let pn = U.Pos.of_posint (count z) in
      U.Pos.((a * pn) / ((U.Pos.of_float 1.) + (a * h * pn * pn)))


    let rec eval_atom :
      type a. a Atom.symb -> (pop -> a) = Atom.(
      function
      | Add (e, e') ->
          let f = eval_atom e in
          let f' = eval_atom e' in
          (fun z -> U.Pos.add (f z) (f' z))
      | Times (e, e') ->
          let f = eval_atom e in
          let f' = eval_atom e' in
          (fun z -> U.Pos.mul (f z) (f' z))
      | Div (e, e') ->
          let f = eval_atom e in
          let f' = eval_atom e' in
          (fun z -> U.Pos.div (f z) (f' z))
      | Constant v ->
          let x = Value.eval v in
          (fun _ -> x)
      | Count_id g ->
          (fun z -> U.Posint.of_int (P.count_id z g))
      | Count_nonid g ->
          (fun z -> U.Posint.of_int (P.count_nonid z g))
      | As_pos e ->
          let f = eval_atom e in
          (fun z -> U.Pos.of_posint (f z))
      | Holling_I { a } ->
          let a' = (Value.eval a) in
          holling_i a'
      | Holling_II { a ; h ; count } ->
          let a' = Value.eval a in
          let h' = Value.eval h in
          let fcount = eval_atom count in
          holling_ii fcount a' h'
      | Holling_III { a ; h ; count } ->
          let a' = Value.eval a in
          let h' = Value.eval h in
          let fcount = eval_atom count in
          holling_iii fcount a' h'
    )




    let eval_event n ev =
      let choose = eval_choose ev.indivs in
      let lin_rate = eval_linear_rate ev.indivs in
      let trait_rate' = eval_trait ev.trait_rate in
      let scores, bound_rate' = eval_bound n ev.bound_rate in
      let atom_rate' = eval_atom ev.atom_rate in
      let modif' = eval_modif ev.modif in
      let bound_rate _ _ z =
        U.Pos.to_float (U.Pos.mul
            (U.Pos.mul (atom_rate' z) (lin_rate z))
            (bound_rate' z)
        )
      in
      (* here we need the result of the draw though *)
      let erase_proba inds _ _ z =
        1. -. U.Pos.to_float (U.Pos.div
          (trait_rate' inds)
          (bound_rate' z)
        )
      in
      let smodif_atom inds = Evn.modif (fun rng _ -> modif' inds rng) in
      let smodif = P.color_atom
        (fun rng _ _ z -> choose rng z)
        (P.erasei erase_proba smodif_atom)
      in (scores, (bound_rate, smodif))



    let compare_of_score (f : 'a -> positive) x x' =
      compare (f x) (f x')

    let eval_model { groups ; events } =
      let rec f scores rate_smodifs n =
        function
        | Nil_event ->
            (scores, rate_smodifs)
        | And_event (ev, evs) ->
            let (scores', rate_smodif) = eval_event n ev in
            f (scores @ scores')
              (rate_smodif :: rate_smodifs)
              (n + L.length scores')
              evs
      in
      let scores, rate_smodifs = f [] [] 0 events in
      let compares = List.map compare_of_score scores in
      (* mem could be the maximum number of indivs for an event *)
      let create = P.create ~compares ~groups in
      let event = Sim.Ctmjp.Sig.Non_auto (
        Sim.Ctmjp.Event.drop_and_color_of_choices rate_smodifs
      ) in (create, event)
  end


(* Examples *)
(*

let ev e e' =
  Trait.Eval (e, e')

let chain e e' =
  Trait.Eval (Trait.Lift_fun e', e)

let db_chain e e' =
  Trait.Eval (Trait.Lift_fun (Trait.Lift_fun e'), e)

let db_add :
  (('a indiv -> positive) -> ('a indiv -> positive) -> 'a indiv -> positive) Trait.symb =
  Trait.Lift_double Trait.Add


let db_db_add :
  (('a indiv -> 'a indiv -> positive) -> ('a indiv -> 'a indiv -> positive) -> 'a indiv -> 'a indiv -> positive) Trait.symb =
  Trait.Lift_double db_add



(* need to decide if they're id or nonid (indiv) *)
let dim1 = Trait.(
  Lift_in (chain (Trait P.trait_of) (chain (Dim 1) Abs))
)

let dim2 = Trait.(
  Lift_out (chain (Trait P.trait_of) (chain (Dim 2) Abs))
)


(* rate is sum of trait comp 1 from indiv 1 and trait comp 2 from indiv 2
 * this should pass bounding :
 * two order functions, and a sum of those values for the total bound *)
let sum_rate = Trait.(
  Tuple (Eval (Eval (db_db_add, dim1), dim2))
)

let cp_normal = Trait.Comp_normal (Literal 0., Literal (Pos.of_float 1.))

let trait_trait_normal = Trait.(
  db_chain Trait_diff (chain (Dim 1) cp_normal)
)

let diff_rate = Trait.(
  Tuple (Eval (
    Eval (
      Lift_double (Lift_double trait_trait_normal),
      Lift_in (Trait P.trait_of)
    ),
    Lift_out (Trait P.trait_of)
  ))
)

*)

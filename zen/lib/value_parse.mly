%parameter<H : sig
  val params : (string, float) BatHashtbl.t
end>

%start <Pos.t Value.t> value

%%

value:
  | e = valexpr ; EOF
    { e }
  ;

%public
valexpr:
  | LPAREN; e = valexpr ; RPAREN
    { e }
  | e1 = valexpr ; PLUS ; e2 = valexpr
    { Value.map2 Pos.add e1 e2 }
  | e1 = valexpr ; MINUS ; e2 = valexpr
    { Value.map2 Pos.sub e1 e2 }
  | e1 = valexpr ; TIMES ; e2 = valexpr
    { Value.map2 Pos.mul e1 e2 }
  | e1 = valexpr ; DIV ; e2 = valexpr
    { Value.map2 Pos.div e1 e2 }
  | v = posval
    { v }
  ;


%public
posval:
  | v = POS
    { v |> Value.literal }
  | name = VAR
    { Value.bind_pos H.params name }
  ;


module A = BatArray

module Zpop = Zen__Pop
module T = Zpop.Trait


let string_of_id_trait (T.Id (g, v)) =
  A.fold_left (Printf.sprintf "%s;%f") g v


let header (groups_id, groups_nonid) =
  [ "t" :: groups_id @ groups_nonid ]


let main model_json run_json =
  let ctxz = Unjson.contextualize run_json in
  let m_pop = match ctxz.mode with
    | `Demography ->
        (module Zpop.Demography : Zpop.COMPLEX)
    | `Genealogy ->
        (module Zpop.Genealogy : Zpop.COMPLEX)
  in
  let module P = (val m_pop : Zpop.COMPLEX) in
  let module Uj = Unjson.Make (P) in
  let module Us = Unsymb.Make (P) in
  let model = Uj.symb_model ctxz.params model_json in
  let create, event = Us.eval_model model in
  let line (gids, gnonids) _ t z =
       (string_of_float t)
    :: ((List.map (fun g -> g |> P.count_id z |> string_of_int) gids)
      @ (List.map (fun g -> g |> P.count_nonid z |> string_of_int) gnonids))
  in
  let header = header model.groups in
  let line = line model.groups in
  let next = Sim.Ctmjp.Gill.integrate event (Util.rng ctxz.seed) in
  let chan = open_out (ctxz.out ^ ".demography.csv") in
  let dt = ctxz.dt in
  let z0 = create ctxz.nindivs in
  let init_id, init_nonid = ctxz.init in
  List.iter (fun (k, x) -> P.add_new_id ?k z0 x) init_id ;
  List.iter (fun (k, x) -> P.add_new_nonid ?k z0 x) init_nonid ;
  let tf = ctxz.tf in
  let tf, zf = Sim.Csv.simulate_until ~header ~line ~next ~chan ?dt () tf z0 in
  (* this should happen only if we want the genealogy *)
  (* FIXME this should come from Tree.genealogy ideally *)
  let chan_gen = open_out (ctxz.out ^ ".genealogy.csv") in
  P.output_genealogy string_of_id_trait chan_gen zf

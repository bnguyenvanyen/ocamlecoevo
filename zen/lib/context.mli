

type 'a key

type 'a value = 'a

type t

val key : 'a value -> string -> 'a key

val create : unit -> t

val register : t -> 'a key -> 'a value -> unit

val lookup : t -> 'a key -> 'a value

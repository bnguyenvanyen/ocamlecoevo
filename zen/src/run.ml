module Yb = Yojson.Basic

let model_r = ref None
let run_r = ref None


let specl = [
  ("-model", Arg.String (fun s -> model_r := Some s),
   "Path to the JSON model to use.") ;
  ("-run", Arg.String (fun s -> run_r := Some s),
   "Path to the JSON run specification.") ;
]


let anon_fun s = Printf.printf "Ignored anonymous argfument : %s" s


let usage_msg = " Run a stochastic simulation of the specified system."


let main () =
  begin
    Arg.parse specl anon_fun usage_msg ;
    match !model_r, !run_r with
    | Some model, Some run ->
        let model_json = Yb.from_file model in
        let run_json = Yb.from_file run in
        Zen.Main.main model_json run_json
    | _ ->
        raise (Arg.Bad "Both '-model' and '-run' must be given.")
  end;;

main ()

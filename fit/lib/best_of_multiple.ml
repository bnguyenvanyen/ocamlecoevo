(** Propose by keeping the best scoring out of [n] moves.
 *
 *  For any ['a Propose.t] proposal [q], this defines
 *  a new valid proposal.
 *)

open Sig


let draw score n proposal rng (x_max, _) =
  let f acc _ =
    let x = Propose.draw_from rng x_max proposal in
    let (z : float), y = score x in
    match acc with
    | None ->
        Some (x, y, z, [])
    | Some (xm, ym, zm, xs) ->
        if z >= zm then
          Some (x, y, z, xm :: xs)
        else
          Some (xm, ym, zm, x :: xs)
  in
  match U.int_fold ~f ~x:None ~n with
  | None ->
      invalid_arg "Best_of_multiple.draw"
  | Some (x, y, z, xs) ->
      (x, y, z, xs)


let log_pd_ratio proposal (x_max, x_others) (x_max', x_others') =
  match Propose.dist proposal with
  | None ->
     invalid_arg "Does not define a dist"
  | Some prop ->
  let qmax = prop x_max in
  let qmax' = prop x_max' in
  let logr = L.fold_left2 (fun logr x x' ->
         Dist.fsum (Dist.log_dens_proba qmax' x)
      -. Dist.fsum (Dist.log_dens_proba qmax x')
      +. logr
    ) 0. (x_max :: x_others) (x_max' :: x_others')
  in
  [logr]


let propose score n (q : _ Propose.t) =
  let draw rng xs =
    let x_max', _, _, x_others' = draw score n q rng xs in
    x_max', x_others'
  in
  let log_pd_ratio xs xs' =
    log_pd_ratio q xs xs'
  in
  let move_to_sample x =
    x
  in
  Propose.Base { draw ; log_pd_ratio ; move_to_sample }

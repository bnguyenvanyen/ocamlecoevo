(** Bayesian inference
 
    Mostly focused on MCMC, but also contains some other basic methods,
    for acceptance-rejection or ABC.

    The basic building blocks are in {!Fit.Dist} and {!Fit.Propose},
    to represent distributions and proposals.

    {!Fit.Mcmc} implements a basic MCMC algorithm,
    while {!Fit.Ram} implements MCMC with adaptive gaussian proposal.
 *)


exception Simulation_error = Sig.Simulation_error
exception Draw_error = Sig.Draw_error

let raise_draw_error e =
  let bt = Printexc.get_raw_backtrace () in
  Printexc.raise_with_backtrace (Draw_error e) bt


(** @canonical Fit.Sig *)
module Sig = Sig

(** @canonical Fit.Csv *)
module Csv = Fit__Csv

(** @canonical Fit.Dist *)
module Dist = Dist

(** @canonical Fit.Propose *)
module Propose = Propose

(** @canonical Fit.Draw_lognormal *)
module Draw_lognormal = Draw_lognormal

(** @canonical Fit.Markov_chain *)
module Markov_chain = Markov_chain

(** @canonical Fit.Sample *)
module Sample = Sample

(** @canonical Fit.Acrej *)
module Acrej = Acrej

(** @canonical Fit.Mh *)
module Mh = Mh

(** @canonical Fit.Ram *)
module Ram = Ram

(** @canonical Fit.Mtm *)
module Mtm = Mtm

(** @canonical Fit.Mcmc *)
module Mcmc = Mcmc

(** @canonical Fit.Am *)
module Am = Am

(** @canonical Fit.Sequential_metropolis *)
module Sequential_metropolis = Sequential_metropolis

(** @canonical Fit.Seqram *)
module Seqram = Seqram

(** @canonical Fit.Seqcustom *)
module Seqcustom = Seqcustom

(** @canonical Fit.Param *)
module Param = Param

(** @canonical Fit.Infer *)
module Infer = Infer

(** @canonical Fit.Term *)
module Term = Term

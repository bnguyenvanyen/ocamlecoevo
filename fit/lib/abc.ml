(** Simple Rejection ABC
    - sample parameters from prior
    - derive data corresponding to parameters from simulation
    - compute distance with data
    - compare with threshold
    - make some output of accepted parameters and distance
    (and sampled data ? )
  *)

open Sig


(* eps *)
type algparam = float


(*
type ('a, 'b, 'c) sampler = ('a, 'b, unit, 'c * 'b * float) S.sampler
*)


let specl = [
  ("-eps", Lift_arg.Float (fun _ x -> x),
   "Threshold distance for acception.")
]

(* everything is accepted *)
let default = infinity


let integrate ?(apar=default) distance prior simdata =
  let sample rng hypar x =
    let par' = Dist.draw_from prior rng hypar in
      try
        let x' = simdata hypar par' in
        let d = distance x x' in
        if d < apar then
          Some (par', x', d)
        else
          None
      with Simulation_error _ ->  (* ignore simulation errors *)
        None
  in sample

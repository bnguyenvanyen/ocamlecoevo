open Sig


let scale dx y =
  exp dx *. y


let diff y y' =
  (* maybe use log1p *)
  log (y' /. y)


let draw ~get ~set dx th =
  th
  |> get
  |> scale dx
  |> set th


(* log_pd_ratio for a scaling proposal
   this actually corresponds to the ratio between two
   lognormal densities *)
let ratio ~get th th' =
  log (get th') -. log (get th)


let if_adapt draw infer =
 if Infer.is_free_adapt infer then
   [draw]
 else
   []


let left_draw draw rng dx (y, z) =
  (draw rng dx y, z)


let right_draw draw rng dx (y, z) =
  (y, draw rng dx z)


let fold_ratio rs =
  (fun x x' -> L.fold_right (fun r logrs -> r x x' :: logrs) rs [])


let left_ratio ratio (y, _) (y', _) =
  ratio y y'


let right_ratio ratio (_, z) (_, z') =
  ratio z z'

(** Sample from some distribution *)

open Sig


type ('a, 'b) dist = ('a, 'b) Dist.t



(* a specialist conv for the bayesian case *)

let bayes_line prior likelihood line data k sample =
  let likd = likelihood data in
  let logprior = Dist.fsum (Dist.log_dens_proba prior sample) in
  let loglik = Dist.log_dens_proba likd sample in
  line k Mh.(
    Return.accepted (fun x -> x) { sample ; logprior ; loglik }
  )


let posterior ?seed ~(output : ('a, 'b) output) ~dist ~n_iter =
  let rng = U.rng seed in
  output.start 0 None ;
  U.int_fold ~f:(fun () i ->
    let x = Dist.draw_from rng dist in
    output.out (n_iter - i) (Some x) ;
    ()
  ) ~x:() ~n:n_iter ;
  output.return n_iter None

open Sig

(* FIXME propagate the U.Float/Int types to return types *)
(* for now _float. can we extend ? *)

(* FIXME should we use richer function return types + Eval
 * instead of passing args ? 
 * We don't even necessarily need Eval.
 * It would make at least Normal proposals and the like look cleaner
 * (no need to go through the constructor each time) *)


(** Distributions, that we can draw from and compute log-likelihoods on. *)
type (_, _) t =
  | Base : 
      { draw : (Random.State.t -> 'a) ;
        log_dens_proba : ('a -> float list) } -> ('a, float list) t
  | Base_poly :
      { draw : (Random.State.t -> 'a) ;
        log_dens_proba : ('a -> 'b) ;
        sum : ('b -> float) ;
        exp : ('b -> 'b) ;
        output : (out_channel -> 'b -> unit) ;
      } -> ('a, 'b) t
  (** User-defined distribution *)
  | Choose :
      'a list -> ('a, float list) t
  (** Uniform choice in list *)
  | Constant :
      'a -> ('a, float list) t  (* careful with floats *)
  (** Certain variable [x]. 
      The likelihood is [0.] for [y <> x]. *)
  | Constant_flat :
      'a -> ('a, float list) t
  (** Certain variable [x].
      The likelihood is always [1.] (improper). *)
  | Bernoulli :
      U.anyproba -> (bool, float list) t
  (** Bernoulli variable of proba [p]. *)
  | Iuniform :
      int * int -> (int, float list) t
  (** Uniform variable in integer interval. *)
  | Binomial :
      U.anyposint * U.anyproba -> (int, float list) t
  (** Binomial variable. *)
  | Poisson :
      _ U.anypos -> (int, float list) t
  (** Poisson variable. *)
  | Negbin :
      _ U.anypos * U.anyproba -> (int, float list) t
  (** Negative binomial variable. *)
  | Flat :
      ('a, float list) t
  (** Flat distribution : cannot draw from it,
      and the likelihood is [1.] everywhere. *)
  | Uniform :
      float * float -> (float, float list) t
  (** Uniform distribution in [[a, b]]. *)
  | Exponential :
      _ U.anypos -> (float, float list) t
  (** Exponential distribution with rate [lbd]. *)
  | Normal :
      float * _ U.anypos -> (float, float list) t
  (** Normal distribution. *)
  | Lognormal :
      float * _ U.anypos -> (float, float list) t
  (** Lognormal distribution. *)
  | Gamma :
      _ U.anypos * _ U.anypos -> (float, float list) t
  (** Gamma distribution with shape [alpha] and rate [beta] *)
  | Dirichlet :
      _ U.anypos list -> (float list, float list) t
  (** Dirichlet distribution with concentration parameters [alphas] *)
  | Bound : {
      low : float ;
      high : float ;
      probin : float ;
      base : (float, float list) t
    } -> (float, float list) t
  (** Bounded version of [base] in [[low, high]],
      with [P_base([low, high]) = probin]. *)
  | Multinormal : {
      u : Lac.Vec.t ;
      x : Lac.Vec.t ;
      mean : Lac.Vec.t ;
      cov : U.covariance ;
    } -> (Lac.Vec.t, float list) t
  (** Multinormal distribution *)
  | Multinormal_center : {
      u : Lac.Vec.t ;
      x : Lac.Vec.t ;
      cov : U.covariance ;
    } -> (Lac.Vec.t, float list) t
  (** Proposal multinormal distribution *)
  | Option :
      U.anyneg * ('a, float list) t -> ('a option, float list) t
  (** [Option (logp, d)] is [None] with probability [exp logp],
   *  and otherwise [Some d]. *)
  | Map :
      ('a -> 'b) * ('b -> 'a) * ('b, 'c) t -> ('a, 'c) t
  (** Map from a ['b t] to a ['a t] *)
  | Also : {
      get : ('a -> 'b) ;
      set : ('a -> 'b -> 'a) ;
      sub : ('b, float list) t ;
      base : ('a, float list) t
    } -> ('a, float list) t
  (** Draw from [base] distribution, then from [sub] *)
  | Also_dep : {
      get : ('a -> 'b) ;
      set : ('a -> 'b -> 'a) ;
      sub : ('a -> ('b, float list) t) ;
      base : ('a, float list) t
    } -> ('a, float list) t
  (** Draw from [base] then from [sub] which depends on the result
   *  from [base] *)
  | Joint : {
      first : ('b, float list) t ;
      second : ('b -> ('c, float list) t) ;
      get : ('a -> 'b * 'c) ;
      combine : ('b -> 'c -> 'a) ;
    } -> ('a, float list) t
  | Joint_indep : {
      first : ('b, float list) t ;
      second : ('c, float list) t ;
      get : ('a -> 'b * 'c) ;
      combine : ('b -> 'c -> 'a) ;
    } -> ('a, float list) t


type ('a, 'b) dist = ('a, 'b) t


let variance x : _ U.pos = F.of_float_unsafe x


let rec draw_from : type v w. Random.State.t -> (v, w) dist -> v =
  fun rng ->
  function
  | Base { draw ; _ } ->
      draw rng
  | Base_poly { draw ; _ } ->
      draw rng
  | Choose l ->
      let n = Random.int (List.length l) in
      List.nth l n
  | Constant x ->
      x
  | Constant_flat x ->
      x
  | Bernoulli p ->
      U.rand_bernoulli ~rng p
  | Iuniform (low, high) ->
      (high - low)
      |> U.Int.Pos.of_int
      |> U.rand_int ~rng
      |> U.Int.to_int
      |> (fun n -> low + n)
  | Binomial (n, p) ->
      U.rand_binomial ~rng n p
  | Poisson lbd ->
      (* for now we don't propagate the U types *)
      U.Int.to_int (U.rand_poisson ~rng lbd)
  | Negbin (r, p) ->
      U.Int.to_int (U.rand_negbin ~rng r p)
  | Flat ->
      raise (Draw_error (Failure "invalid flat distribution"))
  | Uniform (low, high) ->
      (high -. low)
      |> F.Pos.of_float
      |> U.rand_float ~rng
      |> F.to_float
      |> (fun x -> low +. x)
  | Exponential lbd ->
      F.to_float (U.rand_exp ~rng lbd)
  | Normal (m, v) ->
      U.rand_normal ~rng m v
  | Lognormal (m, v) ->
      U.rand_lognormal ~rng m v
  | Gamma (alpha, beta) ->
      F.to_float (U.rand_gamma ~rng alpha beta)
  | Dirichlet alphas ->
      L.map F.to_float (U.rand_dirichlet ~rng alphas)
  | Bound { low ; high ; base ; _ } as d ->
      let x = draw_from rng base in
      (* ! doesn't necessarily terminate *)
      if (low <= x) && (x <= high) then x else draw_from rng d
  | Multinormal { u ; x ; mean ; cov } ->
      begin match U.rand_multi_normal ~rng ~m:mean ~cov ~u x with
      | x ->
          x
      | exception Failure _ -> (* assume Cholesky failure *)
          let bt = Printexc.get_raw_backtrace () in
          Printexc.raise_with_backtrace (Draw_error (Failure
            "invalid covariance matrix for Cholesky decomposition"
          )) bt
      end
  | Multinormal_center { u ; x ; cov } ->
      begin match U.rand_multi_normal_center ~rng ~cov ~u x with
      | () ->
          x
      | exception Failure _ ->
          let bt = Printexc.get_raw_backtrace () in
          Printexc.raise_with_backtrace (Draw_error (Failure
            "invalid covariance matrix for Cholesky decomposition"
          )) bt
      end
  | Option (logp, d') ->
      if U.rand_bernoulli ~rng (F.Neg.exp logp) then
        None
      else
        Some (draw_from rng d')
  | Map (_, g, d) ->
      g (draw_from rng d)
  | Also { set ; sub ; base ; _ } ->
      set (draw_from rng base) (draw_from rng sub)
  | Also_dep { set ; sub ; base ; _ } ->
      let a = draw_from rng base in
      let b = draw_from rng (sub a) in
      set a b
  | Joint { first ; second ; combine ; _ } ->
      let a = draw_from rng first in
      let b = draw_from rng (second a) in
      combine a b
  | Joint_indep { first ; second ; combine ; _ } ->
      let a = draw_from rng first in
      let b = draw_from rng second in
      combine a b


let partmap2 f l l' =
  let rec g nl l l' =
    match l, l' with
    | [], [] -> nl
    | _ :: _, [] -> (List.rev l) @ nl
    | [], _ :: _ -> (List.rev l') @ nl
    | x :: tl, x' :: tl' -> g ((f x x') :: nl) tl tl'
  in List.rev (g [] l l')


let partsub ldp ldp' =
  let dn = L.length ldp - L.length ldp' in
  if dn = 0 then
    L.map2 (-.) ldp ldp'
  else if dn > 0 then
    let zeros = L.make dn 0. in
    L.map2 (-.) ldp (ldp' @ zeros)
  else
    let zeros = L.make (~- dn) 0. in
    L.map2 (-.) (ldp @ zeros) ldp'


let rec string_of : type v w. (v, w) dist -> string =
  function
  | Base _ ->
      "_"
  | Base_poly _ ->
      "_"
  | Choose _ ->
      "U(l)"
  | Constant _ ->
      "delta"
  | Constant_flat _ ->
      "_delta"
  | Bernoulli p ->
      Printf.sprintf "B(%f)" (F.to_float p)
  | Iuniform (low, high) ->
      Printf.sprintf "IU([%i,%i])" low high
  | Binomial (n, p) ->
      Printf.sprintf "B(%i,%f)" (U.Int.to_int n) (F.to_float p)
  | Poisson lbd ->
      Printf.sprintf "P(%f)" (F.to_float lbd)
  | Negbin (r, p) ->
      Printf.sprintf "NB(%f,%f" (F.to_float r) (F.to_float p)
  | Flat ->
      "flat"
  | Uniform (low, high) ->
      Printf.sprintf "U([%f,%f])" low high
  | Exponential lbd ->
      Printf.sprintf "E(%f)" (F.to_float lbd)
  | Normal (m, v) ->
      Printf.sprintf "N(%f,%f)" m (F.to_float v)
  | Lognormal (m, v) ->
      Printf.sprintf "LN(%f,%f)" m (F.to_float v)
  | Gamma (alpha, beta) ->
      Printf.sprintf "G(%f,%f)" (F.to_float alpha) (F.to_float beta)
  | Dirichlet alphas ->
      let out = BatIO.output_string () in
      L.print F.print out alphas ;
      let l = BatIO.close_out out in
      Printf.sprintf "Dir%s" l
  | Bound { low ; high ; base ; _ } ->
      Printf.sprintf "%s AND IN [%f,%f]" (string_of base) low high
  | Multinormal _ ->
      (* FIXME print m and cov *)
      "MN"
  | Multinormal_center _ ->
      "MNC"
  | Option (_, d') ->
      Printf.sprintf "?(%s)" (string_of d')
  | Map (_, _, d) ->
      Printf.sprintf "Map(%s)" (string_of d)
  | Also { sub ; base ; _ } ->
      Printf.sprintf "%s/(%s)" (string_of base) (string_of sub)
  | Also_dep { base ; _ } ->
      Printf.sprintf "%s/(?)" (string_of base)
  | Joint { first ; _ } ->
      Printf.sprintf "Joint(%s, ?)" (string_of first)
  | Joint_indep { first ; second ; _ } ->
      Printf.sprintf "Joint_indep(%s, %s)"
      (string_of first) (string_of second)


(* TODO used very little, so just replace it by exp log_dens_proba ? *)
let rec dens_proba : type v w. (v, w) dist -> v -> w =
  fun d x ->
  match d with
  | Base { log_dens_proba ; _ } ->
      List.map exp (log_dens_proba x)
  | Base_poly { log_dens_proba ; exp ; _ } ->
      exp (log_dens_proba x)
  | Choose l ->
      if List.mem x l then
        [~-. (float (List.length l))]
      else
        [0.]
  | Constant x' ->
      if x = x' then
        [1.]
      else
        [0.]
  | Constant_flat _ ->
      [1.]
  | Bernoulli p ->
      [F.to_float (U.p_bernoulli p x)]
  | Iuniform (low, high) ->
      [F.to_float (U.d_unif_int low high x)]
  | Binomial (n, p) ->
      [F.to_float (U.p_binomial n p x)]
  | Poisson lbd ->
      [F.to_float (U.p_poisson lbd x)]
  | Negbin (r, p) ->
      [F.to_float (U.p_negbin r p x)]
  | Flat ->
      [1.]
  (* density *)
  | Uniform (low, high) ->
      [F.to_float (U.d_unif low high x)]
  | Exponential lbd ->
      [F.to_float (U.d_exp lbd x)]
  | Normal (m, v) ->
      [F.to_float (U.d_normal m v x)]
  | Lognormal (m, v) ->
      [F.to_float (U.d_lognormal m v x)]
  | Gamma (alpha, beta) ->
      [F.to_float (U.d_gamma alpha beta x)]
  | Dirichlet alphas ->
      [F.to_float (U.d_dirichlet alphas x)]
  | Bound { low ; high ; probin ; base } ->
      if (low <= x) && (x <= high) then
        (1. /. probin) :: (dens_proba base x)
      else
        0. :: (dens_proba base x)
  | Multinormal { mean ; cov ; _ } ->
      [F.to_float (U.d_multi_normal mean cov x)]
  | Multinormal_center { cov ; _ } ->
      [F.to_float (U.d_multi_normal_center cov x)]
  | Option (logp, d') ->
      begin match x with
      | Some x ->
          F.to_float (F.Proba.compl (F.Neg.exp logp))
          :: (dens_proba d' x)
      | None ->
          [F.to_float (F.Neg.exp logp)]
      end
  | Map (f, _, d) ->
      dens_proba d (f x)
  | Also { get ; sub ; base ; _ } ->
      (dens_proba base x) @ (dens_proba sub (get x))
  | Also_dep { get ; sub ; base ; _ } ->
      (dens_proba base x) @ (dens_proba (sub x) (get x))
  | Joint { first ; second ; get ; _ } ->
      let a, b = get x in
      (dens_proba first a) @ (dens_proba (second a) b)
  | Joint_indep { first ; second ; get ; _ } ->
      let a, b = get x in
      (dens_proba first a) @ (dens_proba second b)


let rec log_dens_proba : type v w. (v, w) dist -> v -> w =
  fun d x ->
  match d with
  | Base { log_dens_proba ; _ } ->
      log_dens_proba x
  | Base_poly { log_dens_proba ; _ } ->
      log_dens_proba x
  | Choose l ->
      if L.mem x l then
        l
        |> L.length
        |> float
        |> log
        |> Float.neg
        |> (fun x -> [x])
      else
        [neg_infinity]
  | Constant x' ->
      if x = x' then
        [0.]
      else
        [neg_infinity]
  | Constant_flat _ ->
      [0.]
  | Bernoulli p ->
      [F.to_float (U.logp_bernoulli p x)]
  | Iuniform (low, high) ->
      [F.to_float (U.logd_unif_int low high x)]
  | Binomial (n, p) ->
      [F.to_float (U.logp_binomial n p x)]
  | Poisson lbd ->
      [F.to_float (U.logp_poisson lbd x)]
  | Negbin (r, p) ->
      [F.to_float (U.logp_negbin r p x)]
  | Flat ->
      [0.]
  | Uniform (low, high) ->
      [F.to_float (U.logd_unif low high x)]
  | Exponential lbd ->
      [F.to_float (U.logd_exp lbd x)]
  | Normal (m, v) ->
      [F.to_float (U.logd_normal m v x)]
  | Lognormal (m, v) ->
      [F.to_float (U.logd_lognormal m v x)]
  | Gamma (alpha, beta) ->
      [F.to_float (U.logd_gamma alpha beta x)]
  | Dirichlet alphas ->
      [F.to_float (U.logd_dirichlet alphas x)]
  | Bound { low ; high ; probin ; base } ->
      if (low <= x) && (x <= high) then
        (~-. (log probin)) :: (log_dens_proba base x)
      else
        neg_infinity :: (log_dens_proba base x)
  | Multinormal { mean ; cov ; _ } ->
      [F.to_float (U.logd_multi_normal mean cov x)]
  | Multinormal_center { cov ; _ } ->
      [F.to_float (U.logd_multi_normal_center cov x)]
  | Option (logp, d') ->
      begin match x with
      | Some x ->
          (log (F.to_float (F.Proba.compl (F.Neg.exp logp))))
          :: (log_dens_proba d' x)
      | None ->
          [F.to_float logp]
      end
  | Map (f, _, d) ->
      log_dens_proba d (f x)
  | Also { get ; sub ; base ; _ } ->
      (log_dens_proba base x) @ (log_dens_proba sub (get x))
  | Also_dep { get ; sub ; base ; _ } ->
      (log_dens_proba base x) @ (log_dens_proba (sub x) (get x))
  | Joint { first ; second ; get ; _ } ->
      let a, b = get x in
      (log_dens_proba first a) @ (log_dens_proba (second a) b)
  | Joint_indep { first ; second ; get ; _ } ->
      let a, b = get x in
      (log_dens_proba first a) @ (log_dens_proba second b)


let fsum xs =
  L.fold_left (+.) 0. xs


let rec sum : type v w. (v, w) dist -> w -> float =
  (fun d x ->
    match d with
    | Base_poly { sum ; _ } ->
        (* only special branch *)
        sum x
    | Base _ ->
        fsum x
    | Choose _ ->
        fsum x
    | Constant _ ->
        fsum x
    | Constant_flat _ ->
        fsum x
    | Bernoulli _ ->
        fsum x
    | Iuniform _ ->
        fsum x
    | Binomial _ ->
        fsum x
    | Poisson _ ->
        fsum x
    | Negbin _ ->
        fsum x
    | Flat ->
        fsum x
    | Uniform _ ->
        fsum x
    | Exponential _ ->
        fsum x
    | Normal _ ->
        fsum x
    | Lognormal _ ->
        fsum x
    | Gamma _ ->
        fsum x
    | Dirichlet _ ->
        fsum x
    | Bound _ ->
        fsum x
    | Multinormal _ ->
        fsum x
    | Multinormal_center _ ->
        fsum x
    | Option _ ->
        fsum x
    | Also _ ->
        fsum x
    | Also_dep _ ->
        fsum x
    | Joint _ ->
        fsum x
    | Joint_indep _ ->
        fsum x
    | Map (_, _, d) ->
        sum d x
  )


let rec output : type v w. (v, w) dist -> out_channel -> w -> unit =
  (fun d c x ->
    let fout c x =
      let c = BatIO.output_channel c in
      L.print 
        ~first:""
        ~last:""
        ~sep:","
        BatFloat.print
        c
        x
    in
    match d with
    | Base_poly { output ; _ } ->
        output c x
    | Base _ ->
        fout c x
    | Choose _ ->
        fout c x
    | Constant _ ->
        fout c x
    | Constant_flat _ ->
        fout c x
    | Bernoulli _ ->
        fout c x
    | Iuniform _ ->
        fout c x
    | Binomial _ ->
        fout c x
    | Poisson _ ->
        fout c x
    | Negbin _ ->
        fout c x
    | Flat ->
        fout c x
    | Uniform _ ->
        fout c x
    | Exponential _ ->
        fout c x
    | Normal _ ->
        fout c x
    | Lognormal _ ->
        fout c x
    | Gamma _ ->
        fout c x
    | Dirichlet _ ->
        fout c x
    | Bound _ ->
        fout c x
    | Multinormal _ ->
        fout c x
    | Multinormal_center _ ->
        fout c x
    | Option _ ->
        fout c x
    | Also _ ->
        fout c x
    | Also_dep _ ->
        fout c x
    | Joint _ ->
        fout c x
    | Joint_indep _ ->
        fout c x
    | Map (_, _, d) ->
        output d c x
  )


let sample rng ~n d =
  let rec f acc = function
    | 0 -> acc
    | k -> f (draw_from rng d :: acc) (k - 1)
  in f [] n


let eprint ?name values =
  begin match name with
  | None ->
      ()
  | Some s ->
      Printf.eprintf "%s\n" s
  end ;
  List.iter (Printf.eprintf "%f ") values ;
  Printf.eprintf "\n"


let assert_no_neg_infty ?name l =
  try
    List.iter (fun x -> assert (x <> neg_infinity)) l
  with Assert_failure _ as e ->
    begin
      eprint ?name l ;
      raise e
    end


let assert_no_nan ?name l =
  try
    List.iter (fun x -> assert (classify_float x <> FP_nan)) l
  with Assert_failure _ as e ->
    begin
      eprint ?name l ;
      raise e
    end


let and_dist sub base =
  Also {
    get=(fun x -> x) ;
    set=(fun _ x -> x) ;
    sub ;
    base ;
  }


let and_proposal p' p =
  (fun x -> and_dist (p' x) (p x))


let pair first second =
  Joint_indep {
    first ;
    second ;
    get = (fun ab -> ab) ;
    combine = (fun a b -> (a, b)) ;
  }


let trio a b c =
  pair a (pair b c)


let tuple_4 a b c d =
  pair a (trio b c d)


let tuple_5 a b c d e =
  pair a (tuple_4 b c d e)


let tuple_6 a b c d e f =
  pair a (tuple_5 b c d e f)


let joint6 ~get ~combine a b c d e f =
  Joint_indep {
    first = a ;
    second = Joint_indep {
      first = b ;
      second = Joint_indep {
        first = c ;
        second = Joint_indep {
          first = d ;
          second = Joint_indep {
            first = e ;
            second = f ;
            get = (fun ef -> ef) ;
            combine = (fun e f -> (e, f)) ;
          } ;
          get = (fun def -> def) ;
          combine = (fun d ef -> (d, ef)) ;
        } ;
        get = (fun cdef -> cdef) ;
        combine = (fun c def -> (c, def)) ;
      } ;
      get = (fun bcdef -> bcdef) ;
      combine = (fun b cdef -> (b, cdef)) ;
    } ;
    get = (fun j6 ->
      let a, b, c, d, e, f = get j6 in (a, (b, (c, (d, (e, f)))))
    ) ;
    combine = (fun a (b, (c, (d, (e, f)))) -> combine a b c d e f) ;
  }


let vector_getset getsets =
  let n = A.length getsets in
  let v = Lac.Vec.make0 n in
  let get th =
    A.iteri (fun i (get, _) -> v.{i+1} <- get th) getsets ;
    v
  in
  let set th v =
    A.fold_lefti (fun th' i (_, set) -> set th' v.{i+1}) th getsets
  in
  (get, set)


let vector_getset_random getsets =
  let n = A.length getsets in
  let v = Lac.Vec.make0 n in
  let get th =
    A.iteri (fun i (get, _) -> v.{i+1} <- get th) getsets ;
    v
  in
  let set rng th v =
    A.fold_lefti (fun th' i (_, set) -> set rng th' v.{i+1}) th getsets
  in
  (get, set)


let vector_set_trmv getsets triang =
  Printf.eprintf "vector_set_trmv : %i\n%!" (BatArray.length getsets) ;
  let set th v =
    Lac.trmv ~up:false triang v ;
    BatArray.fold_lefti (fun th' i (_, set) -> set th' v.{i+1}) th getsets
  in
  set

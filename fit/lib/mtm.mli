(** Multiple-Try Metropolis

    A sample is chosen among multiple possible draws proportional
    to some weights.
    See Liu et al. 2000, or Yang et al. 2017 for instance.
    We obtain a proposal reversible wrt the distribution with scores as
    density.

    It builds upon one or multiple proposals
 *)

open Sig


(** {1:simple Choose between multiple draws from a single proposal} *)

module Simple :
  sig
    (** This corresponds to the original algorithm from Liu et al. 2000.
        [m] draws are obtained from a simple proposal,
        and the sample is chosen following the weights.
     *)

    (** [draw m density prop] is a reversible draw
        from the distribution with density proportional to [density],
        made by drawing [m] samples
        from the proposal distribution [prop]. *)
    val draw :
      int ->
      ('a -> _ U.anypos) ->
      ('a -> ('a, float list) Dist.t) ->
        (U.rng -> 'a -> 'a)

    (** [dens_proba_ratio density x y] is the ratio of the density
        of the multiple try proposal at [x] over its density at [y],
        taken with respect to [D / d], that is the distribution
        with density [1] where [d x > 0], and density [0] where [d x = 0] *)
    val dens_proba_ratio :
      ('a -> _ U.anypos) ->
        ('a -> 'a -> _ U.pos)

    (** [log_pd_ratio] is the log of {!dens_proba_ratio} *)
    val log_pd_ratio :
      ('a -> _ U.anypos) ->
        ('a -> 'a -> float list)

    (** [proposal m density prop] is the proposal distribution
        which draws with {!draw} and has log ratio {!log_pd_ratio}. *)
    val proposal :
      int ->
      ('a -> _ U.anypos) ->
      ('a -> ('a, float list) Dist.t) ->
        ('a, 'a) Propose.t
  end


(** {1:general Choose between multiple draws from multiple proposals} *)
module General :
  sig
    (** This corresponds to the first algorithm from Yang et al. 2017.
        [m] draws are obtained from [m] different proposals respectively,
        and then a sample is chosen according to some weights,
        so that the resulting proposal distribution is reversible
        wrt the distribution with density proportional to [density].
     *)

    (** [draw ?kernel density proposals] is a reversible draw
        from the distribution with density proportional to [density],
        made by drawing [m] samples from the proposal distributions
        [proposals] respectively.
        [kernel] can be given to control the weights. *)
    val draw :
      ?kernel:('a -> 'a -> _ U.anypos) ->
      ('a -> _ U.anypos) ->
      ('a -> ('a, float list) Dist.t) list ->
        (U.rng -> 'a -> 'a)

    (** [dens_proba_ratio density x y] is the ratio of the density
        of the multiple try proposal at [x] over its density at [y],
        taken with respect to [D / d], that is the distribution
        with density [1] where [d x > 0], and density [0] where [d x = 0] *)
    val dens_proba_ratio :
      ('a -> _ U.anypos) ->
        ('a -> 'a -> _ U.pos)

    (** [log_pd_ratio] is the log of {!dens_proba_ratio} *)
    val log_pd_ratio :
      ('a -> _ U.anypos) ->
        ('a -> 'a -> float list)

    (** [proposal ?kernel density proposals] is the proposal distribution
        which draws with {!draw} and has log ratio {!log_pd_ratio}. *)
    val proposal :
      ?kernel:('a -> 'a -> _ U.anypos) ->
      ('a -> _ U.anypos) ->
      ('a -> ('a, float list) Dist.t) list ->
        ('a, 'a) Propose.t
  end

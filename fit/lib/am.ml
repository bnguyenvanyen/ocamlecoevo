(** Adaptive-Metropolis algorithm (variant)
  
    Roberts, G.O. and Rosenthal, J.S., 2009.
    Examples of adaptive MCMC.
    Journal of Computational and Graphical Statistics,
    18(2), pp.349-367.
  
    Same algorithm as Metropolis-Hastings in Mcmc,
    but the proposal is multivariate normal with a covariance estimated
    from the accepted samples.
  
    This implementation differs from the algorithm in Roberts and Rosenthal
    by the inclusion of an n_batch parameter that
    allows the covariance estimate to depend more on recent samples
    than older samples.
  
    With n_batch > n_iter, we get back the algorithm.
 *)

open Sig


(* we don't directly take a sample, since *we* chose the proposal
 * we do need some kind of map/also from theta to matrix
 *)


(* n_batch should be > 2 * getsets length at least *)
let posterior ~(output : ('a, 'b) output)
              ~prior ?proposal ~getsets ~likelihood ~beta ?n_batch
              ?seed ~n_iter
              par =
  let n = A.length getsets in
  (* let (get, set) = Dist.vector_getset getsets in *)
  let update cov m k v =
    U.update_mean_estimate m k v ;
    U.update_cov_mle cov m k v
  in
  let sampler_of ~beta u v c =
    let multi =
      let draws = A.map (fun (_, set) ->
          (fun _ x th -> set th x)
        ) getsets
      in
      let draw = Propose.vector_draw draws in
      let log_pd_ratio _ _ = [0.] in
      Propose.multi_gaussian_id ~u draw log_pd_ratio (fun x -> x) beta c v
    in
    let base =
      match proposal with
      | None ->
          Propose.Constant_flat
      | Some prop ->
          prop
    in
    let prop =
      Propose.Also {
        get = (fun x -> x) ;
        set = (fun _ x -> x) ;
        sub = multi ;
        base ;
      }
    in Mh.draw prior prop likelihood
  in
  let rng =
    match seed with
    | None -> Random.State.make_self_init ()
    | Some n -> Random.State.make (Array.init 1 (fun _ -> n))
  in
  (* u will contain the most recent standard multi normal sample *)
  let u = Lac.Vec.make0 n in
  (* v will contain the most recent multi-normal sample *)
  let v = Lac.Vec.make0 n in
  (* m will contain the mean estimate of all vs *)
  let m = Lac.Vec.make0 n in
  let c0 = Lac.Mat.identity n in
  let cov = ref (Util.Cov c0) in
  let rec to_the_end smplr smpl j = function
    | 0 ->
        smpl
    | i when i < 0 ->
        invalid_arg "to_the_end : negative i"
    | i ->
        let res = smplr rng smpl in
        output.out (n_iter - i) (res, U.cov_to_mat cov) ;
        (* oevery_cov ~n_thin chan' i (U.cov_to_mat cov) ; *)
        let i' = i - 1 in
        Mh.(match res with
           | Accept { sample ; logprior ; loglik ; _ } ->
               let wsmpl' = { sample ; logprior ; loglik } in
               (* accept *)
               let k =
                 match n_batch with
                 | None ->
                     1 + j
                 | Some n_batch ->
                     (if j < n_batch then
                        1 + j
                      else
                        n_batch + j mod n_batch)
               in
               Printf.eprintf "k=%i\n%!" k ;
               let _ = update cov m k v in
               let smplr' =
                 let c' = U.cov_to_mat cov in
                 if j < 2 * n then
                   (* doesn't use c *)
                   sampler_of ~beta:1. u v c'
                 else
                   sampler_of ~beta u v c'
               in
               let j' = j + 1 in
               to_the_end smplr' wsmpl' j' i'
           | Failed_proposal _
           | Bad_prior _
           | Bad_proposal _
           | Bad_likelihood _
           | Reject _ ->
               (* reject *)
               to_the_end smplr smpl j i')
  in
  let smplr0 = sampler_of ~beta:1. u v c0 in
  let smpl0 = Mh.Sample.weight prior likelihood par in
  output.start
    0
    (Mh.Return.accepted (fun x -> x) smpl0, U.cov_to_mat cov) ;
  (* start from n_iter + 1 to ignore id in the estimate *)
  let smplf = to_the_end smplr0 smpl0 0 n_iter in
  output.return
    n_iter
    (Mh.Return.accepted (fun x -> x) smplf, U.cov_to_mat cov)

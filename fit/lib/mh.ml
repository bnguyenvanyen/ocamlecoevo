(** Metropolis-Hastings proposal

    For the Bayesian setting, where we distinguish between the prior
    which can be computed fast, and the likelihood, which is slow to compute.
    *)


open Sig


type ('a, 'b, 'c) return =
  | Failed_proposal of {
      sample : 'a ;
      logprior : float ;
      loglik : 'c ;
    }
  | Bad_prior of {
      sample : 'a ;
      logprior : float ;
      loglik : 'c ;
      move : 'b ;
      move_logprior : float ;
    }
  | Bad_proposal of {
      sample : 'a ;
      logprior : float ;
      loglik : 'c ;
      move : 'b ;
      move_logprior : float ;
      p'p : float ;
    }
  | Bad_likelihood of {
      sample : 'a ;
      logprior : float ;
      loglik : 'c ;
      move : 'b ;
      move_logprior : float ;
      move_loglik : 'c ;
      p'p : float ;
      qq' : float ;
    }
  | Reject of {
      sample : 'a ;
      logprior : float ;
      loglik : 'c ;
      move : 'b ;
      move_logprior : float ;
      move_loglik : 'c ;
      p'p : float ;
      qq' : float ;
      l'l : float ;
      logv : float ;
      logu : float ;
    }
  | Accept of {
      sample : 'a ;
      logprior : float ;
      loglik : 'c ;
      move : 'b ;
      p'p : float ;
      qq' : float ;
      l'l : float ;
      logv : float ;
      logu : float ;
    }


type ('a, 'b) weighted = {
  sample : 'a ;
  logprior : float ;
  loglik : 'b ; 
}


type ('a, 'b, 'c, 'd) t =
  (('a, 'c) weighted, ('a, 'b, 'd) return) Propose.t


module Sample =
  struct
    let weight prior likelihood sample =
      let logprior =
        match Dist.log_dens_proba prior sample with
        | x ->
            Dist.fsum x
        | exception Simulation_error _ ->
            neg_infinity
      in
      (* TODO find a way to handle exception Simulation_error _ *)
      let loglik = Dist.log_dens_proba likelihood sample in
      { sample ; logprior ; loglik }

    let map f w =
      { w with sample = f w.sample }
  end


module Return =
  struct
    let sample =
      function
      | Failed_proposal { sample ; _ }
      | Bad_proposal { sample ; _ }
      | Bad_prior { sample ; _ }
      | Bad_likelihood { sample ; _ }
      | Reject { sample ; _ }
      | Accept { sample ; _ } ->
          sample

    let move =
      function
      | Failed_proposal _ ->
          invalid_arg "move : failed_proposal"
      | Bad_prior { move ; _ }
      | Bad_proposal { move ; _ }
      | Bad_likelihood { move ; _ }
      | Reject { move ; _ }
      | Accept { move ; _ } ->
          move

    let logprior =
      function
      | Failed_proposal { logprior ; _ }
      | Bad_prior { logprior ; _ }
      | Bad_proposal { logprior ; _ }
      | Bad_likelihood { logprior ; _ }
      | Reject { logprior ; _ }
      | Accept { logprior ; _ } ->
          logprior

    let loglik =
      function
      | Failed_proposal { loglik ; _ }
      | Bad_prior { loglik ; _ }
      | Bad_proposal { loglik ; _ }
      | Bad_likelihood { loglik ; _ }
      | Reject { loglik ; _ }
      | Accept { loglik ; _ } ->
          loglik

    let logv =
      function
      | Failed_proposal _
      | Bad_prior _
      | Bad_proposal _
      | Bad_likelihood _ ->
          neg_infinity
      | Reject { logv ; _ }
      | Accept { logv ; _ } ->
          logv

    let weighted =
      function
      | Failed_proposal { sample ; logprior ; loglik ; _ }
      | Bad_prior { sample ; logprior ; loglik ; _ }
      | Bad_proposal { sample ; logprior ; loglik ; _ }
      | Bad_likelihood { sample ; logprior ; loglik ; _ }
      | Reject { sample ; logprior ; loglik ; _ }
      | Accept { sample ; logprior ; loglik ; _ } ->
          { sample ; logprior ; loglik }

    let accepted f { sample ; logprior ; loglik } =
      Accept {
        sample ;
        move = f sample ;
        logprior ;
        loglik ;
        logu = 0. ;
        p'p = infinity ;
        qq' = infinity ;
        l'l = infinity ;
        logv = infinity ;
      }

    let map f g =
      function
      | Failed_proposal r ->
          Failed_proposal {
            r with sample = f r.sample
          }
      | Bad_prior r ->
          Bad_prior {
            r with sample = f r.sample ;
                   move = g r.move ;
          }
      | Bad_proposal r ->
          Bad_proposal {
            r with sample = f r.sample ;
                   move = g r.move ;
          }
      | Bad_likelihood r ->
          Bad_likelihood {
            r with sample = f r.sample ;
                   move = g r.move ;
          }
      | Reject r ->
          Reject {
            r with sample = f r.sample ;
                   move = g r.move ;
          }
      | Accept r ->
          Accept {
            r with sample = f r.sample ;
                   move = g r.move ;
          } 
  end


let bad_ratio x =
     (x = neg_infinity)
  || (classify_float x = FP_nan)


let draw prior proposal likelihood rng { sample ; loglik ; _ } =
  (* logprior might have changed since the last time *)
  let logprior = Dist.fsum (Dist.log_dens_proba prior sample) in
  match Propose.draw_from rng sample proposal with
  | exception (Draw_error _ | Simulation_error _) ->
      Failed_proposal { sample ; logprior ; loglik }
  | move ->
  let sample' = Propose.move_to_sample proposal move in
  let logprior' = Dist.fsum (Dist.log_dens_proba prior sample') in
  let p'p = logprior' -. logprior in
  if bad_ratio p'p then
    Bad_prior {
      sample ;
      logprior ;
      loglik ;
      move ;
      move_logprior = logprior' ;
    }
  else
  (* proposal from par' *)
  let qq' =
    Dist.fsum (Propose.log_dens_proba_ratio proposal sample move)
  in
  if bad_ratio qq' then
    Bad_proposal {
      sample ;
      logprior ;
      loglik ;
      move ;
      move_logprior = logprior' ;
      p'p ;
    }
  else
    try
      (* FIXME find a way to handle Simulation_error correctly *)
      let loglik' = Dist.log_dens_proba likelihood sample' in
      let l'l =
        Dist.sum likelihood loglik' -. Dist.sum likelihood loglik
      in
      if bad_ratio l'l then
        begin
          (* Dist.eprint ~name:"loglik'" loglik' ; *)
          Bad_likelihood {
            sample ;
            logprior ;
            loglik ;
            move ;
            move_logprior = logprior' ;
            move_loglik = loglik' ;
            p'p ;
            qq' ;
          }
        end
      else
        begin
          let logv = p'p +. qq' +. l'l in
          if logv > 0. then
            Accept {
              sample = sample' ;
              move ;
              logprior = logprior' ;
              loglik = loglik' ;
              logu = 0. ;
              p'p ;
              qq' ;
              l'l ;
              logv ;
            }
          else
            begin
              let logu = F.to_float (F.Pos.log (U.rand_float ~rng F.one)) in
              if logu <= logv then
                Accept {
                  sample = sample' ;
                  move ;
                  logprior = logprior' ;
                  loglik = loglik' ;
                  logu ;
                  p'p ;
                  qq' ;
                  l'l ;
                  logv ;
                }
              else
                Reject {
                  sample ;
                  logprior = logprior ;
                  loglik = loglik ;
                  move ;
                  move_logprior = logprior' ;
                  move_loglik = loglik' ;
                  logu ;
                  p'p ;
                  qq' ;
                  l'l ;
                  logv ;
                }
            end
        end
    with _ as e ->
      Printf.eprintf "caught error during sampling\n%!" ;
      raise e


(* the Metropolis-Hastings proposal samples from
 * the posterior = prior * likelihood,
 * so its log_pd_ratio is the ratio of the posterior density.
 * However I don't really see an example
 * where this should be used in practice.
 *)
let log_pd_ratio likelihood sample move =
  let logp =
    sample.logprior +. Dist.sum likelihood sample.loglik
  in
  let logp' =
    Return.logprior move +. Dist.sum likelihood (Return.loglik move)
  in
  [logp -. logp']


let move_to_sample =
  Return.weighted


let adapt proposal k move =
  match Return.move move with
  | mv ->
      Propose.adapt proposal k mv
  | exception Invalid_argument _ ->
      (* give up on adaptation, not a great solution *)
      ()


let proposal prior proposal likelihood =
  let draw = draw prior proposal likelihood in
  let adapt = adapt proposal in
  let log_pd_ratio = log_pd_ratio likelihood in
  Propose.Adaptive {
    draw ;
    log_pd_ratio ;
    move_to_sample ;
    adapt
  }


module Loop =
  struct
    let draw ~output sample_as_move prior prop likelihood =
      let prop = proposal prior prop likelihood in
      let loop = Markov_chain.loop ~output prop in
      (fun rng (k, smpl) ->
         let move = Return.accepted sample_as_move smpl in
         let move' = loop rng k move in
         (k + 1, Return.weighted move')
      )

    let log_pd_ratio likelihood (_, sample) (_, sample') =
      let logp =
        sample.logprior +. Dist.sum likelihood sample.loglik
      in
      let logp' =
        sample'.logprior +. Dist.sum likelihood sample'.loglik
      in
      [logp -. logp']

    let move_to_sample ksmpl =
      (* or k - 1 ? *)
      ksmpl

    let proposal ~output sample_as_move prior prop likelihood =
      let draw = draw ~output sample_as_move prior prop likelihood in
      let log_pd_ratio = log_pd_ratio likelihood in
      Propose.Base {
        draw ;
        log_pd_ratio ;
        move_to_sample ;
      }
  end

(** Simulate a Markov Chain


    Iterate a proposal, and also perform adaptation side-effects
    at every iteration.
 *)

open Sig


let loop ~(output:(_,_) output) proposal rng k y =
  let x = Propose.move_to_sample proposal y in
  let y' = Propose.draw_from rng x proposal in
  Propose.adapt proposal k y' ;
  output.out k y' ;
  y'


let simulate ~(output:(_,_) output) ?seed proposal y0 niter =
  let rng = U.rng seed in
  output.start 0 y0 ;
  let yf = U.int_fold ~f:(fun y i ->
    loop ~output proposal rng (niter - i) y
  ) ~x:y0 ~n:niter
  in
  output.return niter yf


let direct_simulate ?seed proposal x0 niter =
  let rng = U.rng seed in
  U.int_fold ~f:(fun x i ->
    let k = niter - i in
    let y = Propose.draw_from rng (k, x) proposal in
    let _, x' = Propose.move_to_sample proposal y in
    x'
  ) ~x:x0 ~n:niter

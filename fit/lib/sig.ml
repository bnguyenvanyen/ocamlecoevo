module L = BatList
module A = BatArray

module Lac = Lacaml.D

module U = Util
module F = U.Float


exception Simulation_error of exn
exception Draw_error of exn


type ('a, 'b) output = (int, 'a, 'b) Util.Out.t

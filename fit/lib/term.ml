open Cmdliner
open Sig


(* I think this is slow but it doesn't matter here *)
let fold_fun f =
  L.fold_left (fun g x y -> f x (g y)) (fun y -> y)


let infer_custom tags f =
  let doc =
    "Infer the specified parameter with its custom proposal."
  in
  let arg =
    Arg.(value
      & opt_all (enum tags) []
      & info ["infer-custom"] ~docv:"INFER-CUSTOM" ~doc
    )
  in Term.(const (fold_fun f) $ arg)


let infer_adapt tags f =
  let doc =
    "Infer the specified parameter with an adaptive proposal."
  in
  let arg = 
    Arg.(value
     & opt_all (enum tags) []
     & info ["infer-adapt"] ~docv:"INFER-ADAPT" ~doc
    )
  in Term.(const (fold_fun f) $ arg)


let infer_prior tags f =
  let doc =
    "Infer the specified parameter using its prior as proposal."
  in
  let arg =
    Arg.(value
      & opt_all (enum tags) []
      & info ["infer-prior"] ~docv:"INFER-PRIOR" ~doc
    )
  in Term.(const (fold_fun f) $ arg)


let fix tags f =
  let doc =
    "Fix the specified parameter to its default value."
  in
  let arg =
    Arg.(value
      & opt_all (enum tags) []
      & info ["fix"] ~docv:"FIX" ~doc
    )
  in
  let g l =
    fold_fun (fun tag -> f tag) l
  in
  Term.(const g $ arg)

(* Fit.Param
 *
 * Combine inference over multiple parameters and allow
 * them to be independently inferred or not, adaptively or not,
 * for priors and proposals.
 *)


open Sig


type (_, _) eq = Eq : ('a, 'a) eq


(* For jumps *)

(* set and jump *)
module SJ =
  struct
    (*
     * set should be symmetric in the sense that
     * set dx par = par' <=> set (~-. dx) par' = par
     *)
    type 'par t = {
      set : (float -> 'par -> 'par) ;
      diff : ('par -> 'par -> float) ;
      jump : float ;
    }


    let map (type par) ~get ~set (sj : par t) =
      let set' dx th =
        set th (sj.set dx (get th))
      in
      let diff th th' =
        sj.diff (get th) (get th')
      in
      {
        set = set' ;
        jump = sj.jump ;
        diff ;
      }
  end


(* draw and jump *)
module DJ =
  struct
    type 'par t = {
      draw : (U.rng -> float -> 'par -> 'par) ;
      diff : ('par -> 'par -> float) ;
      (* diff can't contain the full info, or would not need U.rng *)
      jump : float ;
    }

    let of_sj SJ.{ set ; diff ; jump } =
      let draw _ dx th =
        set dx th
      in { draw ; diff ; jump }

    let map (type par) ~get ~set (rj : par t) =
      let draw' rng dx th =
        set th (rj.draw rng dx (get th))
      in
      let diff th th' =
        rj.diff (get th) (get th')
      in
      {
        draw = draw' ;
        jump = rj.jump ;
        diff ;
      }
  end


type _ t =
  (* basic constructors *)
  | Fixed :
      'a t
  | Custom : {
      prior : ('a, float list) Dist.t ;
      proposal : ('a, 'a) Propose.t ;
    } -> 'a t
  | Adaptive_float : {
      prior : (float, float list) Dist.t ;
      jump : float ;
    } -> float t
  | Adaptive : {
      prior : ('a, float list) Dist.t ;
      jumps : 'a SJ.t list ;
    } -> 'a t
  | Random : {
      prior : ('a, float list) Dist.t ;
      log_pd_ratio : ('a -> 'a -> float list) option ;
      jumps : 'a DJ.t list ;
    } -> 'a t


module Depend =
  struct
    type nonrec ('a, 'b) t = {
      get : ('a -> 'b) ;
      set : ('a -> 'b -> 'a) ;
      elt : 'b t ;
    }

    let map
      (type a b c)
      ~(get : (c -> a))
      ~(set : (c -> a -> c))
      (d : (a, b) t)
      : (c, b) t =
      let set (x : c) (y : b) : c =
        set x (d.set (get x) y)
      in
      let get (x : c) : b =
        d.get (get x)
      in
      { d with get ; set }
  end


let dep ~get ~set elt =
  Depend.{ get ; set ; elt }


let prior : type a. a t -> (a, float list) Dist.t =
  function
  | Fixed ->
      invalid_arg "Fit.Param.prior : Fixed"
  | Custom { prior ; _ }
  | Adaptive { prior ; _ }
  | Random { prior ; _ } ->
      prior
  | Adaptive_float { prior ; _ } ->
      prior


let custom_proposal : type a. a t -> (a, a) Propose.t =
  function
  | Adaptive_float _
  | Adaptive _
  | Random _
  | Fixed ->
      Propose.Constant_flat
  | Custom { proposal ; _ } ->
      proposal


let adapt_draw : type a. a t -> (U.rng -> float -> a -> a) list =
  function
  | Fixed
  | Custom _ ->
      []
  | Adaptive_float _ ->
      [(fun _ dx th -> th +. dx)]
  | Adaptive { jumps ; _ } ->
      let f SJ.{ set ; _ } _ dx th =
        set dx th
      in
      (L.map f jumps)
  | Random { jumps ; _ } ->
      let f DJ.{ draw ; _ } rng dx th =
        draw rng dx th
      in
      (L.map f jumps)


let log_pd_ratio : type a. a t -> (a -> a -> float list) option =
  function
  | Fixed
  | Custom _
  | Adaptive_float _
  | Adaptive _ ->
      (* assume symmetrical *)
      None
  | Random { log_pd_ratio ; _ } ->
      log_pd_ratio


let adapt_diff : type a. a t -> (a -> a -> float) list =
  function
  | Fixed
  | Custom _ ->
      []
  | Adaptive_float _ ->
      [(fun th th' -> th' -. th)]
  | Adaptive { jumps ; _ } ->
      L.map (fun SJ.{ diff ; _ } -> diff) jumps
  | Random { jumps ; _ } ->
      L.map (fun DJ.{ diff ; _ } -> diff) jumps


let adapt_jump : type a. a t -> float list =
  function
  | Fixed
  | Custom _ ->
      []
  | Adaptive_float { jump ; _ } ->
      [jump]
  | Adaptive { jumps ; _ } ->
      (L.map (fun SJ.{ jump ; _ } -> jump) jumps)
  | Random { jumps ; _ } ->
      (L.map (fun DJ.{ jump ; _ } -> jump) jumps)
        


module Combine =
  struct
    type ('tag, 'par, 'a) t = 'tag * ('par, 'a) Depend.t

    let map ~get ~set (tag, dep) =
      (tag, Depend.map ~get ~set dep)

    let prior (type a) base ((_, { get ; set ; elt }) :  (_,_,a) t) =
      match elt with
      | Fixed ->
          base
      | Custom _
      | Adaptive _
      | Adaptive_float _
      | Random _ ->
          Dist.Also {
            get ;
            set ;
            sub = prior elt ;
            base
          }

    let custom_proposal (type a) base ((_, { get ; set ; elt }) : (_,_,a) t) =
      match elt with
      | Fixed
      | Adaptive_float _
      | Adaptive _
      | Random _ ->
          base
      | Custom { proposal ; _ } ->
          Propose.Also {
            get ;
            set ;
            sub = proposal ;
            base
          }

    (* adapt_draw and adapt_jump are then used in module List,
     * which needs to reverse to get the right order of elements,
     * so we also need to reverse here,
     * to get the right order for each element *)

    let adapt_draw (type a b) l ((_, { get ; set ; elt }) : (_,a,b) t) =
      let lift draw =
        (fun rng dx th ->
          let y = draw rng dx (get th) in
          set th y
        )
      in
      (* reverse because use in List *)
      (L.map lift (L.rev (adapt_draw elt))) @ l

    let adapt_jump (type a) l ((_, { elt ; _ }) : (_,_,a) t) =
      (* reverse because use in List *)
      (L.rev (adapt_jump elt)) @ l

    let adapt_diff (type a b) (n, diff_in)
      ((_, { get ; elt ; _ }) : (_,a,b) t) =
      let lift diff =
        (fun th th' -> diff (get th) (get th'))
      in
      let diffs = adapt_diff elt in
      let k = L.length diffs in
      if k = 0 then
        (n, diff_in)
      else
        let diff_in' th th' dx =
          diff_in th th' dx ;
          L.iteri (fun j diff ->
            dx.{n + j} <- (lift diff) th th'
          ) diffs
        in
        (n + k, diff_in')
        
    let log_pd_ratio (type a b) ratio ((_, { elt ; get ; _ }) : (_,a,b) t) =
      (* here we only want the adaptive part,
       * we then multiply with the custom part as part of the also
       * (and we assume that we have the right to do this) *)
      match ratio, log_pd_ratio elt with
      | None, None ->
          None
      | Some f, None ->
          Some f
      | None, Some f ->
          Some (fun x x' -> f (get x) (get x'))
      | Some _, Some _ ->
          invalid_arg "more than one non-symmetrical adaptive proposal"

    let infer_tag (type a) tags ((tag, { elt ; _ }) : (_,_,a) t) =
      match elt with
      | Fixed ->
          tags
      | Custom _
      | Adaptive_float _
      | Adaptive _
      | Random _ ->
          tag :: tags

    let adapt_tag (type a) tags ((tag, { elt ; _ }) : (_,_,a) t) =
      match elt with
      | Fixed
      | Custom _ ->
          tags
      | Adaptive_float _
      | Adaptive _
      | Random _ ->
          tag :: tags
  end


module List =
  struct
    type ('tag, 'par, 'a) par = ('tag, 'par, 'a) Combine.t

    type (_, _, _, _) t =
      | [] :
          ('tag, 'par, 'v, 'v) t
      | (::) :
          ('tag, 'par, 'a) par * ('tag, 'par, 'ty, 'v) t ->
            ('tag, 'par, 'a * 'ty, 'v) t

    type ('tag, 'x, 'y) polymap =
      { f : 'a. ('tag, 'x, 'a) par -> ('tag, 'y, 'a) par }

    type ('tag, 'par, 'b) poly =
      { f : 'a. 'b -> ('tag, 'par, 'a) par -> 'b }

    let rec map :
      type a v.
      ('tag, 'x, 'y) polymap -> ('tag, 'x, a, v) t -> ('tag, 'y, a, v) t =
      fun { f } ->
      function
      | [] ->
          []
      | x :: tl ->
          (f x) :: (map { f } tl)

    let rec fold_left :
      type a v. ('tag, 'par, 'b) poly -> 'b -> ('tag, 'par, a, v) t -> 'b =
      fun { f } y ->
      function
      | [] ->
          y
      | x :: tl ->
          fold_left { f } (f y x) tl

    let rec append :
      type a b c.
      ('t, 'p, a, b) t -> ('t, 'p, b, c) t -> ('t, 'p, a, c) t =
      fun l l' ->
      match l with
      | [] ->
          l'
      | h :: tl ->
          h :: append tl l'

    let (@) = append

    let length l =
      fold_left { f = (fun n _ -> n + 1) } 0 l

    let prior th l =
      fold_left
        { f = Combine.prior }
        (Dist.Constant_flat th)
        l

    let custom_proposal l =
      fold_left
        { f = Combine.custom_proposal }
        Propose.Constant_flat
        l

    let adapt_draws l =
      (* conserve original order *)
      L.rev (
        fold_left
          { f = Combine.adapt_draw }
          []
          l
      )

    let vector_draw l =
      l
      |> adapt_draws
      |> A.of_list
      |> Propose.vector_draw ~start:1

    let adapt_jumps l =
      L.rev (
        fold_left
          { f = Combine.adapt_jump }
          []
          l
      )

    let adapt_diff ?(start=1) l =
      snd (
        fold_left
          { f = Combine.adapt_diff }
          (start, (fun _ _ _ -> ()))
          l
      )

    let log_pd_ratio l =
      fold_left
        { f = Combine.log_pd_ratio }
        None
        l

    let infer_tags l =
      L.rev (
        fold_left
          { f = Combine.infer_tag }
          []
          l
      )

    let adapt_tags l =
      L.rev (
        fold_left
          { f = Combine.adapt_tag }
          []
          l
      )

    (* initial Cholesky matrix with jump values *)
    let init_chol l =
      l
      |> adapt_jumps
      |> A.of_list
      |> Lac.Vec.of_array
      |> Lac.Mat.of_diag

    let find_start l tag =
      let exception Break of int in
      let f (type a) i (tag', ({ elt ; _ } : (_, a) Depend.t)) =
        if tag = tag' then
          raise (Break i)
        else
          let n = L.length (adapt_jump elt) in
          i + n
      in
      match fold_left { f } 1 l with
      | _ ->
          None
      | exception Break i ->
          Some i

    let find_range l (tag, Depend.{ elt ; _ }) =
      let n = L.length (adapt_jump elt) in
      (find_start l tag, n)

    (* use values from mat in mat' where it corresponds *)
    let copy_to l mat l' mat' =
      (* fold over rows of mat *)
      ignore (fold_left { f = (fun r pr ->
        let ro', nr = find_range l' pr in
        if nr = 0 then
          (* pr not adaptive in l *)
          r
        else begin
        (* fold over columns of mat *)
        let _ = fold_left { f = (fun c pc ->
          let co', nc = find_range l' pc in
          if nc = 0 then
            (* pc not adaptive in l *)
            c
          else begin
          (* copy block to mat' if appropriate *)
          begin match ro', co' with
          | None, None
          | Some _, None
          | None, Some _ ->
              ()
          | Some r', Some c' ->
              (* over rows *)
              for kr = 0 to nr - 1 do
                (* over columns *)
                for kc = 0 to nc - 1 do
                  mat'.{r' + kr, c' + kc} <- mat.{r + kr, c + kc}
                done
              done
          end
          ;
          c + nc
          end
        ) } 1 l
        in
        r + nr
        end
      ) } 1 l)
  end

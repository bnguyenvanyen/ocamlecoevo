open Sig


type (_, _) t =
  | Base : {
      draw : (Random.State.t -> 'a -> 'b) ;
      (** [draw rng x] is a sample from the proposal *)
      log_pd_ratio : ('a -> 'b -> float list) ;
      (** [log_pd_ratio x x'] is logP(x | x') - logP(x' | x) *)
      move_to_sample : ('b -> 'a) ;
      (** a move should determine a sample *)
    } -> ('a, 'b) t
  | Adaptive : {
      draw : (Random.State.t -> 'a -> 'b) ;
      (** [draw rng x] is a sample from the proposal *)
      log_pd_ratio : ('a -> 'b -> float list) ;
      (** [log_pd_ratio x x'] is logP(x | x') - logP(x' | x) *)
      move_to_sample : ('b -> 'a) ;
      (** a move should determine a sample *)
      adapt : (int -> 'b -> unit) ;
      (** [adapt k move logv] adapts the proposal for step [k]
          for the move [move]. *)
    } -> ('a, 'b) t
  | Conditional : {
      depends : ('a, _) Dist.t ;
      draw : (Random.State.t -> 'a -> 'b -> 'c) ;
      log_pd_ratio : ('b -> 'c -> float list) ;
      (* the log ratio should take depends into account *)
      move_to_sample : ('c -> 'b) ;
      (** a move should determine a sample *)
    } -> ('b, 'c) t
  | Conditional_adaptive : {
      depends : ('a, _) Dist.t ;
      draw : (Random.State.t -> 'a -> 'b -> 'c) ;
      (* draw depending on a value from depends *)
      log_pd_ratio : ('b -> 'c -> float list) ;
      (* the log ratio should take depends into account *)
      move_to_sample : ('c -> 'b) ;
      (** a move should determine a sample *)
      adapt : (int -> 'c -> unit) ;
      (* adapt can affect draw and/or depends *)
    } -> ('b, 'c) t
  | Dist : {
      (* for now limited to ('b, float) Dist.t *)
      dist : ('a -> ('b, float list) Dist.t) ;
      (** distribution of move conditional on sample value *)
      move_to_sample : ('b -> 'a) ;
      (** a move should determine a sample *)
      reverse : ('b -> 'a -> 'b) ;
      (** [reverse y x] is the reverse move
          corresponding to obtaining [x] from [y] *)
    } -> ('a, 'b) t
  | Constant_flat :
      ('a, 'a) t
  | Constant_flat_any : {
      sample_to_move : ('a -> 'b) ;
      move_to_sample : ('b -> 'a) ;
    } -> ('a, 'b) t
  | Option :
      ('a, 'b) t -> ('a option, 'b option) t
  | Choice :
      (('a, 'b) t * _ U.anypos) list -> ('a, 'b * int) t
  | Also : {
      get : ('a -> 'b) ;
      set : ('a -> 'b -> 'a) ;
      sub : ('b, 'b) t ;
      base : ('a, 'a) t
    } -> ('a, 'a) t
    (* dimensions should remain independent *)
  | Also_dep : {
      get : ('a -> 'b) ;
      set : ('a -> 'b -> 'a) ;
      sub : ('a -> ('b, 'b) t) ;
      base : ('a, 'a) t
    } -> ('a, 'a) t
  | Both : {
      first : ('fst, 'fst_mv) t ;
      (** first proposal *)
      second : ('snd, 'snd_mv) t ;
      (** second proposal *)
      get_sample : ('smpl -> 'fst * 'snd) ;
      (** get sub-samples *)
      get_move : ('mv -> 'fst_mv * 'snd_mv) ;
      (** get sub-moves *)
      combine_sample : ('fst -> 'snd -> 'smpl) ;
      (** combine sub-samples *)
      combine_move : ('fst_mv -> 'snd_mv -> 'mv) ;
      (** combine sub-moves *)
    } -> ('smpl, 'mv) t
    (** arbitrary combination of independent proposals *)
  | Both_dep : {
      first : ('fst, 'fst_mv) t ;
      (** first proposal *)
      second : 'fst_mv -> ('snd, 'snd_mv) t ;
      (** second proposal *)
      get_sample : ('smpl -> 'fst * 'snd) ;
      (** get sub-samples *)
      get_move : ('mv -> 'fst_mv * 'snd_mv) ;
      (** get sub-moves *)
      combine_sample : ('fst -> 'snd -> 'smpl) ;
      (** combine sub-samples *)
      combine_move : ('fst_mv -> 'snd_mv -> 'mv) ;
      (** combine sub-moves *)
    } -> ('smpl, 'mv) t
    (** combination of depedent proposals *)
      


let rec move_to_sample : type u v. (u, v) t -> (v -> u) =
  function
  | Base { move_to_sample ; _ }
  | Adaptive { move_to_sample ; _ }
  | Dist { move_to_sample ; _ }
  | Constant_flat_any { move_to_sample ; _ } ->
      move_to_sample
  | Conditional { move_to_sample ; _ }
  | Conditional_adaptive { move_to_sample ; _ } ->
      move_to_sample
  | Choice proposals ->
      (fun (x, i) ->
        let prop, _ = L.at proposals i in
        move_to_sample prop x
      )
  | Option prop ->
      (fun x -> Option.map (move_to_sample prop) x)
  | Constant_flat ->
      (fun x -> x)
  | Also _ ->
      (fun x -> x)
  | Also_dep _ ->
      (fun x -> x)
  | Both { first ; second ; get_move ; combine_sample ; _ } ->
      (fun z' ->
         let x', y' = get_move z' in
         let x = move_to_sample first x' in
         let y = move_to_sample second y' in
         combine_sample x y
      )
  | Both_dep { first ; second ; get_move ; combine_sample ; _ } ->
      (fun z' ->
          let x', y' = get_move z' in
          let x = move_to_sample first x' in
          let y = move_to_sample (second x') y' in
          combine_sample x y
      )


let dist : type u v. (u, v) t -> (u -> (v, float list) Dist.t) option =
  function
  | Dist { dist ; _ } ->
      Some dist
  | Constant_flat ->
      Some (fun x -> Dist.Constant_flat x)
  | Constant_flat_any { sample_to_move ; _ } ->
      Some (fun x -> Dist.Constant_flat (sample_to_move x))
  | Base _
  | Adaptive _
  | Conditional _
  | Conditional_adaptive _
  | Option _
  | Choice _
  | Also _
  | Also_dep _
  | Both _
  | Both_dep _ ->
      None


let rec draw_from : type u v. Random.State.t -> u -> (u, v) t -> v = 
  fun rng x ->
  function
  | Dist { dist ; _ } ->
      Dist.draw_from rng (dist x)
  | Base { draw ; _ }
  | Adaptive { draw ; _ } ->
      draw rng x
  | Conditional { depends ; draw ; _ } ->
      draw rng (Dist.draw_from rng depends) x
  | Conditional_adaptive { depends ; draw ; _ } ->
      draw rng (Dist.draw_from rng depends) x
  | Constant_flat ->
      x
  | Constant_flat_any { sample_to_move ; _ } ->
      sample_to_move x
  | Option prop ->
      U.Option.map (fun y -> draw_from rng y prop) x
  | Choice proposals ->
      (* choose a proposal, and memorize its index *)
      let iproposals = L.mapi (fun i (prop, w) -> ((i, prop), w)) proposals in
      let i, prop = U.rand_choose ~rng iproposals in
      (draw_from rng x prop, i)
  | Also { get ; set ; sub ; base ; _ } ->
      let x' = draw_from rng x base in
      (* Also chains proposals also on the same dimension,
       * so we get on [x'] and not on [x]. *)
      let y = get x' in
      let y' = draw_from rng y sub in
      set x' y'
  | Also_dep { get ; set ; sub ; base } ->
      let x' = draw_from rng x base in
      let y = get x' in
      let y' = draw_from rng y (sub x') in
      set x' y'
  | Both { first ; second ; get_sample ; combine_move ; _ } ->
      let x1, x2 = get_sample x in
      let x1' = draw_from rng x1 first in
      let x2' = draw_from rng x2 second in
      combine_move x1' x2'
  | Both_dep { first ; second ; get_sample ; combine_move ; _ } ->
      let x1, x2 = get_sample x in
      let x1' = draw_from rng x1 first in
      let x2' = draw_from rng x2 (second x1') in
      combine_move x1' x2'


(* log P(x | x') - log P(x' | x) *)
let rec log_dens_proba_ratio :
  type u v. (u, v) t -> u -> v -> float list =
  fun prop x x' ->
  match prop with
  | Dist { dist ; move_to_sample ; reverse } ->
      let ldp =
        Dist.log_dens_proba (dist (move_to_sample x')) (reverse x' x)
      in
      let ldp' =
        Dist.log_dens_proba (dist x) x'
      in
      [Dist.fsum ldp -. Dist.fsum ldp']
  | Base { log_pd_ratio ; _ }
  | Adaptive { log_pd_ratio ; _ }
  | Conditional { log_pd_ratio ; _ }
  | Conditional_adaptive { log_pd_ratio ; _ } ->
      log_pd_ratio x x'
  | Constant_flat
  | Constant_flat_any _ ->
      [0.]
  | Option prop' ->
      begin match x, x' with
      | None, None ->
          [0.]
      | Some _, None
      | None, Some _ ->
          [neg_infinity]
      | Some y, Some y' ->
          log_dens_proba_ratio prop' y y'
      end
  | Choice proposals ->
      (* v tells us what proposal we need to look at *)
      (* if this is used for Metropolis-Hastings, this is ok,
       * as they're all reversible *)
      let x', k = x' in
      let prop, _ = L.at proposals k in
      log_dens_proba_ratio prop x x'
  | Also { get ; sub ; base ; _ } ->
      (* independent *)
        (log_dens_proba_ratio base x x')
      @ (log_dens_proba_ratio sub (get x) (get x'))
  | Also_dep { get ; sub ; base ; _ } ->
        (log_dens_proba_ratio base x x')
      @ (log_dens_proba_ratio (sub x') (get x) (get x'))
  | Both { first ; second ; get_sample ; get_move ; _ } ->
      let x1, x2 = get_sample x in
      let x1', x2' = get_move x' in
        (log_dens_proba_ratio first x1 x1')
      @ (log_dens_proba_ratio second x2 x2')
  | Both_dep { first ; second ; get_sample ; get_move ; _ } ->
      let x1, x2 = get_sample x in
      let x1', x2' = get_move x' in
        (log_dens_proba_ratio first x1 x1')
      @ (log_dens_proba_ratio (second x1') x2 x2')


let rec to_string : type u v. (u, v) t -> string =
  function
  | Base _ ->
      "_"
  | Adaptive _ ->
      "Adaptive"
  | Dist _ ->
      "D"
  | Constant_flat
  | Constant_flat_any _ ->
      "_delta"
  | Option prop ->
      Printf.sprintf "?(%s)" (to_string prop)
  | Choice proposals ->
      let s =
       proposals
       |> L.map (fun (prop, _) -> to_string prop)
       |> L.reduce (fun s s' -> s ^ " ; " ^ s')
      in
      Printf.sprintf "Choice[%s]" s
  | Conditional { depends ; _ } ->
      Printf.sprintf "cond(%s)" (Dist.string_of depends)
  | Conditional_adaptive { depends ; _ } ->
      Printf.sprintf "cond_adapt(%s)" (Dist.string_of depends)
  | Also { sub ; base ; _ } ->
      Printf.sprintf "%s/(%s)" (to_string base) (to_string sub)
  | Also_dep { base ; _ } ->
      Printf.sprintf "%s/(_)" (to_string base)
  | Both { first ; second ; _ } ->
      Printf.sprintf "Both(%s, %s)" (to_string first) (to_string second)
  | Both_dep { first ; _ } ->
      Printf.sprintf "Both_dep(%s, ?)" (to_string first)


let adapt_option adapt k y =
  match y with
  | None ->
      ()
  | Some x' ->
      adapt k x'


let rec adapt : type u v. (u, v) t -> (int -> v -> unit) =
  function
  | Base _
  | Dist _
  | Constant_flat
  | Constant_flat_any _
  | Conditional _ ->
      (fun _ _ -> ())
  | Adaptive { adapt ; _ }
  | Conditional_adaptive { adapt ; _ } ->
      adapt
  | Option prop ->
      (fun k y ->
        match y with
        | None ->
            ()
        | Some x' ->
            adapt prop k x'
      )
  | Choice proposals ->
      (fun k y ->
        let x', i = y in
        let prop, _ = L.at proposals i in
        adapt prop k x'
      )
  | Also { sub ; base ; get ; _ } ->
      (fun k y ->
         adapt sub k (get y) ;
         adapt base k y
      )
  | Also_dep { sub ; base ; get ; _ } ->
      (fun k y ->
         adapt (sub y) k (get y) ;
         adapt base k y
      )
  | Both { first ; second ; get_move ; _ } ->
      (fun k y ->
         let y1, y2 = get_move y in
         adapt first k y1 ;
         adapt second k y2
      )
  | Both_dep { first ; second ; get_move ; _ } ->
      (fun k y ->
         let y1, y2 = get_move y in
         adapt first k y1 ;
         adapt (second y1) k y2
      )


let endomorphism draw log_pd_ratio =
  Base { draw ; log_pd_ratio ; move_to_sample = (fun x -> x) }


let endo_dist d =
  Dist {
    dist = d ;
    move_to_sample = (fun x -> x) ;
    reverse = (fun _ x -> x) ;
  }

 
let independent d =
  endo_dist (fun _ -> d)


let pair first second =
  Both {
    first ;
    second ;
    get_sample = (fun x1x2 -> x1x2) ;
    get_move = (fun x1x2 -> x1x2) ;
    combine_sample = (fun x1 x2 -> (x1, x2)) ;
    combine_move = (fun x1 x2 -> (x1, x2)) ;
  }


let pair_dep first second =
  Both_dep {
    first ;
    second ;
    get_sample = (fun x1x2 -> x1x2) ;
    get_move = (fun x1x2 -> x1x2) ;
    combine_sample = (fun x1 x2 -> (x1, x2)) ;
    combine_move = (fun x1 x2 -> (x1, x2)) ;
  }


let constant_flat_2 () =
  pair Constant_flat Constant_flat

let constant_flat_3 () =
  pair Constant_flat (constant_flat_2 ())

let constant_flat_4 () =
  pair Constant_flat (constant_flat_3 ())

let constant_flat_5 () =
  pair Constant_flat (constant_flat_4 ())


let multi_gaussian_chol ~u draw log_pd_ratio move_to_sample chol x =
  let cov = ref (U.Chol chol) in
  let depends = Dist.Multinormal_center { u ; x ; cov } in
  Conditional {
    depends ;
    draw ;
    log_pd_ratio ;
    move_to_sample
  }


let multi_gaussian_chol_adapt ~u draw log_pd_ratio move_to_sample adapt chol x =
  let cov = ref (U.Chol chol) in
  let depends = Dist.Multinormal_center { u ; x ; cov } in
  Conditional_adaptive {
    depends ;
    draw ;
    log_pd_ratio ;
    move_to_sample ;
    adapt
  }


let multi_gaussian_id ~u draw log_pd_ratio move_to_sample beta chol x =
  let n = Lac.Mat.dim1 chol in
  let chol' = Lac.Mat.identity n in
  Lac.Mat.scal (U.sq (beta *. 0.1) /. float n) chol' ;
  Lac.Mat.axpy ~alpha:(U.sq ((1. -. beta) *. 2.38) /. float n) chol chol' ;
  let cov = ref (U.Cov chol') in
  let depends = Dist.Multinormal_center { u ; x ; cov } in
  Conditional {
    depends ;
    draw ;
    log_pd_ratio ;
    move_to_sample
  }


let vector_draw ?(start=1) draws rng v x =
  A.fold_lefti (fun x' i d -> d rng v.{i + start} x') x draws

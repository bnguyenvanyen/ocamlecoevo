(** Metropolis-Hastings algorithm for Bayesian inference

    For sampling from a posterior distribution prior * likelihood.
    Builds on the implementation of distributions from {!Dist}
    and from proposal distributions from {!Propose}.
 *)


open Sig


(** Call a sampler again and again,
 *  executing some actions along the trajectory. *)


(** [posterior ~output ?seed sample_as_move prior proposal likelihood n_iter paro]
 *  will initialize the chain with [init] and [paro],
 *  then call [sample] [n_iter] times,
 *  with a PRNG initialized from [seed].
 *  The output is controlled from [output]. *)
val posterior :
  output:(('th, 'mv, 'logl) Mh.return, 'ret) output ->
  ?seed : int ->
  ('th -> 'mv) ->
  ('th, float list) Dist.t ->
  ('th, 'mv) Propose.t ->
  ('th, 'logl) Dist.t ->
  n_iter : int ->
  'th ->
    'ret

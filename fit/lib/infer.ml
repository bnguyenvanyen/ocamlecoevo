module P = Param

type t =
  | Free of [`Custom | `Adapt | `Prior]
  | Fixed


let to_string =
  function
  | Fixed ->
      "Fixed"
  | Free `Custom ->
      "Free_Custom"
  | Free `Adapt ->
      "Free_Adapt"
  | Free `Prior ->
      "Free_Prior"


let custom = Free `Custom
let adapt = Free `Adapt
let prior = Free `Prior

let free mode = Free mode
let fixed = Fixed


let is_fixed =
  function
  | Fixed ->
      true
  | Free _ ->
      false


let is_free =
  function
  | Free _ ->
      true
  | Fixed ->
      false


let is_free_prior =
  function
  | Free `Prior ->
      true
  | Free `Custom | Free `Adapt | Fixed ->
      false


let is_free_custom =
  function
  | Free `Custom ->
      true
  | Free `Prior | Free `Adapt | Fixed ->
      false


let is_free_adapt =
  function
  | Free `Adapt ->
      true
  | Free `Prior | Free `Custom | Fixed ->
      false


module Param =
  struct
    let only_custom ~prior ~proposal infr : _ P.t =
      match infr with
      | Free `Custom ->
          Custom { prior ; proposal }
      | Free (`Adapt | `Prior) ->
          invalid_arg "only custom"
      | Fixed ->
          Fixed

    let maybe_adaptive 
      (type a)
      ~prior
      ~proposal
      ~jumps
      infr :
      a P.t =
      match infr with
      | Free adapt ->
          begin match adapt with
          | `Adapt ->
              Adaptive { prior ; jumps }
          | `Custom ->
              Custom { prior ; proposal }
          | `Prior ->
              let proposal = Propose.independent prior in
              Custom { prior ; proposal }
          end
      | Fixed ->
          Fixed

    let maybe_adaptive_random
      (type a)
      ~prior
      ~proposal
      ~jumps
      ~log_pd_ratio
      infr :
      a P.t =
      match infr with
      | Free adapt ->
          begin match adapt with
          | `Adapt ->
              Random { prior ; log_pd_ratio ; jumps }
          | `Custom ->
              Custom { prior ; proposal }
          | `Prior ->
              let proposal = Propose.independent prior in
              Custom { prior ; proposal }
          end
      | Fixed ->
          Fixed

    let maybe_adaptive_float ~prior ~proposal ~jump infr : _ P.t =
      match infr with
      | Free adapt ->
          begin match adapt with
          | `Adapt ->
              Adaptive_float { prior ; jump }
          | `Custom ->
              Custom { prior ; proposal }
          | `Prior ->
              let proposal = Propose.independent prior in
              Custom { prior ; proposal }
          end
      | Fixed ->
          Fixed
  end


let decorate ~tag ~get ~set elt : (_,_,_) P.Combine.t =
  (tag, { get ; set ; elt })


let only_custom ~tag ~get ~set ~prior ~proposal infr =
  decorate
    ~tag ~get ~set
    (Param.only_custom ~prior ~proposal infr)


let maybe_adaptive ~tag ~get ~set ~prior ~proposal ~jumps infr =
  decorate
    ~tag ~get ~set
    (Param.maybe_adaptive ~prior ~proposal ~jumps infr)


let maybe_adaptive_random ~tag ~get ~set ~prior ~proposal ~jumps ~log_pd_ratio infr =
  decorate
    ~tag ~get ~set
    (Param.maybe_adaptive_random ~prior ~proposal ~jumps ~log_pd_ratio infr)
 

let maybe_adaptive_float ~tag ~get ~set ~prior ~proposal ~jump infr =
  decorate
    ~tag ~get ~set
    (Param.maybe_adaptive_float ~prior ~proposal ~jump infr)

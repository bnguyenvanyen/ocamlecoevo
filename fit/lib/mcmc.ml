(* This module implements the basic Metropolis-Hastings MCMC sampler,
   in the Bayesian context, to sample posterior = prior * likelihood *)

open Sig


let posterior
  ~(output : (_, _) output)
  ?seed
  sample_as_move
  prior
  proposal
  likelihood
  ~n_iter
  par =
  let mh_proposal = Mh.proposal prior proposal likelihood in
  let wsmpl0 = Mh.Sample.weight prior likelihood par in
  let res0 = Mh.Return.accepted sample_as_move wsmpl0 in
  Markov_chain.simulate ~output ?seed mh_proposal res0 n_iter

open Sig

(* This cannot directly reuse Mcmc.sample,
 * because the proposal q and the likelihood are interleaved
 *)


(* sim might raise Simulation_error *)
let score sim obs lik =
  let f th x nu =
    match sim th nu x with
    | x_next ->
        let y = obs th x x_next in
        let logl = Dist.log_dens_proba lik y in
        (Dist.fsum logl, x_next)
    | exception Simulation_error _ ->
        (neg_infinity, x)
  in f


let complement n nus =
  L.map (fun nu ->
    (nu, L.init (n - 1) (fun _ -> nu))
  ) nus


module Weighted =
  struct
    let sample augment prior sim obs lik =
      let propagate_sim par z0 nus =
        let _, loglik = L.fold_left (fun (x, logps) (nu, _) ->
            let x_next = sim par nu x in
            let y = obs par x x_next in
            let logp = Dist.log_dens_proba lik y in
            (x_next, logp :: logps)
          ) (augment z0, []) nus
        in
        loglik
      in
      let f th z0 nus : _ Mh.weighted =
        let sample = ((th, z0), nus) in
        let logprior = Dist.fsum (Dist.log_dens_proba prior (th, z0)) in
        let loglik =
          match propagate_sim th z0 nus with
          | logps ->
              logps
          | exception Simulation_error _ ->
              []
        in { sample ; logprior ; loglik }
      in f
  end


let draw augment prior proposal seq_prop sim obs lik n =
  let score = score sim obs lik in
  (* keep the best out of n proposal for one of the nus *)
  let multi_seq_draw par x rng nus =
    Best_of_multiple.draw (score par x) n (seq_prop par x) rng nus
  in
  let multi_seq_ratio par x nus nus' =
    Best_of_multiple.log_pd_ratio (seq_prop par x) nus nus'
  in
  (* chain proposals, sims, and likelihoods *)
  let propagate_sim ~rng par z0 nus =
    let (_, logq_ratio, loglik'), nus' =
      L.fold_left_map (fun (x, logq_ratio, loglik') nus ->
        let nu', x_next, logl', other_nus' =
          multi_seq_draw par x rng nus
        in
        let nus' = (nu', other_nus') in
        let qq' = multi_seq_ratio par x nus nus' in
        (x_next, (qq' @ logq_ratio), (logl' :: loglik')),
        nus'
      ) (augment z0, [], []) nus
    in
    (logq_ratio, loglik', nus')
  in
  let f rng Mh.{ sample ; loglik ; _ } =
    let (th, z0), nus = sample in
    let logprior = Dist.fsum (Dist.log_dens_proba prior (th, z0)) in
    match Propose.draw_from rng (th, z0) proposal with
    | exception Draw_error _ ->
        Mh.Failed_proposal { sample ; logprior ; loglik }
    | move ->
        let th', z0' = Propose.move_to_sample proposal move in
        let logprior' = Dist.fsum (Dist.log_dens_proba prior (th', z0')) in
        let p'p = logprior' -. logprior in
        (* I cheat here : I want to avoid going through propagate_sim,
         * if I already know the prior is bad.
         * But I don't have the real full move yet,
         * so I replace nus' (which I don't have) by nus *)
        if Mh.bad_ratio p'p then
          begin
            Printf.eprintf "logprior = %f\n" logprior ;
            Printf.eprintf "logprior' = %f\n" logprior' ;
            Bad_prior {
              sample ;
              logprior ;
              loglik ;
              move = ((th', z0'), nus) ;
              move_logprior = logprior' ;
            }
          end
        else
        match
          propagate_sim ~rng th' z0' nus
        with
        | exception Simulation_error _ ->
            (* I'd like to handle this better, but this works *)
            Mh.Failed_proposal { sample ; logprior ; loglik }
        | logq_ratio, loglik', nus' ->
        (* full move made of [th'], [x0'], and [nus'] *)
        let move = ((th', z0'), nus') in
        let qq' = Dist.fsum logq_ratio in
        let l'l = Dist.fsum loglik' -. Dist.fsum loglik in
        if Mh.bad_ratio qq' then
          Bad_proposal {
            sample ;
            logprior ;
            loglik ;
            move ;
            move_logprior = logprior' ;
            p'p ;
          }
        else if Mh.bad_ratio l'l then
          Bad_likelihood {
            sample ;
            logprior ;
            loglik ;
            move ;
            move_logprior = logprior' ;
            move_loglik = loglik' ;
            p'p ;
            qq' ;
          }
        else begin
          let logv = p'p +. qq' +. l'l in
          if logv > 0. then
            Accept {
              sample = move ;
              move ;
              logprior = logprior' ;
              loglik = loglik' ;
              logu = 0. ;
              p'p ;
              qq' ;
              l'l ;
              logv ;
            }
          else begin
            let logu = F.to_float (F.Pos.log (U.rand_float ~rng F.one)) in
            if logu <= logv then
              Accept {
                sample = move ;
                move ;
                logprior = logprior' ;
                loglik = loglik' ;
                logu ;
                p'p ;
                qq' ;
                l'l ;
                logv ;
              }
            else
              Reject {
                sample ;
                logprior = logprior ;
                loglik = loglik ;
                move ;
                move_logprior = logprior' ;
                move_loglik = loglik' ;
                logu ;
                p'p ;
                qq' ;
                l'l ;
                logv ;
              }
          end
        end
  in f


let log_pd_ratio likelihood sample move =
  Mh.log_pd_ratio likelihood sample move


let move_to_sample =
  Mh.move_to_sample


let adapt proposal k move =
  let thz0, _ = Mh.Return.move move in
  Propose.adapt proposal k thz0


let proposal augment prior proposal seq_prop sim obs lik n =
  let draw = draw augment prior proposal seq_prop sim obs lik n in
  let log_pd_ratio = log_pd_ratio lik in
  let adapt = adapt proposal in
  Propose.Adaptive {
    draw ;
    log_pd_ratio ;
    move_to_sample ;
    adapt
  }

(** Like Seqram but without RAM *)


open Sig


type ('th, 'z, 'nu) prop = {
  theta_state : ('th * 'z, 'th * 'z) Propose.t ;
  nu : ('nu, 'nu) Propose.t ;
}



let sampler
  (type a b)
  (prior : _ Seqram.prior) (prop : _ prop)
  (augment_state : a -> b) (strip_state : b -> a)
  sim obs lik =
  let simulate th z0 nus =
    let _, rev_zs, logliks = L.fold_left (fun (z, rev_zs, rev_logps) nu ->
      let z_next, loglik =
        try
          let z_next = sim th nu z in
          let y = obs th z z_next in
          let loglik = Dist.fsum (Dist.log_dens_proba lik y) in
          (z_next, loglik)
        with Simulation_error _ ->
          (z, neg_infinity)
      in
      (
        z_next,
        z :: rev_zs,
        loglik :: rev_logps
      )
    ) (augment_state z0, [], []) nus
    in
    (* the final state [zf] is not kept as part of the traj *)
    let zs = L.rev rev_zs in
    (zs, logliks)
  in
  let propose_and_sim ~rng zs nus logliks th' z0' =
    (* simulate and successively propose *)
    (* By folding over triplets of :
     * - [z] the previous iteration starting state for this step
     * - [nu] the previous iteration 'step variables' for this step
     * - [logp] the previous iteration observation likelihood.
     * We build a traj of [z'], of [logp'], and of [nu']
     *)
    Printf.eprintf "propose_and_sim\n%!" ;
    let _, rev_zs', rev_nus', rev_logps', p'p, qq' =
      Seqram.list_fold_left3 (fun (z', rev_zs', rev_nus', rev_logps', p'p, qq') _ nu _ ->
        (* propose nu conditionally *)
        let nu' = Propose.draw_from rng nu prop.nu in
        (* simulate the next z' *)
        let z_next' = sim th' nu' z' in
        (* observation likelihood *)
        let y = obs th' z' z_next' in
        let logp' = Dist.fsum (Dist.log_dens_proba lik y) in
        (* nu prior ratio *)
        let prior_ratio =
             Dist.fsum (Dist.log_dens_proba prior.nu nu')
          -. Dist.fsum (Dist.log_dens_proba prior.nu nu)
        in
        let prop_ratio =
          Dist.fsum (Propose.log_dens_proba_ratio prop.nu nu nu')
        in
        (
         z_next',
         z' :: rev_zs',
         nu' :: rev_nus',
         logp' :: rev_logps',
         (prior_ratio +. p'p),
         (prop_ratio +. qq')
        )
      ) (augment_state z0', [], [], [], 0., 0.) zs nus logliks
    in
    (* the last state [zf'] is not kept as part of the traj *)
    let zs' = L.rev rev_zs' in
    let nus' = L.rev rev_nus' in
    (* not strictly necessary -- only the sum really matters *)
    let loglik' = L.rev rev_logps' in
    (zs', nus', loglik', p'p, qq')
  in
  let init th z0 nus =
    let logprior =
      Dist.fsum (Dist.log_dens_proba prior.theta_state (th, z0))
    in
    let zs, loglik = simulate th z0 nus in
    let sample = ((th, zs), nus) in
    Mh.{ sample ; logprior ; loglik }
  in
  (* The previous trajectory is needed for conditioning *)
  let sample rng k Mh.{ sample ; loglik ; _ } =
    Printf.eprintf "seqcustom sample %i\n%!" k ;
    let (th, zs), nus = sample in
    let z0, tzs =
      match zs with
      | zz0 :: tzs ->
          (* tzs used to cheat in Bad_prior case *)
          strip_state zz0, tzs
      | [] ->
          invalid_arg "empty traj"
    in
    let logprior =
      Dist.fsum (Dist.log_dens_proba prior.theta_state (th, z0))
    in
    let th', z0' = Propose.draw_from rng (th, z0) prop.theta_state in
    let logprior' =
      Dist.fsum (Dist.log_dens_proba prior.theta_state (th', z0'))
    in
    let p'p = logprior' -. logprior in
    (* cheat for move replacement *)
    let pseudo_move = ((th', augment_state z0' :: tzs), nus) in
    if Mh.bad_ratio p'p then
      Mh.Bad_prior {
        sample ;
        logprior ;
        loglik ;
        move = pseudo_move ;
        move_logprior = logprior' ;
      }
    else
    match propose_and_sim ~rng zs nus loglik th' z0' with
    | exception Simulation_error _ ->
        Mh.Bad_proposal {
          sample ;
          logprior ;
          loglik ;
          move = pseudo_move ;
          move_logprior = logprior' ;
          p'p ;
        }
    | zs', nus', loglik', p'p_nus, qq' ->
    (* compute the full logv *)
    let qq' =
         qq'
      +. (Dist.fsum (
        Propose.log_dens_proba_ratio prop.theta_state (th, z0) (th', z0')
        ))
    in
    let move = ((th', zs'), nus') in
    let l'l = Dist.fsum loglik' -. Dist.fsum loglik in
    if Mh.bad_ratio qq' then
      Mh.Bad_proposal {
        sample ;
        logprior ;
        loglik ;
        move ;
        move_logprior = logprior' ;
        p'p ;
      }
    else if Mh.bad_ratio l'l then
      Mh.Bad_likelihood {
        sample ;
        logprior ;
        loglik ;
        move ;
        move_logprior = logprior' ;
        move_loglik = loglik' ;
        p'p ;
        qq' ;
      }
    else begin
      let logv = p'p +. p'p_nus +. l'l +. qq' in
      if logv > 0. then
        Mh.Accept {
          sample = move ;
          move ;
          logprior = logprior' ;
          loglik = loglik' ;
          logu = 0. ;
          p'p ;
          qq' ;
          l'l ;
          logv ;
        }
      else begin
        let logu = F.to_float (F.Pos.log (U.rand_float ~rng F.one)) in
        if logu <= logv then
          Mh.Accept {
            sample = move ;
            move ;
            logprior = logprior' ;
            loglik = loglik' ;
            logu ;
            p'p ;
            qq' ;
            l'l ;
            logv ;
          }
        else
          Mh.Reject {
            sample ;
            logprior = logprior ;
            loglik = loglik ;
            move ;
            move_logprior = logprior' ;
            move_loglik = loglik' ;
            logu ;
            p'p ;
            qq' ;
            l'l ;
            logv ;
          }
      end
    end
  in
  init, sample


(* FIXME missing adaptation ? *)
let posterior
  ~(output : (_, _) output)
  ?seed ~n_iter ~prior ~prop
  augment_state strip_state sim obs lik th z0 nus =
  let init, sample_f =
    sampler
      prior prop
      augment_state strip_state
      sim obs lik
  in
  let rng = U.rng seed in
  let loop k tup =
    let res = sample_f rng k tup in
    output.out k res ;
    match res with
    | Failed_proposal _
    | Bad_prior _
    | Bad_proposal _
    | Bad_likelihood _
    | Reject _ ->
        tup
    | Accept { sample ; logprior ; loglik ; _ } ->
        Mh.{ sample ; logprior ; loglik }
  in
  let smpl0 = init th z0 nus in
  output.start 0 (Mh.Return.accepted (fun x -> x) smpl0) ;
  let smplf = U.int_fold ~f:(fun smpl i ->
    loop (n_iter - i) smpl
  ) ~x:smpl0 ~n:n_iter
  in
  output.return n_iter (Mh.Return.accepted (fun x -> x) smplf)

(** Multiple-Try Metropolis

    A sample is chosen among multiple possible draws proportional
    to some weights.
    See Liu et al. 2000, or Yang et al. 2017 for instance.
    We obtain a proposal reversible wrt the distribution with scores as
    density.

    It builds upon one or multiple proposals
 *)

open Sig


module General =
  struct
    let draw ?(kernel=(fun _ _ -> F.one)) density proposals =
      let pos_sum zs =
        L.fold_left F.Pos.add F.zero zs
      in
      (* weight for b, moving from a *)
      let weight prop b a =
        let qs = Dist.dens_proba (prop a) b in
        F.Pos.Op.(density b * F.Pos.of_float (Dist.fsum qs) * kernel a b)
      in
      let draw rng x =
        (* get draws *)
        let pys = L.map (fun prop ->
          (prop, Dist.draw_from rng (prop x))
        ) proposals
        in
        (* compute the weights (simplest version) *)
        let ws = L.map (fun (prop, y) -> weight prop y x) pys in
        (* choose y *)
        let yws = L.map2i (fun i (_, y) w -> (i, y), w) pys ws in
        let k, y = U.rand_choose ~rng yws in
        (* draw reversals *)
        (* TODO use a Seq *)
        let pxs = L.mapi (fun i (prop, y) ->
          (* draw from y, except for k, where it's x *)
          if k = i then
            (prop, x)
          else
            (prop, Dist.draw_from rng (prop y))
        ) pys
        in
        (* compute reverse weights *)
        (* TODO use a Seq *)
        let ws' = L.map (fun (prop, x) -> weight prop x y) pxs in
        (* compute acceptance ratio *)
        let v = F.Pos.div (pos_sum ws) (pos_sum ws') in
        (* accept or reject *)
        if F.Op.(v > F.one) then
          y
        else if U.rand_bernoulli ~rng (F.Proba.of_pos v) then
          y
        else
          x
      in draw

    (* to be used in Metropolis-Hastings, make the proposal symmetric,
       by compensating for density *)
    let dens_proba_ratio density x y =
      (* compensate density *)
      F.Pos.div (density x) (density y)
      
    let log_pd_ratio density x y =
      [F.to_float (F.log (dens_proba_ratio density x y))]

    let proposal ?kernel density proposals =
      Propose.Base {
        draw = draw ?kernel density proposals ;
        log_pd_ratio = log_pd_ratio density ;
        move_to_sample = (fun x -> x) ;
      }
  end


module Simple =
  struct
    let draw m density proposal =
      let proposals = L.init m (fun _ -> proposal) in
      General.draw density proposals

    let dens_proba_ratio density =
      General.dens_proba_ratio density
   
    let log_pd_ratio density =
      General.log_pd_ratio density

    let proposal m density proposal =
      let proposals = L.init m (fun _ -> proposal) in
      General.proposal density proposals
  end

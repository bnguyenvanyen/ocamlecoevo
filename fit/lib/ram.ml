(** Robust Adaptive Metropolis algorithm
  
    Vihola, Mcmc., 2012.
    Robust adaptive Metropolis algorithm with coerced acceptance rate.
    Statistics and Computing, 22(5), pp.997-1008.

    Adaptation is made by a robust covariance matrix estimation.

    The implementation provides an adaptive multivariate normal proposal,
    to then be used in the Metropolis-Hastings algorithm.
 *)


open Sig


(** [eta ?alpha ~gamma k] is an adaptation weight at iteration [k].
    [alpha] is an optional linear scaling factor,
    and [gamma] is the exponent *)
let eta ?(alpha=1.) ~gamma k =
  (alpha *. (float k)) ** (~-. gamma)


(** [adjust ?k0 ?kf ?alpha ~gamma n] is an adaptation function
    for a system with [n] dimensions, starting from iteration [k0],
    stopping adaptation after iteration [kf],
    and with parameters [alpha] and [gamma] for the weight sequence,
    given by {!eta}. *)
let adjust ?(k0=0) ?kf ?alpha ~gamma n =
  let storage = Lac.Mat.make0 n n in
  (* we re-use memory for x, which is updated by chol_update,
   * then we discard on next copy *)
  let x_mem = Lac.Vec.make0 n in
  (* We make a Cholesky up/down date
   * S S_T +/- f(eta,k) Delta_ratio / || u || ** 2 S U U_T S_T
   * which is
   * S S_T +/- (a S U) * (a S U)_T
   * with
   * a = sqrt(f(eta,k) |Delta_ratio|) / || u ||
   *)
  let adj k triang u dist =
    (* keep a snapshot of triang in storage *)
    let _ = Lac.lacpy ~b:storage triang in
    let delta =
      sqrt ((min 1. (float n *. eta ?alpha ~gamma (k0 + k))) *. abs_float dist)
    in
    (* a *)
    let a = delta /. (Lac.nrm2 u) in
    (* u <- triang * u * delta / ||u|| *)
    (* use x for next operations (stored in x_mem) *)
    let x = Lac.copy ~y:x_mem u in
    (* FIXME if dim is 0 it fails here *)
    Lac.trmv ~up:false triang x ;
    Lac.scal a x ;
    (* x now contains [a S U] *)
    let up =
      if dist > 0. then
        `Up
      else
        `Down
    in
    (* on a downdate, this can create an invalid matrix
     * we force the diagonal elements to be > 0
     * we should actually force the eigenvalues to be in [a,b]
     * to ensure stability *)
    U.chol_update ~up triang x ;
    let dmin = Lac.Vec.min (Lac.Mat.copy_diag triang) in
    let sum = Lac.Mat.sum triang in
    let cfsum = classify_float sum in
    (* if the new triang is bad *)
    if (dmin <= 0.)
    || (cfsum = FP_infinite)
    || (cfsum = FP_nan) then
      (* restore it from storage *)
      ignore (Lac.lacpy ~b:triang storage)
  in
  if n = 0 then
    (fun _ _ _ _ -> ())
  else
    match kf with
    | None ->
        adj
    | Some kf ->
        (fun k triang u dist ->
           if k0 + k >= kf then
             ()
           else
             adj k triang u dist
        )


let distance ~target_ratio ~logv =
  (min 1. (exp logv)) -. target_ratio
  (* atan (logv -. log target_ratio) in *)
  (* exp (logv -. log target_ratio) -. 1. in *)


(* the base multinormal proposal *)
let propose_and_adapt
  ?alpha
  ?(gamma=(2./.3.))
  ?(target_ratio=0.234)
  ?k0 ?kf
  ?(draws_ratio=(fun _ _ -> [0.]))
  ~move_to_sample
  n
  chol
  vdraw =
  (* chol should be a square matrix *)
  assert (Lac.Mat.dim1 chol = Lac.Mat.dim2 chol) ;
  (* u will contain the last multi normal standard draw *)
  let u = Lac.Vec.make0 n in
  (* v will contain the last multi normal centered draw *)
  let v = Lac.Vec.make0 n in
  let adj = adjust ?k0 ?kf ?alpha ~gamma n in
  let update k logv =
    let d = distance ~target_ratio ~logv in
    adj k chol u d
  in
  let multi =
    if n > 0 then
      Propose.multi_gaussian_chol
        ~u
        vdraw
        draws_ratio
        move_to_sample
        chol
        v
    else if n = 0 then
      Propose.Base {
        draw = (fun rng x -> vdraw rng v x) ;
        log_pd_ratio = draws_ratio ;
        move_to_sample ;
      }
    else
      invalid_arg "n negative"
  in
  multi, update


let draw prior proposal likelihood rng sample =
  Mh.draw prior proposal likelihood rng sample


let log_pd_ratio likelihood sample move =
  Mh.log_pd_ratio likelihood sample move


let move_to_sample move =
  Mh.move_to_sample move


let adapt_with f k move =
  f k (Mh.Return.logv move)


let proposal
  ?(adapt=true)
  ?alpha
  ?gamma
  ?target_ratio
  ?k0 ?kf
  ?draws_ratio
  mts
  n
  chol
  prior
  vdraw
  likelihood =
  let multi, upd = propose_and_adapt
    ?gamma
    ?target_ratio
    ?alpha
    ?k0 ?kf
    ?draws_ratio
    ~move_to_sample:mts
    n
    chol
    vdraw
  in
  let draw = draw prior multi likelihood in
  let log_pd_ratio = log_pd_ratio likelihood in
  if adapt then
    let adapt = adapt_with upd in
    Propose.Adaptive {
      draw ;
      log_pd_ratio ;
      move_to_sample ;
      adapt ;
    }
  else
    Propose.Base {
      draw ;
      log_pd_ratio ;
      move_to_sample ;
    }


let posterior
  ~(output : (_, _) output)
  ?seed
  ?adapt
  ?alpha
  ?gamma
  ?target_ratio
  ?k0 ?kf
  ?draws_ratio
  ~n
  ~chol
  ~move_to_sample
  ~sample_as_move
  ~prior
  ~draw
  ~likelihood
  ~n_iter
  par =
  let ram_proposal = proposal
    ?adapt
    ?alpha
    ?gamma
    ?target_ratio
    ?k0 ?kf
    ?draws_ratio
    move_to_sample
    n
    chol
    prior
    draw
    likelihood
  in
  let wsmpl0 = Mh.Sample.weight prior likelihood par in
  let res0 = Mh.Return.accepted sample_as_move wsmpl0 in
  Markov_chain.simulate ~output ?seed ram_proposal res0 n_iter


module Also =
  struct
    let propose_and_adapt
      ?alpha
      ?gamma
      ?target_ratio
      ?k0 ?kf
      ?draws_ratio
      ~move_to_sample
      n
      chol
      vdraw
      proposal =
      let multi, update = propose_and_adapt
        ?alpha
        ?gamma
        ?target_ratio
        ?k0 ?kf
        ?draws_ratio
        ~move_to_sample
        n
        chol
        vdraw
      in
      Propose.Also {
        get = (fun x -> x) ;
        set = (fun _ x -> x) ;
        sub = multi ;
        base = proposal ;
      },
      update

    let draw prior proposal likelihood rng sample =
      Mh.draw prior proposal likelihood rng sample

    let log_pd_ratio likelihood sample move =
      Mh.log_pd_ratio likelihood sample move

    let move_to_sample move =
      Mh.move_to_sample move

    let adapt_with f k move =
      f k (Mh.Return.logv move)

    let proposal
      ?(adapt=true)
      ?alpha
      ?gamma
      ?target_ratio
      ?k0 ?kf
      ?draws_ratio
      mts
      n
      chol
      prior
      vdraw
      proposal
      likelihood =
      let prop, upd = propose_and_adapt
        ?alpha
        ?gamma
        ?target_ratio
        ?k0 ?kf
        ?draws_ratio
        ~move_to_sample:mts
        n
        chol
        vdraw
        proposal
      in
      let draw = draw prior prop likelihood in
      let log_pd_ratio = log_pd_ratio likelihood in
      if adapt then
        let adapt = adapt_with upd in
        Propose.Adaptive {
          draw ;
          log_pd_ratio ;
          move_to_sample ;
          adapt ;
        }
      else
        Propose.Base {
          draw ;
          log_pd_ratio ;
          move_to_sample ;
        }
  end


module Loop =
  struct
    (* At the top to avoid some shadowing *)
    module Also =
      struct
        let draw
          ~output
          ?adapt
          ?alpha
          ?gamma
          ?target_ratio
          ?k0 ?kf
          ?draws_ratio
          ?custom_proposal
          move_to_sample
          sample_as_move
          n
          chol
          prior
          vdraw
          likelihood =
          let prop =
            match custom_proposal with
            | None ->
                proposal
                  ?adapt
                  ?alpha
                  ?gamma
                  ?target_ratio
                  ?k0 ?kf
                  ?draws_ratio
                  move_to_sample
                  n
                  chol
                  prior
                  vdraw
                  likelihood
            | Some cus ->
                Also.proposal
                  ?adapt
                  ?alpha
                  ?gamma
                  ?target_ratio
                  ?k0 ?kf
                  ?draws_ratio
                  move_to_sample
                  n
                  chol
                  prior
                  vdraw
                  cus
                  likelihood
          in
          let loop = Markov_chain.loop ~output prop in
          (fun rng (k, smpl) ->
             let move = Mh.Return.accepted sample_as_move smpl in
             let move' = loop rng k move in
             (k + 1, Mh.Return.weighted move')
          )

        let log_pd_ratio likelihood ksample ksample' =
          Mh.Loop.log_pd_ratio likelihood ksample ksample'

        let move_to_sample ksmpl =
          Mh.Loop.move_to_sample ksmpl
        
        let proposal 
          ~output
          ?adapt
          ?alpha
          ?gamma
          ?target_ratio
          ?k0 ?kf
          ?draws_ratio
          ?custom_proposal
          mts
          sample_as_move
          n
          chol
          prior
          vdraw
          likelihood =
          let draw = draw
            ~output
            ?adapt
            ?alpha
            ?gamma
            ?target_ratio
            ?k0 ?kf
            ?draws_ratio
            ?custom_proposal
            mts
            sample_as_move
            n
            chol
            prior
            vdraw
            likelihood
          in
          let log_pd_ratio = log_pd_ratio likelihood in
          Propose.Base {
            draw ;
            log_pd_ratio ;
            move_to_sample ;
          }
      end

    let draw
      ~output
      ?adapt
      ?alpha
      ?gamma
      ?target_ratio
      ?k0 ?kf
      ?draws_ratio
      move_to_sample
      sample_as_move
      n
      chol
      prior
      vdraw
      likelihood =
      let prop = proposal
        ?adapt
        ?alpha
        ?gamma
        ?target_ratio
        ?k0 ?kf
        ?draws_ratio
        move_to_sample
        n
        chol
        prior
        vdraw
        likelihood
      in
      let loop = Markov_chain.loop ~output prop in
      (fun rng (k, smpl) ->
         let move = Mh.Return.accepted sample_as_move smpl in
         let move' = loop rng k move in
         (k + 1, Mh.Return.weighted move')
      )

    let log_pd_ratio likelihood ksample ksample' =
      Mh.Loop.log_pd_ratio likelihood ksample ksample'

    let move_to_sample ksmpl =
      Mh.Loop.move_to_sample ksmpl
    
    let proposal 
      ~output
      ?adapt
      ?alpha
      ?gamma
      ?target_ratio
      ?k0 ?kf
      ?draws_ratio
      mts
      sample_as_move
      n
      chol
      prior
      vdraw
      likelihood =
      let draw = draw
        ~output
        ?adapt
        ?alpha
        ?gamma
        ?target_ratio
        ?k0 ?kf
        ?draws_ratio
        mts
        sample_as_move
        n
        chol
        prior
        vdraw
        likelihood
      in
      let log_pd_ratio = log_pd_ratio likelihood in
      Propose.Base {
        draw ;
        log_pd_ratio ;
        move_to_sample ;
      }
  end

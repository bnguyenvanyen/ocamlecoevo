(** Clever RAM for timeseries data *)

open Sig


let list_fold_left3 f accum l1 l2 l3 =
  let rec loop f accum l1 l2 l3 =
    match l1, l2, l3 with
    | [], [], [] ->
        accum
    | h1 :: t1, h2 :: t2, h3 :: t3 ->
        loop f (f accum h1 h2 h3) t1 t2 t3
    | _ ->
        invalid_arg "list_fold_left3: list lengths differ"
  in
  match loop f accum l1 l2 l3 with
  | res ->
      res
  | exception (Invalid_argument _ as e) ->
      Printf.eprintf "List lengths : %i, %i, and %i\n"
      (L.length l1) (L.length l2) (L.length l3) ;
      raise e


(* the only proposal is the multi proposal,
 * on theta, then state, then step variables
 * chol should contain the corresponding dimensions in this order,
 * so that we can easily condition on values of the state z *)


type ('th, 'z, 'nu, 'logp, 'lognu) prior = {
  theta_state : ('th * 'z, 'logp) Dist.t ;
  nu : ('nu, 'lognu) Dist.t ;
}


type ('th, 'z, 'nu) draws = {
  theta : (U.rng -> float -> 'th -> 'th) list ;
  state : (U.rng -> float -> 'z -> 'z) list ;
  nu : (U.rng -> float -> 'nu -> 'nu) list ;
}


type ('th, 'z, 'nu) ratio = {
  theta : ('th -> 'th -> float list) option ;
  state : ('z -> 'z -> float list) option ;
  nu : ('nu -> 'nu -> float list) option ;
}


(* FIXME also need custom proposal *)
let sampler (type a b)
  ~gamma ~target_ratio ~p_redraw_nu
  (prior : _ prior) (draws : _ draws) (ratio : _ ratio) chol
  diff_state (augment_state : a -> b) (strip_state : b -> a)
  sim obs lik =
  let n_theta = L.length draws.theta in
  let n_state = L.length draws.state in
  (* Define the proposals we will need *)
  (*   assume chol has the right dims *)
  let n = Lac.Mat.dim1 chol in
  let cov = ref (U.Chol chol) in
  let m = Lac.Vec.make0 n in
  let u = Lac.Vec.make0 n in
  let dx = Lac.Vec.make0 n in
  (* adaptation *)
  let adapt =
    let adj = Ram.adjust ~gamma n in
    (fun k logv ->
      adj k chol u (Ram.distance ~target_ratio ~logv)
    )
  in
  (*   theta + z0 proposal *)
  let stop = n_theta + n_state in
  (* FIXME handle stop is 0 *)
  (*     combine the draws *)
  let draw_thz =
    Propose.vector_draw (A.of_list (
        (L.map (fun d rng dx (th, z) -> (d rng dx th, z)) draws.theta)
      @ (L.map (fun d rng dx (th, z) -> (th, d rng dx z)) draws.state)
    ))
  in
  let propose_thz ~rng thz =
    (*   update u dx *)
    if stop > 0 then begin
      U.rand_multi_normal_center ~rng ~stop ~cov ~u dx ;
      draw_thz rng dx thz
    end else
      thz
  in
  (* nu proposal conditional on successive (theta +) z values *)
  let draw_nu =
    Propose.vector_draw ~start:(stop + 1) (A.of_list draws.nu)
  in
  let propose_nu ~rng z z' nu =
    (* implicitly here we assume that the values corresponding to theta
     * are already correctly set in dx and u *)
    if stop > 0 then begin
      (*   condition on *current* z *)
      diff_state z z' dx ;
      U.condition_on ~stop ~cov ~x:dx u
    end ;
    (*   update u x conditionally *)
    let dx = 
      U.rand_multi_normal_conditional ~rng ~start:(stop + 1) ~m ~cov ~u dx
    in
    draw_nu rng dx nu
  in
  let ratio_nu =
    match ratio.nu with
    | None ->
        (fun _ _ -> 0.)
    | Some f ->
        (fun nu nu' -> L.fold_left (+.) 0. (f nu nu'))
  in
  let simulate th z0 nus =
    let _, rev_zs, logliks = L.fold_left (fun (z, rev_zs, rev_logps) nu ->
      let z_next, loglik =
        try
          let z_next = sim th nu z in
          let y = obs th z z_next in
          let loglik = Dist.fsum (Dist.log_dens_proba lik y) in
          (z_next, loglik)
        with Simulation_error _ ->
          (z, neg_infinity)
      in
      (
        z_next,
        z :: rev_zs,
        loglik :: rev_logps
      )
    ) (augment_state z0, [], []) nus
    in
    (* the final state [zf] is not kept as part of the traj *)
    let zs = L.rev rev_zs in
    (zs, logliks)
  in
  let propose_and_sim ~rng k zs nus logliks th' z0' =
    (* simulate and successively propose *)
    (* By folding over triplets of :
     * - [z] the previous iteration starting state for this step
     * - [nu] the previous iteration 'step variables' for this step
     * - [logp] the previous iteration observation likelihood.
     * We build a traj of [z'], of [logp'], and of [nu']
     *)
    let _, rev_zs', rev_nus', rev_logps', p'p, qq' =
      list_fold_left3 (fun (z', rev_zs', rev_nus', rev_logps', p'p, qq') z nu logp ->
        (* propose nu conditionally *)
        let is_prop_nu = U.rand_bernoulli ~rng p_redraw_nu in
        let nu' =
          if is_prop_nu then
            propose_nu ~rng (strip_state z) (strip_state z') nu
          else
            (* FIXME do I need a copy ? *)
            nu
        in
        (* simulate the next z' *)
        let z_next' = sim th' nu' z' in
        (* observation likelihood *)
        let y = obs th' z' z_next' in
        let logp' = Dist.fsum (Dist.log_dens_proba lik y) in
        (* nu prior ratio *)
        let prior_ratio =
             Dist.fsum (Dist.log_dens_proba prior.nu nu')
          -. Dist.fsum (Dist.log_dens_proba prior.nu nu)
        in
        let prop_ratio = ratio_nu nu nu' in
        if is_prop_nu then begin
          let logv =
               logp'
            -. logp
            +. prior_ratio
            +. prop_ratio
          in
          adapt k logv
        end
        ;
        (
         z_next',
         z' :: rev_zs',
         nu' :: rev_nus',
         logp' :: rev_logps',
         (prior_ratio +. p'p),
         (prop_ratio +. qq')
        )
      ) (augment_state z0', [], [], [], 0., 0.) zs nus logliks
    in
    (* the last state [zf'] is not kept as part of the traj *)
    let zs' = L.rev rev_zs' in
    let nus' = L.rev rev_nus' in
    (* not strictly necessary -- only the sum really matters *)
    let loglik' = L.rev rev_logps' in
    (zs', nus', loglik', p'p, qq')
  in
  let init th z0 nus =
    let logprior =
      Dist.fsum (Dist.log_dens_proba prior.theta_state (th, z0))
    in
    let zs, loglik = simulate th z0 nus in
    let sample = ((th, zs), nus) in
    Mh.{ sample ; logprior ; loglik }
  in
  (* The previous trajectory is needed for conditioning *)
  let sample ~rng k Mh.{ sample ; loglik ; _ } =
    let (th, zs), nus = sample in
    let z0, tzs =
      match zs with
      | zz0 :: tzs ->
          (* tzs used to cheat in Bad_prior case *)
          strip_state zz0, tzs
      | [] ->
          invalid_arg "empty traj"
    in
    let logprior =
      Dist.fsum (Dist.log_dens_proba prior.theta_state (th, z0))
    in
    match propose_thz ~rng (th, z0) with
    | exception (Draw_error _ | Failure _ | Invalid_argument _) ->
        Mh.Failed_proposal {
          sample ;
          logprior ;
          loglik ;
        }
    | th', z0' ->
    let logprior' =
      Dist.fsum (Dist.log_dens_proba prior.theta_state (th', z0'))
    in
    let p'p = logprior' -. logprior in
    (* cheat for move replacement *)
    let pseudo_move = ((th', augment_state z0' :: tzs), nus) in
    if Mh.bad_ratio p'p then
      Mh.Bad_prior {
        sample ;
        logprior ;
        loglik ;
        move = pseudo_move ;
        move_logprior = logprior' ;
      }
    else
    match propose_and_sim ~rng k zs nus loglik th' z0' with
    | exception Draw_error _ ->
        Mh.Bad_proposal {
          sample ;
          logprior ;
          loglik ;
          move = pseudo_move ;
          move_logprior = logprior' ;
          p'p ;
        }
    | exception Simulation_error _ ->
        Mh.Bad_proposal {
          sample ;
          logprior ;
          loglik ;
          move = pseudo_move ;
          move_logprior = logprior' ;
          p'p ;
        }
    | zs', nus', loglik', p'p_nus, qq' ->
    (* compute the full logv *)
    let move = ((th', zs'), nus') in
    let l'l = Dist.fsum loglik' -. Dist.fsum loglik in
    if Mh.bad_ratio qq' then
      Mh.Bad_proposal {
        sample ;
        logprior ;
        loglik ;
        move ;
        move_logprior = logprior' ;
        p'p ;
      }
    else if Mh.bad_ratio l'l then
      Mh.Bad_likelihood {
        sample ;
        logprior ;
        loglik ;
        move ;
        move_logprior = logprior' ;
        move_loglik = loglik' ;
        p'p ;
        qq' ;
      }
    else begin
      let logv = p'p +. p'p_nus +. l'l +. qq' in
      if logv > 0. then
        Mh.Accept {
          sample = move ;
          move ;
          logprior = logprior' ;
          loglik = loglik' ;
          logu = 0. ;
          p'p ;
          qq' ;
          l'l ;
          logv ;
        }
      else begin
        let logu =
          F.to_float (F.Pos.log (U.rand_float ~rng F.one))
        in
        if logu <= logv then
          Mh.Accept {
            sample = move ;
            move ;
            logprior = logprior' ;
            loglik = loglik' ;
            logu ;
            p'p ;
            qq' ;
            l'l ;
            logv ;
          }
        else
          Mh.Reject {
            sample ;
            logprior = logprior ;
            loglik = loglik ;
            move ;
            move_logprior = logprior' ;
            move_loglik = loglik' ;
            logu ;
            p'p ;
            qq' ;
            l'l ;
            logv ;
          }
      end
    end
  in
  init, sample


let posterior
  ~(output : (_, _) output)
  ?(gamma=2.3) ?(target_ratio=0.234) ?(p_redraw_nu=F.one)
  ?seed ~n_iter
  ~prior ~draws ~ratio chol
  diff_state augment_state strip_state
  sim obs lik
  th z0 nus =
  let init, sample_f =
    sampler
      ~gamma ~target_ratio ~p_redraw_nu
      prior draws ratio chol
      diff_state augment_state strip_state
      sim obs lik
  in
  let rng = U.rng seed in
  let loop k tup =
    let res = sample_f ~rng k tup in
    output.out k res ;
    match res with
    | Failed_proposal _
    | Bad_prior _
    | Bad_proposal _
    | Bad_likelihood _
    | Reject _ ->
        tup
    | Accept { sample ; logprior ; loglik ; _ } ->
        Mh.{ sample ; logprior ; loglik }
  in
  let smpl0 = init th z0 nus in
  output.start 0 (Mh.Return.accepted (fun x -> x) smpl0) ;
  let smplf = U.int_fold ~f:(fun smpl i ->
    loop (n_iter - i) smpl
  ) ~x:smpl0 ~n:n_iter
  in
  output.return n_iter (Mh.Return.accepted (fun x -> x) smplf)

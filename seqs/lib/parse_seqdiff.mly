%{
open Sig

let pos_sub n n' = I.Pos.of_anyint (I.sub n n')
%}

%token UNDER
%token <string> SEQ
%token SEMIC
%token RCHEVR
%token LCHEVR
%token <string> INT
%token EOF
%token BRACK
%token BAR


%start <Sig.seqdiff> input
%%

input:
  | UNDER ; EOF
    { Not }
  | n = inte ; BAR ; c1 = uchar ; RCHEVR ; c2 = uchar ; EOF
    { Sub (P n, c1, c2) }
  | n = inte ; RCHEVR ; s = seq ; LCHEVR ; EOF
    { Ins (P n, s) }
  | n = inte ; LCHEVR ; s = seq ; RCHEVR ; EOF
    { Del (P n, s) }
  | n2 = inte ; RCHEVR ; BRACK ; n1 = inte ; SEMIC ; nk = inte ; BRACK ; LCHEVR ; EOF
    { Dup (P n1, P n2, L (pos_sub nk n1)) }
  | n2 = inte ; LCHEVR ; BRACK ; n1 = inte ; SEMIC ; nk = inte ; BRACK ; RCHEVR ; EOF
    { Undup (P n1, P n2, L (pos_sub nk n1)) }
  | n = inte ; LCHEVR ; LCHEVR ; nk = inte ; EOF
    { Inv (P n, L (pos_sub nk n)) }
  ;

inte:
  | n = INT    { I.Pos.of_int (int_of_string n) }
  ;

seq:
  | s = SEQ    { Base.of_string s }
  ;

uchar:
  | c = SEQ    { BatUChar.of_char c.[0] }
  ;

module Sig = Sig
module Io = Io
module Diff = Diff
module Dna = Dna
module Prot = Prot
module Parse = Parse_fasta
module Lex = Lex_fasta

(* also make some functions from Base directly available *)
include Base

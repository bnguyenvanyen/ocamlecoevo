open Sig

exception Stop_codon

type t = aa

let of_char c =
  if c = 'A' then
    Ala
  else if c = 'C' then
    Cys
  else if c = 'D' then
    Asp
  else if c = 'E' then
    Glu
  else if c = 'F' then
    Phe
  else if c = 'G' then
    Gly
  else if c = 'H' then
    His
  else if c = 'I' then
    Ile
  else if c = 'K' then
    Lys
  else if c = 'L' then
    Leu
  else if c = 'M' then
    Met
  else if c = 'N' then
    Asn
  else if c = 'O' then
    Pyl
  else if c = 'P' then
    Pro
  else if c = 'Q' then
    Gln
  else if c = 'R' then
    Arg
  else if c = 'S' then
    Ser
  else if c = 'T' then
    Thr
  else if c = 'U' then
    Sec
  else if c = 'V' then
    Val
  else if c = 'W' then
    Trp
  else if c = 'Y' then
    Tyr
  else
    invalid_arg "invalid character"


let of_uchar uc = of_char @@ BatUChar.char_of @@ uc

let to_char a =
  match a with
  | Ala -> 'A'
  | Cys -> 'C'
  | Asp -> 'D'
  | Glu -> 'E'
  | Phe -> 'F'
  | Gly -> 'G'
  | His -> 'H'
  | Ile -> 'I'
  | Lys -> 'K'
  | Leu -> 'L'
  | Met -> 'M'
  | Asn -> 'N'
  (* special *)
  | Pyl -> 'O'
  | Pro -> 'P'
  | Gln -> 'Q'
  | Arg -> 'R'
  | Ser -> 'S'
  | Thr -> 'T'
  (* special *)
  | Sec -> 'U'
  | Val -> 'V'
  | Trp -> 'W'
  | Tyr -> 'Y'

let to_uchar a = BatUChar.of_char @@ to_char @@ a

let to_int a =
  match a with
  | Ala -> 0
  | Cys -> 1
  | Asp -> 2
  | Glu -> 3
  | Phe -> 4
  | Gly -> 5
  | His -> 6
  | Ile -> 7
  | Lys -> 8
  | Leu -> 9
  | Met -> 10
  | Asn -> 11
  (* special *)
  | Pyl -> 12
  | Pro -> 13
  | Gln -> 14
  | Arg -> 15
  | Ser -> 16
  | Thr -> 17
  (* special *)
  | Sec -> 18
  | Val -> 19
  | Trp -> 20
  | Tyr -> 21

let of_int k =
  match (k mod 22) with
  | 0 -> Ala
  | 1 -> Cys
  | 2 -> Asp
  | 3 -> Glu
  | 4 -> Phe
  | 5 -> Gly
  | 6 -> His
  | 7 -> Ile
  | 8 -> Lys
  | 9 -> Leu
  | 10 -> Met
  | 11 -> Asn
  | 12 -> Pyl
  | 13 -> Pro
  | 14 -> Gln
  | 15 -> Arg
  | 16 -> Ser
  | 17 -> Thr
  | 18 -> Sec
  | 19 -> Val
  | 20 -> Trp
  | 21 -> Tyr
  | _ -> failwith "impossible after mod"


let to_codon a =
  (* do we make a rope (seq) or a dna (nuc) tuple or what ?
   * how do we handle synonyms ? *)
  match a with
  | Ala -> [ (G, C, T) ; (G, C, C) ; (G, C, A) ; (G, C, G) ]
  | Cys -> [ (T, G, T) ; (T, G, C) ]
  | Asp -> [ (G, A, T) ; (G, A, C) ]
  | Glu -> [ (G, A, A) ; (G, A, G) ]
  | Phe -> [ (T, T, T) ; (T, T, C) ]
  | Gly -> [ (G, G, T) ; (G, G, C) ; (G, G, A) ; (G, G, G) ]
  | His -> [ (C, A, T) ; (C, A, C) ]
  | Ile -> [ (A, T, T) ; (A, T, C) ; (A, T, A) ]
  | Lys -> [ (A, A, A) ; (A, A, G) ]
  | Leu -> [ (T, T, A) ; (T, T, G) ; (C, T, T) ; (C, T, C) ; (C, T, A) ; (C, T, G) ]
  | Met -> [ (A, T, G) ]
  | Asn -> [ (A, A, T) ; (A, A, C) ]
  (* special *)
  | Pyl -> []
  | Pro -> [ (C, C, T) ; (C, C, C) ; (C, C, A) ; (C, C, G) ]
  | Gln -> [ (C, A, A) ; (C, A, G) ]
  | Arg -> [ (C, G, T) ; (C, G, C) ; (C, G, A) ; (C, G, G) ; (A, G, A) ; (A, G, G) ]
  | Ser -> [ (T, C, T) ; (T, C, C) ; (T, C, A) ; (T, C, G) ; (A, G, T) ; (A, G, C) ]
  | Thr -> [ (A, C, T) ; (A, C, C) ; (A, C, C) ; (A, C, A) ; (A, C, G) ]
  (* special *)
  | Sec -> []
  | Val -> [ (G, T, T) ; (G, T, C) ; (G, T, A) ; (G, T, G) ]
  | Trp -> [ (T, G, G) ]
  | Tyr -> [ (T, A, T) ; (T, A, C) ]


let of_codon n1 n2 n3 =
  match n1 with
  | T ->
    (match n2 with
     | T ->
       (match n3 with
        | (T | C) -> Phe
        | (A | G) -> Leu)
     | C ->
       Ser
     | A ->
       (match n3 with
        | (T | C) -> Tyr
        | (A | G) -> raise Stop_codon)
     | G ->
       (match n3 with
        | (T | C) -> Cys
        | A -> raise Stop_codon
        | G -> Trp))
  | C ->
    (match n2 with
     | T -> Leu
     | C -> Pro
     | A ->
       (match n3 with
        | (T | C) -> His
        | (A | G) -> Gln)
     | G -> Arg)
  | A ->
    (match n2 with
     | T ->
       (match n3 with
        | (T | C | A) -> Ile
        | G -> Met)
     | C -> Thr
     | A ->
       (match n3 with
        | (T | C) -> Asn
        | (A | G) -> Lys)
     | G ->
       (match n3 with
        | (T | C) -> Ser
        | (A | G) -> Arg))
  | G ->
    (match n2 with
     | T -> Val
     | C -> Ala
     | A ->
       (match n3 with
        | (T | C) -> Asp
        | (A | G) -> Glu)
     | G -> Gly)


let find_start_codon_last n =
  match n with
  | (T | C | A) -> false
  | G -> true

let find_start_codon_second n =
  match n with
  | (C | A | G) -> false
  | T -> true

let find_start_codon_first n =
  match n with
  | (T | C | G) -> false
  | A -> true

let find_start_codon n1 n2 n3 =
  match n1 with
  | (T | C | G) ->
    false
  | A ->
    (match n2 with
     | (C | A | G) ->
       false
     | T ->
       (match n3 with
        | (T | C | A) ->
          false
        | G ->
          true))

type framepos =
  | Start0  (* haven't found the start of a start codon *)
  | Start1  (* have found the first nucleotide A of the start codon *)
  | Start2  (* have found the second nucleotide T of the start codon *)
  | Codon0  (* waiting to read the first nucleotide of a codon *)
  | Codon1 of (nuc -> nuc -> aa) (* waiting to read the second nucleotide of a codon *)
  | Codon2 of (nuc -> aa) (* waiting to read the third nucleotide of a codon *)


let of_dna dnarope =
  (* we assume seq is a 'dna' seq
   * maybe we can fold with a subfunction that accumulates codons
   * and does the needful *)
  (* FIXME what do we do with stop codons *)
  let f_fold (reader, protrope) uc =
    match reader with
    | Start0 ->
      (match find_start_codon_first @@ Dna.of_uchar @@ uc with
       | false -> (reader, protrope)
       | true -> (Start1, protrope))
    | Start1 ->
      (match find_start_codon_second @@ Dna.of_uchar @@ uc with
       | false -> (reader, protrope)
       | true -> (Start2, protrope))
    | Start2 ->
      (match find_start_codon_last @@ Dna.of_uchar @@ uc with
       | false -> (reader, protrope)
       | true -> (Codon0, BatText.append_char (to_uchar Met) protrope))
    | Codon0 ->
      (Codon1 (of_codon @@ Dna.of_uchar @@ uc), protrope)
    | Codon1 reader ->
      (Codon2 (reader @@ Dna.of_uchar @@ uc), protrope)
    | Codon2 reader ->
      try
        let read = to_uchar (reader @@ Dna.of_uchar @@ uc) in
        (Codon0, BatText.append_char read protrope)
      with Stop_codon ->
        (Start0, protrope)
  in
  let _, protrope = BatText.fold f_fold (Start0, BatText.empty) dnarope in
  protrope

module U = Util
module I = U.Int

type uchar = BatUChar.t

type nuc =
  | A
  | T
  | C
  | G


type codon = nuc * nuc * nuc

type aa =
  | Ala
  | Cys
  | Asp
  | Glu
  | Phe
  | Gly
  | His
  | Ile
  | Lys
  | Leu
  (* start *)
  | Met
  | Asn
  (* special *)
  | Pyl
  | Pro
  | Gln
  | Arg
  | Ser
  | Thr
  (* special *)
  | Sec
  | Val
  | Trp
  | Tyr


type seq = BatText.t

type len = L of U.anyposint [@@ unbox]
type pos = P of U.anyposint [@@ unbox]

(* need to differentiate Ins(ertion) and Dup(lication) for homology *)
type seqdiff =
  | Not                      (* nothing *)
  | Sub of pos * uchar * uchar       (* (j, x, x') : site at j does x -> x' *)
  | Ins of pos * seq         (* (j, s) : s is inserted at pos j *)
  | Del of pos * seq         (* (j, s) : the seq s at [j,j+k[ is clipped *)
  | Dup of pos * pos * len   (* (j, j', k) : the seq at [j,j+k[ is duplicated at [j',j'+k[ *)
  | Undup of pos * pos * len (* when we undiff *) 
  | Inv of pos * len         (* (j, k) : the seq at [j,j+k[ is inverted *)

(* then seqdiff list -- tie it into seq pop
 * and record those at birth as atrait ?
 * then accumulate them going back up after coalesce --
 * after coalesce not too easy since we don't know the descent
 * but we should be able to go back up ? *)

%token SEMIC
%token <string> INT
%token EOF


%start <int list> input
%%

input:
  | n = inte ; SEMIC ; v = input    { n :: v }
  | n = inte ; EOF                  { [n] }
  ;

inte:
  | n = INT    { int_of_string n }
  ;

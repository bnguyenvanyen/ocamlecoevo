(** Sequences of nucleotides or amino-acids

    Uses a rope ([BatText.t]) representation.
    With an economic representation of substitutions, deletions, duplications,
    repetitions and reversions, that can be replayed onto the sequences.
 *)

module Sig :
  sig
    (** nucleotides *)
    type nuc = A | T | C | G
    type codon = nuc * nuc * nuc

    type aa =
      | Ala  (** alanine *)
      | Cys  (** cysteine *)
      | Asp  (** aspartic acid *)
      | Glu  (** glutamic acid *)
      | Phe  (** phenylalanine *)
      | Gly  (** glycine *)
      | His  (** histidine *)
      | Ile  (** isoleucine *)
      | Lys  (** lysine *)
      | Leu  (** leucine *)
      | Met  (** methionine *)
      | Asn  (** asparagine *)
      (* special *)
      | Pyl  (** pyrrolysine *)
      | Pro  (** proline *)
      | Gln  (** glutamine *)
      | Arg  (** arginine *)
      | Ser  (** serine *)
      | Thr  (** threonine *)
      (* special *)
      | Sec  (** selenocysteine *)
      | Val  (** valine *)
      | Trp  (** tryptophan *)
      | Tyr  (** tyrosine *)

    (** unicode character type from batteries, used in ropes *)
    type uchar = BatUChar.t

    (** the main type *)
    type seq = BatText.t

    (** integer length *)
    type len = L of Util.anyposint

    (** integer position (starts at 0) *)
    type pos = P of Util.anyposint

    (** event that can be patched on a seq *)
    type seqdiff =
      | Not                 (** no diff *)
      | Sub of pos * uchar * uchar  (** (j, x, x') : site at j does x -> x' *)
      | Ins of pos * seq  (** (j, s) : s is inserted at pos j *)
      | Del of pos * seq  (** (j, s) : the seq s at (j,j+k( is clipped *)
      | Dup of pos * pos * len (** (j, j', k) : the seq at (j,j+k( is duplicated at (j',j'+k( (so before the site at j') *)
      | Undup of pos * pos * len (** when we undiff *) 
      | Inv of pos * len  (** (j, k) : the seq at (j,j+k( is inverted *)
  end


module Io :
  sig
    val of_string : string -> Sig.seq
    val to_string : Sig.seq -> string
    val string_of_seqdiff : Sig.seqdiff -> string
    val seqdiff_of_string : string -> Sig.seqdiff
    val seqdiff_print : 'a BatIO.output -> Sig.seqdiff -> unit

    (** [fasta_of_assoc chan seqs] outputs the sequences [seqs]
        to [chan] in fasta format with integer tags. *)
    val fasta_of_assoc :
      out_channel ->
      (int * Sig.seq) list ->
        unit

    (** [stamped_fasta_of_assoc chan seqs] outputs the sequences [seqs]
        to [chan] in fasta format with tags "%i:%f". *)
    val stamped_fasta_of_assoc :
      out_channel ->
      (int * float * Sig.seq) list ->
        unit

    (** [assoc_of_fasta chan] reads the list of sequences
        contained in [chan] in fasta format, with integer tags.
        The result is [Some seqs] if parsing succeeds, and [None] otherwise. *)
    val assoc_of_fasta :
      in_channel ->
        (int * Sig.seq) list option

    (** [assoc_of_stamped_fasta chan] reads the list of sequences
        contained in [chan] in fasta format, with tags in format "%i:%f".
        The result is [Some seqs] if parsing succeeds, and [None] otherwise. *)
    val assoc_of_stamped_fasta :
      in_channel ->
        (int * float * Sig.seq) list option

    (** [assoc_of_any_fasta parse chan] reads the list of sequences
        contained in [chan] in fasta format, with tags read by [parse].
        The result is [Some seqs] if parsing succeeds, and [None] otherwise. *)
    val assoc_of_any_fasta :
      (string -> 'a option) ->
      in_channel ->
        ('a  * Sig.seq) list option

    val print : 'a BatIO.output -> Sig.seq -> unit
  end


module Diff :
  sig
    (** represent mutations of sequences *)
    type t = Sig.seqdiff =
      | Not
      | Sub of Sig.pos * Sig.uchar * Sig.uchar
      | Ins of Sig.pos * Sig.seq
      | Del of Sig.pos * Sig.seq
      | Dup of Sig.pos * Sig.pos * Sig.len
      | Undup of Sig.pos * Sig.pos * Sig.len
      | Inv of Sig.pos * Sig.len

    (** [copy sd] is a fresh copy of sd *)
    val copy :
      t ->
      t

    (** [undo sd] is the seqdiff that undoes sd *)
    val undo :
      t ->
      t

    (** [undo_list sdl] is the seqdiff list that undoes sdl *)
    val undo_list :
      t list ->
      t list

    (** [diff sdl sdl'] is the minimal list of seqdiffs 
      * that undoes sdl then applies sdl' *)
    val diff :
      t list ->
      t list ->
      t list
  end


module Dna :
  sig
    (** represent nucleotides *)
    type t = Sig.nuc = A | T | C | G

    (** [[A ; T ; C ; G ]] *)
    val nucleotides : t list

    val to_seq : t -> Sig.seq

    val of_uchar :
      Sig.uchar ->
      t

    val to_uchar :
      t ->
      Sig.uchar

    val of_int :
      int ->
      t

    val to_int :
      t ->
      int

    val print : 'a BatIO.output -> t -> unit
  end


module Prot :
  sig
    (** represent amino-acids *)
    type t = Sig.aa
    
    val of_char : char -> t
    val of_uchar : Sig.uchar -> t
    val to_char : t -> char
    val to_uchar : t -> Sig.uchar
    val to_int : t -> int
    val of_int : int -> t
    val to_codon : t -> Sig.codon list
    val of_codon : Sig.nuc -> Sig.nuc -> Sig.nuc -> t
    val find_start_codon : Sig.nuc -> Sig.nuc -> Sig.nuc -> bool
    val of_dna : BatText.t -> BatText.t
  end


type t = Sig.seq

val empty : t

(** length of the sequence *)
val length :
  t ->
  'b Util.posint

(** read sequence from string *)
val of_string :
  string ->
  t 

(** [to_string seq] converts the sequence [seq] to string *)
val to_string :
  t ->
  string

(** random (uniform) seq *)
val random :
  ?chars : char list ->
  ?rng : Random.State.t ->
  int ->
  t

(** get uchar at position n *)
val get :
  n : Sig.pos ->
  t ->
  Sig.uchar

(** set uchar at position n to x *)
val set :
  n : Sig.pos ->
  x : Sig.uchar ->
  t ->
  t 

(** [choose_site rng seq] chooses a site uniformly from [seq] *)
val choose_site :
  Random.State.t ->
  t ->
  Sig.pos * Sig.uchar

(** [inc n k] increases position n by length k *)
val inc :
  Sig.pos ->
  Sig.len ->
  Sig.pos

(** [pos_to_int n] is the integer associated with the position n *)
val pos_to_int : Sig.pos -> int

(** [len_to_int k] is the integer associated with the length k *)
val len_to_int : Sig.len -> int

(** specialized functions for nucleotides *)
val to_nuc_list :
  t ->
  Sig.nuc list

val of_nuc_list :
  Sig.nuc list ->
  t

val to_int_array : Sig.seq -> int array


(** [copy seq] is a copy of seq
 *  (shares the in memory representation of the rope) *)
val copy : t -> t

(** [sub ~n ~k seq] is the subsequence of seq
 *  from n (inclusive) to (n+k) (exclusive) *)
val sub :
  n : Sig.pos ->
  k : Sig.len ->
  t ->
  t

(** [append s s'] concatenates s and s' *)
val append :
  t ->
  t ->
  t 

(** [patch sd seq] applies the seqdiff to the seq *)
val patch :
  Sig.seqdiff ->
  t ->
  t

(** equality matrix between seqs *)
val equality :
  t ->
  t ->
  bool array array

(** homology matrix between seqs *)
val homology :
  t ->
  Sig.seqdiff list ->
  t * t * bool array array

(** [fold f x seq] folds [f] over the sequence [seq] *)
val fold : ('a -> Sig.uchar -> 'a) -> 'a -> t -> 'a

(** [iter f seq] iterates [f] over the sequence [seq] *)
val iter : (Sig.uchar -> unit) -> t -> unit

(** [iteri f seq] iterates [f] over the sequence [seq],
    index and uchar *)
val iteri : (int -> Sig.uchar -> unit) -> t -> unit

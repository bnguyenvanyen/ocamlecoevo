%parameter<S :
  sig
    type t
    val of_string : string -> t option
  end
>

%start <(S.t * Sig.seq) list option> input
%%

input:
  | CHEVRON ; ho = head ; NEWLINE ; x = seq ; BLANKLINE ; vo = input
  { Util.Option.map2 (fun h v -> (h, x) :: v) ho vo }
  | EOF
  { Some [] }
  ;

head:
  | h = OTHER
  { S.of_string h }
  ;

seq:
  | x = OTHER
  { Base.of_string x }
  | x = OTHER ; NEWLINE ; y = seq
  { Base.append (Base.of_string x) y }

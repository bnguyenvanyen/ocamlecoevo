{
  exception SyntaxError of string

  module T = Tokens
}

let white = [' ' '\t']+
let newline = '\r' | '\n' | "\r\n"
let blankline = newline newline
let digit = ['0' - '9']
let letter = ['A'-'Z' 'a'-'z']
let symbol = ['.' ',' ';' ':' '-' '_' '/']
(* nothing or any of digit|letter|symbol or newlines in the middle *)
let other = (digit | letter | symbol)*
(* let seq = ['A' 'T' 'C' 'G' 'U' 'a' 't' 'c' 'g' 'u']* *)

rule read =
  parse
  | white     { read lexbuf }
  | blankline { T.BLANKLINE }
  | newline   { T.NEWLINE }
  | other     { T.OTHER (Lexing.lexeme lexbuf) }
  | '>'       { T.CHEVRON }
  | eof       { T.EOF }
  | _         { raise (SyntaxError ("Unexpected char: " ^ Lexing.lexeme lexbuf)) }

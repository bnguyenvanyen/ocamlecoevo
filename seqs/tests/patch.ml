open Sig

let%expect_test _ =
  let sd = Seqs.Sig.Not in
  let seq = Seqs.of_string "ATCG" in
  let patched = Seqs.patch sd seq in
  print_string (Seqs.to_string patched) ;
  [%expect{| ATCG |}]


let%expect_test _ =
  let sd = Seqs.(Sig.(Sub (P I.one,
                           Dna.to_uchar T,
                           Dna.to_uchar A)))
  in
  let seq = Seqs.of_string "ATCG" in
  let patched = Seqs.patch sd seq in
  print_string (Seqs.to_string patched) ;
  [%expect{| AACG |}]


let%expect_test _ =
  let four = I.Pos.of_int 4 in
  let sd = Seqs.Sig.(Dup (P I.one, P four, L I.two)) in
  let seq = Seqs.of_string "ATCG" in
  let patched = Seqs.patch sd seq in
  print_string (Seqs.to_string patched) ;
  [%expect{| ATCGTC |}]


let%expect_test _ =
  let sd = Seqs.Sig.(Inv (P I.one, L I.two)) in
  let seq = Seqs.of_string "ATCG" in
  let patched = Seqs.patch sd seq in
  print_string (Seqs.to_string patched) ;
  [%expect{| ACTG |}]





let%expect_test _ =
  let s = Seqs.of_string "AAA" in
  let s' = Seqs.of_string "TTT" in
  let app = Seqs.append s s' in
  print_string (Seqs.to_string app) ;
  [%expect{| AAATTT |}]

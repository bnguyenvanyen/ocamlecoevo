open Sig


let%expect_test _ =
  let seq = Seqs.of_string "ATCG" in
  let sub = Seqs.sub ~n:(Seqs.Sig.P I.one) ~k:(Seqs.Sig.L I.two) seq in
  print_string (Seqs.to_string sub) ;
  [%expect{| TC |}]

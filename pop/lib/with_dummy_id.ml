open Sig


module Augment
  (P : POP_WITH_DYN with type 'a isid = nonid
                     and type group_data = Nonid.group_data) =
  struct
    type 'a stoch = 'a

    type 'a isid = 'a
    type 'a trait = 'a P.trait
    type 'a group = 'a P.group
    type 'a indiv = 'a P.indiv

    module I = P.I
    module T = P.T

    type group_data = P.group_data

    type t = P.t

    let create = P.create

    let copy = P.copy

    let new_indiv (type a) _ (x : a trait) : a indiv =
      match T.isid (T.group_of x) with
      | Isnotid Eq ->
          I.nonid x
      | Isid Eq ->
          invalid_arg "new_indiv id"

    let indiv_from_group =
      P.indiv_from_group

    let count (type a) ~(group : a group) z =
      match T.isid group with
      | Isnotid Eq ->
          P.count ~group z
      | Isid Eq ->
          invalid_arg "count id"

    let fold_groups = P.fold_groups

    let fold_indivs (type a) (f : a indiv -> 'b -> 'b) z (g : a group) y0 =
      match T.isid g with
      | Isnotid Eq ->
          P.fold_indivs f z g y0
      | Isid Eq ->
          invalid_arg "fold_indivs id"

    let add :
      type a. t -> a I.t -> unit =
      fun z ->
      function
      | (I.Nonid _ as ind) ->
          P.add z ind
      | I.Id _ ->
          invalid_arg "add id"

    let remove :
      type a. t -> a I.t -> unit =
      fun z ->
      function
      | (I.Nonid _ as ind) ->
          P.remove z ind
      | I.Id _ ->
          invalid_arg "remove id"

    let choose (type a) ~rng ~(group : a group) _ : a I.t =
      ignore rng ;
      match T.isid group with
      | Isnotid Eq ->
          indiv_from_group group
      | Isid Eq ->
          invalid_arg "choose id"

    let check_counts z = P.check_counts z
  end


module Make (T : TRAIT) =
  struct
    include Augment (Nonid.Make (T))
  end

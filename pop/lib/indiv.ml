open Sig


module Make
  (T :
    sig
      type 'a t
      val copy : 'a t -> 'a t
    end) :
  (INDIV with type 'a trait = 'a T.t) =
  struct
    type 'a trait = 'a T.t

    type _ t =
      | Id : id trait * int -> id t
      | Nonid : nonid trait -> nonid t

    let id x i = Id (x, i)

    let nonid x = Nonid x

    let trait_of :
      type a. a t -> a trait =
      function
      | Nonid x ->
          x
      | Id (x, _) ->
          x

    let copy :
      type a. a t -> a t =
      function
      | Nonid x ->
          Nonid (T.copy x)
      | Id (x, i) ->
          Id (T.copy x, i)

    let ident (Id (_, i)) = i
  end

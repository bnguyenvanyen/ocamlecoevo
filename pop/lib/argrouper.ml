(* Following idea from François
 * Batteries.DynArray plus set of unoccupied spaces
 * int DynArray where each occupied space stores an ident
 * unoccupied spaces store ~-1 or something *)

(* FIXME type confusion between int and ident *)

open Sig

module A = BatDynArray
module I = U.Int

module Is = BatSet.Make(struct
  type t = int
  let compare (i:int) (i':int) = compare i i'
end)

type t = {
  idents : int A.t ;
  mutable unused : Is.t ;
  mutable n_unused : U.anyposint ;
}


let make n = {
  idents = A.make n ;  (* or init to set init values ? *)
  unused = Is.empty ;
  n_unused = I.zero ;
}


let copy { idents ; unused ; n_unused } =
  { idents = A.copy idents ;
    unused ;
    n_unused = n_unused }


let is_unused ag k = Is.mem k ag.unused

let is_alive ag k = not (is_unused ag k)

let get_opt k ag =
  if is_alive ag k then
    (* FIXME should we also return k ? *)
    Some (k, A.get ag.idents k)
  else
    None


let count_total ag : 'a U.posint =
  I.of_int_unsafe (A.length ag.idents)


let count_alive ag : 'a U.posint =
  I.promote_unsafe (I.sub (count_total ag) ag.n_unused)


let compact ag =
  (* delete unused idents from the last *)
  let rec f un =
    match Is.pop_max un with
    | k, un' ->
        A.delete ag.idents k ;
        f un'
    | exception Not_found ->
        (* is is empty *)
        ag.unused <- un ;
        ag.n_unused <- I.of_int_unsafe (Is.cardinal un)
  in
  f ag.unused ;
  A.compact ag.idents


let hundred = I.Pos.of_int 100

let maybe_compact ?(ratio=hundred) ag =
  let n = count_total ag in
  let n_alive = count_alive ag in
  if I.Op.(ratio * n_alive < n) then
    compact ag


let to_list ag =
  (* easiest implementation but not necessarily efficient *)
  let lr = ref [] in
  let f k i =
    if is_alive ag k then
      lr := i :: !lr
  in
  A.iteri f ag.idents ;
  !lr

let fold f ag y0 =
  (* no "foldi" so we need to use iteri *)
  let yr = ref y0 in
  let f' k i =
    if is_alive ag k then
      yr := f i !yr
  in
  A.iteri f' ag.idents ;
  !yr

(* insert ident i into ag. also returns chosen index k of insertion *)
let insert i ag =
  match Is.choose ag.unused with
  | k ->
      A.set ag.idents k i ;
      ag.unused <- Is.remove k ag.unused ;
      ag.n_unused <- I.promote_unsafe (I.pred ag.n_unused) ;
      k
  | exception Not_found ->
      A.add ag.idents i ;
      (* new added index *)
      A.length ag.idents - 1


(* remove ident i positioned at index k from ag *)
let remove k ag =
  (* est-ce qu'on a vraiment besoin d'enlever la valeur ? *)
  A.set ag.idents k (~-1) ;
  ag.unused <- Is.add k ag.unused ;
  ag.n_unused <- I.Pos.succ ag.n_unused


(*
(* choose uniformly from used (alive) idents. also returns chosen array index *)
let choose ?rng ag =
  let n = A.length ag.idents in
  let n_alive = n - ag.n_unused in
  if 100 * n_alive < n then compact ag ;
  (* n changes if compact *)
  let n = A.length ag.idents in
  (* check ident chosen is used (alive) *)
  let g () = Util.rand_int ?rng n in
  match Util.valid (is_alive ag) g with
  | k ->
      (k, A.get ag.idents k)
  | exception (Failure _) ->
      (* check if there is anyone alive left directly *)
      let m = Is.cardinal ag.unused in
      if m = n then
        failwith "No alive individual left to choose"
      else
        failwith "No alive individual found (but they exist)"
*)

let ntries = hundred
let twenty = I.Pos.of_int 20

let choose ~rng ag =
  (* count_total after count_alive as it might change in count_alive *)
  maybe_compact ~ratio:I.Pos.Op.(ntries / twenty) ag ;
  let n = count_total ag in
  let f ~rng =
    let k = I.to_int (Util.rand_int ~rng n) in
    get_opt k ag
  in
  match Util.insist ~rng ~ntries f with
  | i ->
      i
  | exception (Failure _ as e) ->
      let m = I.of_int_unsafe (Is.cardinal ag.unused) in
      if m = n then
        failwith "Empty"
      else
        raise e

let find i ag =
  A.index_of (fun i' -> i' = i) ag.idents

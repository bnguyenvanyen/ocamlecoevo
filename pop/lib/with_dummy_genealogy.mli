(** A population with genealogy NOT tracked. *)


(** Corresponds to the POP_COMPLEX signature,
  * but is actually identical to Pop.With_max. *)

open Sig


module Augment :
  functor (P : POP_WITH_DYN) ->
  (POP_WITH_GENEALOGY_DYN with type 'a stoch = 'a P.stoch
                           and type 'a isid = 'a P.isid
                           and type 'a trait = 'a P.trait
                           and type 'a group = 'a P.group
                           and type 'a I.t = 'a P.I.t
                           and type group_data = P.group_data
                           and type t = P.t)



module Augment_id :
  functor (P : POP_ID_WITH_DYN) ->
  (POP_WITH_GENEALOGY_DYN with type 'a stoch = 'a P.stoch
                           and type 'a isid = 'a P.isid
                           and type 'a trait = 'a P.trait
                           and type 'a group = 'a P.group
                           and type 'a I.t = 'a P.I.t
                           and type group_data = P.group_data
                           and type t = P.t)


module Augment_max :
  functor (P : POP_WITH_MAX_DYN) ->
  (POP_WITH_MAX_GENEALOGY_DYN with type 'a stoch = 'a P.stoch
                               and type 'a isid = 'a P.isid
                               and type 'a trait = 'a P.trait
                               and type 'a group = 'a P.group
                               and type 'a I.t = 'a P.I.t
                               and type group_data = P.group_data
                               and type t = P.t)


module Make :
  functor (T : TRAIT) ->
  (POP_WITH_MAX_GENEALOGY_DYN with type 'a stoch = U.rng
                               and type 'a isid = 'a
                               and type 'a trait = 'a T.t
                               and type 'a group = 'a T.group)

(** A population with genealogy tracked. *)


(** Corresponds to the POP_COMPLEX signature
  * Builds on Pop.With_max.
  * Tracks number of individuals in groups, 
  * traits of identified individuals,
  * maximum values in the population under some comparison functions
  * (given at functor application),
  * and genealogy of individuals.
  *)

open Sig


module Augment :
  functor (P : POP_ID_WITH_DYN with type 'a isid = 'a) ->
  (POP_WITH_GENEALOGY_DYN with type 'a stoch = 'a P.stoch
                           and type 'a isid = 'a P.isid
                           and type 'a trait = 'a P.trait
                           and type 'a group = 'a P.group
                           and type 'a I.t = 'a P.I.t
                           and type group_data = P.group_data)


module Augment_max :
  functor (P : POP_WITH_MAX_DYN with type 'a isid = 'a) ->
  (POP_WITH_MAX_GENEALOGY_DYN with type 'a stoch = 'a P.stoch
                               and type 'a isid = 'a P.isid
                               and type 'a trait = 'a P.trait
                               and type 'a group = 'a P.group
                               and type 'a I.t = 'a P.I.t
                               and type group_data = P.group_data)


(** Functor to get a POP_COMPLEX from a TRAIT *)
module Make :
  functor (T : TRAIT) ->
  (POP_WITH_GENEALOGY_DYN with type 'a stoch = U.rng
                           and type 'a isid = 'a
                           and type 'a trait = 'a T.t
                           and type 'a group = 'a T.group
                           and type group_data = With_id.group_data)


(* Map with max size implemented through map + fifo *)


type ('a, 'b) t = {
  max : int ;
  queue : 'a Queue.t ;
  table : ('a, 'b) Hashtbl.t ;
}

let create n = {
  max = n ;
  queue = Queue.create () ;
  table = Hashtbl.create n ;
}


let copy { max ; queue ; table } =
  { max ;
    queue = Queue.copy queue ;
    table = Hashtbl.copy table }


let add k v m =
  if Queue.length m.queue >= m.max then
    begin
      let k' = Queue.take m.queue in
      Hashtbl.remove m.table k'
    end ;
  Queue.add k m.queue ;
  Hashtbl.replace m.table k v

let find k m =
  Hashtbl.find m.table k



open Sig


module Augment :
  functor (P : POP_WITH_DYN) ->
    (POP_WITH_MAX_DYN with type 'a stoch = 'a P.stoch
                       and type 'a isid = 'a P.isid
                       and type 'a trait = 'a P.trait
                       and type 'a group = 'a P.group
                       and type 'a I.t = 'a P.I.t
                       and type group_data = P.group_data
                       and type t = P.t)


module Augment_id :
  functor (P : POP_ID_WITH_DYN) ->
  (POP_WITH_MAX_DYN with type 'a stoch = 'a P.stoch
                     and type 'a isid = 'a P.isid
                     and type 'a trait = 'a P.trait
                     and type 'a group = 'a P.group
                     and type 'a I.t = 'a P.I.t
                     and type group_data = P.group_data
                     and type t = P.t)


module Augment_genealogy :
  functor (P : POP_WITH_GENEALOGY_DYN) ->
  (POP_WITH_MAX_GENEALOGY_DYN with type 'a stoch = 'a P.stoch
                               and type 'a isid = 'a P.isid
                               and type 'a trait = 'a P.trait
                               and type 'a group = 'a P.group
                               and type 'a I.t = 'a P.I.t
                               and type group_data = P.group_data
                               and type t = P.t)

(* This is sort of a hack,
 * for best compatibility with POP_SIMPLE etc,
 * with some cost incurred compared to a simpler version *)

open Sig


type group_data = {
  mutable count : int ;
}


module Make (T : TRAIT) :
  (POP_WITH_DYN with type 'a isid = nonid
                 and type 'a trait = 'a T.t
                 and type 'a group = 'a T.group
                 and type 'a stoch = 'a
                 and type group_data = group_data) =
  struct
    (* only nonid indivs will actually ever exist *)
    module I = Indiv.Make (T)
    module T = T

    type 'a stoch = 'a (* for choose *)

    type 'a isid = nonid

    type 'a trait = 'a T.t
    type 'a group = 'a T.group
    type 'a indiv = 'a I.t

    type nonrec group_data = group_data

    type t = (nonid group, group_data) H.t

    let create ~ngroups =
      let z = H.create ngroups in
      z

    let find_nonid z g =
      match H.find z g with
      | gd ->
          gd
      | exception Not_found ->
          let gd = { count = 0 } in
          H.add z g gd ;
          gd

    let copy z =
      H.map (fun _ { count } -> { count }) z

    let new_indiv _ x = I.nonid x

    let indiv_from_group g = I.nonid (T.of_group g)

    let count_nonid ~group z : 'a U.posint =
      match find_nonid z group with { count } -> U.Int.of_int_unsafe count

    let count ~group z =
      count_nonid ~group z

    (* FIXME can't fold on groups that don't exist yet.
     * Do we really need this ? *)
    let fold_groups f z y0 =
      H.fold (fun _ gdni y' -> f gdni y') z y0

    (* actually does not 'fold' per se, only applies the function
     * to the canonical individual from group [g] *)
    let fold_indivs f _ g y0 =
      let ind = I.nonid (T.of_group g) in
      f ind y0

    let add z ((I.Nonid x) : nonid I.t) =
      let g = T.group_of x in
      match find_nonid z g with ({ count } as gd) ->
        gd.count <- count + 1

    let remove z ((I.Nonid x) : nonid I.t) =
      let g = T.group_of x in
      match find_nonid z g with ({ count } as gd) ->
        if count <= 0 then raise Not_found ;
        gd.count <- count - 1

    let choose ~rng ~group _ =
      ignore rng ;
      indiv_from_group group

    let check_counts _ =
      ()
  end

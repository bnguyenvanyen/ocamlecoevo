open Sig

type group_data_id = {
  mutable count : int ;
  idents : Argrouper.t ;
}

type group_data =
  | Id of group_data_id
  | Nonid of Nonid.group_data


module Augment :
  functor (P : POP_WITH_DYN with type 'a isid = nonid
                             and type group_data = Nonid.group_data) ->
    (POP_ID_WITH_DYN with type 'a stoch = U.rng
                      and type 'a isid = 'a
                      and type 'a trait = 'a P.trait
                      and type 'a group = 'a P.group
                      and type 'a I.t = 'a P.I.t
                      and type group_data = group_data)


module Make :
  functor (T : TRAIT) ->
    (POP_ID_WITH_DYN with type 'a stoch = U.rng
                      and type 'a isid = 'a
                      and type 'a trait = 'a T.t
                      and type 'a group = 'a T.group
                      and type group_data = group_data)

open Sig

(* the dummy implementation for scores, compares and max functions *)
module Dummy =
  struct
    let scores ~i _ =
      ignore i ;
      (fun _ -> 0.)

    let compares ~i _ =
      ignore i ;
      (fun _ _ -> 0)

    let max ~i _ =
      ignore i ;
      raise Not_found
  end


module Augment (P : POP_WITH_DYN) =
  struct
    include (P : POP_WITH_DYN_WO_CREATE
      with type 'a stoch = 'a P.stoch
       and type 'a isid = 'a P.isid
       and type 'a trait = 'a P.trait
       and type 'a group = 'a P.group
       and type 'a I.t = 'a P.I.t
       and type group_data = P.group_data
       and type t = P.t
    )

    let create ?mem ~ngroups ~scores ~nindivs =
      ignore mem ;
      ignore scores ;
      ignore nindivs ;
      P.create ~ngroups
    
    include Dummy
  end


module Augment_id (P : POP_ID_WITH_DYN) =
  struct
    include (P : POP_WITH_DYN_WO_CREATE
      with type 'a stoch = 'a P.stoch
       and type 'a isid = 'a P.isid
       and type 'a trait = 'a P.trait
       and type 'a group = 'a P.group
       and type 'a I.t = 'a P.I.t
       and type group_data = P.group_data
       and type t = P.t
    )

    let create ?mem ~ngroups ~scores ~nindivs =
      ignore scores ;
      P.create ?mem ~ngroups ~nindivs

    include Dummy
  end


module Augment_genealogy (P : POP_WITH_GENEALOGY_DYN) =
  struct
    include (P : POP_WITH_GENEALOGY_DYN_WO_CREATE
      with type 'a stoch = 'a P.stoch
       and type 'a isid = 'a P.isid
       and type 'a trait = 'a P.trait
       and type 'a group = 'a P.group
       and type 'a I.t = 'a P.I.t
       and type group_data = P.group_data
       and type t = P.t
    )

    let create ?mem ~ngroups ~scores ~nindivs =
      ignore scores ;
      P.create ?mem ~ngroups ~nindivs

    include Dummy
  end


module Make (T : TRAIT) =
  struct
    include Augment_id (With_id.Make (T))
  end
